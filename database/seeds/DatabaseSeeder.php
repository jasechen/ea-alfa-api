<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use App\Services\AuthorServ;
use App\Services\AuthorSeoServ;
use App\Services\CategoryServ;
use App\Services\CategorySeoServ;
use App\Services\PostServ;
use App\Services\PostSeoServ;
use App\Services\FileServ;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->ImportAuthorTableSeeder();
        $this->ImportCategoryTableSeeder();
        $this->ImportPostTableSeeder();
    }


    public function ImportAuthorTableSeeder()
    {

        $authorServ = app(AuthorServ::class);
        $authorSeoServ = app(AuthorSeoServ::class);
        $fileServ = app(FileServ::class);

        $parentType = 'author';
        $status = 'disable';
        $fileType = 'image';

        $defaultAuthorSData = config('default_authors');

        foreach ($defaultAuthorSData as $key => $value) {

            $value['slug'] = str_replace(['"'," ",".html"], "", trim($value['slug']));

            $authorSeoDatum = $authorSeoServ->findBySlug($value['slug']);

            if ($authorSeoDatum->isNotEmpty()) {
                continue;
            } // ENd if

            unset($value['id']);


            $imageLink = $value['feature_image_url'];
            $imagePath = parse_url($imageLink, PHP_URL_PATH);

            $imageName = pathinfo($imagePath, PATHINFO_FILENAME);
            $imageExt  = pathinfo($imagePath, PATHINFO_EXTENSION);
            $saveImagePath = '/tmp/'.$imageName.'.'.$imageExt;

            copy($imageLink, $saveImagePath);

            $fileSize = filesize($saveImagePath);
            $fileExtension = $imageExt;
            $fileMimeType = $fileType . '/' . $fileExtension;
            $filename = $fileServ->convertImage($parentType, $saveImagePath, $fileExtension);

            $fileDatum = $fileServ->create($parentType, 0, 0, $filename, $fileExtension, $fileMimeType, $fileSize, $fileType, $status);

            if ($fileDatum->isNotEmpty()) {
                unlink($saveImagePath);
            } // END if


            $authorDatum = $authorServ->create($value['name'], $filename, $value['meta_description']);

            if ($authorDatum->isEmpty()) {
                continue;
            } // ENd if

            $authorId = $authorDatum->first()->id;
            $slug    = $value['slug'];
            $excerpt = $value['excerpt'];
            $ogTitle       = $value['og_title'];
            $ogDescription = $value['og_description'];
            $metaTitle       = $value['meta_title'];
            $metaDescription = $value['meta_description'];
            $avatarTitle = $value['feature_image_title'];
            $avatarAlt   = $value['feature_image_alt'];

            $authorSeoServ->create($authorId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $avatarTitle, $avatarAlt);

            Storage::disk('gallery')->makeDirectory($parentType . '/' . $authorId, 0755, true);
            $fileServ->moveFileTo($filename, $parentType.'/' . $authorId, $parentType);

            $cmd = 'chown -R www-data:www-data '.Storage::disk('gallery')->path($parentType . '/' . $authorId);
            system($cmd);

            $fileServ->update(['parent_id' => $authorId, 'status' => 'enable'], ['id' => $fileDatum->first()->id]);


            if (!empty(env('AWS_BUCKET_GALLERY'))) {
                $filePaths = $fileServ->findLinks($filename);

                foreach ($filePaths as $filePath) {
                    $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                    $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                    $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                    if (empty($exists)) {
                        $sFilename = public_path($filePath);
                        $sFilename = new File($sFilename);
                        Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                    } // END if
                } // END foreach

            } // END if

        } // END foreach

    } // END function


    public function ImportCategoryTableSeeder()
    {

        $categoryServ = app(CategoryServ::class);
        $categorySeoServ = app(CategorySeoServ::class);
        $fileServ = app(FileServ::class);

        $parentType = 'category';
        $status = 'disable';
        $fileType = 'image';

        $defaultCategoryData = config('default_categories');

        foreach ($defaultCategoryData as $key => $value) {

            $value['slug'] = str_replace(['"'," ",".html"], "", trim($value['slug']));

            $categoryDatum = $categoryServ->findByName($value['name']);

            if ($categoryDatum->isNotEmpty()) {
                continue;
            } // ENd if

            unset($value['id']);
            unset($value['channel_id']);


            $imageLink = $value['feature_image_url'];
            $imagePath = parse_url($imageLink, PHP_URL_PATH);

            $imageName = pathinfo($imagePath, PATHINFO_FILENAME);
            $imageExt  = pathinfo($imagePath, PATHINFO_EXTENSION);
            $saveImagePath = '/tmp/'.$imageName.'.'.$imageExt;

            copy($imageLink, $saveImagePath);

            $fileSize = filesize($saveImagePath);
            $fileExtension = $imageExt;
            $fileMimeType = $fileType . '/' . $fileExtension;
            $filename = $fileServ->convertImage($parentType, $saveImagePath, $fileExtension);

            $fileDatum = $fileServ->create($parentType, 0, 0, $filename, $fileExtension, $fileMimeType, $fileSize, $fileType, $status);

            if ($fileDatum->isNotEmpty()) {
                unlink($saveImagePath);
            } // END if


            $categoryDatum = $categoryServ->create($value['name'], $filename, 'enable');

            if ($categoryDatum->isEmpty()) {
                continue;
            } // ENd if

            $categoryId = $categoryDatum->first()->id;
            $slug    = $value['slug'];
            $excerpt = $value['excerpt'];
            $ogTitle       = $value['og_title'];
            $ogDescription = $value['og_description'];
            $metaTitle       = $value['meta_title'];
            $metaDescription = $value['meta_description'];
            $coverTitle = $value['feature_image_title'];
            $coverAlt   = $value['feature_image_alt'];

            $categorySeoServ->create($categoryId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);


            Storage::disk('gallery')->makeDirectory($parentType . '/' . $categoryId, 0755, true);
            $fileServ->moveFileTo($filename, $parentType.'/' . $categoryId, $parentType);

            $cmd = 'chown -R www-data:www-data '.Storage::disk('gallery')->path($parentType . '/' . $categoryId);
            system($cmd);

            $fileServ->update(['parent_id' => $categoryId, 'status' => 'enable'], ['id' => $fileDatum->first()->id]);


            if (!empty(env('AWS_BUCKET_GALLERY'))) {
                $filePaths = $fileServ->findLinks($filename);

                foreach ($filePaths as $filePath) {
                    $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                    $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                    $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                    if (empty($exists)) {
                        $sFilename = public_path($filePath);
                        $sFilename = new File($sFilename);
                        Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                    } // END if
                } // END foreach
            } // END if

        } // END foreach
    } // END function


    public function ImportPostTableSeeder()
    {

        $postServ = app(PostServ::class);
        $postSeoServ = app(PostSeoServ::class);
        $authorSeoServ = app(AuthorSeoServ::class);
        $categorySeoServ = app(CategorySeoServ::class);
        $categoryServ = app(CategoryServ::class);
        $fileServ = app(FileServ::class);

        $parentType = 'post';
        $status = 'disable';
        $fileType = 'image';

        $defaultPostData = config('default_posts');
        $defaultAuthorData = config('default_authors');
        $defaultCategoryData = config('default_categories');

        foreach ($defaultPostData as $key => $value) {

            $value['slug'] = str_replace(['"'," ",".html"], "", trim($value['slug']));

            $postSeoDatum = $postSeoServ->findBySlug($value['slug']);

            if ($postSeoDatum->isNotEmpty()) {
                continue;
            } // ENd if


            foreach ($defaultAuthorData as $ak => $av) {
                if ($av['id'] != $value['author_id']) continue;
                $authorSlug = $av['slug'];
            } // END foreach

            if (empty($authorSlug)) continue;
            $authorSeoDatum = $authorSeoServ->findBySlug($authorSlug);
            $authorId = $authorSeoDatum->first()->author_id;


            foreach ($defaultCategoryData as $ck => $cv) {
                if ($cv['id'] != $value['category_id']) continue;
                $categorySlug = $cv['slug'];
            } // END foreach

            if (empty($categorySlug)) continue;
            $categorySeoDatum = $categorySeoServ->findBySlug($categorySlug);
            $categoryId = $categorySeoDatum->first()->category_id;

            unset($value['id']);


            $imageLink = $value['feature_image_url'];
            $imagePath = parse_url($imageLink, PHP_URL_PATH);

            $imageName = pathinfo($imagePath, PATHINFO_FILENAME);
            $imageExt  = pathinfo($imagePath, PATHINFO_EXTENSION);
            $saveImagePath = '/tmp/'.$imageName.'.'.$imageExt;

            copy($imageLink, $saveImagePath);

            $fileSize = filesize($saveImagePath);
            $fileExtension = $imageExt;
            $fileMimeType = $fileType . '/' . $fileExtension;
            $filename = $fileServ->convertImage($parentType, $saveImagePath, $fileExtension);

            $fileDatum = $fileServ->create($parentType, 0, 0, $filename, $fileExtension, $fileMimeType, $fileSize, $fileType, $status);

            if ($fileDatum->isNotEmpty()) {
                unlink($saveImagePath);
            } // END if


            $published = $value['published'] == 1 ? 'publish' : 'draft';

            $postDatum = $postServ->create($value['title'], $value['content'], $filename, $authorId, $categoryId, $published, false);

            if ($postDatum->isEmpty()) {
                continue;
            } // ENd if

            $postId = $postDatum->first()->id;
            $slug    = $value['slug'];
            $excerpt = $value['excerpt'];
            $canonicalUrl = $value['canonical_url'];
            $ogTitle       = $value['og_title'];
            $ogDescription = $value['og_description'];
            $metaTitle       = $value['meta_title'];
            $metaDescription = $value['meta_description'];
            $coverTitle = $value['feature_image_title'];
            $coverAlt   = $value['feature_image_alt'];

            $postSeoServ->create($postId, $slug, $excerpt, $canonicalUrl, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);

            if ($published == 'publish') {
                $categoryServ->updateNumPosts($categoryId);
            } // END if


            Storage::disk('gallery')->makeDirectory($parentType . '/' . $postId, 0755, true);
            $fileServ->moveFileTo($filename, $parentType.'/' . $postId, $parentType);

            $cmd = 'chown -R www-data:www-data '.Storage::disk('gallery')->path($parentType . '/' . $postId);
            system($cmd);

            $fileServ->update(['parent_id' => $postId, 'status' => 'enable'], ['id' => $fileDatum->first()->id]);


            if (!empty(env('AWS_BUCKET_GALLERY'))) {
                $filePaths = $fileServ->findLinks($filename);

                foreach ($filePaths as $filePath) {
                    $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                    $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                    $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                    if (empty($exists)) {
                        $sFilename = public_path($filePath);
                        $sFilename = new File($sFilename);
                        Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                    } // END if
                } // END foreach
            } // END if

        } // END foreach
    } // END function


}
