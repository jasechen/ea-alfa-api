<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\CategoryServ;
use App\Services\CategorySeoServ;
use App\Services\PostServ;
use App\Services\FileServ;

class Category extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ  = app(SessionServ::class);
        $this->categoryServ = app(CategoryServ::class);
        $this->categorySeoServ = app(CategorySeoServ::class);
        $this->postServ = app(PostServ::class);
        $this->fileServ = app(FileServ::class);
    } // END function


    /**
     * create
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $name   = $request->input('name');
        $cover   = $request->input('cover');
        $status   = $request->input('status', config('tbl_categories.DEFAULT_CATEGORIES_STATUS'));

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($cover)) {
            $code = 400;
            $comment = 'cover empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($excerpt)) {
            $code = 400;
            $comment = 'excerpt empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogTitle)) {
            $code = 400;
            $comment = 'og_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogDescription)) {
            $code = 400;
            $comment = 'og_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaTitle)) {
            $code = 400;
            $comment = 'meta_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaDescription)) {
            $code = 400;
            $comment = 'meta_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverTitle)) {
            $code = 400;
            $comment = 'cover_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverAlt)) {
            $code = 400;
            $comment = 'cover_alt empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $categorySeoDatum = $this->categorySeoServ->findBySlug($slug);

        if ($categorySeoDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'slug error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findByFilename($cover);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'cover error';

            Jsonponse::fail($comment, $code);
        } // END if

        $categoryDatum = $this->categoryServ->findByName($name);

        if ($categoryDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'name error';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum = $this->categoryServ->create($name, $cover, $status);

        if ($categoryDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $this->categorySeoServ->create($categoryDatum->first()->id, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);


        Storage::disk('gallery')->makeDirectory('category/' . $categoryDatum->first()->id, 0755, true);

        $updatedata = ['parent_id' => $categoryDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        $updateWhere = ['id' => $fileDatum->first()->id];
        $this->fileServ->update($updatedata, $updateWhere);

        $this->fileServ->moveFileTo($cover, 'category/' . $categoryDatum->first()->id, 'category');


        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_GALLERY'))) {
            $filePaths = $this->fileServ->findLinks($cover);

            foreach ($filePaths as $filePath) {
                $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                if (empty($exists)) {
                    $sFilename = public_path($filePath);
                    $sFilename = new File($sFilename);
                    Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                } // END if
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'category_id' => $categoryDatum->first()->id];

        Jsonponse::success('create success', $resultData);
    } // END function


    /**
     * update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $categoryDatum = $this->categoryServ->findById($id);

        if ($categoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $name   = $request->input('name');
        $cover   = $request->input('cover');
        $status   = $request->input('status');

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');

        $updateData = $updateSeoData = [];

        if (!empty($name) AND $name != $categoryDatum->first()->name) {
            $updateData['name'] = $name;
        } // END if

        if (!empty($cover) AND $cover != $categoryDatum->first()->cover) {
            $fileDatum = $this->fileServ->findByFilename($cover);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'cover error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['cover'] = $cover;
        } // END if

        if (!empty($status) AND $status != $categoryDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_categories.CATEGORIES_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if

       if (!empty($slug) AND $slug != $categoryDatum->first()->slug) {
            $categorySeoDatum = $this->categorySeoServ->findBySlug($slug);

            if ($categorySeoDatum->isNotEmpty() AND $categorySeoDatum->first()->category_id != $id) {
                $code = 409;
                $comment = 'slug error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateSeoData['slug'] = $slug;
        } // END if

        if (!empty($excerpt) AND $excerpt != $categoryDatum->first()->excerpt) {
            $updateSeoData['excerpt'] = $excerpt;
        } // END if

        if (!empty($ogTitle) AND $ogTitle != $categoryDatum->first()->og_title) {
            $updateSeoData['og_title'] = $ogTitle;
        } // END if

        if (!empty($ogDescription) AND $ogDescription != $categoryDatum->first()->og_description) {
            $updateSeoData['og_description'] = $ogDescription;
        } // END if

        if (!empty($metaTitle) AND $metaTitle != $categoryDatum->first()->meta_title) {
            $updateSeoData['meta_title'] = $metaTitle;
        } // END if

        if (!empty($metaDescription) AND $metaDescription != $categoryDatum->first()->meta_description) {
            $updateSeoData['meta_description'] = $metaDescription;
        } // END if

        if (!empty($coverTitle) AND $coverTitle != $categoryDatum->first()->cover_title) {
            $updateSeoData['cover_title'] = $coverTitle;
        } // END if

        if (!empty($coverAlt) AND $coverAlt != $categoryDatum->first()->cover_alt) {
            $updateSeoData['cover_alt'] = $coverAlt;
        } // END if


        if (empty($updateData) AND empty($updateSeoData)) {
            $code = 400;
            $comment = 'updateData / updateSeoData empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $updateFail = $updateSeoFail = false;

        if (!empty($updateData)) {
            $categoryDatum = $this->categoryServ->update($updateData, ['id' => $id]);

            if (empty($categoryDatum) OR $categoryDatum->isEmpty()) {
                $updateFail = true;
            } else {
                if (!empty($updateData['cover'])) {
                    $updatedata = ['parent_id' => $categoryDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
                    $updateWhere = ['id' => $fileDatum->first()->id];

                    $this->fileServ->update($updatedata, $updateWhere);

                    $this->fileServ->moveFileTo($cover, 'category/' . $categoryDatum->first()->id);

                    if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_GALLERY'))) {
                        $filePaths = $this->fileServ->findLinks($cover);

                        foreach ($filePaths as $filePath) {
                            $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                            $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                            $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                            if (empty($exists)) {
                                $sFilename = public_path($filePath);
                                $sFilename = new File($sFilename);
                                Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                            } // END if
                        } // END foreach
                    } // END if

                } // END if
            } // END if
        } // END if

        if (!empty($updateSeoData)) {
            $categorySeoDatum = $this->categorySeoServ->findByCategoryId($id);

            if ($categorySeoDatum->isNotEmpty()) {
                $categorySeoDatum = $this->categorySeoServ->update($updateSeoData, ['id' => $categorySeoDatum->first()->id]);

                if (empty($categorySeoDatum) OR $categorySeoDatum->isEmpty()) {
                    $updateSeoFail = true;
                } // END if
            } // END if
        } // END if

        if (!empty($updateFail) OR !empty($updateSeoFail)) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $categoryDatum = $this->categoryServ->findById($id);

        if ($categoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateData = ['status' => config('tbl_categories.CATEGORIES_STATUS_DELETE')];
        $categoryDatum = $this->categoryServ->update($updateData, ['id' => $id]);

        if ($categoryDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $categoryDatum = $this->categoryServ->findById($id);

        if ($categoryDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $categoryDatum->first()->cover_links = [];
        if (!empty($categoryDatum->first()->cover)) {
            $coverLinkData = $this->fileServ->findLinks($categoryDatum->first()->cover);
            $categoryDatum->first()->cover_links = $coverLinkData;
        } // END if else

        $categoryDatum->first()->s3_url = env('AWS_BUCKET_URL');
        $categoryDatum->first()->url_path = empty($categoryDatum->first()->slug) ? '' : '/' . $categoryDatum->first()->slug . '-c' . $categoryDatum->first()->id;


        // $resultData = ['session' => $sessionCode, 'category' => $categoryDatum->first()];
        $resultData = ['category' => $categoryDatum->first()];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     *
     * @throws
     * @return
     */
    public function findAll(Request $request, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['c.id' => $orderWay];
        $status = config('tbl_categories.DEFAULT_CATEGORIES_STATUS');

        $categoryData = $this->categoryServ->findAll($status, $orderby, $page);

        if ($categoryData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($categoryData->all() as $categoryDatum) {

            $categoryDatum->cover_links = [];
            if (!empty($categoryDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($categoryDatum->cover);
                $categoryDatum->cover_links = $coverLinkData;
            } // END if else

            $categoryDatum->s3_url = env('AWS_BUCKET_URL');
            $categoryDatum->url_path = empty($categoryDatum->slug) ? '' : '/' . $categoryDatum->slug . '-c' . $categoryDatum->id;

            array_push($finalData, $categoryDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'categories' => $finalData];
        $resultData = ['categories' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * make2Html
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function make2Html(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum = $this->categoryServ->findById($id);

        if ($categoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum->first()->site_name = 'English.Agency';
        $categoryDatum->first()->url_path = '/' . $categoryDatum->first()->slug . '-c' . $categoryDatum->first()->id;

        $categoryDatum->first()->cover_links = [];
        if (!empty($categoryDatum->first()->cover)) {
            $coverLinkData = $this->fileServ->findLinks($categoryDatum->first()->cover);
            $categoryDatum->first()->cover_links = $coverLinkData;
        } // END if else

        $categoryDatum->first()->recommend_posts = [];
        $postData = $this->postServ->findByCategoryIdAndRecommended($categoryDatum->first()->id, 'publish', ['p.id' => 'DESC']);
        if ($postData->isNotEmpty()) {
            foreach($postData->all() as $postDatum) {

                $postDatum->cover_links = [];
                if (!empty($postDatum->cover)) {
                    $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                    $postDatum->cover_links = $coverLinkData;
                } // END if else

                $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

                $categoryDatum->first()->recommend_posts[] = get_object_vars($postDatum);
            }
        } // END if


        $tHtmlPath = 'category/' . $categoryDatum->first()->status . '/';
        $htmlFile  = $categoryDatum->first()->id . '.html';

        $tExists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);
        if (!empty($tExists)) {
            Storage::disk('public')->delete($tHtmlPath . $htmlFile);
        } // END if


        $inputData = get_object_vars($categoryDatum->first());
        $content = view('category', $inputData)->__toString();

        Storage::disk('public')->put($tHtmlPath . $htmlFile, $content);


        $exists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);

        if (empty($exists)) {
            $code = 404;
            $comment = 'html file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $this->categoryServ->update(['html_made' => true], ['id' => $id]);


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('make success', $resultData, 202);
    } // END function


    /**
     * syncHtml
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function syncHtml(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum = $this->categoryServ->findById($id);

        if ($categoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $oHtmlPath = 'public/category/' . $categoryDatum->first()->status . '/';
        $tHtmlPath = 'sync/category/';
        $htmlFile = $categoryDatum->first()->id . '.html';


        if ($categoryDatum->first()->status == config('tbl_categories.CATEGORIES_STATUS_ENABLE')) {

            if (empty($categoryDatum->first()->html_made)) {
                $code = 422;
                $comment = 'html_made error';

                Jsonponse::fail($comment, $code);
            } // END if

            $oExists = Storage::disk('local')->exists($oHtmlPath . $htmlFile);
            if (!empty($oExists)) {
                $tExists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);
                if (!empty($tExists)) {
                    Storage::disk('local')->delete($tHtmlPath . $htmlFile);
                } // END if

                Storage::disk('local')->copy($oHtmlPath . $htmlFile, $tHtmlPath . $htmlFile);
            } // END if
        } // END if

        if ($categoryDatum->first()->status != config('tbl_categories.CATEGORIES_STATUS_ENABLE')) {

            $exists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);

            if (!empty($exists)) {
                Storage::disk('local')->delete($tHtmlPath . $htmlFile);
            } // END if
        } // END if


        $this->categoryServ->update(['synced' => true], ['id' => $id]);


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('sync success', $resultData, 202);
    } // END function


} // END class
