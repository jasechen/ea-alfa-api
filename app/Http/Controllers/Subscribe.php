<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\SubscribeServ;


class Subscribe extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ   = app(SessionServ::class);
        $this->subscribeServ = app(SubscribeServ::class);
    } // END function


    /**
     * create
     *
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function create(Request $request)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        $name = $request->input('name');

        if (empty($name)) {
            $code = 400;
            $comment = 'name empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $email = $request->input('email');

        if (empty($email)) {
            $code = 400;
            $comment = 'email empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $code = 422;
            $comment = 'email format error';

            Jsonponse::fail($comment, $code);
        } // END if


        $subscriberDatum = $this->subscribeServ->findByEmail($email);

        if ($subscriberDatum->isNotEmpty()) {
            if ($subscriberDatum->first()->status == config('tbl_subscribers.SUBSCRIBERS_STATUS_UNSUBSCRIBE')) {
                $this->subscribeServ->switchStatusByEmail($email);
            } else {
                $code = 409;
                $comment = 'email already exist';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        $subscriberDatum = $this->subscribeServ->create($name, $email);

        if ($subscriberDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        // $resultData = ['session' => $sessionCode, 'subscriber_id' => $subscriberDatum->first()->id];
        $resultData = ['subscriber_id' => $subscriberDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * findBySubscribe
     *
     * @method GET
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function findByStatusSubscribe(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $status = config('tbl_subscribers.SUBSCRIBERS_STATUS_SUBSCRIBE');

        $subscribeData = $this->subscribeServ->findByStatus($status);

        if ($subscribeData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $resultData = ['session' => $sessionCode, 'subscribers' => $subscribeData->all()];

        Jsonponse::success('find success', $resultData);
    } // END function

} // END class
