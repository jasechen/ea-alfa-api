<?php
$fields = [
    'FILES_PARENT_TYPE_POST' => 'post',
    'FILES_PARENT_TYPE_AUTHOR' => 'author',
    'FILES_PARENT_TYPE_CATEGORY' => 'category',

    'FILES_TYPE_IMAGE' => 'image',
    'FILES_TYPE_VIDEO' => 'video',
    'FILES_TYPE_AUDIO' => 'audio',
    'FILES_TYPE_OTHERS' => 'others',

    'FILES_STATUS_ENABLE'  => 'enable',
    'FILES_STATUS_DISABLE' => 'disable',
    'FILES_STATUS_DELETE'  => 'delete',
];

$fields['DEFAULT_FILES_PARENT_TYPE'] = $fields['FILES_PARENT_TYPE_POST'];
$fields['FILES_PARENT_TYPES'] = [
    $fields['FILES_PARENT_TYPE_POST'],
    $fields['FILES_PARENT_TYPE_AUTHOR'],
    $fields['FILES_PARENT_TYPE_CATEGORY']
];

$fields['DEFAULT_FILES_TYPE'] = $fields['FILES_TYPE_IMAGE'];
$fields['FILES_TYPES'] = [
    $fields['FILES_TYPE_IMAGE'],
    $fields['FILES_TYPE_VIDEO'],
    $fields['FILES_TYPE_AUDIO'],
    $fields['FILES_TYPE_OTHERS']
];

$fields['DEFAULT_FILES_STATUS'] = $fields['FILES_STATUS_ENABLE'];
$fields['FILES_STATUS'] = [
    $fields['FILES_STATUS_ENABLE'],
    $fields['FILES_STATUS_DISABLE'],
    $fields['FILES_STATUS_DELETE']
];

return $fields;