<?php

namespace App\Docs;

    /**
     * 查詢 單篇文章
     *
     * @api {GET} /post/{id} 04. 查詢 單篇文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 單篇文章
     * @apiName GetPostFind
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          文章 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.post   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post": {
            "id": "38771511879929856",
            "title": "標題1",
            "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
            "cover": "1517067835270712",
            "author_id": "1",
            "status": "draft",
            "category_id": "1",
            "recommended": "1",
            "html_made": "0",
            "synced": "0",
            "updated_at": "2018-01-27 23:44:08",
            "created_at": "2018-01-27 23:44:08",
            "slug": "jaskdjfhskjdhfkasdhf",
            "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
            "canonical_url": "",
            "og_title": "jaskdjfhskjdhfkasdhf",
            "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
            "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
            "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
            "cover_title": "qqqq",
            "cover_alt": "qq",
            "category": {
                "id": "1",
                "name": "分類1",
                "description": "",
                "cover": "",
                "num_posts": "0",
                "updated_at": "2018-01-27 16:06:07",
                "created_at": "2018-01-27 16:06:07",
                "slug": "cat1",
                "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                "og_title": "cat1cat1",
                "og_description": "cat1cat1cat1",
                "meta_title": "cat1cat1cat1",
                "meta_description": "cat1cat1cat1cat1",
                "cover_title": "cat1",
                "cover_alt": "cat1"
            },
            "author": {
                "id": "1",
                "fullname": "作者1",
                "avatar": "3333",
                "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                "updated_at": "2018-01-27 16:05:20",
                "created_at": "2018-01-27 16:05:20"
            },
            "cover_links": {
                "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
            },
            "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


// ===
    /**
     * 查詢 所有已同步文章
     *
     * @api {GET} /post/list/{order_way?}/{page?} 05. 查詢 所有已同步文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有已同步文章
     * @apiName GetPostFindAll
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [
            {
                "id": "38771511879929856",
                "title": "標題1",
                "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
                "cover": "1517067835270712",
                "author_id": "1",
                "status": "draft",
                "category_id": "1",
                "recommended": "1",
                "html_made": "0",
                "synced": "0",
                "updated_at": "2018-01-27 23:44:08",
                "created_at": "2018-01-27 23:44:08",
                "slug": "jaskdjfhskjdhfkasdhf",
                "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
                "canonical_url": "",
                "og_title": "jaskdjfhskjdhfkasdhf",
                "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "cover_title": "qqqq",
                "cover_alt": "qq",
                "category": {
                    "id": "1",
                    "name": "分類1",
                    "description": "",
                    "cover": "",
                    "num_posts": "0",
                    "updated_at": "2018-01-27 16:06:07",
                    "created_at": "2018-01-27 16:06:07",
                    "slug": "cat1",
                    "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                    "og_title": "cat1cat1",
                    "og_description": "cat1cat1cat1",
                    "meta_title": "cat1cat1cat1",
                    "meta_description": "cat1cat1cat1cat1",
                    "cover_title": "cat1",
                    "cover_alt": "cat1"
                },
                "author": {
                    "id": "1",
                    "fullname": "作者1",
                    "avatar": "3333",
                    "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                    "updated_at": "2018-01-27 16:05:20",
                    "created_at": "2018-01-27 16:05:20"
                },
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 412.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 412.02
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */


// ===
     /**
      * 用 slug 查詢 單篇文章
      *
      * @api {GET} /post/slug/{slug} 06. 用 slug 查詢 單篇文章
      * @apiVersion 0.1.0
      * @apiDescription ・ 用 slug 查詢 單篇文章
      * @apiName GetPostFindBySlug
      * @apiGroup Post
      *
      * @apiHeader {string}   session                             Session 代碼
      *
      * @apiHeaderExample {json} Header
 {
     "session": "8f3e973061800dc6ebcb367079b305d8"
 }
      *
      * @apiParam {string}                     slug          文章 slug
      *
      * @apiParamExample {json} Request
 {
     "slug" : "test1"
 }
      *
      * @apiSuccess (Success) {string}   status              回傳狀態
      * @apiSuccess (Success) {int}      code                回傳代碼
      * @apiSuccess (Success) {string}   comment             回傳訊息
      * @apiSuccess (Success) {object}   data                回傳資訊
      * @apiSuccess (Success) {string}   data.session        Session 代碼
      * @apiSuccess (Success) {string}   data.post   文章
      *
      * @apiSuccessExample {json}    Response: 200
 {
     "status": "success",
     "code": 200,
     "comment": "fetch success",
     "data": {
         "session": "8f3e973061800dc6ebcb367079b305d8",
         "post": {
            "id": "38771511879929856",
            "title": "標題1",
            "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
            "cover": "1517067835270712",
            "author_id": "1",
            "status": "draft",
            "category_id": "1",
            "recommended": "1",
            "html_made": "0",
            "synced": "0",
            "updated_at": "2018-01-27 23:44:08",
            "created_at": "2018-01-27 23:44:08",
            "slug": "jaskdjfhskjdhfkasdhf",
            "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
            "canonical_url": "",
            "og_title": "jaskdjfhskjdhfkasdhf",
            "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
            "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
            "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
            "cover_title": "qqqq",
            "cover_alt": "qq",
            "category": {
                "id": "1",
                "name": "分類1",
                "description": "",
                "cover": "",
                "num_posts": "0",
                "updated_at": "2018-01-27 16:06:07",
                "created_at": "2018-01-27 16:06:07",
                "slug": "cat1",
                "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                "og_title": "cat1cat1",
                "og_description": "cat1cat1cat1",
                "meta_title": "cat1cat1cat1",
                "meta_description": "cat1cat1cat1cat1",
                "cover_title": "cat1",
                "cover_alt": "cat1"
            },
            "author": {
                "id": "1",
                "fullname": "作者1",
                "avatar": "3333",
                "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                "updated_at": "2018-01-27 16:05:20",
                "created_at": "2018-01-27 16:05:20"
            },
            "cover_links": {
                "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
            },
            "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
        }
     }
 }
      *
      * @apiSuccessExample {json}    Response: 204
 {}
      *
      * @apiError (Error) {string}   status      回傳狀態
      * @apiError (Error) {int}      code        回傳代碼
      * @apiError (Error) {string}   comment     回傳訊息
      *
      *
      * @apiErrorExample {json}  Response: 400.01
 {
     "status": "fail",
     "code": 400,
     "comment": "session empty"
 }
      *
      * @apiErrorExample {json}  Response: 400.02
 {
     "status": "fail",
     "code": 400,
     "comment": "slug empty"
 }
      *
      * @apiErrorExample {json}  Response: 410.01
 {
     "status": "fail",
     "code": 410,
     "comment": "session is NOT alive"
 }
      *
      */



// ===
    /**
     * 用作者 ID 查詢 publish 已同步文章
     *
     * @api {GET} /post/author/{id}/{order_way?}/{page?} 07. 用作者 ID 查詢已同步文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 用作者 ID 查詢 publish 已同步文章
     * @apiName GetPostFindByAuthorId
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          作者 ID
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "id" : "1",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [
            {
                "id": "38771511879929856",
                "title": "標題1",
                "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
                "cover": "1517067835270712",
                "author_id": "1",
                "status": "draft",
                "category_id": "1",
                "recommended": "1",
                "html_made": "0",
                "synced": "0",
                "updated_at": "2018-01-27 23:44:08",
                "created_at": "2018-01-27 23:44:08",
                "slug": "jaskdjfhskjdhfkasdhf",
                "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
                "canonical_url": "",
                "og_title": "jaskdjfhskjdhfkasdhf",
                "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "cover_title": "qqqq",
                "cover_alt": "qq",
                "category": {
                    "id": "1",
                    "name": "分類1",
                    "description": "",
                    "cover": "",
                    "num_posts": "0",
                    "updated_at": "2018-01-27 16:06:07",
                    "created_at": "2018-01-27 16:06:07",
                    "slug": "cat1",
                    "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                    "og_title": "cat1cat1",
                    "og_description": "cat1cat1cat1",
                    "meta_title": "cat1cat1cat1",
                    "meta_description": "cat1cat1cat1cat1",
                    "cover_title": "cat1",
                    "cover_alt": "cat1"
                },
                "author": {
                    "id": "1",
                    "fullname": "作者1",
                    "avatar": "3333",
                    "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                    "updated_at": "2018-01-27 16:05:20",
                    "created_at": "2018-01-27 16:05:20"
                },
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

// ===
    /**
     * 用分類 ID 查詢 publish 已同步文章
     *
     * @api {GET} /post/category/{id}/{order_way?}/{page?} 08. 用分類 ID 查詢已同步文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 用分類 ID 查詢 publish 已同步文章
     * @apiName GetPostFindByCategoryId
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          分類 ID
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "id" : "1",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
       "session": "8f3e973061800dc6ebcb367079b305d8",
       "posts": [
            {
                "id": "38771511879929856",
                "title": "標題1",
                "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
                "cover": "1517067835270712",
                "author_id": "1",
                "status": "draft",
                "category_id": "1",
                "recommended": "1",
                "html_made": "0",
                "synced": "0",
                "updated_at": "2018-01-27 23:44:08",
                "created_at": "2018-01-27 23:44:08",
                "slug": "jaskdjfhskjdhfkasdhf",
                "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
                "canonical_url": "",
                "og_title": "jaskdjfhskjdhfkasdhf",
                "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "cover_title": "qqqq",
                "cover_alt": "qq",
                "category": {
                    "id": "1",
                    "name": "分類1",
                    "description": "",
                    "cover": "",
                    "num_posts": "0",
                    "updated_at": "2018-01-27 16:06:07",
                    "created_at": "2018-01-27 16:06:07",
                    "slug": "cat1",
                    "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                    "og_title": "cat1cat1",
                    "og_description": "cat1cat1cat1",
                    "meta_title": "cat1cat1cat1",
                    "meta_description": "cat1cat1cat1cat1",
                    "cover_title": "cat1",
                    "cover_alt": "cat1"
                },
                "author": {
                    "id": "1",
                    "fullname": "作者1",
                    "avatar": "3333",
                    "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                    "updated_at": "2018-01-27 16:05:20",
                    "created_at": "2018-01-27 16:05:20"
                },
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */


    /**
     * 用作者 ID 及分類 ID 查詢 publish 已同步文章
     *
     * @api {GET} /post/author/category/{author_id}/{category_id}/{order_way?}/{page?} 09. 用作者 ID 及分類 ID 查詢已同步文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 用作者 ID 及分類 ID 查詢 publish 已同步文章
     * @apiName GetPostFindByAuthorIdAndCategoryId
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     author_id          作者 ID
     * @apiParam {string}                     category_id          分類 ID
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "author_id" : "1",
    "category_id" : "1",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
       "session": "8f3e973061800dc6ebcb367079b305d8",
       "posts": [
            {
                "id": "38771511879929856",
                "title": "標題1",
                "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
                "cover": "1517067835270712",
                "author_id": "1",
                "status": "draft",
                "category_id": "1",
                "recommended": "1",
                "html_made": "0",
                "synced": "0",
                "updated_at": "2018-01-27 23:44:08",
                "created_at": "2018-01-27 23:44:08",
                "slug": "jaskdjfhskjdhfkasdhf",
                "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
                "canonical_url": "",
                "og_title": "jaskdjfhskjdhfkasdhf",
                "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "cover_title": "qqqq",
                "cover_alt": "qq",
                "category": {
                    "id": "1",
                    "name": "分類1",
                    "description": "",
                    "cover": "",
                    "num_posts": "0",
                    "updated_at": "2018-01-27 16:06:07",
                    "created_at": "2018-01-27 16:06:07",
                    "slug": "cat1",
                    "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                    "og_title": "cat1cat1",
                    "og_description": "cat1cat1cat1",
                    "meta_title": "cat1cat1cat1",
                    "meta_description": "cat1cat1cat1cat1",
                    "cover_title": "cat1",
                    "cover_alt": "cat1"
                },
                "author": {
                    "id": "1",
                    "fullname": "作者1",
                    "avatar": "3333",
                    "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                    "updated_at": "2018-01-27 16:05:20",
                    "created_at": "2018-01-27 16:05:20"
                },
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "author_id empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "category_id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */


// ===
    /**
     * 用分類 ID 查詢 publish 的推薦已同步文章
     *
     * @api {GET} /post/recommend/category/{id}/{order_way?}/{page?} 12. 用分類 ID 查詢推薦已同步文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 用分類 ID 查詢 publish 的推薦已同步文章
     * @apiName GetPostFindByCategoryIdAndRecommended
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          分類 ID
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "id" : "1",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
       "session": "8f3e973061800dc6ebcb367079b305d8",
       "posts": [
            {
                "id": "38771511879929856",
                "title": "標題1",
                "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
                "cover": "1517067835270712",
                "author_id": "1",
                "status": "draft",
                "recommended": "1",
                "category_id": "1",
                "html_made": "0",
                "synced": "0",
                "updated_at": "2018-01-27 23:44:08",
                "created_at": "2018-01-27 23:44:08",
                "slug": "jaskdjfhskjdhfkasdhf",
                "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
                "canonical_url": "",
                "og_title": "jaskdjfhskjdhfkasdhf",
                "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "cover_title": "qqqq",
                "cover_alt": "qq",
                "category": {
                    "id": "1",
                    "name": "分類1",
                    "description": "",
                    "cover": "",
                    "num_posts": "0",
                    "updated_at": "2018-01-27 16:06:07",
                    "created_at": "2018-01-27 16:06:07",
                    "slug": "cat1",
                    "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                    "og_title": "cat1cat1",
                    "og_description": "cat1cat1cat1",
                    "meta_title": "cat1cat1cat1",
                    "meta_description": "cat1cat1cat1cat1",
                    "cover_title": "cat1",
                    "cover_alt": "cat1"
                },
                "author": {
                    "id": "1",
                    "fullname": "作者1",
                    "avatar": "3333",
                    "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                    "updated_at": "2018-01-27 16:05:20",
                    "created_at": "2018-01-27 16:05:20"
                },
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */


// ===
    /**
     * 用分類 slug 查詢 publish 已同步文章
     *
     * @api {GET} /post/category/slug/{slug}/{order_way?}/{page?} 13. 用分類 slug 查詢已同步文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 用分類 slug 查詢 publish 已同步文章
     * @apiName GetPostFindByCategorySlug
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     slug          分類 slug
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "slug" : "cat1",
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
       "session": "8f3e973061800dc6ebcb367079b305d8",
       "posts": [
            {
                "id": "38771511879929856",
                "title": "標題1",
                "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
                "cover": "1517067835270712",
                "author_id": "1",
                "status": "draft",
                "category_id": "1",
                "recommended": "1",
                "html_made": "0",
                "synced": "0",
                "updated_at": "2018-01-27 23:44:08",
                "created_at": "2018-01-27 23:44:08",
                "slug": "jaskdjfhskjdhfkasdhf",
                "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
                "canonical_url": "",
                "og_title": "jaskdjfhskjdhfkasdhf",
                "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "cover_title": "qqqq",
                "cover_alt": "qq",
                "category": {
                    "id": "1",
                    "name": "分類1",
                    "description": "",
                    "cover": "",
                    "num_posts": "0",
                    "updated_at": "2018-01-27 16:06:07",
                    "created_at": "2018-01-27 16:06:07",
                    "slug": "cat1",
                    "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                    "og_title": "cat1cat1",
                    "og_description": "cat1cat1cat1",
                    "meta_title": "cat1cat1cat1",
                    "meta_description": "cat1cat1cat1cat1",
                    "cover_title": "cat1",
                    "cover_alt": "cat1"
                },
                "author": {
                    "id": "1",
                    "fullname": "作者1",
                    "avatar": "3333",
                    "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                    "updated_at": "2018-01-27 16:05:20",
                    "created_at": "2018-01-27 16:05:20"
                },
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category seo error"
}
     *
     */


// ===
    /**
     * 查詢 所有文章
     *
     * @api {GET} /post/nimda/list/{order_way?}/{page?} 14. 查詢 所有文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有文章
     * @apiName GetPostFindAllNimda
     * @apiGroup Post
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts   文章
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [
            {
                "id": "38771511879929856",
                "title": "標題1",
                "content": "標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1標題1",
                "cover": "1517067835270712",
                "author_id": "1",
                "status": "draft",
                "category_id": "1",
                "recommended": "1",
                "html_made": "0",
                "synced": "0",
                "updated_at": "2018-01-27 23:44:08",
                "created_at": "2018-01-27 23:44:08",
                "slug": "jaskdjfhskjdhfkasdhf",
                "excerpt": "asdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadfasdfasdfsadf\r\n\r\n\r\nasdfasdfsadfasdfasdfsadf\r\n\r\nasdfasdfsadfasdfasdfsadf",
                "canonical_url": "",
                "og_title": "jaskdjfhskjdhfkasdhf",
                "og_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf\r\n\r\njaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_title": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "meta_description": "jaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhfjaskdjfhskjdhfkasdhf",
                "cover_title": "qqqq",
                "cover_alt": "qq",
                "category": {
                    "id": "1",
                    "name": "分類1",
                    "description": "",
                    "cover": "",
                    "num_posts": "0",
                    "updated_at": "2018-01-27 16:06:07",
                    "created_at": "2018-01-27 16:06:07",
                    "slug": "cat1",
                    "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                    "og_title": "cat1cat1",
                    "og_description": "cat1cat1cat1",
                    "meta_title": "cat1cat1cat1",
                    "meta_description": "cat1cat1cat1cat1",
                    "cover_title": "cat1",
                    "cover_alt": "cat1"
                },
                "author": {
                    "id": "1",
                    "fullname": "作者1",
                    "avatar": "3333",
                    "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
                    "updated_at": "2018-01-27 16:05:20",
                    "created_at": "2018-01-27 16:05:20"
                },
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/jaskdjfhskjdhfkasdhf-p38771511879929856"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 412.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 412.02
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

