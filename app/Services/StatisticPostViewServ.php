<?php

namespace App\Services;

use App\Repositories\StatisticPostViewRepo;


/**
 * Class StatisticPostViewServ
 *
 * @package namespace App\Services;
 */
class StatisticPostViewServ
{


    public function __construct()
    {

        $this->statisticPostViewServRepo = new StatisticPostViewRepo();
    } // END function


    /*
     * findByPostId
     *
     * @param $postId
     *
     * @return
     */
    public function findByPostId($postId)
    {
        $where = ['post_id' => $postId];

        return $this->statisticPostViewServRepo->fetchDatum($where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->statisticPostViewServRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->statisticPostViewServRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $description
     *
     * @return
     */
    public function create($postId, $numTotal)
    {
        $data = ['post_id' => $postId,
                 'num_total' => $numTotal
        ];

        return $this->statisticPostViewServRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->statisticPostViewServRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->statisticPostViewServRepo->fetchDatum($where);
    } // END function

}
