<?php

namespace App\Docs;

    /**
     * 用 ID 查詢 作者
     *
     * @api {GET} /author/{id} 01. 用 ID 查詢 作者
     * @apiVersion 0.1.0
     * @apiDescription ・ 用 ID 查詢 所有作者。
     * @apiName GetAuthorFind
     * @apiGroup Author
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {integer}                     id          作者 ID
     *
     * @apiParamExample {json} Request
{
    "id" : 1
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {object}   data.author   作者
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "author": {
            "id": "1",
            "fullname": "作者1",
            "avatar": "3333",
            "bio": "作者1作者1作者1作者1\r\n作者1作者1\r\n作者1作者1作者1",
            "updated_at": "2018-01-27 16:05:20",
            "created_at": "2018-01-27 16:05:20"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


// ===
    /**
     * 查詢 所有作者
     *
     * @api {GET} /author/list/{order_way?}/{page?} 02. 查詢 所有作者
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有作者。
     * @apiName GetAuthorFindAll
     * @apiGroup Author
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {integer}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.authors   作者
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "authors": [
            {
                "id": "1",
                "fullname": "任我行",
                "avatar": "1232342343",
                "bio": "jsaldncjalsdncjdncd\r\najksdlfaskdjfdsl\r\nnksldnflsdc",
                "updated_at": "2018-01-24 15:16:43",
                "created_at": "2018-01-24 15:16:43"
            },
            {
                "id": "2",
                "fullname": "令狐沖",
                "avatar": "99999999",
                "bio": "jsaldncjalsdncjdncd\r\najksdlfaskdjfdsl\r\nnksldnflsdc",
                "updated_at": "2018-01-24 15:16:43",
                "created_at": "2018-01-24 15:16:43"
            },
            ...
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */

