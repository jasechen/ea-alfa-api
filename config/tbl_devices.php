<?php
$fields = [
    'DEVICES_OS_IOS'     => 'ios',
    'DEVICES_OS_ANDRIOD' => 'andriod',
    'DEVICES_OS_MACOS'   => 'macos',
    'DEVICES_OS_WINDOWS' => 'windows',
    'DEVICES_OS_OTHERS'  => 'others',

    'DEVICES_TYPE_IPHONE'   => 'iphone',
    'DEVICES_TYPE_IPAD'     => 'ipad',
    'DEVICES_TYPE_MOBILE'   => 'mobile',
    'DEVICES_TYPE_PC'       => 'pc',
    'DEVICES_TYPE_OTHERS'   => 'others',

    'DEVICES_LANG_TW' => 'zh-tw',
    'DEVICES_LANG_CN' => 'zh-cn',
    'DEVICES_LANG_EN' => 'en',
    'DEVICES_LANG_JP' => 'jp',

    'DEVICES_CHANNEL_APNS'   => 'apns',
    'DEVICES_CHANNEL_GCM'    => 'gcm',
    'DEVICES_CHANNEL_BAIDU'  => 'baidu',
    'DEVICES_CHANNEL_OTHERS' => 'others',
];

$fields['DEFAULT_DEVICES_OS'] = $fields['DEVICES_OS_OTHERS'];
$fields['DEVICES_OS'] = [
    $fields['DEVICES_OS_IOS'],
    $fields['DEVICES_OS_ANDRIOD'],
    $fields['DEVICES_OS_MACOS'],
    $fields['DEVICES_OS_WINDOWS'],
    $fields['DEVICES_OS_OTHERS']
];

$fields['DEFAULT_DEVICES_TYPE'] = $fields['DEVICES_TYPE_OTHERS'];
$fields['DEVICES_TYPES'] = [
    $fields['DEVICES_TYPE_IPHONE'],
    $fields['DEVICES_TYPE_IPAD'],
    $fields['DEVICES_TYPE_MOBILE'],
    $fields['DEVICES_TYPE_PC'],
    $fields['DEVICES_TYPE_OTHERS']
];

$fields['DEFAULT_DEVICES_LANG'] = $fields['DEVICES_LANG_EN'];
$fields['DEVICES_LANGS'] = [
    $fields['DEVICES_LANG_TW'],
    $fields['DEVICES_LANG_CN'],
    $fields['DEVICES_LANG_EN'],
    $fields['DEVICES_LANG_JP']
];

$fields['DEFAULT_DEVICES_CHANNEL'] = $fields['DEVICES_CHANNEL_OTHERS'];
$fields['DEVICES_CHANNELS'] = [
    $fields['DEVICES_CHANNEL_APNS'],
    $fields['DEVICES_CHANNEL_GCM'],
    $fields['DEVICES_CHANNEL_BAIDU'],
    $fields['DEVICES_CHANNEL_OTHERS']
];

return $fields;
