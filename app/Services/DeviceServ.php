<?php

namespace App\Services;

use Webpatser\Uuid\Uuid;

use App\Repositories\DeviceRepo;


/**
 * Class SessionServ
 *
 * @package namespace App\Services;
 */
class DeviceServ
{


    public function __construct()
    {

        $this->deviceRepo = new DeviceRepo();
    } // END function


    /**
     *
     */
    public function initCode()
    {
        do {

            $code = Uuid::generate(4)->string;
            $deviceDatum = $this->findByCode($code);

        } while ($deviceDatum->isNotEmpty());

        return $code;
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->deviceRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->deviceRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $email
     *
     * @return
     */
    public function create($code, $os = 'macos', $type = 'pc', $lang = 'en', $channel = 'others', $token = '')
    {
        $data = ['code'  => $code,
                 'os'    => $os,
                 'type'  => $type,
                 'lang'  => $lang,
                 'token' => $token,
                 'channel' => $channel
        ];

        return $this->deviceRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->deviceRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByChannel
     *
     * @param $channel
     *
     * @return
     */
    public function findByChannel($channel, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['channel' => $channel];

        return $this->deviceRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByLang
     *
     * @param $lang
     *
     * @return
     */
    public function findByLang($lang, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['lang' => $lang];

        return $this->deviceRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByType
     *
     * @param $type
     *
     * @return
     */
    public function findByType($type, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['type' => $type];

        return $this->deviceRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByOS
     *
     * @param $os
     *
     * @return
     */
    public function findByOS($os, $orderby = [], $page = -1, $numItems = 20)
    {
        $where = ['os' => $os];

        return $this->deviceRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByToken
     *
     * @param $token
     *
     * @return
     */
    public function findByToken($token)
    {
        $where = ['token' => $token];

        return $this->deviceRepo->fetchDatum($where);
    } // END function


    /*
     * findByCode
     *
     * @param $code
     *
     * @return
     */
    public function findByCode($code)
    {
        $where = ['code' => $code];

        return $this->deviceRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->deviceRepo->fetchDatum($where);
    } // END function

}
