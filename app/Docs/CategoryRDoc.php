<?php

namespace App\Docs;

    /**
     * 查詢 分類
     *
     * @api {GET} /category/{id} 04. 查詢 分類
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 分類
     * @apiName GetCategoryFind
     * @apiGroup Category
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                     id          分類 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category   分類
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category": {
            "id": "1",
            "name": "分類1",
            "description": "",
            "cover": "",
            "status": "enable",
            "html_made": "0",
            "synced": "0",
            "num_posts": "0",
            "updated_at": "2018-01-27 16:06:07",
            "created_at": "2018-01-27 16:06:07",
            "slug": "cat1",
            "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
            "og_title": "cat1cat1",
            "og_description": "cat1cat1cat1",
            "meta_title": "cat1cat1cat1",
            "meta_description": "cat1cat1cat1cat1",
            "cover_title": "cat1",
            "cover_alt": "cat1",
            "cover_links": [],
            "url_path": "/cat1-c1"
        }
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */


    /**
     * 查詢 所有分類
     *
     * @api {GET} /category/list/{order_way?}/{page?} 05. 查詢 所有分類
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有分類。
     * @apiName GetCategoryFindAll
     * @apiGroup Category
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=ASC,DESC}                     [order_way="ASC"]          排列升降冪
     * @apiParam {integer}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "order_way" : "ASC",
    "page" : "-1"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.categories   分類
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "categories": [
            {
                "id": "1",
                "name": "分類1",
                "description": "",
                "cover": "",
                "status": "enable",
                "html_made": "0",
                "synced": "0",
                "num_posts": "0",
                "updated_at": "2018-01-27 16:06:07",
                "created_at": "2018-01-27 16:06:07",
                "slug": "cat1",
                "excerpt": "cat1cat1cat1\r\ncat1cat1\r\n\r\ncat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1\r\n\r\n\r\n\r\ncat1cat1\r\ncat1cat1",
                "og_title": "cat1cat1",
                "og_description": "cat1cat1cat1",
                "meta_title": "cat1cat1cat1",
                "meta_description": "cat1cat1cat1cat1",
                "cover_title": "cat1",
                "cover_alt": "cat1",
                "cover_links": [],
                "url_path": "/cat1-c1"
            },
            {
                "id": "2",
                "name": "分類2",
                "description": "",
                "cover": "",
                "status": "enable",
                "html_made": "0",
                "synced": "0",
                "num_posts": "0",
                "updated_at": "2018-01-27 16:06:18",
                "created_at": "2018-01-27 16:06:18",
                "slug": "",
                "excerpt": "",
                "og_title": "",
                "og_description": "",
                "meta_title": "",
                "meta_description": "",
                "cover_title": "",
                "cover_alt": "",
                "cover_links": [],
                "url_path": ""
            },
            {
                "id": "40921940755615744",
                "name": "分類三三",
                "description": "分類三分類三分類三分類三分類三分類三",
                "cover": "1517067835270712",
                "status": "enable",
                "html_made": "0",
                "synced": "0",
                "num_posts": "0",
                "updated_at": "2018-02-03 00:18:23",
                "created_at": "2018-02-02 22:09:10",
                "slug": "cate3",
                "excerpt": "分類三三",
                "og_title": "分類三",
                "og_description": "分類三分類三分類三分類三分類三分類三",
                "meta_title": "分類三",
                "meta_description": "分類三分類三分類三分類三分類三分類三",
                "cover_title": "分類三",
                "cover_alt": "分類三",
                "cover_links": {
                    "c": "gallery/category/40921940755615744/1517067835270712_c.jpeg",
                    "z": "gallery/category/40921940755615744/1517067835270712_z.jpeg",
                    "n": "gallery/category/40921940755615744/1517067835270712_n.jpeg",
                    "q": "gallery/category/40921940755615744/1517067835270712_q.jpeg",
                    "sq": "gallery/category/40921940755615744/1517067835270712_sq.jpeg",
                    "o": "gallery/category/40921940755615744/1517067835270712_o.jpeg"
                },
                "url_path": "/cate3-c40921940755615744"
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "order_way error"
}
     *
     */
