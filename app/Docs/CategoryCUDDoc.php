<?php

namespace App\Docs;

    /**
     * 新增分類
     *
     * @api {POST} /category 01. 新增分類
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增分類
     * @apiName PostCategoryCreate
     * @apiGroup Category
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           name                名稱
     * @apiParam {string}                                           description          描述
     * @apiParam {string}                                           cover                封面
     * @apiParam {string=enable,delete,disable}                     [status="enable"]     狀態
     * @apiParam {string}                                           slug                 位址
     * @apiParam {string}                                           excerpt              摘要
     * @apiParam {string}                                           og_title             og 標題
     * @apiParam {string}                                           og_description       og 描述
     * @apiParam {string}                                           meta_title           meta 標題
     * @apiParam {string}                                           meta_description     meta 描述
     * @apiParam {string}                                           cover_title          封面標題
     * @apiParam {string}                                           cover_alt            封面 alt
     *
     * @apiParamExample {json} Request
{
    "name" : "標題一",
    "description" : "test1test1test1test1test1test1test1\n\r test1test1test1test1test1",
    "cover" : "7876923123987",
    "status" : "enable",
    "slug" : "test1",
    "excerpt" : "test1test1test1test1test1",
    "og_title" : "標題一",
    "og_description" : "test1test1test1test1test1",
    "meta_title" : "標題一",
    "meta_description" : "test1test1test1test1test1",
    "cover_title" : "標題一",
    "cover_alt" : "標題一",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category_id    分類 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "name empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "description empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "cover empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "slug empty"
}
     *
     * @apiErrorExample {json}  Response: 400.06
{
    "status": "fail",
    "code": 400,
    "comment": "excerpt empty"
}
     *
     * @apiErrorExample {json}  Response: 400.07
{
    "status": "fail",
    "code": 400,
    "comment": "og_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.08
{
    "status": "fail",
    "code": 400,
    "comment": "og_decription empty"
}
     *
     * @apiErrorExample {json}  Response: 400.09
{
    "status": "fail",
    "code": 400,
    "comment": "meta_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.10
{
    "status": "fail",
    "code": 400,
    "comment": "meta_description empty"
}
     *
     * @apiErrorExample {json}  Response: 400.11
{
    "status": "fail",
    "code": 400,
    "comment": "cover_title empty"
}
     *
     * @apiErrorExample {json}  Response: 400.12
{
    "status": "fail",
    "code": 400,
    "comment": "cover_alt empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */


// ===
    /**
     * 修改分類
     *
     * @api {PUT} /category/{id} 02. 修改分類
     * @apiVersion 0.1.0
     * @apiDescription ・ 修改分類
     * @apiName PutCategoryUpdate
     * @apiGroup Category
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id                     分類 ID
     * @apiParam {string}                                           [name]                名稱
     * @apiParam {string}                                           [description]          描述
     * @apiParam {string}                                           [cover]                封面
     * @apiParam {string=enable,delete,disable}                     [status]               狀態
     * @apiParam {string}                                           [slug]                 位址
     * @apiParam {string}                                           [excerpt]              摘要
     * @apiParam {string}                                           [og_title]             og 標題
     * @apiParam {string}                                           [og_description]       og 描述
     * @apiParam {string}                                           [meta_title]           meta 標題
     * @apiParam {string}                                           [meta_description]     meta 描述
     * @apiParam {string}                                           [cover_title]          封面標題
     * @apiParam {string}                                           [cover_alt]            封面 alt
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
    "name" : "標題一",
    "description" : "test1test1test1test1test1test1test1\n\r test1test1test1test1test1",
    "cover" : "7876923123987",
    "status" : "enable",
    "slug" : "test1",
    "excerpt" : "test1test1test1test1test1",
    "og_title" : "標題一",
    "og_description" : "test1test1test1test1test1",
    "meta_title" : "標題一",
    "meta_description" : "test1test1test1test1test1",
    "cover_title" : "標題一",
    "cover_alt" : "標題一",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category_id        分類 ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "cover error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "slug error"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "updateData  / updateSeoData empty"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "update error"
}
     *
     * @apiErrorExample {json}  Response: 500.02
{
    "status": "fail",
    "code": 500,
    "comment": "update seo error"
}
     *
     */


// ===
    /**
     * 刪除分類
     *
     * @api {DELETE} /category/{id} 03. 刪除分類
     * @apiVersion 0.1.0
     * @apiDescription ・ 刪除分類
     * @apiName DeleteCategoryDelete
     * @apiGroup Category
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id           分類 ID                                        [slug]             位址
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category_id        分類 ID
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "delete error"
}
     *
     */


// ===
    /**
     * 轉成 HTML
     *
     * @api {PUT} /category/{id}/html/make 06. 轉成 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 轉成 HTML
     * @apiName PutCategoryMake2Html
     * @apiGroup Category
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id                     分類 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category_id        分類 ID
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "make success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "html file error"
}
     *
     */



// ===
    /**
     * 同步 HTML
     *
     * @api {PUT} /category/{id}/html/sync 07. 同步 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 同步 HTML
     * @apiName PutCategorySyncHtml
     * @apiGroup Category
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id                     分類 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category_id        分類 ID
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "sync success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "html_made error"
}
     *
     */
