<?php

namespace App\Services;

use App\Repositories\AuthorRepo;
use App\Repositories\AuthorSeoRepo;


/**
 * Class AuthorServ
 *
 * @package namespace App\Services;
 */
class AuthorServ
{


    public function __construct()
    {

        $this->authorRepo = new AuthorRepo();
        $this->authorSeoRepo = new AuthorSeoRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->authorRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->authorRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $description
     *
     * @return
     */
    public function create($fullname, $avatar, $bio)
    {
        $data = ['fullname' => $fullname,
                 'avatar' => $avatar,
                 'bio' => $bio
        ];

        return $this->authorRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {

        $query  = "SELECT a.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.avatar_title, s.avatar_alt ";
        $query .= "FROM authors AS a ";
        $query .= "LEFT JOIN author_seos AS s ON s.author_id = a.id ";
        $query .= "WHERE 1 ";

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->authorRepo->fetch($query);

    } // END function


    /*
     * findFullnameByTerm
     *
     * @param $term
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findFullnameByTerm($term, $orderby = [], $page = -1, $numItems = 10)
    {

        $bindValues = [];

        $query  = "SELECT a.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.avatar_title, s.avatar_alt ";
        $query .= "FROM authors AS a ";
        $query .= "LEFT JOIN author_seos AS s ON s.author_id = a.id ";
        $query .= "WHERE a.fullname LIKE :term ";

        $bindValues['term'] = '%' . $term . '%';

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->authorRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        $datum = $this->authorRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoWhere = ['author_id' => $id];
        $seoDatum = $this->authorSeoRepo->fetchDatum($seoWhere);

        $datum->first()->slug    = $seoDatum->first()->slug;
        $datum->first()->excerpt = $seoDatum->first()->excerpt;
        $datum->first()->og_title       = $seoDatum->first()->og_title;
        $datum->first()->og_description = $seoDatum->first()->og_description;
        $datum->first()->meta_title       = $seoDatum->first()->meta_title;
        $datum->first()->meta_description = $seoDatum->first()->meta_description;
        $datum->first()->avatar_title = $seoDatum->first()->avatar_title;
        $datum->first()->avatar_alt   = $seoDatum->first()->avatar_alt;

        return $datum;
    } // END function

}
