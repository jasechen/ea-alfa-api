<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\ClickItemServ;
use App\Services\ClickLogServ;
use App\Services\StatisticClickServ;

use App\Services\PostServ;
use App\Services\PostViewLogServ;
use App\Services\StatisticPostViewServ;

use App\Services\SessionServ;


class Log extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->clickItemServ = app(ClickItemServ::class);
        $this->clickLogServ  = app(ClickLogServ::class);
        $this->statisticClickServ = app(StatisticClickServ::class);

        $this->postServ = app(PostServ::class);
        $this->postViewLogServ  = app(PostViewLogServ::class);
        $this->statisticPostViewServ = app(StatisticPostViewServ::class);

        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * createClick
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function createClick(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $itemCode  = $request->input('code');

        if (empty($itemCode)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $clickItemDatum = $this->clickItemServ->findByCode($itemCode);

        if ($clickItemDatum->isEmpty()) {
            $code = 404;
            $comment = 'clickItem error';

            Jsonponse::fail($comment, $code);
        } // END if


        $sessionDatum = $this->sessionServ->findByCode($sessionCode);


        $clickLogDatum = $this->clickLogServ->create($clickItemDatum->first()->id, $sessionDatum->first()->id, 1);

        if ($clickLogDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $statisticClickDatum = $this->statisticClickServ->findByItemId($clickItemDatum->first()->id);

        if ($statisticClickDatum->isEmpty()) {
            $statisticClickDatum = $this->statisticClickServ->create($clickItemDatum->first()->id, 1);
        } else {
            $updateData = ['num_total' => intval($statisticClickDatum->first()->num_total) + 1];
            $updateWhere = ['id' => $statisticClickDatum->first()->id];
            $statisticClickDatum = $this->statisticClickServ->update($updateData, $updateWhere);
        } // END if


        $resultData = ['session' => $sessionCode, 'click_log_id' => $clickLogDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


    /**
     * createViewPost
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function createViewPost(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $postId  = $request->input('post_id');

        if (empty($postId)) {
            $code = 400;
            $comment = 'post_id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $postDatum = $this->postServ->findById($postId);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if


        $sessionDatum = $this->sessionServ->findByCode($sessionCode);


        $postViewLogDatum = $this->postViewLogServ->create($postDatum->first()->id, $sessionDatum->first()->id, 1);

        if ($postViewLogDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $statisticPostViewDatum = $this->statisticPostViewServ->findByPostId($postDatum->first()->id);

        if ($statisticPostViewDatum->isEmpty()) {
            $statisticPostViewDatum = $this->statisticPostViewServ->create($postDatum->first()->id, 1);
        } else {
            $updateData = ['num_total' => intval($statisticPostViewDatum->first()->num_total) + 1];
            $updateWhere = ['id' => $statisticPostViewDatum->first()->id];
            $statisticPostViewDatum = $this->statisticPostViewServ->update($updateData, $updateWhere);
        } // END if


        $resultData = ['session' => $sessionCode, 'post_view_log_id' => $postViewLogDatum->first()->id];

        Jsonponse::success('create success', $resultData, 201);
    } // END function


} // END class
