<?php

namespace App\Services;

use App\Repositories\ClickLogRepo;


/**
 * Class ClickLogServ
 *
 * @package namespace App\Services;
 */
class ClickLogServ
{


    public function __construct()
    {

        $this->clickLogRepo = new ClickLogRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->clickLogRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->clickLogRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $description
     *
     * @return
     */
    public function create($itemId, $sessionId, $num = 1)
    {
        $data = ['item_id' => $itemId,
                 'session_id' => $sessionId,
                 'num' => $num
        ];

        return $this->clickLogRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->clickLogRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->clickLogRepo->fetchDatum($where);
    } // END function

}
