<?php

namespace App\Services;

use App\Repositories\PostRepo;

use App\Services\PostSeoServ;
use App\Services\CategoryServ;
use App\Services\AuthorServ;
use App\Services\FileServ;

/**
 * Class PostServ
 *
 * @package namespace App\Services;
 */
class PostServ
{


    public function __construct()
    {

        $this->postRepo = new PostRepo();

        $this->postSeoServ = new PostSeoServ();
        $this->categoryServ = new CategoryServ();
        $this->authorServ = new AuthorServ();
        $this->fileServ = new FileServ();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $title
     * @param $content
     * @param $cover
     * @param $authorId
     * @param $categoryId
     * @param $status
     *
     * @return
     */
    public function create($title, $content, $cover, $authorId, $categoryId, $status, $recommended = false)
    {
        $data = ['title'   => $title,
                 'content' => $content,
                 'cover'   => $cover,
                 'author_id' => $authorId,
                 'category_id' => $categoryId,
                 'status' => $status,
                 'recommended' => $recommended
        ];

        return $this->postRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10, $all = false)
    {

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.canonical_url, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= empty($all) ? "WHERE p.synced = 1 " : "WHERE 1 ";

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query);
    } // END function


    /*
     * findTitleByTerm
     *
     * @param $term
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findTitleByTerm($term, $status = '', $orderby = [], $page = -1, $numItems = 10)
    {

        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.canonical_url, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.title LIKE :term ";
        $query .= "AND p.synced = 1 ";

        $bindValues['term'] = '%' . $term . '%';

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findByAuthorIdAndCategoryId
     *
     * @param $authorId
     * @param $categoryId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByAuthorIdAndCategoryId($authorId, $categoryId, $status = '', $orderby = [], $page = -1, $numItems = 10)
    {

        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.canonical_url, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.author_id = :authorId ";
        $query .= "AND p.category_id = :categoryId ";

        $bindValues['authorId'] = $authorId;
        $bindValues['categoryId'] = $categoryId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findByAuthorIdAndStatus
     *
     * @param $authorId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByAuthorId($authorId, $status = '', $orderby = [], $page = -1, $numItems = 10)
    {

        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.canonical_url, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.author_id = :authorId ";
        $query .= "AND p.synced = 1 ";

        $bindValues['authorId'] = $authorId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findByCategoryId
     *
     * @param $categoryId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByCategoryIdAndRecommended($categoryId, $status = '', $orderby = [], $page = -1, $numItems = 10)
    {

        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.canonical_url, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.recommended = 1 ";
        $query .= "AND p.synced = 1 ";
        $query .= "AND p.category_id = :categoryId ";

        $bindValues['categoryId'] = $categoryId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if


        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findByCategoryId
     *
     * @param $categoryId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByCategoryId($categoryId, $status = '', $orderby = [], $page = -1, $numItems = 10)
    {

        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.canonical_url, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.category_id = :categoryId ";
        $query .= "AND p.synced = 1 ";

        $bindValues['categoryId'] = $categoryId;

        if (!empty($status)) {
            $query .= "AND p.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByStatus($status, $orderby = [], $page = -1, $numItems = 10)
    {

        $bindValues = [];

        $query  = "SELECT p.*, ";
        $query .= "s.slug, s.excerpt, s.canonical_url, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM posts AS p ";
        $query .= "LEFT JOIN post_seos AS s ON s.post_id = p.id ";
        $query .= "WHERE p.status = :status ";

        $bindValues['status'] = $status;

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->postRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {

        $where = ['id' => $id];
        $datum = $this->postRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoDatum = $this->postSeoServ->findByPostId($id);

        $datum->first()->slug    = $seoDatum->first()->slug;
        $datum->first()->excerpt = $seoDatum->first()->excerpt;
        $datum->first()->canonical_url = $seoDatum->first()->canonical_url;
        $datum->first()->og_title       = $seoDatum->first()->og_title;
        $datum->first()->og_description = $seoDatum->first()->og_description;
        $datum->first()->meta_title       = $seoDatum->first()->meta_title;
        $datum->first()->meta_description = $seoDatum->first()->meta_description;
        $datum->first()->cover_title = $seoDatum->first()->cover_title;
        $datum->first()->cover_alt   = $seoDatum->first()->cover_alt;

        return $datum;
    } // END function

}
