<?php

namespace App\Services;

use App\Repositories\AuthorSeoRepo;


/**
 * Class AuthorSeoServ
 *
 * @package namespace App\Services;
 */
class AuthorSeoServ
{


    public function __construct()
    {

        $this->authorSeoRepo = new AuthorSeoRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->authorSeoRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->authorSeoRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $categoryId
     * @param $slug
     * @param $excerpt
     * @param $ogTitle
     * @param $ogDescription
     * @param $metaTitle
     * @param $metaDescription
     * @param $avatarTitle
     * @param $avatarAlt
     *
     * @return
     */
    public function create($authorId, $slug, $excerpt, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $avatarTitle, $avatarAlt)
    {
        $data = ['author_id' => $authorId,
                'slug' => $slug,
                'excerpt' => $excerpt,
                'og_title' => $ogTitle,
                'og_description' => $ogDescription,
                'meta_title' => $metaTitle,
                'meta_description' => $metaDescription,
                'avatar_title' => $avatarTitle,
                'avatar_alt' => $avatarAlt
        ];

        return $this->authorSeoRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {
        return $this->authorSeoRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findBySlug
     *
     * @param $slug
     *
     * @return
     */
    public function findBySlug($slug)
    {
        $where = ['slug' => $slug];

        return $this->authorSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findByAuthorId
     *
     * @param $authorId
     *
     * @return
     */
    public function findByAuthorId($authorId)
    {
        $where = ['author_id' => $authorId];

        return $this->authorSeoRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->authorSeoRepo->fetchDatum($where);
    } // END function

}
