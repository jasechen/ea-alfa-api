<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->char('title', 255);
            $table->text('content');
            $table->char('cover', 255);
            $table->bigInteger('author_id');
            $table->enum('status', ['draft', 'publish', 'disable', 'delete'])->default('draft');
            $table->bigInteger('category_id');
            $table->boolean('html_made')->default(false);
            $table->boolean('synced')->default(false);

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index(['author_id','status']);
            $table->index(['status','html_made','synced']);
            $table->index(['html_made','synced']);
            $table->index('synced');
            $table->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
