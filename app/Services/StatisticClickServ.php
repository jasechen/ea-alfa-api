<?php

namespace App\Services;

use App\Repositories\StatisticClickRepo;


/**
 * Class StatisticClickServ
 *
 * @package namespace App\Services;
 */
class StatisticClickServ
{


    public function __construct()
    {

        $this->statisticClickRepo = new StatisticClickRepo();
    } // END function


    /*
     * findByItemId
     *
     * @param $itemId
     *
     * @return
     */
    public function findByItemId($itemId)
    {
        $where = ['item_id' => $itemId];

        return $this->statisticClickRepo->fetchDatum($where);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->statisticClickRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->statisticClickRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $description
     *
     * @return
     */
    public function create($itemId, $numTotal)
    {
        $data = ['item_id' => $itemId,
                 'num_total' => $numTotal
        ];

        return $this->statisticClickRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->statisticClickRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->statisticClickRepo->fetchDatum($where);
    } // END function

}
