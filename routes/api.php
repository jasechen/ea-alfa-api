<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// ===
Route::pattern('id', '[0-9]+');
Route::pattern('author_id', '[0-9]+');
Route::pattern('category_id', '[0-9]+');


Route::group(['prefix' => 'author'], function () use ($router) {
    $router->get('{id}', 'Author@find');
    $router->get('list/{order_way?}/{page?}', 'Author@findAll');
});


Route::group(['prefix' => 'category'], function () use ($router) {
    $router->post('',       'Category@create');
    $router->put('{id}',    'Category@update');
    $router->delete('{id}', 'Category@delete');

    $router->get('{id}', 'Category@find');
    $router->get('list/{order_way?}/{page?}', 'Category@findAll');

    $router->put('{id}/html/make', 'Tool@makeCategory2Html');
    $router->put('{id}/html/sync', 'Tool@syncCategoryHtml');
});


Route::group(['prefix' => 'file'], function () use ($router) {
    $router->post('', 'File@upload');
});


Route::group(['prefix' => 'log'], function () use ($router) {
    $router->post('click',     'Log@createClick');
    $router->post('view/post', 'Log@createViewPost');
});


Route::group(['prefix' => 'post'], function () use ($router) {
    $router->post('',       'Post@create');
    $router->put('{id}',    'Post@update');
    $router->delete('{id}', 'Post@delete');

    $router->get('{id}', 'Post@find');
    $router->get('list/{order_way?}/{page?}', 'Post@findAll');

    $router->get('slug/{slug}', 'Post@findBySlug');
    $router->get('author/{id}/{order_way?}/{page?}',   'Post@findByAuthorId');
    $router->get('category/{id}/{order_way?}/{page?}', 'Post@findByCategoryId');
    $router->get('author/category/{author_id}/{category_id}/{order_way?}/{page?}', 'Post@findByAuthorIdAndCategoryId');

    $router->put('{id}/html/make', 'Tool@makePost2Html');
    $router->put('{id}/html/sync', 'Tool@syncPostHtml');

    $router->get('recommend/category/{id}/{order_way?}/{page?}', 'Post@findByCategoryIdAndRecommended');
    $router->get('category/slug/{slug}/{order_way?}/{page?}', 'Post@findByCategorySlug');

    $router->get('nimda/list/{order_way?}/{page?}', 'Post@findAllNimda');
});


Route::group(['prefix' => 'search'], function () use ($router) {
    $router->get('{term}/{page?}', 'Search@findByTerm');
});


Route::group(['prefix' => 'session'], function () use ($router) {
    $router->post('init',  'Session@init');
    $router->get('{code}', 'Session@findByCode');
});


Route::group(['prefix' => 'subscribe'], function () use ($router) {
    $router->post('', 'Subscribe@create');
    $router->get('subscribers', 'Subscribe@findByStatusSubscribe');
});


Route::group(['prefix' => 'tool'], function () use ($router) {
    $router->put('category/{id}/html/make', 'Tool@makeCategory2Html');
    $router->put('category/{id}/html/sync', 'Tool@syncCategoryHtml');
    $router->put('category/all/html/make', 'Tool@makeCategoryAll2Html');
    $router->put('category/all/html/sync', 'Tool@syncCategoryAllHtml');

    $router->put('post/{id}/html/make', 'Tool@makePost2Html');
    $router->put('post/{id}/html/sync', 'Tool@syncPostHtml');
    $router->put('post/all/html/make', 'Tool@makePostAll2Html');
    $router->put('post/all/html/sync', 'Tool@syncPostAllHtml');

    $router->put('htaccess/make', 'Tool@makeHtaccess');
});



