<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\CategoryServ;
use App\Services\CategorySeoServ;
use App\Services\AuthorServ;
use App\Services\PostServ;
use App\Services\PostSeoServ;
use App\Services\FileServ;


class Post extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->sessionServ  = app(SessionServ::class);
        $this->categoryServ = app(CategoryServ::class);
        $this->categorySeoServ = app(CategorySeoServ::class);
        $this->authorServ = app(AuthorServ::class);
        $this->postServ = app(PostServ::class);
        $this->postSeoServ = app(PostSeoServ::class);
        $this->fileServ = app(FileServ::class);
    } // END function



    /**
     * create
     *
     * @method POST
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @throws
     * @return
     */
    public function create(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        $title   = $request->input('title');
        $content = $request->input('content');
        $cover   = $request->input('cover');
        $status  = $request->input('status', config('tbl_posts.DEFAULT_POSTS_STATUS'));
        $authorId   = $request->input('author_id');
        $categoryId = $request->input('category_id');
        $recommended = $request->input('recommended', 'false');

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $canonicalUrl = $request->input('canonical_url');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');

        if (empty($title)) {
            $code = 400;
            $comment = 'title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($content)) {
            $code = 400;
            $comment = 'content empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($cover)) {
            $code = 400;
            $comment = 'cover empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($authorId)) {
            $code = 400;
            $comment = 'author_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($categoryId)) {
            $code = 400;
            $comment = 'category_id empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($excerpt)) {
            $code = 400;
            $comment = 'excerpt empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($canonicalUrl)) {
            $code = 400;
            $comment = 'canonical_url empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogTitle)) {
            $code = 400;
            $comment = 'og_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($ogDescription)) {
            $code = 400;
            $comment = 'og_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaTitle)) {
            $code = 400;
            $comment = 'meta_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($metaDescription)) {
            $code = 400;
            $comment = 'meta_description empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverTitle)) {
            $code = 400;
            $comment = 'cover_title empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($coverAlt)) {
            $code = 400;
            $comment = 'cover_alt empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_posts.POSTS_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $recommendedValidator = Validator::make(['recommended' => $recommended],
            ['recommended' => ['in:true,false']]
        );

        if ($recommendedValidator->fails()) {
            $code = 422;
            $comment = 'recommended error';

            Jsonponse::fail($comment, $code);
        } // END if
        $recommended = $recommended == "true" ? true : false;

        $authorDatum = $this->authorServ->findById($authorId);

        if ($authorDatum->isEmpty()) {
            $code = 404;
            $comment = 'author error';

            Jsonponse::fail($comment, $code);
        } // END if

        $categoryDatum = $this->categoryServ->findById($categoryId);

        if ($categoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if

        $postSeoDatum = $this->postSeoServ->findBySlug($slug);

        if ($postSeoDatum->isNotEmpty()) {
            $code = 409;
            $comment = 'slug error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileDatum = $this->fileServ->findByFilename($cover);

        if ($fileDatum->isEmpty()) {
            $code = 404;
            $comment = 'cover error';

            Jsonponse::fail($comment, $code);
        } // END if


        $postDatum = $this->postServ->create($title, $content, $cover, $authorId, $categoryId, $status, $recommended);

        if ($postDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if

        $this->postSeoServ->create($postDatum->first()->id, $slug, $excerpt, $canonicalUrl, $ogTitle, $ogDescription, $metaTitle, $metaDescription, $coverTitle, $coverAlt);


        // update num_posts in category
        $this->categoryServ->updateNumPosts($categoryId);


        Storage::disk('gallery')->makeDirectory('post/' . $postDatum->first()->id, 0755, true);
        $this->fileServ->moveFileTo($cover, 'post/' . $postDatum->first()->id, 'post');

        $updatedata = ['parent_id' => $postDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
        $updateWhere = ['id' => $fileDatum->first()->id];
        $this->fileServ->update($updatedata, $updateWhere);


        if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_GALLERY'))) {
            $filePaths = $this->fileServ->findLinks($cover);

            foreach ($filePaths as $filePath) {
                $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                if (empty($exists)) {
                    $sFilename = public_path($filePath);
                    $sFilename = new File($sFilename);
                    Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                } // END if
            } // END foreach
        } // END if


        $resultData = ['session' => $sessionCode, 'post_id' => $postDatum->first()->id];

        Jsonponse::success('create success', $resultData);
    } // END function


    /**
     * update
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function update(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if


        $title   = $request->input('title');
        $content = $request->input('content');
        $cover   = $request->input('cover');
        $status  = $request->input('status');
        $authorId   = $request->input('author_id');
        $categoryId = $request->input('category_id');
        $recommended  = $request->input('recommended');

        $slug = $request->input('slug');
        $excerpt = $request->input('excerpt');
        $canonicalUrl = $request->input('canonical_url');
        $ogTitle = $request->input('og_title');
        $ogDescription = $request->input('og_description');
        $metaTitle = $request->input('meta_title');
        $metaDescription = $request->input('meta_description');
        $coverTitle = $request->input('cover_title');
        $coverAlt = $request->input('cover_alt');

        $updateData = $updateSeoData = [];

        if (!empty($title) AND $title != $postDatum->first()->title) {
            $updateData['title'] = $title;
        } // END if

        if (!empty($content) AND $content != $postDatum->first()->content) {
            $updateData['content'] = $content;
        } // END if

        if (!empty($cover) AND $cover != $postDatum->first()->cover) {
            $fileDatum = $this->fileServ->findByFilename($cover);

            if ($fileDatum->isEmpty()) {
                $code = 404;
                $comment = 'cover error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['cover'] = $cover;
        } // END if

        if (!empty($status) AND $status != $postDatum->first()->status) {
            $statusValidator = Validator::make(['status' => $status],
                ['status' => ['in:' . implode(',', config('tbl_posts.POSTS_STATUS'))]]
            );

            if ($statusValidator->fails()) {
                $code = 422;
                $comment = 'status error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['status'] = $status;
        } // END if

        $recommendedChecked = empty($postDatum->first()->recommended) ? "false" : "true";
        if (!empty($recommended) AND $recommended != $recommendedChecked) {
            $recommendedValidator = Validator::make(['recommended' => $recommended],
                ['recommended' => ['in:true,false']]
            );

            if ($recommendedValidator->fails()) {
                $code = 422;
                $comment = 'recommended error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['recommended'] = $recommended == "false" ? false : true;
        } // END if

        if (!empty($authorId) AND $authorId != $postDatum->first()->author_id) {
            $authorDatum = $this->authorServ->findById($authorId);

            if ($authorDatum->isEmpty()) {
                $code = 404;
                $comment = 'author error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['author_id'] = $authorId;
        } // END if

        if (!empty($categoryId) AND $categoryId != $postDatum->first()->category_id) {
            $categoryDatum = $this->categoryServ->findById($categoryId);

            if ($categoryDatum->isEmpty()) {
                $code = 404;
                $comment = 'category error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateData['category_id'] = $categoryId;
        } // END if

        if (!empty($slug) AND $slug != $postDatum->first()->slug) {
            $postSeoDatum = $this->postSeoServ->findBySlug($slug);

            if ($postSeoDatum->isNotEmpty() AND $postSeoDatum->first()->post_id != $id) {
                $code = 409;
                $comment = 'slug error';

                Jsonponse::fail($comment, $code);
            } // END if

            $updateSeoData['slug'] = $slug;
        } // END if

        if (!empty($excerpt) AND $excerpt != $postDatum->first()->excerpt) {
            $updateSeoData['excerpt'] = $excerpt;
        } // END if

        if (!empty($canonicalUrl) AND $canonicalUrl != $postDatum->first()->canonical_url) {
            $updateSeoData['canonical_url'] = $canonicalUrl;
        } // END if

        if (!empty($ogTitle) AND $ogTitle != $postDatum->first()->og_title) {
            $updateSeoData['og_title'] = $ogTitle;
        } // END if

        if (!empty($ogDescription) AND $ogDescription != $postDatum->first()->og_description) {
            $updateSeoData['og_description'] = $ogDescription;
        } // END if

        if (!empty($metaTitle) AND $metaTitle != $postDatum->first()->meta_title) {
            $updateSeoData['meta_title'] = $metaTitle;
        } // END if

        if (!empty($metaDescription) AND $metaDescription != $postDatum->first()->meta_description) {
            $updateSeoData['meta_description'] = $metaDescription;
        } // END if

        if (!empty($coverTitle) AND $coverTitle != $postDatum->first()->cover_title) {
            $updateSeoData['cover_title'] = $coverTitle;
        } // END if

        if (!empty($coverAlt) AND $coverAlt != $postDatum->first()->cover_alt) {
            $updateSeoData['cover_alt'] = $coverAlt;
        } // END if


        if (empty($updateData) AND empty($updateSeoData)) {
            $code = 400;
            $comment = 'updateData / updateSeoData empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateFail = $updateSeoFail = false;

        if (!empty($updateData)) {
            $updatePostDatum = $this->postServ->update($updateData, ['id' => $id]);

            if (empty($updatePostDatum) OR $updatePostDatum->isEmpty()) {
                $updateFail = true;
            } else {

                // update num_posts in category
                if (!empty($updateData['status']) AND $updateData['status'] != 'publish' AND $postDatum->first()->status == 'publish') {
                    $this->categoryServ->updateNumPosts($categoryId, -1);
                } // END if

                if (!empty($updateData['cover']) AND $updateData['cover'] != $postDatum->first()->cover) {
                    $updatedata = ['parent_id' => $postDatum->first()->id, 'status' => config('tbl_files.FILES_STATUS_ENABLE')];
                    $updateWhere = ['id' => $fileDatum->first()->id];
                    $this->fileServ->update($updatedata, $updateWhere);

                    $this->fileServ->moveFileTo($cover, 'post/' . $postDatum->first()->id, 'post');

                    if (!empty(env('AWS_SECRET_ACCESS_KEY')) AND !empty(env('AWS_BUCKET_GALLERY'))) {
                        $filePaths = $this->fileServ->findLinks($cover);

                        foreach ($filePaths as $filePath) {
                            $tFolder   = mb_substr(pathinfo($filePath, PATHINFO_DIRNAME), 8);
                            $tFilename = pathinfo($filePath, PATHINFO_BASENAME);

                            $exists = Storage::disk('s3-gallery')->exists($tFolder . '/' . $tFilename);
                            if (empty($exists)) {
                                $sFilename = public_path($filePath);
                                $sFilename = new File($sFilename);
                                Storage::disk('s3-gallery')->putFileAs($tFolder, $sFilename, $tFilename, 'public');
                            } // END if
                        } // END foreach
                    } // END if

                } // END if
            } // END if
        } // END if

        if (!empty($updateSeoData)) {
            $postSeoDatum = $this->postSeoServ->findByPostId($id);
            if ($postSeoDatum->isNotEmpty()) {
                $updatePostSeoDatum = $this->postSeoServ->update($updateSeoData, ['id' => $postSeoDatum->first()->id]);

                if (empty($updatePostSeoDatum) OR $updatePostSeoDatum->isEmpty()) {
                    $updateSeoFail = true;
                } // END if

            } // END if
        } // END if

        if (!empty($updateFail) OR !empty($updateSeoFail)) {
            $code = 500;
            $comment = 'update error';

            Jsonponse::fail($comment, $code);
        } // END if


        $oHtmlPath = 'post/' . $postDatum->first()->status . '/';
        $htmlFile  = $postDatum->first()->id . '.html';

        $oExists = Storage::disk('public')->exists($oHtmlPath . $htmlFile);
        if (!empty($oExists)) {
            Storage::disk('public')->delete($oHtmlPath . $htmlFile);
        } // END if

        // $this->postServ->update(['html_made' => false, 'synced' => false], ['id' => $id]);
        $this->postServ->update(['html_made' => false], ['id' => $id]);


        $resultData = ['session' => $sessionCode, 'post_id' => $id];

        Jsonponse::success('update success', $resultData, 201);
    } // END function


    /**
     * delete
     *
     * @method DELETE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function delete(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if

        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if

        if ($postDatum->first()->status == config('tbl_posts.POSTS_STATUS_DELETE')) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if


        $updateData = ['synced' => false, 'status' => config('tbl_posts.POSTS_STATUS_DELETE')];
        $updatePostDatum = $this->postServ->update($updateData, ['id' => $id]);

        if ($updatePostDatum->isEmpty()) {
            $code = 500;
            $comment = 'delete error';

            Jsonponse::fail($comment, $code);
        } // END if


        $oHtmlPath = 'post/' . $postDatum->first()->status . '/';
        $tHtmlPath = 'post/delete/';
        $htmlFile  = $postDatum->first()->id . '.html';

        $oExists = Storage::disk('public')->exists($oHtmlPath . $htmlFile);
        if (!empty($oExists)) {
            Storage::disk('public')->move($oHtmlPath . $htmlFile, $tHtmlPath . $htmlFile);
        } // END if


        $resultData = ['session' => $sessionCode, 'post_id' => $id];

        Jsonponse::success('delete success', $resultData);
    } // END function


    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $categoryDatum = $this->categoryServ->findById($postDatum->first()->category_id);
        $postDatum->first()->category = $categoryDatum->first();

        $authorDatum = $this->authorServ->findById($postDatum->first()->author_id);
        $authorDatum->first()->avatar_links = [];
        if (!empty($authorDatum->first()->avatar)) {
            $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
            $authorDatum->first()->avatar_links = $avayarLinkData;
        } // END if else
        $postDatum->first()->author = $authorDatum->first();

        $postDatum->first()->cover_links = [];
        if (!empty($postDatum->first()->cover)) {
            $coverLinkData = $this->fileServ->findLinks($postDatum->first()->cover);
            $postDatum->first()->cover_links = $coverLinkData;
        } // END if else

        $postDatum->first()->s3_url = env('AWS_BUCKET_URL');
        $postDatum->first()->url_path = empty($postDatum->first()->slug) ? '' : '/' . $postDatum->first()->slug . '-p' . $postDatum->first()->id;


        // $resultData = ['session' => $sessionCode, 'post' => $postDatum->first()];
        $resultData = ['post' => $postDatum->first()];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findAllNimda(Request $request, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['p.id' => $orderWay];

        $postData = $this->postServ->findAll($orderby, $page, 10, true);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {
            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $postDatum->category = $categoryDatum->first();

            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else
            $postDatum->author = $authorDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            $postDatum->s3_url = env('AWS_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'posts' => $finalData];
        $resultData = ['posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findAll(Request $request, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['p.id' => $orderWay];

        $postData = $this->postServ->findAll($orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {
            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $postDatum->category = $categoryDatum->first();

            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else
            $postDatum->author = $authorDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            $postDatum->s3_url = env('AWS_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'posts' => $finalData];
        $resultData = ['posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findBySlug
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     *
     * @throws
     * @return
     */
    public function findBySlug(Request $request, $slug)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $postSeoDatum = $this->postSeoServ->findBySlug($slug);

        if ($postSeoDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $postDatum = $this->postServ->findById($postSeoDatum->first()->post_id);

        $categoryDatum = $this->categoryServ->findById($postDatum->first()->category_id);
        $postDatum->first()->category = $categoryDatum->first();

        $authorDatum = $this->authorServ->findById($postDatum->first()->author_id);
        $authorDatum->first()->avatar_links = [];
        if (!empty($authorDatum->first()->avatar)) {
            $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
            $authorDatum->first()->avatar_links = $avayarLinkData;
        } // END if else
        $postDatum->first()->author = $authorDatum->first();

        $postDatum->first()->cover_links = [];
        if (!empty($postDatum->first()->cover)) {
            $coverLinkData = $this->fileServ->findLinks($postDatum->first()->cover);
            $postDatum->first()->cover_links = $coverLinkData;
        } // END if else

        $postDatum->first()->s3_url = env('AWS_BUCKET_URL');
        $postDatum->first()->url_path = empty($postDatum->first()->slug) ? '' : '/' . $postDatum->first()->slug . '-p' . $postDatum->first()->id;


        // $resultData = ['session' => $sessionCode, 'post' => $postDatum->first()];
        $resultData = ['post' => $postDatum->first()];

        Jsonponse::success('find success', $resultData);
    } // END function



    /**
     * findByAuthorId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByAuthorId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['p.id' => $orderWay];
        $status = config('tbl_posts.POSTS_STATUS_PUBLISH');

        $postData = $this->postServ->findByAuthorId($id, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {

            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $postDatum->category = $categoryDatum->first();

            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else
            $postDatum->author = $authorDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            $postDatum->s3_url = env('AWS_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'posts' => $finalData];
        $resultData = ['posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByCategoryId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByCategoryId(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['p.id' => $orderWay];
        $status = config('tbl_posts.POSTS_STATUS_PUBLISH');

        $postData = $this->postServ->findByCategoryId($id, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {
            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $postDatum->category = $categoryDatum->first();

            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else
            $postDatum->author = $authorDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            $postDatum->s3_url = env('AWS_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'posts' => $finalData];
        $resultData = ['posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByCategoryId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByCategoryIdAndRecommended(Request $request, $id, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['p.id' => $orderWay];
        $status = config('tbl_posts.POSTS_STATUS_PUBLISH');

        $postData = $this->postServ->findByCategoryIdAndRecommended($id, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {

            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $postDatum->category = $categoryDatum->first();

            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else
            $postDatum->author = $authorDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            $postDatum->s3_url = env('AWS_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'posts' => $finalData];
        $resultData = ['posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByAuthorIdAndCategoryId
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $author_id
     * @param  $category_id
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByAuthorIdAndCategoryId(Request $request, $author_id, $category_id, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($author_id)) {
            $code = 400;
            $comment = 'author_id empty';

            Jsonponse::fail($comment, $code);
        } // END if
        $authorId = intval($author_id);

        if (empty($category_id)) {
            $code = 400;
            $comment = 'category_id empty';

            Jsonponse::fail($comment, $code);
        } // END if
        $categoryId = intval($category_id);


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['p.id' => $orderWay];
        $status = config('tbl_posts.POSTS_STATUS_PUBLISH');

        $postData = $this->postServ->findByAuthorIdAndCategoryId($authorId, $categoryId, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {

            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $postDatum->category = $categoryDatum->first();

            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else
            $postDatum->author = $authorDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            $postDatum->s3_url = env('AWS_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'posts' => $finalData];
        $resultData = ['posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findByCategorySlug
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     * @param  $order_way
     * @param  $page
     *
     * @throws
     * @return
     */
    public function findByCategorySlug(Request $request, $slug, $order_way = 'ASC', $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($slug)) {
            $code = 400;
            $comment = 'slug empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if

        $slug = urldecode($slug);
        $categorySeoDatum = $this->categorySeoServ->findBySlug($slug);

        if ($categorySeoDatum->isEmpty()) {
            $code = 404;
            $comment = 'category seo error';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryId = $categorySeoDatum->first()->category_id;
        $orderby = ['p.id' => $orderWay];
        $status = config('tbl_posts.POSTS_STATUS_PUBLISH');

        $postData = $this->postServ->findByCategoryId($categoryId, $status, $orderby, $page);

        if ($postData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $finalData = [];
        foreach ($postData->all() as $postDatum) {
            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $postDatum->category = $categoryDatum->first();

            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else
            $postDatum->author = $authorDatum->first();

            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            $postDatum->s3_url = env('AWS_BUCKET_URL');
            $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

            array_push($finalData, $postDatum);
        } // END foreach


        // $resultData = ['session' => $sessionCode, 'posts' => $finalData];
        $resultData = ['posts' => $finalData];

        Jsonponse::success('find success', $resultData);
    } // END function



} // END class
