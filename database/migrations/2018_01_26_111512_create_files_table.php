<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('files', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->enum('parent_type', ['post', 'author', 'category'])->default('post');
            $table->unsignedBigInteger('parent_id')->default(0);
            $table->unsignedBigInteger('session_id');
            $table->string('filename');
            $table->string('extension');
            $table->string('mime_type');
            $table->integer('size');

            $table->enum('type', ['image', 'video', 'audio', 'others'])->default('image');
            $table->enum('status', ['enable', 'disable', 'delete'])->default('enable');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index(['parent_type','parent_id','status']);
            $table->index('session_id');
            $table->unique('filename');
            $table->index('type');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
