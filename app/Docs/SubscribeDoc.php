<?php

namespace App\Docs;

    /**
     * 訂閱 EDM
     *
     * @api {POST} /subscribe 01. 訂閱 EDM
     * @apiVersion 0.1.0
     * @apiDescription ・ 用 email 訂閱 EDM
     * @apiName PostSubscribeCreate
     * @apiGroup Subscribe
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           name             姓名
     * @apiParam {string}                                           email             E-Mail
     *
     * @apiParamExample {json} Request
{
    "name" : "John",
    "email" : "account@abc.com"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.subscriber_id  訂閱 EDM ID
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "subscriber_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "name empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "email empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "email format error"
}
     *
     * @apiErrorExample {json}  Response: 409.01
{
    "status": "fail",
    "code": 409,
    "comment": "email already exist"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */


// ===
    /**
     * 查詢 所有訂閱者
     *
     * @api {GET} /subscribe/subscribers 02. 查詢 所有訂閱者
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢 所有訂閱者。
     * @apiName GetSubscribeFindByStatusSubscribe
     * @apiGroup Subscribe
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.subscribers    訂閱者
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "subscribers": [
            {
                "id": "1",
                "name": "John",
                "email": "jasechen@gmail.com",
                "status": "subscribe",
                "updated_at": "2018-01-22 11:28:55",
                "created_at": "2018-01-22 11:28:55"
            },
            {
                "id": "2",
                "name": "John",
                "email": "jase.chen@gmail.com",
                "status": "subscribe",
                "updated_at": "2018-01-22 11:28:55",
                "created_at": "2018-01-22 11:28:55"
            },
            ...
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */
