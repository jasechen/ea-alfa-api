<?php

namespace App\Docs;


    /**
     * 搜尋 文章標題 title 及 作者姓名 fullname
     *
     * @api {GET} /search/{term} 01. 搜尋
     * @apiVersion 0.1.0
     * @apiDescription ・ 搜尋 文章標題 title 及 作者姓名 fullname
     * @apiName GetSearchfindByTerm
     * @apiGroup Search
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    term                           搜尋字串 (請先用 urlencode)
     * @apiParam {string}                     [page="-1"]          頁數
     *
     * @apiParamExample {json} Request
{
    "term"   : "出國",
    "page" : "-1"
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string[]}   data.posts    文章
     * @apiSuccess (Success) {string[]}   data.authors    作者
     *
     * @apiSuccessExample {json}    Response: 200
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "posts": [],
        "authors": [
            {
                "id": "43128077835964416",
                "fullname": "秋天小姐",
                "avatar": "1518106534354692",
                "bio": "喜歡秋天光影下的溫度，有時嬉鬧有時安靜。喜愛旅行，著迷於美好事物的細細品嘗。　　",
                "updated_at": "2018-02-09 00:15:35",
                "created_at": "2018-02-09 00:15:35",
                "slug": "miss_autumn",
                "excerpt": "喜歡秋天光影下的溫度，有時嬉鬧有時安靜。                                                                                                        喜愛旅行，著迷於美好事物的細細品嘗。",
                "og_title": "秋天小姐",
                "og_description": "喜歡秋天光影下的溫度，有時嬉鬧有時安靜。喜愛旅行，著迷於美好事物的細細品嘗。　　",
                "meta_title": "秋天小姐",
                "meta_description": "喜歡秋天光影下的溫度，有時嬉鬧有時安靜。喜愛旅行，著迷於美好事物的細細品嘗。　　",
                "avatar_title": "Miss Autumn",
                "avatar_alt": "Miss Autumn",
                "avatar_links": {
                    "l": "gallery/author/43128077835964416/1518106534354692_l.jpeg",
                    "c": "gallery/author/43128077835964416/1518106534354692_c.jpeg",
                    "z": "gallery/author/43128077835964416/1518106534354692_z.jpeg",
                    "n": "gallery/author/43128077835964416/1518106534354692_n.jpeg",
                    "q": "gallery/author/43128077835964416/1518106534354692_q.jpeg",
                    "sq": "gallery/author/43128077835964416/1518106534354692_sq.jpeg",
                    "o": "gallery/author/43128077835964416/1518106534354692_o.jpeg"
                }
            }
        ]
    }
}
     *
     * @apiSuccessExample {json}    Response: 204
{}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "term is empty"
}
     *
     * @apiErrorExample {json}  Response: 410
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     */
