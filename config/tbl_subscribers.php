<?php
$fields = [
    'SUBSCRIBERS_STATUS_SUBSCRIBE'   => 'subscribe',
    'SUBSCRIBERS_STATUS_UNSUBSCRIBE' => 'unsubscribe'
];

$fields['DEFAULT_SUBSCRIBERS_STATUS'] = $fields['SUBSCRIBERS_STATUS_SUBSCRIBE'];
$fields['SUBSCRIBERS_STATUS'] = [
    $fields['SUBSCRIBERS_STATUS_SUBSCRIBE'],
    $fields['SUBSCRIBERS_STATUS_UNSUBSCRIBE']
];

return $fields;