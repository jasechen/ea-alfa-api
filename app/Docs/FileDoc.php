<?php

namespace App\Docs;

    /**
     * 上傳檔案
     *
     * @api {POST} /file    01. 上傳檔案
     * @apiVersion 0.1.0
     * @apiDescription ・ 上傳檔案
     * @apiName PostFileUpload
     * @apiGroup File
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string=post,author,category}            [parent_type="post"]       所屬項目
     * @apiParam {string=enable,disable,delete}           [status="disable"]         狀態
     * @apiParam {file}                            file                         檔案
     *
     * @apiParamExample {json} Request
{
    "parent_type" : "post",
    "status" : "disable",
    "file" : ""
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.file_id        檔案 ID
     * @apiSuccess (Success) {string}   data.filename       檔案名稱
     * @apiSuccess (Success) {string[]} data.file_links     各尺寸圖檔連結
     *
     * @apiSuccessExample {json}    Response: 201
{
    "status": "success",
    "code": 201,
    "comment": "upload success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "file_id": "38771455470735360",
        "filename": "1517067835270712",
        "file_links": {
            "c": "gallery/post/1517067835270712_c.jpeg",
            "z": "gallery/post/1517067835270712_z.jpeg",
            "n": "gallery/post/1517067835270712_n.jpeg",
            "q": "gallery/post/1517067835270712_q.jpeg",
            "sq": "gallery/post/1517067835270712_sq.jpeg",
            "o": "gallery/post/1517067835270712_o.jpeg"
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "file empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "parent_type error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "file error"
}
     *
     * @apiErrorExample {json}  Response: 500.01
{
    "status": "fail",
    "code": 500,
    "comment": "create error"
}
     *
     */

