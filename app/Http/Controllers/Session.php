<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\DeviceServ;
use App\Services\SessionServ;


class Session extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->deviceServ   = app(DeviceServ::class);
        $this->sessionServ   = app(SessionServ::class);
    } // END function


    /**
     * init
     *
     * @method POST
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function init(Request $request)
    {

        $deviceCode  = $request->input('device_code', $this->deviceServ->initCode());
        $deviceOS    = $request->input('device_os',   config('tbl_devices.DEFAULT_DEVICES_OS'));
        $deviceType  = $request->input('device_type', config('tbl_devices.DEFAULT_DEVICES_TYPE'));
        $deviceLang  = $request->input('device_lang', config('tbl_devices.DEFAULT_DEVICES_LANG'));
        $deviceToken = $request->input('device_token');
        $deviceChannel = $request->input('device_channel');

        $remoteAddress = $request->ip();


        $deviceOSValidator = Validator::make(['device_os' => $deviceOS],
            ['device_os' => ['in:' . implode(',', config('tbl_devices.DEVICES_OS'))]]
        );

        if ($deviceOSValidator->fails()) {
            $code = 422;
            $comment = 'device_os error';

            Jsonponse::fail($comment, $code);
        } // END if


        $deviceTypeValidator = Validator::make(['device_type' => $deviceType],
            ['device_type' => ['in:' . implode(',', config('tbl_devices.DEVICES_TYPES'))]]
        );

        if ($deviceTypeValidator->fails()) {
            $code = 422;
            $comment = 'device_type error';

            Jsonponse::fail($comment, $code);
        } // END if


        $deviceLangValidator = Validator::make(['device_lang' => $deviceLang],
            ['device_lang' => ['in:' . implode(',', config('tbl_devices.DEVICES_LANGS'))]]
        );

        if ($deviceLangValidator->fails()) {
            $code = 422;
            $comment = 'device_lang error';

            Jsonponse::fail($comment, $code);
        } // END if


        if (!empty($deviceChannel)) {
            $deviceChannelValidator = Validator::make(['device_channel' => $deviceChannel],
                ['device_channel' => ['in:' . implode(',', config('tbl_devices.DEVICES_CHANNELS'))]]
            );

            if ($deviceChannelValidator->fails()) {
                $code = 422;
                $comment = 'device_channel error';

                Jsonponse::fail($comment, $code);
            } // END if
        } // END if


        if (empty($deviceChannel)) {
            if ($deviceOS == config('tbl_devices.DEVICES_OS_IOS')) {
                $deviceChannel = config('tbl_devices.DEVICES_CHANNEL_APNS');
            } else if ($deviceOS == config('tbl_devices.DEVICES_OS_ANDRIOD')) {
                $deviceChannel = config('tbl_devices.DEVICES_CHANNEL_GCM');
            } else {
                $deviceChannel = config('tbl_devices.DEVICES_CHANNEL_OTHERS');
            } // END if else
        } // END if


        $deviceDatum = $this->deviceServ->findByCode($deviceCode);

        if ($deviceDatum->isEmpty()) {
            $deviceDatum = $this->deviceServ->create($deviceCode, $deviceOS, $deviceType, $deviceLang, $deviceChannel, $deviceToken);
        } // END if


        $sessionDatum = $this->sessionServ->findByDeviceId($deviceDatum->first()->id);

        if ($sessionDatum->isEmpty()) {
            $sessionDatum = $this->sessionServ->create($deviceDatum->first()->id, $remoteAddress);

            $request->session()->setId($sessionDatum->first()->code);
        } else {
            $sessionFields = ['remote_address' => $remoteAddress];

            $isAlive = $this->sessionServ->isAlive($sessionDatum->first()->code);

            if (empty($isAlive)) {
                $sessionFields['code'] = $this->sessionServ->initCode();

                $request->session()->setId($sessionFields['code']);
            } // END if

            $this->sessionServ->update($sessionFields, ['id' => $sessionDatum->first()->id]);
            $sessionDatum = $this->sessionServ->findByDeviceId($device->first()->id);
        } // END if


        $resultData = ['device_code' => $deviceDatum->first()->code, 'session' => $sessionDatum->first()->code];

        Jsonponse::success('init success', $resultData, 201);
    } // END function


    /**
     * findByCode
     *
     * @method GET
     * @param  $code
     * @throws
     * @return
     */
    public function findByCode($code)
    {

        if (empty($code)) {
            $code = 400;
            $comment = 'code empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $sessionDatum = $this->sessionServ->findByCode($code);

        if ($sessionDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        $isAlive = $this->sessionServ->isAlive($code);

        $status = empty($isAlive) ? 'invalid' : 'valide';

        $resultData = ['session' => $sessionDatum->first()->code, 'status'  => $status];

        Jsonponse::success('fetch success', $resultData);
    } // END function
} // END class
