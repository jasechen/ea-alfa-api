<?php

namespace App\Docs;


    /**
     * 單一分類轉成 HTML
     *
     * @api {PUT} /tool/category/{id}/html/make 01. 分類轉成 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 單一分類轉成 HTML
     * @apiName PutCategoryMake2Html
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id                     分類 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category_id        分類 ID
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "make success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "html file error"
}
     *
     */


    /**
     * 單一分類同步 HTML
     *
     * @api {PUT} /tool/category/{id}/html/sync 02. 分類同步 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 單一分類同步 HTML
     * @apiName PutCategorySyncHtml
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id                     分類 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.category_id        分類 ID
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "sync success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "category_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "html_made error"
}
     *
     */


    /**
     * 所有分類轉成 HTML
     *
     * @api {PUT} /tool/category/all/html/make 03. 所有分類轉成 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 所有分類轉成 HTML
     * @apiName PutCategoryAllMake2Html
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.success_category_ids        分類 IDs
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "make success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "success_category_ids": ["36946497446744064"]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "html file error",
    "data": {
        "fail_category_ids": ["36946497446744064"]
    }
}
     *
     */


    /**
     * 所有分類同步 HTML
     *
     * @api {PUT} /tool/category/all/html/sync 04. 所有分類同步 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 所有分類同步 HTML
     * @apiName PutCategoryAllSyncHtml
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.success_category_ids        分類 IDs
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "sync success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "success_category_ids": ["36946497446744064"]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "html_made error",
    "data": {
        "fail_category_ids": ["36946497446744064"]
    }
}
     *
     */


// ===
    /**
     * 單一文章轉成 HTML
     *
     * @api {PUT} /tool/post/{id}/html/make 05. 文章轉成 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 單一文章轉成 HTML
     * @apiName PutPostMake2Html
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id                     文章 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.post_id        文章 ID
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "make success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "html file error"
}
     *
     */


    /**
     * 單一文章同步 HTML
     *
     * @api {PUT} /tool/post/{id}/html/sync 06. 文章同步 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 單一文章同步 HTML
     * @apiName PutPostSyncHtml
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                           id                     文章 ID
     *
     * @apiParamExample {json} Request
{
    "id" : "36946497446744064",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.post_id        文章 ID
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "sync success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "post_id": "36946497446744064"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "html_made error"
}
     *
     */


    /**
     * 所有文章轉成 HTML
     *
     * @api {PUT} /tool/post/all/html/make 07. 所有文章轉成 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 所有文章轉成 HTML
     * @apiName PutPostAllMake2Html
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.success_post_ids        文章 IDs
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "make success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "success_post_ids": ["36946497446744064"]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "html file error",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "fail_post_ids": ["36946497446744064"]
    }
}
     *
     */


    /**
     * 所有文章同步 HTML
     *
     * @api {PUT} /tool/post/all/html/sync 08. 所有文章同步 HTML
     * @apiVersion 0.1.0
     * @apiDescription ・ 所有文章同步 HTML
     * @apiName PutPostAllSyncHtml
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.success_post_ids        文章 IDs
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "sync success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "success_post_ids": ["36946497446744064"]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "html_made error",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "fail_post_ids": ["36946497446744064"]
    }
}
     *
     */



// ===
    /**
     * 產生 htaccess file
     *
     * @api {PUT} /tool/htaccess/make 09. 產生 htaccess file
     * @apiVersion 0.1.0
     * @apiDescription ・ 產生 htaccess file
     * @apiName PutToolMakeHtaccess
     * @apiGroup Tool
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     *
     * @apiSuccessExample {json}    Response: 202
{
    "status": "success",
    "code": 202,
    "comment": "make success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session empty"
}
     *
     * @apiErrorExample {json}  Response: 410.01
{
    "status": "fail",
    "code": 410,
    "comment": "session is NOT alive"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "post error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "category error"
}
     *
     */



