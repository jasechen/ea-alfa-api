<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;


class PostViewLog extends Model
{

    protected $connection = 'log';
    protected $readConnection = 'log-read';

    protected $table = "post_view_logs";

    // protected $fillable = [];

    protected $guarded = ['created_at', 'updated_at'];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
}
