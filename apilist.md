#### \# Something Before Using API

+ **SESSION**

> 1. 使用所有 API，必須在 Request 的 header 傳 `session`，來辨別是哪次傳輸。
> 2. 目前預設 `session` 存活時間為 86400 秒 (24 hours)，所以當第一次或 `session` 已過期，須使用 `/session/init` 來產生及得到 `session`。


#### \# AUTHOR

| No. | Route        | Method | Header Params | Body Params         | Permission | Description           |
|:---:|:-------------|:------:|:--------------|:--------------------|:-----------|:----------------------|
| 01. | /author/{id} |  GET   | session       | id                  |            | Find the author.      |
| 02. | /author/list /{order_way?}/{page?} |  GET   | session       | [order_by] / [page] |            | Find the author list. |
| 03. | /author/html/make |  PUT   | session       |   |            | Make the htaccess file.                      |


#### \# CATEGORY

| No. | Route          | Method | Header Params | Body Params                                                                                                                                                        | Permission | Description              |
|:---:|:---------------|:------:|:--------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|:-------------------------|
| 01. | /category      |  POST  | session       | name / description / cover / slug / excerpt / og\_title / og\_description / meta\_title / meta\_description / cover\_title / cover\_alt                            |            | Create the category.     |
| 02. | /category/{id} |  PUT   | session       | id / [name] / [description] / [cover] / [slug] / [excerpt] / [og\_title] / [og\_description] / [meta\_title] / [meta\_description] / [cover\_title] / [cover\_alt] |            | Update the category.     |
| 03. | /category/{id} | DELETE | session       | id                                                                                                                                                                 |            | Delete the category.     |
| 04. | /category/{id} |  GET   | session       | id                                                                                                                                                                 |            | Find the category by id. |
| 05. | /category/list /{order_way?}/{page?} |  GET   | session       | [order_by] / [page]                                                                                                                                                |            | Find the category list.  |
| 06. | /category/{id}/html/make |  PUT   | session       | id  |            | Make the category to HTML page.                      |
| 07. | /category/{id}/html/sync |  PUT   | session       | id  |            | Sync the category HTML to Web Server.                |


#### \# FILE

| No. | Route | Method | Header Params | Body Params                      | Permission | Description      |
|:---:|:------|:------:|:--------------|:---------------------------------|:-----------|:-----------------|
| 01. | /file |  POST  | session       | [parent\_type] / [status] / file |            | Upload the file. |


#### \# LOG

| No. | Route          | Method | Header Params | Body Params | Permission | Description              |
|:---:|:---------------|:------:|:--------------|:------------|:-----------|:-------------------------|
| 01. | /log/click     |  POST  | session       | code        |            | Log the click count.     |
| 02. | /log/view/post |  POST  | session       | post_id     |            | Log the post view count. |


#### \# POST

| No. | Route                                                              | Method | Header Params | Body Params                                                                                                                                                                                              | Permission | Description                                      |
|:---:|:-------------------------------------------------------------------|:------:|:--------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|:-------------------------------------------------|
| 01. | /post                                                              |  POST  | session       | title / content / cover / [status] / author_id / category_id / slug / excerpt / og\_title / og\_description / meta\_title / meta\_description / cover\_title / cover\_alt                                |            | Create the post.                                 |
| 02. | /post/{id}                                                         |  PUT   | session       | id / [title] / [content] / [cover] / [status] / [author_id] / [category_id] / [slug] / [excerpt] / [og\_title] / [og\_description] / [meta\_title] / [meta\_description] / [cover\_title] / [cover\_alt] |            | Update the post.                                 |
| 03. | /post/{id}                                                         | DELETE | session       | id                                                                                                                                                                                                       |            | Delete the post.                                 |
| 04. | /post/{id}                                                         |  GET   | session       | id                                                                                                                                                                                                       |            | Find the post by id.                             |
| 05. | /post/list /{order_way?}/{page?}                                      |  GET   | session       | [order_way] / [page]                                                                                                                                                                                     |            | Find the syned post list.                              |
| 06. | /post/slug/{slug}                                                  |  GET   | session       | slug                                                                                                                                                                                                     |            | Find the post by slug.                           |
| 07. | /post/author/{id} /{order_way?}/{page?}                               |  GET   | session       | id / [order_way] / [page]                                                                                                                                                                                |            | Find the syned post list by author id.                 |
| 08. | /post/category/{id} /{order_way?}/{page?}                             |  GET   | session       | id / [order_way] / [page]                                                                                                                                                                                |            | Find the syned post list by category id.               |
| 09. | /post/author/category/{author_id}/{category_id} /{order_way?}/{page?} |  GET   | session       | author_id / category_id / [order_way] / [page]                                                                                                                                                           |            | Find the syned post list by author id and category id. |
| 10. | /post/{id}/html/make                                               |  PUT   | session       | id                                                                                                                                                                                                       |            | Make the post to HTML page.                      |
| 11. | /post/{id}/html/sync                                               |  PUT   | session       | id                                                                                                                                                                                                       |            | Sync the post HTML to Web Server.                |
| 12. | /post/recommend/category/{id} /{order_way?}/{page?} |  GET   | session       | id / [order_way] / [page]                                                                                                                                                           |            | Find the recommended and syned post list by category id. |
| 13. | /post/category/slug/{slug} /{order_way?}/{page?}                             |  GET   | session       | slug / [order_way] / [page]                                                                                                                                                                                |            | Find the syned post list by category slug.               |
| 14. | /post/nimda/list /{order_way?}/{page?}                                      |  GET   | session       | [order_way] / [page]                                                                                                                                                                                     |            | Find the all post list.                              |


#### \# SEARCH

|  No.  | Route          | Method | Header Params     | Body Params | Permission | Description                                             |
|:-----:|----------------|:------:|-------------------|-------------|------------|---------------------------------------------------------|
|  01.  | /search/{term} |   GET  | session | term        |            | Find the post title and author fullname. |


#### \# SESSION

| No. | Route           | Method | Header Params | Body Params                                                                                         | Permission | Description       |
|:---:|:----------------|:------:|:--------------|:----------------------------------------------------------------------------------------------------|:-----------|:------------------|
| 01. | /session/init   |  POST  |               | device\_code / [device\_os] / [device\_type] / [device\_lang] / [device\_token] / [device\_channel] |            | Init the session. |
| 02. | /session/{code} |  GET   |               | code                                                                                                |            | Find the session. |


#### \# SUBSCRIBE

| No. | Route                  | Method | Header Params | Body Params | Permission | Description           |
|:---:|:-----------------------|:------:|:--------------|:------------|:-----------|:----------------------|
| 01. | /subscribe             |  POST  | session       | email       |            | Subscribe the E-Mail. |
| 02. | /subscribe/subscribers |  GET   | session       |             |            | Find the Subscribers. |


#### \# TOOL

| No. | Route          | Method | Header Params | Body Params                                                                                                                                                        | Permission | Description              |
|:---:|:---------------|:------:|:--------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|:-------------------------|
| 01. | /tool/category/{id}/html/make |  PUT   | session       | id  |            | Make the category to HTML page.                      |
| 02. | /tool/category/{id}/html/sync |  PUT   | session       | id  |            | Sync the category HTML to Web Server.                |
| 03. | /tool/category/all/html/make |  PUT   | session       |   |            | Make the all category to HTML page.                      |
| 04. | /tool/category/all/html/sync |  PUT   | session       |   |            | Sync the all category HTML to Web Server.                |
| 05. | /tool/post/{id}/html/make  |  PUT   | session       | id  |            | Make the post to HTML page.                      |
| 06. | /tool/post/{id}/html/sync   |  PUT   | session       | id     |            | Sync the post HTML to Web Server.                |
| 07. | /tool/post/all/html/make  |  PUT   | session       |   |            | Make the all post to HTML page.                      |
| 08. | /tool/post/all/html/sync   |  PUT   | session       |      |            | Sync the all post HTML to Web Server.                |

