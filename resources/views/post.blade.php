<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />

        <title>{{ $title }} - {{ $site_name }}</title>
        <meta name="title" content="{{ $meta_title }}" />
        <meta name="description" content="{{ $meta_description }}">
        <meta name="robots" content="noodp" />
        <link rel="canonical" href="{{ $canonical_url }}" />

        <meta property="fb:admins" content="100000010603810">
        <meta property="fb:page_id" content="985568048148380">
        <meta property="fb:app_id" content="1808465192748376">
        <meta property="og:stie_name" content="{{ $site_name }}">
        <meta property="og:title" content="{{ $og_title }}" />
        <meta property="og:description" content="{{ $og_description }}" />

        <meta property="og:url" content="{{ env('WEB_URL') }}{{ $url_path }}" />
        <meta property="og:image" content="@isset($cover_links['o']){{ env('WEB_URL') }}/{{ $cover_links['o'] }}@endisset" />
        <meta property="og:type" content="website" />
        <meta property="twitter:site" content="{{ $site_name }}">

        <link rel="stylesheet" href="app?nm=style" />

    </head>

    <body>

        <div class="app">
            <view-header></view-header>
            <div class="container" :class="{richview:isRichview,essayview:isBoardShow}">
                <div class="board">
                    <a href="/" /> 首頁 </a>
                    <a href="{{ $category['url_path'] }}">{{ $category['name'] }}</a>
                    <a>{{ $title }}</a>
                    <article>

    <h1>{{ $title }}</h1>
    <aside>
<img src="@isset($author['avatar_links']['o']){{ env('WEB_URL') }}/{{ $author['avatar_links']['o'] }}@endisset" />
<div class="board__author">{{ $author['fullname'] }}</div>
<div class="board__info">{{ $author['bio'] }}</div>
<time>{{ $created_at }}</time>
    </aside>
    <picture>
        <img src="@isset($cover_links['o']){{ env('WEB_URL') }}/{{ $cover_links['o'] }}@endisset" />
    </picture>
    <p>@php
        echo html_entity_decode(str_replace('""','"',$content))
    @endphp</p>
    <aside>
<img src="@isset($author['avatar_links']['o']){{ env('WEB_URL') }}/{{ $author['avatar_links']['o'] }}@endisset" />
<div class="board__author">{{ $author['fullname'] }}</div>
<div class="board__info">{{ $author['bio'] }}</div>
    </aside>

                    </article>
                </div>
                <view-grid v-if="isGridShow"></view-grid>
            </div>
            <view-crm></view-crm>
        </div>

    </body>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
    <script src="app?nm=script"></script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "{{ env('WEB_URL') }}",
      "name": "首頁",
      "image": ""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{ env('WEB_URL') }}{{ $category['url_path'] }}",
      "name": "{{ $category['name'] }}",
      "image": "@isset($category['cover_links']['o']){{ env('WEB_URL') }}/{{ $category['cover_links']['o'] }}@endisset"
    }
  },{
    "@type": "ListItem",
    "position": 3,
    "item": {
      "@id": "{{ env('WEB_URL') }}{{ $url_path }}",
      "name": "{{ $title }}",
      "image": "@isset($cover_links['o']){{ env('WEB_URL') }}/{{ $cover_links['o'] }}@endisset"
    }
  }]
}
</script>
</html>