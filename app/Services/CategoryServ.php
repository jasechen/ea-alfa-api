<?php

namespace App\Services;

use App\Repositories\CategoryRepo;
use App\Repositories\CategorySeoRepo;

use App\Services\FileServ;

/**
 * Class CategoryServ
 *
 * @package namespace App\Services;
 */
class CategoryServ
{


    public function __construct()
    {

        $this->categoryRepo = new CategoryRepo();
        $this->categorySeoRepo = new CategorySeoRepo();

        $this->fileServ = new FileServ();
    } // END function


    /*
     * updateNumPosts
     *
     * @param $categoryId
     * @param $num
     *
     * @return
     */
    public function updateNumPosts($categoryId, $num = 1)
    {

        $datum = $this->findById($categoryId);

        if ($datum->isEmpty()) {
            return false;
        } // END if

        $updateData  = ['num_posts' => intval($datum->first()->num_posts) + $num];
        $updateWhere = ['id' => $categoryId];

        return $this->update($updateData, $updateWhere);
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->categoryRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->categoryRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $description
     *
     * @return
     */
    public function create($name, $cover = '', $status = '')
    {
        $data = ['name' => $name,
                 'cover' => $cover
        ];

        if (!empty($status)) {
            $data['status'] = $status;
        }

        return $this->categoryRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($status = '', $orderby = [], $page = -1, $numItems = 10)
    {
        $bindValues = [];

        $query  = "SELECT c.*, ";
        $query .= "s.slug, s.excerpt, s.og_title, s.og_description, s.meta_title, s.meta_description, s.cover_title, s.cover_alt ";
        $query .= "FROM categories AS c ";
        $query .= "LEFT JOIN category_seos AS s ON s.category_id = c.id ";
        $query .= "WHERE 1 ";

        if (!empty($status)) {
            $query .= "AND c.status = :status ";

            $bindValues['status'] = $status;
        } // END if

        if (!empty($orderby)) {
            $i = 0;
            foreach ($orderby as $column => $direction) {
                $query .= ($i == 0) ? "ORDER BY " : ", ";
                $query .= $column . " " . strtoupper($direction) . " ";

                $i++;
            } // END foreach
        } // END if

        if ($page > 0) {
            $offset = ($page - 1) * $numItems;
            $query .= "LIMIT " . $offset. ", " . $numItems;
        } // END if

        return $this->categoryRepo->fetch($query, $bindValues);
    } // END function


    /*
     * findByName
     *
     * @param $name
     *
     * @return
     */
    public function findByName($name)
    {
        $where = ['name' => $name];
        $datum = $this->categoryRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoWhere = ['category_id' => $datum->first()->id];
        $seoDatum = $this->categorySeoRepo->fetchDatum($seoWhere);

        $datum->first()->slug    = $seoDatum->first()->slug;
        $datum->first()->excerpt = $seoDatum->first()->excerpt;
        $datum->first()->og_title       = $seoDatum->first()->og_title;
        $datum->first()->og_description = $seoDatum->first()->og_description;
        $datum->first()->meta_title       = $seoDatum->first()->meta_title;
        $datum->first()->meta_description = $seoDatum->first()->meta_description;
        $datum->first()->cover_title = $seoDatum->first()->cover_title;
        $datum->first()->cover_alt   = $seoDatum->first()->cover_alt;

        return $datum;
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];
        $datum = $this->categoryRepo->fetchDatum($where);

        if ($datum->isEmpty()) {
            return $datum;
        } // END if

        $seoWhere = ['category_id' => $id];
        $seoDatum = $this->categorySeoRepo->fetchDatum($seoWhere);

        $datum->first()->slug    = $seoDatum->first()->slug;
        $datum->first()->excerpt = $seoDatum->first()->excerpt;
        $datum->first()->og_title       = $seoDatum->first()->og_title;
        $datum->first()->og_description = $seoDatum->first()->og_description;
        $datum->first()->meta_title       = $seoDatum->first()->meta_title;
        $datum->first()->meta_description = $seoDatum->first()->meta_description;
        $datum->first()->cover_title = $seoDatum->first()->cover_title;
        $datum->first()->cover_alt   = $seoDatum->first()->cover_alt;

        return $datum;
    } // END function

}
