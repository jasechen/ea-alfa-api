<?php

namespace App\Services;

use App\Repositories\PostViewLogRepo;


/**
 * Class PostViewLogServ
 *
 * @package namespace App\Services;
 */
class PostViewLogServ
{


    public function __construct()
    {

        $this->postViewLogRepo = new PostViewLogRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postViewLogRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->postViewLogRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $name
     * @param $description
     *
     * @return
     */
    public function create($postId, $sessionId, $num = 1)
    {
        $data = ['post_id' => $postId,
                 'session_id' => $sessionId,
                 'num' => $num
        ];

        return $this->postViewLogRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 20)
    {
        return $this->postViewLogRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->postViewLogRepo->fetchDatum($where);
    } // END function

}
