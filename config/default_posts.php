<?php
$fields = [
  ['id' => '3','title' => '【英國留遊學】倫敦大學金匠學院','slug' => '英國留遊學-倫敦大學金匠學院','excerpt' => '"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。"','content' => '"<p>倫敦大學在台灣應該是個耳熟能詳的英國大學，畢竟民進黨的黨主席蔡英文女士就是畢業於倫敦大學的其中一個學院 &ndash; 倫敦政治經濟學院 （London School of Economics and Political Science），但是除了這個幾乎家喻戶曉的學院之外，其實聯邦制體系的倫敦大學還有其他專攻不同領域的院校，而我今天要介紹的就是專攻藝文與傳媒的金匠學院(Goldsmiths)。</p>
<p>如果你喜歡英國當代藝術，或是你有些許研究的話，你應該會有聽過The Young British Artists（簡稱YBAs）這個名稱，他們是一群崛起於80年代的藝術家們，並且在90年代在藝術界造成了一陣轟動，也深深影響了現在的當代藝術。在這群藝術家中有多位都是畢業於金匠學院的藝術科系，例如，在2007年推出全世界最貴的藝術作品 For the love of God的Damien Hirst跟以諷刺手法來表現兩性觀點的Sarah Lucas，他們都是金匠學院的校友。如果你喜歡時尚的話，你一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，你不知道的是，Vivian Westwood也是畢業於金匠學院的設計科系。另外，金匠學院的大眾傳播學系(Media and Communication)也是國際知名的學校，像韓國的導演也都曾來此就讀。可見金匠學院在藝文界真的是有舉足輕重的地位，也培育出了許多赫赫有名的人才。</p>
<p>在當年就是衝著這股濃濃藝文味，於是我選擇到了金匠學院，主修大眾傳播媒體，在就讀的期間也感受到了學校的優點與缺點，在這裡就跟可能有計畫來就讀的你分享一下身為校友的個人的經驗吧！</p>
<p>首先，有一件事你一定要先知道，在英國，大學的<strong>就讀時間大部分為三年</strong>，也就是讀完三年沒被當的話就能夠順利畢業，另外學校採用的是<strong>三學期制</strong>，跟在台灣熟悉的二學期制不太一樣，雖然這樣假期似乎看起來會比較多，但是其實課業量反而更繁重，因為相同的課程就變成要在較短的時間內學會，而且每一次的假期也就代表著一次的報告dead line，所以，想玩就必須好好做時間規劃，把時間規劃好，才能夠順利繳交報告跟同時享有假期啊！</p>
<p>另外，我覺得倫敦大學的學校，相對於倫敦藝術大學，會顯得比較著重於理論，因為我有認識的朋友是就讀於倫敦藝術大學，他們時常都是忙於繳交作品，個人覺得是比較偏向實作方面，但是在金匠學院他們是採取<strong>理論與實作並重</strong>的方式來教課，也就是說作品量的累積跟實作的經驗比起倫敦藝術大學的同學可能會有些落差，但是在理論上知識的基礎可能會更加深厚。會這樣著重理論，是因為他們認為在知識上有著較深厚的基礎就能夠培養出批判性的思考能力(critical thinking)，創造出來的作品會更有內涵並能利用學到的知識來引導觀者，藉以得到更大的共鳴。</p>
<p>在大眾傳播的實作課程中，你可以從八種不同的課程來做選擇，分別為：</p>
<p>Journalism</p>
<p>Photography</p>
<p>Filmmaking</p>
<p>Illustration</p>
<p>Script and Props</p>
<p>Animation</p>
<p>Radio</p>
<p>Interactive Media</p>
<p>在一年級的時候你可以選修四種實作課程，但是到了二年級只能選修兩種，到了三年級就必須專精於其中一項，應該是希望透過嘗試讓學生找到自己有興趣或是有潛能的課程，再加以訓練跟培養。</p>
<p>我一年級時嘗試了animation，photography，filmmaking跟illustration，二年級的時候選擇了動畫跟攝影，到三年級就專精於攝影。其中動畫課在一二年級的時候，我們學習到的是stop-motion的製作方式，還有如何製作story board，但是到了三年級會學習到使用After Effect這個軟體來製作動畫。filmmaking其實很有趣，你會跟著一個團隊一起合作，可以嘗試到像採訪陌生人，或是自己尋找演員、找場地拍攝，甚至是製作配樂等經驗。在攝影課我們使用的幾乎都是底片相機，學校都有設備可以出借，但是有規定的出借跟歸還時間。在課堂上你會學到關於攝影的理論跟實際操作器具的體驗，不只是相機的操作，還有燈光的調整，底片的沖洗等等。基本上每學年你都需要製作一個自己的project，主題很自由，老師可能會給你一個大方向，但是你可以自己決定主題去製作。</p>
<p>不過，專精於其中一項不代表你只能侷限自己做某一個課程，相反的，學校反而鼓勵學生互相合作、學習，甚至跟不同科系的學生也都能夠互相合作來籌畫活動，推行企劃。但是，這相互間的合作通常是要靠個人去籌劃跟聯繫，學校並不會當中間人或指導人，<strong>主動性的培養</strong>，我覺得這是臺灣跟國外學校的一個最主要的差別吧！老師們，可能會提出你可以籌辦活動的想法，但是做與不做就全權交給你去決定，如果要做了，你有問題都可以主動提出，他們也都會很樂意幫助你，但比較不會主動去督促你的進度。像上必修課也都是一樣的，基本上都是lecture接著上seminar，書籍都是一開學就發放給你，在上課前你必須自己先研讀，到學校講師在講課，講課的內容會以書籍裡的內容為基礎，然後延伸出其他更深更廣的理論，有問題再在seminar上提出討論，老師並不會帶著你唸課文，所以如果你事前預習沒做好，在講師講課的時候可能就已經被搞得一頭霧水了。</p>
<p>如果你在大學裡期待的是非常多的實作經驗，還有學校幫你選擇好的實習課程，那金匠大學可能不是屬於你的選擇，但是如果你想把底子學好跟喜歡挑戰非商業性的創作，靠自己去尋找實習機會，金匠大學就會是屬於你的不二選擇喔～</p>"','published' => '1','og_title' => '【英國留遊學】倫敦大學金匠學院','og_description' => '"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。"','meta_title' => '【英國留遊學】倫敦大學金匠學院','meta_description' => '"如果你喜歡時尚，一定知道巧妙把龐克元素引領入時尚圈的Vivian Westwood，Vivian Westwood也是畢業於金匠學院的設計系。可見金匠學院在藝文界真的有舉足輕重的地位，培育出了許多人才。"','canonical_url' => 'https://tw.english.agency/英國留遊學-倫敦大學金匠學院','feature_image_alt' => '倫敦大學金匠學院','feature_image_title' => '倫敦大學金匠學院','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5936e5fd272f4-md4GgEaQ5OiTRkalmVDFYKwk7OeSGw-1200x630.png','author_id' => '3','category_id' => '3','created_at' => '"2017-06-06 09:27:26"'],
  ['id' => '6','title' => '澳洲打工度假？出發前先學好英文！','slug' => '澳洲打工度假-出發前先學好英文','excerpt' => '大家在出國之前，一定要先了解基本英文單字用法，如此以來，才不會發生不愉快的誤解。','content' => '"<h2>存錢、學好英文、度假！但....</h2>
<p>我是Peggy，在澳洲的伯斯 打工度假 一年，時薪是21.61澳幣，工作存了二十萬元台幣。大學畢業以後我在桃園國際機場工作了一年，看到許多網友在網路上分享的都是打工度假的照片，在海邊嬉戲、在跟無尾熊互動或者是到美麗的景點拍照，所以我就決定要去澳洲打工度假了。本來我設定了三個目標，第一存錢，第二學好英文，第三好好度假。</p>
<p>到了澳洲在網路上很快就找到了胡蘿蔔農場的工作，工作內容主要是洗蘿蔔跟切蘿蔔，雖然同事們多半是講國語的，還是有少數的俄羅斯人講英文，所以我還是設定了將英文學好的目標，希望我每天下班可以看看書背背單字，結交些澳洲當地的朋友。沒想到勞力工作就讓我每天的體力用光了。</p>
<p>工作結束後連洗菜的力氣都沒有，隨便吃完飯，洗個澡，就想睡覺了，英文書根本沒有拿出來過。</p>
<p>&nbsp;</p>
<p><img title=""打工度假-袋鼠"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c359e54b86-9dv2cItPYNLucpI1UKc5nJ51hzdRWh-760x275.png"" alt=""打工度假-袋鼠"" width=""760"" height=""275"" /></p>
<h2>英文不好，工作老是跌跌撞撞....</h2>
<p>打工度假期間．英文對我來說一直是一個障礙，找工作怕怕，看合約更怕，簽約都要看個好幾遍，才敢怕怕的簽約。我常在想若是去澳洲打工度假前，英文就有一定基礎，就可以在澳洲從事飯店或者是餐廳的工作，這類薪資較高，又可跟各國人互動，一定很有意思，不像我現在這樣去了澳洲一年，英文完全沒進步。</p>
<p>&nbsp;</p>
<p>在澳洲時候我發現凡是英文不好的朋友，找工作只能仰賴台灣人介紹以及網路上的澳洲打工的版看工作，或是透過工作代辦或者是工頭，找到的幾乎都是勞力辛苦的第一級產業。辛苦賺來的錢，往往還要讓代辦抽成或者跟工頭分，都有點心酸。</p>
<p>&nbsp;</p>
<p>因此建議要前往澳洲打工度假的新鮮人，最好是先搞定你的英文，我在那邊認識些在餐飲業工作的韓國人都說他們是去菲律賓上過密集英語課程後，才來澳洲找工作。</p>
<p>&nbsp;</p>
<hr />
<h2>有了好英文，真的才能找到好的工作！</h2>
<p>也就是搞定了英文才來澳洲，所以工作比較好找，也不一定需要從事勞力的工作，避免職業傷害，像我現在右手的手腕，偶爾還會有痠痛的感覺，我猜大概是在澳洲洗蘿蔔的職業傷害，雖然我蒐集到了二簽，可以留在澳洲繼續打工，我還是決定回台灣了，因為繼續再從事勞力的工作是沒有意義的。</p>
<p>我滿喜歡到國外工作的感覺，不過如果我要再度去國外 打工度假 ，我會先去菲律賓上三個英語密集課程，搞定我的英文再出發。</p>"','published' => '1','og_title' => '澳洲打工度假？出發前先學好英文！','og_description' => '今天小編就要教教大家在出國之前，一定要先了解一些基本英文單字用法，大家也不要緊張，這些大都像是多益單字。如此以來，才不會發生不愉快的誤解。','meta_title' => '澳洲打工度假？出發前先學好英文！','meta_description' => '今天小編就要教教大家在出國之前，一定要先了解一些基本英文單字用法，大家也不要緊張，這些大都像是多益單字。如此以來，才不會發生不愉快的誤解。','canonical_url' => 'https://tw.english.agency/澳洲打工度假-出發前先學好英文','feature_image_alt' => '機場英文','feature_image_title' => '機場英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c3b753c261-rgMWrpyUBcl8g40c282tH60PYsqHHN-900x600.jpeg','author_id' => '6','category_id' => '','created_at' => '"2017-06-06 09:27:32"'],
  ['id' => '7','title' => '"英國留學生活 ，必做的四件事！"','slug' => '英國留學生活-必做的四件事','excerpt' => '去英國留學並不只有課堂學習而已！可以利用地利之便，從生活周遭學習，舉凡博物館、旅遊、bar、朋友聚會等等，無不都是陶冶性情、認識異文化的機會!','content' => '"<p>初來到英國留學的我，對於倫敦這座大城市並沒有甚麼概念，只大略從親友們口中得知這是一個非常適合設計人去進修的城市。由於抱持著念書的心情，因此一開始也稱不上相當興奮，帶著有點忐忑不安的心情懵懵懂懂的準備迎接一個新的開始。就在時光飛逝的第一年過去後，我也感受到自己越來越愛上這個城市，喜歡的原因有很多，在這裡的每個角落都充滿了活力與新鮮感，在我的英國留學生活中，每天接觸到的事物與刺激，總是鼓勵我擁有更旺盛的求知慾去探索更多的事。</p>
<h2>Bar Time</h2>
<p>在倫敦藝術學院就讀工業設計碩士，基本上一到五的時間都在校園裡面，不是和同學討論就是做研究。但班上同學總是有個默契，那就是每到周五一定要去學校裡的 BAR 喝一杯放鬆一下。</p>
<p>「酒吧」對於歐洲人來說，是一個很奇妙的場所，而且也非常重要，因為在歐洲的夜生活不像台灣這麼多元，沒有凌晨的午夜場電影，也沒有開到早上的清粥小菜，更沒有夜市。</p>
<p>所以 bar 就成為下班下課後和朋友小聚放鬆的地點，換句話來說也是一個非常重要的社交場合。在這個時候也是練習英文最好的時候，一方面結交新朋友，另一方面讓你有更多時間和自己本來的外國同學分享除了學校課業以外的東西，聊聊生活、聊聊自己國家的文化。喝酒好像就是一種生活裡的配件、交朋友的必備品、就算只是假日在草皮上曬曬太陽，也要配上啤酒，這才是它們享受的生活態度。</p>
<p><img title=""出國留學-酒吧"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c2fbddca21-o2KqH9Xhuhl1E3102laxxeGC2Tz4f9-940x540.jpeg"" alt=""出國留學-酒吧"" width=""940"" height=""540"" /></p>
<h2>美食文化交流</h2>
<p>美食文化交流也是一件非常有趣的活動，除了酒，美食是最容易和朋友一起分享的東西。我們常常會不定期舉辦 Culture exchange 的美食Party，參加的人要自備一道自己國家的美食，而從準備美食這件事和內容菜色也可以發現文化的特色和習慣，例如，愛爾蘭同就很會做 baileys (愛爾蘭奶酒)，印度同學真的很會做咖哩，韓國人其實也有不吃辣的，歐洲人比美國人還可以接受亞洲料理，甚至像是清蒸鱈魚一類的食物。更多的好處是還可以了解到不同的餐桌禮儀等等。</p>
<h2>藝術活動</h2>
<p>然而在英國生活的兩年，我始終堅守一個原則，那就是周末放假絕對不待在家裡面，盡量把握所有可以外出的時間，每個周末從我的私人清單裡安排一個展覽活動參加。</p>
<p>倫敦對於藝術人來說絕對是一個非常棒的城市，藝文活動多的驚人沒話說，絕大多數的博物館和藝廊甚至都是免費參觀。即便花上一年 365 天的時間，也沒辦法把所有博物館和藝廊逛完。</p>
<p>有趣的是，不同博物館在每個月會有一天museum night，顧名思義就是博物館會開放到晚上，民眾可以體驗「博物館驚魂夜」，而通常在那一天晚上各大博物館都會有特別活動與講座、WORKSHOP、音樂會等等。全部都免費參加，甚至還有機會與設計師或是藝術家面對面交流。</p>
<p>參觀與探索不熱門的藝廊和小型博物館，被我列為一個非常好的學習方式，從中可以培養自己對於不同領域的興趣，還可以提供很多靈感。</p>
<p>舉例來說，觀光書本裡沒有的 Hunterian museum 和 Grant museum 這兩個以生物標本和人類解剖學為主的博物館，是除了動物園外了解動物最好的地方，超過千百種以上的動物解剖標本讓你眼睛看到花，這裡不只周末有固定的講座可以參加，在館內都有專業的導覽人員和研究人員，可以把握任何時間和它們交流。簡直就是一個活生生的教課書。</p>
<p><img title=""出國留學-出國旅遊"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c2fc3a73ee-t9pXQECpQ0HZ3Yzn0knoCOR6kNcaTk-900x325.jpeg"" alt=""出國留學-出國旅遊"" width=""900"" height=""325"" /></p>
<h2>把握假期或校外交流到鄰近國家旅遊</h2>
<p>而出國留學最少不了的當然就是把握好放長假的時間出去附近國家繞繞，除了聖誕假期與暑假一定會安排自己到附近國家走走之外。我真的很推薦如果學校有類似校外交流的活動一定要參加，記得在復活節假期的時候參與了學校安排去義大利交流的小旅行，和班上同學們一起到義大利的各大知名設計公司和廠房參觀，像是 ALLESSI、KARTELL、汽車工廠、皮革家具等等，這絕對是一個非常值得的經驗。</p>
<p>不僅僅了解到實際業界的運作模式以及學習到真正產品的製作過程外，透過幾天下來的旅行，和同學之間的感情又更緊密了一些，每天回到旅館大家在房間裡開 Party 玩紙牌遊戲，還記得那時候剛好是我生日，大夥坐在義大利餐廳享用，餐廳剛好有位駐唱歌手，結果全場一起為我用義大利文唱生日歌，這樣的禮物我一輩子都不會忘記，重點是我還學了幾句義大利文。</p>
<p>或許就是因為能夠處在一個這麼有活力又多元文化的環境，造就了許多可能的創意。我在歐洲生活的日子，無時無刻都在吸收新的東西。對我來說，留學，不僅是在學校認真把自己的研究做好，而是透過在國外的生活，去發現自我的成長。學習不單只是學術上的學習，更多是來自於環境、人與生活，透過這些去認識更多的自己，與探索自我。</p>"','published' => '1','og_title' => '"英國留學生活 海外留學不是只有學，體驗多彩生活必做的四件事"','og_description' => '"在倫敦藝術學院就讀工業設計碩士，基本上一到五的時間都在校園裡面，不是和同學討論就是做研究。但班上同學總是有個默契，那就是每到周五一定要去學校裡的 BAR 喝一杯放鬆一下。"','meta_title' => '"英國留學生活 海外留學不是只有學，體驗多彩生活必做的四件事!"','meta_description' => '去英國留學並不只有課堂學習而已！可以利用地利之便，從生活周遭學習，舉凡博物館、旅遊、bar、朋友聚會等等，無不都是陶冶性情、認識異文化的機會!','canonical_url' => '"https://tw.english.agency/英國留學生活 -必做的四件事"','feature_image_alt' => '英國留學生活','feature_image_title' => '英國留學生活','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6863986a98f-CelCS8h7qZ7U9TE2u9h5VexhgpScRa-1200x611.jpeg','author_id' => '4','category_id' => '3','created_at' => '"2017-06-06 09:27:35"'],
  ['id' => '8','title' => '一封給即將啟程到海外留學的你的信','slug' => '一封給即將啟程到-海外留學-的你的信','excerpt' => '要出發的你，不管你抱持著怎樣的心情出發，都要好好珍惜這樣的機會！以下是我在英國留學得到的心得！','content' => '"<blockquote>
<p>給 所有即將出國進修的朋友們：</p>
<p>好多次身邊的朋友問我，如果再選擇一次你還會選擇出國嗎？你還會選擇去倫敦留學嗎？我的答案仍然是：會。人生難得有機會可以離開自己的舒適圈，到一個完全不同的文化環境裡生活，那是一個多麼可遇不可求的機會。回想起這一路走來，有許許多多的心得想分享給各位。尤其是剛要出發的你，不管你抱持著怎樣的心情出發，都要好好珍惜這樣的機會！以下是我在英國留學得到的心得，這幾件事是到海外留學絕對不能少的！</p>
</blockquote>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h1>把握每個周末假日與課餘時間</h1>
<p>這些時間可以好好地把他利用起來，下課以後到倫敦巷弄裡探索美麗的咖啡廳，去發現有趣的小店，周末假日就把自己當作半個觀光客，去挖掘自己所不知道的倫敦，參與當地人也會參與的活動，這些時間是最好觀察與體驗不同文化的空檔，所以千萬不要浪費的宅在家裡看劇或上網。</p>
<p><img title=""倫敦-風景"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c2b9744905-mjq60VhK1Z2LjW7WttDvlru2MVA9Bp-702x336.jpeg"" alt=""倫敦-風景"" width=""702"" height=""336"" /></p>
<h3>&nbsp;</h3>
<h3>多走路</h3>
<p>在倫敦生活的日子，我愛上走路，原因除了交通費昂貴以外，其實在市中心的地段地鐵站相連的距離都不算非常遙遠，我常常把走路當成逛街，一邊走一邊沿途記下這裡的風景、店家、餐廳、藝廊、咖啡廳或是酒吧有哪些。等到有需要的時候，我就可以很快地找到在那些地方有我需要的店家、或是有氣氛感覺不錯的咖啡廳，周末和朋友聚會就可以相約在那裏。而走路，確實是一個快速探索倫敦的好方法喔！</p>
<h3>紀錄事物</h3>
<p>學會記錄東西是我在英國留學時養成最值得驕傲的習慣，也是一個激發自己靈感的好方法，對於學設計的我來說，把生活中各種有趣的事物利用文字圖像等等記錄下來，都可以變成創作的泉源。而這也是學校一直以來鼓勵每個學生做的事情。同時，你也會更細心地去觀察你的生活周遭，而不是只專注於手上這個狹小的手機螢幕而已。</p>
<h3>學會獨處</h3>
<p>以前在台灣，身邊有很多朋友告訴我，他們出門一定要有朋友陪，無論看展覽還是逛街都要找同伴，不敢一個人坐在餐廳裡吃飯，覺得自己一人坐在咖啡廳裡喝咖啡很尷尬。但在國外，和自己相處的時間變得很多，我認為這是件好事，多出了很多時間和自己相處，學會認識自己，並且思考自己的人生。</p>
<h3>用力玩</h3>
<p>我相信不是每個人出國都有很足夠的錢可以花費在旅遊上，但我想說的是，如果你可以打工或是接點小案子賺外快，把這些錢當作旅遊的基金也是很棒的選擇。一趟遙遠的里程來到了地球另一邊，就好好把握距離歐洲其他國家都這麼近的日子，背上背包，買張機票或火車票，早上在倫敦TATE museum閒晃，下午就可以到巴黎的Tokyo palace看展。我還學會在旅程中自己安排所有事物，並且交到了不同國家的朋友。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<p><img title=""倫敦-景色"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c2b9ba4a17-pgDeiKAgKOTbC2h3k9OsF56deYnm31-702x336.jpeg"" alt=""倫敦-景色"" width=""702"" height=""336"" /></p>
<h1>用力發問</h1>
<p>發問這件事情的養成很重要，不僅是甚訓練自己主動參與及學習的良好態度，另外也展現出自己有自信與有想法的一面。在歐洲的教育文化裡，你若不主動發問，是不會有人來和你解釋或主動問你懂不懂，另一方面，你若不主動發表意見，也不會有人在意你。所以凡事都要學會主動積極，有想要的事情就要會爭取。這是一響很重要的課題，但對亞洲人來說總是一個難以跨越的障礙。</p>
<h3>痛苦並存</h3>
<p>當然，做每件事情都是一體兩面，出國留學有很多不為人知的辛酸，但每個人的抗壓程度與個性本來就不同，當初我在做畢業製作的時候可真的是痛苦的不得了，因為種種的因素，例如: 語言、環境不熟 (找材料很麻煩)、個人學習習慣 (太過被動)、學校校風、思考模式等等原因，讓我深深感覺其實出國念碩士真不簡單。現在想起來卻是陣陣懷念。這些痛苦早已忘記，只記得自己完成了那些事情，感到滿足。</p>
<h3>交朋友不是一種壓力</h3>
<p>出國前大家都知道，我們不要時常和會說中文的華人在一起，而要多多打入外國人的圈子裡。但說實話，其實人種並沒有任何的差別，一樣是交朋友，在台灣的時候，我們也不可能和班上的每一個人都交心；相同的，在國外也不需要要求自己和班上每一位同學都變成好朋友，個性度相投的人大有人在，不要為了和外國人打成一片而給自己甚大的壓力，能夠結交到兩三個交心的歐洲朋友時就已經很不錯了。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>做一個有自信的人</h2>
<p>最後我想要給各位的最後一個意見，也是我認為最重要的。&rdquo;做一個有自信的人&rdquo; 這是我在歐洲同學身上看到最值得學習的優點，也就因為對於自己的自信，讓他們往往可以很堅定地做出很多決定，並堅持地走在自己的路上，他們清楚知道自己的優點，並且找到適合的方法表現出來。不吝嗇展現自己的同時，相對地，也可以從他人身上得到很多回饋。</p>
<p>希望所有即將啟程留學的朋友們，甚至是正在海外讀書的你們，都能有所收穫，不虛此行！</p>"','published' => '1','og_title' => '"一封給即將啟程到 海外留學 的你的信"','og_description' => '要出發的你，不管你抱持著怎樣的心情出發，都要好好珍惜這樣的機會！以下是我在英國留學得到的心得！','meta_title' => '"一封給即將啟程到 海外留學 的你的信"','meta_description' => '要出發的你，不管你抱持著怎樣的心情出發，都要好好珍惜這樣的機會！以下是我在英國留學得到的心得！','canonical_url' => 'https://tw.english.agency/一封給即將啟程到-海外留學-的你的信','feature_image_alt' => '海外留學','feature_image_title' => '海外留學','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c2e5e41a09-mBfZRKQej1jnCvnSRB43Hmgmthy7hL-899x600.jpeg','author_id' => '6','category_id' => '3','created_at' => '"2017-06-06 09:27:36"'],
  ['id' => '9','title' => '七件出國留學後才真正明白的事！','slug' => '出國留學-後才真正明白的事','excerpt' => '留學這條路，有很多背後不為人知的事情不常被人拿來討論，有很多快樂的事情，但是我必須要說，也充滿了許多讓人內心糾結與難過的事，只是我從來沒有後悔自己走過這段路。','content' => '"<p>足行萬里路，勝讀萬卷書 &ndash; 書摘</p>
<p>留學這條路，有很多背後不為人知的事情不常被人拿來討論，有很多快樂的事情，但是我必須要說，也充滿了許多讓人內心糾結與難過的事，只是我從來沒有後悔自己走過這段路，反而心存感謝這段路給我的成長，讓我離找到真正的自己又更前進了一步，這是我的 英國留學經驗談 。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>我從來不是想像中的堅強，孤單寂寞原來是這麼一回事</h2>
<p>我從國中開始就不是住在家裡，高中在家裡住了三年，然後大學隨即到外地去讀書，從以前我就自認自己是一個堅強獨立的小孩，我不害怕自己打理一切，因為有任何不懂只要願意開口、願意學，總是會有辦法。</p>
<p>自己做家務、打掃、安排生活全都不是問題。但當我第一次意識到自己真的是「一個人」的時候，內心不猶地開始慌。在台灣，即使都是一個人處理事情，但是身邊的家人朋友幾乎是一通電話或是腳程分鐘以內的距離，就可以有個可靠讓你安心的肩膀。遠在英國的我沒有任何朋友親戚在這裡，相隔的不是只有八小時的時差還有十三小時的飛行里程數，語言也確實造成心理某些層面的阻礙，這種內心真正的孤單是我從來沒有體會過的。</p>
<p>但是我知道這是我選擇的路，每種選擇也都等於是一種放棄，當你做出 A 決定的時候，就要有勇氣接受放棄 B，以及其帶來的後果。但這種孤單寂寞也隨著融入當地生活、結交新朋友以後慢慢好轉。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<p>&nbsp;</p>
<h2>學習從「他人」開始</h2>
<p>相信留遊學過的朋友都知道，國外的教育方式和亞洲的確相差甚遠，從小的文化教導出的歐洲人也都各具特色，互動式居多的教學方法強調同儕之間的學習與自我思考的能力。我必須說學校給我的東西不多，反而是看其他同學在處理同一件事上面的不同方法與態度，讓我明白甚麼叫做亞洲人真的「想太多」。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>名言金句有其真意</h2>
<p>小學的時候我們就開始讀很多名言金句，甚至是很多思想家或是名人文學家說的經典名言，但這些話充其只是常住在我腦海裡面的某些句子，偶爾拿出來寫寫作文用。例如: 吃的苦中苦、方為人上人。這類型的老梗句子，確實是要在自己熬過一些困難與克服某些障礙以後方能體會含意的一句話。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>了解自己然後接受失敗與不完美的自己</h2>
<p>話說接受自己的失敗真不是一件容易的事，不管你本身是一位有自信的人又或是沒自信的人，要完全坦然的面對自己的失敗並且再站起來真是不容易。</p>
<p>但我要說，這件事是我在留學生活裡體會到最重要的事。無法接受自己的失敗有一個原因其實是因為不夠了解自己，而出國受教育卻讓我有了新的機會重新認識自己，你真的知道自己是甚麼樣子嗎?</p>
<p>或是清楚自己真正想要的東西是甚麼嗎?外國人的教育模式和環境讓每個人開始會思考自我價值，並且對自己提出許多質疑。很多問題是我過去從來不會問自己的，在這過程中當然非常煎熬，因為發現自己有許多不足的地方，或許是接觸的人多元了、環境變化更多樣了、這些種種因素打破了你舊有的思考模式。</p>
<p>讓你站在另一個角度去面對自己，才會看到自我缺點，並接受自己的不完美進而建立信心再站起來，將會在每一步有所成長。</p>
<p>以前對於哲學壓根不想碰，而我對於哲學的概念就僅限於老子: 清淨無為。</p>
<p>但不知道為什麼與許多英國留學生接觸後發現，無論甚麼不同的科系，英國的教授們常常會在課堂上提到許多哲學理念，包含設計與藝術領域。</p>
<p>在參觀過無數展覽以後也不乏發現許多作品運用靈感起於哲學思想，哲學既用在生活理念上，亦可用於創作與設計。然而這也是在我沒出國以前絕對不會想花心思探討的。原來存在主義、本我、非我的思想也可以是設計的守則之一。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>這裡不分出身高低，尋找自己的優點</h2>
<p>我常說念碩士就好像是花了兩年上了一堂人生的課，在英國只要你有能力才華，不分身分高低也不論你的職業或是專業，很多人願意欣賞你而且會直接讚美你，因此讓大多數的年輕人他們不會在意別人的眼光，只要有能力、有寶就會盡力表現出來，而大多數的人知道自己的優點，並且盡力發揮。</p>
<p>相反的我覺得部份的亞洲人會擔心很多，擔心自己這個不好那個不好，而錯失了許多機會，常常忘記回頭看看自己的優點在哪裡。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>顛覆固有思想，跳出框框，原來甚麼事情都可以做</h2>
<p>來了一趟歐洲，我才發現一些事情其實也不用如此堅持，一件事情總是有許多出路與辦法，因為接觸的多了，你知道在這七十五億地球人口裡並不是只有一百種職業，而後你才知道原來自己可以做的事情有這麼多，而且想做也不是不可能。我很清楚自己的想法被改變了，當然我再度強調，過程裡也有許多的迷惘，教授曾經告訴我，人在每個階段都在思考下一步要做甚麼，但是我們永遠不知道會發生甚麼事，你又何必把自己定位在一個別人給你定位的位子上呢? 雖然答案一直都很難確定，但至少這次我知道在這個階段我可以做的事情又多了一點，終於丟掉我手中那些頑固的牌。</p>"','published' => '1','og_title' => '七件，出國留學，後才真正明白的事！','og_description' => '留學這條路，有很多快樂與內心糾結與難過的事，只是我從來沒有後悔自己走過這段路。','meta_title' => '七件，出國留學，後才真正明白的事！','meta_description' => '學這條路，有很多背後不為人知的事情不常被人拿來討論，有很多快樂的事情，但是我必須要說，也充滿了許多讓人內心糾結與難過的事，只是我從來沒有後悔自己走過這段路，','canonical_url' => 'https://tw.english.agency/出國留學-後才真正明白的事','feature_image_alt' => '出國留學','feature_image_title' => '出國留學','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c2a8ac446f-q5cZoEH4i6r985MSsbYFlCWllJHc7G-1067x600.jpeg','author_id' => '1','category_id' => '3','created_at' => '"2017-06-06 09:27:37"'],
  ['id' => '12','title' => '進修飯店管理，讓你翻身不再當魯蛇？','slug' => '飯點管理-心得','excerpt' => '台灣的旅館業已成低薪苦海，你只能隨波逐流？一個女孩如何從飯店管理，走向國際高新？','content' => '"<h1>台灣的旅館業已成低薪苦海，你只能隨波逐流？</h1>
<p>韓劇《情定大飯店》當中看似光鮮亮麗的飯店人員，事實上真是如此嗎？與大家分享台灣女孩 Jenny 的飯店工作實錄，以及她改變職場人生的故事。</p>
<p>Jenny 去年夏天從五專觀光科畢業後，進入一間中型旅館擔任櫃台接待人員，旅館的客人多是港澳與陸客，她月薪 25 K，還需配合 12 小時制的輪班，一年下來，Jenny 覺得身心俱疲，加上陸客來台驟減，甚至聽到有被裁員的可能。看到新聞報導飯店服務業的 22K 苦澀「薪」情，Jenny 覺得那就是她與同學們的真實青貧寫照。</p>
<p>Jenny 想起她看美國電影《慾望城市》當中，四名女主角到杜拜的高級飯店住宿時，從check-in、旅遊諮詢，到房間、餐食等，提供旅客無微不至的住宿體驗，像這樣子在國際級飯店工作，正是她所嚮往。</p>
<p><strong>國際級飯店徵才的共同要求，就是良好外語溝通能力。</strong>除了流利的英文之外，同時需要跨國級飯店的工作經歷。Jenny 的英文不好，往往連面試的門檻都達不到，但現在本土旅館業的低薪、超時工作，加上客源減少的裁員危機，讓她開始認真思考，到底有什麼方法讓自己走出低薪困境？怎麼樣的投資才是最佳選擇呢？</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>全球認證＋國際經驗，手握一流飯店的高薪金鑰！</h2>
<p>Jenny 當初會選擇觀光科，就是因為喜歡跟人群接觸，喜歡幫助他人的成就感，而旅館飯店業不僅相對穩定，也有不同的發揮機會。某次她發揮巧思，為一位想求婚的男性旅客代訂精緻花束、安排驚喜，最終完美達成求婚任務，看著他們幸福洋溢的笑容，覺得那就是她莫大的動力。</p>
<p>Jenny 堅定她的飯店職涯之路，<strong>並且決定要用英文與國際經驗為自己開啟更多選擇</strong>，擺脫青貧一族。多方諮詢討論後，Jenny 決定選擇到加拿大多倫多進行為期 18 個月的進修課程，該課程由美國飯店協會國際證照（AH &amp; LA）認證，並含 9 個月實習。這樣一來，不僅拿到全球認證的 AH &amp; LA，還能同時獲得海外國際級飯店工作經歷，擁有畢業即就業的人才實力。</p>
<p>Jenny 順利完成飯店管理課程，並且在多倫多的艾美酒店（Le Meridien）擔任實習禮賓員。全英語環境下學習與工作經驗，她覺得自己脫胎換骨，擁有在跨國級飯店服務的自信與實力。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>國際經驗讓你成為飯店管理人才，開展海內外就業機會</h2>
<p>Jenny 一拿到結業證書便獲得美國的五星級飯店錄取，再三思考下，她還是想回台灣工作，有更多陪伴家人的機會。適逢各大國際飯店來台駐點的熱潮，包括寒舍艾美酒店、W 飯店集團等，為爭搶飯店管理人才，更開出 50K 起跳的高薪，讓 Jenny 決定申請徵選。</p>
<p>憑藉著 AH &amp; LA 證書、國際級飯店實習經驗，以及加拿大海外生活磨練出的應變與溝通能力，加上面試時流利的英文應答，讓 Jenny 打敗眾多競爭者，成功拿到該高級飯店管理職的聘約，她知道她的投資選擇沒有錯！</p>
<p>國際經驗是開拓職場之路的最佳選擇，正確的自我投資能讓更多的 Jenny 們揮別苦勞，找到工作的價值，譜出人生新樂章。</p>"','published' => '1','og_title' => '進修飯店管理，讓你翻身不再當魯蛇！','og_description' => '台灣的旅館業已成低薪苦海，你只能隨波逐流？進修飯店管理，讓你再次走向國際！','meta_title' => '飯店管理，讓你接軌國際！國際經驗讓你搭上高薪列車！','meta_description' => '台灣的旅館業已成低薪苦海，你只能隨波逐流？進修飯店管理，讓你再次走向國際！','canonical_url' => 'https://tw.english.agency/飯點管理-心得','feature_image_alt' => '美國飯店管理','feature_image_title' => '美國飯店管理','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5936e610353e5-tlzDjMLBZDPt2lDvXZS0ycMzo0y93Q-1200x630.png','author_id' => '3','category_id' => '1','created_at' => '"2017-06-06 09:27:46"'],
  ['id' => '13','title' => '倫敦郊區大學-瑪麗皇后大學行銷系','slug' => '英國留遊學-瑪麗皇后大學行銷系','excerpt' => '皇后瑪麗大學是一所位於東倫敦的大學，少數在倫敦還擁有⾃己校園和附設宿舍的學校。','content' => '"<div style=""font-size: 18px;"">皇后瑪麗大學是一所位於東倫敦的大學，它也是少數在倫敦還擁有自己校園和附設宿舍的學校。相較於都市的嘈雜擾攘，Mile End 這個地方比較多的是印度和巴基斯坦裔的外來移民，學校校園被旁邊的大片綠地及住宅區環繞，是個鬧中取靜而且購物便利又便宜的好地方。
<p>流經校園旁邊的運河偶爾會以開著長船的人們經過，運河邊的步道也都會有人悠閒的散步或慢跑，瑪麗皇后大學的學生宿舍恰好就坐落在運河邊，潺潺水聲配上公園廣闊的綠地，成了最美麗的一窗景色。</p>
<p>這所大學在倫敦有多個校區，主要是醫學院校區、主校區、法律學院校區，但是大部分的科系都是在主校區這裡。住在學校宿舍相當的方便，上放學時較不受倫敦時常出包的交通工具影響，研究生的宿舍也較雅靜獨立，是個很適合讀書的環境。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2 style=""font-size: 22px;""><strong><span style=""color: #008080;"">行銷學什麼？</span></strong></h2>
<p>瑪麗皇后大學的行銷系是倫敦大學聯盟中最早成立的行銷系，系上師資盡是些傑出的研究學者和商場前輩。還記得當年的德籍論文指導教授本身就是麥肯錫的資深顧問，有條理的嚴謹，思路清晰確實，跟著他，學到的更多的是思考的方式和做事的態度。而英國是一個精英主義的國家，教育的方式偏向「師父領進門，修行在個人」。</p>
<p>每周老師都會開立一排的書單，包含必讀的書籍文章以及選讀的論文期刊等等。而碩士班的授課方式分成兩個部分：一是兩個小時左右的演講(Lecture)、二是小組討論(Seminar)。</p>
<p>演講課有時候會與同院所的其他系一起上課，所以有可能一堂演講就坐滿了正個講堂一百多個人，而小組討論的規模較小，一班不會超過１５人，目的是讓學生能就當週的課程內容有著更深入的探討及討論。</p>
<p>至於行銷系的內容，因各個學校著種的導向不同，課程內容也不盡相同。瑪麗皇后大學的行銷課程是專門開給一些以前沒有相關商業背景的人的，因此課程比較偏向概念，管理學上的一些知識也會包括在課程當中，包括企業管理學、策略管理、國際行銷、顧客心理學以及品牌概論等等，算是非常豐富的課程。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2 style=""font-size: 22px;""><strong><span style=""color: #008080;"">留學是加速成長的催化劑</span></strong></h2>
<p>一個人隻身在異地留學，雖然相較於在異地工作沒有那麼辛苦，但是當一個研究生也不是一件簡單的事情。舉凡安排生活及學業的平衡、語言及文化上的弱勢以及不適應、看著外國同學三個小時就能完成的報告你必須花個幾天的時間準備潤飾、在外生病意外發生時孤立無援的情況. . . .這些都是在台灣這個我們既熟悉又舒適的環境中沒辦法體會的。</p>
<p>常常有人跟我說：「你們在國外玩的那的開⼼心，根本就過著很爽的生活吧。」我想對於這句話，只能說是肯定也是否定的。畢竟在我們的Facebook上或近況報告上，幾乎都是吃喝玩樂的照片。但想想，又有誰會把工作讀書的辛苦和在外地的孤苦一直同別人說呢？ 留學生活一定是苦樂參半的極值，樂是而這個苦是良藥苦口的那種苦。 讀過一次碩士寫過一次論文，就像生過一場瀕臨死亡的大病，痊癒了之後身體就會產生抵抗力，變得更強壯。︒當明確地知道自己有什麼樣的獨立成長之後，再怎麼辛苦也都值得了。</p>
</div>"','published' => '1','og_title' => '倫敦郊區大學-瑪麗皇后大學行銷系介紹','og_description' => '"皇后瑪麗大學是一所位於東倫敦的大學，它也是少數在倫敦還擁有⾃自⼰己校園和附設宿舍的學校。相較於都市的嘈雜擾攘，Mile End 這個地方比較多的是印度和巴基斯坦裔的外來移民，學校校園被旁邊的大片綠地及住宅區環繞，是個鬧中取靜⽽而且購物便利又便宜的好地方。"','meta_title' => '倫敦郊區大學-瑪麗皇后大學行銷系介紹','meta_description' => '"皇后瑪麗大學是一所位於東倫敦的大學，它也是少數在倫敦還擁有⾃自⼰己校園和附設宿舍的學校。相較於都市的嘈雜擾攘，Mile End 這個地方比較多的是印度和巴基斯坦裔的外來移民，學校校園被旁邊的大片綠地及住宅區環繞，是個鬧中取靜⽽而且購物便利又便宜的好地方。"','canonical_url' => 'https://tw.english.agency/英國留遊學-瑪麗皇后大學行銷系','feature_image_alt' => '瑪麗皇后大學行銷系','feature_image_title' => '瑪麗皇后大學行銷系','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59acf48cc0c93-s8OgsNSOIfhXDD9wW8qgofDTNf5V7T-5000x2625.png','author_id' => '2','category_id' => '3','created_at' => '"2017-06-06 09:27:52"'],
  ['id' => '14','title' => '『奧客』英文怎麼說？奧客英文整理！','slug' => '奧客-英文-整理','excerpt' => '不過想必坐過飛機的人一定都有碰到過機上其他乘客做出令人不愉悅的事的經驗，這些就是大家稱之為奧客的人我們就來看看國內外研究統計出的奧客行為有哪些吧！','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-weight: 400;"">雖然全球經濟不景氣，物價上漲、薪資不調，</span><span style=""font-weight: 400;"">但花錢坐飛機出國旅遊，</span><span style=""font-weight: 400;"">仍是大家津津樂道、犒賞自己辛苦工作的最佳選擇之一。</span><span style=""font-weight: 400;"">不過想必坐過飛機的人一定都有碰到過</span><span style=""font-weight: 400;"">機上其他乘客做出令人不愉悅的事的經驗，</span><span style=""font-weight: 400;"">這些就是大家稱之為奧客的人</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">(picky customer，snobbish customer)</span></span></p>
<p><span style=""font-weight: 400;"">我們就來看看國內外研究統計出的奧客行為有哪些吧！</span><span style=""font-weight: 400;"">也提醒自己下次出國時，不要做出這些行為喔！</span></p>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<hr />
<h2><strong>1.Reclining their seat so far back they are practically in your lap.</strong></h2>
<h3><strong>椅背太過後傾幾乎已經躺到你的大腿上了。</strong></h3>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">recline</span>這個字就是斜躺、斜頃的意思，<span style=""color: #2e8b57;"">lap </span>就是大腿的意思，</span><span style=""font-weight: 400;"">那飛機上還有以下這些令人不舒服的動作！</span></p>
<p>例句單字小補充：</p>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">take a yawn </span>打哈欠</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">snoring</span> 打呼</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">stretching </span>&nbsp;伸懶腰</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">burps/blech </span>打嗝</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">picking nose </span>&nbsp;挖鼻孔</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">bite one&rsquo;s nails </span>&nbsp;咬指甲</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">sniff/sniffle</span> &nbsp;吸鼻涕</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">crack chuckle</span> &nbsp;凹指甲</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">snap gun</span> 嚼口香糖</span></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2><strong>2.Placing their elbows firmly on the arm rests to leave no room for you.</strong></h2>
<h3><strong>兩手穩穩的放在扶手位上，你的手都不知道要放在哪裡了。</strong></h3>
<p><span style=""font-weight: 400;""> <span style=""color: #2e8b57;"">arm rest</span> 座位扶手，然而與一般樓梯的扶手英文則是&nbsp;<span style=""color: #2e8b57;"">railing / handrail</span>&nbsp;，</span><span style=""font-weight: 400;"">而我們中文裡常說圍籬、圍欄這類與其相似的建築則是&nbsp;<span style=""color: #2e8b57;"">fence</span></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2><strong>3.Talking loudly to their friends on the plane so you can&rsquo;t rest.</strong></h2>
<h4><strong>在機上跟朋友大聲喧嘩讓你無法休息。</strong></h4>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">除了大聲喧嘩，當耀形容一個人話多、喋喋不休的時候就可以用以下幾個單字：</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Chatty/Loquacious/garrulous/Voluble/Conversational</span></span></p>
<p><span style=""font-weight: 400;"">(健談的、話多的、喋喋不休的)</span></p>
<p>註: <span style=""color: #2e8b57;"">wordy/ verbose&nbsp;</span>這兩個字較常用於負面，表示他人廢話很多，而<span style=""color: #2e8b57;"">verbal</span>又常跟<span style=""color: #2e8b57;"">verbose</span>搞混，意思是訴諸文字的，而不單單只是口頭上的意思！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2><strong>4.Letting their kids scream and run wild on the plane.</strong></h2>
<h3><strong>縱容小孩在機上尖叫和亂跑。</strong></h3>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">run wild</span> 表示四處亂跑，<span style=""color: #2e8b57;"">wild</span> 做副詞時可以表達狂野的意思，</span><span style=""font-weight: 400;"">而跑、衝、小碎步、等等這類型的動作應文如下:</span></p>
<p><span style=""font-weight: 400;"">例句單字小補充(與jog 有關的單字):&nbsp;</span></p>
<ul>
<li><span style=""font-weight: 400;"">慢跑 &nbsp;<span style=""color: #2e8b57;"">go for a jog、go running、go jogging</span></span></li>
<li><span style=""font-weight: 400;"">小跑步 &nbsp;<span style=""color: #2e8b57;"">trot</span></span></li>
<li><span style=""font-weight: 400;"">小碎步 &nbsp;<span style=""color: #2e8b57;"">scurry、scamper、scuttle</span></span></li>
<li><span style=""font-weight: 400;"">飛奔 &nbsp;<span style=""color: #2e8b57;"">dart、make a dash</span></span></li>
<li><span style=""font-weight: 400;"">衝刺、全速跑 &nbsp;<span style=""color: #2e8b57;"">sprint</span></span></li>
<li><span style=""font-weight: 400;"">拔腿狂奔 &nbsp;<span style=""color: #2e8b57;"">break into a run</span></span></li>
<li><span style=""font-weight: 400;"">落跑 &nbsp;<span style=""color: #2e8b57;"">run away、run off</span></span></li>
<li><span style=""font-weight: 400;"">蹦蹦跳跳地跑 &nbsp;<span style=""color: #2e8b57;"">bound</span></span></li>
<li><span style=""font-weight: 400;"">原地跑 &nbsp;<span style=""color: #2e8b57;"">run in place、run on the spot</span></span></li>
<li><span style=""font-weight: 400;"">昂首闊步、大步走 &nbsp;<span style=""color: #2e8b57;"">stride</span></span></li>
</ul>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<p>&nbsp;</p>
<h2><strong>5. Carrying hand luggage that is clearly too big to leave no place for yours.</strong></h2>
<h3><strong>手提行李箱太大佔去別人的空間。</strong></h3>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Luggage</span>&nbsp;是行李箱、行李的意思，而各種類型的行李箱英文如下：</span></p>
<ul>
<li><span style=""color: #2e8b57;"">Carry-Ons </span>&nbsp;小型可拖運的行李箱。</li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Wheeled Duffel Bags</span> &nbsp;裝有輪子，可手提或手拉的大型行李袋。</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">rolling bad</span> 裝有輪子，可以拖拉的大型袋子。</span></li>
</ul>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<p>&nbsp;</p>
<h2><strong>6.Being rude to the cabin crew.</strong></h2>
<h3><strong>認為花錢的最大，對機上服務人員不禮貌。</strong></h3>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Cabin </span>就是機艙、客艙的意思，</span><span style=""font-weight: 400;"">而<span style=""color: #2e8b57;"">crew</span>泛稱做工作人員，而飛機內部、各種各艙的英文如下：</span></p>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">International Flight </span>&nbsp;國際班機 </span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Round-Trip Ticket </span>&nbsp;來回機票</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Business Class </span>&nbsp;商務客艙</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Domestic Flight </span>&nbsp;國內班機 </span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">One-Way Ticket </span>&nbsp;單程機票</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">First Class</span> &nbsp;頭等艙 </span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Economy Class </span>經濟艙 </span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Lavatory</span> &nbsp;盥洗室</span></li>
</ul>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2><strong>7.Consistantly getting up to retrieve things from the overhead lockers.</strong></h2>
<h3><strong>不斷起身從頂上行李置物櫃拿東西。</strong></h3>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">retrieve </span>這個字是取回的意思，讀音與<span style=""color: #2e8b57;"">retreat</span> 相似，</span><span style=""font-weight: 400;"">而置物櫃、衣櫃、等等這些櫃子英文如下:</span></p>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">closet </span>&nbsp;衣櫃(固定在牆上的)</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">dresser</span> &nbsp;有大面落地鏡子的衣櫃、梳妝台</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">nightstand </span>&nbsp;床頭几</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Vanity table</span> &nbsp;梳妝台</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">wardrobe</span> &nbsp;衣櫃</span></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;&nbsp;</h2>
<h2><strong>8.Eating heavy taste food，drinking too much，polishing the nails and combing hair.</strong></h2>
<h4><strong>吃味道重的食物、喝太多酒、塗指甲油和梳頭髮。</strong></h4>
<p>這些東西散發出來的氣味都很令人不悅。<span style=""font-weight: 400;"">談到塗指甲油～以下也是幾個較為常見的美妝動作！</span></p>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">nail polishing</span> 塗指甲油。</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">put on lipstick</span> 塗口紅。</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">trim</span> 修剪</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">a dye</span> 染髮</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">highlights</span> &nbsp;挑染</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">a hair care session</span> &nbsp;護髮 </span></li>
</ul>
</div>"','published' => '1','og_title' => '空姐專欄-奧客英文怎麼說?飛機上的常見的奧客行徑英文!','og_description' => '不過想必坐過飛機的人一定都有碰到過機上其他乘客做出令人不愉悅的事的經驗，這些就是大家稱之為奧客的人我們就來看看國內外研究統計出的奧客行為有哪些吧！','meta_title' => '空姐專欄-奧客英文怎麼說?飛機上的常見的奧客行徑英文!','meta_description' => '不過想必坐過飛機的人一定都有碰到過機上其他乘客做出令人不愉悅的事的經驗，這些就是大家稱之為奧客的人我們就來看看國內外研究統計出的奧客行為有哪些吧！','canonical_url' => 'https://tw.english.agency/奧客-英文-整理','feature_image_alt' => '奧客行徑英文','feature_image_title' => '奧客行徑英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599bd60cb9334-Djg9QIprhqzqyWWgHA7k0ceEtm2WLI-5035x2714.png','author_id' => '5','category_id' => '10','created_at' => '"2017-06-06 09:27:53"'],
  ['id' => '15','title' => '10句你一定要知道的實用英文諺語！','slug' => '英文諺語-整理','excerpt' => '在職場上無法侃侃而談是許多人進修英文的目的，今天小編的生活英文要來帶大家重新審視一些常用而且可以拉近點距離的實用英語喔！','content' => '"<div style=""font-size: 18px;"">
<p>在職場上無法侃侃而談是許多人進修英文的目的，今天小編的生活英文要來帶大家重新審視一些常用而且可以拉近點距離的實用英語喔！</p>
<p>英文諺語就跟中文成語一樣，都有其智慧，只是小時候大家還無法理解，現在長大了，多了點人生經驗，再回頭看這些諺語的時候，其實就能夠恍然大悟了喔，現在就一起來看看這些常見的進修英文吧！接下來，我們就來看看有那些實用常見的進修英語吧！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<span style=""color: #008080;""> 1. Advice when most needed is least heeded. 忠言逆耳</span></h2>
<p><span style=""color: #2e8b57;"">Advice</span>就是「勸告，忠告」的意思，<span style=""color: #2e8b57;"">most needed</span>就是「最需要」，而<span style=""color: #2e8b57;"">least heeded</span>是「最不被留意」的意思，這個句子的中文直翻就是「忠告在最需要的時候通常最不被留意」。其實如果大家回想一下我們對於所謂的「忠告」的態度，常常都是沒有很注意在聽，而且有的時候，就是不知道為什麼越是重要的忠告就越是聽不進去，所以其實這句諺語是在<strong>告訴那些自以為很厲害所以聽不進去別人建議的人說不要再那麼自以為是啦</strong>。</p>
<p>例句:</p>
<ul>
<li>A:<strong><span style=""color: #2e8b57;"">I broke up with Ben，he was cheating on me.</span></strong></li>
<li>B:<strong><span style=""color: #2e8b57;"">Didn&rsquo;t I tell you that he was totally not trustworthy?</span></strong></li>
<li>A:<strong> <span style=""color: #2e8b57;"">And he borrowed a lot of money from me，I don&rsquo;t think I can take them back.</span></strong></li>
<li>B:<strong> <span style=""color: #2e8b57;"">Well，advice when most needed is least heeded.</span></strong></li>
</ul>
<p>中文翻譯：</p>
<ul>
<li>A: 我跟Ben分手了，他竟然偷吃耶！</li>
<li>B: 我不是早就告訴你他根本不可靠嗎？</li>
<li>A: 而且他跟我借了好多錢，我想是要不回來了。</li>
<li>B: 忠言逆耳啊。</li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<span style=""color: #008080;"">2. Easier said than done. 出一張嘴</span></h2>
<p>這句話如果看字面上的意思呢？就是「說比做容易」，小編相信大家應該都覺得一件事情，一定是用嘴巴說的，比用做的容易很多，<span style=""color: #2e8b57;""> than</span>是「比，比較」的意思，<span style=""color: #2e8b57;"">easier</span>是<span style=""color: #2e8b57;"">easy</span>的比較級，所以也延伸出「<strong>用嘴巴說是很容易的一件事情，如果真的要去做的話，就不見得了</strong>」。</p>
<p>例句：</p>
<p><strong><span style=""color: #2e8b57;"">The doctor said that I should exercise more and eat less，but that&rsquo;s easier said than done.</span></strong> (醫生說我應該要做運動少吃，但是對於我來說，真是說比做容易啊！)</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<span style=""color: #008080;"">3. Everybody&rsquo;s business is nobody&rsquo;s business. 眾人之事無人管</span></h2>
<p><span style=""color: #2e8b57;"">Business</span>這個字除了有「商業」之外，也有「職責；本分」的意思喔，所以這句話的字面意思是說「人人的職責就是沒有人的職責」。其實意思就是說<strong>每個人的事情就等於沒有人的事情</strong>，大家想看看，如果某一件事情是大家都要負責的，通常這件事情到最後就是沒有人會負責，這句話是職場人士在進修英文時，值得學習的一句！</p>
<p>例句：</p>
<p><strong><span style=""color: #2e8b57;"">People just stopping cleaning the park since it&rsquo;s opened to the public，everybody&rsquo;s business is nobody&rsquo;s business. </span></strong>(自從公園開放給大眾之後，就沒有人去清理了。真是眾人之事無人管啊。)</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<span style=""color: #008080;"">4. Every dog has his day. 風水輪流轉</span></h2>
<p><span style=""color: #2e8b57;""><strong> Every dog has its day</strong> </span>這句話的字面是說「讓每一隻狗也都有意氣風發的一天」，其實意思就是說，每個人都一定會有飛黃騰達的一天，或者也可以說是 &ldquo;每個人總會有實現自己願望的一天&rdquo;。</p>
<p>英文例句：</p>
<ul>
<li>A:<strong><span style=""color: #2e8b57;"">Did you hear something about Frank?</span></strong></li>
<li>B:<strong><span style=""color: #2e8b57;"">No，what&rsquo;s that?</span></strong></li>
<li>A:<strong><span style=""color: #2e8b57;"">Frank just hit a jackpot and won six million dollars.</span></strong></li>
<li>B: <strong><span style=""color: #2e8b57;"">Wow，hopefully I&rsquo;ll be the next lucky guy.</span></strong></li>
<li>A:<strong> <span style=""color: #2e8b57;"">Sure you will. Every dog has its day.</span></strong></li>
</ul>
<p>中文翻譯：</p>
<ul>
<li>A：你聽說過有關法蘭克的事了嗎？</li>
<li>B：沒有，怎麼了？</li>
<li>A：吉米中了大獎而且贏了六百萬元獎金。</li>
<li>B：哇，真希望我也可以這樣幸運。</li>
<li>A：你會的。 你也會有走運的一天。</li>
</ul>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<span style=""color: #008080;"">5. Half a loaf is better than none. 沒魚蝦也好</span></h2>
<p>這句諺語如果直接從字面上來翻譯的話，<strong>中文意思是「半條麵包總比沒有麵包好」</strong>，其實常用在規勸別人要接受眼前有的東西，也就是說一件事物如果沒有你所預期的好，或不如你希望中的多，最好還是要接受它，因為有總比沒有好。</p>
<p>英文例句：</p>
<ul>
<li>A:<strong><span style=""color: #2e8b57;"">I can\'t stand it anymore.</span></strong></li>
<li>B:<strong> <span style=""color: #2e8b57;"">What happened?</span></strong></li>
<li>A:<strong> <span style=""color: #2e8b57;"">My boss is so tightfisted. I want to quit!</span></strong></li>
<li>B:<strong> <span style=""color: #2e8b57;"">Didn\'t you just make a lot of money for them?</span></strong></li>
<li>A:<strong> <span style=""color: #2e8b57;"">Yes，so I asked for a pay raise from the company.</span></strong></li>
<li>B:<strong> <span style=""color: #2e8b57;"">How much did you ask for?</span></strong></li>
<li>A:<strong><span style=""color: #2e8b57;""> I asked for a 30 percent increase，but my boss was only willing to give me 10 percent.</span></strong></li>
<li>B:<strong><span style=""color: #2e8b57;"">Half a loaf is better than none.</span></strong></li>
<li>A:<strong> <span style=""color: #2e8b57;"">No，I don&rsquo;t think the company is worth slaving away anymore.</span></strong></li>
</ul>
<p><strong>&nbsp;</strong></p>
<p>中文翻譯：</p>
<ul>
<li>Ａ：我老闆太吝嗇了，我要辭職。</li>
<li>Ｂ：你不是幫公司賺了不少錢嗎？</li>
<li>Ａ：對阿，所以我要求加薪。</li>
<li>Ｂ：你要求多少呢？</li>
<li>Ａ：我要求３成，但是我老闆只願意給我一成。</li>
<li>Ｂ：沒魚蝦也好囉。</li>
<li>Ａ：不要，我覺得這家公司不值得我打拼下去了。</li>
</ul>
<p>以上介紹是小編在國高中最常見的諺語排行榜，不知道你的是那些呢？</p>
</div>"','published' => '1','og_title' => '"本週主編精選：在海外如何學好英文？十個在倫敦練習英文的方法｜ 英國留學經驗分享"','og_description' => '"十個在倫敦練習英文的方法｜ 英國留學經驗分享 倫敦是個能提供加強你的閱讀、口說和聽力能力的絕妙機會。這是作者的 英國留學經驗分享 ，推薦你十個能練習英文能力的好方法，不僅能增進能力，也可以認識新朋友和更加了解這個奇妙的城市。"','meta_title' => '『忠言逆耳』英文怎麼說？10句你一定要知道的英文諺語！','meta_description' => '"十個在倫敦練習英文的方法｜ 英國留學經驗分享 倫敦是個能提供加強你的閱讀、口說和聽力能力的絕妙機會。這是作者的 英國留學經驗分享 ，推薦你十個能練習英文能力的好方法，不僅能增進能力，也可以認識新朋友和更加了解這個奇妙的城市。"','canonical_url' => 'https://tw.english.agency/英文諺語-整理','feature_image_alt' => '如何學英文','feature_image_title' => '如何學英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593bd8e89cd07-BF8X8ymSJEPyYX1aAWQysBpEGXYcF6-1000x600.jpeg','author_id' => '7','category_id' => '10','created_at' => '"2017-06-06 09:27:56"'],
  ['id' => '17','title' => '「混血兒」的英文別說Mix！','slug' => '多益單字-混血兒的英文','excerpt' => '在台灣混血兒好像很稀有、珍貴，但其實在歐美國家，種族融合早就是一個見怪不怪!今天來教教大家多益單字，以下有幾個忠告，請各位好好記在小腦袋瓜裡！','content' => '"<div style=""font-size: 18px;"">
<p>在台灣混血兒好像很稀有、珍貴，但其實在歐美國家，種族融合早就是一個見怪不怪很普遍的現象了呢！ 舉美國為例，一直以來都有世界各地移民過去的人種，近幾年來量最大的前幾名是墨西哥、強國人、印度、菲律賓等移民，而在英國也有很多從其他印度、波蘭、巴基斯坦。</p>
<p>當你好奇一定要問外國人或混血兒的血統，可千萬要小心詢問喔～ 不然你可是會把氣氛搞得僵掉又顯得自己很沒見識，難堪到斃！ 今天來教教大家<strong><a href=""https://www.goeducation.tw/%E9%81%8A%E5%AD%B8/%E5%A4%9A%E7%9B%8A%E6%BA%96%E5%82%99-TOEIC%E6%BA%96%E5%82%99%E6%96%B9%E6%B3%95"" target=""_blank"" rel=""noopener noreferrer"">多益</a>單字</strong>，以下有幾個忠告，請各位好好記在小腦袋瓜裡！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2><strong>1. 糟糕的刻板印象No.1</strong></h2>
<p>「黑人不等於非洲人或美國人!!!」現在的台灣人應該比較不會那麼沒常識了&hellip;但還是要提醒一下看到像黑人的歪國人可千萬拜託不要問以下這個問題！</p>
<p>(<span style=""color: #ff0000;"">X</span>) <strong><span style=""color: #2e8b57;""> Are you from Africa/America?</span></strong> (你從非洲/美國來的嗎?)</p>
<p>&nbsp;</p>
<p>想問人家從哪裡來，就請不要亂假設好好地讓人家自己告訴你，其實英國、法國也都有很多當地黑人居民der，直接假設他們美國來的他們可是會火大的唷～ 用這句問就準沒錯！</p>
<p>(<span style=""color: #ff0000;"">O</span>) <strong><span style=""color: #2e8b57;""> Where are you from?</span></strong> (貴客您打哪兒來呢?)</p>
<p>&nbsp;</p>
<p><span style=""color: #ff0000;"">Q</span>：該怎麼形容、亞裔、非裔、等等僑鄉朋友呢?</p>
<p><span style=""color: #ff0000;"">A</span>：「原屬國籍」+「現在國籍」&nbsp;eg. African American 非裔美國人。(美國黑人)</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<strong>2.&nbsp;</strong><strong>糟糕的刻板印象No.2</strong></h2>
<p>&nbsp;</p>
<p>「黑人不一定都會唱饒舌或喜歡聽嘻哈！ 」也不要蠢蠢地以為所有黑人都會唱饒舌、聽嘻哈</p>
<p>(<span style=""color: #ff0000;"">X</span>) <strong><span style=""color: #2e8b57;""> Hey，you must be good at rapping! What hip-hop songs do you listen to?</span></strong>(ㄟ你一定很會唱饒舌！你都聽哪些嘻哈歌曲啊？)</p>
<p>這樣換來的可能不會是一個答案，而是一個大白眼甚至是一拳&hellip;</p>
<p>(<span style=""color: #ff0000;"">O</span>) <strong><span style=""color: #2e8b57;""> What music genres/singers do you like?</span></strong> (你喜歡哪種音樂類型/甚麼歌手？)</p>
<p>&nbsp;</p>
<p>這樣是不是好多了呢？以上會讓人感到不爽的程度呢，就好比一個老外一看你的黑髮黃皮膚，就用破中文問你說：「你中國來的？」「喔～你從台灣來？你每天都吃臭豆腐嗎？」這樣&hellip; 讓人覺得傻眼又瞎，不要讓這些笨話從你嘴裡冒出來&hellip; 加油，好嗎? :roll:<a href=""http://www.goeducation.com.tw/wp-content/uploads/2015/07/mix2.gif""><br /></a></p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<h2><strong>3. 混血兒到底怎麼說？</strong></h2>
<p>別輕易相信中英字典，用了不該用的字形容「混血兒」！！！否則下場悽慘啊&hellip;雖然現在網路上很多中翻英字典都很好用又好方便，但我真的不懂，為什麼裡面會放一些根本不該用的字！！！ (怒吼) 以下是網路中英字典可查到的「混血兒」英文，但求求你不要用拜託！！！</p>
<p>&nbsp;(<span style=""color: #ff0000;"">X</span>) &nbsp;<strong><span style=""color: #2e8b57;""> hybrid</span></strong>&nbsp;(混能車、雜種動植物)</p>
<p>&nbsp;(<span style=""color: #ff0000;"">X</span>) &nbsp;<strong><span style=""color: #2e8b57;""> mix</span></strong>&nbsp;&nbsp;(混種狗)</p>
<p>這兩個字是用來指人類以外的配種&hellip;用這些字來問人是不是混血兒&hellip; 你說是不是有點不想活了呢？&hellip; @@&rdquo;</p>
<p>&nbsp;</p>
<p>真正形容「混血兒」的字眼兒：</p>
<p>(<span style=""color: #ff0000;"">O</span>) <strong><span style=""color: #2e8b57;""> halfer</span></strong>&nbsp;&nbsp;&nbsp;混血兒 (n.) 口語、非正式</p>
<p><em>""<strong><span style=""color: #2e8b57;"">He\'s so hot. I think he\'s a halfer!</span></strong>"" (他好帥喔！我想他應該是混血兒喔！)</em></p>
<p>(<span style=""color: #ff0000;"">O</span>) <strong><span style=""color: #2e8b57;""> biracial</span></strong>&nbsp;&nbsp;&nbsp;混兩個種族的 (adj.)</p>
<p>(<span style=""color: #ff0000;"">O</span>)&nbsp;<strong><span style=""color: #2e8b57;""> multiracial</span></strong>&nbsp;&nbsp;&nbsp;混三個以上種族的(adj.)</p>
<p><em>&ldquo;<strong><span style=""color: #2e8b57;"">Are you biracial / multiracial?</span></strong>&rdquo; (你是混血兒嗎？)</em></p>
<p>&nbsp;</p>
<hr />
<h2><strong>4. &nbsp;mixed-race&nbsp;&nbsp;&nbsp;&nbsp;混不同種族的 (adj.) 口語、非正式</strong></h2>
<p><em><strong><span style=""color: #2e8b57;""> Wow，that mixed-race girl is so hot!&nbsp;</span></strong>(哇～那個混血女孩好正喔！)</em></p>
<p>不過坦白跟各位說啊，其實「混血兒」的地位，在我們亞洲人來講感覺好像是比較「尊貴、優越的」，但在歐美國家的話，可不是這麼回事喔！是個很敏感的話題！！！ 因為他們有種族優越論，自認為白人最優，所以其實問人家是不是混血兒這個問題，我想&hellip;建議還是不要亂問比較好&hellip;不然無論你怎麼問，其實都是很冒犯的一個問題！ 但你如果就是那麼堅持、頑固，一定要問到人家的血源背景，我只能提供你這個奧客一個最正式、最不會得罪人的問法：</p>
<p><strong>&nbsp;</strong>(<span style=""color: #ff0000;"">O</span>) <strong><span style=""color: #2e8b57;""> What&rsquo;s your ethnicity / ethnic background?&nbsp;</span></strong>(你的種族淵源/種族背景是什麼？)</p>
<p>&nbsp;</p>
<p>就如果你自以為是調查局的話，問這句應該是比問什麼都稍微不容易被海扁啦...</p>
<p>以上就是這次<strong>多益單字</strong>的教學～要學起來歐！</p>
</div>"','published' => '1','og_title' => '「混血兒」的英文別說Mix！','og_description' => '在台灣混血兒好像很稀有、珍貴，但其實在歐美國家，種族融合早就是一個見怪不怪!今天來教教大家多益單字，以下有幾個忠告，請各位好好記在小腦袋瓜裡！','meta_title' => '「混血兒」的英文別說Mix！3分鐘搞懂混血兒的英文怎麼說！','meta_description' => '在台灣混血兒好像很稀有、珍貴，但其實在歐美國家，種族融合早就是一個見怪不怪!今天來教教大家多益單字，以下有幾個忠告，請各位好好記在小腦袋瓜裡','canonical_url' => 'https://tw.english.agency/多益單字-混血兒的英文','feature_image_alt' => '混血兒的英文','feature_image_title' => '混血兒的英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593bf7f408b37-JIWHlONSZhQy73eJjyv6rEDGc2EUIg-951x551.jpeg','author_id' => '3','category_id' => '10','created_at' => '"2017-06-06 09:27:58"'],
  ['id' => '18','title' => '紅血球、白血球、血小板英文怎麼說？','slug' => '紅血球－白血球-英文怎麼說','excerpt' => '你知道身體裡這些細胞、人體常見的病症、器官的英文怎麼說嗎？有關人體的英文整理！','content' => '"<div style=""font-size: 18px;"">
<p>國中的時候上生物課，你有沒有常常被細胞，細胞膜還是細胞質給搞混啊？ 如果現在再看一次而且還是用英文來學習的話，應該可以學習地更多吧！今天我們要來看看生物課裡面必學的幾個單字，讓你英文也可以說出幾個很厲害的單字喔！一起來看看這些TOEIC準備必學的單字吧!</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<strong><span style=""color: #008080;"">1.紅血球/白血球/細胞</span></strong></h2>
<p>細胞基本上是所有生物的基本組成單位，它是除了病毒之外，最具有完整生命力的生物的最小單位喔，也有個很可愛的外號叫做「生命的積木」。而人體與細胞有關的詞還有以下這些，而這些詞彙都是TOEIC考試常考的單字：</p>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> red blood cell</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;紅血球</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> white blood cell</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;白血球</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> platelet</span></span><span style=""font-weight: 400;"">&nbsp;➡ 血栓、血小板</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> antibody</span><span style=""font-weight: 400;"">&nbsp;➡ 抗體</span></span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> artery </span>➡</span><span style=""font-weight: 400;"">&nbsp;動脈</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> vein </span>➡</span><span style=""font-weight: 400;"">&nbsp;血管 </span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> lymph </span>➡</span><span style=""font-weight: 400;"">&nbsp;淋巴</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> micro-organism</span> ➡</span><span style=""font-weight: 400;"">&nbsp;微生物</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> microscope</span> ➡</span><span style=""font-weight: 400;"">&nbsp;顯微鏡</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> nucleus </span>➡</span><span style=""font-weight: 400;"">&nbsp;細胞核</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> chromosome</span> ➡</span><span style=""font-weight: 400;"">&nbsp;染色體</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> plasma </span>➡</span><span style=""font-weight: 400;"">&nbsp;汗腺</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> capillary</span>➡</span><span style=""font-weight: 400;"">&nbsp;毛細管</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Human body is made of billions of cells.</span><span style=""font-weight: 400;"">&nbsp;➡ 人體是由數十億個細胞所組成的。</span></span></li>
</ul>
<p>&nbsp;</p>
<p><span style=""color: #00008b;"">文法小補充:</span></p>
<ul>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">A(大) be made of B(小)</span>=</span><span style=""font-weight: 400;"">A由B組成</span></li>
<li><span style=""font-weight: 400;"">例句：<span style=""color: #2e8b57;""> The pipes are be made of plastic .</span>（<span style=""font-weight: 400;"">這些管子是用塑料製作的。）</span></span></li>
</ul>
</ul>
<p><span style=""font-weight: 400;""><span style=""color: #00008b;"">補充:</span></span></p>
<ul>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> billions of+複數名詞 </span>&nbsp;➡ 數十億個的&hellip;.</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> thousands of + 複數名詞 </span>&nbsp;➡ 數以千計的&hellip;&hellip;</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> dozens of + 複數名詞 </span>➡&nbsp;數十的&hellip;&hellip;</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> hundreds of + 複數名詞 </span>➡&nbsp;數以百計的&hellip;&hellip;</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> tens of thousands of + 複數名詞</span>➡ 數以萬計的&hellip;&hellip;</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> hundreds of thousands of + 複數名詞</span> ➡ 數以十萬計的&hellip;&hellip;(由one hundred thousand『十萬』變化而成)</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> millions of + 複數名詞</span> &nbsp;➡ &nbsp;數以百萬計的&hellip;&hellip;</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> tens of millions of + 複數名詞</span>➡ 數以千萬計的&hellip;&hellip;(由ten million『千萬』變化而成)</span></li>
</ul>
</ul>
<h2>&nbsp;</h2>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<strong><span style=""color: #008080;"">2.人體常見臟器的英文</span></strong></h2>
<p>人體的臟器可以說是去國外看醫生必須一定要會的英文！而人體的臟器英文主要如下(PS.常見的臟器也都是多益考試常考的喔！)</p>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> internal organs</span> ➡</span><span style=""font-weight: 400;"">&nbsp;內臟</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> heart</span> ➡</span><span style=""font-weight: 400;"">&nbsp;心臟</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> blood vessel</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;血管</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> liver</span> ➡</span><span style=""font-weight: 400;"">&nbsp;肝臟</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> bile </span>➡</span><span style=""font-weight: 400;"">&nbsp;膽汁</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> gallbladder ➡</span><span style=""font-weight: 400;"">&nbsp;膽囊</span></span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> lung </span>➡</span><span style=""font-weight: 400;"">&nbsp;肺</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> spleen</span> ➡</span><span style=""font-weight: 400;"">&nbsp;脾臟</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> pancreas</span> ➡</span><span style=""font-weight: 400;"">&nbsp;胰腺</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> diaphragm </span>➡</span><span style=""font-weight: 400;"">&nbsp;橫隔膜</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> brain</span>➡</span><span style=""font-weight: 400;"">&nbsp;腦</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> cerebrum </span>➡</span><span style=""font-weight: 400;"">&nbsp;大腦</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> cerebellum</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;小腦</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> medulla</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;延腦</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> esophagus</span> ➡</span><span style=""font-weight: 400;"">&nbsp;食道</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> stomach</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;胃</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> small intestine </span>➡</span><span style=""font-weight: 400;"">&nbsp;小腸</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> large intestine</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;大腸</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> appendix</span> ➡</span><span style=""font-weight: 400;"">&nbsp;盲腸</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> appendicitis</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;盲腸炎</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> rectum</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;直腸</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> anus </span>➡</span><span style=""font-weight: 400;"">&nbsp;肛門</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> (have) diarrhea </span>➡</span><span style=""font-weight: 400;"">&nbsp;腹瀉</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> constipation</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;便秘</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> constipated</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;便秘的</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> kidney</span>➡</span><span style=""font-weight: 400;"">&nbsp;腎臟</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> bladder</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;膀胱</span></li>
</ul>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;<strong><span style=""color: #008080;"">3.常見的疾病</span></strong></h2>
<p><span style=""font-weight: 400;"">下方為人體常見的疾病英文，其中高血壓、低血壓、也都是多益考試中的常客喔！</span></p>
<ul>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Hypertension</span></span><span style=""font-weight: 400;"">&nbsp;➡ 高血壓</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Hypotension</span></span><span style=""font-weight: 400;"">&nbsp;➡ 低血壓</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Postural hypotension</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;体位性低血壓</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Stroke</span></span><span style=""font-weight: 400;"">&nbsp;➡ 腦中風</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Hypoglycemia</span></span><span style=""font-weight: 400;"">&nbsp;➡ 低血糖</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Dementia</span>➡</span><span style=""font-weight: 400;"">&nbsp;老人痴呆症</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Heart failure</span>➡</span><span style=""font-weight: 400;"">&nbsp;心臟衰竭</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Renal failure</span>➡</span><span style=""font-weight: 400;"">&nbsp;腎衰竭</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Hyper-cholesterolemia</span>➡</span><span style=""font-weight: 400;"">&nbsp;高膽固醇</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Hernia</span>&nbsp;➡</span><span style=""font-weight: 400;"">&nbsp;疝/小腸氣</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Scabies</span></span><span style=""font-weight: 400;"">&nbsp;➡&nbsp;疥瘡</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Bed sore</span></span><span style=""font-weight: 400;"">&nbsp;➡&nbsp;壓瘡</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Pneumonia</span>➡&nbsp;</span><span style=""font-weight: 400;"">肺炎</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Aspiration pneumonia</span>➡&nbsp;</span><span style=""font-weight: 400;"">吸入性肺炎</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Asthma</span>➡</span><span style=""font-weight: 400;"">&nbsp;哮喘</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Hemorrhoids</span>➡</span><span style=""font-weight: 400;"">&nbsp;痔瘡 </span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> cancer</span> ➡&nbsp;</span><span style=""font-weight: 400;"">癌症</span></li>
<li><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> Fracture</span>➡</span><span style=""font-weight: 400;"">&nbsp;骨折</span></li>
</ul>
</div>"','published' => '1','og_title' => '-紅血球、白血球、血小板英文怎麼說？你必須知道的人體英文！','og_description' => '"國中的時候上生物課，你有沒有常常被細胞，細胞膜還是細胞質給搞混啊? 如果現在再看一次而且還是用英文來學習的話，應該可以學習地更多吧！"','meta_title' => '-紅血球、白血球、血小板英文怎麼說？你必須知道的人體英文！','meta_description' => '"國中的時候上生物課，你有沒有常常被細胞，細胞膜還是細胞質給搞混啊？ 如果現在再看一次而且還是用英文來學習的話，應該可以學習地更多吧！"','canonical_url' => 'https://tw.english.agency/紅血球－白血球-英文怎麼說','feature_image_alt' => '人體英文','feature_image_title' => '人體英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593bf8fd70f73-FtLLDYXdX9GNc4i6NGzneF8ifkcEa8-900x600.jpeg','author_id' => '4','category_id' => '10','created_at' => '"2017-06-06 09:27:59"'],
  ['id' => '19','title' => '英文進修方案怎麼選？5種方案比較！','slug' => '英文進修方案分析','excerpt' => '到底哪種方式最適合自己？筆者在台灣以及國外嘗試過各種的英文進修方式，因而整理出幾個英文進修的學習方式比較給大家參考。','content' => '"<div style=""font-size: 18px;"">
<p>英文是國際共通語言，在台灣，我們能夠練習英文會話的機會並不多，然而根據統計，台灣人是世界上花最多心力在英文學習，平均效果又最不好的一群；職場上升遷或轉職的需求、海外升學的語言要求、國內大學畢業的英文最低標準、或者是最單純的因為興趣而進修等，這些都是需要英文的地方，但是，到底哪種方式最適合自己？筆者在台灣以及國外嘗試過各種英文進修方式，整理出來給大家參考，但是在開始分析之前，我的習慣還是先講結論：</p>
<p>&nbsp;</p>
<h3>&nbsp;</h3>
<h3><span style=""color: #00008b;""> 表1. 台灣人英文進修的選擇</span></h3>
<p>進修方式 師生比 課程特性 附加價值 缺點 學習時數/週 學費/小時</p>
<table style=""border 2px solid gray; cellpadding: 3;"">
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: left;""><strong>進修方式</strong></td>
<td style=""border: 1px solid gray; text-align: left;""><strong>師生比</strong></td>
<td style=""border: 1px solid gray; text-align: left;""><strong>課程特性</strong></td>
<td style=""border: 1px solid gray; text-align: left;""><strong>附加價值</strong></td>
<td style=""border: 1px solid gray; text-align: left;""><strong>缺點</strong></td>
<td style=""border: 1px solid gray; text-align: left;""><strong>學習時數/週</strong></td>
<td style=""border: 1px solid gray; text-align: left;""><strong>學費/小時</strong></td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">自修(含交外國朋友語言交換)</td>
<td style=""border: 1px solid gray; text-align: left;"">0：1</td>
<td style=""border: 1px solid gray; text-align: left;"">自己掌控時間與安排</td>
<td style=""border: 1px solid gray; text-align: left;"">無</td>
<td style=""border: 1px solid gray; text-align: left;"">自制力要很強</td>
<td style=""border: 1px solid gray; text-align: left;"">看自己安排</td>
<td style=""border: 1px solid gray; text-align: left;"">free</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">連鎖補習班</td>
<td style=""border: 1px solid gray; text-align: left;"">1：20</td>
<td style=""border: 1px solid gray; text-align: left;"">可重複上課</td>
<td style=""border: 1px solid gray; text-align: left;"">同班同學可討論</td>
<td style=""border: 1px solid gray; text-align: left;"">經營不善，倒閉事件頻傳</td>
<td style=""border: 1px solid gray; text-align: left;"">8小時以下</td>
<td style=""border: 1px solid gray; text-align: left;"">100-300</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">英美遊學</td>
<td style=""border: 1px solid gray; text-align: left;"">1：15</td>
<td style=""border: 1px solid gray; text-align: left;"">下課後時間安排在於個人</td>
<td style=""border: 1px solid gray; text-align: left;"">體驗國外城市特色&amp;旅遊</td>
<td style=""border: 1px solid gray; text-align: left;"">誘惑力大，可能玩過頭治安不佳，美國甚至有Gun Shot危險</td>
<td style=""border: 1px solid gray; text-align: left;"">15-25小時</td>
<td style=""border: 1px solid gray; text-align: left;"">500-600</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">菲律賓密集式英文</td>
<td style=""border: 1px solid gray; text-align: left;"">1：1</td>
<td style=""border: 1px solid gray; text-align: left;"">平日密集專心學習，吃住在學校，安全不用擔心</td>
<td style=""border: 1px solid gray; text-align: left;"">週末假期可以去度假或參觀景點</td>
<td style=""border: 1px solid gray; text-align: left;"">國人對菲賓刻板印象不佳</td>
<td style=""border: 1px solid gray; text-align: left;"">40-50小時</td>
<td style=""border: 1px solid gray; text-align: left;"">200-250</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">台灣家教</td>
<td style=""border: 1px solid gray; text-align: left;"">1：1</td>
<td style=""border: 1px solid gray; text-align: left;"">老師到府教學</td>
<td style=""border: 1px solid gray; text-align: left;"">輔導學生心理狀態</td>
<td style=""border: 1px solid gray; text-align: left;"">時間要遷就教家教老師</td>
<td style=""border: 1px solid gray; text-align: left;"">8小時以下</td>
<td style=""border: 1px solid gray; text-align: left;"">600-1000</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">外籍家教</td>
<td style=""border: 1px solid gray; text-align: left;"">1：1</td>
<td style=""border: 1px solid gray; text-align: left;"">台灣人普遍覺得外來的和尚會唸經</td>
<td style=""border: 1px solid gray; text-align: left;"">老師可分享國外見聞</td>
<td style=""border: 1px solid gray; text-align: left;"">外籍老師背景無法了解</td>
<td style=""border: 1px solid gray; text-align: left;"">8小時以下</td>
<td style=""border: 1px solid gray; text-align: left;"">1000-1500</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">線上英文</td>
<td style=""border: 1px solid gray; text-align: left;"">1：1</td>
<td style=""border: 1px solid gray; text-align: left;"">網路上課，與國外老師同步</td>
<td style=""border: 1px solid gray; text-align: left;"">想學再開電腦預約</td>
<td style=""border: 1px solid gray; text-align: left;"">網路不穩/師資不確認/教材不一</td>
<td style=""border: 1px solid gray; text-align: left;"">2-8小時</td>
<td style=""border: 1px solid gray; text-align: left;"">150-400</td>
</tr>
</tbody>
</table>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #008080;"">自修</span></h2>
<p>這是進修英文最便宜的方式，也是大家在準備多益最常見的方式，網路上分享自修英文方式的文章，多到我不想在這裡贅述了，總之自修的方法有很多種，但不論視聽廣播、看美劇、讀英文小說、交外國朋友，通通圍繞在一件事情，就是 ""定力&rdquo;，但選擇這個族群的人，相對於其他族群，一般來說學習的方式比較不是那麼的有系統，還有就是當遇到問題、困難、瓶頸時，想要解決心裡的疑惑，因為大部分的資源都不花錢，所以網路上尋找或是現實世界裡詢問別人，想得到滿意的答案必須要靠點運氣。</p>
<p>至於結交外國朋友聊天學英文，其實我個人是不反對，而且我也有這樣的朋友，但是這樣的語言交換學習方式，比較怕遇到的狀況是 ""I can&rsquo;t explain your questions，but usually we said like that！&rdquo; (意思是外國朋友解釋不出來原因，她只知道這個用法在她們自己英文的系統裡就是這樣用的)，筆者在學習上又是喜歡打破沙鍋的人，所以有時候遇到這種情形會很抓狂！</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #008080;"">連鎖補習班</span></h2>
<p>近來連鎖補習班通常是一般民眾英文進修、準備多益的選擇，但倒閉事件頻傳，包括連鎖補習班威爾斯美語(註一)、學承電腦(註二)等都在今年初無預警倒閉，這些補習班所瞄準的客源，大部分都是社會人士，有些補習班老師甚至遭欠薪數萬元，也有學生甚至表示自己是貸款購買終身課程，被騙錢讓他們感到既無助又憤怒；數百名學生也到北檢門口進行抗議，希望能夠討回應有的權益。筆者並沒有一竿子打翻一船人的意思，但是建議大家在挑選補習班時要注意自己的權益，以下幾點可以提供您參考：</p>
<p>1.補習班是否合法立案？</p>
<p>2.建管、消防是否符合規定？</p>
<p>3.課程是否符合需求？</p>
<p>4.保證班所履行的契約請詳細閱讀，未確認前不要輕易繳費。</p>
<p>5.是否登載收、退費規定？收費與退費是否合理？</p>
<p>希望選擇以補習作為加強自己實力的朋友們，都能順利找到適合自己的補習班與課程喔。</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #008080;"">英美遊學</span></h2>
<p>雖然說「來來來，來台大；去去去，去美國」這句話是很早期的觀念，然而不可否認，國內近年來所舉辦的大小各類型教育展，去到現場的民眾還是問美國的最多，但現在這個時間點去到美國，學費成本是比較貴的，去到歐洲學英文，也只有愛爾蘭與英國比較適合，但是生活費卻是這兩個國家又比美國貴，我們用一個簡單的表格來比較一下成本比較</p>
<h3><span style=""color: #00008b;"">表2. 英美加愛英文遊學費用比較簡表(註三)</span></h3>
<table>
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">國家</td>
<td style=""border: 1px solid gray; text-align: left;"">一個月學費</td>
<td style=""border: 1px solid gray; text-align: left;"">一個月食衣住行生活費</td>
<td style=""border: 1px solid gray; text-align: left;"">一個月總費用</td>
<td style=""border: 1px solid gray; text-align: left;"">上課時數</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">美國(註四)</td>
<td style=""border: 1px solid gray; text-align: left;"">NTD$3.5~6萬</td>
<td style=""border: 1px solid gray; text-align: left;"">NTD$2.25萬~$4.8萬</td>
<td style=""border: 1px solid gray; text-align: left;"">約5.75~10.8萬台幣</td>
<td style=""border: 1px solid gray; text-align: left;"">15~25小時/週</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">加拿大</td>
<td style=""border: 1px solid gray; text-align: left;"">NTD$3.2~4萬</td>
<td style=""border: 1px solid gray; text-align: left;"">NTD$2.2萬~$3萬</td>
<td style=""border: 1px solid gray; text-align: left;"">約5.4~7萬台幣</td>
<td style=""border: 1px solid gray; text-align: left;"">15~25小時/週</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">英國/愛爾蘭</td>
<td style=""border: 1px solid gray; text-align: left;"">NTD$3~5.5萬</td>
<td style=""border: 1px solid gray; text-align: left;"">NTD$5萬~6萬</td>
<td style=""border: 1px solid gray; text-align: left;"">約8~11.5萬台幣</td>
<td style=""border: 1px solid gray; text-align: left;"">20~25小時/週</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>英美愛加四國雖然成本高，但是對於真的想要感受西方文化的朋友們，前往這幾個國家，可以感受到當地真正的人文氣息，還有去到當地的景點參觀遊玩，在這一點上，是所有的選項當中，最能夠感受到原汁原味的English environments的選擇，比如筆者當年最喜歡的景點就是英國的格林威治天文臺，但是對於倫敦的交通，實在是不太敢恭維；堂妹去年在美國加州大學念書，因為有人持槍進入校園，他也從這件事經歷到了學校的標準SOP---馬上關門(可不是放狗喔)，當天所有課程暫停，第二天要不要上課，得看學校官網通知，這些事情都是沒經歷過，無法體驗的</p>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #008080;""> 菲律賓密集式英文</span></h2>
<p>如果筆者寫這篇文章的時間點在五年前，可能還不知道密集式英文是什麼東西，一般人對於菲律賓講英文這件事情應該不會太陌生，但是因為這個國家給台灣人之前的刻板印象並不太好，造成台灣消費者想要學英文，都不會把菲律賓當成選項；然而撇開這個因素來說，其實從客觀的角度來看，他的英文資源不會輸給其他的English speaking國家太多，近來來也漸漸走向英文進修的重要選擇之一。</p>
<p>當筆者在接觸到這個資訊的時候，最大的亮點是菲國的語言學校，大多數<strong><span style=""color: #2e8b57;""> 採用1對1的課程教學</span></strong>，這簡直就是家教班嘛，但是因為當地人工便宜，因此反映出來的學費，相較於英美遊學，相當划算，可是娛樂與旅遊的部分就不要期望太高，事實上因為學生都被統一要求住在學校宿舍當中，所以幾乎是吃喝拉撒睡+課程都在校園中完成，這個概念有點像是台灣部分強迫住校的私立高中那種感覺，<strong><span style=""color: #2e8b57;""> 每天 8~10 小時的課，等於一週會上到 40~50 小時，一個月的食衣住行+課程下來才大約 4~5 萬台幣之間，幾乎是在英國與美國的一半費用，上課時數又是英美的將近兩倍</span></strong>，理論上，全部吃下來，在菲律賓的學習，進步的會比較快，而且在這裡只要專心的念好英文就好，吃的有人準備。</p>
<p>筆者也曾對於菲律賓的師資有過疑問，比如老師的品質如何？口音會不會很重等？最後親自上過課，得出來的結論是---<strong><span style=""color: #2e8b57;""> 菲律賓老師講的英文，聽起來比美國的老布希總統講英文，要容易聽的懂</span></strong>，這件事情讓筆者大感意外；後來才知道，菲律賓是全世界最大的 call center 集中地，不少北美的企業，是把它們的電話服務專線外包給菲律賓在做的，為了不讓消費者發現他們是在跟菲律賓人講電話，這些 call center 的菲律賓人上工前，口音還要經過矯正，如果被投訴了，還要被扣薪水的，這裡的老師，很多人在學生時期，就去這種 call center 打過工，因此口音本身已經被矯正不少，再加上考到政府的教師執照，語言學校裡面還有一些英籍美籍教師幫忙訓練菲律賓老師的教學與口音，難怪菲律賓有這麼多語言學校可供消費者選擇，此外這些學校有的本身還是多益考試的考場，可以說是準備多益的最佳基地。</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #008080;"">台灣家教與外籍家教</span></h2>
<p>說真的，先讓筆者發個小小牢騷，筆者很小的時候曾經跟鄰居和請過一個家教，除了他每次期中考都要請一週的假，我們必須要配合他之外，其他的我早已忘記當年的感受了；近年來則是我擔任過別人的家教，深深地感覺到家教這行真的很難賺，消費者相信外來的和尚會唸經，我遇過要求試教先不給薪水的也有 (但是這個消費者請外國人試教卻有給薪水)，最後他雖然選擇我來教他，直到有一天他跟我提起，我才知道，之前他沒有選那個外籍老師的原因是，那位老外雖然是經由他朋友介紹的，但他是來台灣打工度假的，並且只有高中畢業，但試教過程中，他覺得老外沒有辦法很精確的解釋一些用語的規則，鐘點費還比較貴，相反的我解釋的還比較清楚，並且抓住他的需求，所以他選擇我 (沒錯！選我正解啦！)，我教了他一個月的英文，因為他要被公司調去新加坡，我們才中斷了主雇關係，但我真的覺得，找家教要靠一點運氣，溝通的方法跟學生能否接受有很大的關係</p>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #008080;"">線上英文</span></h2>
<p>線上英文目前外師的部分，我的了解是，各品牌為了壓低成本，近年來雇用菲律賓老師的部分有增加的趨勢，甚至整個系統都是菲律賓老師的也有，前面講過菲律賓老師的優勢是人工便宜，並且他們對英文的接觸從中小學就開始了，而且除了他們的國語(馬加祿語)之外，所有的課本與考卷都是英文編輯的，所以她們對英文的熟悉度本來就比較好，但是線上英文要考慮的，除了老師之外，還有網路品質，課程選擇、時段選擇是否方便等因素均需要考慮，因此如果想要選用線上英文來加強，<span style=""color: #2e8b57;""> 一定要選那種&rdquo;<strong>給予試聽課</strong>&rdquo;的線上英文品牌喔。</span></p>
<p>&nbsp;</p>
<p>參考資料</p>
<p>_________________________________</p>
<p>註一參考資料 :</p>
<p>http://news.ltn.com.tw/news/life/paper/950975</p>
<p>http://news.ltn.com.tw/news/society/breakingnews/1824378</p>
<p>&nbsp;</p>
<p>註二參考資料 :</p>
<p>http://www.ettoday.net/news/20160408/676996.htm</p>
<p>http://www.cna.com.tw/news/afe/201608270075-1.aspx</p>
<p>註三 本表的計算方式，學費資料來源為IH / ILSC / Embassy / Sprachcaffe 等世界連鎖型語言學校，生活費則是筆者個人當地生活經驗、當地台灣學生的個人經驗，並參照台灣駐各國經濟文化辦事處、各國移民局網站對於每月生活費的建議所表列出來</p>
<p>註四 食衣住行成本以美國來算，依照一線到三線城市不等，大約每個月是USD$100~700之間(換算台幣NTD$48000~$22400之間)</p>
</div>"','published' => '1','og_title' => '如何選擇好的英文進修方案','og_description' => '到底哪種方式最適合自己？筆者在台灣以及國外嘗試過各種的英文進修方式，因而整理出幾個英文進修的學習方式比較給大家參考。','meta_title' => '如何選擇好的英文進修方案？5種英文進修方案比較！','meta_description' => '到底哪種方式最適合自己？筆者在台灣以及國外嘗試過各種的英文進修方式，因而整理出幾個英文進修的學習方式比較給大家參考。','canonical_url' => 'https://tw.english.agency/英文進修方案分析','feature_image_alt' => '英文進修選擇','feature_image_title' => '英文進修選擇','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599ea3f85d0c6-0cGDEzPyOi1Ta74fbpaqDff0BZe2yw-5035x2714.png','author_id' => '6','category_id' => '10','created_at' => '"2017-06-06 09:28:02"'],
  ['id' => '20','title' => '30而立，培養英文讓你擁有更多籌碼','slug' => '英文進修為自己加分','excerpt' => '進修就是增加自我價值，給自己的最佳禮物。30歲的你，讓英文能力開啟人生更多選擇。','content' => '"<div style=""font-size: 18px;"">
<h2>30而立，進修讓你擁有更多籌碼</h2>
<p>學生時代的你，想像自己的 30 歲是什麼樣子呢？擁有一份穩定的工作，不錯的薪水，有一筆存款，開始計劃買車、買房。那麼，現在的你，是你當初想像的模樣嗎？你會說自己「而立」嗎？在物價高漲、薪水難漲的現在，你擁有讓你感到滿足的工作與生活嗎？</p>
<p><strong><span style=""color: #2e8b57;""> 進修就是增加自我價值，給自己的最佳禮物。</span></strong>根據統計，上班族最愛的進修課程依序是：語言、興趣才藝、資訊科技相關。有將近 2/3 的上班族選擇語言課程進修，具備好英文，不僅更容易進入外商或大型企業工作，也更容易獲得升遷、加薪的機會。</p>
<p><strong><span style=""color: #2e8b57;""> 30 歲的你，擁有更清晰的職涯藍圖，讓英文能力成為你的助力，進修為你開啟人生更多選擇。</span></strong></p>
<p>&nbsp;</p>
<hr />
<h2>菲律賓英文進修，最高CP值的國際語言學校</h2>
<p>說到英文進修，你會想到什麼？補習班、大學的推廣部，還是線上英文課程呢？<strong><span style=""color: #2e8b57;""> 國際間最熱門的英文進修，其實就在東協菲律賓！</span></strong>你沒看錯，日本、韓國更是每年有好幾萬人到菲律賓的語言學校學英文。</p>
<p>菲律賓語言學校上百間，地點從台灣人熟悉的馬尼拉、宿霧，延伸到碧瑤、克拉克等地，菲律賓可說是東協國家中的語言中心之冠，在菲國的語言中心教室裡，宛如是小型聯合國。為何菲律賓吸引這麼多外國人到此學英文呢？</p>
<p>&nbsp;</p>
<h3><span style=""color: #2e8b57;""> 優點1.全英文環境</span></h3>
<p>菲國的官方語言就是英文，正統的發音，校內「只能說英文」規定，讓你24小時學習不間斷，自然開口說英文。</p>
<p>&nbsp;</p>
<h3><span style=""color: #2e8b57;""> 優點2.學校就是官方考場</span></h3>
<p>菲律賓許多語言學校本身就是官方英文檢定指定考場，包括「多益」、「托福」、「雅思」等，上完課程後，在熟悉的環境原地測驗，取得證照。</p>
<p>&nbsp;</p>
<h3><span style=""color: #2e8b57;""> 優點3.一對一的個人教學</span></h3>
<p>以個人為課程中心，大量的一對一時間，老師如同你的專屬私人家教，針對你的需求加強輔導。</p>
<p>&nbsp;</p>
<h3><span style=""color: #2e8b57;""> 優點4.多元課程選擇</span></h3>
<p>包括英文檢定專班，帶領你全力衝刺；專業英文，旅遊、醫療等領域，邁向國際職場；會話加強，說出一口流利好英文等。</p>
<p>&nbsp;</p>
<h3><span style=""color: #2e8b57;""> 優點5.最高CP值</span></h3>
<p>比起動輒數十萬的歐美遊學，菲律賓只需不到 1/3 的價格，不僅食宿全包，還有專人打掃與洗衣，只需專心學習；不僅如此，還能搭配休閒活動，邊上課邊度假，包括潛水、品酒等，多種行程任你搭配。</p>
</div>"','published' => '1','og_title' => '30而立，進修讓你擁有更多籌碼！進修英文，開啟人生第二曲線！','og_description' => '進修就是增加自我價值，給自己的最佳禮物。30歲的你，擁有更清晰的職涯藍圖，讓英文能力成為你的助力，進修為你開啟人生更多選擇。','meta_title' => '30而立，進修讓你擁有更多籌碼','meta_description' => '進修就是增加自我價值，給自己的最佳禮物。30歲的你，擁有更清晰的職涯藍圖，讓英文能力成為你的助力，進修為你開啟人生更多選擇。','canonical_url' => 'https://tw.english.agency/英文進修為自己加分','feature_image_alt' => '進修轉職','feature_image_title' => '進修轉職','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599e89c067cc2-BWAN4PNpJ712vZIMHVQDLSXEgHc7kq-5035x2714.png','author_id' => '7','category_id' => '1','created_at' => '"2017-06-06 09:28:04"'],
  ['id' => '21','title' => 'Email的詢問、回覆該怎麼寫？','slug' => '實用英文-email英文常用語總整理','excerpt' => '"上班族一定會碰到撰寫英文電子郵件的時候吧？ 快速帶你弄懂常用的e-mail英文吧！"','content' => '"<div style=""font-size: 18px;"">
<p>上班族一定都會碰到撰寫英文電子郵件的時候吧？有時候就那麼簡單的一句話，可是要用英文寫出來就是想半天，常常簡單一封電子郵件卻好像寫了一輩子‧‧‧‧‧‧今天，我們就要快速解決這個小問題，跟著小編一句一句學，包準你看完了這篇，以後任何電子郵件都難不倒你啦，那現在就一起來看看這篇實用英文中介紹的多益必考的題型吧！</p>
<h2>&nbsp;&nbsp;</h2>
<hr />
<h2>&nbsp;1.問候</h2>
<p>寫一封電子郵件的開頭往往也是需要問候對方，這部分為多益考題中，商業書信句型的部分，請參考以下:</p>
<p><strong><span style=""color: #2e8b57;"">How are you? It&rsquo;s been a long time since we last met. I hope you are well.</span></strong></p>
<p>(您好嗎？ 自從我們見面以來已經過很久了。我希望您過得很好。)</p>
<p><strong><span style=""color: #2e8b57;"">How are things with you? It&rsquo;s been a long time since the last meeting. I hope you are doing all right.</span></strong></p>
<p>(您好嗎？ 自從上次開會以來已經過很久了。我希望您過得很好。)</p>
<p><strong><span style=""color: #2e8b57;"">How&rsquo;s it going with you? It&rsquo;s been a long time since our last e-mail. I hope everything&rsquo;s okay.</span></strong></p>
<p>(您好嗎？ 自從上次通信以來已經很久了。我希望您事事順利。)</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;2. 目的</h2>
<p>撰寫電子郵件的時候，如果馬上進入正題不免會讓讀信者感到不愉快，所以大家要記得先說明一下這封電子郵件的目的喔。</p>
<p><strong><span style=""color: #2e8b57;"">The purpose of my e-mail is to discuss some urgent issues.</span></strong></p>
<p>(我來信的目的是要討論一些緊急的議題。)</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo;m e-mailing you to ask for your help.</span></strong></p>
<p>(我來信是想要請求您的幫忙。)</p>
<p>基本上這類的句型可以用以上兩種來交互代替，目的有很多，這裡就不一一贅述了。</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;3. 確認</h2>
<p>有時候使用電子郵件來進行確認是非常方便有效率的，以下句型都可以用來確認:</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo;d like to confirm my order.</span></strong></p>
<p>(我想確認我的訂購。)</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo;m e-mailing you to confirm the details of our meeting.</span></strong></p>
<p>(我來信是想跟您確認會議的細節。)</p>
<hr />
<h2>&nbsp;4. 詢問</h2>
<p>寫電子郵件來詢問某種狀況的機會其實非常多，所以以下句型都很實用喔:</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo; d like to inquire about the types of products available.</span></strong></p>
<p>(我想詢問可以購買的產品種類。)</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo;d like to ask about the details of next week&rsquo;s meeting.</span></strong></p>
<p>(我想詢問下次會議的細節。)</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo;m writing to inquire about the product you mentioned to us at time.</span></strong></p>
<p>(我想詢問上次您提起的產品。)</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;5. 關於</h2>
<p>有時候寫信要表達關於某件事情的時候，不知道大家是使用甚麼樣的句型呢？</p>
<p><strong><span style=""color: #2e8b57;"">With regard to the cost，we should talk about it later.</span></strong></p>
<p>(關於費用內容，我們之後還需要再討論。)</p>
<p><strong><span style=""color: #2e8b57;"">Thank you for your e-mail of yesterday concerning the error in the program.</span></strong></p>
<p>(謝謝您昨天的來信告知程式的錯誤。)</p>
<p><strong><span style=""color: #2e8b57;"">We&rsquo;ve received your reply regarding the cost.</span></strong></p>
<p>(我們收到關於費用的回覆。)</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo; like to know more about the compensation.</span></strong></p>
<p>(我想要多了解一點有關於償還費用。)&nbsp;</p>
<hr />
<h2>&nbsp;6. 回覆</h2>
<p>電子郵件往返除了詢問的功能，最多也是回覆的機會了。</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo; m writing in response to your inquiry about the latest products.</span></strong></p>
<p>(我回信來回覆有關於您對於最新產品的詢問。)</p>
<p><strong><span style=""color: #2e8b57;"">In response to your request，we can offer 20 percent volume discount.</span></strong></p>
<p>(回覆您的要求，我們可以提供大量購買20%的優惠。)&nbsp;</p>
<hr />
<h2>&nbsp;7. 請求</h2>
<p>有時候在信件內容需要跟對方請求或者是徵求意見的時候，可以使用以下句型:</p>
<p><strong><span style=""color: #2e8b57;"">Please let me know your opinion about my proposal</span></strong></p>
<p>(關於我的提案，請讓我知道您的想法。)</p>
<p><strong><span style=""color: #2e8b57;"">Please reply when you have the schedule.</span></strong></p>
<p>(當您有行程表的時候請回覆。)</p>
<p><strong><span style=""color: #2e8b57;"">I&rsquo;d appreciate your reply when you have received my e-mail.</span></strong></p>
<p>(我會很感謝您對於收到這封信的回覆。)&nbsp;</p>
<hr />
<h2>&nbsp;8. 希望</h2>
<p>有時候需要跟對方表達希望對方遇到需要詢問事情的時候不要猶豫，這時候就可以使用以下句型:</p>
<p><strong><span style=""color: #2e8b57;"">If you have any questions，please feel free to contact me.</span></strong></p>
<p>(如有任何問題，請隨時連絡我。)</p>
<p><strong><span style=""color: #2e8b57;"">Please do not hesitate to contact me anytime.</span></strong></p>
<p>(請不要猶豫歡迎隨時連絡我。)</p>
<p><strong><span style=""color: #2e8b57;"">If you have any suggestions for improvement，please do not hesitate to call me.</span></strong></p>
<p>(如果您有任何改善的建議，請不要猶豫歡迎打給我。)</p>
<p><strong><span style=""color: #2e8b57;"">Don&rsquo;t hesitate to get back to me if you need any more information.</span></strong></p>
<p>(如您需要更多訊息，請不要猶豫歡迎回電給我。)</p>
<hr />
<h2>&nbsp;9. 附加檔案</h2>
<p>附加檔案是電子郵件時常出現的句型，請參考以下用法:</p>
<p><strong><span style=""color: #2e8b57;"">I have attached our price list.</span></strong></p>
<p>(我已附加我們的價格表。)</p>
<p><strong><span style=""color: #2e8b57;"">Attached are copies of the invoice.</span></strong></p>
<p>(附件是發票副本。)</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;10. 收尾</h2>
<p>信件結尾通常有以下句型可供使用:</p>
<p><strong><span style=""color: #2e8b57;"">Please reply to my e-mail.</span></strong></p>
<p>(請回覆我的信件。)</p>
<p><strong><span style=""color: #2e8b57;"">Please contact me at extension 2819.</span></strong></p>
<p>(請撥打分機2819聯絡我。)</p>
<p><strong><span style=""color: #2e8b57;"">I look forward to meeting you.</span></strong></p>
<p>(我期待與您見面。)</p>
<p><strong><span style=""color: #2e8b57;"">Thank you for your help.</span></strong></p>
<p>(感謝您的幫忙。)</p>
<p>&nbsp;</p>
<p>以上實用句型都是可以快速完成撰寫一封電子郵件喔，希望看完今天的文章對於撰寫英文電子郵件再也不會一個頭兩個大囉～面對多益考試也更能如魚得水拉！</p>
</div>"','published' => '1','og_title' => '實用英文-Email的詢問、回覆該怎麼寫?Email英文常用語總整理!','og_description' => '"上班族一定會碰到撰寫英文電子郵件的時候吧? 有時候簡單的一句話要用英文寫就是想半天，今天就要解決這個小問題，包準以後任何電子郵件都難不倒你，一起來看看這篇實用英文介紹的多益必考的題型!"','meta_title' => '實用英文-Email的詢問、回覆該怎麼寫?Email英文常用語總整理!','meta_description' => '"上班族一定會碰到撰寫英文電子郵件的時候吧? 有時候簡單的一句話要用英文寫就是想半天，今天就要解決這個小問題，包準以後任何電子郵件都難不倒你，一起來看看這篇實用英文介紹的多益必考的題型!"','canonical_url' => 'https://tw.english.agency/實用英文-email英文常用語總整理','feature_image_alt' => 'email用語','feature_image_title' => 'email用語','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599688262a773-xBrAporsQUEWHZzda9Ix9iwDlCyyTy-5000x2625.png','author_id' => '2','category_id' => '10','created_at' => '"2017-06-06 09:28:05"'],
  ['id' => '22','title' => '"名詞子句怎麼用？ N子句文法大解析！"','slug' => '名詞子句-用法','excerpt' => '"多益閱讀準備時，疑惑到底甚麼叫做「名詞子句」? 快速帶你認識這重要文法！"','content' => '"<p>天啊！ 到底甚麼叫做「名詞子句」？到底甚麼叫做「間接問句」？</p>
<p>在做多益閱讀準備時，覺得它們好像既熟悉又陌生嗎？</p>
<p>別怕，今天看完小編精心為大家整理講解的文章之後，</p>
<p>保證大家之後都可以跟他們做好朋友了喔。</p>
<h2>&nbsp;</h2>
<p>&nbsp;</p>
<hr />
<h3>&nbsp;1. 名詞子句</h3>
<p>&nbsp;</p>
<p>首先，我們要來看看什麼叫做「名詞子句」，英文叫作 ""noun clause""</p>
<p>先來看看以下兩者的不同：</p>
<p><strong>句子: 有主詞及動詞，而且句意還有句型都很完整。</strong></p>
<p><strong>子句: 有主詞及動詞，需要依附在句子當中。</strong></p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; (它只是整個句子中的一小部分，所以被稱為子句)</p>
<p>I don&rsquo;t know <span style=""color: #0000cd;"">that she is a police officer.</span>(我不知道她是名警察。)</p>
<p>以上是一個句子。只有＂that she is a police officer ＂為子句。</p>
<p>而所謂的名詞子句，就是把一個句子當作名詞來用。</p>
<p>大家都知道名詞的一般用途就是被當成主詞或受詞，</p>
<p>所以名詞子句也一樣可以被拿來當作主詞或受詞，只不過它是個句子。</p>
<p>&nbsp;</p>
<hr />
<h3>&nbsp;2. 名詞子句的類型</h3>
<h3>&nbsp;</h3>
<h4>A. wh-疑問詞為首的</h4>
<p>（what-甚麼/ why－為什麼/when－何時/who－誰/where－哪裡/how－如何）</p>
<p>（1）I want to know<span style=""color: #0000cd;""><strong> where</strong> he lives.</span>(我想要知道他住在哪裡。)</p>
<p>-&gt;子句內容是跟地點有關，就用<strong>where</strong>帶領</p>
<p>（2）I want to know<span style=""color: #0000cd;""> <strong>when</strong> his English class is.</span> (我想要知道他何時上英文課。)</p>
<p>-&gt;子句內容是跟時間有關，就用<strong>when</strong>帶領</p>
<p>（3）I want to know <span style=""color: #0000cd;""><strong>who</strong> his father is.</span> (我想要知道他的父親是誰。)</p>
<p>-&gt;子句內容是跟某人身分有關，就用<strong>who</strong>帶領</p>
<h3>&nbsp;</h3>
<h4>B. if/whether為首的</h4>
<p>當名詞子句和「是否」有關的時候，就可以使用 ""if"" 或 ""whether"" 把它帶出來。</p>
<p>(1)I want to know <span style=""color: #0000cd;""><strong>if</strong> she knows the truth.</span> (我想知道她是否知道真相。)</p>
<p>(2)I want to know<span style=""color: #0000cd;""><strong> whether </strong>he likes me.</span> (我想知道他是否喜歡我。)</p>
<h3>&nbsp;</h3>
<h4>C. that 為首的子句</h4>
<p>當名詞子句為一個<strong>直接敘述的事實</strong>時，就可以直接用<strong> that </strong>把它帶出來。</p>
<p>例句:</p>
<p>(1)I believe <span style=""color: #0000cd;""><strong>that </strong>you had a good time in Penghu.</span></p>
<p>&nbsp; &nbsp; (我相信你在澎湖玩得很愉快。)</p>
<p>(2)I hope <span style=""color: #0000cd;""><strong>that </strong>my sister can bring us enough food.</span></p>
<p>&nbsp; &nbsp; (我希望我妹妹能夠帶足夠的食物給我們。)</p>
<p><strong>大家注意一下，如果是 that 引導的子句作受詞時，我們在口語中，通常會省略 that 喔。</strong></p>
<p>&nbsp;</p>
<hr />
<h3>&nbsp;3. 特殊動詞、形容詞VS名詞子句</h3>
<p>在英文當中，有一些動詞被稱為<span style=""color: #0000cd;""><strong>建議／懇求性動詞（subjunctive verbs）</strong></span>，</p>
<p>一些形容詞被稱為<span style=""color: #0000cd;""><strong>建議性/懇求性形容詞（subjunctive adjectives）</strong></span></p>
<p>在使用這類詞時，通常都會使用名詞子句作為受詞，比較特殊的是，</p>
<p>這種名詞子句裏的動詞必須要用 <span style=""color: #ff0000;""><strong>原形動詞</strong></span>。</p>
<h3>&nbsp;</h3>
<div style=""overflow-x: auto;"">
<table style=""width: 322px; height: 141px;"">
<thead>
<tr>
<th style=""color: #000080; width: 306px;"" colspan=""4"">A.建議／懇求性動詞.（subjunctive verbs）</th>
</tr>
</thead>
<tbody>
<tr>
<td style=""color: #0000cd; width: 63.1333px;"">
<p>suggest</p>
<span style=""color: #000000;"">提議</span></td>
<td style=""color: #0000cd; width: 66.8667px;"">
<p>order</p>
<span style=""color: #000000;"">命令</span></td>
<td style=""color: #0000cd; width: 75px;"">
<p>advise</p>
<span style=""color: #000000;"">提出忠告</span></td>
<td style=""color: #0000cd; width: 101px;"">
<p>demand</p>
<span style=""color: #000000;"">要求</span></td>
</tr>
<tr>
<td style=""color: #0000cd; width: 63.1333px;"">
<p>insist</p>
<span style=""color: #000000;"">堅持</span></td>
<td style=""color: #0000cd; width: 66.8667px;"">
<p>propose</p>
<span style=""color: #000000;"">提議</span></td>
<td style=""color: #0000cd; width: 75px;"">
<p>require</p>
<span style=""color: #000000;"">需要</span></td>
<td style=""color: #0000cd; width: 101px;"">
<p>recommend</p>
<span style=""color: #000000;"">建議</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
</div>
<p>（１）I <span style=""color: #0000cd;""><strong>suggest</strong></span> that you <span style=""color: #ff0000;""><strong>see</strong></span> the doctor right now.</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; (我建議你現在馬上去看醫生。)</p>
<p>（２）Our teacher <span style=""color: #0000cd;""><strong>requires</strong> </span>that we <span style=""color: #ff0000;""><strong>complete</strong></span> this project today.</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; (我們老師要我們今天完成這份報告。)</p>
<h4>&nbsp;</h4>
<div style=""overflow-x: auto;"">
<table>
<thead>
<tr>
<th style=""color: #000080;"" colspan=""4"">B. 建議性/懇求性形容詞（subjunctive adjectives）</th>
</tr>
</thead>
<tbody>
<tr>
<td style=""color: #0000cd;"">
<p>important</p>
<span style=""color: #000000;"">重要的</span></td>
<td style=""color: #0000cd;"">
<p>vital</p>
<span style=""color: #000000;"">極重要的</span></td>
<td style=""color: #0000cd;"">
<p>necessary</p>
<span style=""color: #000000;"">必須的</span></td>
<td style=""color: #0000cd;"">
<p>essential</p>
<span style=""color: #000000;"">重要的</span></td>
</tr>
<tr>
<td style=""color: #0000cd;"" colspan=""4"">
<p>advisable</p>
<span style=""color: #000000;"">明智的</span></td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>
<p>It is <span style=""color: #0000cd;""><strong>important</strong></span> that we <span style=""color: #ff0000;""><strong>protect</strong></span> our Mother Earth.</p>
<p>(保護我們的地球是很重要的。)</p>
<p>&nbsp;</p>
<hr />
<h3>&nbsp;4. 子句、主要子句的時態</h3>
<p>&nbsp;最後，不知道大家會不會出現一個疑問就是，那時態要怎麼用呢。其實只要遵守以下兩個用法就好了喔。</p>
<p>（1）如果主要子句為現在式，則名詞子句可以是任何時式。</p>
<p>（2）如果主要子句為過去式，則名詞子句大多是與過去相關的時式。</p>
<p>（3）if/whether 為首的子句，必須考量到假設語態與主要子句之時態來變化。</p>"','published' => '1','og_title' => '"名詞子句怎麼用? N子句文法大解析!"','og_description' => '"多益閱讀準備時，疑惑到底甚麼叫做「名詞子句」? 到底甚麼叫做「間接問句」? 今天看完小編精心為大家整理講解的文章之後，保證大家之後都可以跟他們做好朋友了喔。"','meta_title' => '"名詞子句怎麼用? N子句文法大解析!"','meta_description' => '"多益閱讀準備時，疑惑到底甚麼叫做「名詞子句」? 快速帶你認識這重要文法！"','canonical_url' => 'https://tw.english.agency/名詞子句-用法','feature_image_alt' => '名詞子句','feature_image_title' => '名詞子句','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/598d655ccf61a-5wHwY3APnXkYQcXfs55NSG4sL8ncZc-1000x600.png','author_id' => '3','category_id' => '10','created_at' => '"2017-06-06 09:28:09"'],
  ['id' => '24','title' => '多益英文聽力怎麼準備？3個小撇步！','slug' => '多益準備-英文聽力小撇步','excerpt' => '美國人學英文也從來不學文法，英文從來就沒有文法這個東西，他們都是自然而然就會說了?當然不是這樣！今天就來帶你重新認識文法吧！','content' => '"<div style=""font-size: 24px;"">很多人一輩子都學不好英文，其實大多數的原因都是方法不對。如何讓多益準備更輕鬆有效率呢？ 趕快來看今天多益小編為大家準備的小撇步！ 我們都知道英文分成聽說讀寫，為什麼要是這個順序呢？ 因為學一個語言的時候，<strong><span style=""color: #2e8b57;"">聽和說其實是最重要的！</span></strong> 小時候我們剛開始學講話的時候，不就是學著父母牙牙學語， 就是因為嬰兒時期大量聽父母或者是周遭的人說話， 中文就這麼「聽」進去的，所以學語言其實最重要的一個環節 是訓練一個聽力耳，因為當你聽得懂的時候，你就可以模仿著說。 所以很多人認為學英文就要開始學文法，其實是錯誤的， 大家想想看，我們在學中文的時候，有學過任何的文法嗎？ 其實<strong><span style=""color: #2e8b57;"">美國人學英文也從來不學文法</span></strong> 英文從來就沒有文法這個東西，他們都是自然而然就會說了。 因為由此可見，學英文的正確步驟是從聽到說， 當你在聽的過程碰到不會的字彙就把它記下來， 等到下次在聽到同樣字彙就能夠記住了， 其實這樣對於學習單字也是很有幫助的。 所以，正確學英文的方式，其實應該先讓 <strong><span style=""color: #2e8b57;"">英文聽力能力提升上來之後，口說能力當然就跟著增加</span></strong>； 然後再配合做英文閱讀，培養閱讀能力之後， 寫作能力也會跟著提昇，這才是學英文正確的順序。 所以，今天要來跟大家分享如何訓練自己的英文聽力能力的小祕訣喔。 &nbsp;<hr />
<h2>&nbsp;1.初級</h2>
<strong>一開始訓練自己的英文聽力的時候，不要太貪心，可以先聽一小段就可以了喔。</strong> 首先，可以先閱讀全文，先了解全文的意思。 中英文的文章句子可以對照著看，了解自己不懂的地方，並且加以註解。 接下來，可以在播放音檔的時候，可以一邊聽一邊看著文字內容， 特別針對自己不會的單字，利用聽力的方式「學」單字。 如果可以利用這樣的方式反覆練習， 原本不熟的單字就可以自然而然牢牢深刻印在腦海裡喔， 因為這樣的練習是以<strong><span style=""color: #2e8b57;"">重複性的練習</span></strong>為主，在練習聽力的時候， 就可以很強烈的把單字的聲音和文字連結在一起。 &nbsp;<hr />
<h2>&nbsp;2.中級</h2>
<strong>如果覺得自己的英文聽力稍有進步之後，可以開始加入新的練習方式 :</strong>
<h3>Shadowing 影子跟讀法</h3>
所謂的&rdquo;shadowing&rdquo;就是「跟述、跟讀」，是一種語言的模仿練習。 &rdquo;Shadow&rdquo;這個單字本身是「影子」的意思，帶有「跟著」的意味， 而 &ldquo;shadowing&rdquo;在英文口說技巧練習上，就是<strong><span style=""color: #2e8b57;"">「跟讀法」</span></strong>。 這是一個訓練聽說能力非常有效的一種學習方式， 也是專門訓練口譯人員可以同步聽說的方法。 訓練方式就是<span style=""color: #2e8b57;"">先聽音檔說話者朗讀一次，了解說話者的速度和語調之後，</span> <span style=""color: #2e8b57;"">再重覆播放一次，不過第二次播放的時候，記得就要開始跟著音檔說話</span>， 他說一個字你就要緊跟著說一個字，就像你是說話者的影子一樣， 要練習到能跟上說話者的速度為止。其實如果利用這樣的方式練習， 其實不只可以訓練自己的英文聽力，也可以提升自己的口語能力 以及矯正英文口音，是一個很棒的英文學習方式喔。 開始「聽」自己喜歡的電影/影集/短片 其實很多學生一定都有利用這個方式學過英文， 但是首先一定要注意所挑的影片到底適不適合自己的程度以及興趣， 不要覺得用影片學英文就是要挑一些比較難的影片， 這樣一沒有效果，二來也會很快就失去興趣。 所以<strong><span style=""color: #2e8b57;"">挑選適合自己程度的影片絕對是這個學習方法最重要的關鍵。</span></strong> 接下來，就是影片字幕的問題了， 如果本身是初級的學習者，記得在觀看英文的時候， <span style=""color: #2e8b57;"">第一次字幕要選擇中文，第二次要選擇英文字幕，最後一次就要選擇無字幕喔。</span> 因為第一次先看中文字幕，可以先了解整部影片主旨， 才不會一開始就因為聽不懂而失去學習的興趣和信心。 第二次的時候，因為已經了解影片的大概了，開始看英文字幕的時候， 就可以開始習慣影片說話的節奏再搭配剛剛理解的中文意思， 開始猜測一些新單字的意思，並且將新單字加以註解甚至可以記錄下來， 成為自己的英文單字本。 第三次的時候，可以關掉字幕，跟著劇情的節奏， 回想剛剛觀看字幕的記憶，你就會發現你可以跟著影片主角做跟讀喔。 如果你是比較進階的學習者，建議先從無字幕開始看起， 接下來再打開英文字幕，最後一次再看一次無字幕的影片。 因為程度比較好的話，如果可以先邊聽邊猜測影片內容， 可以讓自己大量練習英文聽力，練習語感。 接下來打開英文字幕的時候，就可以知道剛剛自己不懂的地方猜對多少， 也可以將自己所學的新單字或者是句子記錄下來。 最後一次再用無字幕的方式觀看影片，開始跟讀， 可以檢視自己是不是還有那裡不懂。<hr />
<h2>&nbsp;</h2>
<h2>3.高級</h2>
如果你的英文程度已經達到高級的話，其實開始善用網路資源來練習聽力。 網路有很多免費的資源可以利用， 例如 <strong><span style=""color: #2e8b57;"">Ted Talks演講/ BBC News/ British Council</span></strong>等等， 因為這些網站提供的影片，大多都有程度的分級， 大家可以根據自己的程度選擇適合自己的影片， 而且主題廣泛，也可以選擇自己喜歡的影片來聽。 其實訓練英文聽力是急不得的，因為這需要長時間的能力累積， 所以千萬不要想要一步登天，以為一天花個10個小時猛聽英文， 就會有進步，其實這樣做，不僅會很快失去耐心，對於學習也沒有多大的幫助。 <strong><span style=""color: #2e8b57;"">最好的方式就是積少成多，每天聽20到30分鐘來為多益聽力準備</span></strong>， 利用通勤或者是走路的方式，既不浪費時間又可以訓練聽力，是不是很棒呢。</div>"','published' => '1','og_title' => '多益英文聽力怎麼準備？3個小撇步！','og_description' => '美國人學英文也從來不學文法，英文從來就沒有文法這個東西，他們都是自然而然就會說了。由此可見，學英文的正確步驟是從聽到說，今天就來重新定義你對英文學習的方法','meta_title' => '多益英文聽力好吃力怎麼辦？今天來教你小撇步','meta_description' => '美國人學英文也從來不學文法，英文從來就沒有文法這個東西，他們都是自然而然就會說了多益英文聽力好吃力怎麼辦？今天來教你小撇步。由此可見，學英文的正確步驟是從聽到說，今天就來重新定義你對英文學習的方法','canonical_url' => 'https://tw.english.agency/多益準備-英文聽力小撇步','feature_image_alt' => '英文聽力小撇步','feature_image_title' => '英文聽力小撇步','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/local_testing/593c485944ddf-wQprMFbQvvM12ybV5GI4WEvad78lt8-1000x666.jpeg','author_id' => '6','category_id' => '','created_at' => '"2017-06-06 09:28:11"'],
  ['id' => '28','title' => '最新消息、棚內轉播的英文怎麼說？常用新聞英文術語大公開！','slug' => '常用新聞英文術語大公開','excerpt' => '最新消息、棚內轉播英文你知道怎麼說嗎?當大家在看應英文新聞時，必須先了解到這些常見的英文，今天就來看看這些常見的英文吧！','content' => '"<div style=""font-size: 18px;"">最新消息、棚內轉播英文你知道怎麼說嗎？當大家在看應英文新聞時，必須先了解到這些常見的英文，今天就來看看這些常見的英文吧！
<h2><strong><span style=""color: #008080;""> 1.常見的通用新聞單字</span></strong></h2>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> muckraking culture</span></span> <em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;"">爆料文化</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> not rule out the possibility of</span></span><em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;""> 不排除可能性</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> status quo</span></span> <em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;"">現狀</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> consensus</span></span> <em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;"">共識</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> made headline</span></span> <em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;"">上頭條</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> mouthpiece</span></span> <em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;"">傳聲筒子；代言人。</span></p>
<h2><strong><span style=""color: #008080;""> 2.常見的新聞句型</span></strong></h2>
<p><span style=""font-weight: 400;"">〈1〉<span style=""color: #2e8b57;""> Breaking News. </span>最新消息</span></p>
<p><span style=""color: #2e8b57;""><span style=""font-weight: 400;""> In late -<strong> breaking news</strong><span style=""font-weight: 400;""> ，local fisherman Nick Castle . .</span></span></span></p>
<p><span style=""font-weight: 400;"">(最新消息，本地漁民尼克卡索. .)</span></p>
<p><span style=""color: #2e8b57;""><span style=""font-weight: 400;""> We\'ve got some important<strong> breaking news</strong><span style=""font-weight: 400;""> for you about the status quo in Syria &nbsp;.</span></span></span></p>
<p><span style=""font-weight: 400;"">(我們將會對你帶來一些有關敘利亞的最新消息。)</span></p>
<p><span style=""font-weight: 400;"">〈2〉<span style=""color: #2e8b57;""> The just in. </span>剛收到最新消息..</span></p>
<p><span style=""color: #2e8b57;""> <strong>The just in</strong><span style=""font-weight: 400;"">，The USA government have made up their decision for....</span></span></p>
<p><span style=""font-weight: 400;"">(剛收到最新消息，美國政府已經針對.....做出決議。)</span></p>
<p><span style=""font-weight: 400;"">〈3〉<span style=""color: #2e8b57;""> Top story</span> 頭條報導</span></p>
<p><span style=""color: #2e8b57;""> <span style=""font-weight: 400;"">In the <strong>Top story</strong></span><span style=""font-weight: 400;""> for this week，we will have series of &nbsp;in-depth coverage for the latest situation of Syria </span></span></p>
<p><span style=""font-weight: 400;"">(在本周頭條新聞的部分，我們將會為敘利亞的最新情勢做一系列的深入報導。)</span></p>
<p><span style=""font-weight: 400;"">*<span style=""color: #2e8b57;""> In-depth coverage</span>深入報導</span></p>
<p><span style=""font-weight: 400;""> &nbsp;TOEIC專欄-記者</span></p>
<p><span style=""font-weight: 400;"">〈4〉<span style=""color: #2e8b57;""> Our source tell us </span>根據消息指出</span></p>
<p><span style=""color: #2e8b57;""><strong>Our source tell us</strong><span style=""font-weight: 400;""> that the city government issued the alerts for the coming calamities from Typhoon.</span><span style=""font-weight: 400;""><strong>Stay tuned</strong>，our reporter john who\'s <strong>reporting live</strong> at city government will have an <strong>exclusive interview</strong> with the mayor for you.</span></span></p>
<p><span style=""font-weight: 400;""> (根據消息指出，市府當局已經針對颱風可能帶來的災做出警告。別轉台，我們在市府的特派記者John將會帶來即時報導，他將會為您帶來一段與市長先生的獨家專訪。)</span></p>
<p><em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">&nbsp;stay tuned</span> 別轉台。</span></p>
<p><em><span style=""font-weight: 400;"">➡&nbsp;</span></em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">reporting live&nbsp;</span>現場即時報導。</span></p>
<p><em><span style=""font-weight: 400;"">➡</span></em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">&nbsp;exclusive interview&nbsp;</span>獨家專訪。</span></p>
<p><span style=""font-weight: 400;"">*小解析 At the scene vs Reporting live</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> <strong>At the scene</strong></span> 就是指人在現場，而<span style=""color: #2e8b57;""><strong>reporting live</strong></span>則是指人在現場做報導，但坐在攝影棚的主播常常將兩者結合在一起告訴觀眾。</span></p>
<p><span style=""color: #2e8b57;""> <span style=""font-weight: 400;"">Let\'s hear from Nick，who\'s <strong>reporting live at the scene</strong>. </span></span></p>
<p><span style=""font-weight: 400;"">(讓我們透過現在人在事件現場的Nick，了解一下現場狀況。)</span></p>
</div>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '最新消息、棚內轉播的英文怎麼說?常用新聞英文術語大公開!','meta_description' => '最新消息、棚內轉播英文你知道怎麼說嗎?當大家在看應英文新聞時，必須先了解到這些常見的英文，今天就來看看這些常見的英文吧!','canonical_url' => 'https://tw.english.agency/常用新聞英文術語大公開','feature_image_alt' => '英文學習-新聞英文術語','feature_image_title' => '轉撥英文單字','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599fcbb77a08d-I7mrEe2caT1WH16XjK3Dr2rQz2S9z8-5181x2856.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-12 09:21:32"'],
  ['id' => '29','title' => '"傳單、海報、展台英文怎麼說? 你必須知道的展場英文單字!"','slug' => '傳單-海報-展台-英文','excerpt' => '展場是向客戶介紹公司或者商品非常重要的媒介，因為透過展場可以針對不同類型的客戶做面對面直接的接待以及說明，今天就來學一些重要的展場英文單字吧','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-weight: 400;"">英文早已經成為上班族需要外銷到全世界的一個重要溝通技巧，因此今天我們就要來學學一些重要的展場英文單字還有用語吧。展場是向客戶介紹公司或者是商品非常重要的一個媒介之一，因為透過展場可以針對各種不同類型的客戶做面對面非常直接的接待以及說明，或者是藉由展示來介紹一些最新產品以便讓其他人認識了解公司的潛力以及競爭力，而這些展場的英文單字大多來自</span><a href=""https://www.goeducation.com.tw/toeic%E6%BA%96%E5%82%99/""><span style=""font-weight: 400;"">多益考試</span></a><span style=""font-weight: 400;"">，常常出現在多益閱讀中，所以小編為大家整理的這些的展場單字是非常實用的。</span></p>
<h2><strong>1.傳單</strong><strong>Leaflet/flyer</strong></h2>
<p><span style=""font-weight: 400;"">首先展場中最重要最快速可以傳達公司資訊的方法莫過於發傳單了，以前你一定聽過別人說</span><span style=""font-weight: 400;"">&rdquo;DM&rdquo;，&ldquo;<span style=""color: #2e8b57;"">DM</span>&rdquo;</span><span style=""font-weight: 400;""> 其實是</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Direct Mail</span></span><span style=""font-weight: 400;"">，是指說</span><span style=""font-weight: 400;"">「<span style=""color: #2e8b57;"">直郵廣告信</span>」</span><span style=""font-weight: 400;"">，是指信箱裡面會收到的廣告信件，其實根本就不是紙本傳單的意思喔。<span style=""color: #2e8b57;"">傳單</span>的英文可以用&rdquo;</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">leaflet</span></span><span style=""font-weight: 400;"">&rdquo;或者是&rdquo; </span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">flyer</span></span><span style=""font-weight: 400;"">&rdquo;。例句:</span></p>
<p>A:<span style=""color: #2e8b57;""><span style=""font-weight: 400;""> &ldquo;Excuse me，can</span><span style=""font-weight: 400;""> I get a leaflet/flyer</span><span style=""font-weight: 400;""> from you?&rdquo;&nbsp;</span></span></p>
<p><span style=""font-weight: 400;"">(先生，可以給我一張傳單嗎?)</span></p>
<p><span style=""font-weight: 400;"">B: &ldquo;Sure.&rdquo;&nbsp;</span>(那當然。)</p>
<h2><strong>2.海報</strong><strong>poster</strong></h2>
<p><span style=""font-weight: 400;"">在英文裡面，一般常見的<span style=""font-weight: 400;"">宣傳</span><span style=""font-weight: 400;"">海報</span>，可以用</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">poster</span></span><span style=""font-weight: 400;""> 來表示，然而如果是如</span><span style=""font-weight: 400;"">劇場、歌劇</span><span style=""font-weight: 400;"">等表演性質的可以用</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">paybill</span>. 而貼海報這個動作則是要用<span style=""color: #2e8b57;"">paste</span> 這個詞喔！</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">(1)<span style=""color: #2e8b57;""> I pasted posters onto the wall.&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(我把海報貼在牆上了。)</span></p>
<p><em><span style=""font-weight: 400;"">(2)<span style=""color: #2e8b57;"">The room was full of posters and advertisements.&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(房間裡貼滿了海報和廣告</span><span style=""font-weight: 400;"">。)</span></p>
<p><em><span style=""font-weight: 400;"">(3)<span style=""color: #2e8b57;"">This is the playbill of the Disney musical group.&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(這是迪士尼歌舞劇團的宣傳海報。)</span></p>
<p><span style=""font-weight: 400;"">小補充:<span style=""color: #2e8b57;"">Musical group</span>歌舞劇團/<span style=""color: #2e8b57;"">Musical drama</span> 歌舞劇、音樂劇/<span style=""color: #2e8b57;"">circus </span>馬戲團</span></p>
<h2><strong>3.攤位</strong><strong><em>booth</em></strong></h2>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">攤位</span>的英文叫作&rdquo;</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">booth</span></span><span style=""font-weight: 400;"">&rdquo;，這個字指呃也就是所謂的小空間、小亭子、在展場中也就是小展台，而</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">ticket booth</span></span><span style=""font-weight: 400;""> 也就是</span><span style=""font-weight: 400;"">售票亭</span><span style=""font-weight: 400;"">喔！相關的延伸單字有:</span></p>
<p><em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">booth area</span></span></em><span style=""font-weight: 400;""> 攤位面積 </span></p>
<p><em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">booth number/size</span></span></em><span style=""font-weight: 400;""> &nbsp;攤位號/攤位尺寸</span></p>
<p><em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">booth sign</span></span></em><span style=""font-weight: 400;""> 攤位楣板(用於標識參展商的公司名稱攤位號等)</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">(1)<span style=""color: #2e8b57;"">I have got us a booth at the trade fair in Frankfurt!&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(我們在法蘭克福展覽會上爭取到了一個攤位。)</span></p>
<p><em><span style=""font-weight: 400;"">(2) <span style=""color: #2e8b57;"">When will you start the booth?&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(何時你要開始搭建這個攤位？)</span></p>
<p><span style=""font-weight: 400;"">小補充*<span style=""color: #2e8b57;"">telephone booth (call booth)&nbsp;</span>電話亭</span></p>
<h2><strong>4.展覽</strong><strong><em>exhibition/fair/expo</em></strong></h2>
<p><span style=""font-weight: 400;"">只要是聊到「展場」，怎麼可以沒有學到「展覽」的英文，可以用</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">exhibition</span> 之外，還可以用<span style=""color: #2e8b57;"">expo，fair，exposition </span>等字眼喔！</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">(1) <span style=""color: #2e8b57;"">Did you get the invitation for the job fair?&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(你有收到就職博覽會的邀請涵嗎？)</span></p>
<p><em><span style=""font-weight: 400;"">(2) <span style=""color: #2e8b57;"">This is an annual expo. &nbsp;</span></span></em><span style=""font-weight: 400;"">(這可是年度大展！)</span></p>
<p><em><span style=""font-weight: 400;"">(3) <span style=""color: #2e8b57;"">This car exhibition is coming.&nbsp;</span></span></em><span style=""font-weight: 400;"">(汽車展就快來了。)</span></p>
<h2><strong>5.識別證</strong><strong><em>Badge</em></strong></h2>
<p><span style=""font-weight: 400;"">只要是參展的公司或者是工作人員，一定會拿到識別證，英文叫作</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">badge</span>，這個字還帶有紀念章、勳章、證章等意思。</span><span style=""font-weight: 400;"">而我們一般的證件則稱作</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">credentials / paperwork</span>.</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">(1) <span style=""color: #2e8b57;"">Excuse me，do I need to change my badge?&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(請問需要換識別證嗎?) </span></p>
<p><em><span style=""font-weight: 400;"">(2) <span style=""color: #2e8b57;"">This is your badge，thank you.</span></span></em></p>
<p><span style=""font-weight: 400;"">(您的識別證，謝謝。)</span></p>
<p><span style=""font-weight: 400;"">小補充*<span style=""color: #2e8b57;"">ID( Identification) card</span> 身分證/ <span style=""color: #2e8b57;"">health insurance card</span> &nbsp;健保卡/<span style=""color: #2e8b57;"">credentials </span>證書、證照。 </span></p>
<h2><strong>6.邀請信/入場券</strong></h2>
<h2><strong><em>invitation/admission</em></strong></h2>
<p><span style=""font-weight: 400;"">除了相關廠商和工作人員之外的應該都是會拿</span><span style=""font-weight: 400;"">「邀請信」</span><span style=""font-weight: 400;"">或者是</span><span style=""font-weight: 400;"">「入場券」</span><span style=""font-weight: 400;"">，英文叫作&rdquo; </span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">invitation</span></span><span style=""font-weight: 400;"">&rdquo;或者是&rdquo;</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">admission</span></span><span style=""font-weight: 400;"">&rdquo;。</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">(1) <span style=""color: #2e8b57;"">Thank you for your invitation to this great exhibition.&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(謝謝你邀請我來這麼棒的展覽。)</span></p>
<p><em><span style=""font-weight: 400;"">(2) <span style=""color: #2e8b57;"">Hundreds of invitations of The 35th Int\'l Designers\' Exhibition are being sent out this week.&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(本週將寄出「2016設計展」數百份邀請函。)</span></p>
<p><span style=""font-weight: 400;"">(</span><em><span style=""font-weight: 400;"">3) <span style=""color: #2e8b57;"">Gates open at 10.00 am and admission are free for the first ten people.</span></span></em></p>
<p><span style=""font-weight: 400;"">(早上10:00開門，前10名免費入場。)</span></p>
<h2><strong>7.展示櫃</strong><strong>S</strong><strong><em>howcase</em></strong></h2>
<p><span style=""font-weight: 400;"">&ldquo;<span style=""color: #2e8b57;"">Show</span>&rdquo;可以用來說明展示櫃</span><span style=""font-weight: 400;"">，而</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">display</span></span><span style=""font-weight: 400;"">這個動詞，有</span><span style=""font-weight: 400;"">具有展示</span><span style=""font-weight: 400;"">的意思 例如: <span style=""color: #2e8b57;"">The cakes were displayed in the window ，they looked so delicious .</span>(這些蛋糕都陳列在櫥窗中，看起來都好好吃喔。)因此，</span><span style=""font-weight: 400;"">也有人用<span style=""color: #2e8b57;"">display case</span>來形容喔!</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">I need some high-quality display cases for my products.</span></span></p>
<p><span style=""font-weight: 400;"">(我需要一些高品質的展示櫃來展示我的產品。)</span></p>
<p><span style=""font-weight: 400;"">小補充*<span style=""color: #2e8b57;"">Display</span> 這個字多半用於單純靜態的呈現，而<span style=""color: #2e8b57;"">demonstration</span>則是動的示範。</span></p>
<h2><strong>8.接駁車</strong><strong><em>Courtesy Car/ Shuttle bus</em></strong></h2>
<p><span style=""font-weight: 400;"">有些展覽會提供接駁車供參展民眾搭乘，英文叫作是""</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">shuttle</span>""，</span><span style=""font-weight: 400;"">而機場常見的、飯店常見的那種休旅車、保母接駁車，則要用</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">courtesy car</span></span><span style=""font-weight: 400;"">來稱呼。</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Where can I take the shuttle bus to The World Trade Center in Hall 1? &nbsp;</span></span></em><span style=""font-weight: 400;"">(我要去哪裡搭去世貿一館的</span><span style=""font-weight: 400;"">接駁巴士</span><span style=""font-weight: 400;"">?)</span></p>
<p><span style=""font-weight: 400;"">小補充*Shuttle 本身就帶有接駁車的意思囉！ 如果加上bus就是表示接駁巴士喔！</span></p>
<h2><strong>9.問卷</strong><strong><em>Questionnaire</em></strong></h2>
<p><span style=""font-weight: 400;"">展覽會場常會需要民眾填問卷送東西，問卷的英文就叫作</span><span style=""font-weight: 400;"">&rdquo; <span style=""color: #2e8b57;"">questionnaire</span>&rdquo;</span><span style=""font-weight: 400;"">，「填寫問卷」可以用</span><span style=""font-weight: 400;"">&rdquo; <span style=""color: #2e8b57;"">fill out the questionnaire</span>&rdquo;。</span><span style=""font-weight: 400;"">&rdquo;Fill out&rdquo; 就是「填寫」的意思</span></p>
<p><span style=""font-weight: 400;"">例如: &nbsp;</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">Everyone fills out the application carefully.</span></span></p>
<p><span style=""font-weight: 400;"">(大家都仔細填寫申請表。)</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">All participants will be asked to fill in a questionnaire.</span></span></em></p>
<p><span style=""font-weight: 400;"">(所有參與者將被要求完成一份問卷調查。)</span></p>
<h2><strong>10.名片</strong><strong><em>Business Card</em></strong></h2>
<p><span style=""font-weight: 400;"">參加展覽尤其是廠商工作人員一定要記得隨身攜帶名片，至少要將聯絡方式給對方，才能有後續的溝通管道，英文叫作&rdquo;</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">business card</span>&rdquo;</span><span style=""font-weight: 400;"">。</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">I exchange my business card with a client.&nbsp;</span></span></em></p>
<p><span style=""font-weight: 400;"">(我跟客戶交換我的名片。)</span></p>
<p>小補充:常見的詢問聯絡方式的英文則是:</p>
<p><em><span style=""font-weight: 400;"">-&gt;<span style=""color: #2e8b57;"">Kindly advise the contact info of XXX.</span></span></em></p>
<p><em><span style=""font-weight: 400;"">-&gt;<span style=""color: #2e8b57;"">Please advise how I can get in contact with XXX.</span></span></em></p>
<p>___________________________________________________________________</p>
<h3><strong>接下來提供以下相關用語:</strong></h3>
<p><em><span style=""font-weight: 400;"">1.<span style=""color: #2e8b57;"">Excuse me，which exhibition would you like to visit?&nbsp;</span></span></em><span style=""font-weight: 400;"">(請問您要參觀哪個展覽？)</span></p>
<p><em><span style=""font-weight: 400;"">2.<span style=""color: #2e8b57;"">Sorry，this exhibition is only open to businesses of related industries.&nbsp;</span></span></em><span style=""font-weight: 400;"">(本次展覽只提供給相關業者入場。)</span></p>
<p><em><span style=""font-weight: 400;"">3.<span style=""color: #2e8b57;"">Excuse me，do I need to change my badge?&nbsp;</span></span></em><span style=""font-weight: 400;"">(請問需要換識別證嗎?)</span></p>
<p><em><span style=""font-weight: 400;"">4.<span style=""color: #2e8b57;"">We need to give you your badge in exchange with your business card.&nbsp;</span></span></em><span style=""font-weight: 400;"">(我們需要用名片換識別證。)</span></p>
<p><em><span style=""font-weight: 400;"">5.<span style=""color: #2e8b57;"">Each hall provides a shuttle bus every 10 minutes.&nbsp;</span></span></em><span style=""font-weight: 400;"">(每10分鐘就會有一班接駁車，它都會在每一館停靠。)</span></p>
<p><em><span style=""font-weight: 400;"">6.<span style=""color: #2e8b57;"">Hi，would you like to know something about our products?&nbsp;</span></span></em><span style=""font-weight: 400;"">(您好，有興趣了解我們的產品嗎？)</span></p>
<p><em><span style=""font-weight: 400;"">7.<span style=""color: #2e8b57;"">This is a questionnaire，please help me fill it out.&nbsp;</span></span></em><span style=""font-weight: 400;"">(這邊有一份問卷，麻煩請幫我做填寫。)</span></p>
<p><em><span style=""font-weight: 400;"">8.<span style=""color: #2e8b57;"">Because of the limited amount of gifts，we provide these gifts to the first fifty visitors every day.&nbsp;</span></span></em><span style=""font-weight: 400;"">(因為禮物數量有限，每日提供前50名的訪客。)</span></p>
<p><em><span style=""font-weight: 400;"">9.<span style=""color: #2e8b57;"">Please step forward.&nbsp;</span></span></em><span style=""font-weight: 400;"">(麻煩請往前走。)</span></p>
<p><em><span style=""font-weight: 400;"">10.<span style=""color: #2e8b57;"">If you are interested in the booths and the map of the room，you can ask for help at the information desk.&nbsp;</span></span></em><span style=""font-weight: 400;"">(如果您想要詢問有關於攤位位置或地圖，麻煩請到詢問台。)</span></p>
</div>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '"傳單、海報、展台英文怎麼說? 你必須知道的展場英文單字!"','meta_description' => '展場是向客戶介紹公司或者商品非常重要的媒介，因為透過展場可以針對不同類型的客戶做面對面直接的接待以及說明，今天就來學一些重要的展場英文單字吧','canonical_url' => 'https://tw.english.agency/傳單-海報-展台-英文','feature_image_alt' => '英文怎麼說-展場英文單字','feature_image_title' => '傳單、海報英文怎麼說','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59a5153659e3b-Os4NqGbX2pOQq7k7ceouvQH0wsKh5U-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2017-06-12 09:23:38"'],
  ['id' => '30','title' => '"沐浴乳、洗面乳英文怎麼說？ 常見衛浴用品的英文總整理！"','slug' => '常見衛浴用品單字整理','excerpt' => '飯店的清潔用品都用英文標示，到底哪瓶是洗髮精或沐浴乳，常常讓人搞不清，快來學一些簡易的個人清潔用品單字，包準你以後出國不再出糗！','content' => '"<p><span style=""font-weight: 400;"">飯店空調開得很強，覺得皮膚好乾啊！</span></p>
<p><span style=""font-weight: 400;"">趕緊拿起這一罐白白看起來像是乳液的東西來擦一下！</span></p>
<p><span style=""font-weight: 400;"">但怎麼都不會吸收反而越抹越黏，拿給看得懂英文的朋友看看到底怎麼回事...</span></p>
<p><span style=""font-weight: 400;"">才發現傻傻地把全身抹上了沐浴乳根本就是超糗的天啊...</span></p>
<p><span style=""font-weight: 400;"">(就差沒流汗不然就直接乾洗泡泡浴惹&hellip;囧|||)</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">出國旅遊大部分的飯店都會附上一些方便的個人清潔用品，</span></p>
<p><span style=""font-weight: 400;"">讓你不用裝一堆瓶瓶罐罐在行李箱裡面，</span></p>
<p><span style=""font-weight: 400;"">不過都是用英文標示的清潔用品，到底哪瓶是洗髮精或沐浴乳，總是常常讓人搞不清楚&hellip;</span></p>
<p><span style=""font-weight: 400;"">不要再製造拿刮鬍膏去刷牙或拿牙膏去洗臉的糗事了！！！</span></p>
<p><span style=""font-weight: 400;"">快來學一下這些簡易的個人清潔用品單字，包準你以後出國不再出糗 </span></p>
<p><span style=""font-weight: 400;"">請跟姊一起來看看以下各類的個人清潔用品，來～讓我們先從頭頂開始往下走：</span></p>
<div style=""overflow-x: auto;"">
<table style=""border: 5px solid #cccccc; width: 587px;"" rules=""all"" cellpadding=""5"">
<tbody>
<tr>
<td style=""background-color: #66cdaa; width: 595px;"" colspan=""4"">
<p><strong>頭髮清潔護理</strong></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">&nbsp;</td>
<td style=""width: 70px;"">
<p><strong>中文</strong></p>
</td>
<td style=""width: 114.867px;"">
<p><strong>英文</strong></p>
</td>
<td style=""width: 393.133px;"">
<p><strong>不負責任備註</strong></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">1</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">洗髮乳</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>shampoo</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">聽起來像 &ldquo;香撲&rdquo; &ndash; 女生洗完頭髮香香der就讓人很想把她給撲倒！</span><span style=""font-weight: 400;"">^o^ (抱歉有點限制級?)</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">2</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">潤髮乳</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>conditioner</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">讓頭髮的<span style=""color: #2e8b57;"">condition</span>(狀態)更好的 <span style=""color: #2e8b57;"">conditioner</span>！ LOL</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">3</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">護髮乳</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>hair mask</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">頭髮也要敷面膜就變得滑溜溜柔柔順順～</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 595px;"" colspan=""4"">
<p><strong>臉部清潔</strong></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">4</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">洗面乳</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>face wash /</em></span></strong></p>
<p><strong><span style=""color: #2e8b57;""><em>facial cleanser</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">洗臉，就<span style=""color: #2e8b57;"">face wash</span>很直接了當&hellip; </span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">facial cleanser</span>根本就臉部清潔劑直翻簡單吧！</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">5</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">牙膏</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>tooth paste</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">牙膏「貼paste」在牙齒上才能讓牙齒細菌死光光！</span></p>
<p><span style=""color: #2e8b57;"">paste</span>這個字通常比較常看到是用來當動詞「貼」的意思，<span style=""font-weight: 400;"">但當名詞也可以用來指「膏狀」的東西喔！</span></p>
<p><span style=""font-weight: 400;"">比如說「豆沙」 = <span style=""color: #2e8b57;"">bean paste</span> 就是指膏狀、泥狀物。</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">6</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">刮鬍膏</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>shaving cream</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">這還蠻顧名思義的就是「刮鬍+膏」，</span><span style=""font-weight: 400;"">這時候眼尖的網友一定會問姊: </span></p>
<p><span style=""font-weight: 400;"">「為什麼刮鬍膏不叫 shaving paste而叫<span style=""color: #2e8b57;"">shaving cream</span>呢？」</span></p>
<p><span style=""font-weight: 400;"">因為刮鬍膏通常都是像泡沫慕斯、像是<span style=""color: #2e8b57;"">whip cream的質地，</span><span style=""font-weight: 400;"">所以是用<span style=""color: #2e8b57;"">shaving cream</span>而不是shaving paste唷！</span></span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 595px;"" colspan=""4"">
<p><strong>身體清潔護理</strong></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">7</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">沐浴乳</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>shower gel /</em></span></strong></p>
<p><strong><span style=""color: #2e8b57;""><em>shower cream</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">通常<span style=""color: #2e8b57;"">gel</span>都是半透明的那種凝膠狀的啦，那乳白色那種奶奶的有保濕功效的就會接<span style=""color: #2e8b57;"">cream</span>囉～</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">8</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">去角質(身體&amp;臉部)</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>exfoliator / scrub</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">exfoliate</span>這字本身就是去角質的動詞，把皮膚上面的那一層老皮給去掉，後面加上or就變成名詞去角質(清潔劑)囉！</span>而<span style=""color: #2e8b57;"">scrub</span>的話通常是指那種裡面有顆粒的磨砂膏那一款的，<span style=""font-weight: 400;"">有些去角質只是那種透明可以把一層老皮膚帶掉的沒有顆粒的，</span><span style=""font-weight: 400;"">通常都會用<span style=""color: #2e8b57;"">exfoliator</span>這個字。</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">9</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">洗手乳</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>hand wash / hand soap</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">這個也超直接的，就洗手直翻XD </span></p>
<p><span style=""font-weight: 400;"">或是洗手的肥皂 &ldquo;<span style=""color: #2e8b57;"">hand soap</span>&rdquo;，</span><span style=""font-weight: 400;"">但不瞞您說其實姊在捷運站看過廁所裡面的英文標示居然寫&rdquo;hand washing cream&rdquo;&hellip; </span><span style=""font-weight: 400;"">真是讓人有點傻眼&hellip; </span></p>
<p><span style=""font-weight: 400;"">因為<span style=""color: #2e8b57;"">hand cream</span>是護手霜，</span><span style=""font-weight: 400;"">但前面又硬要加個hand washing&hellip; </span><span style=""font-weight: 400;"">翻完變成四不像，根本不對啦 &gt;&lt;&rdquo;</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">10</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">身體乳液</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>body lotion / cream /</em></span></strong></p>
<p><strong><span style=""color: #2e8b57;""><em>moisturizer</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">剛提到<span style=""color: #2e8b57;"">hand cream是護手霜，</span><span style=""font-weight: 400;"">所以身體乳液當然就是用<span style=""color: #2e8b57;"">body cream</span>準沒錯，</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">lotion</span>的話就是比<span style=""color: #2e8b57;"">cream</span>還要清爽一點的乳液，</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">moisturizer</span>也只是保濕，跟<span style=""color: #2e8b57;"">lotion</span>一樣通常屬於較清爽的質地，</span>而<span style=""color: #2e8b57;"">cream</span>就是較濃稠的霜狀了，建議油性肌的捧油們少用點啊。</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 595px;"" colspan=""4"">
<p><strong>其他沐浴用品</strong></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">11</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">沐浴球</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>shower ball</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">沐浴球的話也非常簡單！直接加ball即可～</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">12</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">刮鬍刀</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>razor</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">飯店附送的那種人工刮鬍刀是用<span style=""color: #2e8b57;"">razor</span>這個字，而<span style=""color: #2e8b57;"">shaver</span>的話指的是電動刮鬍刀喔！</span></p>
</td>
</tr>
<tr>
<td style=""background-color: #66cdaa; width: 17px;"">
<p><span style=""font-weight: 400;"">13</span></p>
</td>
<td style=""width: 70px;"">
<p><span style=""font-weight: 400;"">浴帽</span></p>
</td>
<td style=""width: 114.867px;"">
<p><strong><span style=""color: #2e8b57;""><em>shower cap</em></span></strong></p>
</td>
<td style=""width: 393.133px;"">
<p><span style=""font-weight: 400;"">有時只想洗澡怕淋濕頭髮就可以帶<span style=""color: #2e8b57;"">shower cap</span></span></p>
<p><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">cap</span>這個字本身就是帽子的意思啦～</span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p><span style=""font-weight: 400;"">這些沐浴相關的用品如果記不起全部的話，其實只需要記自己需要使用的就可以囉！</span></p>
<p><span style=""font-weight: 400;"">只不過就姊自己的經驗，通常去北美的旅館住宿，</span></p>
<p><span style=""font-weight: 400;"">tooth brush and tooth paste飯店幾乎是不提供的喔！</span></p>
<p><span style=""font-weight: 400;"">因為北美講求環保，宣稱這種消耗性的拋棄式物品會破壞環境，</span></p>
<p><span style=""font-weight: 400;"">所以會請客人們自行攜帶平常使用的牙膏和牙刷，</span></p>
<p><span style=""font-weight: 400;"">但其實如果你真的忘記帶的話，</span></p>
<p><span style=""font-weight: 400;"">通常櫃台也是會提供販賣的tooth brush and tooth paste，</span></p>
<p><span style=""font-weight: 400;"">只是姊遇過一人份收到 $3塊美金，也就是台幣$100左右(根本坑觀光客嘛！！！)</span></p>
<p><span style=""font-weight: 400;"">為了省錢還是記得自己帶著吧！！！祝各位住宿愉快啊～ Peace out！</span></p>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '"沐浴乳、洗面乳英文怎麼說？ 常見衛浴用品的英文總整理！"','meta_description' => '飯店的清潔用品都用英文標示，到底哪瓶是洗髮精或沐浴乳，常常讓人搞不清，快來學一些簡易的個人清潔用品單字，包準你以後出國不再出糗！','canonical_url' => 'https://tw.english.agency/常見衛浴用品單字整理','feature_image_alt' => '英文學習-盥洗用品單字','feature_image_title' => '常見盥洗用品英文單字','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599a8dce1683b-KI4P6q9M92lUqaF3JopcGVLlfmkob4-5035x2714.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-12 09:31:32"'],
  ['id' => '31','title' => '調整光圈、對焦、英文怎麼說？常用拍照、攝影英文會話分享！','slug' => '拍照-攝影英文怎麼說','excerpt' => '很多人旅遊的時候喜歡東拍西拍，但你知道光圈、對焦等等的專業英文單字怎說嗎？今天我們就來看看這些常見的英語單字吧！','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-weight: 400;"">每天生活的點滴大家都喜歡用照片來記錄，旅遊的時候更喜歡東拍西拍，但你知道光圈、對焦等等的專業英文單字怎說嗎？今天我們就要來看看怎麼說？今天就來跟小邊練習一下這些常見的英語會話吧！這些都是</span><a href=""https://www.goeducation.com.tw/toeic%E6%BA%96%E5%82%99""><span style=""font-weight: 400;"">多益</span></a><span style=""font-weight: 400;"">必考的喔！</span></p>
<p><span style=""font-weight: 400;"">首先，我們先來學有關於拍照、攝影的英文，這些都是相當常見的英語會話：</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>拍(某事物/某人的)照片</strong></h4>
<h4><strong>take a picture (of sth./sb.)</strong></h4>
<h4><strong>= get a picture (of sth./sb.)</strong></h4>
<h4><strong>= snap a picture (sth./sb.)</strong></h4>
<h4>(上述的也都是常用的拍照英文用語喔！)</h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">I would like to take a picture of this cute cat.</span></span></p>
<p><span style=""font-weight: 400;"">(我想要拍一張這隻可愛貓貓的照片。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>幫某人拍照片 take a picture for sb.&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">Can you take a picture for us?</span></span></p>
<p><span style=""font-weight: 400;"">(你可以幫我們拍一張照片嗎？)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>和某事物/某人一起拍照</strong></h4>
<h4><strong>take a picture with sth./sb.</strong></h4>
<h4><strong>=get a picture with sth./sb.&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">I saw my favorite singer on the street today，and I took a picture with him. I was so excited.</span></span></p>
<p><span style=""font-weight: 400;"">(我今天在街上看到我最愛的歌手，而且我也跟他合拍了一張。我超興奮的。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>拍幾張照片</strong><strong>snap a few pictures&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;""> I just want to snap a few pictures of the beautiful scenery on our way to the top of mountain.</span></span></p>
<p><span style=""font-weight: 400;"">(我只想要在我們攻頂的路上拍幾張美麗風景的照片。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>拍某人的照片 take sb&rsquo;s picture&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">I take my younger sister&rsquo;s picture almost every day. She is just adorable.</span></span></p>
<p><span style=""font-weight: 400;"">(我幾乎每天都會拍我妹妹的照片。她真是太可愛了。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>擺姿勢供拍照 pose for a picture&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">Do you know how to pose for a picture like a pro for your photo or selfie?</span></span></p>
<p><span style=""font-weight: 400;"">(你知道如何專業擺姿勢拍照或自拍嗎？)</span></p>
<p>接著我們要來學學各種不同有關調整照片的拍攝英文:</p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>動態照片 action shot&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;"">All of the action shots were taken using hand held cameras.</span></span></p>
<p><span style=""font-weight: 400;"">(所有動態照片全都由手持照相機拍攝而成。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>動作自然的照片 candid shot&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;""> Although taking a candid shot can seem easy，I\'m going to give you a few tips to follow that will help you improve the quality of your shot.</span></span></p>
<p><span style=""font-weight: 400;"">(雖然拍這些動作自然的照片看起來很簡單，我還是會提供一些秘訣幫助你提升你作品的品質。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>近拍照片 close-up shot&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句：<span style=""color: #2e8b57;"">A close - up shot of the actress reveals wrinkles around her eyes.</span></span></p>
<p><span style=""font-weight: 400;"">(近拍照片露出了女演員眼角的皺紋。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>廣角照片 wide shot&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;"">A wide shot like this one can be used in many ways.</span></span></p>
<p><span style=""font-weight: 400;"">(一張像這樣的廣角照片可以有很多不同用途。)</span></p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<p>要拍好一張照片，一定要學會一些動作的攝影英文喔：</p>
<ul style=""text-align: justify;"">
<li style=""font-weight: 400;"">
<h4><strong>把鏡頭的蓋子拿下來 take off the lens cap&nbsp;</strong></h4>
</li>
</ul>
<p style=""text-align: justify;""><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">Pay attention to the proper way to take the lens cap off the lens of your camera.</span></span></p>
<p style=""text-align: justify;""><span style=""font-weight: 400;"">(注意如何正確地把你的相機鏡頭蓋子拿下來。)</span></p>
<ul style=""text-align: justify;"">
<li style=""font-weight: 400;"">
<h4><strong>打開/關掉相機 turn on/off the camera&nbsp;</strong></h4>
</li>
</ul>
<p style=""text-align: justify;""><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">You can\'t even turn the camera on here.</span></span></p>
<p><span style=""font-weight: 400;"">(在這裡你甚至無法打開相機。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>關掉閃光燈 turn off the flash&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">Be sure to turn off the flash when taking the pandas&rsquo; pictures.</span></span></p>
<p><span style=""font-weight: 400;"">(幫熊貓拍照的時候記得關掉閃光燈。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>對焦 focus the lens&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">It&rsquo;s not difficult to manually focus the lens.&nbsp;</span></span><span style=""font-weight: 400;"">(手動對焦不難。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>調整光圈 adjust the aperture&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;""> Is it possible to adjust the aperture on the camera?</span></span></p>
<p><span style=""font-weight: 400;"">(調整相機的光圈有可能嗎？)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>設定快門速度 set the shutter speed&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;""> &ldquo;Shutter Priority&rdquo; mode means you set the shutter speed by yourself.</span></span></p>
<p><span style=""font-weight: 400;"">(「快門優先」模式是說你自己可以設定你要的快門速度。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>設定計時器 set the timer&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句: <span style=""color: #2e8b57;"">You can set the timer when you take a selfie.</span></span></p>
<p><span style=""font-weight: 400;"">(自拍的時候可以設定計時器。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>儲存/刪除照片 save/delete the picture&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;""> Please teach me how to save/delete the picture from this camera.</span></span></p>
<p><span style=""font-weight: 400;"">(請教我從這台相機儲存／刪除照片。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>取出記憶卡 remove the memory card&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句：<span style=""color: #2e8b57;""> I removed the memory card from my camera last time，and I forgot to insert it back.</span></span></p>
<p><span style=""font-weight: 400;"">(我上次把我的記憶卡取出來，然後我就忘記插回去了。)</span></p>
<ul>
<li style=""font-weight: 400;"">
<h4><strong>上傳/下載照片 upload/download the pictures&nbsp;</strong></h4>
</li>
</ul>
<p><span style=""font-weight: 400;"">例句:<span style=""color: #2e8b57;""> I need to teach my mom how to upload/download the pictures from her smart phone.</span></span></p>
<p><span style=""font-weight: 400;"">(我要教我媽如何從手機上傳/下載照片。)</span></p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">學完以上的單字，接下來就是重頭戲了，拍照的時候有時候需要請別人幫忙或者幫別人拍照，相關常用的攝影英文會話又有哪些呢？</span></p>
<ul>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">Can you take a picture for us?</span></strong> (你可以幫我們拍一張照嗎？)</li>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">You cut off my head，can you take one more? </span></strong>(你切掉我的頭了，可以再拍一張嗎？)</li>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">Can you get the coffee shop in the background?</span></strong> (你可以把咖啡館拍進背景裡嗎？)</li>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">Can you all get closer together?</span> </strong>(你們可以靠近一點嗎？)</li>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">Please move a little to the right/left. </span></strong>(請往右邊/左邊移一點。)</li>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">Everyone says cheese. </span></strong>(大家微笑吧。)</li>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">Look at the camera over here. </span></strong>(請看著相機。)</li>
<li style=""font-weight: 400;""><strong><span style=""color: #2e8b57;"">On the count of three. One，two ，three！</span></strong>(數到三，一，二，三！)</li>
</ul>
</div>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '調整光圈、對焦、英文怎麼說?常用拍照、攝影英文會話分享!','meta_description' => '很多人旅遊的時候喜歡東拍西拍，但你知道光圈、對焦等等的專業英文單字怎說嗎?今天我們就來看看這些常見的英語單字吧！','canonical_url' => 'https://tw.english.agency/拍照-攝影英文怎麼說','feature_image_alt' => '英文學習-攝影英文單字','feature_image_title' => '拍照、攝影英文會話','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59a522a38d7c6-2v8PxU0HkW8BY8ZEgZ8HDVpYP45tGr-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2017-06-12 09:40:24"'],
  ['id' => '32','title' => '"NG 片段英文怎麼說？3個只有台灣人在用的英文縮寫！"','slug' => 'NG-英文-縮寫','excerpt' => '今天要跟各位分享，只有本土台灣人才會使用的英文縮寫，不信邪的話，請任意找位英語為母語的人用以下這些縮寫聊聊，包準對方聽得霧煞煞，有聽沒有懂啊！','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-weight: 400;"">今天要跟各位看官們分享的，就是只有本土台灣人自high會使用的英文縮寫！自以為說起來跟ABC一樣英文好好很像好棒棒膩&hellip;</span><span style=""font-weight: 400;"">但全世界英語系國家的人聽到，都只會覺得一頭霧水的</span>假英文&hellip;囧|||「怎麼會呢？ 英文縮寫不是全世界通用的嗎？&hellip;」<span style=""font-weight: 400;"">不信邪的話，請任意找位英語為母語人士用以下這些縮寫聊聊，包準對方是聽得霧煞煞，有聽沒有懂啊！</span></p>
來來來！第一砲首發！
<p>&nbsp;</p>
<hr />
<h1><strong>1.傳單""</strong><strong>ＤＭ</strong><strong>""</strong></h1>
DM什麼意思？ 那<span style=""color: #2e8b57;"">DM原文</span>是什麼？原文其實是來自於<span style=""color: #2e8b57;""> Direct Mail</span>，就是E-mail裡面會收到的廣告信件，其實根本就不是紙本傳單的意思&hellip; 真正廣告傳單請使用下列兩個字：</div>
<div style=""font-size: 18px;""><span style=""color: #2e8b57;""> leaflet</span>：像葉子一樣一張一張的～傳單</div>
<div style=""font-size: 18px;""><span style=""color: #2e8b57;""> flyer/flier</span>：會在路上亂飛的～傳單</div>
<div style=""font-size: 18px;"">發傳單這個動作可以用<span style=""color: #2e8b57;"">hand out /distribute/pass out</span>，發傳單的人就是<span style=""color: #2e8b57;"">distributor</span></div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">例句：</div>
<div style=""font-size: 18px;""><span style=""color: #2e8b57;""> Passenger:Sir，can I get a leaflet/flyer from you?</span> (先生，可以給我一張傳單嗎?)</div>
<div style=""font-size: 18px;""><span style=""color: #2e8b57;""> Flyer distributor: Sure，there you go.</span> (那當然，給你。)</div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">*小解析:各種信件的英文怎麼說？
<ul>
<li><strong><span style=""color: #2e8b57;""> address</span> &nbsp;</strong>地址</li>
<li><strong><span style=""color: #2e8b57;""> money order</span></strong> 匯票</li>
<li><strong><span style=""color: #2e8b57;""> parcel</span></strong>&nbsp;包裹</li>
<li><strong><span style=""color: #2e8b57;""> mail-box</span> &nbsp;</strong>郵筒</li>
<li><strong><span style=""color: #2e8b57;""> regular mail</span></strong>&nbsp; 普通郵件</li>
<li><strong><span style=""color: #2e8b57;""> have a letter registered</span></strong>&nbsp; 寄掛號信</li>
<li><strong><span style=""color: #2e8b57;""> postcard </span>&nbsp;</strong>明信片</li>
<li><strong><span style=""color: #2e8b57;""> registered mail</span> &nbsp;</strong>掛號信</li>
<li><strong><span style=""color: #2e8b57;""> airmail</span></strong>&nbsp; 航空郵件</li>
<li><strong><span style=""color: #2e8b57;""> postage</span></strong>&nbsp; 郵資</li>
<li><strong><span style=""color: #2e8b57;""> post-free</span></strong>&nbsp; 郵資已付</li>
<li><strong><span style=""color: #2e8b57;""> stamp</span> &nbsp;</strong>郵票</li>
<li><strong><span style=""color: #2e8b57;""> weigh a parcel</span></strong>&nbsp; 稱量包裹</li>
<li><strong><span style=""color: #2e8b57;""> zip code</span></strong>&nbsp; 郵遞區號</li>
</ul>
<p>&nbsp;</p>
<hr />
<h1><strong>2. ""</strong><strong>ＮＧ""</strong><strong>&nbsp;片段</strong></h1>
<span style=""color: #2e8b57;"">NG的原文</span>是啥？很簡單，就是<span style=""color: #2e8b57;"">Not Good</span>，聽起來很合理是吧～不過你把原文放在句子裡說說看 ""Hey，did you see the Not Good scenes from Harry Potter? They&rsquo;re really funny!"" 聽起來是不是有夠 Chinglish!? (中式英文) <span style=""color: #ff0000;"">英文用來形容NG片段，拜託請用<strong>bloopers</strong>這個字！ Bloopers 用來形容not good的片段，也稱作NG takes較常用bloopers</span>。 <span style=""color: #2e8b57;"">cut</span>就是指拍攝中斷的意思，也就是&rdquo;卡&rdquo;
<p><span style=""color: #2e8b57;"">Did you see the bloopers from Harry Potter? They&rsquo;re really funny!</span>(你有看哈利波特的NG片段嗎？真得很好笑耶！) 你看看是不是好多惹？</p>
<p>*小解析: 常用的電影單字</p>
<h3><strong>1.各種電影的英文</strong></h3>
<ul>
<li><strong><span style=""color: #2e8b57;""> Action films</span> &nbsp;動作片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Adventure films </span>&nbsp;冒險片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Children&rsquo;s films</span> &nbsp;兒童電影</strong></li>
<li><strong><span style=""color: #2e8b57;""> Comedy films</span>&nbsp;喜劇電影</strong></li>
<li><strong><span style=""color: #2e8b57;""> Crime films</span>&nbsp;犯罪電影</strong></li>
<li><strong><span style=""color: #2e8b57;""> Detective films</span>&nbsp;偵探片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Disaster films</span> &nbsp;災難片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Documentary films</span> &nbsp;紀錄片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Drama films</span>&nbsp;劇情片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Heist films</span>&nbsp;搶劫片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Historical films </span>&nbsp;歷史片</strong></li>
<li><strong><span style=""color: #2e8b57;""> Hood films</span>&nbsp;兄弟片(美國黑人)</strong></li>
<li><strong><span style=""color: #2e8b57;""> Horror films</span> &nbsp;恐怖片</strong></li>
</ul>
<p>&nbsp;</p>
<h3><strong>2.劇組、幕後工作人員</strong></h3>
<ul>
<li><strong><span style=""color: #2e8b57;""> behind the scenes</span> &nbsp;</strong>幕後花絮</li>
<li><strong><span style=""color: #2e8b57;""> sponsor the manufacturer</span></strong>&nbsp; 贊助廠商<strong>　</strong></li>
<li><strong><span style=""color: #2e8b57;""> special efficacy</span></strong>&nbsp; 特效<strong>　</strong></li>
<li><strong><span style=""color: #2e8b57;""> incidental music</span></strong>&nbsp; 配樂</li>
<li><strong><span style=""color: #2e8b57;""> play wright</span></strong><strong> &nbsp;</strong>編劇<strong>　</strong></li>
<li><strong><span style=""color: #2e8b57;""> Actualite </span>&nbsp;</strong>實況電影</li>
<li><strong><span style=""color: #2e8b57;""> Back Lot</span></strong>&nbsp; 布景</li>
<li><strong><span style=""color: #2e8b57;""> Barn Doors</span></strong>&nbsp; 拉板</li>
<li><strong><span style=""color: #2e8b57;""> Cameraman</span></strong>&nbsp; 攝影師</li>
<li><strong><span style=""color: #2e8b57;""> Camera Operator</span></strong>&nbsp; 攝影機操作員</li>
<li><strong><span style=""color: #2e8b57;""> Gaffer</span> &nbsp;</strong>燈光師</li>
<li><strong><span style=""color: #2e8b57;""> Take</span></strong>&nbsp; 鏡次</li>
</ul>
<p>&nbsp;</p>
<hr />
<h1><strong>3.內心獨白</strong><strong>&nbsp;""ＯＳ""</strong></h1>
<p><strong><span style=""color: #2e8b57;"">OS = 內心獨白</span>，</strong>在看英文影集的時候，打開英文字幕，你絕對不會看到字幕上出現OS這兩個大字！！！ 真正OS內心獨白英文用的字叫做 &nbsp;Soliloquy [səˋlɪləkwɪ]不過你可能在看小說或漫畫的時候，寫主角正在內心獨白說「&hellip;」的時候，也不一定會用這麼正式的字，反而可能會直接寫說：<span style=""color: #2e8b57;"">Thinking to herself&hellip; ""I have to let you know Ruo Yu is your legitimate daughter!!!""</span></p>
<p><strong>*</strong>小解析：<strong><span style=""color: #2e8b57;""> monologue</span> </strong>VS<strong> <span style=""color: #2e8b57;"">soliloquy</span></strong></p>
<p><span style=""font-weight: 400;"">這兩個字都翻譯作獨白，但真的在專業領域上使用上這兩個字時，是有相當的差異的:</span></p>
<p>1.<strong><span style=""color: #2e8b57;""> monologue</span></strong>：通常指一個腳色內心的獨白，但是是對觀眾所說，如同電影死侍打破第四道牆。</p>
<p>2.<strong><span style=""color: #2e8b57;""> soliloquy</span></strong>：&nbsp;通常指一個腳色的獨白，但是對自己說話，並不會與觀眾互動。</p>
</div>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '"NG 片段英文怎麼說？3個只有台灣人在用的英文縮寫！"','meta_description' => '今天要跟各位分享，只有本土台灣人才會使用的英文縮寫，不信邪的話，請任意找位英語為母語的人用以下這些縮寫聊聊，包準對方聽得霧煞煞，有聽沒有懂啊！','canonical_url' => 'https://tw.english.agency/NG-英文-縮寫','feature_image_alt' => '英文怎麼說-3個英文縮寫','feature_image_title' => 'NG片段英文怎麼說','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59a90ac6c9502-N2ahqYhPAzL2k4nfKbXPUflW5S7H0C-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2017-06-12 09:47:36"'],
  ['id' => '33','title' => '"多益必考的""hand""用法"','slug' => '多益必考的hand用法','excerpt' => '"有時候，當你走在路上遇到外國人，他可能對你說“Can you give me a hand?”這是什麼意思?是指「你可以給我一隻手嗎？」還是到底對方想要表達什麼呢？"','content' => '"<p><span style=""font-weight: 400;"">有時候，當你走在路上遇到外國人，他/她可能突然對你說了一句</span></p>
<p><span style=""font-weight: 400;"">&ldquo;<span style=""color: #0000cd;""><strong>Can you give me a hand</strong>?</span>&rdquo;</span></p>
<p><span style=""font-weight: 400;"">這是什麼意思？</span><span style=""font-weight: 400;"">是指「你可以給我一隻手嗎？」</span></p>
<p><span style=""font-weight: 400;"">此時你可能會一頭霧水，怎麼會突然被要求說自己能給對方一隻手嗎？</span></p>
<p><span style=""font-weight: 400;"">還是到底對方想要表達什麼？</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">對方真正的意思是要請你「<span style=""color: #0000cd;""><strong>幫他/她一個忙</strong></span>」！</span></p>
<p><span style=""font-weight: 400;"">hand 是「手」，這是一個再簡單不過的字彙，</span></p>
<p><span style=""font-weight: 400;"">但是它有一些在國際職場裡的延伸用法。</span></p>
<p><span style=""font-weight: 400;"">要幫助別人就必須要伸出手來給予協助，所以 hand 又可以指「幫助」，</span></p>
<p><span style=""font-weight: 400;"">而&nbsp;<strong><span style=""color: #0000cd;"">give somebody a hand</span>&nbsp;</strong>即為<span style=""color: #0000cd;"">幫助某人</span>，</span></p>
<p><span style=""font-weight: 400;"">而<span style=""color: #0000cd;""> <strong>need/want a hand </strong></span>即為<span style=""color: #0000cd;"">需要幫助</span>。</span></p>
<p><span style=""font-weight: 400;"">例句：Would you need a hand with searching the information you need?</span></p>
<p><span style=""font-weight: 400;"">（你需要人協助你找你需要的資料嗎？）</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">在公司開會中，有人在台上做簡報（presentation）</span></p>
<p><span style=""font-weight: 400;"">你的老闆說：<span style=""color: #0000cd;""><strong>Let&rsquo;s give her a big hand.</strong></span></span></p>
<p><span style=""font-weight: 400;"">字面上看起來是「讓我們給她一個大手。」</span></p>
<p><span style=""font-weight: 400;"">但是正確的意思是，</span><span style=""font-weight: 400;"">你的老闆覺得做簡報的人說得很好，</span></p>
<p><span style=""font-weight: 400;"">所以他要大家給她「<span style=""color: #0000cd;""><strong>拍手、鼓掌</strong></span>」。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">另外，你也可能聽過人家說 <span style=""color: #0000cd;""><strong>on the other hand</strong></span>。</span></p>
<p><span style=""font-weight: 400;"">On the other hand 正確的意思是「<span style=""color: #0000cd;""><strong>另一方面/就另一方面來說</strong></span>」。</span></p>
<p><span style=""font-weight: 400;"">例如：On the other hand ，inconstancy often results in failure. </span></p>
<p><span style=""font-weight: 400;"">(反之，缺乏恆心通常會導致失敗。)</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">Hand 在小時候學習英文的時候很早就學到了，</span></p>
<p><span style=""font-weight: 400;"">但是 hand 的用法在國際上卻還是有許多其他的用法。</span></p>
<p><span style=""font-weight: 400;"">下次當別人對你再說 &ldquo;Would you please give me a hand?&rdquo;的時候，</span></p>
<p><span style=""font-weight: 400;"">千萬不要再以為是在跟你講說「你可以給我一隻手嗎？」囉！</span></p>"','published' => '1','og_title' => '"多益必考的""hand""用法"','og_description' => '"有時候，當你走在路上遇到外國人，他可能對你說“Can you give me a hand?”這是什麼意思?是指「你可以給我一隻手嗎？」還是到底對方想要表達什麼呢？"','meta_title' => '"多益必考的""hand""用法"','meta_description' => '"有時候，當你走在路上遇到外國人，他可能對你說“Can you give me a hand?”這是什麼意思?是指「你可以給我一隻手嗎？」，還是到底對方想要表達什麼呢？"','canonical_url' => 'https://tw.english.agency/多益必考的hand用法','feature_image_alt' => '英文怎麼說-hand用法','feature_image_title' => 'hand用法','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59928d3b39c8d-M83cBpP0E04t3PEUTQInOxh3P72QGt-4167x2500.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-13 02:51:53"'],
  ['id' => '34','title' => '"不爽的英文怎麼說？ 九句口語教你輕鬆表達憤怒！"','slug' => '不爽的英文怎麼說-九句口語教你','excerpt' => '"相信大家一定都有心情特別糟的時候，但偏偏又有人來跟你開玩笑，心情糟到極點的你實在是笑不出來，這時又想表達自己的憤怒時該怎麼辦呢？ 別擔心，快牢記以下幾句口語，讓你輕鬆表達你的不悅！"','content' => '"<p><span style=""font-weight: 400;"">相信大家一定都有心情特別糟的時候，但偏偏又有人來跟你開玩笑，</span></p>
<p><span style=""font-weight: 400;"">心情糟到極點的你實在是笑不出來，這時又想表達自己的憤怒時該怎麼辦呢？ </span></p>
<p><span style=""font-weight: 400;"">別擔心，快牢記以下幾句口語，讓你輕鬆表達你的不悅！</span></p>
<p>&nbsp;</p>
<h4><strong>句型一: You let me down.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「你讓我失望了」</span></p>
<p><span style=""font-weight: 400;"">當某人做了某件事讓你非常失望時你就可以說這句話，</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #0000cd;"">down </span>可以解釋成心情失落、沮喪的意思。</span></p>
<p><span style=""font-weight: 400;"">另外，當你看到朋友鬱鬱寡歡、情緒低落時你可以用</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #0000cd;"">Why are you feeling down?</span></span></p>
<p><span style=""font-weight: 400;"">來問他發生什麼事喔！</span></p>
<p>&nbsp;</p>
<h4><strong>句型二: I don&rsquo;t give a shit.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「我一點都不在乎！」</span></p>
<p><span style=""font-weight: 400;"">從字面上來看就是連個屎都不給的意思，引申義就是不屑一顧，</span></p>
<p><span style=""font-weight: 400;"">當某人在你背後說你的壞話就可以說這句話來表達你的灑脫，</span></p>
<p><span style=""font-weight: 400;"">如果覺得<span style=""color: #0000cd;"">shit</span>太粗俗，可以換成<span style=""color: #0000cd;"">damn</span>，</span></p>
<p><span style=""font-weight: 400;"">整句話就會變成<span style=""color: #0000cd;""> I don&rsquo;t give a damn.</span></span></p>
<p>&nbsp;</p>
<h4><strong>句型三: You&rsquo;re getting on my nerves.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「你惹毛我了！」</span></p>
<p><span style=""font-weight: 400;"">照字面上看來是你碰到我的神經的意思，碰到神經一定會很痛，</span></p>
<p><span style=""font-weight: 400;"">所以引申為你讓我不開心的意思。</span></p>
<p><span style=""font-weight: 400;"">還有一個類似的說法是 <span style=""color: #0000cd;"">Jump on my back. </span></span></p>
<p><span style=""font-weight: 400;"">意思也是惹毛別人，因為就像有人在你背上跳啊跳，想當然會惹毛你囉！</span></p>
<p>&nbsp;</p>
<h4><strong>句型四: You make me sick!</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「你讓我感到噁心！」</span></p>
<p><span style=""font-weight: 400;"">假如有個男生有女朋友，但又去搭訕別的女生，</span></p>
<p><span style=""font-weight: 400;"">這時你就可以對他說 </span></p>
<p><span style=""font-weight: 400;""><span style=""color: #0000cd;"">1.You make me sick！</span></span></p>
<p><span style=""font-weight: 400;""><span style=""color: #0000cd;"">2.People like you make me sick！</span></span></p>
<p><span style=""font-weight: 400;"">(像你這樣的人讓我感到噁心)</span></p>
<p><span style=""font-weight: 400;"">來表達你對他的反感與不滿喔！</span></p>
<p>&nbsp;</p>
<h4><strong> 句型五: Knock it off.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「停下來，不要鬧了」</span></p>
<p><span style=""font-weight: 400;"">如果有個人開玩笑開得太過火了讓你很不開心，</span></p>
<p><span style=""font-weight: 400;"">你就可以對他說這句話，相關的說法還有</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #0000cd;"">1.cut in out </span></span></p>
<p><span style=""font-weight: 400;""><span style=""color: #0000cd;"">2.stop it</span></span></p>
<p><span style=""font-weight: 400;"">都可以用來制止別人做讓你反感的事喔!</span></p>
<p>&nbsp;</p>
<h4><strong>句型六: Leave me alone.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「別打擾我，讓我一個人靜一靜」</span></p>
<p><span style=""font-weight: 400;"">比較沒禮貌的說法是<span style=""color: #0000cd;""> Go away！</span></span></p>
<p><span style=""font-weight: 400;"">有點像是中文說「走開」的意思，</span></p>
<p><span style=""font-weight: 400;"">當有個人在你心情不好又一直煩你時，你就可以這樣說喔！</span></p>
<p>&nbsp;</p>
<h4><strong>句型七: You piss me off.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「你惹毛我了、你氣死我了」</span></p>
<p><span style=""font-weight: 400;"">相信大家應該很常在電影裡聽過這句話，</span></p>
<p><span style=""font-weight: 400;"">這句話和句型三使用的情況還蠻類似的，</span></p>
<p><span style=""font-weight: 400;"">一樣都是用來表達自己很生氣，當某人對你很無禮時，</span></p>
<p><span style=""font-weight: 400;"">你就可以用這句話來表達你的憤怒！</span></p>
<p>&nbsp;</p>
<h4><strong>句型八: I got the short end of the stick.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「這是我遇過最糟的情況了」</span></p>
<p><span style=""font-weight: 400;"">有點類似諺語屋漏偏逢連夜雨的感覺，就是衰到不能再衰的心情，</span></p>
<p><span style=""font-weight: 400;"">比如說你今天出門時突然下雨，害你淋的整身濕，</span></p>
<p><span style=""font-weight: 400;"">去上班時又踩到狗屎，鑰匙還掉了，這時你就可以說</span></p>
<p><span style=""font-weight: 400;""><span style=""color: #0000cd;"">I got the short end of the stick. </span></span></p>
<p><span style=""font-weight: 400;"">來表達你一整天下來所經歷過的事有多衰。</span></p>
<p>&nbsp;</p>
<h4><strong>句型九: Today is not my day.</strong></h4>
<p><span style=""font-weight: 400;"">解析: 這句話是「我今天真倒楣」</span></p>
<p><span style=""font-weight: 400;"">可以用來表達一整天都遇到很倒楣的事的心情，</span></p>
<p><span style=""font-weight: 400;"">有點類似句型八的感覺。</span></p>
<p><span style=""font-weight: 400;"">相反義：</span><span style=""font-weight: 400;""><span style=""color: #0000cd;"">Something/Someone made my day.</span></span></p>
<p><span style=""font-weight: 400;"">就可以用來形容什麼事情讓你心情很好，讓你很開心。</span></p>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '"不爽的英文怎麼說? 九句口語教你輕鬆表達憤怒!"','meta_description' => '"相信大家一定都有心情特別糟的時候，但偏偏又有人來跟你開玩笑，心情糟到極點的你實在是笑不出來，這時又想表達自己的憤怒時該怎麼辦呢? 別擔心，快牢記以下幾句口語，讓你輕鬆表達你的不悅！"','canonical_url' => 'https://tw.english.agency/不爽的英文怎麼說-九句口語教你','feature_image_alt' => '英文學習-九句口語','feature_image_title' => '不爽的英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599569f723269-eyqVdpvbu2TCWEDEcUfFUL9QuV6VG9-4167x2500.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-13 04:35:11"'],
  ['id' => '35','title' => '單寧、麂皮、法蘭絨怎麼說？你必須要知道的材質英文！','slug' => '材質相關英文單字','excerpt' => '不知道怎麼用英文形容衣服嗎？今天就跟著小編來學習多益閱讀中常考的一些衣服材質款式的英文，保證下次你到服裝店就可以非常輕鬆買到你想要的衣服！','content' => '"<p><span style=""font-weight: 400;"">去逛街買衣服的時候，總是不知道要怎麼形容衣服嗎？ </span></p>
<p><span style=""font-weight: 400;"">我要一件穿起來舒服又涼爽的？ 我想要一件穿起來酷酷的？&nbsp;</span></p>
<p><span style=""font-weight: 400;"">我想要一件比紅色還要深的？我想要一件比黃色還要亮的？</span></p>
<p><span style=""font-weight: 400;"">等你形容完，可能店員都快昏倒啦！</span></p>
<p><span style=""font-weight: 400;"">今天就跟著小編來學習多益閱讀</span><span style=""font-weight: 400;"">中常考的一些衣服材質款式的英文</span></p>
<p><span style=""font-weight: 400;"">保證下次你到服裝店就可以非常輕鬆買到你想要的衣服喔！</span></p>
<p><span style=""font-weight: 400;"">首先，衣服有很多材質，我們先來看看有哪些～</span></p>
<h4><strong>1.衣服材質：</strong></h4>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">cotton </span></span><span style=""font-weight: 400;"">➩ 棉質</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">denim </span></span><span style=""font-weight: 400;"">➩ 單寧</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">silk </span></span><span style=""font-weight: 400;"">➩ 絲質</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">wool </span></span><span style=""font-weight: 400;"">➩ 羊毛</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">velvet </span></span><span style=""font-weight: 400;"">➩ 絲絨</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">cotton blend </span><span style=""font-weight: 400;"">➩ 混棉</span></span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">satin</span> </span><span style=""font-weight: 400;"">➩ 緞質</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">spandex</span> </span><span style=""font-weight: 400;"">➩ 彈性人造纖維</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">linen </span></span><span style=""font-weight: 400;"">➩ 亞麻</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">flannel </span></span><span style=""font-weight: 400;"">➩ 法蘭絨</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">leather </span><span style=""font-weight: 400;"">➩ 皮</span></span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">composition leather </span></span><span style=""font-weight: 400;"">➩ 合成皮</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">suede</span> </span><span style=""font-weight: 400;"">➩ 麂皮</span></li>
</ul>
<p>【補充】 如果你需要形容一件衣服的話，就可以這樣說：</p>
<p>〈1〉<span style=""color: #5599ff;"">a leather jacket&nbsp;</span>(一件皮外套) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p>〈2〉<span style=""color: #5599ff;"">a flannel shirt</span> (一件法蘭絨襯衫) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p>〈3〉<span style=""color: #5599ff;"">a denim jeans</span> (一件單寧牛仔褲) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p>材質後面直接加上你需要的衣服款式就好了！</p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">接下來要介紹的就是衣服的顏色，這可是各位小資女的特別才藝</span></p>
<p><span style=""font-weight: 400;"">可能對男生來說，紫紅色和紅色都是一樣的</span></p>
<p><span style=""font-weight: 400;"">不過對於女生們來說這可是相差的天南地北呢！</span></p>
<p><span style=""font-weight: 400;"">一些常見的衣服顏色怎麼說呢～</span></p>
<h4><strong>2.衣服顏色：</strong></h4>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">burgundy</span> </span><span style=""font-weight: 400;"">➩ 酒紅色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">maroon</span> </span><span style=""font-weight: 400;"">➩ 褐紫紅色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">pink </span></span><span style=""font-weight: 400;"">➩ 粉紅色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">sunflower</span></span><span style=""font-weight: 400;"">➩ 亮黃色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">lime green</span> </span><span style=""font-weight: 400;"">➩檸檬綠</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">teal </span></span><span style=""font-weight: 400;"">➩ 藍綠色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">kelly green</span> </span><span style=""font-weight: 400;"">➩ 鮮綠色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">forest green</span> </span><span style=""font-weight: 400;"">➩ 暗綠色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">sky blue </span></span><span style=""font-weight: 400;"">➩ 天藍色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">navy</span> </span><span style=""font-weight: 400;"">➩ 深藍色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">turquoise </span></span><span style=""font-weight: 400;"">➩ 土耳其藍</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">light blue </span></span><span style=""font-weight: 400;"">➩ 淺藍色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">off-white</span> </span><span style=""font-weight: 400;"">➩ 米白色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">khaki</span> </span><span style=""font-weight: 400;"">➩ 卡其色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">violet </span></span><span style=""font-weight: 400;"">➩ 紫羅蘭色</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">gray</span>(美式)/<span style=""color: #0000cd;"">grey</span>(英式) </span><span style=""font-weight: 400;"">➩ 灰色</span></li>
</ul>
<p><span style=""font-weight: 400;"">【補充】</span></p>
<p><span style=""font-weight: 400;"">■ 深藍色navy還有海軍的意思。</span></p>
<p><span style=""font-weight: 400;"">像是以狙擊賓拉登之役而聲名大噪的美國海豹部隊，原名為Navy Seals (SEa，Air，Land)</span></p>
<p><span style=""font-weight: 400;"">■ 英文中有很多關於顏色的idioms (俚語)，提供一些比較常聽到的：</span></p>
<p><span style=""font-weight: 400;"">〈1〉<span style=""color: #5599ff;"">green thumb </span></span><span style=""font-weight: 400;""> (綠色大拇指) － 園藝技巧精湛 </span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp;例句：James likes planting. He really has a </span><span style=""font-weight: 400;"">green thumb</span><span style=""font-weight: 400;"">.</span></p>
<p><span style=""font-weight: 400;"">〈2〉<span style=""color: #5599ff;"">blue blood </span></span><span style=""font-weight: 400;""> (藍色血液) － 貴族血統 </span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp;例句： Jacky is a real </span><span style=""font-weight: 400;"">blue blood</span><span style=""font-weight: 400;"">.</span></p>
<p><span style=""font-weight: 400;"">〈3〉<span style=""color: #5599ff;"">black sheep </span></span><span style=""font-weight: 400;""> (黑羊) － 害群之馬 </span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp;例句： John is the </span><span style=""font-weight: 400;"">black sheep</span><span style=""font-weight: 400;""> in his basketball team; </span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;he lost more than 20 points.</span></p>
<p><span style=""font-weight: 400;"">〈4〉<span style=""color: #5599ff;"">white lie </span></span><span style=""font-weight: 400;""> (白色謊言) － 善意的謊言 </span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp;例句：Sometimes we are forced to tell </span><span style=""font-weight: 400;"">white lies</span><span style=""font-weight: 400;"">.</span></p>
<p><span style=""font-weight: 400;"">〈5〉<span style=""color: #5599ff;"">gray/grey area </span></span><span style=""font-weight: 400;""> (灰色地帶) － 模稜兩可 </span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp;例句：There is a </span><span style=""font-weight: 400;"">gray/grey area</span><span style=""font-weight: 400;""> in this law. </span></p>
<p><span style=""font-weight: 400;"">〈6〉<span style=""color: #5599ff;"">golden &nbsp;opportunity </span></span><span style=""font-weight: 400;""> (金色機會) － 絕佳的機會 </span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp;例句：Jack has a </span><span style=""font-weight: 400;"">golden opportunity</span><span style=""font-weight: 400;""> to win the championship.</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">還有最重要的花樣，當然也要一起學</span></p>
<h4><strong>3.衣服花樣：</strong></h4>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">plain</span> </span><span style=""font-weight: 400;"">➩ 無花紋的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">solid </span></span><span style=""font-weight: 400;"">➩ 單色的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">patterned</span> </span><span style=""font-weight: 400;"">➩ 有圖案的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">striped </span></span><span style=""font-weight: 400;"">➩ 有條紋的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">pinstriped </span></span><span style=""font-weight: 400;"">➩ 細條紋</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">plaid</span> </span><span style=""font-weight: 400;"">➩ 格子圖案的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">embroidered</span> </span><span style=""font-weight: 400;"">➩ 繡花紋的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">flowered/flowery/floral print</span> </span><span style=""font-weight: 400;"">➩ 碎花圖案的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">polka-dotted</span> </span><span style=""font-weight: 400;"">➩ 有(小)點點的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">spotted </span></span><span style=""font-weight: 400;"">➩ 有(大)點點的</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">camouflage</span> </span><span style=""font-weight: 400;"">➩ 迷彩的</span><span style=""font-weight: 400;""> &nbsp;</span></li>
</ul>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">最後，當然要補充一下，服飾店裡面會出現的英文單字用法：</span></p>
<h4><strong>4.服飾店英文</strong></h4>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">window display </span></span><span style=""font-weight: 400;"">➩ 櫥窗展示</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">salesperson </span></span><span style=""font-weight: 400;"">➩ 銷售人員</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">fitting/dressing room </span></span><span style=""font-weight: 400;"">➩ 試衣間</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">check-out counter</span> </span><span style=""font-weight: 400;"">➩ 結帳櫃檯</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">cash register</span> </span><span style=""font-weight: 400;"">➩ 收銀機</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">hanger</span> </span><span style=""font-weight: 400;"">➩ 衣架</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">security sensor</span> </span><span style=""font-weight: 400;"">➩ 防盜感應器</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""color: #0000cd;"">security tag</span> </span><span style=""font-weight: 400;"">➩ 防盜標籤</span></li>
</ul>
<p><span style=""font-weight: 400;"">【補充】</span></p>
<p><span style=""font-weight: 400;"">常見的日常服飾店用語：</span></p>
<p><span style=""font-weight: 400;"">〈1〉<span style=""color: #5599ff;"">Clerk: May I help you ? &nbsp;&nbsp;Answer: I\'m just looking around &nbsp;&nbsp;&nbsp;</span></span></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp;(店員：需要幫忙嗎 &nbsp;&nbsp;回答：我隨便看看)</span></p>
<p><span style=""font-weight: 400;"">〈2〉 <span style=""color: #5599ff;"">May I try this on? </span>(我可以試穿這件嗎？)</span></p>
<p><span style=""font-weight: 400;"">〈3〉 <span style=""color: #5599ff;"">Where is the fitting room?</span> (請問試衣間在哪裡？)</span></p>
<p><span style=""font-weight: 400;"">〈4〉&nbsp;<span style=""color: #5599ff;"">It doesn\'t fit me/ It\'s not my size.</span> (我不適合/不是我的size)</span></p>
<p><span style=""font-weight: 400;"">〈5〉<span style=""color: #5599ff;"">Can I have the next size up/down?</span> (我可以試大/小一號的嗎？)</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">現在這些常見的多益英文你都了解嗎？</span></p>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '單寧、麂皮、法蘭絨怎麼說?你必須要知道的材質英文!','meta_description' => '今天就跟著小編來學習多益閱讀中常考的一些衣服材質款式的英文，保證下次你到服裝店就可以非常輕鬆買到你想要的衣服！','canonical_url' => 'https://tw.english.agency/材質相關英文單字','feature_image_alt' => '英文學習-材質相關英文單字','feature_image_title' => '材質英文介紹','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59953cb90e3cc-EFDD9RWMabnuep1FurokbaIMHHWSWN-4167x2500.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-13 08:05:06"'],
  ['id' => '36','title' => '你很龜毛！英文怎麼說？5句實用流行語！','slug' => '5句實用英文流行語','excerpt' => '學校英文教科書看來看去就是一大堆單字還有對話以及閱讀測驗，有時候看久了還真的希望能夠學點不太一樣的英文！今天我們就來學學一些平常在學校會用到的實用英文句子吧！','content' => '"<p>學校英文教科書看來看去就是一大堆單字還有對話以及閱讀測驗，</p>
<p>有時候看久了還真的希望能夠學點不太一樣的英文呢！</p>
<p>今天我們就來學學一些平常在學校會用到的這些句子， 英文怎麼說吧？</p>
<p>&nbsp;</p>
<h4><strong>1.Stop making excuses. 不要再硬拗了啦！/ 不要在掰了！</strong></h4>
<p><span style=""font-weight: 400;"">有時候學生忘記帶課本或者是功課，就想東想西，一下子說忘了帶，</span></p>
<p><span style=""font-weight: 400;"">一下子說被家裡的狗狗給啃了，就是不肯誠實的說沒有寫功課或者是就是課本拿去墊泡麵了。</span></p>
<p><span style=""font-weight: 400;"">所以當遇到這樣的情形，一直再硬拗一些理由或者是藉口，但也可以用以下幾句片語來說明喔！</span></p>
<p><span style=""color: #0000cd;""><span style=""font-weight: 400;"">1.make thing up.</span></span></p>
<p><span style=""color: #0000cd;""><span style=""font-weight: 400;"">2.make stuff up .</span></span></p>
<p><span style=""color: #0000cd;""><span style=""font-weight: 400;"">3.tell a story .</span></span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">A: I swear I have done my homework，I just forgot where I put it.</span></em></p>
<p><span style=""font-weight: 400;"">(我發誓我真的寫完功課了，我只是忘記放哪裡了。)</span></p>
<p><em><span style=""font-weight: 400;"">B: Please</span></em><em><span style=""font-weight: 400;"">，</span></em><em><span style=""font-weight: 400;""><span style=""color: #0000cd;"">stop making excuses.</span></span></em></p>
<p><span style=""font-weight: 400;"">(拜託，<span style=""color: #0000cd;"">不要再硬拗了啦。</span>)</span></p>
<p>&nbsp;</p>
<h4><strong>2.</strong><strong>You are so anal！</strong><strong>你很龜毛耶！</strong></h4>
<p><span style=""font-weight: 400;"">每個人的要求不太一樣，有時候在學校，老師對於學生的作業，</span></p>
<p><span style=""font-weight: 400;"">或者是分數的要求更會跟學生有所差異，甚至有些要求完美的老師，</span></p>
<p><span style=""font-weight: 400;"">更是希望學生有很好的表現，這時候就會針對學生做出更多的要求，</span></p>
<p><span style=""font-weight: 400;"">這時候就會說這樣的人很龜毛，要求比較完美的人也不是不好，</span></p>
<p><span style=""font-weight: 400;"">只是有時候還真的會令人受不了，不過如果直接翻譯成</span><span style=""font-weight: 400;"">&rdquo;turtle hair&rdquo;</span><span style=""font-weight: 400;"">的話，</span></p>
<p><span style=""font-weight: 400;"">全台灣的英文老師應該也會受不了，這句話的英文就會這樣說:</span></p>
<p><span style=""font-weight: 400;"">&ldquo;<span style=""color: #0000cd;"">Anal</span>&rdquo;這個字原本是「肛門的」意思，</span></p>
<p><span style=""font-weight: 400;"">不過在這裡解釋成「<span style=""color: #0000cd;"">龜毛</span>」，</span><span style=""font-weight: 400;"">也可以用&rdquo;<span style=""color: #0000cd;"">picky</span>&rdquo;來形容喔！</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">A: Our English teacher is asking us to hand in each assignment on time.</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; (我們的英文老師要求每次交作業都要準時！)</span></p>
<p><em><span style=""font-weight: 400;"">B: You teache is so&nbsp;<span style=""color: #0000cd;"">anal</span>!</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; (你的老師好<span style=""color: #0000cd;"">龜毛</span>喔！)</span></p>
<p>&nbsp;</p>
<h4><strong>3.</strong><strong>You are an ass！</strong><strong>你太機車啦！</strong></h4>
<p><span style=""font-weight: 400;"">如果你要形容一個人很討厭，或者是很挑剔或是意見很多的話，</span></p>
<p><span style=""font-weight: 400;"">中文我們會說這個人好機車喔，這句話的英文就會這樣說:</span></p>
<p><span style=""font-weight: 400;"">&ldquo;Ass&rdquo;這個字原本是「屁股」的意思喔！</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">A: <span style=""color: #0000cd;"">You are an ass!</span> You have asked too much from your kids.</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp;(<span style=""color: #0000cd;"">你好機車喔！</span>你對你的小孩也要求太多了吧！)</span></p>
<p><em><span style=""font-weight: 400;"">B: What did you say？</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; (你說什麼？)</span></p>
<p>&nbsp;</p>
<h4><strong>4. </strong><strong>Don&rsquo;t act dumb.</strong><strong>不要給我裝傻！</strong></h4>
<p><span style=""font-weight: 400;"">有時候有人就是喜歡「裝傻」，什麼事情都裝不知道，</span></p>
<p><span style=""font-weight: 400;"">問他甚麼事情也只會回答「不知道」，這時候就想要一巴掌&hellip;</span></p>
<p><span style=""font-weight: 400;"">搖醒他，讓他清醒一點，這句話的英文就會這樣說:</span></p>
<p><span style=""font-weight: 400;"">&ldquo;act&rdquo;有「扮演」的意思，&rdquo;<span style=""color: #0000cd;"">dumb</span>&rdquo;有「<span style=""color: #0000cd;"">啞的</span>」的意思，</span></p>
<p><span style=""font-weight: 400;"">所以字面上來說是叫人家不要扮演啞的人，其實就是叫人家不要「裝笑維」啦！</span></p>
<p><span style=""font-weight: 400;"">例句:</span></p>
<p><em><span style=""font-weight: 400;"">A: <span style=""color: #0000cd;"">Don&rsquo;t act dumb.</span> You already promised me to take me to the movies.</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; (<span style=""color: #0000cd;"">不要裝傻了！</span>你已經答應我要帶我去看電影。)</span></p>
<p><em><span style=""font-weight: 400;"">B: Did I ?</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; (有嗎？)</span></p>
<p>&nbsp;</p>
<h4><strong>5.</strong><strong>He is in a cranky mood.&nbsp;</strong><strong>他心情不好。</strong></h4>
<p><span style=""font-weight: 400;"">有時候心情不好，你只會用&rdquo;He is in a bad mood.&rdquo;嗎？</span></p>
<p><span style=""font-weight: 400;"">心情不好的時候，真希望大家都可以閃得遠遠的，</span></p>
<p><span style=""font-weight: 400;"">這句話的英文就會這樣說 ""<span style=""color: #0000cd;"">Somebody in a cranky mood.</span>""</span></p>
<p><span style=""font-weight: 400;"">&ldquo;<span style=""color: #0000cd;"">cranky</span>&rdquo;這個單字有「<span style=""color: #0000cd;"">胡思亂想的、好奇的、發瘋似的</span>」</span><span style=""font-weight: 400;"">的意思，</span></p>
<p><span style=""font-weight: 400;"">心情不好可能有時候會比較思考不周全或者是只想要不顧後果的發洩不好的情緒，</span></p>
<p><span style=""font-weight: 400;"">所以用&rdquo;cranky&rdquo;這個字來描寫是很有趣的喔。</span></p>
<p><em><span style=""font-weight: 400;"">A: What&rsquo;s wrong with Mom? &nbsp;She just shouted at me when I came home.</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; (老媽怎麼了？ 我一回家她就對我大吼大叫。)</span></p>
<p><em><span style=""font-weight: 400;"">B: <span style=""color: #0000cd;"">She&rsquo;s in a cranky mood today.</span>Just stay away from her.</span></em></p>
<p><span style=""font-weight: 400;"">&nbsp; &nbsp; (<span style=""color: #0000cd;"">她今天心情不好。</span>你最好離她遠一點吧！)</span></p>
<p><span style=""font-weight: 400;"">以上5句都是日常生活當中常常會用到的用語，大家記起來了嗎？</span></p>
<p><span style=""font-weight: 400;"">我們再來多做一點練習吧！</span></p>"','published' => '1','og_title' => '你很龜毛！英文怎麼說？5句實用流行語！','og_description' => '學校英文教科書看來看去就是一大堆單字還有對話以及閱讀測驗，有時候看久了還真的希望能夠學點不太一樣的英文!今天我們就來學學一些平常在學校會用到的實用英文句子吧！','meta_title' => '你很龜毛！英文怎麼說？5句實用流行語！','meta_description' => '學校英文教科書看來看去就是一大堆單字還有對話以及閱讀測驗，有時候看久了還真的希望能夠學點不太一樣的英文!今天我們就來學學一些平常在學校會用到的實用英文句子吧！','canonical_url' => 'https://tw.english.agency/5句實用英文流行語','feature_image_alt' => '英文學習-你很龜毛怎麼說','feature_image_title' => '5句英文流行語','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599520ff00ddf-9L7tdJfoGkOMnvwHJ2jTChF6Z8iqBH-4167x2500.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-13 08:18:15"'],
  ['id' => '37','title' => '保溫瓶、保鮮膜、烤箱英文怎麼說？常見廚房家具、用具整理！','slug' => '廚房用具-英文','excerpt' => '廚房可以烹煮出全家人愛吃的食物，全家一起準備食物也是維繫感情的另外一種方法，廚房常見家具用具，你會說多少呢？今天小編就要帶著你來廚房學英文囉！','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-weight: 400;"">廚房是一個家庭當中最重要的地方，因為廚房可以烹煮出全家人愛吃的食物，</span><span style=""font-weight: 400;"">全家一起準備食物也是維繫感情的另外一種方法。</span><span style=""font-weight: 400;"">廚房常見家具用具，你會說多少呢？</span><span style=""font-weight: 400;"">今天小編就要帶著你來廚房學英文囉！</span></p>
<p><span style=""font-weight: 400;"">家具我們可以用&rdquo;</span><span style=""font-weight: 400;"">furniture</span><span style=""font-weight: 400;"">&rdquo;來表示，記得這個字是不可數名詞喔！</span><span style=""font-weight: 400;"">所以如果你要表達<span style=""color: #2e8b57;"">一件家具</span>的話，就可以用&rdquo;<span style=""color: #2e8b57;"">a piece of furniture</span>&rdquo;。</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">廚房用具</span>的英文是 </span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">kitchenware</span></span><span style=""font-weight: 400;""> 或是</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;""> kitchen utensils，</span></span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">utensil</span> 的中文意思就是「<span style=""color: #2e8b57;"">器皿、用器</span>」的意思，</span><span style=""font-weight: 400;"">另外補充<span style=""color: #2e8b57;"">電器</span>的說法，英文可以用&rdquo;</span><span style=""font-weight: 400;""><span style=""color: #2e8b57;"">appliance</span></span><span style=""font-weight: 400;"">&rdquo;，</span><span style=""font-weight: 400;"">一般我們常見到的家電產品都可以用""<span style=""color: #2e8b57;"">appliance</span>""這個字，是一個可數名詞喔；</span><span style=""font-weight: 400;"">更明確指「<span style=""color: #2e8b57;"">家電</span>」可以寫成 ""<span style=""color: #2e8b57;"">domestic/household appliances""</span></span><span style=""font-weight: 400;"">那當然<span style=""color: #2e8b57;"">廚房家電</span>就可以用&rdquo;<span style=""color: #2e8b57;"">kitchen appliances</span>&rdquo;了。</span><span style=""font-weight: 400;"">我們先來看看一些基本說法，這些都經常出現在多益考試的題目</span><span style=""font-weight: 400;"">中唷～</span></p>
<p>&nbsp;</p>
<h2><strong>Kitchen furniture廚房家具:</strong></h2>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">dining table</span><em><span style=""font-weight: 400;"">&rarr;</span></em><span style=""font-weight: 400;"">餐桌</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">cupboard</span><em><span style=""font-weight: 400;"">&rarr;</span></em><span style=""font-weight: 400;"">碗櫥</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">counter</span><em><span style=""font-weight: 400;"">&rarr;</span></em><span style=""font-weight: 400;"">流理台</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">cabinet</span><em><span style=""font-weight: 400;"">&rarr;</span></em><span style=""font-weight: 400;"">儲藏櫃</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">gas stove</span><em><span style=""font-weight: 400;"">&rarr;</span></em><span style=""font-weight: 400;"">瓦斯爐</span></li>
</ul>
<h4>&nbsp;</h4>
<h2><strong>Kitchen appliance 廚房電器:</strong></h2>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">microwave oven&rarr;微波爐</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">stove&rarr;火爐</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">icebox / refrigerator / fridge&rarr;冰箱</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">dishwasher&rarr;洗碗機</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">range fan&rarr;抽油煙機</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">oven / grill&rarr;烤箱</span>
<ul>(oven通常用來烤麵包 ，grill用來烤雞之類的)</ul>
</li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">toaster&rarr;烤吐司機</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">rice cooker&rarr;電鍋</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">blender / juice extractor&rarr;果汁機 / 榨果汁機</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">food processer&rarr;食物處理（攪切）機</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">grinder&rarr;研磨機</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">percolator/ drip filler&rarr; 咖啡機</span></li>
</ul>
<h4>&nbsp;</h4>
<h2><strong>Kitchenware 廚房用具:</strong></h2>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">dish rack&rarr;碗架、盤架</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">chopping/cutting board &rarr;砧板</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">knife&rarr;刀</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">cleaver&rarr;菜刀；剁肉刀</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">pan&rarr;平底鍋</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">wok&rarr;炒菜鍋</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">pot&rarr;鍋子</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">saucepan&rarr;小鍋子(把手)</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">spatula&rarr;鍋鏟</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">lid，cover&rarr;蓋子</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">plate&rarr;盤子</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">bowl&rarr;碗</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">spoon&rarr;湯匙</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">ladle&rarr;杓子、湯</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">whisk&rarr;攪拌器、打蛋器</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">whisk/mixer&rarr;手動攪拌器/電動攪拌器</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">sifter&rarr;篩子</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">grater/shredder&rarr;刨絲器</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">peeler&rarr;削皮刀</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">timer&rarr;計時器</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">measuring spoons&rarr;量匙</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">tray&rarr;托盤</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">can opener&rarr;開罐器</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">potholder，oven mitt&rarr;隔熱墊/隔熱手套</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">rag&rarr;菜瓜布</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">ice tray&rarr;冰盒</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">tinfoil&rarr;鋁箔紙</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">plasticwrap&rarr; 保鮮膜</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">foodstorage container&rarr; 保鮮盒</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><span style=""font-weight: 400;"">thermos bottle/ vacuum bottle/vacuum flask&rarr;保溫瓶</span></span>
<ul>(原本是保溫瓶Thermos的品牌名稱，後來因十分普及而成為此類產品的通稱)</ul>
</li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">thermal mug/travel mug/travel tumbler&rarr;隨手型保溫杯</span></li>
</ul>
<h4>&nbsp;</h4>
<h2><strong>補充一些廚房料理的相關動詞:</strong></h2>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">chop (up) &rarr; 切；剁</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">cube&rarr; 切成小塊</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">dice&rarr; 切丁</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">slice&rarr; 切薄片</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">mince&rarr; 切成粉末、碎塊</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">grate&rarr; 磨碎(薑或大蒜)</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">peel&rarr; 去皮；剝皮</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">stir&rarr; （用湯匙等）攪動；攪拌</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">shred&rarr; 切碎、剁碎</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">whisk&rarr; 攪打</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">fry&rarr; 油炸；煎；炒</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">stir-fry&rarr; 翻炒</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">boil&rarr; （在沸水中）烹煮；煮沸</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">stew&rarr; 燉</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">bake&rarr; 烘；烤</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">mash&rarr; 攪拌成泥</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">season&rarr; 調味</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">dip&rarr; 稍微浸泡、沾醬</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">squeeze&rarr;搾汁</span></li>
</ul>
</div>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '保溫瓶、保鮮膜、烤箱英文怎麼說？常見廚房家具、用具整理！','meta_description' => '廚房可以烹煮出全家人愛吃的食物，全家一起準備食物也是維繫感情的另外一種方法，廚房常見家具用具，你會說多少呢？今天小編就要帶著你來廚房學英文囉！','canonical_url' => 'https://tw.english.agency/廚房用具-英文','feature_image_alt' => '英文學習-廚具英文用語','feature_image_title' => '廚房用具英文單字整理','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599290cf7aa23-p84hExIHjKGP2sN1IHnh48Yn4QHgBW-4167x2500.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-13 08:48:10"'],
  ['id' => '38','title' => '"【澳洲 打工度假】出國打工 出發前先學好英文"','slug' => '出國打工-出發前先學好英文','excerpt' => 'Peggy大學畢業以後在桃園機場工作了一年，看到許多網友分享打工度假的照片，在海邊嬉戲、跟無尾熊互動等，所以就決定要去澳洲打工度假了。她設定了三個目標，第一存錢，第二學好英文，第三好好度假。','content' => '"<p><span style=""font-weight: 400;"">我是Peggy，在澳洲的伯斯 打工度假 一年，時薪是21.61澳幣，工作存了二十萬元台幣。大學畢業以後我在桃園國際機場工作了一年，看到許多網友在網路上分享的都是打工度假的照片，在海邊嬉戲、在跟無尾熊互動或者是到美麗的景點拍照，所以我就決定要去澳洲打工度假了。本來我設定了三個目標，第一存錢，第二學好英文，第三好好度假。</span></p>
<p><span style=""font-weight: 400;"">到了澳洲在網路上很快就找到了胡蘿蔔農場的工作，工作內容主要是洗蘿蔔跟切蘿蔔，雖然同事們多半是講國語的，還是有少數的俄羅斯人講英文，所以我還是設定了將英文學好的目標，希望我每天下班可以看看書背背單字，結交些澳洲當地的朋友。沒想到勞力工作就讓我每天的體力用光了。工作結束後連洗菜的力氣都沒有，隨便吃完飯，洗個澡，就想睡覺了，英文書根本沒有拿出來過。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">打工度假期間．英文對我來說一直是一個障礙，找工作怕怕，看合約更怕，簽約都要看個好幾遍，才敢怕怕的簽約。我常在想若是去澳洲打工度假前，英文就有一定基礎，就可以在澳洲從事飯店或者是餐廳的工作，這類薪資較高，又可跟各國人互動，一定很有意思，不像我現在這樣去了澳洲一年，英文完全沒進步。</span></p>
<p><span style=""font-weight: 400;"">在澳洲時候我發現凡是英文不好的朋友，找工作只能仰賴台灣人介紹以及網路上的澳洲打工的版看工作，或是透過工作代辦或者是工頭，找到的幾乎都是勞力辛苦的第一級產業。辛苦賺來的錢，往往還要讓代辦抽成或者跟工頭分，都有點心酸。</span></p>
<p><span style=""font-weight: 400;"">因此建議要前往澳洲打工度假的新鮮人，最好是先搞定你的英文，我在那邊認識些在餐飲業工作的韓國人都說他們是去菲律賓上過密集英語課程後，才來澳洲找工作。也就是搞定了英文才來澳洲，所以工作比較好找，也不一定需要從事勞力的工作，避免職業傷害，像我現在右手的手腕，偶爾還會有痠痛的感覺，我猜大概是在澳洲洗蘿蔔的職業傷害，雖然我蒐集到了二簽，可以留在澳洲繼續打工，我還是決定回台灣了，因為繼續再從事勞力的工作是沒有意義的。我滿喜歡到國外工作的感覺，不過如果我要再度去國外 打工度假 ，我會先去菲律賓上三個英語密集課程，搞定我的英文再出發。</span></p>"','published' => '1','og_title' => '"【澳洲 打工度假】出國打工 出發前先學好英文"','og_description' => 'Peggy大學畢業以後在桃園機場工作了一年，看到許多網友分享打工度假的照片，在海邊嬉戲、跟無尾熊互動等，所以就決定要去澳洲打工度假了。她設定了三個目標，第一存錢，第二學好英文，第三好好度假。','meta_title' => '"【澳洲 打工度假】出國打工 出發前先學好英文"','meta_description' => 'Peggy大學畢業以後在桃園機場工作了一年，看到許多網友分享打工度假的照片，在海邊嬉戲、跟無尾熊互動等，所以就決定要去澳洲打工度假了。她設定了三個目標，第一存錢，第二學好英文，第三好好度假。','canonical_url' => 'https://tw.english.agency/出國打工-出發前先學好英文','feature_image_alt' => '打工度假-學好英文','feature_image_title' => '澳洲打工度假','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/593fa88b61746-q09BRMF6gJFjmlnpqlo2auY0HHN76y-1000x600.jpeg','author_id' => '1','category_id' => '','created_at' => '"2017-06-13 09:08:36"'],
  ['id' => '39','title' => '禁止跳水，英文怎麼說？常見的警示標語！','slug' => '警止-標語-英文','excerpt' => '出入國內外公共場合時，在不同的地點會有不同的警示標語。這些標語可能是政府、主辦單位或是場地所有者設置，用來維護大眾的權益、安全及公共制序，我們就來看一些常見的警示標語的英文怎麼說！','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-weight: 400;"">出入國內外公共場合時，在不同的地點會有不同的警示標語。</span><span style=""font-weight: 400;"">這些標語可能是政府、主辦單位或是場地所有者設置，</span><span style=""font-weight: 400;"">用來維護大眾的權益、安全及公共制序，</span><span style=""font-weight: 400;"">讓民眾使用時更有品質、減少不必要的破壞並延長場地的使用壽命。</span></p>
<p>&nbsp;</p>
<h2><strong>下面是一些國內公共場所常見的警示標語的中英文對照：</strong></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">No bicycle. No bikes. ➡</span><span style=""font-weight: 400;"">禁止騎單車。</span></p>
<p><span style=""font-weight: 400;"">No climbing.</span><span style=""font-weight: 400;""> ➡ </span><span style=""font-weight: 400;"">禁止攀爬。</span></p>
<p><span style=""font-weight: 400;"">No Pushing &amp; Pulling. ➡</span> <span style=""font-weight: 400;"">禁止推拉欄杆。</span></p>
<p><span style=""font-weight: 400;"">Keep off the grass.</span><span style=""font-weight: 400;""> ➡</span><span style=""font-weight: 400;"">禁止踐踏草坪。</span></p>
<p><span style=""font-weight: 400;"">No smoking. Smoking prohibited.➡</span> <span style=""font-weight: 400;"">禁止吸煙。</span></p>
<p><span style=""font-weight: 400;"">No feeding.➡</span> <span style=""font-weight: 400;"">禁止餵食。</span></p>
<p><span style=""font-weight: 400;"">No releasing of wildlife.</span><span style=""font-weight: 400;""> ➡</span><span style=""font-weight: 400;"">禁止放生。</span></p>
<p><span style=""font-weight: 400;"">No fishing.➡</span> <span style=""font-weight: 400;"">禁止垂釣。</span></p>
<p><span style=""font-weight: 400;"">No swimming.</span><span style=""font-weight: 400;""> ➡</span><span style=""font-weight: 400;"">禁止游泳。</span></p>
<p><span style=""font-weight: 400;"">Diving prohibited.➡</span> <span style=""font-weight: 400;"">禁止跳水。</span></p>
<p><span style=""font-weight: 400;"">No vending.</span><span style=""font-weight: 400;""> ➡</span><span style=""font-weight: 400;"">禁止攤販。</span></p>
<p><span style=""font-weight: 400;"">No littering.</span><span style=""font-weight: 400;""> ➡</span><span style=""font-weight: 400;"">禁止亂丟垃圾。</span></p>
<p><span style=""font-weight: 400;"">No urinating.➡</span> <span style=""font-weight: 400;"">禁止隨地便溺。</span></p>
<p><span style=""font-weight: 400;"">No entry for vehicles.➡</span> <span style=""font-weight: 400;"">車輛禁止進入。</span></p>
<p><span style=""font-weight: 400;"">No touching.➡</span> <span style=""font-weight: 400;"">禁止觸摸。</span></p>
<p><span style=""font-weight: 400;"">No photos.➡</span> <span style=""font-weight: 400;"">禁止拍照。</span></p>
<p><span style=""font-weight: 400;"">The use of cameras or video equipment is prohibited.</span></p>
<p><span style=""font-weight: 400;"">➡</span> <span style=""font-weight: 400;"">禁止使用相機或攝影器材。</span></p>
<p><span style=""font-weight: 400;"">No eating &amp; drinking.➡</span> <span style=""font-weight: 400;"">禁止飲食。</span></p>
<p><span style=""font-weight: 400;"">No pets.➡</span><span style=""font-weight: 400;""> &nbsp;</span><span style=""font-weight: 400;"">禁止寵物。</span></p>
<p><span style=""font-weight: 400;"">No entry，authorised persond only.➡</span> <span style=""font-weight: 400;"">非相關人員禁止進入。</span></p>
<p><span style=""font-weight: 400;"">No nearing.➡</span> <span style=""font-weight: 400;"">禁止靠近。</span></p>
<p><span style=""font-weight: 400;"">No firearms.</span><span style=""font-weight: 400;""> ➡ </span><span style=""font-weight: 400;"">禁止槍械。</span></p>
<p><span style=""font-weight: 400;"">No alcohol. ➡</span> <span style=""font-weight: 400;"">禁止酒精飲料。</span></p>
<p><span style=""font-weight: 400;"">No drugs.</span><span style=""font-weight: 400;""> &nbsp;➡</span><span style=""font-weight: 400;"">禁止毒品。</span></p>
<p><span style=""font-weight: 400;"">No knives.</span><span style=""font-weight: 400;""> ➡</span><span style=""font-weight: 400;"">禁止刀械。</span></p>
<p><span style=""font-weight: 400;"">Please cherish the public property.➡</span> <span style=""font-weight: 400;"">請愛護公物。</span></p>
<p><span style=""font-weight: 400;"">No flammable materials.➡</span> <span style=""font-weight: 400;"">禁放易燃物品。</span></p>
<p>&nbsp;</p>
<h2><strong>下面是一些溫馨提醒：</strong></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">Watch your step.</span> <span style=""font-weight: 400;"">&nbsp;➡</span><span style=""font-weight: 400;"">上下階梯請小心。</span></p>
<p><span style=""font-weight: 400;"">Beware of approaching cars.</span><span style=""font-weight: 400;""> &nbsp;➡</span><span style=""font-weight: 400;"">注意後方來車。</span></p>
<p><span style=""font-weight: 400;"">Cyclists yield to pedestrains</span><span style=""font-weight: 400;"">. ➡</span><span style=""font-weight: 400;"">自行車應禮讓行人。</span></p>
<p><span style=""font-weight: 400;"">Mind the gap</span><span style=""font-weight: 400;"">.➡ </span><span style=""font-weight: 400;"">小心間隙。</span></p>
<p><span style=""font-weight: 400;"">Beware of slippery floor.</span><span style=""font-weight: 400;"">➡ &nbsp;</span><span style=""font-weight: 400;"">小心地滑。</span></p>
</div>"','published' => '1','og_title' => '禁止跳水，英文怎麼說？常見的警示標語！','og_description' => '出入國內外公共場合時，在不同的地點會有不同的警示標語。這些標語可能是政府、主辦單位或是場地所有者設置，用來維護大眾的權益、安全及公共制序，我們就來看一些常見的警示標語的英文怎麼說！','meta_title' => '禁止跳水，禁止釣魚的英文怎麼說？常見的警示標語！','meta_description' => '出入國內外公共場合時，在不同的地點會有不同的警示標語。這些標語可能是政府、主辦單位或是場地所有者設置，用來維護大眾的權益、安全及公共制序，我們就來看一些常見的警示標語的英文怎麼說！','canonical_url' => 'https://tw.english.agency/警止-標語-英文','feature_image_alt' => '英文學習-警示語英文','feature_image_title' => '禁止跳水英文怎麼說','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599295d719b41-Stw1kmNP41uPb7as4tS0g0q6TmgspK-4167x2500.png','author_id' => '1','category_id' => '10','created_at' => '"2017-06-13 09:33:19"'],
  ['id' => '40','title' => '服務處、免稅店、登機門英文怎麼說？機場常見英文單字整理！','slug' => '多益專欄-服務處-免稅店-登機門英文怎麼說機場.html','excerpt' => '夏天早已經來臨，學生最喜歡的暑假也到了，很多家庭都計畫著出國度假。出國旅遊絕對是一件令人興奮的事情，尤其是前往一個沒有去過的地方體驗當地的民俗風情以及美味的食物。','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-size: 18px;"">所以今天小編就要教教大家在出國之前，一定要先了解一些基本英文單字用法，大家也不要緊張，這些大都像是多益單字。如此以來，才不會發生不愉快的誤解。小編整理出在各國國際機場，一定會看到的10個機場常用的單字與多益準備期間必讀詞彙！希望可以幫助你出國旅遊時有個愉快的假期！</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>1.Arrivals 入境</h2>
<p><br />這個字本身有「到達；到來；達到」的意思</p>
<p>例如：<em><span style=""color: #2e8b57;"">We all waited for the <strong>arrival</strong> of our dear friends.&nbsp;</span></em>(我們都等著親愛的朋友到來。)</p>
<p>所以如果在機場看到這個字就要聯想到「<strong><span style=""color: #2e8b57;"">入境</span></strong>」，就是指說旅客到達本國的地方囉！我們常用 <span style=""color: #2e8b57;"">arrivals lobby、arrivals lounge</span>來表示<strong><span style=""color: #2e8b57;"">入境大廳</span></strong>。另外在機場告示板上面會出現的""<span style=""color: #2e8b57;"">arrival time</span>""就是指飛機抵達機場的時間喔！這是多益單字必讀的內容之一。</p>
<p>例句：<em><span style=""color: #2e8b57;"">I will meet you at <strong>arrival</strong> <strong>lobby</strong> when your plane arrives.&nbsp;</span></em>(當你的班機到達時，我將在入境大廳接你。)</p>
<h2>2.Departures　出境</h2>
<p>&ldquo;<span style=""color: #2e8b57;"">Departure</span>&rdquo;這個字有「離開；出發，起程」的意思</p>
<p>例句：<em><span style=""color: #2e8b57;"">The plane \' s departure was on schedule.&nbsp;</span></em>(飛機準時起飛。)</p>
<p>機場裡面有入境也一定會有出境囉，所以這個字就是指「<strong><span style=""color: #2e8b57;"">出境</span></strong>」，我們會用<span style=""color: #2e8b57;"">departures lobby、departures lounge</span>表示<strong><span style=""color: #2e8b57;"">出境大廳</span></strong>。</p>
<p>例句：<em><span style=""color: #2e8b57;"">I need some information for international <strong>departures</strong> for China Airlines.&nbsp;</span></em></p>
<p>(我需要一些華航國外線出境的資訊。)</p>
<p>&nbsp;</p>
<hr />
<h2>3.Check-in counter　報到櫃台</h2>
<p><br />&ldquo;<span style=""color: #2e8b57;"">Check in</span>&rdquo;這個字也是很常見到的一個字，不管是旅館住宿還是機場報到一定要會的一個字，是指<strong><span style=""color: #2e8b57;"">辦理登記手續</span></strong>的意思。</p>
<p>【補充】機場常見行李單字</p>
<p><span style=""color: #2e8b57;"">1.check-in luggage 託運行李</span></p>
<p><span style=""color: #2e8b57;"">2.carry-on luggage 手提行李</span></p>
<p>這也是在準備多益單字時不能忽略的詞！</p>
<p>例句：<span style=""color: #2e8b57;""><em>Do you know your <strong>check-in</strong> time?&nbsp;</em></span>(你知道辦理登機手續的時間嗎？)</p>
<p>&nbsp;</p>
<hr />
<h2>4.Boarding Gate登機門</h2>
<p><br />&ldquo;<span style=""color: #2e8b57;"">Board</span>&rdquo;這個字是動詞「登機」的意思，&rdquo;<span style=""color: #2e8b57;"">gate</span>&rdquo;這個字則有「出入口；門道；登機門」的意思，所以合起來就是<strong><span style=""color: #2e8b57;"">登機門</span></strong>喔。</p>
<p>相關單字:</p>
<p><span style=""color: #2e8b57;"">1.boarding pass 登機證</span></p>
<p><span style=""color: #2e8b57;"">2.boarding time 登機時間</span></p>
<p>例句：</p>
<p><em><span style=""color: #2e8b57;"">Where is my <strong>boarding gate</strong>?&nbsp;</span></em></p>
<p>(請問我的登機門在哪？)</p>
<p><em><span style=""color: #2e8b57;"">Can I see your <strong>boarding pass</strong>，please?&nbsp;</span></em></p>
<p>(請讓我看看您的登機證？)</p>
<p><em><span style=""color: #2e8b57;"">The boarding time will be noted on your <strong>boarding pass</strong>.&nbsp;</span></em></p>
<p>(登機證上面會有登機時間。)</p>
<p>&nbsp;</p>
<hr />
<h2>5.Terminal 航廈</h2>
<p><br />&ldquo;<span style=""color: #2e8b57;"">Terminal</span>&rdquo;這個字在有些字典查到的意思就是「航空站」，不過航空站其實一般通稱之「機場」喔，這個字我們會稱之為「<strong><span style=""color: #2e8b57;"">航廈</span></strong>」，也就是讓乘客轉換陸上交通與空中交通的設施，方便他們上下飛機的地方。比較大的機場會分成好多航廈，記得要搞清楚喔，多益考試中常常出現喔！</p>
<p><br />例句：<em><span style=""color: #2e8b57;"">The fifth <strong>terminal</strong> is under construction now.&nbsp;</span></em>(第5 航廈正在修建中。)</p>
<p>&nbsp;</p>
<hr />
<h2>6.Cart 手推車</h2>
<p><br />&ldquo;<span style=""color: #2e8b57;"">Cart</span>&rdquo; 這個字很常看到，比如在超市就會看到&rdquo;shopping cart&rdquo;這個用法，這個字的意思是「手推車」，如果在機場看到這個字就是指「<strong><span style=""color: #2e8b57;"">運送行李用的手推車</span></strong>」喔</p>
<p>同義詞：</p>
<p><span style=""color: #2e8b57;"">1.baggage carts</span></p>
<p><span style=""color: #2e8b57;"">2.luggage carts</span></p>
<p><span style=""color: #2e8b57;"">3.luggage trolleys</span></p>
<p><span style=""color: #2e8b57;"">4.trolleys</span></p>
<p>台灣的機場用的就是&rdquo;cart&rdquo;這個字而已。如果你看到&rdquo;<span style=""color: #2e8b57;"">Cart Return</span>&rdquo;就是指<strong><span style=""color: #2e8b57;"">借還手推車的地方</span></strong>啦！</p>
<p>例句：<em><span style=""color: #2e8b57;"">Each international airport offers <strong>baggage carts</strong> for rent.&nbsp;</span></em>(每個國際機場都可以借到行李手推車。)</p>
<p>&nbsp;</p>
<hr />
<h2>7.Information 服務處</h2>
<p><br />&ldquo;<span style=""color: #2e8b57;"">Information</span>&rdquo;這個字常見的是「消息；報導」，其實也有「<strong><span style=""color: #2e8b57;"">詢問處</span></strong>」的意思喔，也就是服務處的意思啦。</p>
<p>例句：<em><span style=""color: #2e8b57;"">You can pick up free brochures and maps at the <strong>Information</strong>.</span></em>&nbsp;(你可以在服務處拿到免費的手冊以及地圖。)</p>
<hr />
<p>&nbsp;</p>
<h2>8.Baggage Service 行李服務處</h2>
<p><br />""<span style=""color: #2e8b57;"">Baggage</span>""這個字是「<span style=""color: #2e8b57;"">行李</span>」，而""<span style=""color: #2e8b57;"">service</span>""這個字就是「<span style=""color: #2e8b57;"">服務</span>」，如果你在機場看到這個標誌就是可以幫你處理任何有關行李的問題喔，例如提領行李，行李存關服務又或者是行李寄存、打包服務喔。</p>
<p>例句：<em><span style=""color: #2e8b57;"">Report damaged baggage to the airport <strong>Baggage Service</strong> Office immediately after the arrival of your flight.&nbsp;</span></em>(遭毀壞行李請在班機抵達後向旅客行李服務處報告。)</p>
<p>&nbsp;</p>
<hr />
<h2>9.Currency Exchange 外幣兌換</h2>
<p><br />""<span style=""color: #2e8b57;"">Exchange</span>""這個字是「交換；調換；兌換」，""<span style=""color: #2e8b57;"">currency</span>""這個字就是「貨幣」，當然在機場如果看到這個字應該就很明顯就是<strong><span style=""color: #2e8b57;"">外幣兌換處</span></strong>啦。</p>
<p>例句：<em><span style=""color: #2e8b57;"">I \' d like to exchange some pounds for dollars，please.&nbsp;</span></em>(請幫我把一些英鎊兌換成美金。)</p>
<p>&nbsp;</p>
<hr />
<h2>10.Duty-free shop/store 免稅店</h2>
<p><br />&ldquo;<span style=""color: #2e8b57;"">Duty</span>&rdquo;這個字在字典有好多意思，例如「責任；義務；本分」，也有「稅」的意思，而&rdquo;<span style=""color: #2e8b57;"">free</span>&rdquo;這個字就有「免費的」的意思，也就是在這家商店購買東西稅是免費的，也就是<strong><span style=""color: #2e8b57;"">免稅</span></strong>的意思啦。</p>
<p>例句：<em><span style=""color: #2e8b57;"">You can pick up a gift from our premium <strong>Duty Free</strong> shop.&nbsp;</span></em>(你可以在我們最優質的免稅店挑選禮物。)</p>
<hr />
<p>&nbsp;</p>
<p>看完本篇文章，希望有幫助到大家！</p>
<p>（好用APP免費下載：<a title=""出國旅遊必備APP，常用旅遊英文100句"" href=""https://play.google.com/store/apps/details?id=com.words300tw"">出國旅遊必備APP，常用旅遊英文100句.</a>）</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '服務處、免稅店、登機門英文怎麼說?機場常見英文單字整理!','og_description' => '夏天早已經來臨，學生最喜歡的暑假也到了，很多家庭都計畫著出國度假。','meta_title' => '服務處、免稅店、登機門英文怎麼說?機場常見英文單字整理!','meta_description' => '夏天早已經來臨，學生最喜歡的暑假也到了，很多家庭都計畫著出國度假。出國旅遊絕對是一件令人興奮的事情，尤其是前往一個沒有去過的地方體驗當地的民俗風情以及美味的食物。','canonical_url' => 'https://tw.english.agency/多益專欄-服務處-免稅店-登機門英文怎麼說機場.html','feature_image_alt' => '機場常見英文','feature_image_title' => '機場常見英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/598c46dd4470f-PTs57mgaI4NNVJ3td9RF3u31eXtavo-1024x768.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2017-06-13 10:27:23"'],
  ['id' => '41','title' => '【澳洲打工度假】背包客最夢寐以求的旅行方式──公路旅行！','slug' => '背包客最夢寐以求的旅行方式','excerpt' => '"若你在澳洲以Backpacker為據點，那室友一定常邀你去「road trip」，road trip是最盡興的旅行方式，可以野炊、搭帳篷露營。聽起來很讓人嚮往，但卻有幾個注意事項，一起來看看吧！"','content' => '"<p><span style=""font-weight: 400;""><strong>Road trip</strong>一詞，指的是「公路旅行」，若你在澳洲選擇以Backpacker為據點，那麼你便會常聽見室友邀請你一同前去「road trip」，road trip是一種最盡興的旅行方式，你可以帶鍋具野炊、搭帳篷露營，甚至，除了不能上天、下地，只要方向盤在握，幾乎沒有無法到達的地方。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">Road Trip聽起來很讓人嚮往，但卻有幾個事項必須注意，筆者在下方列出：</span></p>
<ol>
<li>
<h2><strong> 駕駛人數：</strong></h2>
</li>
</ol>
<p><span style=""font-weight: 400;"">如果你打算駕車環澳，請盡量找有國際駕照的朋友同行，畢竟，澳洲是全球面積第六大的國家，而且，沿途的風景單調，不是一望無際的荒原，便是枝葉扶疏的原始林，這種景象第一次見到會欣喜地說不出話，但要你一連三、四個鐘頭都面對這樣的景色？天啊，It&rsquo;s enough！在澳洲的公路，常見到勸導駕駛勿疲勞駕駛的標語，我想，一成不變的景色就是主因吧！筆者就曾經一人獨自從Perth駕車到一個距離四小時車程的小鎮，中途被一成不變的荒原景色惹的發睏，被迫停在路邊休息！</span></p>
<p><span style=""font-weight: 400;""> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
<ol start=""2"">
<li>
<h2><strong> 油錢：</strong></h2>
</li>
</ol>
<p><span style=""font-weight: 400;"">對一個背包客而言，油錢是除了住宿與用餐外，在整個旅程中最讓人感到負擔的部分。依據筆者友人的經驗，她與男友兩人環澳一周，花了近6萬塊台幣的油錢﹝依據規劃路線的不同，油錢也會有差﹞，所以，若想將油錢分攤掉，最好能找齊一車子的人，但是，旅伴越來越多，隨之而來的便是：旅程中的協調。這個地方他想去、但她不想去，她想吃餐廳、他卻想野炊&hellip;&hellip;等，到最後，你會覺得倒不如一個人的時候自在。</span></p>
<p>&nbsp;</p>
<ol start=""3"">
<li>
<h2><strong> 車子：</strong></h2>
</li>
</ol>
<p><span style=""font-weight: 400;"">背包客的公路旅行，通常是駕駛自己買的二手車，但筆者也曾經租車去達爾文的Kakadu國家公園，所以，以下簡略介紹一下澳洲的租車相關規定：</span></p>
<p><strong>○ 租車：</strong><span style=""font-weight: 400;"">澳洲租車的計價方式有兩種，第一種是是Unlimited Kilometres不限里程，只計天數﹝就像台灣，一天租金多少﹞；另一種是計算天數，又另計總公里數，但每天會附送幾公里的免費額度。</span></p>
<p><strong>AVIS：</strong><a href=""http://www.avis.com/car-rental/avisHome/home.ac""><strong>http://www.avis.com/car-rental/avisHome/home.ac</strong></a></p>
<p><strong>Budget：</strong><a href=""http://www.economybookings.com/33/menu/car-rental-budget?gclid=CJ-a9KXzo7ICFXBUpgodlUUAEA""><strong>http://www.economybookings.com/33/menu/car-rental-budget?gclid=CJ-a9KXzo7ICFXBUpgodlUUAEA</strong></a></p>
<p><strong>Hertz：</strong><a href=""http://www.hertz.com/""><strong>http://www.hertz.com/</strong></a></p>
<p><strong>Thrifly：</strong><a href=""http://www.thrifty.com/""><strong>http://www.thrifty.com/</strong></a></p>
<p><strong>Europcar：</strong><a href=""http://www.europcar.com/""><strong>http://www.europcar.com/</strong></a></p>
<p><strong>Redspot：</strong><a href=""https://www.redspot.com.au/""><strong>https://www.redspot.com.au/</strong></a></p>
<p><strong>○ 保險：</strong><span style=""font-weight: 400;"">在澳洲只要一個小擦撞，修車費可能就動輒上萬塊，所以保險一定要買！</span></p>
<p><span style=""font-weight: 400;"">而通常租車的保險分三種：破撞險(Collision Damage Waiver)、遺失險(Loss Damage Insurance﹞、第三責任險(Liability Insurance Suppliment or Compulsory Third-Party)、道路救援險(Roadside Assistance)和零自負額的全險。其中，破撞險和第三責任險都會有「最高的負擔額」，亦即當車子有任何擦傷、</span><strong>毀損時，你需負擔的最高金額﹝當修復費用超過此金額，你也只需負擔該最高金額﹞，但據筆者自己的切膚之痛，在這裡向各位強力推薦：「零自負額的全險」，當年我駕車去Kakadu國家公園時，意外在園區撞上衝出來的小袋鼠，車頭都撞凹了，雖然有最高負擔額1000塊，但我的心還是在淌血啊：而在澳洲，通常25歲以下的駕駛者保費必須加倍，所以，盡量找25歲以上旅伴的同行吧，另外，也要問清楚是否有保2nd driver，否則因非被保人的駕駛而肇事，依規定是無法受理的喔！</strong></p>
<p><strong>○ 露營回送車：</strong><span style=""font-weight: 400;"">筆者從未租過回送車，但據說因為必須在限定時間內將車子送至某地，且有里程數的限制，所以租金會相當便宜，各位不妨一試，而且，靠一個「移動城堡」旅行應該還蠻有趣的。</span></p>
<p><strong>Transfer car：</strong><a href=""https://www.transfercar.com.au/""><strong>https://www.transfercar.com.au/</strong></a></p>
<p><strong>Drive now：</strong><a href=""http://www.drivenow.com.au/onewayrentals.jspc#/relocations/AU""><strong>http://www.drivenow.com.au/onewayrentals.jspc#/relocations/AU</strong></a></p>
<p><strong>Apollo﹝露營車網站﹞：</strong><a href=""http://www.apollocamper.com/reloc.aspx""><strong>http://www.apollocamper.com/reloc.aspx</strong></a></p>"','published' => '1','og_title' => '【澳洲打工度假】背包客最夢寐以求的旅行方式──公路旅行！','og_description' => '"若你在澳洲以Backpacker為據點，那室友一定常邀你去「road trip」，road trip是最盡興的旅行方式，可以野炊、搭帳篷露營。聽起來很讓人嚮往，但卻有幾個注意事項，一起來看看吧！"','meta_title' => '【澳洲打工度假】背包客最夢寐以求的旅行方式──公路旅行！','meta_description' => '"若你在澳洲以Backpacker為據點，那室友一定常邀你去「road trip」，road trip是最盡興的旅行方式，可以野炊、搭帳篷露營。聽起來很讓人嚮往，但卻有幾個注意事項，一起來看看吧！"','canonical_url' => 'https://tw.english.agency/背包客最夢寐以求的旅行方式','feature_image_alt' => '澳洲打工度假-公路旅行','feature_image_title' => '"road trip公路旅行"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5940b54789c08-2yNcuG5L22ynHo3G1QdJIm7k859C6d-1000x600.jpeg','author_id' => '1','category_id' => '','created_at' => '"2017-06-14 04:06:13"'],
  ['id' => '42','title' => '"【澳洲打工度假】OK & NG的找工作方式！"','slug' => '正確的找工作方式','excerpt' => '"「打工度假」在台灣蔚為流行，以下有幾種OK & NG的找工作行為，其中OK的方法，網路上大同小異，我僅提供個人經驗；至於NG的找工作行為，請別這麼做，免得招來他人怨怒喔"','content' => '"<p><span style=""font-weight: 400;"">如今，「<strong>打工度假</strong>」不僅在台灣蔚為流行，即便在歐洲各國，也是一種年輕人向外探索的趨勢，但談及「打工」，這群歐洲人卻由於先天上的金髮碧眼，使他們找起工作比亞洲人輕鬆許多，我便見過一位白人女仲介對吐不出完整英文句子的德國男孩和顏悅色，卻轉身嘲諷說一口流利英文的東方女孩。</span></p>
<p><span style=""font-weight: 400;"">所以，到了澳洲務必盡12萬分的心力找工作，遭遇挫折了也別氣餒，英文不好的便努力補足，畢竟，若老闆憂心你是否能聽懂他的指令，又怎會雇用你呢？若你想從事農場工作，就不必會太多英語啦！但能有基本的英語能力就是多一個保障，重要的是人在海外別罔顧自己的權益，以免不小心違反合約被取消簽證或是被詐騙辛苦錢那可得不償失！</span></p>
<p><span style=""font-weight: 400;"">以下列了幾種<strong>OK &amp; NG</strong>的找工作行為，其中OK的找工方法，其實網路各家大同小異，我僅提供個人經驗；至於NG的找工作行為，請千萬注意別這麼做，免得招來他人的怨怒喔！</span></p>
<p>&nbsp;</p>
<h2><strong>OK找工作方式：</strong></h2>
<ol>
<li style=""font-weight: 400;"">掃街：我大部分的工作都是掃街、網路而來，掃街的要點是：滴水不漏，一間店都不放過，請別憐惜紙錢，打工履歷重視的是在地的工作經驗，若無的話也盡量詳實記錄在台履歷經驗，或許有機會發揮所長。另外有人說遞履歷時須請出「經理」才有用，因為收履歷的員工可能將你的履歷隨手一扔，就不知去向了，這種「小鬼難纏，閻王好見」的店當然有，但不是每一家餐廳、商店都是如此，所以請別去叨擾「經理們」了吧。此外溝通技巧也很重要，在面談前先準備基本的常用句型，面談時要勇於開口，就算英語口說能力不夠，也要盡力溝通想辦法發揮個人特色，增加僱主印象。</li>
<li style=""font-weight: 400;"">網站：若你使用過台灣的104或123求職網，那麼下列這些網站也難不倒你，我的建議是：雇主有留電話，便打電話，在致電之前先用英文打好草稿，並多加練習，避免過重的口音和不流暢的應答，暴露自己缺點而錯失機會；不擅長在電話中以英語交談者，則傳簡訊；若雇主沒有留電話，才使用e-mail。在我過去的經驗裡，一天寄出50封e-mail是很常見的事，有時還得填好幾二十頁的英語申請表格，總之，每一個網站都逛、每個表格都填、別怕麻煩，看到喜歡的工作﹝而且看起來正派、合理﹞便遞履歷。</li>
</ol>
<table>
<tbody>
<tr>
<td>
<p><span style=""font-weight: 400;"">Gumtree</span></p>
</td>
<td>
<p><span style=""font-weight: 400;"">租屋、找工作，各式廣告五花八門</span></p>
<p><a href=""http://www.gumtree.com.au/""><span style=""font-weight: 400;"">http://www.gumtree.com.au</span></a></p>
</td>
</tr>
<tr>
<td>
<p><span style=""font-weight: 400;"">Job Search</span></p>
</td>
<td>
<p><span style=""font-weight: 400;"">澳洲政府求職網，當然都是白工</span></p>
<p><a href=""http://www.jobsearch.com.au/""><span style=""font-weight: 400;"">http://www.jobsearch.com.au</span></a></p>
</td>
</tr>
<tr>
<td>
<p><span style=""font-weight: 400;"">The Job Shop</span></p>
</td>
<td>
<p><span style=""font-weight: 400;"">主要提供西澳的工作</span></p>
<p><a href=""http://www.thejobshop.com.au/""><span style=""font-weight: 400;"">http://www.thejobshop.com.au</span></a></p>
</td>
</tr>
<tr>
<td>
<p><span style=""font-weight: 400;"">Seek</span></p>
</td>
<td>
<p><span style=""font-weight: 400;"">都是白工的工作機會</span></p>
<p><a href=""http://www.seek.com.au/""><span style=""font-weight: 400;"">http://www.seek.com.au</span></a></p>
</td>
</tr>
<tr>
<td>
<p><span style=""font-weight: 400;"">背包客棧</span></p>
</td>
<td>
<p><span style=""font-weight: 400;"">較多是黑工，薪資低，建議詢問清楚</span></p>
<p><a href=""http://www.backpackers.com.tw/forum/forumdisplay.php?f=121""><span style=""font-weight: 400;"">http://www.backpackers.com.tw/forum/forumdisplay.php?f=121</span></a></p>
</td>
</tr>
<tr>
<td>
<p><span style=""font-weight: 400;"">Backpacker Down Under</span></p>
</td>
<td>
<p><span style=""font-weight: 400;"">提供適合背包客的工作，如農場、鄉村bar attendant</span></p>
<p><a href=""http://www.backpack-downunder.com/""><span style=""font-weight: 400;"">http://www.backpack-downunder.com/</span></a></p>
</td>
</tr>
<tr>
<td rowspan=""3"">
<p><span style=""font-weight: 400;"">速食連鎖店網站</span></p>
</td>
<td>
<p><span style=""font-weight: 400;"">麥當勞</span></p>
<p><a href=""https://apply.mcdonalds.com.au/public/index.cfm""><span style=""font-weight: 400;"">https://apply.mcdonalds.com.au/public/index.cfm</span></a></p>
</td>
</tr>
<tr>
<td>
<p><span style=""font-weight: 400;"">Subway</span></p>
<p><a href=""https://www.mysubwaycareer.com/""><span style=""font-weight: 400;"">https://www.mysubwaycareer.com/</span></a></p>
</td>
</tr>
<tr>
<td>
<p><span style=""font-weight: 400;"">Hungry &nbsp;Jack&rsquo;s﹝=漢堡王﹞</span></p>
<p><a href=""https://www.mysubwaycareer.com/""><span style=""font-weight: 400;"">https://www.mysubwaycareer.com/</span></a></p>
</td>
</tr>
</tbody>
</table>
<ol>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">仲介：背包客肯定都耳聞過《澳洲打工度假聖經》這本書，裡面提供了詳實的各地白人仲介資料，白人仲介都是不收費的，我幾乎都跑過，但就個人經驗而言，不曾因此取得工作。至於，背包客棧上有許多收費不菲的華人仲介，但一份工作要價700塊澳幣？我個人敬謝不敏，且有朋友上任後發現不合適，卻又繳了錢不甘心，只能被迫作到期滿，縱使取得工作，卻也不愉快。但也聽說過有和仲介打好關係，工作一個個無縫接軌的案例，所以出門在外有良好的人際要領和溝通能力，多少還是會替自己多開一扇門。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">朋友介紹：朋友與你交情好，自然會替你介紹工作，所以這就是一個考驗你做人成不成功的時機啦！除了台灣的夥伴外，若外語能力許可，不彷多多和他國朋友交流，也能擴展不同類型的工作。而認識朋友的方法，不外乎：去當地backpacker、share house等眾多背包客聚集的場所。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">公佈欄：商場、backpacker等的公佈欄通常會有工作資訊。</span></li>
</ol>
<p>&nbsp;</p>
<h2><strong>NG找工作方法：</strong></h2>
<ol>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">過度依賴朋友：每當朋友一身疲憊地下工，就趨上前問：「你們公司有缺嗎？」你好歹也先奉個茶、端個點心吧？這樣的人在澳洲屢見不鮮，儼然已成為背包客界中最不受歡迎的一群。更曾有朋友向我抱怨，他與一位背包客僅一面之緣，對方便開口問他：「你在哪工作？薪水多少？現在還有缺嗎？」這都是相當不禮貌的行為，常為人詬病，但初來乍到的背包客往往急於求職，犯此大錯而不自知。其實在澳洲打工度假的背包客通常都有革命情誼，若能真心對待對方，等到有好康的機會時自然會互相照顧。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">在餐廳、商店正忙時去投履歷：相信我，waitress在忙不過來時，只想將你和你那一張履歷趕出餐廳，履歷石沉大海的機率高的破錶！餐廳請別正中午去，大概下午一、兩點最合適。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">遞完履歷便走：務必使櫃台小姐或是員工對你留下好印象，不一定要長篇大論地說什麼，只要開個小玩笑，譬如飯店很大、櫃台很不好找，投履歷時可玩笑似地說：「呼，我以為剛剛經過了一座迷宮。」我就曾因與飯店小姐說了一個小笑話而取得工作，總之：討喜的人機會多。</span></li>
</ol>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p><span style=""font-weight: 400;"">以上便是我旅澳兩年的相關經驗，找第一份工作時難免撞牆，但千萬別氣餒！</span></p>
<p><span style=""font-weight: 400;"">有意要前往澳洲打工度假的話，建議可把握時間儘早在出發前開始加強英語能力和特殊專才，多多發揮個人特質才能在澳洲茫茫的打工度假背包客人海裡面脫穎而出！</span></p>
<p><span style=""font-weight: 400;"">至於其他不可言喻的箇中滋味，就要等大家到了澳洲自己細細品嘗啦！</span></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"【澳洲打工度假】OK & NG的找工作方式！"','og_description' => '"「打工度假」在台灣蔚為流行，以下有幾種OK & NG的找工作行為，其中OK的方法，網路上大同小異，我僅提供個人經驗；至於NG的找工作行為，請別這麼做，免得招來他人怨怒喔"','meta_title' => '"【澳洲打工度假】OK & NG的找工作方式！"','meta_description' => '"「打工度假」在台灣蔚為流行，以下有幾種OK & NG的找工作行為，其中OK的方法，網路上大同小異，我僅提供個人經驗；至於NG的找工作行為，請別這麼做，免得招來他人怨怒喔"','canonical_url' => 'https://tw.english.agency/正確的找工作方式','feature_image_alt' => '澳洲打工度假-找工作方式','feature_image_title' => '"澳洲OK & NG的找工作方式"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5940c240c2b58-yPW7dDbwKQ1GH7EyuQBoohkhONcLXu-1000x600.jpeg','author_id' => '1','category_id' => '','created_at' => '"2017-06-14 05:01:02"'],
  ['id' => '43','title' => '"五個選擇加拿大留學的理由：便宜的學費，高CP值的教育環境與移民機會，Why not ?"','slug' => '五個加拿大留學理由','excerpt' => '"想要出國留學，卻總是拿不定主意應該要選擇哪個國家？這篇文章要和大家分享你可以選擇加拿大留學 、遊學的原因，超高cp值絕對是你的好選擇！"','content' => '"<div style=""font-size: 18px;"">
<p><span style=""font-weight: 400;"">想要出國留學，總是拿不定主意應該要選擇哪個國家？畢竟這可能關乎自己的未來與職業生涯，絕對不能草率。說到海外留學，我們可能最常想到英國、美國，但可能忽略了其實對於我們學生來說，CP值很高的選擇！這篇文章要和大家分享你可以選擇<strong><span style=""color: #2e8b57;"">加拿大留學 、遊學</span></strong>的原因，給大家另一種選擇！</span></p>
<h2><strong>Why Canada? 五個選擇 加拿大留學 的理由</strong></h2>
<h3><strong><span style=""color: #008080;"">1. 學費成本低且師資與學校的世界排名不輸美國</span></strong></h3>
<p><span style=""font-weight: 400;"">相對於美國7000多所的大專院校(含社區大學)，加拿大的公私立大專院校(含社區大學)還不到300所，相對來講精實許多，若以1年2學期的商學院或文學院學費作基準比較，以下是學費比較表 :</span></p>
<div style=""overflow-x: auto;"">
<table style=""border 2px solid gray; cellpadding: 3;"">
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p>&nbsp;</p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">1年(2學期)學費</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">換算台幣</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">加拿大大學 前段名校</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">3萬加幣</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">75萬台幣</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">加拿大大學 中段學校</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">1.8~2.5萬加幣</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">45~62.5萬台幣</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">加拿大 Colleges或社區大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">1.2~1.8萬加幣</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">30~45萬台幣</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國 常春藤系列大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">5萬美金左右</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">165萬台幣</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國大學 其他前段名校</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">4萬美金左右</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">132萬台幣</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國大學 州大系列與中段大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">2.5萬~3萬美金左右</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">82.5~99萬台幣左右</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國大學 其他大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">2.5萬美金起跳</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">82.5萬台幣起跳</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國Colleges或社區大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">8千~1.2萬</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">26.5~40萬</span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p><span style=""font-weight: 400;"">由此可見，美國的高等教育學費，還是比加拿大貴了不少，以上這還只是純學費而已，尚未加入生活費(含吃/住/手機/大眾交通工具月票等)，以波士頓附近的常春藤名校來說，有學生大約一個月花到1800美金(台幣快6萬)，詳見表1 : 美國生活費概估 ; 加拿大生活費計算比較簡單，大約一個月生活費1100加幣(台幣2.75萬~3萬)，要省錢的方式，就是盡量避免外食囉。</span></p>
<h3><strong><span style=""color: #008080;"">2. 在校期間，持學生簽證，可以在校內外合法打工賺取生活費</span></strong></h3>
<p><span style=""font-weight: 400;"">根據學生簽證規定，參加</span><strong><span style=""color: #2e8b57;"">加拿大專業實習課程、加拿大專科課程</span></strong><span style=""font-weight: 400;"">或是</span><strong><span style=""color: #2e8b57;"">大學與研究所</span></strong><span style=""font-weight: 400;"">學生，持學生簽證，是可以在校內外合法打工的(每週最多20小時)，美國則必須是就讀1年後才可以在校外工作，也因此造成近年來選擇加拿大就學的學生愈來愈多 ; 畢竟考量現實，當自己看到銀行的存款在下降又無法&rdquo;開源&rdquo;時，想法與做法上，會往便宜的消費與生活水平，或是易於增加收入的環境靠近。</span></p>
<h3><strong><span style=""color: #008080;"">3. 比起美國，加拿大大學畢業後移民機會高</span></strong></h3>
<p><span style=""font-weight: 400;"">另外對於有想要</span><strong><span style=""color: #2e8b57;"">移民加拿大</span></strong><span style=""font-weight: 400;"">的朋友們，唸加拿大的公立學校超過8個月以上，可以申請畢業後工簽;念2年最多可以拿到3年畢業後工簽，使用畢業後工簽所獲得的工作時數，有助於增加申請移民的累積分數;比起美國的OPT制度要來的方便。</span></p>
<h3><strong><span style=""color: #008080;"">4. 治安與安全，加拿大比美國好</span></strong></h3>
<p><span style=""font-weight: 400;"">說到安全，每個國家，每個城市都有治安死角，但是大部分想前往美國留遊學的消費者也都聽過&hellip;美國可以合法擁槍的事情，過往我們也在不少相關的當地社會新聞中看到不幸的案例，像前一陣子有大人沒有收好槍枝，被小Baby翻出來玩誤扣板機，誤殺了他媽媽，還有n+1次的有人持槍闖入校園，甚至掃射無辜師生的新聞&hellip;造成的傷亡的新聞大家應該都還有印象，各位看官有興趣可以去網路上搜尋這些新聞，而加拿大則沒有這個問題，基本上保持晚上不要出門，大致上人身安全都是ok的。</span></p>
<h3><strong><span style=""color: #008080;"">5. 加拿大曾經連續名列世界宜居評比第一名，被稱為是世界上最理想居住地</span></strong></h3>
<p>在<strong><span style=""color: #2e8b57;"">聯合國近五年來針對181個國家的生活水準綜合評分中，加拿大連續名列第一，被稱為是世界上最理想的居住地，溫哥華更是世界上第一名適合居住的城市。</span></strong>其評分標準綜合了：生活品質，全民教育，就業率，犯罪率，及全民醫療保險等因素。過去幾年，每年在競爭全世界最適宜人居的國家城市當中，加拿大的溫哥華與多倫多總是跟北歐，澳洲，紐西蘭等國家的城市在拼第一名&hellip;雖然第一名只有一個，但是這類型的評比上面，卻沒有看過美國的城市出現在前10名，在這項評比當中，比較有權威的就屬經濟學人(The Economist Liveability Survey) 以及 Economist Intelligence Unit網站。</p>
<p>以2015 Economist Intelligence Unit網站的排名來說，多倫多獲得第10名，溫哥華則是第2名</p>
<p><strong>經濟學人網站的排名來說，進入前10名的則有卡加利/溫哥華/多倫多三個城市。</strong></p>
<p><span style=""font-weight: 400;"">加拿大人口總數約三千萬，面積9976139平方公里，人民平均年收入二萬五千美元，一般家庭都擁有自己的汽車和房屋，全民享受公費醫療保險，是一個高福利，高生活水準的穩定已開發國家。 加拿大同時被世界銀行組織列名全世界第二富裕國家，僅次於澳大利亞。加拿大屬於英聯邦的議會制，首都位於渥太華，是個多元文化及雙語國家，英文及法文為其正式官方語言。人民在政治，經濟，教育，法制，言論等各方面也都享有充分的民主和自由。</span></p>
<p>&nbsp;</p>
<hr />
<h3><strong><span style=""color: #008080;"">加拿大十省三地區英文縮寫以及地理位置圖：</span></strong></h3>
<div style=""overflow-x: auto;"">
<table>
<tbody>
<tr>
<td>
<p>&nbsp;</p>
<table>
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>縮寫</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>英文 省名</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>中文 省名</strong></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>AB</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Alberta</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">亞伯達</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>BC</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">British Columbia</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">英屬哥倫比亞</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>MB</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Manitoba</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">曼尼托巴</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>NB</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">New Brunswick</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">新伯倫瑞克</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>NF</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Newfoundland and Labrador</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">紐芬蘭與拉布拉多</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>NS</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Nova Scotia</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">新斯科細亞</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>NT</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Northwest Territories</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">西北地方</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>NU</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Nunavut</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">努那武特地區</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>ON</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Ontario</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">安大略</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>PE</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Prince Edward Island</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">愛德華王子島</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>QC</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Quebec</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">魁北克</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>SK</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Saskatchewan</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">薩克其萬</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><strong>YK</strong></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">Yukon</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">育空地區</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">渥太華Ottawa-首都-安大略省</span></strong></p>
<p><span style=""font-weight: 400;"">加拿大首都渥太華人口約120萬，是加拿大的科學文化中心，最有影響的自然科學和社會科學研究機構分別是加拿大國家自然科學研究院和加拿大北美學會。這兩大研究機構集中了全國最有影響的自然科學家和社會學者，每年有大量的學術成果問世。渥太華市區北部是高技術集中發展的地區，有&lt;北矽谷&gt;之稱。</span></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">多倫多Toronto-加拿大經濟中心-安大略省</span></strong></p>
<p><span style=""font-weight: 400;"">多倫多，人口４３０多萬，加拿大第一大城市，也是加拿大商業及金融中心。多倫多市地處美國東部大都市群，和紐約、波士頓距離較近。風景秀麗，生活方便，商業氛圍、教育水準及就業機會，令多倫多成為最受移民歡迎的城市之一。</span></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">溫哥華Vancouver-加拿大通向東方的門戶-卑斯省</span></strong></p>
<p><span style=""font-weight: 400;"">溫哥華為加拿大西部布列顛哥倫比亞省British Colombia(BC)，總人口約200萬，濱臨太平洋岸的海港城市，隔喬治亞海峽與溫哥華島遙遙相望，緊臨美國華盛頓州，依山傍海，是英屬哥倫比亞省第一大城市，終年氣候溫和、濕潤，環境宜人，是加拿大著名的旅遊勝地。由於舒服的氣候條件和得天獨厚的自然美景，使溫哥華成為最適合享受生活主義者的樂園。是目前世界上最適合居住環境的城市之一。</span></p>
<p><span style=""font-weight: 400;"">溫哥華交通便利，從市中心到達各處旅遊景點均很方便。有&ldquo;海上巴士Sea Bus&rdquo;之稱的現代化渡輪，是便宜而快速的觀光工具。溫哥華的夜生活極為豐富多彩，有一流的劇院、酒吧、舞廳、優質中餐和日、韓餐館等可供選擇了溫哥華，很快就融入這一片多姿多彩的夜色之中。</span></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">卡加利Galgary-新興的石油工業城市-亞伯達省</span></strong></p>
<p><span style=""font-weight: 400;"">卡加利位於Alberta南部，落磯山脈下，是一座新興的石油工業城市。石油工業和其也產業的蓬勃發展為這個城市帶來了許多就業機會，景氣的經濟帶來了人才濟濟的局面，現在卡城的工程師、科學家占城市總人口的比例是全國最高的。既有大城市的繁華、也有小城市的友善與溫馨，是一個活力十足的城市，充滿運動迷、戶外愛好者，以及友善的居民。</span></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">班芙Banff-洛磯山脈的靈魂-亞伯達省</span></strong></p>
<p><span style=""font-weight: 400;"">班芙是一個用看的比用講的讓人印象要深刻的小鎮，人口不到一萬人，位在洛磯山腳下，是一個飄在藍天、高山、綠地和河流之間的大自然的美妙造物，又稱洛磯山脈的靈魂，班芙整個城鎮都是加拿大班芙國家公園的一部分，而在這個國家公園裡，又分佈著數不盡的瀑布、溪流、山峰和溫泉，的確是遊覽觀光的最佳去處。</span></p>
<p><span style=""font-weight: 400;""> &nbsp;&nbsp;</span><span style=""font-weight: 400;""> &nbsp;&nbsp;</span></p>
<p><strong><span style=""color: #008080;"">蒙特婁Toronto-雙與浪漫之城</span></strong></p>
<p><span style=""font-weight: 400;"">蒙特婁是世界上最大的雙語(英語與法語)城市。人口約343萬，是加拿大第二大城市。作為加拿大的藝術時尚之都，蒙特婁體現出獨特的法國文化底蘊被認為是北美的浪漫之都。置身於蒙特婁，可集欣賞建築、文化藝術、歷史、宗教於一體。無論白天還是夜晚，市中心總是人流不息，洋溢著友善、溫暖和活躍的氣氛。你可以享受各種文化活動，娛樂活動、購物歡樂和活力熱鬧的夜生活。</span></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">魁北克市Quebec city-法國城-魁北克省</span></strong></p>
<p><span style=""font-weight: 400;"">魁北克市是魁北克省省會，加拿大東部重要城市和港口，位於聖勞倫斯河與聖查爾斯河匯合處。魁北克城是加拿大的第九大城市，在魁北克省則僅次於蒙特婁而位居第二。全市總人口約68萬，其中絕大多數為法裔加拿大人，95%的居民只講法語。全城分新區和老區兩部分。新市區高樓林立，商業繁榮，一派現代化城市風貌。舊市區仍保有18世紀時法國城市的風貌。這裡具有18世紀牌匾的店鋪商行臨立，店員身著古裝、梳復古打扮，使整個市區充滿了古色古香的情調。魁北克城名勝古跡甚多，是北美洲的一座最美的歷史名城。</span></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">哈里福克斯Halifox-加東的港都鐵達尼號的終站-新斯科細亞</span></strong></p>
<p><span style=""font-weight: 400;"">哈里福克斯（Halifax），是加拿大新斯科細亞省的省會，北部最大的深水天然港口，氣候適宜，是加拿大第二溫暖的城市。2008年人口為37萬人。哈利法克斯是加拿大東部地區主要的政府服務和商業部門的集中地。農業，漁業，礦業，林業以及天然氣提煉為哈利法克斯地區的主要經濟產業，由於北大西洋的洋流帶來豐富的漁產，這裡魚貨量位居加拿大排名前三，而台灣的龍蝦吃到飽自助餐，它們所提供的龍蝦大多是從這裡捕獲的。</span></p>
<p><span style=""font-weight: 400;"">很遺憾的，鐵達尼號從英國出發後終究沒有到達紐約，哈里福克斯是最接近鐵達尼號失事的陸地港口，因此後來一些從鐵達尼號上打撈出來的無名大體與遺物就葬在市中心的紀念公園內，或是放在博物館內，如果有興趣，可以去參觀一下。</span></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #008080;"">黃刀鎮Yellowknife-極光之城-西北地方</span></strong></p>
<p><span style=""font-weight: 400;"">黃刀鎮本身是西北地方的首府，第一批居民在黃刀鎮定居於1936年，早年是以礦業發展為主的城鎮，曾經有過一陣的掏金熱，但目前的黃刀鎮，早已轉型為以旅遊業為主的城市，由於看極光需要在特定的緯度，並且黃刀鎮因為接近北極圈，造成年均溫均下探0度的寒冷氣候，除了看享受壯麗的極光之外，冬季下雪的時候可以乘坐狗拉雪橇，七月的最高溫大約21度，附近有一些湖區可以走走，因此暑假時也可以到此一遊兼避暑喔。</span></p>
<p>&nbsp;</p>
<hr />
<p><span style=""font-weight: 400;"">附表：</span></p>
<h4><span style=""font-weight: 400;""><span style=""color: #00008b;"">表1. 美國生活費概估</span></span></h4>
<div style=""overflow-x: auto;"">
<table>
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">等級</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">代表城市/區域/州</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">1年生活費 (美金)</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">換算台幣</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">第1級</span></p>
<p><span style=""font-weight: 400;"">美國特大城市</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">華盛頓首都特區、舊金山市、紐約、費城、洛杉磯、波士頓、邁阿密、夏威夷、芝加哥</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">$1.2萬~2.4萬</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">約39.5~79萬</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">第2級</span></p>
<p><span style=""font-weight: 400;"">美國大城市</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">匹茲堡、西雅圖、拉達斯、亞特蘭大、奧斯丁、底特律</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">$1.2萬</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">約39.5萬</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">第3級</span></p>
<p><span style=""font-weight: 400;"">美國南部、中西部、東南部州</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">德州、威斯康辛州，依利諾州、密歇根州、猶他州、科羅拉多州、喬治亞州、維吉尼亞州、北卡羅來納州</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">$9，600左右</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">約31.5萬</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">第4級</span></p>
<p><span style=""font-weight: 400;"">其他地區</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">奧克拉荷馬州、密蘇里州、路易士安納州、南卡羅來納州</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">$7，200左右</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">約23.5萬</span></p>
</td>
</tr>
</tbody>
</table>
<h4><span style=""font-weight: 400;""><span style=""color: #00008b;""> 表2.上述美加地區前中後段大學的定義</span></span></h4>
<table>
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">大學分類</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">定義說明</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國常春藤系列大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">哈佛，耶魯等常春藤名校</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國大學其他前段名校</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">除了常春藤名校外的知名學府，以及其他世界排名前200名的美國大學，如南加大，愛荷華大學等</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國州大系列與中段大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">以仿間的講法&hellip;XX&rdquo;州立(state)&rdquo;大學，以及其他介於世界排名250~500名中間的美國大學，我們將其視作中段大學，如加州州立大學，佛羅里達州大，亞利桑那州大等</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">美國其他大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">這裡是抓世界排名500名以後的學校而言，如內華達大學，緬因大學等</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">加拿大前段大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">包括知名學府，以及其他世界排名前200名的加拿大大學， 如多倫多大學，卑斯大學，麥吉爾大學，滑鐵盧大學等</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">加拿大中段大學</span></p>
</td>
<td style=""border: 1px solid gray; text-align: left;"">
<p><span style=""font-weight: 400;"">我們定義介於世界排名250名之後的加拿大學校，如薩斯科奇萬大學，維多利亞大學等</span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p><span style=""font-weight: 400;"">※以上所引用的2015世界百大大學排名，資料來源為U.S. News &amp; World Report網站。</span></p>
<p><span style=""font-weight: 400;"">※以上加幣台幣匯率換算以1:25計算 ; 美金台幣匯率換算以1:33計算。</span></p>
</div>
<p>&nbsp;</p>
<hr />
<div style=""font-size: 14px;"">
<p><strong>本文所參考資料來源</strong></p>
<p><span style=""font-weight: 400;"">University of Canada網站、加拿大移民局官網、Collegetimes網站、PCTIA網站、The Council of Ministers of Education網站、教育部兩岸與國際教育司網站、The Economist網站、Economist Intelligence Unit網站、U.S. News &amp; World Report網站、台灣銀行官網、各校官網 （美國 : 哈佛大學/耶魯大學/南加大/愛荷華大學/加州州立大學/佛羅里達州大/亞利桑那州大/內華達大學/緬因大學；加拿大 : 多倫多大學/卑斯大學/麥吉爾大學/滑鐵盧大學/薩斯科奇萬大學/維多利亞大學等）</span></p>
</div>"','published' => '1','og_title' => '"五個選擇加拿大留學的理由：便宜的學費，高CP值的教育環境與移民機會，Why not ?"','og_description' => '"想要出國留學，卻總是拿不定主意應該要選擇哪個國家？這篇文章要和大家分享你可以選擇加拿大留學 、遊學的原因，超高cp值絕對是你的好選擇！"','meta_title' => '"五個選擇加拿大留學的理由：便宜的學費，高CP值的教育環境與移民機會，Why not ?"','meta_description' => '"想要出國留學，卻總是拿不定主意應該要選擇哪個國家？這篇文章要和大家分享你可以選擇加拿大留學 、遊學的原因，超高cp值絕對是你的好選擇！"','canonical_url' => 'https://tw.english.agency/五個加拿大留學理由','feature_image_alt' => '加拿大留學-加拿大遊學','feature_image_title' => '加拿大留學的理由','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5940d5a93c66c-eVQw4yxuA2ZqAp1MgZwteBeQTyoL08-1000x600.jpeg','author_id' => '1','category_id' => '3','created_at' => '"2017-06-14 06:20:57"'],
  ['id' => '44','title' => '出國打工度假，工作好找嗎？','slug' => '打工度假工作好找嗎','excerpt' => '打工度假工作真的好找嗎？上班到底是說英文還是中文？真的存得到錢嗎？來看看小資女Jing在澳洲打工度假的經驗分享，一次回答你關於打工度假最想知道的！','content' => '"<p><span style=""font-weight: 400;"">我是JING，大學畢業以後在旅行社擔任門市營業員兩年，薪水普普，業績獎金看的到拿不到，每天的工作都是面對客人，心中一直想成交，可是大部分的客人只是來比價而已，上班下班渾渾噩噩的生活，又累又沒存到錢，做個女生升遷的機會又是比較少。看到網路上壯遊天下的報導非常心動，就下定決心先去 打工度假 兩年，然後環遊世界88天。</span></p>
<p><span style=""font-weight: 400;"">兩年前我離職後先墨爾本去打工，沒去之前，就知道我會在農場打工，因為只有這樣才能集二簽，繼續留在澳洲賺錢。出國打工，工作好找嗎？願意出勞力，工作很好找。不過如果英文不好，就無法在餐廳或旅館要用英文溝通的工作，只能透過代辦找農業的苦力活。所以第一份工作就是黑工，沒保險，沒繳稅，還要被代辦抽傭金。那份工作是在葡萄農場，薪水是論件計酬，十公斤為一箱，一箱兩元澳幣，非常辛苦。如果去澳洲之前我就搞定我的英文，我的選項可以更多元，而不需要花這麼多體力，去從事在台灣我根本不會碰的職業。</span></p>
<p><span style=""font-weight: 400;"">而且農場的工作有淡旺季，冬季的時候後，並非農作物的採收期，工作會變很少。幸好後來我找到墨西哥餅皮工廠，才能繼續待下去賺錢， 打工度假 的工作都是透過網路爬文，會有代辦或者是前輩會在網路上分享工作職缺，真的就像搶便宜機票一樣，投履歷的時間每分每秒都很關鍵，越好的工作被搶走的機率越高。很重要所以說三次，學英文、學英文、學英文！出發前把英文學好，真的就不會有這麼多煩惱。</span></p>
<p><span style=""font-weight: 400;"">將來我要在國際的服務業中工作，在世界各國生活，幾經研究目前最可能的第一步是去菲律賓的線上博弈公司工作，不過還是要英文好一些，才會被錄取。目前這些博弈公司，還在招聘廣告上註明了多益500分以上，薪水可以多加一百美金。正在安排去菲律賓語言學校上密集英語課程三個月，一次把我的英文能力搞定，讓我一直能夠在國際服務業中工作，而且環遊世界。</span></p>
<p><span style=""font-weight: 400;"">如果大家去澳洲打工度假只是想出國體驗生活，建議你買張機票去玩個十天半月就好，若是真的要到澳洲打工度假，還是先把英文搞定再去吧！這樣子你的工作與生活才會多采多姿的。</span></p>"','published' => '1','og_title' => '出國打工度假，工作好找嗎？','og_description' => '打工度假工作真的好找嗎？上班到底是說英文還是中文？真的存得到錢嗎？來看看小資女Jing在澳洲打工度假的經驗分享，一次回答你關於打工度假最想知道的！','meta_title' => '出國打工度假，工作好找嗎？','meta_description' => '打工度假工作真的好找嗎？上班到底是說英文還是中文？真的存得到錢嗎？來看看小資女Jing在澳洲打工度假的經驗分享，一次回答你關於打工度假最想知道的！','canonical_url' => 'https://tw.english.agency/打工度假工作好找嗎','feature_image_alt' => '打工度假-澳洲','feature_image_title' => '澳洲打工度假經驗分享','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5940d76fc7e4b-ueznOxxnOc0T9Jod4o0dmTCeKUGLCl-1000x600.jpeg','author_id' => '1','category_id' => '','created_at' => '"2017-06-14 06:28:34"'],
  ['id' => '45','title' => '你覺得打工度假需不需要好英文?','slug' => '澳洲打工度假','excerpt' => '"去澳洲打工度假英文要很好嗎？ 以Jing在澳洲打工度假 2 年的經驗，他覺得答案是不一定！但有英文能力對工作是大加分，而且選擇更多元、機會更多、更有工作保障。"','content' => '"<p>我是 Jing，以我去澳洲打工度假 2 年的經驗來看，答案是不一定！</p>
<p><br />當了 2 年營業門市員的我，想要走出渾渾噩噩的上下班生活，毅然決然辭職，飛到墨爾本。而我得到的第一份工作就是在葡萄農場打工，一個很累，工作完就直接倒頭大睡的工作，每天就是搬運葡萄箱，一箱 10 公斤，2 元澳幣。其實打工度假會做這個主要是因為我英文沒有很好，如果想做服務業，我沒辦法順利和主管、客人溝通。現在想起來，我應該要先準備英文，再飛去澳洲找工作，雖然只要願意出勞力，基本上都有工作做，但還是要請代辦幫忙，因為英文實在不行，不太能跟農場主人溝通，只是代辦會抽傭金，還沒保險，可以說是黑工，而且超級累，只能跟大家說，有英文能力對工作是大加分，選擇更多元、機會更多、更有工作保障。</p>
<p>&nbsp;</p>
<p>順便來介紹一下澳洲打工度假的工作類型，這樣大家會更了解一點。分成 3 種工作性質：農牧業、餐飲業與服務業。<br />第一個農牧業類型，例如栽種蔬果、切割肉類、包裝農品&hellip;等，都屬於農牧業，以勞力取向為主的工作，也因為如此，不太需要英文能力，而這些也是澳洲人比較不願意做的。第二餐飲業，需要極好的英文，這樣接待客人、介紹菜色或是小聊一下才不會出錯。最後的服務業，例如超商的收銀員，英文需求是中等，稍微會一下就好了。總而言之，你如果想要薪水高，不太出勞力的話，餐飲業外場是首選，但是你的英文就真的需要強。</p>
<p>&nbsp;</p>
<p>而這種需要高英語能力的打工度假工作除了薪水不錯又相較下不累外，還有幾項優點。在工作的時你也可以增加國際觀，怎麼說也是為不同文化背景的人服務，彼此之間交流機會高，服務的同時不但可以擴展眼界，認知一些文化習慣的差異，而同時間你的英文也會因為常使用、常接觸，變得流利也擅用不同詞彙，可以說是一舉數得的工作。</p>
<p>&nbsp;</p>
<p>一般人都以為只要買張機票飛出國就可以增加眼界，就是開拓國際觀，殊不知英文不好，根本沒有機會跟外國人相處，實質上打交道，除了付出勞力和時間，打工度假的收穫明顯就只有賺旅費。所以，雖然說出國打工度假不一定要好英文，但好的英文才能讓你的旅程更值得，以後想起來不會只有「很累」二字而已。用我過來人的經驗跟朋友說後，他就決定先把英文程度提升，他的做法就是去菲律賓遊學。</p>
<p>&nbsp;</p>
<p>菲律賓學英文，乍聽之下很詭異，澳洲學一學直接去打工不就很方便嗎? 我也是後來一問才知道，菲律賓的教學方式採用一對一進行，主打密集式課程學習，每周上課40小時以上，可以說是高效率學習，而一般我們常聽的西方語言學校是小班制上課，一星期大概上20小時，所以真的想快速學英文，推薦大家菲律賓遊學這個選擇。學好英文再去打工度假也不遲。</p>"','published' => '1','og_title' => '你覺得打工度假需不需要好英文?','og_description' => '"去澳洲打工度假英文要很好嗎？ 以Jing在澳洲打工度假 2 年的經驗，他覺得答案是不一定！但有英文能力對工作是大加分，而且選擇更多元、機會更多、更有工作保障。"','meta_title' => '你覺得打工度假需不需要好英文?','meta_description' => '"去澳洲打工度假英文要很好嗎？ 以Jing在澳洲打工度假 2 年的經驗，他覺得答案是不一定！但有英文能力對工作是大加分，而且選擇更多元、機會更多、更有工作保障。"','canonical_url' => 'https://tw.english.agency/澳洲打工度假','feature_image_alt' => '打工度假英文要好嗎','feature_image_title' => '澳洲打工度假','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5940dcd026691-p0R7Win2K8npcIF3zPcfoP3Fx0cadX-1000x600.jpeg','author_id' => '1','category_id' => '','created_at' => '"2017-06-14 06:51:52"'],
  ['id' => '46','title' => '後打工度假人生，到菲律賓學英文讓我多益進步近300分！','slug' => '澳洲-打工度假-學英文','excerpt' => '"在 SME語言學校 第3個月，成績從剛開始的多益365分到現在的650分，但是距離我的目標800分還有一大段的距離，今天不藏私的和各位分享我是如何辦到的！"','content' => '"<h2><strong>四個月，365-650</strong></h2>
<p><span style=""font-weight: 400;"">這是我在 SME語言學校</span><span style=""font-weight: 400;"">&nbsp;的第3個月，準備邁入第4個月，成績從剛開始的多益365分到現在的650分3個月內有明顯的進步，但是距離我的目標800分還有一大段的距離，我知道剛開始進步是最快的，因為一開始不懂考試題型以及解題技巧練習到最後可以徹底發揮原來的實力。</span></p>
<p>而現在，才是艱難的開端，從文法，到單字，你自己必須一一的塞進腦袋中，剩下的1個月內我其實並沒有信心可以進步幅度跟之前一樣，但是老師們也都很鼓勵我，如果放棄就是真的輸了...</p>
<p>&nbsp;</p>
<hr />
<h2><img title=""澳洲打工度假"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/597cde4d47548-ENQqzp5w5xOHPgEughrjwDAtfeANNK-300x169.jpeg"" alt=""澳洲打工度假"" width=""300"" height=""169"" /></h2>
<h2>我恨英文，這都是哥倫布的錯！</h2>
<p><span style=""font-weight: 400;"">國中三年，高中三年，大學四年，沒有一學期不為英文補考而傷腦筋，從以前，只要拿出英文課本，腦袋就進入休眠模式，一度認為，是哥崙布的錯，因為他發現新大陸，所以造就現在的美國強國，不只他，幾乎對美國有貢獻的偉人都被我罵過一遍，由此可知我恨英文恨到這種成度。</span></p>
<p><span style=""font-weight: 400;"">來到SME語言學校</span><span style=""font-weight: 400;"">後，開始了學生生活，從沒想過會再回頭讀書，所以更加把握這次的機會，將我十年所需的東西在這邊一次性的補回來，很多人問我為什麼不出去玩，我的回答是我要得跟大家不一樣，有著努力過後的成果，以及用英文溝通絕佳環境，我想這是我要的，當然，這也是難以忘懷的回憶。</span></p>
<hr />
<h2>&nbsp;</h2>
<h2>我英文變好，關鍵不是澳洲打工度假！</h2>
<p><span style=""font-weight: 400;"">去年一整年我待在 澳洲打工度假 ，很多人認為這是我口說比一般人好的原因，其實並不是，在那邊的環境下，如果你一天到晚跟華人在一起其時也是無法進步的，當然是下班後用自己的時間加強，以及強迫自己跟外國人溝通，因為外國人在工作時絕對沒有耐心等妳查字典，等妳解釋，也不可能一而在在而三的重複。</span></p>
<p><span style=""font-weight: 400;"">所以當我在澳洲一簽簽證結束回台灣後，就興起來這念書的念頭，希望第二次回去時能有所不一樣，這也是講所學後時用的一個方法。不用擔心煩惱任何事物，只需要專心唸書就好，以及可以侃侃而談充滿自信!!</span></p>
<p><span style=""font-weight: 400;"">但是強者不不會被打敗的，從小的軍式教育就是要克服環境，強者就是要補強弱點，想想!!被笑大半輩子的英文也夠了！是該時候證明給那些笑我的人看了！</span></p>"','published' => '1','og_title' => '澳洲打工度假後，多益進步三百分','og_description' => '"在 SME語言學校 第3個月，成績從剛開始的多益365分到現在的650分，但是距離我的目標800分還有一大段的距離，今天不藏私的和各位分享我是如何辦到的！"','meta_title' => '澳洲打工度假後，多益進步三百分','meta_description' => '"在 SME語言學校 第3個月，成績從剛開始的多益365分到現在的650分，但是距離我的目標800分還有一大段的距離，今天不藏私的和各位分享我是如何辦到的！"','canonical_url' => 'https://tw.english.agency/澳洲-打工度假-學英文','feature_image_alt' => '澳洲打工度假','feature_image_title' => '澳洲打工度假','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/597cdcdfa5b84-NWLMNT8PjLj7i9xZwQUDSuZiXTcDm0-900x601.jpeg','author_id' => '1','category_id' => '3','created_at' => '"2017-06-14 08:31:03"'],
  ['id' => '47','title' => '"英國留學心得 ：留學英國，因為想成為更好的人"','slug' => '英國留學心得','excerpt' => '想成為更好的自己、看看不同的世界，所以到了英國，經過了十年，我深深覺得，英國留學這一路上沒有白走。年輕時經歷的一切成為後來工作上的養分，更多是對自我的要求。','content' => '"<p><span style=""font-weight: 400;"">因為想成為更好的自己，因為想看看不同的世界，所以選擇到了英國藝術大都會，經過了十年，我深深覺的，這一路上沒有白走。</span></p>
<p><span style=""font-weight: 400;"">年輕時經歷的一切成為後來工作上的養分，更多是對自我的要求。變美、變有氣質、變專業，不是每個年輕女孩的夢想嗎？我確定在英國留學中找到了，你/妳呢？這是我的 英國留學心得 ，分享給你們。</span></p>
<p><span style=""font-weight: 400;"">距離25歲時到英國留學，不知不覺竟然滿十年了。啊！不小心透露自己的年齡(羞)。最近接到一些邀約，希望可以分享 英國留學心得 。</span></p>
<p><span style=""font-weight: 400;"">我仔細回想這一路走來的心路歷程，從懵懂無知，一路曾經熱血沸騰，現階段的創業(接案)過程；從熱愛旅行亟欲探索世界，到現在享受一個人創作的寧靜，更加願意花時間投入有價值的工作，無論是否有金錢的回饋。一切都要感謝我到英國留學帶給我的成長，對許多人來說，留學等同於拿一張學歷證明(事實上那張畢業證書，薄薄一張A4，幾乎讓人忘記它的存在，淚!)，回到台灣職場上，更多時候還是要看工作能力和運氣。</span></p>
<hr />
<h1><strong>出國留學有必要嗎？這是一個好的選擇嗎？</strong></h1>
<p><span style=""font-weight: 400;"">最近在FB上看到一位朋友的經歷，覺得有必要和大家分享。35歲左右的他，曾經在美國、日本、中國大陸工作，近年來轉職成為企業顧問，尤其是輔導青年創業家。前幾天他開心地分享，他申請上台灣某國立大學碩士班，網友們紛紛祝賀他，並開玩笑說他一定最有實務經驗的碩士生。是的，在英國念碩士時，條件式入學分為兩種，一種是憑著大學成績申請，另一種是以工作資歷申請。</span></p>
<p><span style=""font-weight: 400;"">若是在學成績不理想，也有可能憑著豐富的社團經驗或工作經驗申請到不錯的學校。在那個沒有打工度假的時代，出國留學似乎是唯一可以遠赴重洋的機會，帶著父母的期待，戰戰兢兢地變成窮留學生一族。那時候流行的論壇是Hello UK、背包客棧、窮留學生的電鍋食譜等等，學業似乎是最重要的事。</span></p>
<p><span style=""font-weight: 400;"">時代變了，各國打工度假紛紛開放，甚至不需要語言能力也有許多國際志工的申請機會。這時候我不禁問自己「這樣還有留學的必要性嗎？」。大家出國在意的是什麼？旅行經驗？異國生存能力？賺錢？練習英文？求學彷彿成為最後的一條選擇。</span></p>
<p><span style=""font-weight: 400;"">回想起我的留學經驗交織著歡笑與眼淚，第一次勇敢地在異國說英語，獨立生活，學習下廚，結交到世界各地的朋友。傷心地是交往多年的男友竟因為遠距離而結束了五年的愛情，一個人很孤單呀！但是問我值不值得？我會覺得一切很值得。可以在二十多歲時，憑著自己在台灣幾年的工作經驗申請到英國碩士班，在課堂上自信地介紹著台灣，分享台灣的文化和產業現況。感謝當時的教授看見我的羞澀與努力，溫暖地給予加倍的關懷。</span></p>
<p>&nbsp;</p>
<hr />
<h1><strong>為什麼想到英國留學？</strong></h1>
<p><span style=""font-weight: 400;"">對我來說，這是一條理所當然的路。喜歡英文、熱愛藝術、學的是文學、不想到一個空曠沒有大眾交通工具的城市。所以選擇了英國，一開始真的很後悔，因為濃濃的英國腔讓我相當挫敗。但憑著熱情和努力，終於克服了，我想起語言課時老師說的一句話:「you will be fine after Christmas」半信半疑的我不解，為什麼聖誕節後英文會變好？後來才知道，英國人重視聖誕節，濃濃的過節氣氛讓路上每一個人露出開心的笑容。身邊的教會朋友熱情邀請作客，一杯紅酒下肚，派對的熱鬧氣氛讓人忘記異鄉的孤單。</span></p>
<p><span style=""font-weight: 400;"">在設計和文創尚未成為潮流之前，到英國念藝術的人仍然是少數。回想起自己一個人在倫敦泰晤士河畔看環球莎士比亞的戶外演出，一個人在寒冬中珍惜地吃著一碗WAGAMAMA熱呼呼拉麵(倫敦最大的平價拉麵連鎖店)，頓時覺得自己很勇敢(泣)。</span></p>
<p><span style=""font-weight: 400;"">因為想成為更好的自己，因為想看看不同的世界，所以選擇到了英國藝術大都會，經過了十年，我深深覺得，這一路上沒有白走。年輕時經歷的一切成為後來工作上的養分，更多是對自我的要求。變美、變有氣質、變專業，不是每個年輕女孩的夢想嗎？我確定在英國留學中找到了，你/妳呢？</span></p>"','published' => '1','og_title' => '"英國留學心得 ：留學英國，因為想成為更好的人"','og_description' => '想成為更好的自己、看看不同的世界，所以到了英國，經過了十年，我深深覺得，英國留學這一路上沒有白走。年輕時經歷的一切成為後來工作上的養分，更多是對自我的要求。','meta_title' => '"英國留學心得 ：留學英國，因為想成為更好的人"','meta_description' => '想成為更好的自己、看看不同的世界，所以到了英國，經過了十年，我深深覺得，英國留學這一路上沒有白走。年輕時經歷的一切成為後來工作上的養分，更多是對自我的要求。','canonical_url' => 'https://tw.english.agency/英國留學心得','feature_image_alt' => '出國留學心得-英國','feature_image_title' => '英國留學心得','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/597cd92d0761d-MTEtfGFWMULSj4vPHsebiKuIQGHLKM-1000x577.jpeg','author_id' => '1','category_id' => '3','created_at' => '"2017-06-14 08:47:24"'],
  ['id' => '48','title' => '"英文不好能不能出國打工度假？讓顧問Sean 告訴你！"','slug' => '英文不好能不能出國打工度假嗎','excerpt' => '"很多客戶都會問這句話，「 打工度假 ，需不需要英文很好？」答案是不一定！不過英文能力與打工性質有絕對密切的關聯性。英語能力好還是很加分的，也會比較容易找到工作！"','content' => '"<p><span style=""font-weight: 400;"">從事留遊學代辦與 打工度假 安排已經八年了。很多客戶都會問這句話，「 打工度假 ，需不需要英文很好？」</span></p>
<h3><strong>答案是不一定！</strong><span style=""font-weight: 400;"">不過英文能力與打工性質有絕對密切的關聯性。</span></h3>
<p><span style=""font-weight: 400;"">首先，要知道的是打工度假的工作多半是到了澳洲才找，工作有三種性質，一、農牧產業型，二、服務業型，三、餐飲業型。</span></p>
<p><span style=""font-weight: 400;"">農牧產業型的工作完全不需要英文，因為多半都是採摘農產品，清洗整理農產品，或是電視上報導很多的整理肉類或包裝肉類，這種工作有非常多的代辦，但農忙季節 (每年的十月到五月)很好找，可是服務業類的工作例如：超商收銀員等工作，就必須稍微會英文了。而餐飲業外場的工作就必須英文非常好了。總而言之，只要有足夠的勇氣及已經買好的來回機票，工作是不難找到的。但是英文不好的人只能在台灣人圈圈裡面找工作，往往是華人雇主或是華人代辦業者。幫你安排的工作，勞力性質較高的工作，比如說農場、屠宰場、蝦場等澳洲本地人不想要從事的工作類型。這些勞力工作可以讓你存到錢存錢，體驗會多過於你實質的人生影響。</span></p>
<p><span style=""font-weight: 400;"">從我過去客戶的經驗看來，倘若在澳洲從事餐廳跟旅館等的工作，確實可以累積國際工作與國際溝通經驗，可是大部分英文不好又想去打工度假的人，常幻想會在澳洲能跟許多國際人士用英文溝通，英文可以自動搞好，那確實是不會發生的。我從韓國的留遊學同業那裡知道，韓國客戶中想要去澳洲從事餐飲或旅館相關工作的，而又有英文障礙，通常會先去都是先去菲律賓學習八周的英文，再前往澳洲打工。</span></p>
<p><span style=""font-weight: 400;"">有的客戶問我，學英文在澳洲學不就好了嗎？為什麼要選擇菲律賓呢？以我的了解澳洲跟菲律賓的語言學校教學法有相當的差異，澳洲是小班制的，每周上課時數是二十小時，而菲律賓是一對一式的教學，每周上課時數是四十小時以上。所以密集度與所需要的期間和成本大大的不同。尤其是是想要移民到澳洲的人，還需要雅思成績，菲律賓語言學校的雅思保證班平均三個月雅思可以進步一到一點五分，比起澳洲的語言學校CP值高很多。</span></p>
<p><span style=""font-weight: 400;"">我有一位對咖啡業的客戶，想到澳洲的咖啡廳打工度假，我就建議他，先前往菲律賓先將英文打底，再到澳洲才有可能去咖啡廳工作，還可以去上一些咖啡相關的課程。</span></p>"','published' => '1','og_title' => '"英文不好能不能出國打工度假？讓顧問Sean 告訴你！"','og_description' => '"很多客戶都會問這句話，「 打工度假 ，需不需要英文很好？」答案是不一定！不過英文能力與打工性質有絕對密切的關聯性。英語能力好還是很加分的，也會比較容易找到工作！"','meta_title' => '"英文不好能不能出國打工度假？讓顧問Sean 告訴你！"','meta_description' => '"很多客戶都會問這句話，「 打工度假 ，需不需要英文很好？」答案是不一定！不過英文能力與打工性質有絕對密切的關聯性。英語能力好還是很加分的，也會比較容易找到工作！"','canonical_url' => 'https://tw.english.agency/英文不好能不能出國打工度假嗎','feature_image_alt' => '打工度假-澳洲','feature_image_title' => '英文不好能去打工度假嗎','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5940fcb358859-JHO9UqzbZFwjAvIDNBjbpahTek0dkY-1000x600.jpeg','author_id' => '1','category_id' => '3','created_at' => '"2017-06-14 09:07:29"'],
  ['id' => '49','title' => '托福考試內容到底有多難？三分鐘帶你認識托福！','slug' => '托福考試內容','excerpt' => '你是否正在尋找完整的托福準備資訊呢？那這篇文章就是你要的！詳細介紹托福考試內容中的各種細節和準備方法，讓你迎戰托福能夠自信滿滿、勝券在握！','content' => '"<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">你是否正在尋找完整的托福準備資訊呢？想必接下來的內容對你會有很大幫助！</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;""><a href=""http://www.toefl.com.tw/"">托福（TOEFL）</a>是典型的英文能力測驗之一，全球各地的考生都會藉由測驗來檢視自己的英語程度，優異的測驗成績更是升學和留學海外必備。其中，托福網路測驗（</span><span style=""font-weight: 400;"">TOEFL iBT</span><span style=""font-weight: 400;"">&reg; </span><span style=""font-weight: 400;"">TEST</span><span style=""font-weight: 400;"">）採取最新型態的測驗方式，考生必須使用麥克風和耳機，透過網路連線進行聽、說、讀、寫四大面向的英語測驗。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">測驗的四大面向獨立計分，每一部分的分數為0~30分，總分則是介於0~120分。考試時間大約4個小時，中間有10分鐘的休息時間，測驗前半段進行閱讀和聽力測驗，後半段則是口說和寫作測驗。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">以下一一介紹托福中的各種細節和準備方法，讓你迎戰托福能夠自信滿滿、勝券在握！</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>一、托福閱讀題型</strong></h2>
<p><span style=""font-weight: 400;"">・題型：</span><span style=""font-weight: 400;"">Vocabulary(單字題)、Reference(猜代名詞對象)、Rhetorical Purpose(猜句子功能)、Factual Information(猜選項的對與錯)、Inference(句子推敲)、Sentence Simplification(簡化句子含意)、Insert Text(句子填空)、Prose Summary(主旨)、Fill Table(配對)</span></p>
<p><span style=""font-weight: 400;"">・題數：36~56題</span></p>
<p><span style=""font-weight: 400;"">・考題分佈：基本上是三組閱讀測驗，遇到加試題時總共會有五組閱讀測驗，每一篇閱讀測驗的篇幅大約在700~800字之間(約兩面A4紙的文字量)。</span></p>
<p><span style=""font-weight: 400;"">・測驗時間：60~80分鐘</span></p>
<p><span style=""font-weight: 400;"">・如何準備：托福考試時間其實都不長，但出題方向大多是較學術性的文章，不過測驗的並不是考生的專業知識，而是思考邏輯和理解能力。因此平時可以多閱讀學術性文章、做TPO的模擬考題(見六、)多了解各領域的字彙。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>二、托福聽力測驗</strong></h2>
<p><span style=""font-weight: 400;"">・題型：</span><span style=""font-weight: 400;"">Conversation(情境對話題)、Lecture(長篇敘述題)</span></p>
<p><span style=""font-weight: 400;"">・題數：34~51題</span></p>
<p><span style=""font-weight: 400;"">・考題分佈：基本上是兩組聽力測驗，遇到加試題時總共會有三組聽力測驗，每一組聽力測驗的題型依序為</span><span style=""font-weight: 400;"">Conversation、Lecture、Lecture</span><span style=""font-weight: 400;"">。</span></p>
<p><span style=""font-weight: 400;"">・測驗時間：60~90分鐘</span></p>
<p><span style=""font-weight: 400;"">・如何準備：平時可以多聽TED、看CNN影片來練習聽力，並且加強筆記能力，在正式測驗時必須能夠一邊聽錄音內容，一邊記下關鍵字。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>三、TOFEL口說測驗</strong></h2>
<p><span style=""font-weight: 400;"">・題型：</span><span style=""font-weight: 400;"">Independent test(問答題)、Integrated test(整合論述題)</span></p>
<p><span style=""font-weight: 400;"">・題數：6題</span></p>
<p><span style=""font-weight: 400;"">・考題分佈：第1~2題為</span><span style=""font-weight: 400;"">Independent test，作答內容因人而異；第3~6題為Integrated test，需要從獲得的資訊中發表自己的看法。</span></p>
<p><span style=""font-weight: 400;"">・測驗時間：20分鐘</span></p>
<p><span style=""font-weight: 400;"">・如何準備：由於正式測驗的思考時間非常短，因此整理訊息的速度要快、隨機應變的能力也要好。平常可以多參加讀書會練習口說對話，如果有不錯的句子和想法也可以記下來應用，增進自己的口說流暢度和詞彙量。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>四、TOFEL寫作題型</strong></h2>
<p><span style=""font-weight: 400;"">・題型：</span><span style=""font-weight: 400;"">Intergrated(整合寫作題，建議字數150~225字)、Independant(獨立寫作題，建議字數300字以上)</span></p>
<p><span style=""font-weight: 400;"">・題數：2題</span></p>
<p><span style=""font-weight: 400;"">・考題分佈：第一題是</span><span style=""font-weight: 400;"">Intergrated，必須整合資訊寫出對方的想法；第二題是Independant，可以根據個人經驗和看法進行寫作。</span></p>
<p><span style=""font-weight: 400;"">・測驗時間：50分鐘</span></p>
<p><span style=""font-weight: 400;"">・如何準備：可以參考市面上的寫作練習書，多背一些寫作時可以運用的固定句型，並且多做TPO的模擬考題來累積實際的寫作經驗。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>五、加試題</strong></h2>
<p><span style=""font-weight: 400;"">加試題是用來測驗整體考生程度、會在常規題數之外再額外增加的題目。加試題沒作答並不會計分，若是加試題分數不高的考生居多的話，則會提高整體考生的分數，因此考生可以選擇休息不作答，也可以選擇賭一下加試題來提升自己的分數。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>六、TPO(</strong><strong>TOFEL Practice Online</strong><strong>)</strong></h2>
<p><span style=""font-weight: 400;"">TPO是</span><span style=""font-weight: 400;"">能夠在線上進行托福測驗的模擬考試，考生不但可以透過測驗更加了解托福測驗的題型，也可以事先模擬測驗時的平台操作情形，有助於安定考生的心情。現在也有一些網站有提供免費的TPO考題，例如中國的</span><a href=""http://toefl.kmf.com/read/order""><span style=""font-weight: 400;"">TOEFL考滿分</span></a><span style=""font-weight: 400;"">，建議考生們可以多加利用。</span></p>
<p>錯<img class=""img-responsive"" title=""TOFEL考試內容"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/597acdca9f558-TvIJZPZzr9eNTrkUw3L9X4u6u6doZo-1000x719.png"" alt=""托福考試內容"" width=""1000"" height=""719"" />&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>看完考題，所以有哪些資料可以幫助準備托福考試？</h2>
<p>面對托福考題，其實有很多的準備辦法，除了參考仿間的參考書，你也可以到以下這些資源網站參考：</p>
<p>&nbsp;</p>
<p>1.TOEFL 托福世界</p>
<p>提供大量托福考試、最新時事資訊、又或是與英文學習相關的英文報導， 都可以在托福世界找到，他們提供了：</p>
<p>相關留遊學準備需要知道的資訊、托福考試的最新情報、各國名人專訪，這些都是<a title=""準備托福"" href=""http://www.toeflworld.com.tw/"">準備托福</a>的好工具！</p>
<p>&nbsp;</p>
<p>2.TOEFL 官方：</p>
<p>托福官方不單單只是公布最新的考試資訊、處理報名問題，他們也會給予考生免費的範例試題，告訴考生相關的作答技巧喔！</p>
<p>&nbsp;</p>
<hr />"','published' => '1','og_title' => '托福考試內容到底有多難？三分鐘帶你認識托福！','og_description' => '你是否正在尋找完整的托福準備資訊呢？那這篇文章就是你要的！詳細介紹托福中的各種細節和準備方法，讓你迎戰托福能夠自信滿滿、勝券在握！','meta_title' => '托福考試內容到底有多難？三分鐘帶你認識托福！','meta_description' => '你是否正在尋找完整的托福準備資訊呢？那這篇文章就是你要的！詳細介紹托福中的各種細節和準備方法，讓你迎戰托福能夠自信滿滿、勝券在握！','canonical_url' => 'https://tw.english.agency/托福考試內容','feature_image_alt' => '托福準備方法','feature_image_title' => '托福準備','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/597aacfb432d1-SqJWRtzEUNs2TZ7u4cMfXutpgUlveB-1800x822.png','author_id' => '2','category_id' => '15','created_at' => '"2017-07-26 03:20:39"'],
  ['id' => '50','title' => '去菲律賓遊學生活，你該知道的風俗民情！','slug' => '菲律賓遊學生活','excerpt' => '考慮去菲律賓遊學，卻擔心不適應當地的文化風俗，害怕觸犯禁忌，難以融入嗎？我們整理菲律賓的風俗民情介紹，讓你更了解當地，做好行前預備，輕鬆出發！','content' => '"<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p><span style=""font-weight: 400;"">你正在考慮去菲律賓遊學，卻擔心不適應當地的文化、風俗，害怕觸犯禁忌，難以融入嗎？</span></p>
<p><span style=""font-weight: 400;"">我們整理菲律賓的風俗民情介紹，讓你更加了解當地文化與特色，無論留學、旅遊、生活都能做好行前預備，輕鬆出發！</span></p>
<p>&nbsp;&nbsp;</p>
<hr />
<h2><strong>乾熱的千島之國</strong></h2>
<p><span style=""font-weight: 400;"">　　菲律賓座落於環太平洋，屬於熱帶國家，由七千多個島嶼組成，擁有世界第十二名人口數，約一億多人。菲律賓有豐富的自然資源，有熱帶國家的炎熱多雨，也有適合渡假的島嶼美景。前往菲律賓記得留意季節──菲律賓全年分為三個季節：3-5月的熱季、6-11月的雨季和12-2月的涼季。</span></p>
<p><span style=""font-weight: 400;"">　　雖然如此，其實菲律賓全年氣候相近，平均溫度在攝氏27度左右，而且菲律賓的熱和台灣不同，屬於乾熱，比較不會像在蒸氣房那樣滿身大汗，可說是旅人的福音。而某些地區（如宿霧、碧瑤）的氣溫較為涼爽，全年都適合造訪。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>融合東西方文化</strong></h2>
<p><span style=""font-weight: 400;"">　　經歷西班牙、美國和日本的政權，菲律賓融合多元文化，在不同地區留有不同的文化古蹟。由於深受西方文化影響，菲律賓以英語為官方語言，街道上也常可看到速食餐廳，西方的音樂、電影也深受喜愛。</span></p>
<p><span style=""font-weight: 400;"">　　特別的是，受到西班牙三百年的統治影響，不僅讓菲律賓成為亞洲唯二的天主教國家，也留給菲律賓人享用午茶的習慣──在菲律賓，多數的雇主都會給予員工午茶時間，無論是上午茶還是下午茶時段，員工都享有隨意的喝茶、吃點心時光，令人羨慕不已。</span></p>
<p><span style=""font-weight: 400;"">　　與此同時，菲律賓人也十分喜愛穀類食物，他們的主食多半以米飯為主，生活中也有亞洲文化留下的痕跡。菲律賓融合東西方文化，來到菲律賓，可以好好體驗獨特的文化風情。</span></p>
<h2>&nbsp;</h2>
<hr />
<h2><strong>菲律賓的風俗、習慣、禁忌</strong></h2>
<p><span style=""font-weight: 400;"">　　在菲律賓有「禁忌之手」的說法，認為左手是不潔、污穢的，右手則是用來吃飯與交際的。因此在社交場合，盡量避免用左手與他人互動，尤其在握手的時候，是保持禮數的好方法。</span></p>
<p><span style=""font-weight: 400;"">　　此外，在菲律賓還有一個須留意的習俗：收到禮物時，不能當面拆開，否則是對送禮者很大的不尊重。這和許多國家的想法很不一樣，到當地需多留意。</span></p>
<p><span style=""font-weight: 400;"">　　如果到菲律賓的偏遠鄉村，依然能體驗到傳統的古老風俗，比如巫醫、通靈、傳統的鬥雞競技。來到菲律賓的叢林，將能體會到這裡延續數百年未變的生活型態，有待訪客發掘。</span></p>
<p><br /><br /></p>
<hr />
<h2><strong>節慶眾多的菲律賓</strong></h2>
<p><span style=""font-weight: 400;"">　　菲律賓的節日之多世界有名，全國大大小小的節日有幾百個，全國性的節日也有二十多個。在不同地區，分別有充滿民族、宗教意義的不同節慶。</span></p>
<p><span style=""font-weight: 400;"">　　旅行社總會告訴旅客：不要錯過夏天來訪菲律賓的機會，這裡的夏季節慶很多！光列出菲律賓的重要節日，就已經非常多元：四月復活節、五月花節、六月國慶日及皮納圖博火山節、八月尼諾伊&middot;艾奎諾記念日及國家英雄日、十一月波尼法西歐紀念日及萬聖節&hellip;&hellip;等。依據不同的城市和省份，可以找到不同的假日和節慶活動，前往菲律賓前不妨做好功課，到當地一同慶祝、感受熱鬧氛圍！</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '0','og_title' => '去菲律賓遊學生活，你該知道的風俗民情！','og_description' => '考慮去菲律賓遊學，卻擔心不適應當地的文化風俗，害怕觸犯禁忌，難以融入嗎？我們整理菲律賓的風俗民情介紹，讓你更了解當地，做好行前預備，輕鬆出發！','meta_title' => '去菲律賓遊學生活，你該知道的風俗民情！','meta_description' => '考慮去菲律賓遊學，卻擔心不適應當地的文化風俗，害怕觸犯禁忌，難以融入嗎？我們整理菲律賓的風俗民情介紹，讓你更了解當地，做好行前預備，輕鬆出發！','canonical_url' => 'https://tw.english.agency/菲律賓遊學生活','feature_image_alt' => '菲律賓文化風俗','feature_image_title' => '菲律賓遊學','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59781d080c8b7-07d3wNpCccxYi1QIpdXJX44oxRvaFq-1600x1067.jpeg','author_id' => '7','category_id' => '1','created_at' => '"2017-07-26 04:40:21"'],
  ['id' => '51','title' => '菲律賓是個怎麼樣的國家？三分鐘帶你快速認識！','slug' => '菲律賓遊學-旅遊','excerpt' => '想去菲律賓遊學但覺得這個國家很陌生嗎？看完這篇文章你就能一次瞭解最深入的菲律賓！從當地治安到菲律賓的英文語言課程，一次講解給你聽！','content' => '"<p><span style=""font-weight: 400;"">四季如夏的菲律賓位於東南亞，熱帶氣候群島國家特性，整年氣候僅有乾季（12月至次年5月）與雨季（ 7月至12月）。同時群島環海特性具有得天獨厚的居住優勢，造就了優美寧靜的生態環境，因此，每年吸引不少人前往觀光度假。</span></p>
<p>然而不論是去菲律賓遊學、菲律賓學英文還是要去菲律賓旅遊的朋友通常都會去碧瑤（baugio）、宿霧（cebu）、克拉克（clark）、巴拉望（palawan）以及首都馬尼拉（manila），而其中各睇的氣候與特色也都不盡同，但馬尼拉、宿霧這兩座大城市可以說是菲律賓數一屬二開發程度與台灣不相上下的高度發展市區。</p>
<p>（如果想認識更多菲律賓相關資訊，你可參考-<a title=""菲律賓觀光局"" href=""http://www.itsmorefuninthephilippines.com.tw/"">菲律賓觀光局</a>）</p>
<p>&nbsp;</p>
<p><img title=""馬尼拉-菲律賓遊學"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/598421466ceb6-JWS7s0ruZYY5IUbVLqPRq0fZ4divgS-600x750.jpeg"" alt=""馬尼拉-菲律賓遊學"" width=""600"" height=""750"" /></p>
<p>&nbsp;</p>
<hr />
<h2><strong>旅行心態做好行前準備可增加對於菲律賓治安的信心</strong></h2>
<p><span style=""font-weight: 400;"">無論是準備前往菲律賓度假、出差或是讀書，可能對於該國的治安感到緊張。從另外一個角度來看，台灣算是全球數一數二安全的國家，但還是有治安較差的地方，所以基本上保持著一個正面的態度看待更為重要。馬尼拉、宿霧及達沃等大都市的學校、賣場與銀行等皆有保全人員看守，治安是相對安全的。而就如同前往日本、美國與歐洲等國家旅行般，不要一人在深夜外出，隨身攜帶貴重物品，注意隨身行李，以確保個人安全。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;""><img title=""菲律賓治安"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/598415d4df701-g9VgcNRtHD0h4QHeCic72UPBFOzWLf-550x366.jpeg"" alt=""菲律賓治安"" width=""550"" height=""366"" /></span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>沈浸式學習體驗－英語的官方語言加速語言學習</strong></h2>
<p><span style=""font-weight: 400;"">而菲律賓在歷經西班牙、美國與日本的殖民下，除了95.5%為南島語系民族，包括他加祿人、伊洛戈人、邦班牙人、米沙鄢人和比科爾人外，更有日本人、西班牙人、美國人等後裔，成為宗教與文化的大熔爐，並具備了多國的語言優勢，如英語為菲律賓的官方語言。同時身為東亞國協的其中一員，菲律賓更是東亞國家中最具發展潛力的區域，更是吸引不少外商公司進駐與英語系國家將客服中心設。因此，多數菲律賓人具有正統的美式口音。</span></p>
<p>&nbsp;</p>
<p>也因為正統的美式口音、高比例的英語人口，促使了菲律賓成為了亞洲英語學習的新天地，因此每年陸續前往菲律賓學英文的人逐漸變多，也逐間有不少韓國人、日本人、俄國人、勝制式願在東歐的朋友前往菲律賓語言學校學習！</p>
<p>此外因為菲律賓的幣值，使得菲律賓學英文相較於其他學習方案上，更經濟實惠，而且相當於一次出國旅遊的費用！</p>
<p><span style=""font-weight: 400;""><img title=""菲律賓遊學"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59841aad7e3f2-VbzO8KqybLg1zS2897JJvGIeyjAEOs-300x300.jpeg"" alt=""菲律賓遊學"" width=""300"" height=""300"" /></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2><strong>英語學習的CP值首選提升個人競爭力</strong></h2>
<p><span style=""font-weight: 400;"">看重菲律賓學習英語環境的優點，以及與英國及美加等英語系國家留學相比的價格優勢，吸引不少韓國人與日本人前往提升英語能力。同時菲律賓許多優良信譽佳的語言學校會嚴格把關教學師資，無論是外籍或菲籍老師皆須通過教學認證，其中口音為重要審核關鍵，確保學生可學到正統的英文發音。</span></p>
<p><span style=""font-weight: 400;"">而對於菲律賓的環境、語言學校以及英文的不熟悉，可能會讓人不知如何下手開始計劃，因此建議找菲律賓遊學代辦協助安排。透過專業建議量身挑選最適合的英語課程，同時代辦也可以協助購買機票、申請簽證、住宿安排等事宜。更重要的是，代辦因為與語言學校有合作方案，能提供優惠的價格及優惠。建議可找代辦諮詢，讓你的菲律賓遊學規劃更加輕鬆有效率，享受物超所值的學習！</span></p>
<p><span style=""font-weight: 400;""><img title=""菲律賓學英文"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59841bb96ed30-hZA91s1Ej8vyZe6e2krxkgB2fpPqp4-300x131.png"" alt=""菲律賓學英文"" width=""300"" height=""131"" /></span></p>
<p>&nbsp;</p>
<p>參考資料：</p>
<p><a title=""菲律賓觀光局"" href=""http://www.itsmorefuninthephilippines.com.tw/"" target=""_blank"" rel=""noopener noreferrer"">菲律賓觀光局</a></p>
<p><a title=""GoEducation 為什麼要去菲律賓遊學"" href=""https://www.goeducation.com.tw/菲律賓學英文"" target=""_blank"" rel=""noopener noreferrer"">仁育國際教育GoEducation-為什麼要去菲律賓遊學？</a></p>
<p><a title=""i-connect"" href=""http://i-connectglobal.com/%E8%AA%9E%E8%A8%80%E5%AD%B8%E6%A0%A1%E6%8E%A8%E8%96%A6/%E8%8F%B2%E5%BE%8B%E8%B3%93/"" target=""_blank"" rel=""noopener noreferrer"">I-connect</a></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '菲律賓是個怎麼樣的國家？三分鐘帶你快速認識！','og_description' => '想去菲律賓遊學但覺得這個國家很陌生嗎？看完這篇文章你就能一次瞭解最深入的菲律賓！從當地治安到菲律賓的英文語言課程，一次講解給你聽！','meta_title' => '菲律賓是個怎麼樣的國家？','meta_description' => '想去菲律賓遊學但覺得這個國家很陌生嗎？看完這篇文章你就能一次瞭解最深入的菲律賓！從當地治安到菲律賓的英文語言課程，一次講解給你聽！','canonical_url' => 'https://tw.english.agency/菲律賓遊學-旅遊','feature_image_alt' => '菲律賓介紹','feature_image_title' => '菲律賓特色','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59842b0f51353-zSRGweNFHDISb3TaiD1bWalmmKwA88-800x534.jpeg','author_id' => '1','category_id' => '1','created_at' => '"2017-07-26 05:10:28"'],
  ['id' => '52','title' => '去菲律賓遊學？菲律賓治安大解析','slug' => '菲律賓遊學-治安-解析','excerpt' => '關於治安的擔憂，一直阻止許多人造訪菲律賓，對於想去菲律賓遊學的人來說，語言學校的安全管理非常重要，究竟菲律賓治安如何呢？這篇文章將為你一一介紹。','content' => '"<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">許多台灣人一聽到菲律賓遊學，總會皺眉詢問：「那裡是不是治安不好啊？」</span></p>
<p><span style=""font-weight: 400;"">關於治安的擔憂，一直阻止許多人造訪這個國家，但近年來菲律賓經過政治、經濟的變革，已成為亞洲地區不可忽視的新興勢力，在治安方面，也隨著各地區而有極大不同。</span></p>
<p><span style=""font-weight: 400;"">對於想去菲律賓遊學的人來說，菲律賓語言學校的安全管理非常重要，究竟菲律賓語言學校常見的地區位置和治安狀況如何呢？以下為你一一介紹。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>菲律賓語言學校類型</strong></h2>
<p>&nbsp;</p>
<ol>
<li><strong> 鄰近觀光景點</strong></li>
</ol>
<p><span style=""font-weight: 400;"">　　有些菲律賓語言學校以鄰近觀光景點為賣點，擁有便利的交通與豐富的自然資源，也是兼顧學語言及渡假的好所在。像是宿霧、長灘島有許多這類型的學校。</span></p>
<p>&nbsp;</p>
<ol start=""2"">
<li><strong> 大都會、市中心</strong></li>
</ol>
<p><span style=""font-weight: 400;"">　　有些菲律賓語言學校選擇開設在繁華的大都會市中心，例如馬尼拉。這類地區有許多消費娛樂設施，包括購物中心、夜店、商城等，可以享受城市的便利、繁華，但相對的也要忍受複雜的生活環境。</span></p>
<p>&nbsp;</p>
<ol start=""3"">
<li><strong> 郊區</strong></li>
</ol>
<p><span style=""font-weight: 400;"">　　在較偏遠的山區，有些地方以涼爽的氣候聞名，遠離都會也離開了城市的誘惑，可以專心讀書。碧瑤或克拉克等地區是這類型的例子。</span></p>
<p><br /><br /></p>
<hr />
<h2><strong><img title=""Cebu city "" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0bfa1b27e68-CDSZzDVXcOEgmC06Dv2Aqr4gRmHL7h-910x1024.jpeg"" alt=""Cebu city "" width=""100%"" height=""auto"" /></strong></h2>
<h2><strong>常見菲律賓語言學校地區</strong></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">　　菲律賓的菲律賓語言學校有不同類型，分佈的地區也各有不同。以下列出較常見的菲律賓語言學校地區及特色介紹：</span></p>
<ol>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""馬尼拉"" href=""https://zh.wikipedia.org/wiki/%E9%A9%AC%E5%B0%BC%E6%8B%89"" target=""_blank"" rel=""noopener noreferrer"">馬尼拉（Manila）</a>：菲律賓首都，資源多，治安相對較差。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""宿霧市介紹"" href=""https://zh.wikipedia.org/wiki/%E5%AE%BF%E9%9C%A7%E5%B8%82"">宿霧（Cebu）</a>：菲律賓第二大城，治安優於馬尼拉，生活機能好。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""怡朗地區介紹"" href=""https://zh.wikipedia.org/wiki/%E4%BC%8A%E6%B4%9B%E4%BC%8A%E6%B4%9B%E5%B8%82"" target=""_blank"" rel=""noopener noreferrer"">怡朗（Iloilo）</a>：菲律賓第三大城，消費較低，風氣純樸，曾獲選為菲律賓最安全的城市。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">巴科羅（Bacolod）：菲律賓第四大城，鄉村的生活型態，消費較低。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""碧瑤市"" href=""https://zh.wikipedia.org/wiki/%E7%A2%A7%E7%91%B6%E5%B8%82"" target=""_blank"" rel=""noopener noreferrer"">碧瑤（Baguio）</a>：菲律賓夏季的首都，氣候涼爽的避暑勝地，治安較好。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""克拉克地區介紹"" href=""https://www.tripadvisor.com.tw/Tourism-g3336531-Clark_Freeport_Zone_Angeles_City_Pampanga_Province_Central_Luzon_Region_Luzon-Vacations.html"" target=""_blank"" rel=""noopener noreferrer"">克拉克（ Clark）</a>：屬於美軍基地，外國人多，費用較高、治安較好。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""長灘島（Boracay）"" href=""https://zh.wikipedia.org/wiki/%E9%95%BF%E6%BB%A9%E5%B2%9B"" target=""_blank"" rel=""noopener noreferrer"">長灘島（Boracay）</a>：前往這裡遊學的人多以玩樂為主，風景優美、消費較高。</span></li>
</ol>
<p><br /><br /></p>
<hr />
<h2><strong><img title=""cebu-city"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0bf97508301-KCttF4R1qXP9yB1m4G23WCkaIQ1MlU-800x592.jpeg"" alt=""cebu-city"" width=""100%"" height=""auto"" /></strong></h2>
<h2><strong>學校週邊的治安狀況</strong></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">　　菲律賓語言學校週邊的治安如何？以宿霧市和碧瑤市為例。在宿霧，根據全球數據庫（Numbeo）網站的資料，宿霧治安和美國舊金山有著差不多的安全指數，比菲律賓首都馬尼拉來得安全。宿霧也是近年來的熱門觀光地點、菲律賓遊學首選，許多人潮聚集處都有警衛，安全有一定的保障。此地的菲律賓語言學校多設有門禁與保全，禁止學生在深夜出門，避免可能的危險。</span></p>
<p><span style=""font-weight: 400;"">　　而碧瑤作為菲律賓夏天的首都，也是以文教為主的城市，那裡同事許多想要前往菲律賓學英文的人參考選項之一，政府機關相當用心維護這座大學城的治安，因此碧瑤是菲律賓較安全的城市。碧瑤的菲律賓語言學校許多以傳統的斯巴達式英文教學為主，適合需要埋頭苦讀、衝刺學習的學生。</span></p>
<p><span style=""font-weight: 400;"">　　最近的菲律賓，在總統杜特蒂的鐵腕統治之下，治安狀況有所好轉。馬尼拉的治安比較混亂，其他地區則各有不同。前往留學前，最好尋找有信譽、負責任的代辦當面諮詢，並選擇知名度較高、管理健全的菲律賓語言學校，讓菲律賓的遊學生活更加安心有保障。</span></p>
<p>&nbsp;</p>
<hr />
<h2><img title=""宿霧鯨鯊"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0bf813c3759-9S4IDo7j8GEEdDOuqwDQw6SuVr78IG-300x169.jpeg"" alt=""宿霧鯨鯊"" width=""100%"" height=""auto"" /></h2>
<h2>那了菲律賓遊學，還可以去哪裡旅遊呢？</h2>
<p>菲律賓除了是近年來興起的英文學習聖地，同時也是許多人嚮往的旅遊景點，<a href=""https://phfuntour.tw"">宿霧自由行</a>、薄荷島跳島、又或是資生堂海鮮，但是越往菲律賓南部則有些旅遊警示區，那一帶建議不要去拜訪。</p>
<p>但如果想再菲律賓旅遊，你可以透過學校經理、或是在地的旅行社協助安排，但通常學校本身就會安排相關活動，像是跳島、浮潛、又或是在市區遊覽歷史古蹟喔！</p>
<p>需要注意的是在部分景點的交通可能需要注意班次、或是特定的交通工具，避免出遊後回程有困難喔！</p>
<hr />"','published' => '1','og_title' => '菲律賓遊學生活，治安解析','og_description' => '關於治安的擔憂，一直阻止許多人造訪菲律賓，對於想去菲律賓遊學的人來說，語言學校的安全管理非常重要，究竟菲律賓治安如何呢？這篇文章將為你一一介紹。','meta_title' => '菲律賓遊學生活，治安解析','meta_description' => '關於治安的擔憂，一直阻止許多人造訪菲律賓，對於想去菲律賓遊學的人來說，語言學校的安全管理非常重要，究竟菲律賓治安如何呢？這篇文章將為你一一介紹。','canonical_url' => 'https://tw.english.agency/菲律賓遊學-治安-解析','feature_image_alt' => '菲律賓遊學治安','feature_image_title' => '菲律賓治安','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/597afb694c270-vSFtQiNipaaPZSJqJguL7A9CI9uNi8-1600x1067.jpeg','author_id' => '1','category_id' => '1','created_at' => '"2017-07-26 05:36:40"'],
  ['id' => '53','title' => '托福要怎麼報名？考前多久前報名呢？','slug' => '托福報名方法','excerpt' => '托福是申請國外大學的必備證明，報名方式需在官方網站操作，對於不熟悉的人難免因為陌生而卻步。報名也有許多細節要注意，現在就讓我們來看看如何報名！','content' => '"<p><a href=""http://www.toefl.com.tw/"">托福</a>（TOEFL）是許多學子申請國外大學的必備證明，它的報名方式需報考者在官方網站操作，對於不熟悉的人難免因為陌生而卻步。同時，報名也有許多細節要注意，現在就讓我們來看看如何報名，和選擇自己想要的場次吧。</p>
<p>&nbsp;</p>
<hr />
<h2><strong>重點一</strong><strong>: 報名步驟的網站操作要點</strong></h2>
<p><span style=""font-weight: 400;"">1.點進</span><a href=""http://www.ets.org""><span style=""font-weight: 400;"">ETS官方網站</span></a><span style=""font-weight: 400;"">&rarr;選擇 Register for the TOEFL iBT&reg; Test&rarr;進入 </span><a href=""https://www.ets.org/bin/getprogram.cgi?test=toefl""><span style=""font-weight: 400;"">報名系統</span></a><span style=""font-weight: 400;""> &rarr;選擇考試地點 (select a &nbsp;testing location)&rarr;台灣 (Taiwan)，完成註冊後&rarr;選擇地點(location)。</span></p>
<ul>
<li><a href=""http://www.post.gov.tw/post/internet/Postal/index.jsp?ID=207""><span style=""font-weight: 400;"">中文地址音譯</span></a></li>
</ul>
<p>&nbsp;</p>
<p><strong>2.</strong><span style=""font-weight: 400;"">以下畫面，選擇日期 </span><span style=""font-weight: 400;"">(以下擷取部分畫面說明)</span></p>
<p><span style=""font-weight: 400;"">建議給一段彈性 例如 ：2017.09.21-2017.10.22</span></p>
<table style=""width: 413px;"">
<tbody>
<tr>
<td style=""width: 119px;"">
<p><strong>Region</strong></p>
</td>
<td style=""width: 81px;"">
<p><strong>Testing Format</strong></p>
</td>
<td style=""width: 43px;"">
<p><strong>Fee</strong></p>
</td>
<td style=""width: 152px;"">
<p><strong>Test Dates</strong></p>
</td>
</tr>
<tr>
<td style=""width: 119px;"">
<p><strong>Taiwan (central)</strong></p>
</td>
<td style=""width: 81px;"">
<p><strong>TOEFL </strong></p>
</td>
<td style=""width: 43px;"">&nbsp;</td>
<td style=""width: 152px;"">&nbsp;</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table>
<tbody>
<tr>
<td>
<p><strong>Taiwan (north)</strong></p>
</td>
<td>
<p><strong>TOEFL iBT</strong></p>
</td>
<td>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
<p><strong>$180</strong></p>
</td>
<td>
<p><strong>Sat.，Jul 08，2017</strong></p>
<p><strong>Sat.，Jul 15，2017</strong></p>
<p><strong>Sat.，Jul 22，2017</strong></p>
<p><strong>Sat.，Jul 29，2017</strong></p>
<p><strong>Sat.，Aug 05，2017</strong></p>
<p><strong>Sat.，Aug 12，2017</strong></p>
<p><strong>Sat.，Aug 26，2017</strong></p>
<p><strong>Sat.，Sep 09，2017</strong></p>
<p><strong>Sun.，Sep 17，2017</strong></p>
<p><strong>Sun.，Sep 24，2017</strong></p>
<p><strong>Sat.，Sep 30，2017</strong></p>
<p><strong>Sat.，Oct 14，2017</strong></p>
<p><strong>Sun.，Oct 15，2017</strong></p>
<p><strong>Sat.，Oct 21，2017</strong></p>
<p><strong>Sat.，Oct 28，2017</strong></p>
<p><strong>Sun.，Oct 29，2017</strong></p>
<p><strong>Sat.，Nov 04，2017</strong></p>
<p><strong>Sun.，Nov 12，2017</strong></p>
<p><strong>Sat.，Nov 18，2017</strong></p>
<p><strong>Sat.，Nov 25，2017</strong></p>
<p><strong>Sat.，Dec 02，2017</strong></p>
<p><strong>Sat.，Dec 09，2017</strong></p>
<p><strong>Sat.，Dec 16，2017</strong></p>
<p><strong>Sun.，Dec 17，2017</strong></p>
</td>
</tr>
</tbody>
</table>
<p><span style=""font-weight: 400;"">Region（區域）：</span></p>
<p><span style=""font-weight: 400;"">North（北部）：台北、桃園、新竹</span></p>
<p><span style=""font-weight: 400;"">Central（中部）：台中</span></p>
<p><span style=""font-weight: 400;"">South（南部）：台南、高雄</span></p>
<p>&nbsp;</p>
<ol start=""3"">
<li>
<h3>按照指引到最後刷卡付款</h3>
</li>
</ol>
<p><span style=""font-weight: 400;"">可刷信用卡，費用為170塊美金 (約5100-5200元台幣)，在報名過程中，ETS提出可以免費為你往4個學校寄成績，記得自己要申請或最有把握上的學校填入。因此，提前查好自己目標學校的代碼比較方便。</span></p>
<p><span style=""font-weight: 400;"">*</span><span style=""font-weight: 400;"">若需要向第五間以上學校寄成績，你可以這樣做：</span></p>
<p><span style=""font-weight: 400;"">網路加發每份成績單20美元，四~七個工作天后由ETS寄出。</span></p>
<p><span style=""font-weight: 400;"">亦可用通信或傳真方式，但須填寫Score Report Request Form，ETS收到申請後兩週寄出。</span></p>
<p><span style=""font-weight: 400;"">傳真： 1-610-290-8972</span></p>
<p><span style=""font-weight: 400;"">通信： ETS-TOEFL iBT PO BOX 6153 Princeton，NJ 08541-6153 USA</span></p>
<p>&nbsp;</p>
<ul>
<li><a href=""https://www.ets.org/toefl/ibt/register/codes""><span style=""font-weight: 400;"">查詢學校代碼</span></a><span style=""font-weight: 400;""> https://www.ets.org/toefl/ibt/register/codes</span></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">4.交易完成後：ETS會告訴你考試時間和地點，然後按照要求email至填入的電子信箱發送一封確認信。</span></p>
<p><br /><br /></p>
<hr />
<h2><strong>重點二:</strong><strong> 報名時間務必提前兩個月!</strong></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;""> &nbsp;&nbsp;&nbsp;&nbsp;理由是有些考生一次即考到理想的成績，但有些同學可能目標成績較高或考得不理想，如果又申請學校在即，建議提前報名可預留再次報考的時間。建議同學每次考試相隔三個禮拜至一個月，如果第一次考試拿到的成績滿意，後面考試即可退掉或轉讓（請注意：交易或轉讓仍有失敗風險，考生需自行承擔）。提早報名最大的好處就是避免理想考場爆滿，而選不到離家近或交通便利的地點。</span></p>
<p><span style=""font-weight: 400;"">前述每次考試隔一個月的理由是因為ETS 公布考試日期要相隔12天以上，故至少兩星期以上；其次，兩次考試之間，為求確實進步，讓同學針對上次考試的弱點改進，三星期剛好可以維持學習記憶，保持動力繼續努力。</span></p>
<p>&nbsp;</p>
<hr />
<h2><strong>重點三: 口說測驗的練習，需習慣四周有人說話</strong></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;""> &nbsp;因目前考場會委託私人的英語補習班或大學當場地，跟其他人間隔多為塑膠隔板，隔音並非百分百完美。小編建議考生平常練習口說可習慣提高音量至左右一兩公尺可聽清楚，或是在人群來往的地方練習口說，考生要提前習慣左右有人講話時，回答口說問題仍老神在在，思維不被旁人拉走的功夫！</span></p>
<p><br /><br /></p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">藉由以上的介紹，相信你對托福網路測驗（TOEFL iBT）如何報名有基本的認識，不妨即刻上</span><a href=""https://www.ets.org/toefl""><span style=""font-weight: 400;"">ETS官網</span></a><span style=""font-weight: 400;"">試著操作、註冊，並規劃報考日期。</span></p>
<p><br /><span style=""font-weight: 400;"">*台灣托福資源中心並不提供TOEFL iBT測驗相關服務，但如需了解測驗簡介與內容，仍可見</span><a href=""http://www.toefl.com.tw/iBT/index.jsp""><span style=""font-weight: 400;"">ETS中文官網的TOEFL iBT</span></a><span style=""font-weight: 400;"">介紹。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '0','og_title' => '托福要怎麼報名？考前多久前報名呢？','og_description' => '托福是申請國外大學的必備證明，報名方式需在官方網站操作，對於不熟悉的人難免因為陌生而卻步。報名也有許多細節要注意，現在就讓我們來看看如何報名！','meta_title' => '托福要怎麼報名？考前多久前報名呢？','meta_description' => '托福是申請國外大學的必備證明，報名方式需在官方網站操作，對於不熟悉的人難免因為陌生而卻步。報名也有許多細節要注意，現在就讓我們來看看如何報名！','canonical_url' => 'https://tw.english.agency/托福報名方法','feature_image_alt' => '托福報名','feature_image_title' => '托福報名方法','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5979bbd906198-JqBOgp4qNNbZtSl53amkglTPGIq5eL-1340x893.jpeg','author_id' => '1','category_id' => '15','created_at' => '"2017-07-27 05:06:51"'],
  ['id' => '54','title' => '"8／13 菲律賓遊學展-全球職涯教育展，免費報名，出席再抽機票大獎！"','slug' => '8-13菲律賓遊學展','excerpt' => '今年八月全球職涯發展協會主辦的「全球職涯暨國際教育展」將於8/12、13盛大開展！內容涵蓋國際證照、英語、國際職涯三大主題，現場註冊課程直接抵用$3000元台幣獎學金！','content' => '"<p>&nbsp;</p>
<p style=""text-align: left;""><strong>活動時間：8 / 13 (日) 台北 10:00 - 17:00 </strong></p>
<p style=""text-align: left;""><strong>活動地點：台大集思會館 - 洛克廳 - 台北市羅斯福路4段85號B1 (近公館2號出口)</strong></p>
<p style=""text-align: left;""><strong>活動費用：</strong><span style=""font-weight: 400;"">免費入場 </span></p>
<p style=""text-align: left;""><strong>報名網頁： </strong><a href=""https://goo.gl/rTSSWp""><strong>https://goo.gl/rTSSWp</strong></a></p>
<p style=""text-align: left;""><strong>活動好康：</strong><span style=""font-weight: 400;"">前100位報名贈送7-11 $100 元禮券，活動現場再抽AirAsia 台北-菲律賓來回機票及海外遊學課程大獎！ </span></p>
<p style=""text-align: left;""><strong>主辦單位：</strong><span style=""font-weight: 400;"">全球職涯發展協會 &nbsp;</span></p>
<p style=""text-align: left;""><strong>協辦單位：</strong><span style=""font-weight: 400;"">Go Education 遊學教育中心/ CanFly 國際證照專家/ HELP English </span></p>
<p style=""text-align: left;""><br /><br /></p>
<hr />
<h2 style=""text-align: left;""><span style=""font-weight: 400;"">活動緣起</span></h2>
<p style=""text-align: left;""><span style=""font-weight: 400;"">根據1111人力銀行調查，有高達63% 的社會新鮮人有意赴海外工作，顯示台灣七八年級生前往海外工作動機最為強烈，而最想投入的產業別其中之一則是「民生、工商服務業」！</span></p>
<p style=""text-align: left;"">&nbsp;</p>
<p style=""text-align: left;""><span style=""font-weight: 400;"">今年八月份，GCDA 中華民國全球職涯發展協會特別以餐旅服務業為核心，主辦「全球職涯暨國際教育展」將於8/12、8/13，分別在台中、台北盛大開展！內容涵蓋：國際證照、英語、國際職涯三大主題，舉辦餐旅高薪在海外、200萬出國唸大學、菲律賓密集英語講座與菲律賓遊學展。 </span></p>
<p style=""text-align: left;"">&nbsp;</p>
<p style=""text-align: left;""><span style=""font-weight: 400;"">其中之一主題：菲律賓遊學展，近三年台灣人前往菲律賓學英文的學生人數逐年上升，而過去日本平均有50000人、韓國80000名學生前往菲律賓進修，</span><strong>都看重在菲律賓全英文環境、密集英語一對一課程安排、相對歐美便宜1/2的價格</strong><span style=""font-weight: 400;"">，因此，特邀台日韓學生最高評價八間語言學校參展，當天直接跟菲律賓語言學校各代表諮詢，了解更清楚！現場註冊課程直接抵用$3000元台幣獎學金，還可以抽神秘大獎！</span></p>
<p style=""text-align: left;"">&nbsp;</p>
<p style=""text-align: left;"">&nbsp;</p>
<hr />
<h2 style=""text-align: left;""><span style=""font-weight: 400;"">菲律賓學英文三大魅力</span></h2>
<ol style=""text-align: left;"">
<li style=""font-weight: 400;""><strong>一對一全英語教學：</strong><span style=""font-weight: 400;"">全英語環境加上語言學校的 EOP(只能說英語)政策實施，英語能力自然快速提升！</span></li>
<li style=""font-weight: 400;""><strong>費用相對低廉：</strong><span style=""font-weight: 400;"">60天英語課程，語言學校食宿全包，卻只需要歐美遊學費用的 1/3-1/2 ！</span></li>
<li style=""font-weight: 400;""><strong>密集學習成效顯著：</strong><span style=""font-weight: 400;"">每天 12 個小時聽說讀寫課程和單字小考，多益只要60天就可以進步 200分</span><a href=""https://goo.gl/EMLe2e""><span style=""font-weight: 400;""> &nbsp;</span></a></li>
</ol>
<p style=""text-align: left;""><img title=""全球職涯發展協會-遊學展"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/598042e435543-Cj0uxTFGyjIqmmVydHBEeJMKLP2YdS-300x424.jpeg"" alt=""8/13遊學教育展"" width=""300"" height=""424"" /><br /><br /></p>
<h2 style=""text-align: left;"">&nbsp;</h2>
<hr />
<h2 style=""text-align: left;"">活動好康</h2>
<ol style=""text-align: left;"">
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">前100名索票贈送 7-11 $100 禮券！</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">完成報到再抽台北- 菲律賓來回機票以及海外遊學課程大獎！</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">當日註冊課程好禮：現場抵用獎學金台幣$3000及好禮二選一，再抽神秘大獎！ &nbsp;</span></li>
</ol>
<p style=""text-align: left;""><br /><br /><span style=""font-weight: 400;"">活動報名網頁：</span><strong><a href=""https://goo.gl/rTSSWp"">https://goo.gl/rTSSWp</a></strong></p>
<p style=""text-align: left;""><strong>活動協辦單位：<a title=""前往AirAsia"" href=""https://www.airasia.com/tw/zh/home.page"">AirAsia</a>、<a title=""前往為電影協會"" href=""http://www.micromovie.org.tw/"">中華民國微電影協會</a>、GoEducation仁育國際教育、Canfly</strong></p>"','published' => '1','og_title' => 'https://admin.english.agency/articles/create','og_description' => '今年八月全球職涯發展協會主辦的「全球職涯暨國際教育展」將於8/12、13盛大開展！內容涵蓋國際證照、英語、國際職涯三大主題，現場註冊課程直接抵用$3000元台幣獎學金！','meta_title' => 'https://admin.english.agency/articles/create','meta_description' => '今年八月全球職涯發展協會主辦的「全球職涯暨國際教育展」將於8/12、13盛大開展！內容涵蓋國際證照、英語、國際職涯三大主題，現場註冊課程直接抵用$3000元台幣獎學金！','canonical_url' => 'https://tw.english.agency/8-13菲律賓遊學展','feature_image_alt' => '全球職涯教育協會','feature_image_title' => '菲律賓遊學展','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/598042f372c57-FhAAl7NYIv9T1HDKuoSWK4VUh0rgmE-1081x541.png','author_id' => '8','category_id' => '1','created_at' => '"2017-08-01 09:27:24"'],
  ['id' => '55','title' => '顧問、工程師英文怎麼說？求職徵才必學職稱英文總整理！','slug' => '職稱-英文-工程師-英文','excerpt' => '想求職卻看不懂徵才廣告上的英文？若想在海外工作，首先要會看懂徵才廣告的內容，才不會傻傻上當！想知道你想要應徵的職位英文怎麼說，現在就來看我們幫你整理的英文職稱大補帖吧！','content' => '"<div style=""font-size: 18px;"">
<p>辦公室裡永遠有說不完的八卦，做不完的瑣事還得看慣老闆臉色！然而談到辦公室各種職稱的英文，常在媒體業、或服務型產業最常聽到的，不外乎就是『工程師』、『顧問』吧？今天就帶大家來看看工程師英文、顧問英文怎麼說！不管你是業務部門、行銷企劃、活動企劃～讓我們學好職場英文為自己找到更讚的工作，快來看看你是哪一個部門吧！</p>
<p>&nbsp;</p>
<h2>&nbsp;財務職稱相關英文</h2>
<p><strong>Accounting </strong><strong>會計</strong></p>
<ul>
<li><span style=""color: #2e8b57;"">Accountant</span> 會計人員</li>
<li><span style=""color: #2e8b57;"">Bookkeeper</span> 記帳人員</li>
<li><span style=""color: #2e8b57;"">Budget analyst </span>預算分析師</li>
<li><span style=""color: #2e8b57;"">Financial analyst </span>財務分析師</li>
</ul>
<p>&nbsp;</p>
<p><strong>Finance </strong><strong>財務</strong></p>
<ul>
<li><span style=""color: #2e8b57;"">CFO (Chief Financial Officer) </span>財務長</li>
<li><span style=""color: #2e8b57;"">FC (Financial controller)</span> 財務主任</li>
<li><span style=""color: #2e8b57;"">Financial Manager</span> 財務經理</li>
<li><span style=""color: #2e8b57;"">Financial clerk</span> 財務管理員</li>
<li><span style=""color: #2e8b57;"">Financial planner</span> 理財規劃顧問</li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3><strong>Administrative </strong><strong>行政職稱相關英文</strong></h3>
<ul>
<li><span style=""color: #2e8b57;"">CEO (Chief Executive Officer)</span>執行長</li>
<li><span style=""color: #2e8b57;"">Secretary</span> 秘書</li>
<li><span style=""color: #2e8b57;"">Administrator</span> 行政人員</li>
<li><span style=""color: #2e8b57;"">Consultant</span> 顧問</li>
<li><span style=""color: #2e8b57;"">Typist </span>打字員</li>
<li><span style=""color: #2e8b57;"">File clerk</span> 檔案管理員</li>
<li><span style=""color: #2e8b57;"">Office manager</span> 辦公室主任</li>
</ul>
<p>&nbsp;</p>
<h3><strong>IT 職稱相關英文</strong></h3>
<ul>
<li><span style=""color: #2e8b57;"">Project manager</span>專案經理</li>
<li><span style=""color: #2e8b57;"">Product manager</span> 產品經理</li>
<li><span style=""color: #2e8b57;"">iOS developer </span>iOS工程師</li>
<li><span style=""color: #2e8b57;"">Java developer</span> Java工程師</li>
<li><span style=""color: #2e8b57;"">.Net developer</span> .NET工程師</li>
<li><span style=""color: #2e8b57;"">Front-end developer</span> 前端工程師</li>
<li><span style=""color: #2e8b57;"">Back-end developer</span> 後端工程師</li>
<li><span style=""color: #2e8b57;"">Data analyst </span>資料分析師</li>
<li><span style=""color: #2e8b57;"">Data architect </span>資料架構師</li>
</ul>
<p>&nbsp;</p>
<h3><strong>Design 設計相關職稱英文</strong></h3>
<ul>
<li><span style=""color: #2e8b57;"">Graphic designer </span>平面設計師</li>
<li><span style=""color: #2e8b57;"">Web designer </span>網頁設計師</li>
<li><span style=""color: #2e8b57;"">Illustrator </span>插畫家</li>
<li><span style=""color: #2e8b57;"">Visual designer</span> 視覺傳達設計師</li>
<li><span style=""color: #2e8b57;"">Interior designer </span>室內設計師</li>
</ul>
<p>&nbsp;</p>
<h3><strong>Sales&nbsp;銷售業務人員、業務專員英文</strong></h3>
<ul>
<li><span style=""color: #2e8b57;"">Sales manager</span> 銷售部經理</li>
<li><span style=""color: #2e8b57;"">Sales executive</span> 行銷專員</li>
<li><span style=""color: #2e8b57;"">Sales assistant</span> 行銷助理</li>
<li><span style=""color: #2e8b57;"">Sales engineer</span> 銷售工程師</li>
<li><span style=""color: #2e8b57;"">Sales director</span> 行銷經理</li>
<li><span style=""color: #2e8b57;"">Telemarketer</span> 電話行銷員</li>
<li><span style=""color: #2e8b57;"">Key account manager</span> 大客戶業務經理</li>
<li><span style=""color: #2e8b57;"">Account manager </span>客戶經理</li>
</ul>
<p>&nbsp;</p>
<p><strong>Customer service </strong><strong>客服</strong></p>
<ul>
<li><span style=""color: #2e8b57;"">Receptionist </span>接待員</li>
<li><span style=""color: #2e8b57;"">Operator</span>接線員</li>
<li><span style=""color: #2e8b57;"">Dispatcher </span>接線員</li>
</ul>
<p>&nbsp;</p>
<h3>建築師相關職稱英文</h3>
<p><strong>Construction </strong><strong>建築</strong></p>
<ul>
<li><span style=""color: #2e8b57;"">Architect</span> 建築師</li>
<li><span style=""color: #2e8b57;"">Construction manager </span>營建經理</li>
<li><span style=""color: #2e8b57;"">Construction foreman </span>營建領班</li>
<li><span style=""color: #2e8b57;"">Welder </span>焊工</li>
<li><span style=""color: #2e8b57;"">Plumber</span> 水管工人</li>
<li><span style=""color: #2e8b57;"">Electrician</span> 電工</li>
<li><span style=""color: #2e8b57;"">Carpenter</span> 木匠</li>
<li><span style=""color: #2e8b57;"">Painter </span>油漆工</li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3><strong>Hospitality</strong>餐旅管理相關職稱英文</h3>
<ul>
<li><span style=""color: #2e8b57;"">Barista</span> 咖啡店店員</li>
<li><span style=""color: #2e8b57;"">Executive chef</span> 行政主廚</li>
<li><span style=""color: #2e8b57;"">Chef </span>主廚</li>
<li><span style=""color: #2e8b57;"">Cook</span> 廚師</li>
<li><span style=""color: #2e8b57;"">Line cook/Chef de partie </span>二廚</li>
<li><span style=""color: #2e8b57;"">Pastry cook</span> 點心師傅</li>
<li><span style=""color: #2e8b57;"">Room attendant</span> 房務員</li>
<li><span style=""color: #2e8b57;"">Porter</span>行李員</li>
<li><span style=""color: #2e8b57;"">Housekeeper</span> 管家</li>
<li><span style=""color: #2e8b57;"">Wait staff</span> (全體)服務生</li>
<li><span style=""color: #2e8b57;"">Waiter/Waitress</span> 男服務生/女服務生</li>
<li><span style=""color: #2e8b57;"">Bartender </span>酒保</li>
<li><span style=""color: #2e8b57;"">Host </span>接待員</li>
<li><span style=""color: #2e8b57;"">Dishwasher </span>洗碗員</li>
</ul>
<p>&nbsp;</p>
<h3><strong>Human Resources (HR) </strong><strong>人資相關職稱英文</strong></h3>
<ul>
<li><span style=""color: #2e8b57;"">HR assistant </span>人資助理</li>
<li><span style=""color: #2e8b57;"">HR specialist </span>人資專員</li>
<li><span style=""color: #2e8b57;"">Staffing Coordinator</span> 人事協調員</li>
</ul>
<p>&nbsp;</p>
<h3><strong>Marketing</strong><strong>行銷企劃人員相關職稱英文</strong></h3>
<ul>
<li><span style=""color: #2e8b57;"">Marketing assistant</span> 行銷助理</li>
<li><span style=""color: #2e8b57;"">Marketing executive </span>行銷專員</li>
<li><span style=""color: #2e8b57;"">Art director </span>藝術指導</li>
<li><span style=""color: #2e8b57;"">Creative director</span> 創意總監</li>
<li><span style=""color: #2e8b57;"">SEO manager</span> SEO經理</li>
<li><span style=""color: #2e8b57;"">SEM specialist</span> SEM專員</li>
</ul>
<p>&nbsp;</p>
<p><span style=""color: #2e8b57;"">SEO</span> 是指「搜尋引擎最佳化(<span style=""color: #2e8b57;"">Search Engine Optimization</span> )」</p>
<p><span style=""color: #2e8b57;"">SEM</span> 「即為搜尋引擎行銷(<span style=""color: #2e8b57;"">Search Engine Marketing</span> )」</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3><strong>Tourism </strong><strong>旅遊職稱相關英文</strong></h3>
<ul>
<li><span style=""color: #2e8b57;"">Travel agent</span> 旅遊專員</li>
<li><span style=""color: #2e8b57;"">Tour guide</span> 導遊</li>
<li><span style=""color: #2e8b57;"">Cabin crew </span> (全體)機艙人員、機組人員</li>
<li><span style=""color: #2e8b57;"">Flight attendant</span> 空服員</li>
<li><span style=""color: #2e8b57;"">Ground staff</span> 地勤人員</li>
<li><span style=""color: #2e8b57;"">Flight dispatcher</span> (航空)簽派員</li>
<li><span style=""color: #2e8b57;"">Pilot</span> 機師</li>
<li><span style=""color: #2e8b57;"">Steward</span> 服務員(飛機、船艙、火車等等)</li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>若想在海外工作，首先要會看懂徵才廣告的內容，才不會傻傻上當！</p>
<p>&nbsp;</p>
<p>1. <span style=""color: #2e8b57;"">Job Ads / Job Advertisement </span>徵才廣告</p>
<p>2. <span style=""color: #2e8b57;"">Applicant</span> 求職者 ◎<span style=""color: #2e8b57;"">Applicant</span>還可用在申請入學</p>
<p>3.<span style=""color: #2e8b57;"">High-end rest. looking for evening dishwasher. Eligible applicants only</span></p>
<p>&nbsp; &nbsp;(高檔餐廳徵求晚班洗碗員，限符合資格者。)</p>
<p>4.<span style=""color: #2e8b57;"">FT/PT server wanted for busy pub in downtown. T.O.</span></p>
<p>&nbsp; &nbsp;(多倫多市中心酒吧徵求全職/兼職服務生)</p>
<ul>
<li><span style=""color: #2e8b57;"">FT </span>是指<span style=""color: #2e8b57;"">Full time</span>，所以<span style=""color: #2e8b57;"">PT</span>就是<span style=""color: #2e8b57;"">Part time</span></li>
</ul>
<p>5.<span style=""color: #2e8b57;"">Must have serving exp and full avail. Min wage+tips</span></p>
<p>&nbsp; &nbsp;(必須擁有服務經驗，可配合時間。薪資：基本薪資+小費)</p>
<p>&nbsp;</p>
<p>最後如果對於商用英文還是有些不理解，可以參考友站內容：<a href=""http://davytw.pixnet.net/blog/post/58007985-%E4%B8%80%E8%88%AC%E5%85%AC%E5%8F%B8%E5%93%A1%E5%B7%A5%E6%88%96%E8%81%B7%E5%93%A1%E8%8B%B1%E6%96%87%E8%81%B7%E7%A8%B1%E5%88%97%E8%A1%A8"">辦公室職稱英文！</a></p>
</div>"','published' => '1','og_title' => '顧問、工程師英文怎麼說？求職徵才必學職稱英文總整理！','og_description' => '想求職卻看不懂徵才廣告上的英文？若想在海外工作，首先要會看懂徵才廣告的內容，才不會傻傻上當！想知道你想要應徵的職位英文怎麼說，現在就來看我們幫你整理的英文職稱大補帖吧！','meta_title' => '顧問、工程師英文怎麼說？求職徵才必學職稱英文總整理！','meta_description' => '想求職卻看不懂徵才廣告上的英文？若想在海外工作，首先要會看懂徵才廣告的內容，才不會傻傻上當！想知道你想要應徵的職位英文怎麼說，現在就來看我們幫你整理的英文職稱大補帖吧！','canonical_url' => 'https://tw.english.agency/職稱-英文-工程師-英文','feature_image_alt' => '求職徵才職稱英文','feature_image_title' => '求職徵才職稱英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/599aaec5660a7-vYCBhPeFZvgPyagUTgP6Higa0NyVP0-5035x2714.png','author_id' => '1','category_id' => '10','created_at' => '"2017-08-21 10:01:03"'],
  ['id' => '56','title' => '"學商用書信為自己加薪！ 寫作規則與單字大補帖   "','slug' => '商用書信','excerpt' => '"你有沒有發現隨著全球化的趨勢，英文能力也日趨變得重要呢？無論是外商公司、科技、光電、製衣甚至媒體產業，都需要基本的商用書信(Business letter)知識。所以不用再特地跑遍各大書局買入商用書籍了，你需要的資訊都在這裡！"','content' => '"<div style=""font-size: 18px;"">
<p>你有沒有發現隨著全球化的趨勢，英文能力也日趨變得重要呢？無論是外商公司、科技、光電、製衣甚至媒體產業，都需要基本的商用書信(Business letter)知識。所以不用再特地跑遍各大書局買入商用書籍了，你需要的資訊都在這裡！</p>
<p>&nbsp;</p>
<p><img title=""商用書信怎麼寫"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59ffe88363428-dmqCoRkgkUUoWCOgcwgp60LBZ2ova3-300x169.png"" alt=""商用書信怎麼寫"" width=""502"" height=""283"" /></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #0044bb;"">書信3C原則：</span></strong></p>
<p><strong>&nbsp;</strong></p>
<ul style=""list-style-type: disc;"">
<li>Clarity(清楚)：信件主旨易懂，但千萬要說到重點。信件內文以倒三角形(Inverted pyramid)形式寫作，開頭說重點</li>
<li>Courtesy(禮貌)：內文口吻避免命令口氣，要讓對方受尊重</li>
<li>Conciseness(簡明)：內容切勿冗長，要讓對方可以短時間讀完</li>
</ul>
<p>&nbsp;</p>
<p><strong><span style=""color: #0044bb;"">標題主旨與寄信須知：</span></strong></p>
<p>&nbsp;</p>
<p>To是收件人，From就是自己的名字。而Cc(Carbon copy)就是副本抄送(當你寄給收信人時，最好也Cc給你公司的同仁，以防資訊漏接)，Bcc(Blind Carbon Copy)就是密件副本(當你不想讓收信人知道這封信也有寄給別人，就可以用這個)，最後的Forward就是轉寄，若你怕Bcc的收件人直接回信沒有Bcc，就可以用這招。</p>
<p>&nbsp;</p>
<p><strong><span style=""color: #0044bb;"">書信寫作規則：</span></strong></p>
<p><strong>&nbsp;</strong></p>
<ol>
<li>字體大小為12，不要特地修改邊界，內容以一頁為主(One page)。</li>
<li>信頭(Letterhead)，也就是書信最上面的標題，為公司的名字。</li>
<li>自己的聯絡方式，無論你要放在信頭之後，或是在內文的結尾之後，盡量控制在六行之內。自己的聯絡方式須具備：署名、手寫簽名、職稱、公司、公司地址(通常分兩行寫，地址由小到大)、電話及傳真、電郵。</li>
<li>寫信日期(Date)的位子，會在收件人的上方一、兩行處。美式日期為：月份日，年(June 1，2011)，英式寫法為：日月份，年(1 June，2011)</li>
</ol>
<p>&nbsp;</p>
<p>&nbsp; &nbsp;※日期一定要記得寫，第一是自保──以防對方說沒收到，這時就可以看寄件備份區查到當天寄的信。第二是紀錄──若你一次與許多公司書信往來，很容易搞混信件。</p>
<ol start=""5"">
<li>對方的聯絡方式，則會在內文的上方處，內容須具備：對方姓名、職稱、公司、公司地址。 內文：若不知道對方姓名時，一開始便可以寫<strong><span style=""color: #008080;"">Dear sir</span></strong>或<strong><span style=""color: #008080;"">Dear madam</span></strong>，若知道對方的職稱，就可以寫<strong><span style=""color: #008080;"">Dear+</span></strong><strong><span style=""color: #008080;"">工作職稱</span></strong></li>
</ol>
<ol start=""6"">
<li>內文以三段式為主，每個段落上下只需要空一行。</li>
</ol>
<p>&nbsp;</p>
<ol start=""7"">
<li>結尾敬語，有<span style=""color: #008080;"">Sincerely yours/Yours sincerely</span>(商用正式)、<span style=""color: #008080;"">Sincerely</span>(稍正式)、<span style=""color: #008080;"">Best/Best regards</span>(較不正式、較普遍)、<span style=""color: #008080;"">Yours faithfully</span>(一開始不知道對方名字)、<span style=""color: #008080;"">Yours truly</span>(私人往來，美式用法)</li>
</ol>
<p>&nbsp;</p>
<p>&nbsp;</p>
常用語句：記下這些常用句子，讓你寫信更有效率！
<p>&nbsp;</p>
<span style=""color: #0044bb;"">※開頭</span>
<p>&nbsp;</p>
<p><u><span style=""color: #008080;"">I&rsquo;m writing to tell you</span></u> the goods you asked before are available now.</p>
<p>我們來信向您通知，您之前詢問的商品有現貨了。</p>
<p>&nbsp;</p>
<p><u><span style=""color: #008080;"">In my previous E-mail</span></u> of August 10，I have ordered the goods that should arrive 5 days ago.</p>
<p>我在八月10日來信，並向您訂購商品，但我們過了五天尚未收到貨。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #0044bb;"">※中間</span></p>
<p><u><span style=""color: #008080;"">Would you be kind enough to</span></u> send us the following information?</p>
<p>能否勞煩您寄送以下資訊給我們？</p>
<p><strong>&nbsp;</strong></p>
<p><u><span style=""color: #008080;"">Would you please</span></u> send back the damaged goods to us? We will replace them with the new ones.</p>
<p>能勞煩您將破損商品寄還我們嗎？我們將為您更換新的商品。</p>
<p>&nbsp;</p>
<p><span style=""color: #0044bb;"">※結尾</span></p>
<p>&nbsp;</p>
<p>We look forward to hearing from you. 期待您的回音</p>
<p>Any information you give us will be highly appreciated. 您提供的任何資訊，我們將不勝感激</p>
<p>Please feel free to contact us if you have any questions. 有任何問題請不吝來信通知</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #0044bb;""><strong>商用字彙：</strong></span></p>
<div>
<p>&nbsp;</p>
<p><span style=""color: #008866;"">Catalo</span>目錄</p>
<p><span style=""color: #008866;"">Quotation</span> 報價單</p>
<p><span style=""color: #008866;"">Order</span> 訂單(n.)、訂購(v.)&nbsp; Place an order 下訂單</p>
<p><span style=""color: #008866;"">Goods</span> 貨物</p>
<p><span style=""color: #008866;"">Price list</span> 價目表</p>
<p><span style=""color: #008866;"">Specification</span> 規格</p>
<p><span style=""color: #008866;"">Discount</span> 折扣</p>
<p><span style=""color: #008866;"">Insurance</span> 保險</p>
<p><span style=""color: #008866;"">Damaged</span> 破損的</p>
<p><span style=""color: #008866;"">Short-shipped</span> 短裝貨</p>
<p><span style=""color: #008866;"">Attach</span> 隨信附上</p>
<p><span style=""color: #008866;"">Enclose</span> 隨信附上</p>
<p><span style=""color: #008866;"">Methods of payment</span> 付款方式</p>
<p><span style=""color: #008866;"">Minimum order</span> 最低訂購量</p>
<p><span style=""color: #008866;"">Delivery time</span> 交貨時間</p>
<p><span style=""color: #008866;"">Carriage cost</span> 運費</p>
<p><span style=""color: #008866;"">c.o.d.</span>貨到付款(Cash on delivery)</p>
<p><span style=""color: #008866;"">f.o.c</span>免費(Free of charge)</p>
</div>
<p>&nbsp;</p>
<p><img /></p>
<p><strong>範例：</strong></p>
<p>&nbsp;<img title=""商用書信怎麼寫"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59ffe8f920650-hlL6TrHz14sdlLDoEfLXAuYOAJ7Jlq-1102x1312.png"" alt=""商用書信怎麼寫"" width=""667"" height=""794"" /></p>
<p>圖片來源：<a href=""https://www.udemy.com/effective-business-writing/"">Udemy</a></p>
</div>"','published' => '1','og_title' => '"學會商用書信為自己加薪！   "','og_description' => '"英文能力所需要用到的基本商用書信(Business letter)知識，從今以後不用再特地跑遍各大書局買入商用書籍了，你需要的資訊都在這裡！"','meta_title' => '"學會商用書信為自己加薪！   "','meta_description' => '"英文能力所需要用到的基本商用書信(Business letter)知識，從今以後不用再特地跑遍各大書局買入商用書籍了，你需要的資訊都在這裡！"','canonical_url' => 'https://tw.english.agency/商用書信','feature_image_alt' => '"學商用書信為自己加薪！ 寫作規則與單字大補帖   "','feature_image_title' => '"學商用書信為自己加薪！ 寫作規則與單字大補帖   "','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59ffe7de164e7-LwhtRyRlBhleo1MjHUZZF1F6huCBUz-1200x630.png','author_id' => '1','category_id' => '19','created_at' => '"2017-09-04 08:57:48"'],
  ['id' => '57','title' => '『颱風來了！』英文怎麼說？你必須要知道的氣象英文！ ','slug' => '氣象英文','excerpt' => '人在國外聽不懂氣象預報？明天會不會下雨？颱風的最新狀況如何？應該要穿厚外套、還是可以出去曬太陽？現在就來學學這些實用氣象英文，在國外也聽得懂天氣預報！可以做好準備再出門～','content' => '"<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">人在國外聽不懂氣象預報？明天會不會下雨？颱風的最新狀況如何？應該要穿厚外套、還是可以出去曬太陽？現在就來學學這些實用氣象英文，在國外也聽得懂天氣預報！可以做好準備再出門～
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59ae45519aa37-zTys3qMnOSCgDfxbEae1b8GrquvvXr-300x216.jpeg"" alt=""氣象氣候天氣英文"" width=""660"" height=""475"" /></p>
<p>&nbsp;</p>
<hr />
<h2 style=""font-size: 26px; text-align: center;""><span style=""color: #8b4513;""><strong>Weather</strong><strong>天氣</strong></span></h2>
<ul>
<li><span style=""color: #ff8c00;""><strong>Weather forecast</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>天氣預報</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Bright/Clear </strong></span><strong>➩</strong><span style=""color: #008080;""><strong>晴朗的</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Sunny </strong></span><strong>➩</strong><span style=""color: #008080;""><strong>有太陽的</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Sun-drenched </strong></span><strong>➩</strong><span style=""color: #008080;""><strong>陽光充足的</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Sun-kissed </strong></span><strong>➩</strong><span style=""color: #008080;""><strong>陽光普照的</strong></span>
<ul>
<li>在形容天氣很熱時，除了可以說It&rsquo;s so hot!，還有其他比較生動的講法：</li>
<li><span style=""color: #2e8b57;""><strong>Today is scorching hot! </strong><strong>今天真是炙</strong><strong>熱！</strong></span></li>
<li><span style=""color: #2e8b57;""><strong>I&rsquo;m nearly melting! </strong><strong>我快</strong><strong>熱</strong><strong>到溶化了！</strong></span></li>
</ul>
</li>
</ul>
<ul>
<li><span style=""color: #ff8c00;""><strong>Cloudy</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>多雲的</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Rainy</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>下雨的</strong></span>
<ul>
<li>It&rsquo;s raining <span style=""color: #2e8b57;""><strong>cats and dogs</strong></span>. 正下著貓和狗，意思是：<span style=""color: #2e8b57;""><strong>下著傾盆大雨</strong></span></li>
</ul>
</li>
<li><span style=""color: #ff8c00;""><strong>Pouring</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>傾盆大雨</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Thunderstorm</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>雷雨</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Snowy</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>下雪的</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Windy</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>有風的</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Typhoon</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>颱風</strong><strong>(</strong><strong>發生在西北太平洋</strong><strong>)</strong></span>
<ul>
<li><span style=""color: #2e8b57;""><strong>Sea warning</strong></span> 海上颱風警報</li>
<li><span style=""color: #2e8b57;""><strong>Land warning</strong></span>陸上颱風警報</li>
<li>The typhoon Hato is approaching Taiwan. The Central Weather Bureau will&nbsp;<span style=""color: #2e8b57;""><strong>issue a sea warning</strong></span> for the typhoon tomorrow.</li>
<li>天鴿颱風正逼近台灣，中央氣象局明天將發布海上颱風警報</li>
</ul>
</li>
</ul>
<ul>
<li><span style=""color: #ff8c00;""><strong>Typhoon eye</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>颱風眼</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Hurricane</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>颶風</strong><strong>(</strong><strong>發生在大西洋、東北太平洋</strong><strong>)</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Cyclone </strong></span><strong>➩</strong><span style=""color: #008080;""><strong>氣旋</strong><strong>(</strong><strong>發生在印度洋</strong><strong>)</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Tornado</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>龍捲風</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Monsoon</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>季風</strong><strong>/</strong><strong>雨季</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Frost</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>霜</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Foggy</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>有霧的</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Partly cloudy</strong></span><span style=""color: #008080;""><strong>➩</strong><strong>多雲</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Partly clear</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>多雲時晴</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Mostly clear</strong></span><span style=""color: #008080;""><strong>➩</strong><strong>晴時多雲</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Mostly cloudy</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>多雲時陰</strong></span></li>
</ul>
<p><strong>&nbsp;</strong></p>
<hr />
<h2 style=""font-size: 26px; text-align: center;""><span style=""color: #8b4513;""><strong>Meteorology </strong><strong>氣象</strong></span></h2>
<p>&nbsp;</p>
<ul>
<li><span style=""color: #ff8c00;""><strong>Temperature</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>溫度</strong></span></li>
<ul>
<li>Degree Celsius攝氏溫度</li>
<li>Degree Fahrenheit華氏溫度</li>
</ul>
</ul>
<ul>
<li><span style=""color: #ff8c00;""><strong>Maximum temperature</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>最高溫度</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Minimum temperature</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>最低溫度</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Precipitation</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>降雨</strong></span><strong>(</strong><strong>量</strong><strong>)</strong></li>
<li><span style=""color: #ff8c00;""><strong>Probability of precipitation</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>降雨概率</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Accumulated precipitation</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>累積降雨量</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Humidity</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>濕度</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Visibility </strong></span><strong>➩</strong><span style=""color: #008080;""><strong>能見度</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Ultraviolet index </strong></span><span style=""color: #008080;""><strong>➩</strong><strong>紫外線指數</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Sunrise</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>日出</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Sunset</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>日落</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Tide</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>浪潮</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Acid rain</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>酸雨</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Front</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>鋒面</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Cold front</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>冷鋒</strong></span></li>
<ul>
<li>冷的說法：</li>
<ol>
<li>It&rsquo;s a bit chilly today. 今天有點冷颼颼的</li>
<li>I&rsquo;m frozen. 我要凍僵了</li>
<li>The wind really chills me to the bone. 寒風刺骨</li>
<li>It&rsquo;s freaking cold today! 外面冷死啦！</li>
<li>It&rsquo;s freezing! 好冷！</li>
</ol>
</ul>
</ul>
<ul>
<li><span style=""color: #ff8c00;""><strong>Warm front</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>暖鋒</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Stationary front</strong></span><span style=""color: #008080;""><strong>➩</strong><strong>滯留鋒</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Earthquake</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>地震</strong></span></li>
<ul>
<li>也可以簡稱<span style=""color: #2e8b57;""><strong>quake</strong></span></li>
</ul>
</ul>
<p>When a quake hits，people should use surrounding items to cover as much as possible.</p>
<p>當地震發生時，人們應該盡可能地使用周圍物品來掩護自己。</p>
<ul>
<li><span style=""color: #ff8c00;""><strong>Aftershock</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>餘震</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Tsunami</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>海嘯</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Drought</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>乾旱</strong></span></li>
<li><span style=""color: #ff8c00;""><strong>Climate</strong></span><strong>➩</strong><span style=""color: #008080;""><strong>氣候</strong></span></li>
<ul>
<li>Climate change 氣候變遷</li>
<li>Extreme climate 極端氣候</li>
<li>Global warming 全球暖化</li>
<li>Green house effect 溫室效應</li>
</ul>
</ul>
<hr />
<p>&nbsp;</p>
<p><img title="""" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59ae46da21c98-YhWmVnhOkOWA8ndF5fOsFWL44ZEtFC-300x214.jpeg"" alt=""詢問天氣怎麼說"" width=""652"" height=""466"" /></p>
<p>在詢問天氣時，我們常常用的句型是：</p>
<ol>
<li><span style=""color: #7b68ee;""><strong>What&rsquo;s the weather like in __(</strong><strong>地方</strong><strong>)__ in __(</strong><strong>時候</strong><strong>)__?</strong></span></li>
</ol>
<p>What&rsquo;s the weather like in Taipei in August? 台北八月的天氣是怎麼樣?</p>
<ol start=""2"">
<li><span style=""color: #7b68ee;""><strong>How&rsquo;s the weather today? </strong><strong>今天天氣怎麼樣</strong></span></li>
</ol>
<p>在回答時，常用句型是：</p>
<ol>
<li><span style=""color: #7b68ee;""><strong>It&rsquo;s __(</strong><strong>形容詞</strong><strong>)__.</strong></span></li>
</ol>
<p>It&rsquo;s rainy/ It&rsquo;s very hot. 在下雨/非常熱</p>
<ol start=""2"">
<li><span style=""color: #7b68ee;""><strong>The weather is __(</strong><strong>形容詞</strong><strong>)___.</strong></span></li>
</ol>
<p>The weather is clear in Kenting today. 墾丁今天天氣晴</p>
<ol start=""3"">
<li><span style=""color: #7b68ee;""><strong>It ___(</strong><strong>動詞</strong><strong>)____.</strong></span></li>
</ol>
<p>It rains heavily outside. 外面下大雨了</p>
<ol start=""4"">
<li><span style=""color: #7b68ee;""><strong>It&rsquo;s ____(</strong><strong>動詞</strong><strong>ing)____.</strong></span></li>
</ol>
<p>It&rsquo;s snowing outside. 外面正在下雪</p>
<ol start=""5"">
<li><span style=""color: #7b68ee;""><strong>There is ___(</strong><strong>名詞</strong><strong>)___.</strong></span></li>
</ol>
<p>There is a lot of rain in Taiwan in summer. 台灣的夏天有很多雨</p>
<ol start=""6"">
<li><span style=""color: #7b68ee;""><strong>__(</strong><strong>主詞</strong><strong>)__ have __(</strong><strong>名詞</strong><strong>)__.</strong></span></li>
</ol>
<p>Do you have any snow there? 你們那裡有下雪嗎?</p>
</div>
<p>&nbsp;</p>"','published' => '1','og_title' => '"『颱風來了！』你必須要知道的氣象英文   "','og_description' => '人在國外聽不懂氣象預報？明天會不會下雨？颱風的最新狀況如何？應該要穿厚外套、還是可以出去曬太陽？現在就來學學這些實用氣象英文，在國外也聽得懂天氣預報！可以做好準備再出門～','meta_title' => '"『颱風來了！』你必須要知道的氣象英文   "','meta_description' => '人在國外聽不懂氣象預報？明天會不會下雨？颱風的最新狀況如何？應該要穿厚外套、還是可以出去曬太陽？現在就來學學這些實用氣象英文，在國外也聽得懂天氣預報！可以做好準備再出門～','canonical_url' => 'https://tw.english.agency/氣象英文','feature_image_alt' => '氣候氣象天氣颱風英文怎麼說','feature_image_title' => '氣候氣象天氣颱風英文怎麼說','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59ae5ef707c5f-WTizgss8lujPcIyBenBYPe84C3iHxA-1200x630.png','author_id' => '1','category_id' => '10','created_at' => '"2017-09-05 06:53:09"'],
  ['id' => '58','title' => '看懂英文料理食譜好簡單！烹飪英文單字整理！','slug' => '烹飪英文單字整理','excerpt' => '想做出道地異國料理，卻看不懂英文食譜嗎？在國外餐廳打工，害怕聽不懂廚師間的專業烹飪術語嗎？現在就來學學這些常見又實用的烹飪英文吧！','content' => '"<div style=""font-size: 18px;"">
<p>&nbsp;</p>
<p>想做出道地異國料理，卻看不懂英文食譜嗎？在國外餐廳打工，害怕聽不懂廚師間的專業烹飪術語嗎？現在就來學學這些常見又實用的烹飪英文吧！</p>
<p>&nbsp;</p>
<hr />
<h2 style=""font-size: 26px; text-align: center;""><span style=""color: #008080;"">烹調方式 Cookery</span></h2>
<ul>
<li><span style=""color: #007799;""><strong>烹飪</strong></span><strong>➩<span style=""color: #20b2aa;"">Cook</span></strong></li>
</ul>
<ul>
<ul>
<li>【易混淆字】</li>
<li>通常英文中，er結尾的字都是&hellip;人，例如Teacher老師、Farmer農夫、Driver司機、Dancer舞者。但是，<strong><span style=""color: #800000;"">Cooker</span></strong>的意思是<strong><span style=""color: #800000;"">爐具</span></strong>！<strong><span style=""color: #800000;"">廚師</span></strong>是<strong><span style=""color: #800000;"">Cook</span></strong> or <strong><span style=""color: #800000;"">Chef</span> </strong>(主廚)像是上圖的帥哥主廚Curtis Stone就是Chef的代表啦</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>水煮</strong></span><strong>➩<span style=""color: #20b2aa;"">Boil</span></strong></li>
</ul>
<ul>
<ul>
<li><strong><span style=""color: #800000;"">Poach</span></strong>也有<strong><span style=""color: #800000;"">水煮</span></strong>的意思，<strong><span style=""color: #800000;"">水煮蛋</span></strong>的英文是<strong><span style=""color: #800000;"">Poach eggs</span></strong></li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>炒</strong></span><strong>➩<span style=""color: #20b2aa;"">Fry</span></strong></li>
</ul>
<ul>
<ul>
<li><strong><span style=""color: #800000;"">炒飯</span></strong>的英文是<strong><span style=""color: #800000;"">Fried rice</span></strong>，依此類推：炒麵 Fried noodle</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>煎炒</strong></span><strong>➩<span style=""color: #20b2aa;"">Pan-fry</span></strong></li>
<li><span style=""color: #007799;""><strong>快炒</strong></span><strong>➩<span style=""color: #20b2aa;"">Stir-fry</span></strong></li>
<li><span style=""color: #007799;""><strong>油炸</strong></span><strong>➩<span style=""color: #20b2aa;"">Deep-fry</span></strong></li>
</ul>
<ul>
<ul>
<li>薯條 French fries，炸雞 Fried chicken，薯餅則叫Hash brown喔</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>燒烤</strong></span><strong>➩<span style=""color: #20b2aa;"">Barbecue</span></strong></li>
<li><span style=""color: #007799;""><strong>火烤</strong></span><strong>➩<span style=""color: #20b2aa;"">Grill</span></strong></li>
<li><span style=""color: #007799;""><strong>煎烤</strong></span><strong>➩<span style=""color: #20b2aa;"">Roast</span></strong></li>
<li><span style=""color: #007799;""><strong>烘焙</strong></span><strong>➩<span style=""color: #20b2aa;"">Bake</span></strong></li>
</ul>
<ul>
<ul>
<li>簡單來說， Grill，Roast，Bake 就是「烤」的方式不同啦！</li>
</ul>
</ul>
<ol>
<ol>
<ol>
<li><strong><span style=""color: #800000;"">Grill</span></strong>是將食物放在鐵網上用大火烤</li>
<li><strong><span style=""color: #800000;"">Roast</span></strong>是將食物放進烤箱或是炭火上烤</li>
<li><strong><span style=""color: #800000;"">Bake</span></strong>則是用烤箱烘焙蛋糕、麵包</li>
</ol>
</ol>
</ol>
<ul>
<li><span style=""color: #007799;""><strong>煙燻</strong></span><strong>➩<span style=""color: #20b2aa;"">Smoke</span></strong></li>
</ul>
<ul>
<ul>
<li><strong><span style=""color: #800000;"">煙燻鮭魚</span></strong>的英文就是<strong><span style=""color: #800000;"">Smoked salmon</span></strong></li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>燉</strong></span><strong>➩<span style=""color: #20b2aa;"">Stew</span></strong></li>
</ul>
<ul>
<ul>
<li><strong><span style=""color: #800000;"">燉飯</span></strong>的英文是<strong><span style=""color: #800000;"">Risotto</span></strong>，例如海鮮燉飯:Seafood risotto，而<strong><span style=""color: #800000;"">Stew with rice</span></strong>是將燉好的肉與醬汁淋在飯上，比較像是中文所說的<strong><span style=""color: #800000;"">燴飯</span></strong>，正統的肉跟飯都燉到爛的燉飯還是要用Risotto比較好喔</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>蒸</strong></span><strong>➩<span style=""color: #20b2aa;"">Steam</span></strong></li>
<li><span style=""color: #007799;""><strong>微波</strong></span><strong>➩<span style=""color: #20b2aa;"">Nuke</span></strong></li>
</ul>
<ul>
<ul>
<li><strong><span style=""color: #800000;"">微波爐</span></strong>的英文是<strong><span style=""color: #800000;"">Microwave</span></strong></li>
</ul>
</ul>
<p><strong>&nbsp;</strong></p>
<hr />
<h2 style=""font-size: 26px; text-align: center;""><span style=""color: #008080;""><strong>食材處理</strong><strong> Process Ingredients</strong></span></h2>
<ul>
<li><span style=""color: #007799;""><strong>冷凍</strong></span><strong>➩<span style=""color: #20b2aa;"">Freeze</span></strong></li>
</ul>
<ul>
<ul>
<li><span style=""color: #800000;"">冷凍庫</span>的英文是<span style=""color: #800000;"">Freezer</span>，<span style=""color: #800000;"">冷藏用Fridge(冰箱)</span>就可以啦</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>解凍</strong></span><strong>➩<span style=""color: #20b2aa;"">Defrost</span></strong></li>
<li><span style=""color: #007799;""><strong>削皮</strong></span><strong>➩<span style=""color: #20b2aa;"">Peel</span></strong></li>
<li><span style=""color: #007799;""><strong>去鱗</strong></span><strong>➩<span style=""color: #20b2aa;"">Scale</span></strong></li>
<li><span style=""color: #007799;""><strong>去核</strong></span><strong>➩<span style=""color: #20b2aa;"">Core</span></strong></li>
</ul>
<ul>
<ul>
<li><span style=""color: #800000;"">Core</span>這個字也有<span style=""color: #800000;"">核心</span>的意思，像是健身的核心肌群英文就是Core muscle</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>切除不要的部分</strong></span><strong>➩<span style=""color: #20b2aa;"">Trim</span></strong></li>
</ul>
<ul>
<ul>
<li><span style=""color: #800000;"">剪輯影片</span>的英文也可以用<span style=""color: #800000;"">Trim</span>，就是在時間軸中處理、裁減的意思</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>切成小塊</strong></span><strong>➩<span style=""color: #20b2aa;"">Cube</span></strong></li>
</ul>
<ul>
<ul>
<li><span style=""color: #800000;"">Cube</span>也有<span style=""color: #800000;"">立方體</span>的意思，像是冰塊就是Ice cube</li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>切成丁狀</strong></span><strong>➩<span style=""color: #20b2aa;"">Dice</span></strong></li>
</ul>
<ul>
<ul>
<li><strong><span style=""color: #800000;"">Dice</span></strong>也是<strong><span style=""color: #800000;"">骰子</span></strong></li>
</ul>
</ul>
<ul>
<li><span style=""color: #007799;""><strong>切薄片</strong></span><strong>➩<span style=""color: #20b2aa;"">Slice</span></strong></li>
<li><span style=""color: #007799;""><strong>切絲</strong></span><strong>➩<span style=""color: #20b2aa;"">Julienne</span></strong></li>
<li><span style=""color: #007799;""><strong>醃漬</strong></span><strong>➩<span style=""color: #20b2aa;"">Pickle</span></strong></li>
<li><span style=""color: #007799;""><strong>翻面</strong></span><strong>➩<span style=""color: #20b2aa;"">Turnover/Flip</span></strong></li>
</ul>
<p><strong>&nbsp;</strong></p>
<p><iframe src=""https://www.youtube.com/embed/Nk6NuVS3nZQ"" width=""640"" height=""360"" frameborder=""0"" allowfullscreen=""""></iframe></p>
<p><strong>在學完了基本的烹調英文單字後，我們來試試看英文食譜吧！要來教大家做的是「芹菜炒魷魚」</strong></p>
<p><strong>Celery Stir Fry Squid </strong><strong>芹菜炒魷魚</strong></p>
<p><strong>Step1.<span style=""color: #8b4513;""> Clean the squid，and remove all skin and intestine</span></strong></p>
<p>第一步 清洗魷魚，並拔掉魷魚的皮與內臟</p>
<ul>
<li><strong><span style=""color: #800000;"">大腸</span></strong>的英文是<strong><span style=""color: #800000;"">Large intestine</span></strong><strong>，</strong>同理<strong><span style=""color: #800000;"">小腸</span></strong>就是<strong><span style=""color: #800000;"">Small intestine</span></strong></li>
</ul>
<p><strong>Step2.<span style=""color: #8b4513;"">Wash celery</span></strong></p>
<p>第二步 洗芹菜</p>
<ul>
<li><strong>【補充】</strong>常見的蔬菜英文：</li>
</ul>
<div style=""color: #008080;"">
<ul>菠菜 Spinach</ul>
<ul>綠花椰菜 Broccoli</ul>
<ul>白花椰菜 Cauliflower</ul>
<ul>萵苣 Lettuce</ul>
<ul>高麗菜 Cabbage</ul>
<ul>韭菜 Garlic Chives</ul>
</div>
<p><strong>Step3.<span style=""color: #8b4513;""> Cut the squid into your desired shape and celery into bite sizes</span></strong></p>
<p>第三步 將魷魚及芹菜切成您喜愛的、好入口的大小</p>
<p><strong>Step4. <span style=""color: #8b4513;"">Stir-fry garlic and oil to bring out the flavor</span></strong></p>
<p>第四步 快炒油、蒜，爆香</p>
<p><strong>Step5.<span style=""color: #8b4513;""> Add in celery</span></strong></p>
<p>第五步 將芹菜加入鍋</p>
<p><strong>Step6. <span style=""color: #8b4513;"">Flavor the dish into your liking (Recommend: Shacha sauce)</span></strong></p>
<p>第六步 調味，調成自己喜歡的味道啦！ (推薦：沙茶醬)</p>
<ul>
<li><strong>【補充】</strong>常見的調味料英文</li>
</ul>
<div style=""color: #008080;"">
<ul>番茄醬Ketchup</ul>
<ul>黃芥末醬 Mustard</ul>
<ul>美乃滋 Mayonnaise 可以簡稱Mayo</ul>
<ul>辣椒醬 Chili sauce</ul>
<ul>醬油 Soy sauce</ul>
<ul>糖醋醬 Sweet and sour sauce</ul>
</div>
</div>"','published' => '1','og_title' => '看懂英文料理食譜好簡單！烹飪英文單字整理！','og_description' => '想做出道地異國料理，卻看不懂英文食譜嗎？在國外餐廳打工，害怕聽不懂廚師間的專業烹飪術語嗎？現在就來學學這些常見又實用的烹飪英文吧！','meta_title' => '看懂英文料理食譜好簡單！烹飪英文單字整理！','meta_description' => '想做出道地異國料理，卻看不懂英文食譜嗎？在國外餐廳打工，害怕聽不懂廚師間的專業烹飪術語嗎？現在就來學學這些常見又實用的烹飪英文吧！','canonical_url' => 'https://tw.english.agency/烹飪英文單字整理','feature_image_alt' => '看懂英文料理食譜好簡單！烹飪英文單字整理！','feature_image_title' => '看懂英文料理食譜好簡單！烹飪英文單字整理！','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59fff39c816db-MrmckMbaLxsFZP2xsGrP7wqRnUfQVQ-5001x2626.png','author_id' => '1','category_id' => '10','created_at' => '"2017-09-05 10:03:11"'],
  ['id' => '59','title' => '【體育賽事】世大運回家比賽，運動項目懶人包','slug' => '世大運運動項目','excerpt' => '台北世大運，讓我們留下難忘時刻，陳金鋒點燃聖火、郭婞淳舉重破世界紀錄、楊俊瀚奪得台灣首金以及楊合貞的「五金行」稱號。家鄉各路英雄，背後歷經數不盡的磨練歲月，才成就今日的輝煌成績。讓我們一起認識運動賽事英文，尊重體育精神，更尊重選手。','content' => '"<div style=""font-size: 18px;""><strong><span style=""color: #35478c;"">「這次，我們回家比賽」，如柯市長所說──夢想越大，離家越遠</span></strong></div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><strong><span style=""color: #4e7ac7;"">陳金鋒點燃聖火、郭婞淳舉重破世界紀錄、楊俊瀚奪得台灣首金以及楊合貞的「五金行」稱號</span></strong></div>
<div style=""font-size: 18px;""><strong><span style=""color: #35478c;"">台北世大運，讓我們留下難忘時刻，家鄉各路英雄，背後歷經數不盡的磨練歲月，才成就今日的輝煌成績</span></strong></div>
<div style=""font-size: 18px;""><strong><span style=""color: #35478c;"">讓我們一起認識運動賽事英文，尊重體育精神，更尊重選手</span></strong></div>
<div style=""font-size: 18px;""><strong><span style=""color: #35478c;"">The 29<sup>th</sup>&nbsp;Summer Universiade第29屆世界大學運動會！這次，我們在台灣支持自己人！</span></strong></div>
<h2>&nbsp;</h2>
<p><strong><img title=""世大運比賽項目"" src=""http://img.mp.sohu.com/upload/20170711/0c0416f1a63e4995bc6351a97672c85b_th.png"" alt="""" width=""770"" height=""671"" /></strong></p>
<h2><strong>&nbsp;</strong></h2>
<div style=""font-size: 20px;""><strong><span style=""color: #f54f29;"">比賽項目：</span></strong></div>
<p><span style=""color: #35478c;""><strong>Baseball</strong> 棒球</span></p>
<p><span style=""color: #35478c;""><strong>Basketball</strong> 籃球</span></p>
<p><span style=""color: #35478c;""><strong>Badminton</strong> 羽球</span></p>
<p><span style=""color: #35478c;""><strong>Tennis</strong> 網球</span></p>
<p><span style=""color: #35478c;""><strong>Billiards</strong> 撞球</span></p>
<p><span style=""color: #35478c;""><strong>Football</strong> 足球</span></p>
<p><span style=""color: #35478c;""><strong>Volleyball</strong> 排球</span></p>
<p><span style=""color: #35478c;""><strong>Table tennis</strong> 桌球</span></p>
<p><span style=""color: #35478c;""><strong>Water polo</strong>水球</span></p>
<p><span style=""color: #35478c;""><strong>Golf</strong> 高爾夫球</span></p>
<p><span style=""color: #35478c;""><strong>Fencing</strong> 擊劍</span></p>
<p><span style=""color: #35478c;""><strong>Archery</strong> 射箭</span></p>
<p><span style=""color: #35478c;""><strong>Athletics</strong> 田徑 (田徑除了賽跑，還有以下比賽項目哦)</span></p>
<ul>
<li>Discus 鐵餅</li>
<li>Javelin 標槍</li>
<li>Hurdles 跨欄</li>
<li>Pole Vault 撐竿跳</li>
<li>Triple jump 三級跳</li>
<li>Shot put鉛球 Hammer 鏈球</li>
<li>Long jump 跳遠 High jump 跳高</li>
<li>Decathlon 男子十項全能 Heptathlon 女子七項全能</li>
</ul>
<p><span style=""color: #35478c;""><strong>Diving</strong> 跳水</span></p>
<p><span style=""color: #35478c;""><strong>Taekwondo</strong> 跆拳道</span></p>
<p><span style=""color: #35478c;""><strong>Weightlifting</strong> 舉重</span></p>
<p><span style=""color: #35478c;""><strong>Judo</strong> 柔道</span></p>
<p><span style=""color: #35478c;""><strong>Wushu</strong>武術 (今年世大運首屆舉辦)</span></p>
<p><span style=""color: #35478c;""><strong>Artistic gymnastics</strong> 競技體操</span></p>
<p><span style=""color: #35478c;""><strong>Artistic rhythm</strong>韻律體操</span></p>
<p><strong><span style=""color: #35478c;"">Roller sports </span></strong><span style=""color: #35478c;"">滑輪溜冰</span></p>
<p><strong>&nbsp;</strong></p>
<p>&nbsp;</p>
<div style=""font-size: 20px;""><strong><span style=""color: #f54f29;"">世大運字彙：</span></strong></div>
<p><span style=""color: #35478c;""><strong>The Universiade</strong>世界大學運動會</span></p>
<p><span style=""color: #35478c;""><strong>FISU (International university sports federation)</strong> 國際大學運動總會</span></p>
<p><span style=""color: #35478c;""><strong>Athletes village</strong>選手村</span></p>
<p><span style=""color: #35478c;""><strong>Torch</strong> 聖火火炬</span></p>
<p><span style=""color: #35478c;""><strong>Torchbearer</strong> 火炬手</span></p>
<p><span style=""color: #35478c;""><strong>Opening ceremony</strong> 開幕式</span></p>
<p><span style=""color: #35478c;""><strong>Closing ceremony</strong> 閉幕式</span></p>
<p><span style=""color: #35478c;""><strong>Competition day</strong> 賽程日</span></p>
<p><span style=""color: #35478c;""><strong>Day with medals</strong> 獎牌日</span></p>
<p>&nbsp;</p>
<p><span style=""color: #35478c;""><strong>WR (World record)</strong>世界紀錄<strong>&nbsp;</strong></span></p>
<p><span style=""color: #35478c;""><strong>UR</strong> <strong>&nbsp;(Universiade record)</strong>世大運紀錄</span></p>
<p><span style=""color: #35478c;""><strong>MR</strong>&nbsp;&nbsp;<strong>(Meet record)</strong>大會紀錄</span></p>
<p><span style=""color: #35478c;""><strong>SB</strong> <strong>(Season&rsquo;s best)</strong>本季最佳&nbsp;</span></p>
<p><span style=""color: #35478c;"">&nbsp;</span></p>
<p><span style=""color: #35478c;""><strong>Competition schedule</strong> 賽程表</span></p>
<p><span style=""color: #35478c;""><strong>Venue</strong> 會場、場館</span></p>
<p><span style=""color: #35478c;""><strong>Singles</strong> 單打</span></p>
<p><span style=""color: #35478c;""><strong>Doubles</strong> 雙打</span></p>
<p><span style=""color: #35478c;""><strong>Mixed doubles</strong> 混雙打</span></p>
<p><span style=""color: #35478c;""><strong>Preliminary</strong> 預賽</span></p>
<p><span style=""color: #35478c;""><strong>Time trial</strong> 計時賽</span></p>
<p><span style=""color: #35478c;""><strong>Elimination</strong> 淘汰賽</span></p>
<p><span style=""color: #35478c;""><strong>Sprint</strong> 爭先賽、衝刺</span></p>
<p><span style=""color: #35478c;""><strong>Relay</strong> 接力賽</span></p>
<p><span style=""color: #35478c;""><strong>Round</strong> 回合</span></p>
<p><span style=""color: #35478c;""><strong>Semi-final</strong> (四強) 準決賽</span></p>
<p><span style=""color: #35478c;""><strong>Final</strong> 決賽</span></p>
<p>&nbsp;</p>
<p><span style=""color: #35478c;""><strong>Gold medal</strong> 金牌</span></p>
<p><span style=""color: #35478c;""><strong>Silver medal</strong> 銀牌</span></p>
<p><span style=""color: #35478c;""><strong>Bronze medal</strong> 銅牌</span></p>
<p>&nbsp;</p>
<p><span style=""color: #35478c;""><strong>Pension reform</strong> 年金改革</span></p>
<p><span style=""color: #35478c;""><strong>Protester</strong> 抗議人士</span></p>
<p><span style=""color: #35478c;""><strong>Opponent</strong> 抗議人士、(比賽)對手</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><img title=""世大運台北我們的主場"" src=""http://img.appledaily.com.tw/images/ReNews/20170526/640_9f8387bae46e1951d79ad7f88d7367b3.jpg"" alt="""" width=""698"" height=""362"" /></p>
<p>&nbsp;</p>
<div style=""font-size: 20px;"">&nbsp;</div>
<div style=""font-size: 20px;""><strong><span style=""color: #5d7359;"">政治Politics</span></strong></div>
<p><strong><span style=""color: #8c5430;"">Opponents staged protest outside the Taipei Municipal Stadium during the Summer Universiade opening ceremony.</span></strong></p>
<p><strong><span style=""color: #8c5430;"">The protesters against the government&rsquo;s pension reform blocked the athletes from entering the venue.</span></strong></p>
<p><span style=""color: #8c5430;"">台北田徑場舉行世大運開幕典禮，遇上反年改人士場外抗議。抗議人士阻擋會場入口，運動選手進場受阻。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 20px;""><strong><span style=""color: #5d7359;"">世大運聖火 The Universiade flame</span></strong></div>
<p><strong><span style=""color: #8c5430;"">ChenChin-feng lit the flame by hitting the home run that sent the burning ball into the UniversiadeCauldron.</span></strong></p>
<p><span style=""color: #8c5430;"">陳金鋒揮出全壘打，將點燃的火球擊到聖火台，點燃世大運聖火。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 20px;""><strong><span style=""color: #5d7359;"">標槍 Javelin</span></strong></div>
<p><strong><span style=""color: #8c5430;"">ChengChao-tsun won the gold medal in javelin at the Taipei Summer Universiadewith a throw of 91.36 meters，settinga new Asia record.</span></strong></p>
<p><span style=""color: #8c5430;"">鄭兆村在台北世大運標槍比賽中獲勝，並以91.36公尺創下亞洲新紀錄。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 20px;""><strong><span style=""color: #5d7359;"">田徑 Athletics</span></strong></div>
<p><strong><span style=""color: #8c5430;"">Yang Chun-han won Taiwan&rsquo;s first-ever gold medal in men&rsquo;s 100m final with a time of 10.22 seconds.</span></strong></p>
<p><span style=""color: #8c5430;"">楊俊瀚在世大運男子組100公尺決賽以10秒22奪下台灣首金。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 20px;""><strong><span style=""color: #5d7359;"">舉重 Weightlifting</span></strong></div>
<p><strong><span style=""color: #8c5430;"">KuoHsing-chun broke the world record by lifting 142 kg in the clean and jerk in women&rsquo;s 58-kilogram weight class.</span></strong></p>
<p><span style=""color: #8c5430;"">郭婞淳在女子組58公斤級比賽中以挺舉142公斤，打破世界新紀錄。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 20px;""><strong><span style=""color: #5d7359;"">滑輪溜冰 Roller sports</span></strong></div>
<p><strong><span style=""color: #8c5430;"">Yang Ho-Chen won five gold medals in women&rsquo;s 10000m point-elimination，15000m elimination，</span></strong></p>
<p><strong><span style=""color: #8c5430;"">1000m sprint，3000m relay，women&rsquo;s marathon final as well as a bronze medal in the 500m sprint.</span></strong></p>
<p><span style=""color: #8c5430;"">楊合貞分別在女子組10000公尺計分淘汰賽、15000公尺淘汰賽、1000公尺爭先賽、</span></p>
<p><span style=""color: #8c5430;"">3000公尺接力賽與馬拉松中贏下五面金牌，以及500公尺爭先賽的銅牌。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 20px;""><strong><span style=""color: #5d7359;"">獎牌數 Medal count</span></strong></div>
<p><strong><span style=""color: #8c5430;"">Taiwan now ranks in fourth place with a total seventy six medals，includingnineteen gold，</span></strong></p>
<p><strong><span style=""color: #8c5430;"">twenty nine silver and twenty eight bronze medals.</span></strong></p>
<p><span style=""color: #8c5430;"">台灣以76個獎牌列居第四，包含19個金牌、29個銀牌和28個銅牌。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '【體育賽事】世大運回家比賽，運動項目懶人包','og_description' => '陳金鋒點燃聖火、郭婞淳舉重破世界紀錄、楊俊瀚奪得台灣首金以及楊合貞的「五金行」稱號。台北世大運，讓我們留下難忘時刻，家鄉各路英雄，背後歷經數不盡的磨練歲月，才成就今日的輝煌成績。讓我們一起認識運動賽事英文，尊重體育精神，更尊重選手。','meta_title' => '【體育賽事】世大運回家比賽，運動項目懶人包','meta_description' => '陳金鋒點燃聖火、郭婞淳舉重破世界紀錄、楊俊瀚奪得台灣首金以及楊合貞的「五金行」稱號。台北世大運，讓我們留下難忘時刻，家鄉各路英雄，背後歷經數不盡的磨練歲月，才成就今日的輝煌成績。讓我們一起認識運動賽事英文，尊重體育精神，更尊重選手。','canonical_url' => 'https://tw.english.agency/世大運運動項目','feature_image_alt' => '世大運回家比賽，用運動走向世界','feature_image_title' => '世大運回家比賽，用運動走向世界','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59b0e92107f83-zxaMwSjC8oE2Bi8QQHJE9BHYwIDYH9-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2017-09-07 04:38:50"'],
  ['id' => '60','title' => '英文書信怎麼寫？求職留學必學技能！','slug' => '英文書信-求職-留學','excerpt' => '英文寫信有分成書信和電子郵件，格式也有分成正式(formal)和非正式(informal)。舉例來說，正式就像是商用書信，寫給老闆、客戶、教授等，而非正式的就是寫給家人、朋友的。今天要教大家的是萬用格式，無論是書信或是電子郵件都可以用上喔！','content' => '"<div style=""font-size: 18px;"">
<p>無論是想應徵外商公司、在國外求職，或是想出國留學、打工度假，學會怎麼用英文寫信都是必備的技能！就像中文書信一樣，英文書信也有許多格式、規矩，現在學會這些規則，要用時就不怕不知道怎麼寫了！</p>
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59b1098840e93-Ecmr0Bi2O1khS3eXF2sF3VFJBjpLWv-300x200.jpeg"" alt=""英文書信怎麼寫？英文書信撰寫技巧注意事項"" width=""570"" height=""380"" /></p>
<p>英文寫信有分成<strong><span style=""color: #ffaa33;"">書信</span></strong>和<strong><span style=""color: #ffaa33;"">電子郵件</span></strong>，格式也有分成<span style=""color: #ffaa33;""><strong>正式</strong><strong>(formal)</strong></span>和<span style=""color: #ffaa33;""><strong>非正式</strong><strong>(informal)</strong></span>。舉例來說，正式就像是商用書信，寫給老闆、客戶、教授等，而非正式的就是寫給家人、朋友的。今天要教大家的是萬用格式，無論是書信或是電子郵件都可以用上喔！</p>
<p>&nbsp;</p>
<ol>
<li>
<h2 style=""font-size: 26px;""><span style=""color: #a42d00;""><strong>Begin with a greeting </strong><strong>開頭問候</strong></span></h2>
</li>
</ol>
<p>英文信件的第一個字是開頭稱呼(<span style=""color: #ffaa33;""><strong>salutations and greetings</strong></span>)，常見的有：</p>
<ul>
<li><span style=""color: #008080;""><strong>Dear</strong></span></li>
</ul>
<p>Dear是最常見的也最萬用的，雖然直翻中文「親愛的」好像有點裝熟，但這裡Dear是個較正式的打招呼法，像是「親愛的用戶」、「親愛的會員」，適用於所有對象。</p>
<ul>
<li><span style=""color: #008080;""><strong>Dear Sir or Madam</strong></span></li>
</ul>
<p>Dear Sir or Madam是用在「不知道收信者姓名」的情況下，比如說在申請國外大學時要寄件給學校招生辦公室 (<span style=""color: #ffaa33;""><strong>admission office</strong></span>)，這時候開頭就可以用Dear Sir or Madam。</p>
<ul>
<li><span style=""color: #008080;""><strong>To whom it may concern</strong></span></li>
</ul>
<p>也是用在不清楚收件者姓名的情況下</p>
<ul>
<li><span style=""color: #008080;""><strong>Hi</strong></span></li>
</ul>
<p>這是較輕鬆的說法，通常用在熟識的對象，也可以說<span style=""color: #ffaa33;""><strong>Hello</strong></span>/ <span style=""color: #ffaa33;""><strong>Hey</strong></span>，對象多人時可以說<span style=""color: #ffaa33;""><strong>Hey folks</strong></span>/ <span style=""color: #ffaa33;""><strong>Hey guys</strong></span></p>
<p>&nbsp;</p>
<ol start=""2"">
<li>
<h2 style=""font-size: 26px;""><span style=""color: #a42d00;""><strong>Say thank you </strong><strong>說謝謝</strong></span></h2>
</li>
</ol>
<p>如果是正在回覆客戶的詢問，我們可以寫 &ldquo;<span style=""color: #ffaa33;""><strong>Thank you for contacting XXX Company</strong></span>&rdquo; ; 如果是有人回覆了而我們要再一次的回信，可以寫 &ldquo;<span style=""color: #ffaa33;""><strong>Thanks for getting back to me</strong></span>&rdquo;，&ldquo;<span style=""color: #ffaa33;""><strong>Thank you for your prompt reply</strong></span>&rdquo;. 重要的是以積極、禮貌的態度開始一封郵件，讓收信者(<span style=""color: #ffaa33;""><strong>recipient</strong></span>)有良好的第一印象。</p>
<p>&nbsp;</p>
<ol start=""3"">
<li>
<h2 style=""font-size: 26px;""><span style=""color: #a42d00;""><strong>State your purpose </strong><strong>信件目的</strong></span></h2>
</li>
</ol>
<p>這邊開始就是整封信的body，也就是寫信的原因，開頭可以寫 &ldquo;<span style=""color: #ffaa33;""><strong>I am writing in reference to &hellip;</strong></span>&rdquo;</p>
<p>以下介紹一些在英文信件中常見的縮寫：</p>
<ul>
<li><span style=""color: #008080;""><strong>asap</strong></span> = <strong>as soon as possible </strong><strong>盡快</strong></li>
<li><span style=""color: #008080;""><strong>RSVP</strong></span> = <strong>please reply </strong><strong>請回覆</strong></li>
<li><span style=""color: #008080;""><strong>cc</strong></span> = <strong>carbon copy </strong><strong>副本</strong></li>
<li><span style=""color: #008080;""><strong>bcc </strong></span>=<strong> blind carbon copy </strong><strong>密件副本</strong></li>
</ul>
<p>&nbsp;</p>
<ol start=""4"">
<li>
<h2 style=""font-size: 26px;""><span style=""color: #a42d00;""><strong> Add a closing lines </strong><strong>結論語句</strong></span></h2>
</li>
</ol>
<p>在結束整封信件之前，有一句的結論語句，例如:</p>
<p>&ldquo;<span style=""color: #ffaa33;""><strong> I look forward to hearing from you</strong></span>&rdquo; <span style=""color: #008080;""><strong>期待您的消息</strong></span></p>
<p>&ldquo; <span style=""color: #ffaa33;""><strong>Thank you for your patience and cooperation</strong></span>&rdquo; <span style=""color: #008080;""><strong>謝謝您的耐心和合作</strong></span></p>
<p>&ldquo; <span style=""color: #ffaa33;""><strong>Once again，I apologize for the inconvenience</strong></span>&rdquo; <span style=""color: #008080;""><strong>很抱歉造成您的不便</strong></span></p>
<p>&nbsp;</p>
<ol start=""5"">
<li>
<h2 style=""font-size: 26px;""><span style=""color: #a42d00;""><strong>End with a closing </strong><strong>結尾用語</strong></span></h2>
</li>
</ol>
<p>就像中文的「敬上」、「敬啟」，英文信件的最後一步也是結尾用語，例如：</p>
<ul>
<li><span style=""color: #ffaa33;""><strong>Sincerely yours，</strong></span></li>
<li><span style=""color: #ffaa33;""><strong>Faithfully yours，</strong></span> ➡用在不知道收件人姓名的情況</li>
<li><span style=""color: #ffaa33;""><strong>Respectfully yours，</strong></span>&nbsp;➡非常正式，通常是正式的商業書信用途</li>
</ul>
<p>*在英式英語中，會將Yours的順序相反: <strong><span style=""color: #008080;"">Yours sincerely</span>，</strong>/ <strong><span style=""color: #008080;"">Yours faithfully</span></strong>，/ <strong><span style=""color: #008080;"">Yours respectfully</span></strong></p>
<ul>
<li><strong><span style=""color: #ffaa33;"">Best regards，/ Kind regards，/ Warm regards</span></strong></li>
</ul>
<p><strong>稍微不正式</strong>一點的用法</p>
<ul>
<li><span style=""color: #ffaa33;""><strong>Best wishes，/ Best，/ Regards，/ Cheers，/ Take care，/ Love，/ Peace</strong></span></li>
</ul>
<p><strong>不正式</strong>的用法，用在朋友、家人之間</p>
<p>&nbsp;</p>
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59b1092dc7546-1A55JJ4ic59iVXm0q9VEq8SM9biFyH-300x197.jpeg"" alt=""英文書信重點訣竅"" width=""630"" height=""414"" /></p>
<p>是不是覺得有點複雜呢？接下來就提供兩個我自己奉為中心思想的寫英文信件訣竅給大家：</p>
<ul>
<li><span style=""color: #ffaa33;""><strong>Informal or Formal? </strong></span><span style=""color: #008080;""><strong>正式還是非正式? </strong></span></li>
</ul>
<p>幾乎所有的greetings，closings 都和與收件者的關係是正式的或非正式的有關。確立好自己和收件者的關係是哪一種後，要套用哪個詞彙就不成問題啦！</p>
<ul>
<li><span style=""color: #ffaa33;""><strong>Be clear and precise </strong></span><span style=""color: #008080;""><strong>清楚、明確的邏輯</strong></span></li>
</ul>
<p>無論想用信件傳遞的內容有多複雜，都要有個清楚的邏輯與連貫性。將內容<strong>分段</strong>、適時地使用 &ldquo;I am writing to you regarding&hellip; &ldquo;、 &ldquo;I am contacting you because &hellip;&rdquo; 等<strong>標註重點的句子</strong>、簡明扼要的講重點，都能讓收信者讀信更輕鬆。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<p>延伸閱讀: <a href=""https://tw.english.agency/%E5%AF%A6%E7%94%A8%E8%8B%B1%E6%96%87-email%E8%8B%B1%E6%96%87%E5%B8%B8%E7%94%A8%E8%AA%9E%E7%B8%BD%E6%95%B4%E7%90%86"">Email的詢問、回覆該怎麼寫？</a></p>
<div class=""article-body"">&nbsp;</div>
</div>"','published' => '1','og_title' => '英文書信怎麼寫？求職留學必學技能！','og_description' => '英文寫信有分成書信和電子郵件，格式也有分成正式(formal)和非正式(informal)。舉例來說，正式就像是商用書信，寫給老闆、客戶、教授等，而非正式的就是寫給家人、朋友的。今天要教大家的是萬用格式，無論是書信或是電子郵件都可以用上喔！','meta_title' => '英文書信怎麼寫？求職留學必學技能！','meta_description' => '英文寫信有分成書信和電子郵件，格式也有分成正式(formal)和非正式(informal)。舉例來說，正式就像是商用書信，寫給老闆、客戶、教授等，而非正式的就是寫給家人、朋友的。今天要教大家的是萬用格式，無論是書信或是電子郵件都可以用上喔！','canonical_url' => 'https://tw.english.agency/英文書信-求職-留學','feature_image_alt' => '求職留學英文書信','feature_image_title' => '求職留學英文書信','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59bf7d0702786-7vSHOqe9zz4ZPDJeWOeWyM0np1Qrw2-1200x630.png','author_id' => '1','category_id' => '18','created_at' => '"2017-09-07 09:47:18"'],
  ['id' => '61','title' => '廚房常用廚具英文｜瓦斯爐、微波爐英文怎麼說？','slug' => '廚房-廚具-英文怎麼說','excerpt' => '"國外外食費昂貴，親自下廚是最好的省錢方式！那麼該怎麼去店裡買廚具呢？或是在餐飲業打工度假，想借把菜刀卻只能講出knife？削皮刀、削皮器、刨絲器的英文又該怎麼區分？ 快來學學這些常見的廚具英文吧！"','content' => '"<div style=""fot-size: 18px;"">
<p>國外外食費昂貴，親自下廚是最好的省錢方式！那麼該怎麼去店裡買廚具呢？</p>
<p>或是在餐飲業打工度假，想借把菜刀卻只能講出knife？削皮刀、削皮器、刨絲器的英文又該怎麼區分？ 快來學學這些常見的廚具英文吧！</p>
<p>&nbsp;</p>
<p>廚具的英文是<strong><span style=""color: #007799;"">kitchenware</span></strong>，而<strong><span style=""color: #007799;"">kitchen utensils </span></strong>(器具) 、<strong><span style=""color: #007799;"">kitchen tools </span></strong>(工具)、<strong><span style=""color: #007799;"">kitchen accessories </span></strong>(配件) 或是更通俗的<strong><span style=""color: #007799;"">kitchen stuff </span></strong>(東西) 也可以表達一樣的意思，<strong><span style=""color: #007799;"">kitchen gadget</span></strong> 指的是比較新奇的廚房小玩意，像是加拿大有這種吃玉米不怕燙手的<span style=""color: #007799;"">corn holder</span>就屬於<span style=""color: #007799;"">kitchen gadget</span>的一種</p>
<p>&nbsp;</p>
<ol>
<li><strong> Cookware </strong><strong>炊具</strong></li>
</ol>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Pot</span></strong> ➩<strong><span style=""color: #00008b;""> 鍋子</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Pan</span></strong> ➩<strong><span style=""color: #00008b;""> 平底鍋</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Saucepan</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 平底深鍋</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Frying pan </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 煎鍋</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Wok </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 炒菜鍋</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Grilling pan</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 烤盤</span></strong></p>
<p style=""padding-left: 30px;"">&nbsp;</p>
<p style=""padding-left: 30px;""><strong>&nbsp;</strong><strong>【例句】</strong></p>
<p style=""padding-left: 60px;"">Heat one TBSP oil in a clean <strong><span style=""color: #008080;"">wok</span></strong> or a <strong><span style=""color: #008080;"">pan</span></strong>，then add the garlic，green onion，and ginger and stir-fry it for one minute.</p>
<p style=""padding-left: 60px;"">在炒菜鍋或是平底鍋中放入一湯匙的油，然後放入蒜頭、青蔥以及薑，翻炒一分鐘。</p>
<p style=""padding-left: 30px;""><strong>【延伸】</strong></p>
<ul>
<li>1 <strong><span style=""color: #008080;"">TBSP</span></strong> = 1 <strong><span style=""color: #008080;"">tablespoon</span></strong> 一湯匙; 一大勺</li>
<li>1 <strong><span style=""color: #008080;"">tsp</span></strong> = 1 <strong><span style=""color: #008080;"">teaspoon</span></strong> 一茶匙; 一小勺</li>
<li><strong><span style=""color: #008080;"">stir-fry</span></strong>翻炒 = <strong><span style=""color: #008080;"">stir</span></strong> 攪動 + <strong><span style=""color: #008080;"">fry</span></strong> 煎、炸、炒;</li>
</ul>
<p style=""padding-left: 30px;""><strong><span style=""color: #008080;"">炒蛋</span></strong>的英文是<strong><span style=""color: #008080;"">scrambled eggs</span></strong> 而不是<span style=""color: #008080;""><strong>fried eggs(</strong><strong>煎蛋、荷包蛋)</strong></span></p>
<p>&nbsp;</p>
<p>&nbsp;<img style=""display: block; margin-left: auto; margin-right: auto;"" title=""廚房用具英文怎麼說"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59fc3569c27c6-76zBWzfr0SP2hRGK6fxn8LPfMVb7RD-1200x630.jpeg"" alt=""廚房用具英文怎麼說"" width=""685"" height=""357"" /></p>
<ol start=""2"">
<li><strong> Kitchen utensils </strong><strong>廚房器具</strong></li>
</ol>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Cleaver</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 中式菜刀/大型切肉刀</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Chef&rsquo;s knife </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 廚師刀/主廚刀</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Paring knife</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 削皮刀</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Peeler</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 削皮器</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Grater</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 刨絲器</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Butcher knife</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 屠刀/剁刀</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Chopping board </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 砧板</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Turner </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 鍋鏟</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Spoon</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 湯匙</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Fork </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 叉子</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Ladle</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 杓子</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Tongs </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 夾子</span></strong></p>
<p><strong>&nbsp;</strong></p>
<p style=""padding-left: 30px;""><strong>【例句】</strong></p>
<p style=""padding-left: 60px;"">Place the chicken，breast-side-up，on a <strong><span style=""color: #008080;"">chopping board</span></strong>. Then cut it into small pieces with a <strong><span style=""color: #008080;"">cleaver</span></strong>.</p>
<p style=""padding-left: 60px;"">把雞肉胸部朝上地放置在砧板上，然後用菜刀切成小塊。</p>
<p style=""padding-left: 30px;""><strong>【延伸】</strong></p>
<ul>
<li><strong><span style=""color: #008080;"">Cut</span></strong> 切，所有的切都可以用cut表示</li>
<li><strong><span style=""color: #008080;"">Slice</span></strong> 切片</li>
</ul>
<p style=""padding-left: 30px;"">一片pizza可以說是<strong><span style=""color: #008080;"">a pizza slice</span></strong>. 而時下<strong><span style=""color: #008080;"">Pizza Slice (PS)</span></strong>也是線上遊戲的私服<strong><span style=""color: #008080;"">Private Server</span></strong>的替代翻譯。For example，&nbsp;</p>
<p style=""padding-left: 30px;"">Do you know any fun <strong><span style=""color: #008080;"">Pizza Slice</span></strong> of Maple Story?</p>
<p style=""padding-left: 30px;"">你知道任何好玩的楓之谷私服嗎?</p>
<ul>
<li><strong><span style=""color: #008080;"">Dice</span></strong> 切丁</li>
</ul>
<p style=""padding-left: 30px;"">Dice 本身也有骰子的意思，像是骰子遊戲吹牛的英文就是<strong>Liar&rsquo;s Dice</strong></p>
<ul>
<li><strong><span style=""color: #008080;"">Shred</span></strong> 切絲</li>
<li><strong><span style=""color: #008080;"">Chop</span></strong> 剁</li>
</ul>
<p>&nbsp;</p>
<ol start=""3"">
<li><strong> Bakeware </strong><strong>烘焙用具</strong></li>
</ol>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Measuring cups </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 量杯</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Measuring spoon </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 量杓</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Spatula </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 抹刀</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Whisk</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 手動攪拌器</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Mixer</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 電動攪拌器</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Mold </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 模具</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Rolling Pin</span> </strong><strong>➩</strong><strong><span style=""color: #00008b;""> 擀麵棍</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Timer</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 計時器</span></strong></p>
<p style=""padding-left: 30px;"">&nbsp;</p>
<p style=""padding-left: 30px;""><strong>【例句】</strong></p>
<p style=""padding-left: 60px;"">After filling the coffee <strong><span style=""color: #008080;"">pot </span></strong>and setting the<strong> <span style=""color: #008080;"">timer</span></strong>，I began writing my essay.</p>
<p style=""padding-left: 60px;"">再裝滿咖啡壺和設定好計時器後，我開始寫我的論文。</p>
<p>&nbsp;</p>
<ol start=""4"">
<li><strong> Kitchen appliances </strong><strong>廚房電器</strong></li>
</ol>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Oven</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 烤箱</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Microwave oven </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 微波爐</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Dishwasher</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 洗碗機</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Refrigerator </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 冰箱</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Freezer </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 冷凍庫</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Induction cooktop </span></strong><strong>➩</strong><strong><span style=""color: #00008b;""> 電磁爐</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Gas cooktop</span> </strong><strong>➩</strong><strong><span style=""color: #00008b;""> 瓦斯爐</span></strong></p>
<p style=""padding-left: 30px;"">˙ <strong><span style=""color: #007799;"">Range hood</span></strong> <strong>➩</strong><strong><span style=""color: #00008b;""> 抽油煙機</span></strong></p>
<p style=""padding-left: 30px;""><strong>【例句】</strong></p>
<p style=""padding-left: 60px;"">Chelsea stooped to see what goodies remained in the <strong><span style=""color: #008080;"">refrigerator</span></strong>. Sadly，she found nothing.</p>
<p style=""padding-left: 60px;"">Chelsea 彎腰看冰箱裡還有什麼好吃的東西，可悲的是，她什麼都沒找到。</p>
<p style=""padding-left: 30px;""><strong>【延伸】</strong></p>
<p style=""padding-left: 30px;"">其實在與外國人溝通時，口語講refrigerator為<strong><span style=""color: #008080;"">fridge</span></strong>可以讓他們覺得你更道地，以下整理了一些常用的口語說法，學會了講英文能更自然喔</p>
<ul>
<li>Telephone <strong>➩&nbsp;</strong><strong><span style=""color: #008080;"">Phone</span></strong></li>
<li>I don&rsquo;t know&nbsp;<strong>➩&nbsp;</strong><strong><span style=""color: #008080;"">No idea/ I&rsquo;m not sure</span></strong></li>
<li>How are you? <strong>➩&nbsp;</strong><strong><span style=""color: #008080;"">How&rsquo;s it going/ How&rsquo;s life/ How are you doing</span></strong></li>
<li>I&rsquo;m fine，thank you <strong>➩ </strong><strong><span style=""color: #008080;"">Pretty good/ Same old same old (老樣子)/ Not bad</span></strong></li>
<li>You&rsquo;re welcome <strong>➩&nbsp;</strong><strong><span style=""color: #008080;"">No problem</span></strong></li>
</ul>
</div>"','published' => '1','og_title' => '','og_description' => '','meta_title' => '','meta_description' => '','canonical_url' => 'https://tw.english.agency/廚房-廚具-英文怎麼說','feature_image_alt' => '廚房常用廚具英文｜瓦斯爐、微波爐英文怎麼說？','feature_image_title' => '廚房常用廚具英文｜瓦斯爐、微波爐英文怎麼說？','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59fc3593dd43e-aDTXWgCbCDF6rddwWhVmOJFEE99YQY-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2017-09-21 06:44:02"'],
  ['id' => '62','title' => '"和老外聊世大運、破紀錄 必學這4句英文！"','slug' => '世大運-破紀錄-必學這4句英文','excerpt' => '2017台北世大運賽事期間，台灣選手屢屢傳來捷報，在多項目中打破全國、亞洲，甚至世界紀錄！來趁機學賽事相關英文，學習如何和外國朋友一起分享這個榮耀！','content' => '"<p>【文／Buffy Kao】</p>
<p>2017台北世大運賽事，自開賽以來台灣選手屢屢傳來捷報，中華代表團表現搶眼可說是有目共睹。其中，台灣在標槍、田徑、舉重等項目打破全國、亞洲，甚至世界紀錄，讓全國人民無不為之瘋狂。<br />此時正是讓外國友人見證台灣軟實力的時候！我們不妨趁機學賽事相關英文，讓你能用英文暢談中華隊的表現。另外，這些詞彙也經常出現在多益測驗中，學會以下英文用語，無論面對外國人或考試都沒煩惱！</p>
<h4><br />1. <strong><span style=""color: #a42d00;"">set a record 創下紀錄</span></strong></h4>
<p><br />在本屆世大運中，有好幾位台灣選手創下紀錄，來看看這個片語如何應用。<br />Cheng Chao-Tsun（鄭兆村）yesterday added another gold medal to Taiwan&rsquo;s tally at the Taipei Summer Universiade and set a Universiade javelin record of 91.36m.<br />（鄭兆村昨日在2017台北世大運又幫台灣贏得一金入袋，並且以91.36公尺創下了世大運標槍的紀錄。）<br />set a record是談到運動賽事必會出現的片語，只是上文中加入一些其他訊息表達詳細資訊，順序由大到小，賽事到項目。<br />(1) 先說明關於什麼賽事的紀錄：the Taipei Summer Universiade 台北世大運<br />(2) 接著說何種比賽的項目：Universiade javelin 世大運標槍項目<br />另外句中用了tally這個單字，英文解釋為a current score or amount（目前的得分或數量），也就是中華隊目前累積獎牌數的意思，而鄭兆村的成績讓台灣獎牌數更上層樓。</p>
<h4><br /><strong>2. <span style=""color: #a42d00;"">clock 取得、設下紀錄</span></strong></h4>
<p><br />clock這個字令你感到匪夷所思嗎？請看以下例句：<br />Irishman Shane Ryan was already knocking on the door of a breakthrough swim<br />when clocking a new national record last night in the men&rsquo;s 50m backstroke，but the 23-year-old made history in Taipei tonight.<br />（愛爾蘭選手Shane Ryany在昨晚的男子50公尺仰式項目早已取得國內新紀錄，敲開游泳項目突破的大門，而今晚這名年僅23歲的選手則是創造了歷史。）<br />用clock來表達「取得、設下紀錄」的動詞，乍看之下似乎很難懂，不過若是想想選手比賽時都是用時鐘計時來決定成績高低，就不難想像這個用法。裁判按下碼表的那刻決定了選手的命運，這也正是他們創造紀錄的時刻。</p>
<h4><br /><strong>3. <span style=""color: #a42d00;"">break the record 打破紀錄</span></strong></h4>
<p><br />除了創紀錄之外，在談論運動比賽經常說的「打破紀錄」該如何表達呢？<br />Kopron broke a record of 75.83 set by Germany&rsquo;s Betty Heidler in 2009.<br />（Kopron則是打破了由德國好手Betty Heidler在2009年所創下75.83的紀錄。）<br />從例句可見，動詞break能用來表示「打破紀錄」。文中 broke a record of 後面接由誰設下的紀錄，數字75.83是該選手當初設下的紀錄，所以本句的意思為「Kopron打破由Betty Heidler設下的75.83紀錄」。<br />Cheng&rsquo;s throw broke the previous Universiade record set by Italy&rsquo;s Marius Corbett in 1997.<br />（鄭兆村這一擲打破了前一屆世大運義大利選手Marius Corbett在1997年所設下的紀錄。）<br />上文則是與前述創紀錄採用同樣的表達法，在中間加上特定訊息，增加句子豐富度，previous（之前的）在此指的是上一屆的世大運。<br />另外，以上兩例句都用了set by表達設下先前紀錄的人是誰，正是前面set a record中的設立一詞。</p>
<p>&nbsp;</p>
<h4><strong>4. <span style=""color: #a42d00;"">a personal best of 個人最佳成績～</span></strong></h4>
<p><br />討論運動比賽怎能忽略選手的「個人最佳成績」？來看看如何用英文表達吧！<br />In the women&rsquo;s 200m final，Italy&rsquo;s Irene Siragusa won gold with a personal best of 22.96 seconds，followed by Latvia&rsquo;s Gunta Latiseva Cudare，who took silver with a<br />personal best of 23.15 seconds，and Italy&rsquo;s Anna Bongiorni won bronze with 23.47<br />seconds.<br />（在女子兩百公尺決賽中，義大利的Irene Siragusa則是以個人最佳成績22.96秒贏得金牌，而拉脫維亞的Gunta Latiseva Cudare則是以個人最佳成績23.15秒贏得銀牌，最後義大利的Anna Bongiorni以23.47秒贏得銅牌。）<br />文中提到兩位選手都破了個人紀錄，因此作者用了兩次a personal best of來表達他們個人最佳成績，介係詞of後面也是加上此紀錄內容，用數字表達。<br />在說明獲得銀牌的選手時所使用的followed by是「接著為～」。原句的意思是 Italy&rsquo;s<br />Irene Siragusa was followed by Latvia&rsquo;s Gunta Latiseva Cudare. 用被動態表達了後者的紀錄跟著前者，也就是「緊接著」，所以followed by後面都要加上第二名或第二位。</p>
<p>&nbsp;</p>
<p><strong><span style=""color: #0000ff;"">多益模擬試題</span></strong></p>
<p><br />To: Hank Martin &lt;hmartin@solarwattage.com&gt;<br />From: Anna Sabiani &lt;asabiani@solarwattage.com&gt;<br />Date: August 28 11:47<br />Subject: Promotional opportunity</p>
<p><br />Hi Hank，<br />I am forwarding the press release about the Solar Trophy that I mentioned to you this morning.<br />I met Bill Bradley，the race coordinator，at the solar energy conference in Manila where he was giving a presentation about the event. I understand from him that the first race was a huge success and generated enormous media coverage both in traditional media and on the Internet.<br />For the next edition of the race he is expecting applications from as many as twenty yachts from seven different countries，including Japan.<br />He suggested that we might like to sponsor one of the crews especially as we are already supplying marine versions of our solar panels to a number of boatyards in Europe and Asia. He particularly recommended that we contact Sven Hadrada of SailYard boats as they are apparently considering entering the race.<br />We&rsquo;ve both worked with SailYard on several occasions. Do you think you could contact them as soon as possible and set up a meeting to explore ways of working together on this?</p>
<p>Best regards<br />Anna</p>
<p><br />1. What is the Solar Trophy?<br />(A) A competition for road vehicles<br />(B) A solar energy research award<br />(C) A boat race<br />(D) A shipping company<br /> <br />2. Which type of craft can enter the Solar Trophy?<br />(A) Ocean liners<br />(B) Inflatable boats<br />(C) Sailboats<br />(D) Solar-powered yachts<br /> <br />3. How many times has the Solar Trophy been held?<br />(A) Once<br />(B) Twice<br />(C) Three Times<br />(D) Ten Times</p>
<p><br /><strong> <span style=""color: #a42d00;"">解析</span></strong></p>
<p>1. 正確答案(C)。題目問Solar Trophy是什麼？信中屢屢提到race這個單字，如I met Bill Bradley，the race coordinator緊接著在作者提到solar trophy後面，還有the first race也提到首次的比賽，都顯示作者正在談論一個比賽。因此只有選項(C)船賽符合文意，(A)車賽、(B)太陽能獎項、(D)船運公司都不合理。</p>
<p>2. 正確答案(D)。題目問什麼樣的作品可以進入太陽獎杯比賽？首先作者提到Bill Bradley在the solar energy conference in Manila（馬尼拉的太陽能會議）中was giving a presentation about the event（上台介紹這個活動），由此可見這場賽事應該跟太陽能有關。後面也提到要這間公司sponsor one of the crews（贊助其中一間參賽隊伍），理由是這間公司already supplying marine versions of our solar panels to a number of boatyards in Europe and Asia（早就提供歐洲和亞洲許多船塢海上版的太陽能板），更可以看出來這場賽事應該是跟太陽能相關的比賽，才會希望早在這領域耕耘的公司認養贊助。因此答案為(Ｄ)太陽能傳動的遊艇。</p>
<p>3. 正確答案(A)。題目問這比賽已經舉辦幾次？文中提到the first race was a huge success（第一次的比賽大成功），而後面提到for the next edition of the race he is expecting &hellip;（下一次的比賽他預期～）這個語法代表的是未來式，代表下一次的賽事還未舉辦，只是在籌辦過程中，因此答案為(A)一次。</p>
<p>photo: 臺北世大運官方網站</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '"和老外聊世大運、破紀錄 必學這4句英文！"','og_description' => '2017台北世大運賽事期間，台灣選手屢屢傳來捷報，在多項目中打破全國、亞洲，甚至世界紀錄！來趁機學賽事相關英文，學習如何和外國朋友一起分享這個榮耀！','meta_title' => '"和老外聊世大運、破紀錄 必學這4句英文！"','meta_description' => '2017台北世大運賽事期間，台灣選手屢屢傳來捷報，在多項目中打破全國、亞洲，甚至世界紀錄！來趁機學賽事相關英文，學習如何和外國朋友一起分享這個榮耀！','canonical_url' => 'https://tw.english.agency/世大運-破紀錄-必學這4句英文','feature_image_alt' => '世大運必學英文','feature_image_title' => '世大運必學英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59ce064bb20a8-Pn9roUtO6dcxRA2TEGYxAuvWO5jckx-1200x630.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2017-09-29 08:38:11"'],
  ['id' => '63','title' => '挑戰爭取理想職務，備好3個必備條件！','slug' => '挑戰爭取理想職務-備好3個必備條件','excerpt' => '要想得到令人稱羨的工作，須具備的條件也不少！美商應用材料公司人資經理告訴你，需要條件有哪些！','content' => '"<p>文／Eileen Shen</p>
<p><br />薪資比行情高5~10%、加班費高於2倍、薪資計算、工時彈性、早餐免費供應、午晚餐價廉、擁有完善設備及課程的健身中心、扁平化從屬關係、國際溝通環境、出差及外派機會多⋯⋯，這些工作條件，聽起來是不是很誘人？但你相信嗎？這是大多數人有著「工時長、很辛苦」刻板印象的半導體產業公司！</p>
<p>「大多數人對我們公司不太認識，有些從我們的公司名判斷我們是賣材料的；有些以為一定要台清交成畢業才能來應徵；往往也都是材料系比較認識我們，但其實我們也相當歡迎電子、電機、機械、控制、航太等科系背景的人加入。」台灣應用材料公司人力資源中心招募經理陳仲賢如是說。</p>
<p>美商應用材料公司（Applied Materials，Inc.）主要提供半導體晶片製造及顯示器相關系統設備，母公司位於美國加州矽谷，全世界有超過18個國家、81個地區設有服務據點，其中台灣子公司（以下簡稱台灣應材）成立至今已超過27年歷史，因客戶服務據點分布全球，包括台灣應材在內，員工、工程師團隊都是以全球服務據點共同作業、彈性調配人力資源。也就是說，你的主管可能是美國人、團隊成員有法國人、客戶是台灣人，明天要到以色列受訓，下周要去日本開會。</p>
<p>「因此，在我們公司能夠體會到真正的外商、國際職場環境，工時彈性、主管下屬之間關係也不像傳統企業，反而是更開放、更活潑，相當適合年輕人發揮。」</p>
<p>「根據多方面的觀察，一方面因為這樣的族群對於新事物的學習意願和動機較高，也較願意配合彈性工時、和客戶頻繁溝通等工作特性。」而台灣應材的薪資福利和工作環境，也的確符合時下的年輕族群對於工作的期待：高薪、開放、設備完善的健身中心及運動課程、國際職場環境、時常有機會出國出差、培訓進修體制健全等等。</p>
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59d736f4d5e72-8ggHYlGtd0TfDTyT0H8IUrn92xuuww-300x222.png"" alt="""" width=""300"" height=""222"" /></p>
<p><strong><span style=""color: #436eee;"">條件：英語力、態度、人際溝通力</span></strong></p>
<p><br />不過，要想得到令人稱羨的工作，須具備的條件也不少。陳仲賢不諱言地表示，「首要條件當然就是英語文能力。」台灣應材的員工、主管、客戶、合作夥伴皆來自世界各國，無論內外部溝通聯繫、會議、文件、email往來，全數以英文進行，因此該公司針對不同職務設定英語文能力標準，如工程師TOEIC550分、業務人員TOEIC 650~700分等等，「至於外派人員要求就更高，不僅評估英語文能力，主管也會依員工各方面工作表現的評量考績而定。」</p>
<p><br />為了能夠爭取外派、晉升或輪調機會，就得先提升英語文能力到職務需求標準。台灣應材提供不少包括英語在內的線上課程，方便同仁能透過課程進修提升能力，陳仲賢說，像是(TOEIC)模擬考題練習課程，便獲得不少同仁青睞。</p>
<p><br />除了英語文能力，台灣應材也相當看重求職者的工作態度及人際關係技巧。陳仲賢解釋，外商就如同念研究所，主管不會對員工的工作緊迫盯人，但更要求員工主動找問題、發現問題、解決問題的能力。「往往客戶或主管和你身處不同時區的國家，容許彈性工作時間，具備主動積極解決問題的工作態度，更易贏得對方信任。」</p>
<p><br />台灣應用材料公司傳播與公共事務部專員李倢宇也表示，以她自己來說，便時常出國和來自不同國家、種族、文化的人接觸，「如何達到溝通效率和共識，成為極大的挑戰，也是很寶貴的收穫。」有時工作夥伴和自己不同時區，當在下班時間接到對方寄來的email 時，如何判斷處理事情的輕重緩急、先後順序，也是在台灣應材工作所需要的能力條件。</p>
<p><br /><strong><span style=""color: #436eee;"">能力比人強 選擇多更多</span></strong></p>
<p><br />事實上，如今有許多來自印度、印尼、馬來西亞、新加坡等地的求職者，專業及英語力都相當優秀，「但台灣年輕學子的優勢在於好學、努力、專業技術能力強、有人情味、刻苦耐勞。」但陳仲賢也說，缺乏國際觀、不敢提出自己的想法，是台灣求職者必須克服的弱項，「例如面試談薪水時，不敢主動提出自己的想法和堅持，但最後離職原因卻正是薪水。」<br />他建議，在學期間應多培養國際觀、英語力，以及獨立思考（判斷）能力，可藉由參加如台灣應材等企業提供的暑期實習計畫，或是參加海外交換、志工等活動，透過企業實習、接觸國際事務，在累積經驗的同時拓展視野，有助於未來追求理想工作，當能力比人強時，能夠選擇的機會自然多更多。</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '挑戰爭取理想職務，備好3個必備條件！','og_description' => '要想得到令人稱羨的工作，須具備的條件也不少！美商應用材料公司人資經理告訴你，需要條件有哪些！','meta_title' => '挑戰爭取理想職務，備好3個必備條件！','meta_description' => '要想得到令人稱羨的工作，須具備的條件也不少！美商應用材料公司人資經理告訴你，需要條件有哪些！','canonical_url' => 'https://tw.english.agency/挑戰爭取理想職務-備好3個必備條件','feature_image_alt' => '職務英語能力標準','feature_image_title' => '職務英語能力標準','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59d739d11c65b-PTuT9ViaBtaEkpiDUYMiB1Hq6ovNkB-1200x579.png','author_id' => '9','category_id' => '19','created_at' => '"2017-10-06 08:08:58"'],
  ['id' => '64','title' => '英、美式英語總是搞混？讓「金牌特務」來教你','slug' => '英-美式英語總是搞混','excerpt' => '在生活、工作中或多或少會接觸到英式英語和美式英語，而這也是多益測驗的考點之一！不妨跟著Kingsman一起學常用英、美式英語吧！','content' => '"<p>文／徐碧霞<br /> <br />夾帶著第一集超高人氣，金牌特務強勢回歸大螢幕！這次在《金牌特務：機密對決》（Kingsman 2: The Golden Circle）中的一大看點是英國紳士（Kingsman）對上美國牛仔（Statesman，美國版的Kingsman），也就是英美文化差異，撞擊出吸引力十足的火花。<br /> <br />相信讀者在生活、工作中或多或少會接觸到英式英語和美式英語，而這也是多益測驗的考點之一！不妨跟著Kingsman一起學常用英、美式英語吧！<br /> <br /><span style=""color: #0000ff;""><strong>紳士的英式英語</strong></span><br />&ldquo;Manners maketh the man.&rdquo;這是金牌特務第一集（Kingsman: the Secrete Agent）裡Harry Hart在餐廳開打場面裡所說的一句話，而這句話也是貫穿電影的核心價值：有禮、有文化和品味，是成為紳士不可或缺的要素。<br /> <br />電影中展現出的英式紳士品味可以從座落於薩佛街（Savile Row）的總部、特務的衣著、配備、配件（signet ring）和對話裡窺知一二。Kingsman穿的bespoke suits中文為量身訂製的西裝。<span style=""color: #a42d00;"">Bespoke</span>（訂製的）屬於英式用法，也就是平時我們常聽到的tailor-made、custom-made、customized或personalized。<br /> <br /><span style=""color: #a42d00;"">tailor</span>可用作名詞，當裁縫師解釋，也可當動詞，當客製或量身訂製。<br />The quality of Hong Kong tailors\' work can rival that of Savile Row tailors\' work.<br />（這位香港裁縫師作品的品質，與Savile Row裁縫師的品質相當。）<br /> <br />The company can plan a corporate event that tailors to your needs. <br />（那家公司可以根據你的需求客製設計公司活動。）<br /> <br />起初Eggsy是一個較為沒有品味文化的年輕人，因此可以用「<span style=""color: #a42d00;"">unrefined</span>」這個字來形容。<span style=""color: #a42d00;"">refined </span>(adj)是精緻的、細緻的，例句：Jane has become more refined in her oil painting techniques.（Jane的油畫技巧越來越精熟細緻。）加上un表示否定的字首，即為「不優雅的、未精製的」。<br />※refine (v) 精緻化、優化、提升、提煉<br /> <br />與unrefined同義的字有<span style=""color: #a42d00;"">crude</span> (adj) 粗俗、粗鄙、原始的（未提煉），可用來形容人或事物。例如，每日國際原油交易上常聽到的布蘭特原油就是「crude oil」。我們所用的汽油都是提煉過的，這些汽油是從煉油廠（refinery）而來。<br /> <br />至於熱映中的《機密對決》裡，Kingsman已經跳脫穿著品味，更強調任務本身。<br /> <br /><span style=""color: #5cacee;"">Being a kingsman is more than the clothing we wear，or the weapons we bear; it\'s about the willingness to sacrifice for the greater good.</span><br />（身為金牌特務的重點不在於我們穿的服裝或拿的武器，而是我們願意為崇高目標犧牲的理念。）<br /> <br /><span style=""color: #a42d00;"">bear</span>在英文的意思有多種意思，在此是動詞，做「配戴、攜帶、裝備」解釋；另一個重要意思是「承受、忍耐」。<br />例句：The pain was so unbearable that he had to take pain relievers for 2 weeks.（那疼痛實在讓他無法忍受，所以他必須連吃兩個星期的止痛藥。）</p>
<p><strong><span style=""color: #0000ff;"">英式英語 VS. 美式英語</span></strong><br />英式、美式英文最明顯的區別即是在單字上。對英文為母語的人來說，常常可由上下文判斷出單字的意思。雖然多數讀者較熟悉美式英文，但只需要學習常見的英式英語用詞，就不會出現聽不懂的狀況囉！以下列出常見英式、美式英語常見的不同：</p>
<p><img title="""" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59d73b4268b34-KioT3nOSRCGPsoWsefZaGeQOCO8ANC-300x180.jpeg"" alt="""" width=""300"" height=""180"" /></p>
<p><span style=""color: #a42d00;"">queue</span>可以當動詞「排隊」或名詞「隊伍」。<br />People queued up for hours in order to get the tickets to the concert. <br />（很多人排了好幾個小時只為了買演唱會的票。）</p>
<p>The taxi stand is over there. Let\'s get in the queue. <br />（計程車招呼站在那裡，我們去排隊吧。）<br /> <br /><span style=""color: #a42d00;"">lift</span>有眾多意思，需由上下文來判斷。常用的意思，有以下幾種：<br /><span style=""color: #a42d00;"">電梯（英式）</span><br />To go to the observatory deck，take the lift over there. <br />（到觀景台要搭那邊的電梯。）<br /> <br /><span style=""color: #a42d00;"">搭載（名詞，等同a ride）</span><br />My car broke down. Can you give me a lift to work tomorrow?<br />（我的車拋錨了，你明天可以載我去公司嗎？）<br /> <br /><span style=""color: #a42d00;"">取消、移除（動詞）</span><br />The judges decided to lift the controversial travel ban. <br />（法官們決定取消那個有爭議的旅行禁令。）<br /> <br /><span style=""color: #a42d00;"">抬起、舉起（動詞）</span><br />Can you lift those boxes for me? They are too heavy. <br />（可以幫我把那些箱子抬起來嗎？他們太重了。）<br /> <br />英式、美式用語的差異有時會讓人混淆，有時候卻有著嘲諷意味，來看一段電影台詞：<br /><span style=""color: #5cacee;"">Eggsy：You\'ve got the brains，skills，and &hellip; skipping ropes.<br />Agent Whiskey: It\'s a lasso!<br />Eggsy: Whatever.<br /> <br />Eggsy：你有腦子、技能，和&hellip;跳繩。<br />Agent Whiskey：是「套索」！<br />Eggsy：都可以啦！<br /> </span><br /><strong><span style=""color: #0000ff;"">小試身手</span></strong><br />Our R&amp;D team has spent a lot of time __________ the design of the product.<br />(A) refunding<br />(B) fining<br />(C) refining<br />(D) rekindling <br /> <br /><strong>解析：</strong><br />正確答案(C)改善；優化。本題為單字題，需看語意選出最符合的單字。選項(A)退款、(B)罰鍰、(D)重新點燃，皆與題意不符。<br />題意「我們研發團隊花很多時間在優化產品的設計。」</p>
<p><br /> <br />Because of the acclaimed _____________ customer service they provide，Smith &amp; Lee<br />Company has won the nation\'s best customer service for 3 years in a row.<br />(A) personifying <br />(B) personified<br />(C) personification <br />(D) personalized<br /> <br /><strong>解析：</strong><br />正確答案(D)個人化。本題為文法兼單字題，題目中in a row表示「連續的」，acclaimed為「稱許」。由於custom service（客服）是「被客製化」，所以必須選擇有被動含義的動詞分詞。personify (v)為「擬人化」，選項(A)是進行式、(B)過去式、(C)名詞形式，與題意不符不選。選項(D) 的personalize為「使個人化」，又是被動語態，故為正解。</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>
<p>photo: KINGSMAN: THE GOLDEN CIRCLE</p>"','published' => '1','og_title' => '英、美式英語總是搞混？讓「金牌特務」來教你','og_description' => '在生活、工作中或多或少會接觸到英式英語和美式英語，而這也是多益測驗的考點之一！不妨跟著Kingsman一起學常用英、美式英語吧！','meta_title' => '英、美式英語總是搞混？讓「金牌特務」來教你','meta_description' => '在生活、工作中或多或少會接觸到英式英語和美式英語，而這也是多益測驗的考點之一！不妨跟著Kingsman一起學常用英、美式英語吧！','canonical_url' => 'https://tw.english.agency/英-美式英語總是搞混','feature_image_alt' => '金牌特務','feature_image_title' => '金牌特務','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59d73eccec1ba-wkvFvH2vvmj6Y6ZxbwLH2XdNCzxbVC-640x426.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2017-10-06 08:29:39"'],
  ['id' => '65','title' => '"想用Face ID解鎖i8 先學會這2個英文字"','slug' => '想用FaceID解鎖i8-先學會這2個英文字','excerpt' => '"蘋果新機以iphone X新增的人臉辨識系統（Face ID）引起大眾熱烈討論。藉由這個超夯議題學相關英文，讓你走在時代尖端，用英文聊高科技吧！"','content' => '"<p>蘋果新機iphone 8、iphone 8 plus和iphone X再度掀起熱潮，其中又以iphone X新增的人臉辨識系統（Face ID）引起大眾熱烈討論。這功能取代過去的指紋辨識系統（Touch ID）用臉部來解鎖，卻引來正反兩極的反應。今天我們藉由這個超夯議題學相關英文，讓你走在時代尖端，用英文聊高科技，而且還能用在多益測驗呢！</p>
<p><strong><span style=""color: #0000ff;"">Facial Identity人臉辨識</span></strong></p>
<p><span style=""color: #5cacee;"">Face ID is Apple&rsquo;s new facial identity scanner. It replaces Touch ID，Apple&rsquo;s fingerprint identity scanner，on the next generation iPhones X.</span><br />（人臉辨識系統是蘋果新的臉部辨識掃描功能。在新一代的iPhones X中，它取代了觸控辨識功能，也就是蘋果的指紋辨識掃描功能。）</p>
<p>Facial來自face這個單字，詞性從字尾「-ial」可判斷為形容詞。一般說的facial expression指的是臉部表情，express當動詞作「表達」之意，expression是它的名詞變化。另外，expressive為「-sive」形容詞變化，意思為表達能力佳的，這是英文字彙常見的字尾變化，值得一記。</p>
<p>接著談到identity，這個單字來自動詞identify（辨認）。identity本身有身份的意思，如 false identity（錯誤的身份），另外有種犯罪行為叫做identity theft，就是竊取他人身份進行盜刷等的行為。</p>
<p>Theft是竊盜的意思，讀者應該熟悉它的另一個名詞變化thief（小偷），此單字為不規則名詞變化，複數為thieves。雖然我們說不規則卻還是有跡可循，若不規則名詞變化字尾為「-f」或「-fe」，通常都是變成「-ves」，其他常見變化為knife-knives、wife-wives等。</p>
<p>Identity還有一個常見的名詞變化為identification（辨認），有時作「認同」之意。我們常說的ID身分證，其實多指的是identification card。兩個名詞變化字尾都是常見的變化，「-ty」和「-tion」同樣可以幫助你判斷詞性。形容詞變化為identifiable，able本身就是一個常見形容詞，表「能夠&hellip;」，而它放在單字後面就變成一個常見的形容詞字尾。</p>
<p><strong><span style=""color: #a42d00;"">例句：</span></strong><br />You can easily identify Tom because he is very tall.（因為湯姆很高，你可以輕易地辨認出他。）</p>
<p>In the absence of specific evidence，any such identification must be regarded with<br />suspicion.（在沒有任何證據的情況之下，任何的辨識都會被認定為可疑的。）</p>
<p>The actor wears glasses and a hat to conceal his identity.（那名演員戴著眼鏡和帽子來掩飾他的身份。）</p>
<p>Her voice is easily identifiable.（她的聲音非常好辨認的。）</p>
<p>藉由例句的使用方法，放清楚了解單字的字義與使用方法，這是作者個人認為最好的學習單字的方法之一，以免落於單字量很多卻無法確實使用而失去了學習的意義。</p>
<p><strong><span style=""color: #0000ff;"">是Recognize還是Identify？</span></strong></p>
<p>雖然人臉辨識這項新技術十分吸睛，但是在發表會上，Face ID卻「突槌」沒有認出蘋果軟體工程高階副總裁Craig Federighi的臉，在全世界面前解鎖失敗。英國衛報這麼報導：<br /><span style=""color: #5cacee;"">Many assumed that the problem was that Face ID system failed to recognize Federighi.</span><br />（許多人認為，這個問題來自於人臉辨識系統無法認出Federighi。）</p>
<p>特別注意recognize與identify這兩個單字的用法。Recognize著重在「認出來」，名詞變化為recognition（認可），形容詞recognizable（能夠認出來的）。直接看例句更清楚：</p>
<p><strong><span style=""color: #a42d00;"">例句：</span></strong><br />I didn&rsquo;t recognize you at first with your new haircut.（你換了新髮型，一開始我還認不出你來。）</p>
<p>The procedure is gaining recognition as the latest advance in organ transplant surgery. （這個新手術正獲認可為器官移植手術的最先進技術發展。）</p>
<p>She was easily recognizable，her dark hair streaming in the breeze of her partially open window.（她很好認，她的頭髮在半掩的門窗前因微風而飄動著。）</p>
<p>從這些例句中，讀者是否發現原來identifiable和recognizable在某些情況下的用法是一樣的，因此在了解單字本身的意思後，藉由前後文來判斷才是能夠得到單字最真實的用法語意的學習方法喔！</p>
<p><strong><span style=""color: #0000ff;"">多益模擬試題</span></strong></p>
<p>All right，let&rsquo;s get started. We&rsquo;ve been tasked with a job of monumental proportions. Let me begin by reminding you that we&rsquo;ve got over 2000 drug companies and 150000 food processing companies，not to mention thousands of small exporters.<br />And in recent months，a few of the，shall we say，&ldquo;less responsible&rdquo; ones have failed to live up to good practices. Recalls of their dangerous products have undermined our reputation in the international market. Somehow，this team has to bring this giant under control and to do that immediately. We&rsquo;ll start with a thorough review of product standards to identify which ones need to be tightened. Then，after developing recommendations for inspection and enforcement，members of this task force will directly supervise execution of the plan. I can assure you，we will have a free hand and full backing of the governing council. Improvement of our image and continued economic growth are foremost in their minds. You&rsquo;ll find my action outline in the folders you&rsquo;ve been given.</p>
<p>1. What is the speaker&rsquo;s position? <br />(A) A member of the government.<br />(B) Executive of an exporting firm.<br />(C) Head of a government taskforce.<br />(D) Owner of a food processing company.</p>
<p>2. Why has this working group been created? <br />(A) To arrest shady businessmen.<br />(B) To close offending businesses.<br />(C) To cover up company errors.<br />(D) To improve standards and compliance.</p>
<p><strong><span style=""color: #a42d00;"">解析：</span></strong><br />1. 正確答案(C)。題目問「說話者的職位為何？」從其使用的句子after developing recommendations for inspection and enforcement，members of this taskforce will directly supervise execution of the plan（在發展出檢驗和執法的建議後，這個特別小組的成員將會直接監督計畫的執行。）這種強勢的語氣，加上使用執行、檢驗與執法等辭彙，我們可以猜測這是類似政府部門的一個部門。<br />而強烈的語氣可看出說話者應該為領導職。We will have a free hand and full backing of the governing council（我們將獲得絕大的自由，及政府議會的全面支持。）這句話更加深了前面對於此為政府方面部門的另一個有力支持，因此答案為(C)。<br />選項(A)並沒有完全點出說話者的身份為一個小組的領導，(B)和(D)都是斷章取義，只是剛好有提到export或food processing，故不選。</p>
<p>2. 題目問「這個特別工作小組為何成立？」這題答案很明顯在說話者所說We&rsquo;ll start with a thorough review of product standards to identify which ones need to be tightened.（我們將會開始徹底地檢驗產品標準來辨認出哪些需要加強管理。）加上前面提到a few of the&ldquo;less responsible&rdquo;ones have failed to live up to good practices（部分較無責任感的公司無法達到好的管理職責）和Recalls of their dangerous products have undermined our reputation in the international market（他們危險產品的召回已經損壞我們在國際市場的名譽），都顯示這個小組成立是為了選項(D)改進標準和促進守法一責。文中並沒有提到要逮捕或是關閉任何企業公司，所以(A)和(B)都不能選。(C) cover up掩蓋事實，與文意完全不符故不選。</p>
<p>文／Buffy Kao</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>
<p>photo: Mark Mathosian，CC licensed</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"想用Face ID解鎖i8 先學會這2個英文字"','og_description' => '"蘋果新機以iphone X新增的人臉辨識系統（Face ID）引起大眾熱烈討論。藉由這個超夯議題學相關英文，讓你走在時代尖端，用英文聊高科技吧！"','meta_title' => '"想用Face ID解鎖i8 先學會這2個英文字"','meta_description' => '"蘋果新機以iphone X新增的人臉辨識系統（Face ID）引起大眾熱烈討論。藉由這個超夯議題學相關英文，讓你走在時代尖端，用英文聊高科技吧！"','canonical_url' => 'http://www.toeicok.com.tw/想用face-id解鎖i8-先學會這2個英文字/','feature_image_alt' => '解鎖i8','feature_image_title' => '解鎖i8','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59e03a92ea62b-HpNjC7HH9SX5JeqxhuhQOQ8HYfxUre-640x386.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2017-10-13 04:02:02"'],
  ['id' => '66','title' => '如何說出一口流利的「會議英語」','slug' => '會議英語','excerpt' => '"不論是想海外派遣、進入外商公司的你，只要戰勝舞台恐懼症(stage fright)，練習開口說會議英語，不僅讓老闆對你印象深刻，更在職場上無往不利。快來記下這些句子，讓你變身英語達人吧！"','content' => '"<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><strong><span style=""color: #35478c;"">不論是想海外派遣、進入外商公司的你，只要戰勝舞台恐懼症</span></strong><span style=""color: #35478c;"">(stage fright)</span><strong><span style=""color: #35478c;"">，練習開口說會議英語，不僅讓老闆對你印象深刻，更在職場上無往不利。快來記下這些句子，讓你變身英語達人吧！</span></strong></div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>A.會議開始</strong></span>&nbsp;</div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Shall we get started?</span></strong></div>
<h4>會議可以開始了嗎？</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">If we are all here，let&rsquo;s get started.</span></strong></div>
<h4>大家都到齊的話，我們開始吧。</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Has everyone read the minutes of the last meeting?</span></strong></div>
<h4>各位有讀過上次的會議紀錄了嗎？</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Can you take minutes/notes?</span></strong></div>
<h4>你能幫忙寫會議紀錄嗎？</h4>
<h4><strong>&nbsp;</strong></h4>
<p>&nbsp;</p>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>B.開場問候：除了問候感謝，若有新進同事也可以這樣開場</strong></span>&nbsp;</div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Good morning / Good afternoon，ladies and gentlemen.</span></strong></div>
<h4>早安/午安，先生女士。</h4>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">I&rsquo;d like to thank you for coming here today.</span></strong></div>
<h4>我想感謝各位出席會議。</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">First of all，I&rsquo;d like you to please join me in welcoming Anna Lamar.</span></strong></div>
<h4>首先，我想讓各位一起歡迎安娜‧拉瑪。</h4>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Please join me in welcoming Anna Lamar to our team.</span></strong></div>
<h4>一起歡迎安娜加入我們的團隊。</h4>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>C.自我介紹：介紹自己以及簡報的大鋼</strong></span></div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">I am Jenny Lee.Today I&rsquo;m going to give a presentation on the effects of the economic crisis on car sales.</span></strong></div>
<h4>我是珍妮‧李，今天我要報告經濟危機對汽車銷售的影響。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">I&rsquo;m Jenny Lee.Today my topic is the effects of the economic crisis on car sales.</span></strong></div>
<h4>我是珍妮‧李，今天我的簡報主題是經濟危機對汽車銷售的影響。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">I&rsquo;d like to give a brief presentation of the products.</span></strong></div>
<h4>我想簡單介紹這項產品。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">My presentation is in three parts，A，B and C.</span></strong></div>
<h4>我的報告將分成三個部分。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">My presentation will cover&hellip;</span></strong></div>
<h4>我的報告將包含...</h4>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong><img title=""一口流利的會議英文"" src=""http://www.atoolye.com/wp-content/uploads/2017/04/sld1.jpg"" alt="""" width=""620"" height=""349"" /></strong></span></div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>D.解釋：訓練自己有技巧、邏輯性背下要分享的內容，並將內容分成</strong><strong>First，Second，And Last</strong></span>&nbsp;</div>
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><strong><span style=""color: #f28f00;"">According to the chart，you will see the sales in the US market are very low. This is attributed to the economic crisis. I suggest we take measures to deal with it.</span></strong></div>
<h4>根據這張圖表，你會發現最近在美國市場銷售量很低，這是因為經濟危機造成的。因此我建議採取措施解決它。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">First，let&rsquo;s look at the chart. <u>You can see</u> the sales figures had been fluctuating since last January.</span></strong></div>
<h4>首先讓我們看這張圖表，你會發現銷售額自去年一月便保持著波動狀態。</h4>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><span style=""color: #304269;""><strong>你也可以說：</strong></span></div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Let&rsquo;s look at the chart，<u>it shows</u> that we suffered a downturn in sales.</span></strong></div>
<h4>看看這張圖表，它顯示了我們銷售呈現低迷狀態。</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Our products make up 35% of the market in Taiwan.</span></strong></div>
<h4>我們的產品在台灣市佔率有35%。</h4>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>E.詢問意見：</strong></span>&nbsp;</div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">I&rsquo;d like to get some ideas from you all.</span></strong></div>
<h4>我想聽聽大家的想法。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">How does that sound to you?</span></strong></div>
<h4>你覺得如何？</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">How do you feel about this project?</span></strong></div>
<h4>你認為這個企劃如何？</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">What do you think about this plan?</span></strong></div>
<h4>你對這計劃有何想法？</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">To discuss this issue，I&rsquo;d like to call on Tom.</span></strong></div>
<h4>談到這個議題，我想請湯姆來發表他的想法。</h4>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>F.如何轉場：若今天會議有許多議程要進行，不妨利用這幾句，讓會議更有效率</strong></span>&nbsp;</div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Let&rsquo;s move on to &hellip;</span></strong></div>
<h4>讓我們把主題轉移到&hellip;</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Now I&rsquo;d like to turn to &hellip;</span></strong></div>
<h4>現在我想談論&hellip;</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Now let&rsquo;s look at another chart.</span></strong></div>
<h4>讓我們看下一張圖吧。</h4>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<hr />
<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><img title=""輕鬆練習會議英文"" src=""https://hrsoft.com/wp-content/uploads/2016/06/bigstock-Business-Meeting-Flat-Illustra-62780707.jpg"" alt="""" width=""665"" height=""414"" />&nbsp;</div>
<div style=""font-size: 18px;"">&nbsp;&nbsp;</div>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>G.進行問答：若你怕問題中斷了報告，可以試試下面這句</strong></span></div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">There will be time for questions at the end of the presentation.</span></strong></div>
<h4>報告結束後，將進行問答</h4>
<h4>&nbsp;</h4>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Please feel free to interrupt if you have any questions.</span></strong></div>
<h4>若有問題，歡迎隨時詢問</h4>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>H.結尾：</strong></span>&nbsp;</div>
<div style=""font-size: 16px;"">&nbsp;</div>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Let me summarize what we have talked about today.</span></strong></div>
<h4>讓我為今天的會議做總結。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><strong><span style=""color: #f28f00;"">Here are the key points of today&rsquo;s talk.</span></strong></strong></div>
<h4>這些就是今日會議的重點。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">In the meeting we have discussed &hellip;</span></strong></div>
<h4>在會議中，我們討論了&hellip;</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Can we fix the next meeting?</span></strong></div>
<h4>請問下次會議安排在何時？</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Can we fix a meeting next Monday at ten o&rsquo;clock?</span></strong></div>
<h4>能將會議訂在下周一的十點嗎？</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Thank you all for attending.</span></strong></div>
<h4>感謝各位的參與。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">Thanks for your participation.</span></strong></div>
<h4>謝謝您的參與。</h4>
<h3>&nbsp;</h3>
<div style=""font-size: 16px;""><strong><span style=""color: #f28f00;"">I&rsquo;d like to thank everyone for coming today.</span></strong></div>
<h4>我想感謝各位前來參與。</h4>
<h3>&nbsp;</h3>
<p>&nbsp;</p>
<div style=""font-size: 18px;""><span style=""color: #304269;""><strong>I.熟習字彙：</strong></span>&nbsp;</div>
<h4>&nbsp;</h4>
<h4><span style=""color: #468966;"">Meeting</span> 會議 (通常較不正式)</h4>
<h4><span style=""color: #468966;"">Conference</span> 會議 (較正式，可能有公司以外的人參加會議)</h4>
<h4><span style=""color: #468966;"">Meeting room</span> 會議室</h4>
<h4><span style=""color: #468966;"">Meeting minutes</span> 會議記錄</h4>
<h4><span style=""color: #468966;"">Take minutes</span> 寫會議記錄</h4>
<h4><span style=""color: #468966;"">Agenda </span> 會議議程</h4>
<h4><span style=""color: #468966;"">Slide </span> 幻燈片</h4>
<h4><span style=""color: #468966;"">Chart </span> 圖表 (以柱狀、圓餅圖呈現)</h4>
<h4><span style=""color: #468966;"">Graph</span> 圖表 (曲線圖呈現)</h4>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '如何說出一口流利的「會議英語」','og_description' => '"不論是想海外派遣、進入外商公司的你，只要戰勝舞台恐懼症(stage fright)，練習開口說會議英語，不僅讓老闆對你印象深刻，更在職場上無往不利。快來記下這些句子，讓你變身英語達人吧！"','meta_title' => '如何說出一口流利的「會議英語」','meta_description' => '"不論是想海外派遣、進入外商公司的你，只要戰勝舞台恐懼症(stage fright)，練習開口說會議英語，不僅讓老闆對你印象深刻，更在職場上無往不利。快來記下這些句子，讓你變身英語達人吧！"','canonical_url' => 'https://tw.english.agency/會議英語','feature_image_alt' => '如何說出一口流利的「會議英語」','feature_image_title' => '如何說出一口流利的「會議英語」','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59f176f9deee0-ypQMTNR1upBVOEjfeUYDDJWs2vFLwa-1200x630.jpeg','author_id' => '1','category_id' => '','created_at' => '"2017-10-26 03:11:08"'],
  ['id' => '67','title' => '職場英文│各類職稱怎麼說？','slug' => '職場英文會話','excerpt' => '"上班一條蟲，下班一條龍！上班族生活就像英國影集《The Office》一樣，辦公室裡永遠有說不完的八卦，做不完的瑣事還得看慣老闆臉色！讓我們學好職場英文為自己找到更讚的工作，快來看看你是哪一個部門吧！"','content' => '"<div style=""font-size: 18px;"">&nbsp;</div>
<div style=""font-size: 18px;""><strong><span style=""color: #35478c;"">上班一條蟲，下班一條龍！上班族生活就像英國影集《</span></strong><span style=""color: #35478c;"">The Office</span><strong><span style=""color: #35478c;"">》一樣，辦公室裡永遠有說不完的八卦，做不完的瑣事還得看慣老闆臉色！讓我們學好職場英文為自己找到更讚的工作，快來看看你是哪一個部門吧！</span></strong></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Accounting 會計</span></strong></div>
<p>Accountant 會計人員</p>
<p>Bookkeeper 記帳人員</p>
<p>Budget analyst 預算分析師</p>
<p>Financial analyst 財務分析師</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Finance 財務</span></strong></div>
<p>CFO (Chief Financial Officer) 財務長</p>
<p>FC (Financial controller) 財務主任</p>
<p>Financial Manager 財務經理</p>
<p>Financial clerk 財務管理員</p>
<p>Financial planner 理財規劃顧問</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Administrative 行政</span></strong></div>
<p>CEO (Chief Executive Officer)執行長</p>
<p>Secretary 秘書</p>
<p>Administrator 行政人員</p>
<p>Consultant 顧問</p>
<p>Typist 打字員</p>
<p>File clerk 檔案管理員</p>
<p>Office manager 辦公室主任</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">IT and Development</span></strong></div>
<p>Project manager專案經理</p>
<p>Product manager 產品經理</p>
<p>iOS developer iOS工程師</p>
<p>Java developer Java工程師</p>
<p>.Net developer .NET工程師</p>
<p>Front-end developer 前端工程師</p>
<p>Back-end developer 後端工程師</p>
<p>Data analyst 資料分析師</p>
<p>Data architect 資料架構師</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Design 設計</span></strong></div>
<p>Graphic designer 平面設計師</p>
<p>Web designer 網頁設計師</p>
<p>Illustrator 插畫家</p>
<p>Visual designer 視覺傳達設計師</p>
<p>Interior designer 室內設計師</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Sales 銷售</span></strong></div>
<p>Sales manager 銷售部經理</p>
<p>Sales executive 行銷專員</p>
<p>Sales assistant 行銷助理</p>
<p>Sales engineer 銷售工程師</p>
<p>Sales director 行銷經理</p>
<p>Telemarketer 電話行銷員</p>
<p>Key account manager 大客戶業務經理</p>
<p>Account manager 客戶經理</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Customer service 客服</span></strong></div>
<p>Receptionist 接待員</p>
<p>Operator 接線員</p>
<p>Dispatcher 接線員</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Construction 建築</span></strong></div>
<p>Architect 建築師</p>
<p>Construction manager 營建經理</p>
<p>Construction foreman 營建領班</p>
<p>Welder 焊工</p>
<p>Plumber 水管工人</p>
<p>Electrician 電工</p>
<p>Carpenter 木匠</p>
<p>Painter 油漆工</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Hospitality 餐旅管理</span></strong></div>
<p>Barista 咖啡店店員</p>
<p>Executive chef 行政主廚</p>
<p>Chef 主廚</p>
<p>Cook 廚師</p>
<p>Line cook/Chef de partie二廚</p>
<p>Pastry cook 點心師傅</p>
<p>Room attendant 房務員</p>
<p>Porter行李員</p>
<p>Housekeeper 管家</p>
<p>Wait staff (全體)服務生</p>
<p>Waiter/Waitress 男服務生/女服務生</p>
<p>Bartender 酒保</p>
<p>Host 接待員</p>
<p>Dishwasher 洗碗員</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Human Resources (HR) 人資</span></strong></div>
<p>HR assistant 人資助理</p>
<p>HR specialist 人資專員</p>
<p>Staffing Coordinator 人事協調員</p>
<p>&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Marketing 行銷</span></strong></div>
<p>Marketing assistant 行銷助理</p>
<p>Marketing executive 行銷專員</p>
<p>Art director 藝術指導</p>
<p>Creative director 創意總監</p>
<p>SEO manager SEO經理，SEO是指「搜尋引擎最佳化(Search Engine Optimization)」</p>
<p>SEM specialist SEM專員，SEM即為「搜尋引擎行銷(Search Engine Marketing)」</p>
<p>&nbsp;&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">Tourism 旅遊業</span></strong></div>
<p>Travel agent 旅遊專員</p>
<p>Tour guide 導遊</p>
<p>Cabin crew (全體)機艙人員、機組人員</p>
<p>Flight attendant 空服員</p>
<p>Ground staff 地勤人員</p>
<p>Flight dispatcher (航空)簽派員</p>
<p>Pilot 機師</p>
<p>Steward 服務員(飛機、船艙、火車等等)</p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<p><img title=""職場英文怎麼說"" src=""http://img.mp.sohu.com/upload/20170811/0a865ddc56df4f37876a2dab68b2b49e_th.png"" alt="""" width=""622"" height=""301"" />&nbsp;</p>
<div style=""font-size: 16px;""><strong><span style=""color: #35478c;"">海外徵才注意事項</span></strong>&nbsp;</div>
<p>若想在海外工作，首先要會看懂徵才廣告的內容，才不會傻傻上當！</p>
<p>&nbsp;</p>
<div style=""font-size: 15px;""><strong><span style=""color: #f28f00;"">Job Ads (Job Advertisement) </span></strong></div>
<p>徵才廣告</p>
<p>&nbsp;</p>
<div style=""font-size: 15px;""><strong><span style=""color: #f28f00;"">Applicant </span></strong></div>
<p>求職者(Applicant還可用在申請入學)</p>
<p>&nbsp;</p>
<div style=""font-size: 15px;""><strong><span style=""color: #f28f00;"">High-end rest.looking for evening dishwasher. Eligible applicants only.</span></strong></div>
<p>高檔餐廳徵求晚班洗碗員，限符合資格者。</p>
<p>&nbsp;</p>
<div style=""font-size: 15px;""><strong><span style=""color: #f28f00;"">FT/PT server wanted for busy pub in downtown. T.O.</span></strong></div>
<p>多倫多市中心酒吧徵求全職/兼職服務生。</p>
<p>（FT 是指Full time，所以PT就是Part time）</p>
<p>&nbsp;</p>
<div style=""font-size: 15px;""><strong><span style=""color: #f28f00;"">Must have serving exp and full avail. Min wage+tips.</span></strong></div>
<p>必須擁有服務經驗，可配合時間。薪資：基本薪資+小費。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '職場英文│各類職稱怎麼說？','og_description' => '"上班一條蟲，下班一條龍！上班族生活就像英國影集《The Office》一樣，辦公室裡永遠有說不完的八卦，做不完的瑣事還得看慣老闆臉色！讓我們學好職場英文為自己找到更讚的工作，快來看看你是哪一個部門吧！"','meta_title' => '職場英文│各類職稱怎麼說？','meta_description' => '"上班一條蟲，下班一條龍！上班族生活就像英國影集《The Office》一樣，辦公室裡永遠有說不完的八卦，做不完的瑣事還得看慣老闆臉色！讓我們學好職場英文為自己找到更讚的工作，快來看看你是哪一個部門吧！"','canonical_url' => 'https://tw.english.agency/職場英文會話','feature_image_alt' => '職場英文│各類職稱怎麼說？','feature_image_title' => '職場英文│各類職稱怎麼說？','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59f1970693673-vcJ6UYGIv5IDX0tDvPbVCiYmvdw8nj-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2017-10-26 06:07:41"'],
  ['id' => '68','title' => '出國不怕訂錯房！必學這兩個多益單字','slug' => '出國不怕訂錯房-必學這兩個多益單字','excerpt' => '前往英語系或非英語系國家旅遊乃至洽公，都可能會要自行訂房間、機位。看懂網站上所提供的相關資訊就很重要！我們一起來學自行訂房時會碰到的多益單字吧！','content' => '"<p>【文／Buffy Kao】</p>
<p>近年來有越來越多人當起背包客、沙發客享受自助旅行。然而，不論你是前往英語系或非英語系國家旅遊乃至洽公，都可能會要自行訂房間、機位。此時，看懂網站上所提供的相關資訊就相當重要了！各位旅客、經常奔波各地的商務人士，我們一起來學自行訂房時會碰到的多益單字吧！</p>
<p><strong><span style=""color: #a42d00;"">1. accommodate 容納、住宿</span></strong><br />Accommodate動詞的意思是容納，因此延伸出來的意思是住宿，名詞變化為accommodation。</p>
<p>例句：<br /><span style=""color: #000080;"">The hotel can accommodate up to 300 guests per night.</span><br />（這間旅館一個晚上可以容納高達300位的旅客。）</p>
<p><span style=""color: #000080;"">They are very satisfied with the accommodation in Japan.</span><br />（他們對於日本住宿處感到很滿意。）</p>
<p>做容納之意時還可以使用<strong><span style=""color: #a42d00;"">hold</span></strong>這個字，常見於口語英文中，例如：The room is too small to hold so many guests.（這個房間太小，無法容納這麼多人。）</p>
<p>Accommodate除了用於住宿的情境之外，另一個多益常見用法為表達<span style=""color: #a42d00;"">空間容納人數</span>的情境，多用在描述會議廳可以容納多少人數開會。</p>
<p>例句：<br /><span style=""color: #000080;"">Conference A can accommodate about 100 people.</span><br />（A會議廳可以容納大概100人。）</p>
<p>除了動詞和名詞的使用之外，它的形容詞變化為<strong><span style=""color: #a42d00;"">accommodating（好客的）</span></strong>，是從容納客人的概念延伸而來，若是形容人<strong><span style=""color: #a42d00;"">「熱情招待」</span></strong>，可以這樣說：The accommodating host makes us feel at home.（那位熱情的主人讓我們感到賓至如歸。）</p>
<p><br /><strong><span style=""color: #a42d00;"">2. amenity 設備、設施</span></strong><br />Amenity指的是住宿地提供的一些額外設施服務，我們來看看一間Airbnb的頁面上，屋主列出房間內附設的家電用品清單：<br /><strong><span style=""color: #5cacee;"">Amenities</span></strong><br />Wireless Internet 無線網路 Washer 洗衣機<br />Iron 熨斗 Hangers衣架<br />Hair dryer吹風機 Essentials 必需品<br />Kitchen 廚房 Heating暖器設備<br />Indoor fireplace室內壁爐 Free parking on premises免費的停車位</p>
<p>這些都是常見的家電用品，值得一提的是<strong><span style=""color: #a42d00;"">washer</span></strong>還有一個常見的說法：<strong><span style=""color: #a42d00;"">washing machine</span></strong>，注意不要跟洗碗機<strong><span style=""color: #a42d00;"">dishwasher</span></strong>搞混！通常單獨用washer指的是洗衣機。</p>
<p>另外，屋主在網站上還寫著：<br /><span style=""color: #5cacee;"">Family/kid friendly Pets allowed<br />Suitable for events Laptop friendly workspace</span></p>
<p><br /><strong><span style=""color: #a42d00;"">family/kid friendly</span></strong>指的是室內環境很適合家庭活動或是孩童玩樂，用friendly表達對&hellip;很友善，也就是適合有小孩的家庭來租。</p>
<p><strong><span style=""color: #a42d00;"">Pets allowed</span></strong>指的是可以養寵物，allow是個常見的動詞，指「允許&hellip;」，所以如果看到N + allowed指的是允許前面提到的名詞，如credit card allowed是「可用信用卡」的意思。</p>
<p><strong><span style=""color: #a42d00;"">Suitable for</span></strong>指的是「適合&hellip;的」，這裡events指的是一些派對等的活動，主人的意思是場地夠大、也有戶外空間適合舉辦一些聚會活動。</p>
<p><strong><span style=""color: #a42d00;"">Laptop friendly workplace</span></strong>則是指適合筆電的工作空間。</p>
<p>除了上述提到的家電用品外，也可能在網頁上看到一些關於旅店附加設施的資訊，例如：</p>
<p><span style=""color: #5cacee;"">Pet friendly 適合帶寵物的<br />Restaurant 餐廳<br />Bar 酒吧<br />Golf 高爾夫球場<br />Pool 游泳池<br />Free WiFi 免費無線網路<br />Fitness Center 健身房<br />Laundry 洗衣間<br />Meeting Space 會議空間</span></p>
<p>不過有些飯店也會用<strong><span style=""color: #a42d00;"">facility</span></strong>這個單字來表達設施，如某間旅館在Meeting &amp; Event Facilities的選項下面描述：</p>
<p><span style=""color: #5cacee;"">Impeccable Style，Gracious Service （完美的風格，親切的服務）</span></p>
<p>Little America Hotel offers outstanding service，catering，and audio visual support paired with 25000 square feet of flexible event and meeting space that can be tailored to every occasion in Salt Lake City.<br />（小美國飯店附有25000平方英尺的場地，提供傑出的服務、外燴以及視聽支援設備，可舉辦任何的活動，也可做會議空間，適合鹽湖城的任何場合。）</p>
<p>&nbsp;</p>
<p><strong><span style=""color: #0000ff;"">多益模擬試題</span></strong><br />1.Did you make an accommodation at the hotel?</p>
<p>(A) No，I haven\'t made a report yet.<br />(B) I have lived there.<br />(C) Yes，for two nights.</p>
<p>2.When is Ms. Mary\'s retirement party held?<br />(A) Ms. Mary is holding it.<br />(B) On next Sunday.<br />(C) Yes，it\'s from Tom.</p>
<p><strong>解析</strong></p>
<p>1. 答案選(C)「是的，訂了兩晚」最合理。<br />這題聽力測驗主要考accommodation這個單字，題目問「你有沒有向那間旅館訂房？」這裡用make an accommodation表達訂房的概念，如此一來當然要回答有或沒有訂房，有的話還可以加上訂了多少天的訊息。<br />選項(A)「沒有，我還沒有打出報告來。」故意用make a report混淆，因為通常聽到和題目中一樣的單字容易讓人產生誤解；選項(B)「我以前住過那裡。」答非所問，故不選。</p>
<p>2. 答案選(B)。這題考兩個概念：retirement party指的是退休派對，hold這裡表達舉辦的意思，而不是容納，題目問「Ms. Mary的退休派對何時舉辦？」答案明顯要選(B)，給予一個明確的時間點。<br />這種W/H問句最重要的是抓疑問詞，這裡問的是When所以要選日期。選項(A)「Ms. Mary正握著它」故意混淆視聽不選；選項(C)「是的，這是Tom給的」與前後文不相關也不選。</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '出國不怕訂錯房！必學這兩個多益單字','og_description' => '前往英語系或非英語系國家旅遊乃至洽公，都可能會要自行訂房間、機位。看懂網站上所提供的相關資訊就很重要！我們一起來學自行訂房時會碰到的多益單字吧！','meta_title' => '出國不怕訂錯房！必學這兩個多益單字','meta_description' => '前往英語系或非英語系國家旅遊乃至洽公，都可能會要自行訂房間、機位。看懂網站上所提供的相關資訊就很重要！我們一起來學自行訂房時會碰到的多益單字吧！','canonical_url' => 'http://www.toeicok.com.tw/%E8%A1%8C%E5%89%8D%E6%BA%96%E5%82%99%E5%BE%88%E9%87%8D%E8%A6%81-%E5%AD%B8%E6%9C%83%E9%80%992%E5%80%8B%E5%A4%9A%E7%9B%8A%E5%96%AE%E5%AD%97%E8%AE%93%E4%BD%A0%E5%87%BA%E5%9C%8B%E4%B8%8D%E6%80%95/','feature_image_alt' => '出國訂房','feature_image_title' => '出國訂房','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59f31108f105c-pqmu8hcFoPV6MYYGIi0y3Tq6cqJex6-640x372.jpeg','author_id' => '9','category_id' => '10','created_at' => '"2017-10-27 11:10:29"'],
  ['id' => '69','title' => '2018多益新制上路，一起預測有哪些單字！','slug' => '多益新制-單字預測','excerpt' => '明年三月即將改制的多益，不但將增減題型中的題目數量，聽力對話中會出現三人對話，還會讓句子變得更口語、生活化(例如Gonna等字眼出現)。你怎麼能不看看小編整理的預測單字及文法呢？','content' => '"<div style=""font-size: 18px;"">
<p>&nbsp;</p>
<p>明年三月即將改制的多益，不但將增減題型中的題目數量，聽力對話中會出現三人對話，還會讓句子變得更口語、生活化(例如Gonna等字眼出現)。你怎麼能不看看小編整理的預測單字及文法呢？&nbsp;</p>
<p>&nbsp;<img title=""多益改制單字預測"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59fffe677f9ea-sxPCfMu2PgfXvUyF1dDz0iALxvSr1M-1038x690.png"" alt=""多益改制單字預測"" width=""640"" height=""425"" /></p>
<p>參考圖片:<a href=""https://www.google.com.tw/imgres?imgurl=https%253A%252F%252Fc.tadst.com%252Fgfx%252F750w%252Fread-a-book-day-fun.jpg%253F1&amp;imgrefurl=https%253A%252F%252Fwww.timeanddate.com%252Fholidays%252Ffun%252Fread-a-book-day&amp;docid=wdO1gTciYSwjXM&amp;tbnid=wYyEF3VfeDBJ9M%253A&amp;vet=10ahUKEwi-uMexjszWAhXIoJQKHSl-Cc4QMwh0KEYwRg..i&amp;w=750&amp;h=500&amp;bih=530&amp;biw=1093&amp;q=read&amp;ved=0ahUKEwi-uMexjszWAhXIoJQKHSl-Cc4QMwh0KEYwRg&amp;iact=mrc&amp;uact=8"">Time and date</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>1. Turnout (</strong><strong>出席</strong><strong>)</strong><strong>人數</strong></span></p>
<p>&nbsp;</p>
<p>There was a record-high turnout at the parade.</p>
<p>參加遊行人數創下歷史新高</p>
<p>&nbsp;</p>
<ul>
<li><span style=""color: #5599ff;"">Voter turnout 投票率</span></li>
</ul>
<p>&nbsp;</p>
<p>The figures show that voter turnout this year is higher than last year.</p>
<p>數字顯示今年投票率比去年高</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>2. Bring together </strong><strong>使聚集</strong></span></p>
<p>&nbsp;</p>
<p>Many of the colleagues were brought together for Andy&rsquo;s birthday party.</p>
<p>許多同事聚集一起為安迪的生日慶生</p>
<p>&nbsp;</p>
<p>This seminar brought together some of the professors of Linguistics.</p>
<p>這場研討會聚集一些語言學教授</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>3. Debut </strong><strong>首演</strong></span></p>
<p>&nbsp;</p>
<p>All the celebrities attended the debut of the show.</p>
<p>所有的名人都參加了這場表演的首演</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>4. Ensure / Assure / Insure </strong><strong>的差別</strong></span></p>
<p>&nbsp;</p>
<p><span style=""color: #855600;""><strong>Ensure&nbsp; + </strong><strong>事物</strong> &nbsp;</span></p>
<p>Ensure是指確保「某事」會發生</p>
<p>&nbsp;</p>
<p>We ensure that your goods will arrive on time and undamaged.</p>
<p>我們會確保您的貨物準時到達並毫髮無傷</p>
<p>&nbsp;</p>
<p>Please ensure that you keep the receipt as proof of purchase. You are required to present it to claim a warranty for your product.</p>
<p>請妥善保存收據，以證明你有購買該商品。要求保固修理時請攜帶收據。</p>
<p>&nbsp;</p>
<ul>
<li><span style=""color: #5599ff;"">Warranty period 保固期</span></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #855600;""><strong>Assure&nbsp; + </strong><strong>人</strong>&nbsp; &nbsp;</span></p>
<p>Assure是向「人」確保，與Ensure不同的是──Assure後面加人，Ensure加事物</p>
<p>&nbsp;</p>
<p>I assure you that we can still get on the plane.</p>
<p>我向你保證，我們上的了飛機</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>讓別人不要為我們擔心時，就可以說這句</p>
<ul>
<li><span style=""color: #5599ff;"">Rest assured 放心吧！</span></li>
</ul>
<p>&nbsp;</p>
<p><br /> <span style=""color: #855600;""><strong>Insure&nbsp; +&nbsp; </strong><strong>物品</strong> &nbsp;</span></p>
<p>Insure是指為(物、人)投保</p>
<p>&nbsp;</p>
<p>-Did you insure your car?</p>
<p>-你給車子投保了嗎？</p>
<p>&nbsp;</p>
<p>-Yes，it is insured.</p>
<p>-有，車子已經被投保</p>
<p>&nbsp;</p>
<p>Insure your home against the natural disasters.</p>
<p>以防天災，請替房子投保</p>
<p>&nbsp;</p>
<p>What does travel insurance cover?</p>
<p>請問旅遊險包含哪些服務？</p>
<p>&nbsp;</p>
<p>We should eat healthy to insure against diseases.</p>
<p>我們應該健康飲食，預防疾病發生</p>
<p>&nbsp;</p>
<p><span style=""color: #008080;"">Compensation 補償/賠償金</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>5. Gonna </strong><strong>將要</strong><strong> (= Going to)</strong></span></p>
<p>&nbsp;</p>
<p><span style=""color: #855600;""><strong>用法：</strong>人 + Be動詞 + gonna + 動作</span></p>
<p>&nbsp;</p>
<p>I think I&rsquo;m gonna go to sleep.</p>
<p>我想我要睡了&nbsp;</p>
<p>&nbsp;</p>
<p>Look at this present! She is gonna love you.</p>
<p>看看這禮物！她一定會愛死你的</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>6. Gotta </strong><strong>必須</strong><strong> (= Have got to)</strong></span></p>
<p>&nbsp;</p>
<p><span style=""color: #855600;""><strong>用法：</strong>人 + gotta + 動詞</span></p>
<p>&nbsp;</p>
<p>You gotta be kidding me.</p>
<p>你一定在開玩笑</p>
<p>&nbsp;</p>
<p>I gotta go.&nbsp;</p>
<p>我得走了 / 我得掛電話了</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>7. Lay off </strong><strong>解雇</strong></span></p>
<p>&nbsp;</p>
<p>Many companies lay off the workers because of the policy.</p>
<p>政策導致許多公司解雇員工</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>8. Give out </strong><strong>分發、用完</strong></span></p>
<p>&nbsp;</p>
<p>We gave out the presents to all the customers during the opening ceremony.</p>
<p>開幕當天我們就把所有給顧客的禮物發光了</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>After climbing five stories，my strength has given out.</p>
<p>爬了五層樓後，我已經筋疲力竭了</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>9. Commission </strong><strong>傭金</strong></span></p>
<p>&nbsp;</p>
<p>Hans gets a commission of 3% when he sells a car.</p>
<p>漢斯每賣一輛車，就能拿到3%的傭金</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>10. Magnify </strong><strong>擴大、放大</strong></span></p>
<p>&nbsp;</p>
<p>The microscope can magnify objects up to 1000 times.</p>
<p>顯微鏡可以將物體放大1000倍</p>
<p>&nbsp;</p>
<ul>
<li><span style=""color: #5599ff;"">Magnifying glass 放大鏡</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li><span style=""color: #5599ff;"">Magnification 放大 (名詞)</span></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""color: #007799;""><strong>11. Cut a deal </strong><strong>達成協議</strong></span></p>
<p>&nbsp;</p>
<p>After months of negotiating，they finally cut a deal.</p>
<p>經過數月的協商後，他們終於達成協議</p>
<p>&nbsp;</p>
<ul>
<li><span style=""color: #5599ff;"">Cut corners (做事)走捷徑、便宜行事</span></li>
</ul>
<p>&nbsp;</p>
<p>The manager cut corners to meet the needs of the client.</p>
<p>經理為了符合客戶需求而便宜行事</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '2018多益新制上路，一起預測有哪些單字！','og_description' => '明年三月即將改制的多益，不但將增減題型中的題目數量，聽力對話中會出現三人對話，還會讓句子變得更口語、生活化(例如Gonna等字眼出現)。你怎麼能不看看小編整理的預測單字及文法呢？','meta_title' => '2018多益新制上路，一起預測有哪些單字！','meta_description' => '明年三月即將改制的多益，不但將增減題型中的題目數量，聽力對話中會出現三人對話，還會讓句子變得更口語、生活化(例如Gonna等字眼出現)。你怎麼能不看看小編整理的預測單字及文法呢？','canonical_url' => 'https://tw.english.agency/多益新制-單字預測','feature_image_alt' => '多益新制上路，一起預測有哪些單字！','feature_image_title' => '多益新制上路，一起預測有哪些單字！','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/59fffdf92849e-J8rSLpoxTQdnVlxgGgFlq1m0OPbj34-1200x630.png','author_id' => '1','category_id' => '19','created_at' => '"2017-11-06 06:15:34"'],
  ['id' => '70','title' => '購物英文│一字領、運動鞋怎麼說？','slug' => 'shopping-in-english','excerpt' => '出國就是血拚季節，但要如何用英文說款式、顏色和試穿呢？出國購物靠這幾句，讓你輕鬆買！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<h2 style=""color: #666666;""><strong><span style=""color: #0088a8;"">購物對話</span></strong></h2>
<p style=""color: #666666;""><span style=""color: #ee7700;""><strong>出國就是血拚季節，但要如何用英文說款式、顏色和試穿呢？</strong></span></p>
<ol style=""color: #666666;"">
<li><em><strong>Shop assistant: Are you looking for anything in particular?</strong></em><br />店員：請問你在找什麼樣的衣服？</li>
<li><em><strong>Customer: Yes，I&rsquo;m looking for a shirt.&nbsp;</strong></em><br />顧客：對，我想要找襯衫</li>
<li><em><strong>Shop assistant: What size are you?</strong></em><br />店員：你的尺寸多少？</li>
<li><em><strong>Customer: I&rsquo;m a (size) small / a medium / a large / an extra large</strong></em><br />顧客：我是S號 / M號 / L號 / XL號</li>
<li><em><strong>Shop assistant: Let&rsquo;s see&hellip;how about this one?</strong></em><br />店員：我來找找&hellip;這件如何？</li>
<li><em><strong>Customer: I like it. Can I try it on?</strong></em><br />顧客：我很喜歡，請問能試穿嗎？</li>
<li><em><strong>Shop assistant: Yes，the fitting room is over there.</strong></em><br />店員：可以，試衣間在那邊</li>
</ol>
<p style=""color: #666666;"">&nbsp;</p>
<h2 style=""color: #666666;""><strong><span style=""color: #0088a8;"">對話常用</span></strong></h2>
<p style=""color: #666666;""><span style=""color: #ee7700;""><strong>出國購物靠這幾句，讓你輕鬆買！</strong></span></p>
<ol style=""color: #666666;"">
<li><strong><em>How can I help you?&nbsp;</em></strong><br />需要幫忙嗎？&nbsp;</li>
<li><strong><em>Is someone looking after you?</em>&nbsp;</strong><br />請問需要幫忙嗎？</li>
<li><strong><em>Excuse me，could you help me?</em>&nbsp;</strong><br />抱歉，請問能幫一下我嗎？</li>
<li><strong><em>Medium should be fine.</em>&nbsp;</strong><br />M號應該就可以</li>
<li><strong><em>Small，I think.</em>&nbsp;</strong><br />我想是S號</li>
<li><strong><em>In medium we got white，black and blue.</em>&nbsp;</strong><br />M號大小有白色、黑色和藍色的款式</li>
<li><strong><em>Do you have a plain shirt?&nbsp;</em></strong><br />請問你們有素色襯衫嗎？</li>
<li><strong><em>Do you have this T-shirt in black?</em></strong><br />請問這件T恤有黑色嗎？</li>
<li><strong><em>No，but we got it in blue，white，purple and green.</em></strong><br />沒有，但我們有藍色、白色、紫色、綠色的</li>
<li><strong><em>How do you like this?</em></strong><br />你覺得這件如何？&nbsp;</li>
<li><strong><em>I prefer the sporty style.</em></strong><br />我比較喜歡運動風格&nbsp;</li>
<li><strong><em>It&rsquo;s beautiful.</em></strong><br />很美</li>
<li><strong><em>I like the color.</em></strong><br />我喜歡這顏色</li>
<li><em><strong>I like the T-shirt，but it doesn&rsquo;t fit me.</strong></em><br />我喜歡這件T恤，但不合身</li>
<li><em><strong>It fits me perfectly.</strong></em><br />;非常合身</li>
<li><em><strong>The dress suits me well.</strong></em><br />這件洋裝很適合我</li>
Fit是指衣服「合身」，而Suit是指衣服「適不適合」人的風格。
<li><em><strong>Can I try it on?</strong></em><br />我能試穿嗎？</li>
<li><em><strong>Yes，you can try it on in a fitting room.</strong></em><br />可以，你可以去試衣間穿穿看。</li>
<li><em><strong>Do you have these in a smaller size?</strong></em><br />請問有沒有尺寸小點的？</li>
<li><em><strong>Is it OK?</strong></em><br />還可以嗎？</li>
<li><em><strong>How does it fit?</strong></em><br />請問合身嗎？</li>
<li><em><strong>I&rsquo;d prefer a blue one.</strong></em><br />我覺得藍色的會比較好</li>
<li><em><strong>Yes，I&rsquo;ll take it.</strong></em><br />很好，我決定買了</li>
<li><em><strong>How much is it?</strong></em><br />請問多少錢？</li>
<li><em><strong>It&rsquo;s &pound;20. (20 pounds)</strong></em><br />二十元英鎊&nbsp;</li>
<li><em><strong>It&rsquo;s $30. (30 dollars)&nbsp;</strong></em><br />三十美元</li>
<li><em><strong>Would you like to pay in cash or by credit card?</strong></em><br />請問要付現還是刷卡？&nbsp;</li>
<li><strong><em>Do you take credit cards?</em></strong><br />你們可以刷卡嗎？</li>
<li><em><strong>Cash，please.</strong></em><br />付現</li>
<li><em><strong>I&rsquo;ll pay by credit card.</strong></em><br />我刷信用卡</li>
<li><em><strong>Thank you. Have a nice day!</strong></em><br />謝謝你，祝你愉快</li>
</ol>
<p style=""color: #666666;"">&nbsp;</p>
<p style=""color: #666666;"">&nbsp;</p>
<p style=""color: #666666;""><img title=""出國購物英文怎麼說"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a001a0db975f-LASbVf9UB6v2Ic4qzP7dPjExkQJdgi-1038x688.png"" alt=""出國購物英文怎麼說"" width=""690"" height=""457"" /></p>
<p style=""color: #666666;"">圖片來源:<a href=""https://pixabay.com/p-791582/?no_redirect"">Pixabay</a></p>
<h2 style=""color: #666666;""><strong><span style=""color: #0088a8;"">衣服款式、風格</span></strong> <strong>&nbsp;</strong></h2>
<p style=""color: #666666;""><span style=""color: #ee7700;""><strong>衣服款式百百種，說得正確才能輕鬆找到想買的風格！</strong></span></p>
<h4 style=""color: #fa8072;""><strong>風格</strong></h4>
<ul style=""color: #666666;"">
<li><em>Trendy</em> 時髦、流行</li>
<li><em>Casual</em> 休閒</li>
<li><em>Elegant</em> 優雅</li>
<li><em>Exotic</em> 異國風</li>
<li><em>Punk</em> 龐克風</li>
<li><em>Vibrant</em> (色彩)鮮明的、有活力</li>
<li><em>Bohemian</em> 波西米亞風</li>
<li><em>Artsy</em> 藝術風</li>
<li><em>Sporty</em> 運動風</li>
</ul>
<p style=""color: #666666;"">&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>花紋款式</strong></h4>
<ul>
<li style=""color: #666666;""><em>Plain 素色</em></li>
<li style=""color: #666666;""><em>Spotted 斑點的</em></li>
<li style=""color: #666666;""><em>Striped 條紋的</em></li>
<li style=""color: #666666;""><em>Checked 格子的</em></li>
<li style=""color: #666666;""><em>Cropped 短版的</em></li>
<li style=""color: #666666;""><em>Ribbed 有稜紋的、坑條</em></li>
<li style=""color: #666666;""><em>U shaped 圓領的</em></li>
</ul>
<p style=""color: #666666;"">&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>衣服種類</strong></h4>
<ul>
<li style=""color: #666666;""><em>Dress 洋裝</em></li>
<li style=""color: #666666;""><em>Skirt裙子</em></li>
<li style=""color: #666666;""><em>Tank top 背心</em></li>
<li style=""color: #666666;""><em>Blouse 女裝襯衫</em></li>
<li style=""color: #666666;""><em>Sweater 毛衣</em></li>
<li style=""color: #666666;""><em>Knit sweater 編織毛衣</em></li>
<li style=""color: #666666;""><em>Jumper / Pullover 運動毛衣</em></li>
<li style=""color: #666666;""><em>Cardigan 開襟羊毛衫、外套</em></li>
<li style=""color: #666666;""><em>Off-the-shoulder top 一字領上衣</em></li>
</ul>
<p style=""color: #666666;"">&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>褲子種類</strong></h4>
<ul>
<li style=""color: #666666;""><em>Trousers 長褲</em></li>
<li style=""color: #666666;""><em>Shorts 短褲</em></li>
<li style=""color: #666666;""><em>Bermuda 百慕達短褲 (也就是五分褲)</em></li>
<li style=""color: #666666;""><em>Crop trousers 七分褲</em></li>
<li style=""color: #666666;""><em>Leggings 緊身褲</em></li>
<li style=""color: #666666;""><em>Wide leg pants 寬褲</em></li>
</ul>
<h4 style=""color: #fa8072;""><strong>其他（運動、居家）</strong></h4>
<ul>
<li style=""color: #666666;""><em>Tracksuit 運動套裝</em></li>
<li style=""color: #666666;""><em>Hoodie 連帽衫、帽T</em></li>
<li style=""color: #666666;""><em>Sweatshirt 毛衣、運動毛衣</em></li>
<li style=""color: #666666;""><em>Slippers (室內)拖鞋</em></li>
<li style=""color: #666666;""><em>Flip flops 人字拖</em></li>
<li style=""color: #666666;""><em>Sandals 涼鞋</em></li>
<li style=""color: #666666;""><em>Trainers / Sneakers 運動鞋</em></li>
<li style=""color: #666666;""><em>Cagoule 防風外套</em></li>
<li style=""color: #666666;""><em>Anorak 連帽外套、防寒大衣</em></li>
</ul>
<p style=""color: #666666;"">&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>更衣地點</strong></h4>
<ul>
<li style=""color: #666666;""><em>Fitting room 試衣間</em></li>
<li style=""color: #666666;""><em>Dressing room <br />試衣間、更衣室(指家中或演員可以更衣的地方)</em></li>
<li style=""color: #666666;""><em>Changing room (Locker room) <br />試衣間、更衣室(指運動後可以更衣的地方)</em></li>
</ul>
<p style=""color: #666666;"">&nbsp;</p>
</div>"','published' => '1','og_title' => '購物英文│一字領、運動鞋怎麼說？','og_description' => '出國就是血拚季節，但要如何用英文說款式、顏色和試穿呢？出國購物靠這幾句，讓你輕鬆買！','meta_title' => '購物英文│一字領、運動鞋怎麼說？','meta_description' => '出國就是血拚季節，但要如何用英文說款式、顏色和試穿呢？出國購物靠這幾句，讓你輕鬆買！','canonical_url' => 'https://tw.english.agency/shopping-in-english','feature_image_alt' => '購物英文│一字領、運動鞋怎麼說？','feature_image_title' => '購物英文│一字領、運動鞋怎麼說？','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a00198890c9b-kPzRMYutHhHp3EiY9jK3GpDbrALEgI-5001x2626.png','author_id' => '1','category_id' => '10','created_at' => '"2017-11-06 07:04:25"'],
  ['id' => '71','title' => '連假出去玩！出國電話訂房就靠這幾句','slug' => 'telephone-booking','excerpt' => '"台灣國慶日(National day)連假(Consecutive holidays)即將來了！喜愛自助旅行(Travel independently)的你卻又擔心不會打電話訂房了嗎？從訂房、詢問設施到看懂所有的飯店細節，小編都為你整理好了。讓你連假開心住、不當冤大頭！"','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>&nbsp;</p>
<p>台灣國慶日(<strong>National day</strong>)連假(<strong>Consecutive holidays</strong>)即將來了！喜愛自助旅行(<strong>Travel independently</strong>)的你卻又擔心不會打電話訂房了嗎？從訂房、詢問設施到看懂所有的飯店細節，小編都為你整理好了。讓你連假開心住、不當冤大頭！</p>
<p>&nbsp;</p>
<h2><span style=""color: #0088a8;""><strong>電話訂房</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">打電話訂房不用緊張，照著下面的指示講準沒錯！</span></strong></p>
<ol>
<li><em>Hello，I\'d like to reserve a room for October 7.</em><br />你好，我想訂十月7號的一間房間。</li>
<li><em>I&rsquo;d like to make a reservation for a suite room，please.</em><br />我想訂一間套房，謝謝。</li>
<li><em>I&rsquo;d like to book a double room for one night，please.</em><br />我想訂一間雙人房一晚，謝謝。&nbsp;</li>
<li><em>Do you have any rooms available / left for the weekend?</em><br />請問你們這周末有沒有空房？</li>
<li><em>Do you have any rooms with 3 beds for three adults?</em><br />請問你們有沒有提供三張床的三人房？</li>
<li><em>Are you all booked that night?</em><br />請問飯店那天客滿了嗎？</li>
<li><em>I&rsquo;m sorry. We&rsquo;re fully booked that night.</em><br />抱歉，那天已客滿了。</li>
</ol>
<p>&nbsp;</p>
<h2><span style=""color: #0088a8;""><strong>常見問答</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">客服人員通常會詢問以下問題，聽懂了就成功了一半</span></strong></p>
<ol>
<li><em>For what dates，Miss?</em><br />請問要訂幾號，小姐？</li>
<li><em>How many nights?</em><br />請問要住多少天？</li>
<li><em>We do have a vacancy. What&rsquo;s the name，please?</em><br />我們剛好有空房，請問貴姓？</li>
<li><em>Could you spell that，please?</em><br />你能拼一下名字嗎？</li>
<li><em>How do you spell your last name，sir?</em><br />請問您的姓氏怎麼拼，先生？</li>
</ol>
<p>&nbsp;</p>
<h2><span style=""color: #0088a8;""><strong>更多問題</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">千萬別忘了詢問更細節的問題，魔鬼藏在細節中啊～</span></strong></p>
<ol>
<li><em>Can I stay an extra night?&nbsp;</em><br />我能多住一晚嗎？</li>
<li><em>How would you like to pay?</em><br />請問您要如何付款？</li>
<li><em>Do I have to pay up front?</em><br />我要事先付款嗎？</li>
<li><em>Can I pay by credit card?</em><br />我能用信用卡付嗎？</li>
<li><em>I&rsquo;d like to pay in cash.</em><br />我想用現金付。</li>
<li><em>I&rsquo;d like to pay by credit card.</em><br />我想用信用卡付款。</li>
<li><em>How much is a single room?</em><br />一間單人房多少錢？</li>
<li><em>Does this price include all taxes?</em><br />請問價錢含稅嗎？</li>
<li><em>What time is the check-in and check-out?</em><br />請問登記入住和退房的時間？</li>
</ol>
<p>&nbsp;</p>
<p><img title=""訂房電話英文會話"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a00254c190e1-ae3vwJtuxDSKDCrmwZM0RuAVrt5xlb-1036x688.png"" alt=""訂房電話英文會話"" width=""677"" height=""449"" />&nbsp;</p>
<p>來源：<a href=""https://www.andrews-sykes.com/blog/cooling-for-hotel-reception/"">Andrews Sykes</a></p>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>字彙：</strong></h4>
<ul>
<li><em>Single room</em> 單人房</li>
<li><em>Double room</em> 雙人房 (一張雙人床)</li>
<li><em>Twin room</em> 雙人房 (兩張單人床)</li>
<li><em>Triple room</em> 三人房</li>
<li><em>Quadruple Room</em> 四人房</li>
<li><em>Studio</em> 精簡小房</li>
<li><em>Family room</em> 家庭房</li>
<li><em>Standard room</em> 標準房</li>
<li><em>Superior room</em> 高級房</li>
<li><em>Deluxe room</em> 豪華房</li>
<li><em>Suite</em> 套房<br /> (比起一般房型有較多房間和設施，例如加大床、廚房、客廳)</li>
<li><em>Queen bed</em>大號雙人床</li>
<li><em>King bed</em>大雙人床<br /> (比Queen bed更大，適合兩個大人+一個小孩)</li>
<li><em>Rollaway / Extra beds</em> 加床 (折疊型的床)</li>
<li><em>Cot</em> 嬰兒床</li>
<li><em>On request</em> 可要求索取</li>
<li><em>Low rate</em> 低價</li>
<li><em>No money back</em> 不可退款</li>
<li><em>Debit card</em> 金融簽帳卡 <br />(和Credit card信用卡不一樣，無法預支金錢)</li>
<li><em>Free cancellation before Sept 10</em><br />在九月10日前可免費取消</li>
<li><em>Prepayment</em> 預先付款</li>
<li><em>Pay at the property</em> 登記時付款</li>
<li><em>Deposit</em> 押金<br /> (有些外國旅店收押金，入住期間使用客房服務或其他設施，就會從裡面扣)</li>
<li><em>Tax</em> 稅金<br /> (有些外國旅店房價不含稅金，會另外索取)</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>設施：</strong></h4>
<ul>
<li><em>Facilities</em> 設施<br /> (飯店提供的健身房、泳池、三溫暖等較大型的設施)</li>
<li><em>Amenities</em> 設施 <br />(指飯店的服務，如盥洗用具、免費網路、茶和咖啡等等)</li>
<li><em>24-hour room service</em> 二十四小時客房服務</li>
<li><em>Concierge</em> 服務台職員</li>
<li><em>Minibar</em> 小型冰箱</li>
<li><em>Bathtub</em> 浴缸</li>
<li><em>En-suite bathroom</em> 獨立衛浴 <br />(比起有些Shared bathroom公用衛浴的房間，許多人偏好獨立衛浴)</li>
<li><em>Kitchenette</em> 小廚房</li>
<li><em>Terrace</em> 陽台</li>
<li><em>Fitness center</em> 健身房</li>
<li><em>Sauna</em> 三溫暖</li>
<li><em>Massage services</em> 按摩服務</li>
<li><em>In-room tea and coffee</em> <br />室內提供茶、咖啡</li>
<li><em>Ironing board and iron</em> <br />燙衣板及熨斗&nbsp;</li>
<li><em>Laundry and dry cleaning service </em><br />洗烘衣服務</li>
<li><em>Continental breakfast </em><br />歐陸式早餐 (通常是自助式Buffet的早餐)</li>
<li><em>Full English breakfast</em><br /> 全英式早餐 (通常含吐司、蛋、培根)</li>
<li><em>IDD call (International direct dialing) </em><br />國際直撥電話</li>
</ul>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '連假出去玩！出國電話訂房就靠這幾句','og_description' => '"台灣國慶日(National day)連假(Consecutive holidays)即將來了！喜愛自助旅行(Travel independently)的你卻又擔心不會打電話訂房了嗎？從訂房、詢問設施到看懂所有的飯店細節，小編都為你整理好了。讓你連假開心住、不當冤大頭！"','meta_title' => '連假出去玩！出國電話訂房就靠這幾句','meta_description' => '"台灣國慶日(National day)連假(Consecutive holidays)即將來了！喜愛自助旅行(Travel independently)的你卻又擔心不會打電話訂房了嗎？從訂房、詢問設施到看懂所有的飯店細節，小編都為你整理好了。讓你連假開心住、不當冤大頭！"','canonical_url' => 'https://tw.english.agency/telephone-booking','feature_image_alt' => '連假出去玩！出國電話訂房就靠這幾句','feature_image_title' => '連假出去玩！出國電話訂房就靠這幾句','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a00365d1257d-DfSS6JSHeLqGqNnGqCldo8DGZZ2Doc-1200x630.png','author_id' => '1','category_id' => '10','created_at' => '"2017-11-06 09:02:41"'],
  ['id' => '72','title' => '生活英文│冰與火之歌，你不可不知的小八卦','slug' => 'A-song-of-Ice-and-Fire','excerpt' => '《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！</p>
<p><em>A song of Ice and Fire is a series of an epic fantasy novel written by George R.R.Martin，who conceived of it as a trilogy. Today five of the seven volumes have beenpublished.</em></p>
<p><em>《冰與火之歌》小說是喬治馬丁筆下的奇幻史詩鉅作，原先以為只會出三部曲，現今已出版五冊，一系列共七冊。</em></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #0088a8;""><strong>你必須知道的冰與火之歌：</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">看冰與火之歌時，一定要懂的13句台詞！</span></strong></p>
<ol>
<li><em>Iron Throne</em> 鐵王座</li>
<li><em>Motto</em> 家訓</li>
<li><em>Declare war on/against</em> 宣戰</li>
<li><em>Come to power</em> 掌權</li>
<li><em>Battle for the crown</em> 爭奪王位</li>
<li><em>Winter is coming.</em> 凜冬將至 (史塔克家族的家訓)</li>
<li><em>Hear me roar</em> 聽我怒吼 (蘭尼斯特家族的家訓)</li>
<li><em>You know nothing，Jon Snow. </em><br />你極其無知、你什麼都不懂(這是用來罵瓊恩什麼都不懂，奇妙的是在書中這句是翻成：你懂個屁呀！)</li>
<li><em>The conflict started between the Starks and the Lannisters. </em><br />史塔克家族和蘭尼斯特家族間產生衝突。</li>
<li><em>They conspired together to betray the hand of the king. </em><br />他們共同密謀，背叛國王首相。</li>
<li><em>They kill the king to take power. </em><br />他們為掌權殺掉國王。</li>
<li><em>Littlefinger tried to turn Sansa against her sister. </em><br />小指頭試圖慫恿珊莎與她妹妹反目成仇。</li>
<li><em>So who is the rightful heir to the Iron Throne? </em><br />所以誰才是鐵王座的合法繼承人？</li>
</ol>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #0088a8;""><strong>劇情太精彩，你可以這樣形容：</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">跟朋友討論冰與火之歌時，這幾句一定派得上用場！</span></strong></p>
<ol>
<li>I&rsquo;m so hyped for tonight&rsquo;s episode.好興奮今晚要播的影集！</li>
<li>After this finale，I have to wait one year for the new season.<br />看完最後一集，我得等上一年才有新的一季。</li>
<li>Boom! 看吧! (當你發現好的一方總是獲勝時，就可以撂下這句話！)</li>
<li>I didn&rsquo;t see that coming.<br /> 我完全沒想過會這樣、這是我始料未及的</li>
<li>Did that just happen?<br /> 剛剛的事都是真的嗎？</li>
<li>It took me a while to register. <br />我剛剛才意識到</li>
<li>I rewatched all the series! <br />我每個系列都重看了</li>
</ol>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #0088a8;""><strong>什麼？瑟曦曾因試鏡「潛規則」丟失角色</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">What? Cersei lost roles for not flirting.</span></strong></p>
<p>飾演瑟曦的琳娜‧海蒂 (Lena Headey) 在與梅西‧威廉斯 (Maisie Williams) 對談中，透露她曾經在試鏡中因不和對方調情而喪失演出。</p>
<ul>
<li><em>She said: &ldquo;When I was in my twenties，and doing a lot of audition tapes in the States，a casting director told me: \'The men take these tapes home and watch them and say，Who would you f*ck?\'</em><br />她說：「當時我二十幾歲，在美國參加許多錄影帶試鏡。一位選角導演告訴我：這些人會把錄影帶帶回家看。然後問我『你會跟誰上床？』」</li>
</ul>
<p>琳娜‧海蒂公開這種莫名的潛規則 (<strong>Unspoken rules</strong>)，也凸顯了女性在職場上的地位不平等。更別說最近出現已久的「<strong>Mansplaining</strong>」(直男癌說教、男性說教)。就連女星珍妮佛‧勞倫斯、臉書營運長雪柔‧桑德伯格都曾表示同工不同酬 (<strong>Gender pay gap</strong>)、女性領導就是強勢 (<strong>Strong、Aggressive、Pushy</strong>)等刻板印象。</p>
<ul>
<li><em>A woman has to say the same thing ten times，but a man says it once and everybody&nbsp;listens.</em><br />同一件事女性得說上十次，大家才會聽；但男性只需要說一次。</li>
</ul>
<p>&nbsp;</p>
<hr />
<h4 style=""color: #fa8072;""><strong>再來聽聽這個小故事：一位男上司誤用女同事的信箱，發現性別歧視的嚴重性</strong></h4>
<ul>
<li><em>Nicole and I worked for a small employment service firm and one complaint always came from our boss: She took too long to work with clients. As her supervisor，I considered this a minor nuisance at best. I figured the reason I got things done faster&nbsp;was from having more experience. So one day I\'m emailing a client back-and- forth&nbsp;about his resume and he is just being IMPOSSIBLE. Rude，dismissive，ignoring my&nbsp;questions. Telling me his methods were the industry standards (they weren\'t) and I&nbsp;couldn\'t understand the terms he used (I could).</em><br />妮可和我在一家小型人力仲介公司工作，但我們的老闆總是抱怨她花太多時間處理客戶。身為她的上司，我頂多當作是小問題處理。我原以為自己做事效率較好是因為經驗豐富的關係。有一天我為了處理一位客戶的履歷，不斷跟他互相來信，他的反應實在不可理喻，他的態度無禮、輕視，還無視我的提問。只告訴我他的方法是業界標準 (明明不是)，還用那些我聽不懂的術語 (我懂)。&nbsp;</li>
<li><em>Anyway I was getting sick of his shit when I noticed something. Thanks to our shared&nbsp;inbox，I\'d been signing all communications as ""Nicole"".</em><br />正當我被他搞得身心俱疲時，我發現了一件事。感謝我用到了她的信箱，郵件上都是署名Nicole。</li>
<li><em>By the time she could get clients to accept that she knew what she was doing，I couldget halfway through another client.</em><br />等到她還在讓客戶相信她知道自己在做什麼時，我可能已經在處理下一位客戶的事了。</li>
</ul>
<p>&nbsp;</p>
<hr />
<h2><span style=""color: #0088a8;""><strong>《冰與火之歌》資料遭洩！</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">Game of Thrones data leak！</span></strong></p>
<ul>
<li><em>Hackers claimed to have obtained 1.5 terabytes of data from the company. So far，an&nbsp;upcoming episode of Ballers and Room 104 have apparently been put online. There&nbsp;is also written material that&rsquo;s allegedly from next week&rsquo;s fourth episode of Game of&nbsp;Thrones. More is promised to be &ldquo;coming soon.&rdquo;</em><br />駭客表示已從公司內部竊取1.5TB的資料。目前即將播出的影集《好球天團》以及《104號房》也已遭公布網路，還包括《冰與火之歌》下周即映的第四集內容手稿。駭客聲稱即將曝光更多內容。</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>遭駭事件讓大家議論紛紛，來聽聽這名網友的評論：</strong></h4>
<ul>
<li><em>I\'d be more worried about an HBO hack that leaked user data，such as credit card&nbsp;info for payments through HBO Go.</em><br />我比較擔心HBO遭駭會洩漏使用者的資料，例如我們使用付費程式HBO Go時填入的信用卡資訊。</li>
</ul>
<h2>&nbsp;</h2>
<hr />
<h4>同場加映：</h4>
<p><a href=""https://www.youtube.com/watch?v=YnxgpgyX4HE&amp;t=35s"" target=""_blank"" rel=""noopener noreferrer"">《冰與火之歌》艾蜜莉亞‧克拉克談論第一次！</a><br /><a href=""https://www.youtube.com/watch?v=AXdfTUb1N-A&amp;t=209s"" target=""_blank"" rel=""noopener noreferrer"">《冰與火之歌》冏恩來晚餐！</a></p>
</div>"','published' => '1','og_title' => '生活英文│冰與火之歌，你不可不知的小八卦','og_description' => '《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！','meta_title' => '生活英文│冰與火之歌，你不可不知的小八卦','meta_description' => '《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！','canonical_url' => 'https://tw.english.agency/A-song-of-Ice-and-Fire','feature_image_alt' => '生活英文│冰與火之歌，你不可不知的小八卦','feature_image_title' => '生活英文│冰與火之歌，你不可不知的小八卦','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a03d77fa1b82-PasJ8aONg3Bk1bh0i8tMSF1t37hj5r-1200x630.png','author_id' => '1','category_id' => '10','created_at' => '"2017-11-09 04:20:38"'],
  ['id' => '73','title' => '學英文不吃苦，就從早餐之旅開始','slug' => '早餐英文之旅','excerpt' => '"工作這麼累，下班這麼晚，打開厚重單字書就想睡，怎麼還會有時間好好學英文? 只要每天從重複會做的事""玩樂學英文""，單字一定牢牢跟緊你! 一日之計在於晨，這篇早餐英文之旅給你滿滿收穫。Let’s Go !"','content' => '"<p><span style=""font-weight: 400;"">是否常常內心總有小劇場在吶喊：英文很重要！但是工作這麼累，下班這麼晚，怎麼還會有時間好好學英文?</span></p>
<p><span style=""font-weight: 400;"">沒錯！就是因為時間不夠用，所以善用群聚單字的概念來記，就會達到較好的效率。</span></p>
<p><span style=""font-weight: 400;"">例如：每天固定挪出一點點時段，從每天重複會做的事""邊作邊學英文""，累積久了單字一定牢牢跟緊你!</span></p>
<p><span style=""font-weight: 400;"">今天就從每日必經的早餐之旅開始學英文。Let&rsquo;s Go !</span></p>
<p>&nbsp;</p>
<p><strong>早餐每天都會吃，那早餐英文到底怎麼說? </strong></p>
<p><span style=""font-weight: 400;"">怎麼說： breakfast（▶ </span><a href=""https://translate.google.com.tw/#en/zh-TW/breakfast""><span style=""font-weight: 400;"">聽發音</span></a><span style=""font-weight: 400;"">）</span><span style=""font-weight: 400;"">怎麼記： &nbsp;break + fast = &nbsp;breakfast </span></p>
<p><span style=""font-weight: 400;"">其實早餐的英文單字來源是這兩個字的組合－break（破壞 / 打斷 ）和fast（最常使用的意思是快速的、但這邊引用的是""禁食""之意）</span></p>
<p><span style=""font-weight: 400;"">所以breakfast就是打斷禁食的意思，因為通常夜裡不會吃東西，所以早上的第一餐就是停止斷食的概念!</span></p>
<p><span style=""font-weight: 400;""><img title=""吃早餐學英文之旅"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a054fb311877-03sZN6rqbG6L1HGA9heWaJAYJTQiVr-600x315.jpeg"" alt=""吃早餐學英文之旅"" /></span></p>
<p>&nbsp;</p>
<h2>&nbsp;</h2>
<p>&nbsp;</p>
<hr />
<h2><strong>吃早餐學英文不卡卡，點餐時會用到單字怎麼說?</strong></h2>
<p><span style=""font-weight: 400;"">早餐使用的單字眾多，今天先從大家較熟悉的台灣西式早餐店來分享，基本菜單大致分三類</span></p>
<p>&nbsp;</p>
<h3><strong>1.蛋餅類 &nbsp;</strong><strong><span style=""color: #f28f00;"">Chinese Omelet </span></strong></h3>
<p>Chinese Omelet是外國人較常使用說法，也可說Egg Pancake</p>
<p>&nbsp;</p>
<table style=""width: 413px;"">
<tbody>
<tr>
<td style=""width: 128.667px;"">
<p><strong>材料/口味 flavor</strong></p>
</td>
<td style=""width: 268.667px;"">&nbsp;</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>起士</strong></p>
<p><strong>Cheese</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with cheese</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>鮪魚</strong></p>
<p><strong>Tune</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with tune</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>玉米</strong></p>
<p><strong>Corn</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with corn</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>肉鬆</strong></p>
<p><strong>Dried Pork Floss</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with dried pork floss</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>火腿</strong></p>
<p><strong>Ham</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with ham</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>培根</strong></p>
<p><strong>Bacon</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with bacon</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>豬肉</strong></p>
<p><strong>Pork</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with pork</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128.667px;"">
<p><strong>燻雞</strong></p>
<p><strong>Smoked Chicken</strong></p>
</td>
<td style=""width: 268.667px;"">
<p><strong>Chinese omelet with smoked chicken</strong></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h3><strong>2. 漢堡類 &nbsp;</strong><strong><span style=""color: #f28f00;""> Burger</span>　/　</strong><strong>吐司類 &nbsp;</strong><strong><span style=""color: #f28f00;"">Sandwich</span> </strong></h3>
<p>只要熟記材料單字後，就可以套用在漢堡或是吐司三明治喔！</p>
<table style=""width: 343px;"">
<tbody>
<tr>
<td style=""width: 128px;"">
<p><strong>豬肉漢堡蛋</strong></p>
</td>
<td style=""width: 203px;"">
<p><strong>Pork and Egg Burger</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128px;"">
<p><strong>培根漢堡蛋</strong></p>
</td>
<td style=""width: 203px;"">
<p><strong>Bacon and Egg Burger</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128px;"">
<p><strong>卡拉雞腿漢堡</strong></p>
</td>
<td style=""width: 203px;"">
<p><strong>Crispy Chicken Burger</strong></p>
</td>
</tr>
<tr>
<td style=""width: 128px;"">
<p><strong>黑胡椒豬排堡</strong></p>
</td>
<td style=""width: 203px;"">
<p><strong>Black Pepper Pork Chop Burger </strong></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h3><strong>3. 飲料類 &nbsp;</strong><strong><span style=""color: #f28f00;""> Beverage　</span></strong></h3>
<p>早餐必搭配的飲料，你最喜歡哪一種呢？</p>
<table style=""width: 345px;"">
<tbody>
<tr>
<td style=""width: 124px;"">
<p><strong>紅茶</strong></p>
</td>
<td style=""width: 208px;"">
<p><strong>Black tea</strong></p>
</td>
</tr>
<tr>
<td style=""width: 124px;"">
<p><strong>奶茶</strong></p>
</td>
<td style=""width: 208px;"">
<p><strong>Milk tea</strong></p>
</td>
</tr>
<tr>
<td style=""width: 124px;"">
<p><strong>綠茶</strong></p>
</td>
<td style=""width: 208px;"">
<p><strong>Green tea</strong></p>
</td>
</tr>
<tr>
<td style=""width: 124px;"">
<p><strong>咖啡</strong></p>
</td>
<td style=""width: 208px;"">
<p><strong>Coffee</strong></p>
</td>
</tr>
<tr>
<td style=""width: 124px;"">
<p><strong>豆漿</strong></p>
</td>
<td style=""width: 208px;"">
<p><strong>Soy milk/ Soybean milk</strong></p>
</td>
</tr>
<tr>
<td style=""width: 124px;"">
<p><strong>米漿</strong></p>
</td>
<td style=""width: 208px;"">
<p><strong> Rice and peanut milk</strong></p>
</td>
</tr>
</tbody>
</table>
<h2>&nbsp;</h2>
<hr />
<h2><img title=""吃早餐學英文之旅"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a05b69335cc3-OHskzrA9sSCJg6f2eWpag66SioAYEK-600x315.jpeg"" alt=""吃早餐學英文之旅"" width=""600"" height=""315"" /></h2>
<h2><strong>6句超實用早餐英文會話，趕快打包私藏！</strong></h2>
<p>&nbsp;</p>
<p><strong><span style=""color: #f28f00;""> 01. May I have a menu，please?　 </span></strong></p>
<p><strong>請給我菜單。</strong></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #f28f00;""> 02. Do you have a menu in chinese?　 </span></strong></p>
<p><strong>是否有中文菜單？</strong></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #f28f00;""> 03. I need a few more minutes.　　</span></strong></p>
<p><strong>我需要多點時間決定。</strong></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #f28f00;""> 04. Excuse me，I would like my coffee warmer. </span></strong></p>
<p><strong><span style=""color: #f28f00;""> Could you please heat up my coffee little bit? Thank you!　</span></strong></p>
<p><strong>請給我熱一點的咖啡，謝謝</strong></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #f28f00;"">05. I would like a fried egg with a hard yolk.</span></strong></p>
<p><strong>我要荷包蛋，蛋黃要煎熟</strong></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #f28f00;""> 06. We are ready for the bill now. </span></strong></p>
<p><strong>我們要買單。</strong></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">&nbsp;</span>&nbsp;</p>
<p><span style=""font-weight: 400;"">學英文最難的不是記單字，而是記了不知道甚麼時候用！</span></p>
<p><span style=""font-weight: 400;"">即使生活環境無法創造百分之百開口說英語，但只要可以從生活的一小部分慢慢累積，</span></p>
<p>不熟的單字也能變熟悉喔!</p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">一天最美的時間是早</span>晨<span style=""font-weight: 400;"">，跟自己說聲早安拉開一日序幕</span></p>
<p><span style=""font-weight: 400;"">☀ 早晨配陽光 X 吃早餐= 充滿正面能量的自己！</span></p>
<p><span style=""font-weight: 400;"">「不管你睡的多晚起的多晚</span></p>
<p><span style=""font-weight: 400;"">晨之美永遠在這裡歡迎光臨你」♬♬♬</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '學英文不吃苦，就從早餐之旅開始','og_description' => '"工作這麼累，下班這麼晚，打開厚重單字書就想睡，怎麼還會有時間好好學英文? 只要每天從重複會做的事""玩樂學英文""，單字一定牢牢跟緊你! 一日之計在於晨，這篇早餐之旅給你滿滿收穫。Let’s Go !"','meta_title' => '學英文不吃苦，就從早餐之旅開始','meta_description' => '"工作這麼累，下班這麼晚，打開厚重單字書就想睡，怎麼還會有時間好好學英文? 只要每天從重複會做的事""玩樂學英文""，單字一定牢牢跟緊你! 一日之計在於晨，這篇早餐之旅給你滿滿收穫。Let’s Go !"','canonical_url' => 'https://tw.english.agency/早餐英文之旅','feature_image_alt' => '吃早餐學英文之旅','feature_image_title' => '吃早餐學英文之旅','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a054933d6b44-h6vQGZN5ipTPfSK5VMIrFMgbEJXFCS-1200x630.jpeg','author_id' => '10','category_id' => '10','created_at' => '"2017-11-10 06:49:04"'],
  ['id' => '74','title' => '"10大最佳旅遊城市 摩登高雄英文怎麽介紹？"','slug' => '10大最佳旅遊城市-摩登高雄英文怎麽介紹','excerpt' => '"國際知名的旅遊指南《孤獨星球》Lonely Planet公布2018年世界最佳旅遊城市名單，高雄成爲唯一入選的亞洲城市。《孤獨星球》是如何介紹高雄呢？我們藉此機會了解外國人眼中的高雄，也學學如何向外國人推薦高雄吧！"','content' => '"<p>國際知名的旅遊指南《孤獨星球》Lonely Planet公布2018年世界最佳旅遊城市名單，高雄成爲唯一入選的亞洲城市。《孤獨星球》是如何介紹高雄呢？我們藉此機會了解外國人眼中的高雄，也學學如何向外國人推薦高雄吧！</p>
<h2><span style=""color: #a42d00;"">不斷evolve的高雄</span></h2>
<p><span style=""color: #000080;"">This ever-evolving collection of dozens of old warehouses is stuffed with boutiques，galleries，cafes and performance venues that line two sweeping boulevards by the port where ships are docked.</span><br />（這個不斷進化並集結好幾十個倉庫的點擠滿了精品店、藝廊、咖啡廳和表演場地，沿著停靠船隻的港口邊延展的大道兩旁排列樹立著。）</p>
<p>首先我們先來學evolve（進化），他的名詞變化為evolution，形容詞變化則是evolutionary。再將evolve改為主動式的現在分詞，加上ever這個副詞，就是ever-evolving不斷進化的。英文有一種常見的複合形容詞喜歡用ever加上分詞表達「不斷～」之意，如ever-changing不斷改變的、ever-lasting不斷持續地等等，都是運用了這個方法來創造出來的單字。</p>
<p>例句：It is said that people evolved from monkeys.（據說人們是由猴子演化而來。）</p>
<p>接下來我們看到be stuffed with，就是「用～填滿」的意思，stuff可以延伸變成動詞，就是塞滿的的意思，因為這個地方不會主動塞滿，因此他也通常用作被動態，要注意介系詞要配with而不是by，因為這個不是指動作者，而是表達「用～東西去填滿」的概念。多益測驗聽力部份喜歡混淆考生視聽，因此注意內容情境來判斷作為名詞或動詞用法，才不會被誤導。</p>
<p>Performance venue則是出現了我們常見的venue一詞，performance來自perform這個動詞，表演之意，performer後面加上「-er」就是表演者的意思。Venue是多益常見單字，表達場地的意思。多益測驗的情境常會看到公司要尋找辦活動或會議的場地，就會用這個單字來表達。</p>
<p>&nbsp;</p>
<h2><span style=""color: #a42d00;"">高雄modernise</span></h2>
<p><span style=""color: #000080;"">A growing number of young chefs and restaurateurs in the city are working to modernise southern Taiwanese cuisine，while shining a spotlight on local produce. </span><br />（城市中不斷成長的年經大廚和餐廳業者努力促使南台灣美食的現代化，同時使當地的農產品成為鎂光燈焦點。）</p>
<p>這邊用了一個常見的片語，a number of表達很多的意思，等同於many，注意這裡後面只能接可數名詞，因此chef和restaurateur都要加上「-s」表達複數。</p>
<p>後面使用的不定詞片語中用了一個動詞modernise，其實就是形容詞modern的動詞，也就是我們中文直接音譯「摩登」的意思。值得注意的是這邊用的是英文拼法，其字尾拼法為「-ise」，美式拼法則是「-ize」，都是表達動詞的字尾。名詞變化modernization/modernisation，現代化之意。</p>
<p>最後介紹一個在多益測驗中很常見的單字，大多數人熟悉的是produce當作動詞，表達產生之意，名詞production抽象概念，product則是具體產品概念。可是這邊是作名詞，意思是農產品，重音在第一個音節，不是第二個音節，因此大家在考聽力時要注意重音來辨別意思。</p>
<h2><br /><span style=""color: #a42d00;"">多益模擬試題</span></h2>
<p>Monday 5th April 2017</p>
<p>ELG is seeking an exceptional graphic designer to join its creative in-house team.<br />This is a fantastic opportunity to be part of a unique global lifestyle brand.</p>
<p>You will be working on all print based materials both for marketing and in-house departments，including POS，in-store signage，look books，stationery to large format graphics. Each project will be designed from concept to completion.</p>
<p>The ideal candidate will be a versatile，reliable designer，with strong layout typographic skills，a good eye for detail，have print production knowledge，a minimum of 3 years commercial experience and possess a sound knowledge of CS suite and Office applications.</p>
<p>We can offer a competitive salary and benefits package. To apply，please email your CV and work samples (no larger than 3MB) together with a covering letter stating your current salary emailed to: recruitment@elg.com</p>
<p>Closing date for applications: Friday 23rd April 2017</p>
<p>1. What is the information above mainly about?<br />(A) technology development<br />(B) job opportunity<br />(C) computer system<br />(D) company rules</p>
<p>2. What is NOT mentioned about the ideal candidate in this information?<br />(A) all-round<br />(B) at least three years of commercial experience<br />(C) good eyesight<br />(D) print production knowledge</p>
<p>3. If you would like to apply，what do you need to do?<br />(A) Send your resume with work sample and current payroll information<br />(B) Get approval from your supervisor<br />(C) Renew the contract<br />(D) Send the package</p>
<p><strong>解析</strong><br />1. 這題是在問這篇文章主要在說什麼？從第一句「 ELG is seeking an exceptional graphic designer to join its creative in-house team.」就可以明確看出來這間公司在徵才，這句的意思是說「ELG正在尋找一名傑出的平面設計師加入這個公司內部創意無限的團隊。」因此答案只能選(B)工作機會。</p>
<p>2. 這題問「關於這個工作人選，哪個資格並沒有提到？」<br />從文中第三段可以一窺究竟，「The ideal candidate will be a versatile，reliable designer，with strong layout typographic skills，a good eye for detail&hellip;&hellip;」。首先提到適合理想的應徵人選必須要「多變圓滑的、可靠的，擁有強大的平面印刷技能，而且做事仔細，對於印刷品有基本認知，至少要有三年商業經驗，且擁有CS軟體和微軟應用軟體的知識。」</p>
<p>所以明顯地答案(C)，因為have a good eye for ~並不是指視力要好，而是對某件事情有一定的敏感度或是感知。</p>
<p>3. 這題題目問「如果你想要應徵，你需要做什麼？」<br />這題則要看到文章的最後一段，「To apply，please email your CV and work samples &hellip;&hellip;」，這裡提到要應徵的人必須要寄履歷表、作品樣本和求職信到公司的email帳號，並註明目前工作薪水。因此答案要選(A)。<br />【 文／Buffy Kao】</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '"10大最佳旅遊城市 摩登高雄英文怎麽介紹？"','og_description' => '"國際知名的旅遊指南《孤獨星球》Lonely Planet公布2018年世界最佳旅遊城市名單，高雄成爲唯一入選的亞洲城市。《孤獨星球》是如何介紹高雄呢？我們藉此機會了解外國人眼中的高雄，也學學如何向外國人推薦高雄吧！"','meta_title' => '"10大最佳旅遊城市 摩登高雄英文怎麽介紹？"','meta_description' => '"國際知名的旅遊指南《孤獨星球》Lonely Planet公布2018年世界最佳旅遊城市名單，高雄成爲唯一入選的亞洲城市。《孤獨星球》是如何介紹高雄呢？我們藉此機會了解外國人眼中的高雄，也學學如何向外國人推薦高雄吧！"','canonical_url' => 'http://www.toeicok.com.tw/10%E5%A4%A7%E6%9C%80%E4%BD%B3%E6%97%85%E9%81%8A%E5%9F%8E%E5%B8%82-%E6%91%A9%E7%99%BB%E9%AB%98%E9%9B%84%E8%8B%B1%E6%96%87%E6%80%8E%E9%BA%BD%E4%BB%8B%E7%B4%B9/','feature_image_alt' => '高雄','feature_image_title' => '高雄','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0423384bb90-OORrin9oMeqcmtPmlDuqdTCYs6Gsx5-640x427.jpeg','author_id' => '9','category_id' => '10','created_at' => '"2017-11-10 09:58:24"'],
  ['id' => '75','title' => '"必知4個關鍵詞彙，讓你30 秒掌握外商徵才！"','slug' => '必知4個關鍵詞彙-讓你30秒掌握外商徵才','excerpt' => '"每年光是應屆畢業生就有30 萬人投入職場，要在職場的徵才廣告中，找到適合自己的工作並進而獲得聘用，這可是需要耐心與技巧的，學會這4個關鍵詞彙，助你30秒快速掌握！"','content' => '"<p>有人說第一份工作很重要，但也有人說第一份工作不重要。很重要的理由是：「好的開始是成功的一半」，從適合自己條件的工作環境中培養工作能力與自信，對往後的生涯發展有幫助。第一份工作不重要的理由是：職場的工作成千上百，人生職涯三十餘載，不需急於第一份工作就要找最適合的工作，不妨騎驢找馬。</p>
<p>兩種說法見仁見智，但是在這個僧多粥少、人浮於事的不確定年代裡，每年光是應屆畢業生就有30 萬人投入職場，要在職場的徵才廣告中，找到適合自己的工作並進而獲得聘用，這可是需要耐心與技巧的。</p>
<h2><strong><span style=""color: #a42d00;"">Get Ready ！培養國際職場英語溝通力</span></strong></h2>
<p>從湯馬斯．佛里曼（Thomas Loren Friedman）所倡述「世界是平的」的觀念中，「全球化」會抹平國家與國家之間的藩籬，使世界愈來愈平。從新加坡的博奕事業要來臺灣找人才、歐美國家的東南亞工廠以網路向世界廣徵管理人才的趨勢看來，你就業的職場極可能不僅限於國內。</p>
<p>因此，你要有向國際職場求職的能力。其中有兩件最基本的事項：一、你必須有一份英文履歷（resume）與求職信（cover letter）；二、你必須看得懂英文的徵才廣告。以下是英文徵才廣告中常見的詞彙：</p>
<p>Wanted ──&ldquo;want&rdquo;的英文字義是「要」，而&ldquo;wanted&rdquo;是它表示「被動式」的過去分詞。&ldquo;Wanted&rdquo;的原意是被警方搜捕追緝，所以是「通緝、通緝犯」；常在電影中看到五、六十年前的警方畫著人臉圖像，上面寫著大大的&ldquo;Wanted&rdquo;要通緝江洋大盜。但是後來被求才徵人廣告借用。比方說&ldquo;Graphic Designer Wanted&rdquo;，意思就是某公司要徵「美編、繪圖設計」。</p>
<p>Cover Letter ── 在一般的觀念裡，找工作只要一份履歷表就可以了。但其實在國際職場裡，通常還要一封&ldquo;cover letter&rdquo;，稱為「求職信」或「自我推薦信」；信中簡略說明自己要應徵的工作、簡短的自我介紹、專長、以及為什麼適合這份工作。通常公司裡收信單位是人事部門，&ldquo;cover letter&rdquo;是給人事經理做初審之用，所以比較簡略。然後人事經理再把較為適合人選的履歷表轉給該項職缺的部門主管，所附履歷表要比較詳細。正確合宜的&ldquo;cover letter&rdquo;可以增加得到面試的機會。</p>
<p>Resume ──「履歷表」是&ldquo;resume&rdquo;。英式英文常用 CV（是 Curriculum Vitae 的縮寫），這一點同學在看英文的徵才廣告時，要注意！</p>
<p>Job Description ── 英文的&ldquo;describe&rdquo;是「描述、描寫、形容」的意思，&ldquo;description&rdquo;是它的名詞，所以&ldquo;job description&rdquo;簡言之，就是「工作內容、職務說明」的意思。</p>
<h3><strong><span style=""color: #000080;"">透析多益測驗求職題型</span></strong></h3>
<p>「求職」是職場的重要議題，做為檢測職場溝通能力的多益測驗（TOEIC），怎麼可能錯過。多益測驗不論在聽力部份或是在閱讀部份，都有許多與求職有關的話題。就聽力部份而言，可能是一段面試的對話，或是一段大型聯合面試的廣播。閱讀部分則可能是徵才廣告、求職信、錄取通知書、面試後詢問信等。在下一頁，我們模擬一家 Palmforce 公司的徵才廣告，讓同學熟悉這一類的題型。</p>
<p>Job Opening：Business Analyst，Palmforce Technology Ltd.</p>
<p>The Palmforce Technology Company is inviting applications for its project in Ha Noi，Vietnam. The factory in Ha Noi is starting the Construction Project A451 from July 2012.</p>
<p>Job Description：</p>
<p>Communicate with customers to get their requirement and business needs.<br />Advise customers based on the findings from business analysis.<br />Transfer the finalized requirements with customers to the project team.<br />Update any requirement change during project implementation.<br />Requirement：</p>
<p>University graduate in economics study or the like.Skilled with problem analyzing and solving. Highly proactive. Good communication and negotiation skills.Expert with Microsoft Office system is an absolute necessity. Good English listening，speaking，reading and writing skills: TOEIC score of at least 750.</p>
<p>To apply，please send a cover letter，a copy of your CV，and a recommendation from previous employment to Mr. Tom Yo，head of the Office of Human Resources.Instead，e-mail your CV to HR_jobs@palmforce.com. The deadline for applications is June 15，2012.</p>
<p>&nbsp;</p>
<p>【譯】</p>
<p>徵求：Palmforce 科技有限公司業務分析師Palmforce 公司越南河內市專案徵才。河內市工廠A451 建案將於2012 年7 月開工。</p>
<p>職務說明：</p>
<p>與客戶溝通，了解客戶需求<br />根據業務分析結果，提供客戶建議<br />將客戶定案提交專案團隊<br />專案執行期間，隨時更新客戶變更<br />應徵條件：</p>
<p>大學經濟相關科系畢。擅長問題處理分析。個性積極，具有良好的溝通談判技巧，務必熟悉微軟 Office 系統。英文聽說讀寫能力良好，多益測驗成績750 分以上。</p>
<p>意者請寄申請函、履歷表以及前任雇主的推薦函至本公司人事室主管Tom Yo 先生。或將履歷表以電子郵件寄至HR_jobs@palmforce.com。2012 年6 月15 日截止。</p>
<p>&nbsp;</p>
<h3>當你看了這間 Palmforce 科技公司的徵人廣告，你能正確回答以下的問題嗎？</h3>
<p>1.According to the advertisement，what is one of the job requirements?<br />(A) Professional experience in economics<br />(B) A certificate of TOEIC<br />(C) Proficiency in Microsoft system<br />(D) A willingness to travel abroad</p>
<p>2. What is the applicant NOT required to submit?<br />(A) A resume<br />(B) A letter of application<br />(C) A recommendation letter<br />(D) A photograph</p>
<p>3.In the advertisement，the word &ldquo;absolute&rdquo; in paragraph 3，line 4，is closest in meaning to______.<br />(A) definite<br />(B) excellent<br />(C) unlimited<br />(D) demanding</p>
<p><strong>【解析】</strong></p>
<p>題目是問，以下何者是應徵條件。正確答案是(C)，因為廣告中提到&ldquo;Expert with Microsoft Office system is an absolute necessity.&rdquo;。&ldquo;expert&rdquo;指「專家」，但是它也當形容詞用，表示「熟練的」。答案(A)在廣告中只提到大學所學，未提到就業經驗（professional experience）。答案(B)在廣告中只提到多益成績的要求，未提到需要証照（certificate）。答案(D)的「願意到國外」，在廣告中未提。</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"必知4個關鍵詞彙，讓你30 秒掌握外商徵才！"','og_description' => '"每年光是應屆畢業生就有30 萬人投入職場，要在職場的徵才廣告中，找到適合自己的工作並進而獲得聘用，這可是需要耐心與技巧的，學會這4個關鍵詞彙，助你30秒快速掌握！"','meta_title' => '"必知4個關鍵詞彙，讓你30 秒掌握外商徵才！"','meta_description' => '"每年光是應屆畢業生就有30 萬人投入職場，要在職場的徵才廣告中，找到適合自己的工作並進而獲得聘用，這可是需要耐心與技巧的，學會這4個關鍵詞彙，助你30秒快速掌握！"','canonical_url' => 'http://www.toeicok.com.tw/30%E7%A7%92%E6%8E%8C%E6%8F%A1%E5%A4%96%E5%95%86%E5%BE%B5%E6%89%8D-4%E5%A4%A7%E9%97%9C%E9%8D%B5%E8%A9%9E%E5%BD%99/','feature_image_alt' => 'job','feature_image_title' => 'job','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0e930377741-AUYQdgwE2mNfPvF963mGMTb7awjmDk-626x417.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2017-11-17 07:43:15"'],
  ['id' => '76','title' => '英文自我介紹難不倒你！3種說法一次上手','slug' => '英文自我介紹難不倒你-3種說法一次上手','excerpt' => '想要打破首次見面的尷尬，又不知道該如何開場？3種不同的英文自我介紹方式，讓你一次就學會！','content' => '"<p>自我介紹時，目光必須直視對方（look people straight in the eyes），這不僅是基本禮貌，而且代表對方是受到你的尊重的。如果你是一位內向（introvert）害羞的人，不妨多保持微笑，微笑總是可以打破僵局（ice breaker）、緩和氣氛。<br />如果已知對方的名字，在輪到你自我介紹時，重覆對方名字，將有助於你熟記對方的姓名，也會讓對方感受到你的誠意。<br />自我介紹時，必須先告知對方自己的全名、公司名稱以及服務單位。若是商務拜訪，也要將目的與事由交代清楚。為了打破首次見面的尷尬與冷場，以寒喧的方式做開場白是個不錯的選擇，親近的溝通容易打破彼此的藩籬，在最短的時間內與顧客打成一片；而簡潔、好的問題可以很快吸引顧客的興趣。</p>
<h2><strong><span style=""color: #5cacee;"">見面時打招呼的英文表達方式</span></strong></h2>
<p><br />Nice to meet you，Daisy. I am Orwell Chang. Please call me Orwell.（黛西，很高興認識您！我叫張歐威爾，請叫我歐威爾。）<br />＝ It&rsquo;s a pleasure to meet you，Daisy. I am Orwell Chang.<br />＝ Nice to meet you，Daisy. My name is Orwell Chang..<br />＝ Let me tell you a little bit about myself，Daisy. I am Orwell Chang.</p>
<p>Hello，This is Orwell Chang from Best Tech. It&rsquo;s nice to meet you.（哈囉！我是最佳科技的張歐威爾。很高興認識您！）<br />＝ It&rsquo;s my pleasure to meet you. I am Orwell Chang from Best Tech.<br />＝ I am so happy to see you. This is Orwell Chang from Best Tech.<br />＝ It&rsquo;s really my honor to have this opportunity to see you here. My name is Orwell Chang and I work with Best Tech.</p>
<p>I am so glad to have this opportunity to introduce myself to you. Currently，I am a sales representative at Annabelle Marketing.<br />（很高興有此機會介紹我自己給您認識，目前我是安納貝爾行銷公司的業務代表。）<br />＝ It&rsquo;s my great pleasure to be here in front of you to present myself. I work at Annabelle Marketing as a sales representative.<br />＝ I really welcome this opportunity to introduce myself. I am a sales representative of Annabelle Marketing.</p>
<h3><strong><span style=""color: #a42d00;"">單字解析</span></strong></h3>
<p><br />表示「樂意、高興」的英文表達：<br />be glad to ＝ be happy to ＝ be pleased to＝ be delighted to</p>
<p><br />introduce (v)：介紹、提出<br />This new CEO introduces innovative ideas into business operation.（新執行長把創新觀念引進至商務運作上。）<br />The chairman introduced the speaker to the audience.<br />（主席將演講者介紹給聽眾。）</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>摘自五南文化事業機構出版<a href=""http://www.wunanbooks.com.tw/product/9789571163840"" target=""_blank"" rel=""nofollow noopener noreferrer"">《商用英文：最佳商務往來溝通藝術》</a></p>"','published' => '1','og_title' => '英文自我介紹難不倒你！3種說法一次上手','og_description' => '想要打破首次見面的尷尬，又不知道該如何開場？3種不同的英文自我介紹方式，讓你一次就學會！','meta_title' => '英文自我介紹難不倒你！3種說法一次上手','meta_description' => '想要打破首次見面的尷尬，又不知道該如何開場？3種不同的英文自我介紹方式，讓你一次就學會！','canonical_url' => 'https://tw.english.agency/英文自我介紹難不倒你-3種說法一次上手','feature_image_alt' => '英文自我介紹','feature_image_title' => '英文自我介紹','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0eaacaabfcf-zSaeGqSxblVR3LOzab1kmDky7ILGBV-1200x698.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-11-17 09:24:42"'],
  ['id' => '77','title' => '台灣好湯季開跑，泡溫泉英文怎麼說？','slug' => '泡溫泉英文怎麼說','excerpt' => '台灣溫泉擁有多樣性泉質，不僅台灣人喜愛，也是觀光客來玩必朝聖之地。微涼秋意來臨，樹葉悄悄上了新裝，台灣好湯季早已陸續開跑，秉著玩樂Fun英文的概念，今天為大家整理與「泡溫泉」有關的英文單字。','content' => '"<p><span style=""font-weight: 400;"">微涼秋意來臨，樹葉悄悄上了新裝，不論秋夜晚風，還是冷冽冬天都是泡湯的好季節！</span></p>
<p><span style=""font-weight: 400;"">加上台灣的溫泉得天獨厚擁有多樣性泉質，不僅台灣人喜愛，也是觀光客來玩必朝聖之地。趁著台灣好湯季開跑，秉著玩樂Fun英文的概念，為大家整理與「泡溫泉」有關的英文單字。</span></p>
<p>&nbsp;</p>
<h2><strong>泡湯超療癒，泡溫泉的英文單字先認識！</strong></h2>
<p><span style=""font-weight: 400;"">怎麼說：<strong>hot spring</strong>（</span><a href=""https://translate.google.com.tw/#en/zh-TW/hot%20spring""><span style=""font-weight: 400;"">聽發音</span></a><span style=""font-weight: 400;"">）</span></p>
<p><span style=""font-weight: 400;"">怎麼記：為什麼溫泉英文要用 hot spring？ &nbsp;spring 大家熟知是春天的意思，但在這裡是引用的是「泉水」之意! 所以很簡單的概念，溫泉英文 hot spring 就是熱的泉水。相信大家多聽幾次發音就會朗朗上口的記起來。</span></p>
<p><br /><img title=""泡溫泉英文怎麼說"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0f008f2dd02-xKZu2Q24j0WUXgIemBrclx48a4NpzZ-600x315.jpeg"" alt=""泡溫泉英文怎麼說"" width=""600"" height=""315"" /></p>
<p>&nbsp;</p>
<hr />
<h2><strong>天冷泡湯趣，哪種泉質你最喜歡？</strong></h2>
<p><span style=""font-weight: 400;"">泡湯不僅能在寒冷的冬天感到無比溫暖，也兼具保養身體的作用。可以讓關節與肌肉放鬆、促進血液循環，泡完身體都暖呼呼起來。然而台灣溫泉種類豐富，哪種泉質最適合你呢？就來一邊泡溫泉一邊學英文長知識！</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">硫磺泉 <span style=""color: #085298;""><strong>sulfur spring</strong>&nbsp;（▶&nbsp;<a href=""https://translate.google.com.tw/#en/zh-TW/sulfur%20spring"">聽發音</a>）</span></span></p>
<p><span style=""font-weight: 400;"">顏色呈白濁或黃褐色，緩解疲勞。代表地區：</span><span style=""font-weight: 400;"">陽明山、北投溫泉</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">碳酸氫鈉泉<span style=""font-weight: 400;""><span style=""color: #085298;""><strong>sodium bicarbonate spring</strong>&nbsp;（▶&nbsp;<a href=""https://translate.google.com.tw/#en/zh-TW/sodium%20bicarbonate%20spring"">聽發音</a>）</span></span></span></p>
<p><span style=""font-weight: 400;"">無色無味也稱美人湯。代表地區：烏來、礁溪、知本溫泉</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">碳酸泉<span style=""color: #085298;""><strong>carbonic acid spring</strong> &nbsp;（▶&nbsp;<a href=""https://translate.google.com.tw/#en/zh-TW/carbonic%20acid%20spring"">聽發音</a>）</span></span></p>
<p><span style=""font-weight: 400;"">溫度較一般溫泉低，對心臟負擔較低。</span></p>
<p><span style=""font-weight: 400;"">此泉質在台灣分布最廣，代表地區：陽明山大屯山區、谷關、泰安、四重溪溫泉</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">泥漿溫泉<span style=""color: #085298;""><strong>volcanic mud spring</strong>&nbsp;（▶&nbsp;<a href=""https://translate.google.com.tw/#en/zh-TW/volcanic%20mud%20spring"">聽發音</a>）</span></span></p>
<p><span style=""font-weight: 400;"">富含泥漿，能去除老廢角質。泥漿溫泉非常稀少，代表地區：台南關子嶺</span></p>
<p><br /><br /><br /></p>
<hr />
<h2><strong>泡溫泉吃美食，還有哪些常用英文單字一起幫你整理！</strong></h2>
<p>去泡溫泉怎麼說&nbsp;<strong><span style=""color: #085298;"">go to a hot spring / visit a hot spring</span></strong></p>
<p><span style=""font-weight: 400;"">溫泉飯店<strong><span style=""color: #085298;""> hot spring hotel / hot spring resort</span></strong></span></p>
<p><span style=""font-weight: 400;"">泡溫泉澡堂 <strong><span style=""color: #085298;"">hot spring baths</span></strong></span></p>
<p><span style=""font-weight: 400;"">露天溫泉<strong><span style=""color: #085298;"">open-air hot springs</span></strong></span></p>
<p><span style=""font-weight: 400;"">溫泉蛋<strong><span style=""color: #085298;""> boiled egg</span></strong></span></p>
<p><span style=""font-weight: 400;"">拉麵 <strong><span style=""color: #085298;"">Ramen (noodle)</span></strong></span></p>
<p><span style=""font-weight: 400;"">懷石料理&nbsp;<strong><span style=""color: #085298;"">Kaiseki Dinner / Kaiseki Meal</span></strong></span></p>
<p><img title=""泡溫泉英文怎麼說"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0f0456236a5-k3cGiv1FIOvog1RITBGc8oiHvpipaV-600x315.jpeg"" alt=""泡溫泉英文怎麼說"" width=""600"" height=""315"" /></p>
<hr />
<h2>超實用！5個泡溫泉情境英文</h2>
<p><strong> <span style=""color: #085298;"">01. I like to go to the Beitou hot springs.</span></strong></p>
<p>我喜歡去北投泡溫泉。</p>
<p>&nbsp;</p>
<p><strong> <span style=""color: #085298;"">02. I feel great after soaking in the hot springs. I feel entirely relaxed. </span></strong></p>
<p>泡完溫泉真舒服，感覺整個人都放鬆下來了。</p>
<p>&nbsp;</p>
<p><strong> <span style=""color: #085298;"">03. Green Island open-air hot springs is very famous.</span></strong></p>
<p>綠島的露天溫泉非常有名。</p>
<p>&nbsp;</p>
<p><strong> <span style=""color: #085298;"">04. There are more than 100 hot springs in Taiwan.</span></strong></p>
<p>全台灣有超過一百處以上的溫泉。</p>
<p>&nbsp;</p>
<p><strong> <span style=""color: #085298;"">05. Taiwan\'s hot springs offer many health benefits.</span></strong></p>
<p>台灣的溫泉對健康大大有益。</p>
<p>&nbsp;</p>
<p><img title=""泡溫泉英文怎麼說"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a0f051ab685c-tuLAZVDkzpCJTUV8SeU9mJLKzS49wz-600x315.jpeg"" alt=""泡溫泉英文怎麼說"" width=""600"" height=""315"" /></p>
<p><span style=""font-weight: 400;"">不知不覺間已經步入深秋時序，身邊的景色捎來信息，街角四處看到各種秋天的色彩。</span></p>
<p><span style=""font-weight: 400;"">趁著秋意深濃之際，趕快不遲疑出發吧！好好感受各地景色秋天的氣息，挑個喜愛的城市，選適合自己的泉質好好享受泡溫泉的樂趣，別忘了同時玩樂中練習這次分享的英文單字喔! *______*</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '台灣好湯季開跑，泡溫泉英文怎麼說？','og_description' => '台灣溫泉擁有多樣性泉質，不僅台灣人喜愛，也是觀光客來玩必朝聖之地。微涼秋意來臨，樹葉悄悄上了新裝，台灣好湯季早已陸續開跑，秉著玩樂Fun英文的概念，今天為大家整理與「泡溫泉」有關的英文單字。','meta_title' => '台灣好湯季開跑，泡溫泉英文怎麼說？','meta_description' => '台灣溫泉擁有多樣性泉質，不僅台灣人喜愛，也是觀光客來玩必朝聖之地。微涼秋意來臨，樹葉悄悄上了新裝，台灣好湯季早已陸續開跑，秉著玩樂Fun英文的概念，今天為大家整理與「泡溫泉」有關的英文單字。','canonical_url' => 'https://tw.english.agency/泡溫泉英文怎麼說','feature_image_alt' => '泡溫泉英文怎麼說','feature_image_title' => '泡溫泉英文怎麼說','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a13c8398c289-qsOMvqgcblE72q4B5noqv1lXKg4qbu-1200x630.png','author_id' => '10','category_id' => '10','created_at' => '"2017-11-17 15:11:38"'],
  ['id' => '78','title' => '找工作需要英文應徵信？自傳5步驟完成！','slug' => '找工作需要英文應徵信-自傳5步驟完成','excerpt' => '寫好英文應徵信，找工作的第一步就準備好了！不知道該如何下手？按照這5步驟，就可以完成自傳囉！','content' => '"<p>自傳撰寫，不僅是求職的第一步，也將有助於豐富自我介紹時的訊息。自傳不僅可以讓用人單位了解你的背景，更可讓你獲得面試的機會，甚至雀屏中選。通常需要英文自傳的公司，多是外商公司、大型企業或涉外單位。在眾多求職者中，英文自傳扮演敲門磚的角色，必須用心撰寫。在有限的篇幅裡，除了成長背景與求學歷程之外，更須強調你個人的特殊性及其與應徵職位之相關性，才能吸引人事專員與用人部門的目光。</p>
<p><br />其實，自傳撰寫並沒有固定書寫的格式，但請記住自傳就如廣告傳單，必須具備簡明、清晰與具吸引力的原則，請加入創意為自己加分。</p>
<h1><span style=""color: #5cacee;"">以下提供撰寫應徵信和自傳時的五個步驟：</span></h1>
<p><strong><span style=""color: #000080;"">Stage 1: Starting Writing (開始)</span></strong><br />Gathering all the information you need <br />收集所有相關訊息：包括你的個人經歷（personal history）、學歷、工作經歷（job history）、成就（achievements）與技能（skills）。<br />Putting your ideas on paper <br />寫下你的想法：自傳是屬於自己的行銷工具（marketing tool），認真誠實地闡述相關主題。<br />Ordering and classifying all the ideas <br />排序所有訊息的先後次序。</p>
<p><br /><strong><span style=""color: #000080;"">Stage 2: Composing (撰寫)</span></strong><br />Transferring your ideas into words <br />將心中的想法訴諸文字。<br />Emphasizing your competence and special events<br />強調能力與特殊事蹟：想看看你的能力或特殊事蹟可以讓你脫穎而出嗎？如果你是人事經理願意給這封自傳作者一個面試的機會嗎？<br />Thinking about who is going to read it <br />確認你的讀者是誰：撰寫自傳時，試圖將你自己置於雇主的角色，檢視你的內容是否引起或符合應徵公司的興趣與要求。<br />Developing the words into phrases and sentences<br />潤飾文句：辭意通暢的文句顯然比辭不達意、錯字連篇的自傳更具優勢。</p>
<p><br /><strong><span style=""color: #000080;"">Stage 3: Revising (修改)</span></strong><br />Reading through what you have written <br />從頭至尾詳讀：順一順自傳內容的流暢度。<br />Asking others to prove read it<br />邀請他人校閱：可以請老師協助檢閱，並給予寶貴意見。<br />Checking! Does it make sense?<br />檢查！確認內容表達是否合理、正確。</p>
<p><br /><strong><span style=""color: #000080;"">Stage 4: Editing (編輯)</span></strong><br />Can you improve them? <br />是否可以寫的更好：想看看是否可以用更文雅、專業的用詞取代通俗、不雅的文字。<br />How can layout and punctuation marks help? <br />格式：整體佈局可以更完善嗎？或是適當的標點符號有助於整篇文章的表達。<br />Are spelling and punctuation marks correct?<br />拼字與標點符號的使用正確嗎？切記：應徵信函與自傳要避免使用時下年輕人喜用的火星文。</p>
<p><br /><strong><span style=""color: #000080;"">Stage 5: Final Copy (完成)</span></strong><br />Writing up work for final presentation <br />成品：應徵信函或自傳完成後，請調整文件格式，讓整體外觀看起來更具吸引力。<br />Using word or PDF file <br />打成 Word 或PDF檔案，以利閱讀：若是你的英文書寫字體工整雅致，不妨試試以手寫代替打字，這樣也可以讓面試者印象深刻喔！</p>
<h2><span style=""color: #a42d00;"">單字解析</span></h2>
<p><br />transfer (v)：轉變成、移轉<br />Timo has transferred from the warehouse to the accounting department.（Timo已由倉庫調至會計部服務。）</p>
<p><br />develop (v)：發展、開發<br />EGA is developing a new business.（EGA 公司正開展一項新的業務。）<br />The corporate strategy gradually developed in the general manager&rsquo;s mind.（公司策略在總經理心中逐漸形成。）</p>
<p><br />layout (n)：佈局、圖樣<br />The plant layout is impressive.（工廠的佈局令人印象深刻。）</p>
<p><br />write up (ph.) 寫成作品<br />To write something again in a complete and useful form.<br />eg: To write up your notes.【Tips: eg = exempli gratia（拉丁文）例如】</p>
<p>摘自五南文化事業機構出版<a href=""http://www.wunanbooks.com.tw/product/9789571163840"" target=""_blank"" rel=""nofollow noopener noreferrer"">《商用英文：最佳商務往來溝通藝術》</a></p>"','published' => '1','og_title' => '找工作需要英文應徵信？自傳5步驟完成！','og_description' => '寫好英文應徵信，找工作的第一步就準備好了！不知道該如何下手？按照這5步驟，就可以完成自傳囉！','meta_title' => '找工作需要英文應徵信？自傳5步驟完成！','meta_description' => '寫好英文應徵信，找工作的第一步就準備好了！不知道該如何下手？按照這5步驟，就可以完成自傳囉！','canonical_url' => 'https://tw.english.agency/找工作需要英文應徵信-自傳5步驟完成','feature_image_alt' => '撰寫英文應徵信自傳','feature_image_title' => '撰寫英文應徵信自傳','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a17e68352391-BlfgKMKspkEXyOFFXG87WvrwUdGSiI-626x417.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-11-24 09:31:13"'],
  ['id' => '79','title' => '7個單字看懂英文氣象預報！Shower可不是指洗澡！','slug' => '7個單字看懂英文氣象預報-Shower可不是指洗澡','excerpt' => '掌握氣象預報在出國遊玩時更是重要，同時也是多益會出現的題型，來看看一些英文氣象預報常見說法，讓你出國玩也不會無所適從喔！','content' => '"<p>最近台灣天氣多變化，隨時掌握氣象預報來穿衣服可說是非常重要的，不然穿太多或穿太少，可能會導致身體無法負荷產生小毛病，加上最近流感盛行，更要懂得看天氣穿衣服。掌握氣象預報在出國遊玩時更是重要，同時也是<a href=""http://www.toeic.com.tw/"" target=""_blank"" rel=""noopener noreferrer"">多益</a>會出現的題型，今天我們來看看一些英文氣象預報常見說法，讓你出國玩也不會無所適從喔！</p>
<p><span style=""color: #333399;""><strong>Showers may continue in the far east overnight，but for many areas it will be dry，clear and frosty.</strong>&nbsp;</span> （夜裡在遠東區域將可能會持續有陣雨，但是在許多地區天氣將會是乾燥、晴朗和嚴寒的。）</p>
<p><span style=""color: #333399;""><strong>Shower</strong></span>這個字大家都懂，<span style=""color: #333399;"">take a shower</span>&nbsp;洗澡想必不陌生，但是當<span style=""color: #333399;"">shower</span>拿來當作天氣單字時，他指的其實是陣雨。其實就跟洗澡的概念一樣，洗澡時用蓮蓬頭一陣一陣的沖洗，就如同雨一陣一陣地下著，這樣一來也不難聯想了。這裡用複數想表達陣雨下的次數多於一次。</p>
<p><span style=""color: #333399;""><strong>Overnight</strong></span>是個很常見的副詞，有一夕之間、過夜的概念。</p>
<p>例句 1：<span style=""color: #333399;"">He became a millionaire overnight.</span> （他一夕之間變成了百萬富翁。）</p>
<p>例句 2 :&nbsp;<span style=""color: #333399;"">Lisa stayed at her best friend&rsquo;s place overnight.</span> （Lisa在他好朋友的家過夜。）</p>
<p>注意在第二個例句中還是必須要有一個動詞<span style=""color: #333399;"">stay</span>來表達待在那裡的意思，因此大家在使用上不會因為他的中文解釋而忽略詞性的重要性，造成句子的不完整甚至使用錯誤，這也是台灣英文學習者常犯的錯誤，還需注意。</p>
<p>後面用了幾個常見的形容詞描述天氣狀況，<span style=""color: #333399;"">dry</span>大家都知道是乾燥的意思，他也可以當動詞用， 例句：<span style=""color: #333399;"">Please dry your hands after you wash them.</span> （請在洗完手後擦乾。）</p>
<p><span style=""color: #333399;""><strong>Clear</strong></span>這個字雖然看起來簡單，卻常常造成學生誤解，主要是因為他在不同情境可以有不同意思，在天氣預報情境中他主要指的是晴朗的，在其他情境中卻有清楚的意思。</p>
<p>例句：<span style=""color: #333399;"">Do you think the teacher&rsquo;s explanation is clear?</span> （你認為老師的解釋清楚嗎？）</p>
<p>這例句裡的<span style=""color: #333399;"">clear</span>明顯不是晴朗的意思，所以只能解釋成是清楚的。由於在多益聽力測驗中前兩部份喜歡考混淆字或同音異義字，因此這要特別注意。</p>
<p>另外這個<span style=""color: #333399;"">clear</span>也可當成動詞，有清除之意。</p>
<p>例句：<span style=""color: #333399;"">Please clear the hallway，so the gurney can come through.</span></p>
<p>（請將走道淨空，這樣病床才能夠穿過。）</p>
<p>另外在多益閱讀部分常有拍賣的情境，其中將這個單字稍做變化後產生的單字就是<span style=""color: #333399;"">clearance</span>，代表清空、清倉的意思，因此<span style=""color: #333399;"">clearance sale</span>就是清倉大拍賣，而<span style=""color: #333399;"">clearance products</span>則是清倉貨品。</p>
<p>最後<span style=""color: #333399;"">frosty</span>這個單字來自於<span style=""color: #333399;"">frost</span>這個名詞，意思是霜，所以就是嚴寒的意思，同時也代表天氣冷到結霜的概念。英文單字通常名詞或動詞後面加上「<span style=""color: #333399;"">-y</span>」常常會改變詞性，變成了形容詞，如<span style=""color: #333399;"">sweat vs. sweaty/&nbsp;wind vs. windy/ mood vs. moody/ bump vs. bumpy</span>等等都是類似的意思，藉由這樣的方法增加單字才能迅速有效。</p>
<p><span style=""color: #333399;""><strong>Gusty winds will ease inland，whilst remaining windy along the coast.</strong>&nbsp;</span> （內陸的陣陣強風將會減緩，不過沿海地區還是維持風大的情況。）</p>
<p><span style=""color: #333399;""><strong>Gusty</strong></span>其實也正是來自於<span style=""color: #333399;"">gust</span>這個名詞的形容詞，一陣陣強風就可以用<span style=""color: #333399;"">gust</span>來形容。</p>
<p>例句：<span style=""color: #333399;"">A sudden gust of wind blew the door shut.</span></p>
<p>（突然一陣強風將門一吹關上了。）</p>
<p>這裡則是採用字尾加上「<span style=""color: #333399;"">-y</span>」變成形容詞的用法，<span style=""color: #333399;"">gusty winds</span>就是陣陣強風的意思。</p>
<p>後面有個動詞大家應該覺得面熟，<span style=""color: #333399;""><strong>ease</strong></span>其實就是跟大家熟知的<span style=""color: #333399;"">easy</span>同樣家族的單字，後者大家知道是形容詞，前者則是動詞，「使～變簡單」所以有減緩的意思。</p>
<p>例句：<span style=""color: #333399;"">The medicine will ease your pain.</span> （這個藥將會減緩你的痛苦。） 這</p>
<p>邊的變化變成去「<span style=""color: #333399;"">-y</span>」加「<span style=""color: #333399;"">-e</span>」，不過廣義來說也是符合前面提到的變化模式。</p>
<p>最後要說一個非常重要的動詞就是<span style=""color: #333399;""><strong>remain</strong></span>，也就是保持、保留的意思。從上面的天氣預報來看，動詞後面加上形容詞的<span style=""color: #333399;"">remaining windy</span>，這種動詞廣義來說屬於<span style=""color: #333399;"">linking verbs</span>&nbsp;連綴動詞，用法主要是連結主詞和後面的受詞，通常是形容詞或名詞，因此後面的受詞其實是用來修飾主詞的補語，所以不能使用一般副詞修飾動詞的用法。</p>
<p>例句 1：<span style=""color: #333399;"">He remained a good student in college.</span></p>
<p>（他大學時期維持好學生的樣子。）</p>
<p>例句 2：<span style=""color: #333399;"">Please remain seated while we change the batteries of the projector.</span> （麻煩大家在我們更換投影機的電池時，維持坐在原位上。）</p>
<p>&nbsp;</p>
<h1><strong>多益模擬試題</strong></h1>
<p><strong>接下來我們來看看這則出現在多益聽力測驗中，關於天氣的一段內容，試著解答下面兩道問題吧！</strong></p>
<p><span style=""color: #333399;"">Coast weather will continue to be warm and sunny for the next two days. At the weekend，though，clouds will be moving in on the north part of the coast. The temperatures there will drop into the low 70s，with showers on Saturday and possible thunder storms on Sunday. On the south coast，the sun should continue through the weekend，with highs in the middle 80s. At the start of next week，we expect some clearing in the north，with rain in the mornings and partly sunny skies in the afternoons. The highs in the north will gradually climb into the 80 again by this time next week. In the south，next week looks to be much like this one has been so far.</span></p>
<p><span style=""color: #333399;"">1. What is the main purpose of the report?</span></p>
<p><span style=""color: #333399;"">(A) To forecast the weather</span></p>
<p><span style=""color: #333399;"">(B) To warn listeners of a storm</span></p>
<p><span style=""color: #333399;"">(C) To broadcast the news</span></p>
<p><span style=""color: #333399;"">(D) To update traffic</span></p>
<p><span style=""color: #333399;"">2. When is rain predicted on the north part of the coast?</span></p>
<p><span style=""color: #333399;"">(A) The next two days</span></p>
<p><span style=""color: #333399;"">(B) Friday</span></p>
<p><span style=""color: #333399;"">(C) On the weekend</span></p>
<p><span style=""color: #333399;"">(D) Next week</span></p>
<p>&nbsp;</p>
<p><strong>解析</strong></p>
<p>1. 這題問「這邊報告的主要目的為何？」說話者用了各種不同的地區名詞表達不同區域的天氣狀況，如<span style=""color: #333399;"">Coast</span>、<span style=""color: #333399;"">the north part of the coast</span>、<span style=""color: #333399;"">On the south coast</span>、<span style=""color: #333399;"">in the north、in the south</span>等，並使用不同形容詞或名詞來代表天氣狀況，如<span style=""color: #333399;"">warm and sunny</span>、<span style=""color: #333399;"">clouds</span>、<span style=""color: #333399;"">showers</span>、<span style=""color: #333399;"">thunder storms</span>、<span style=""color: #333399;"">clearing、highs</span>，在在都顯示這是篇氣象預報，因此答案要選(A)。</p>
<p>2. 這題問「北部沿海地區預計何時會降雨？」後面提到<span style=""color: #333399;"">At the weekend，though，clouds will be moving in on the north part of the coast. The temperatures there will drop &hellip; with showers on Saturday and possible thunder storms on Sunday.</span>&nbsp;（週末期間北部沿海地區會有雲，氣溫會降至&hellip;週六會有陣雨而週日會有雷雨。）這裡很明顯地指出北部沿海地區週末氣候，其中最後兩句提到週末會有陣雨和雷雨，因此答案要選(C)才對。</p>
<p>Photo:&nbsp;<a href=""https://www.flickr.com/photos/kurt-b/15246305669/"" target=""_blank"" rel=""noopener noreferrer"">Kurt Bauschardt</a>&nbsp;，CC Licensed</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '7個單字看懂英文氣象預報！Shower可不是指洗澡！','og_description' => '掌握氣象預報在出國遊玩時更是重要，同時也是多益會出現的題型，來看看一些英文氣象預報常見說法，讓你出國玩也不會無所適從喔！','meta_title' => '7個單字看懂英文氣象預報！Shower可不是指洗澡！','meta_description' => '掌握氣象預報在出國遊玩時更是重要，同時也是多益會出現的題型，來看看一些英文氣象預報常見說法，讓你出國玩也不會無所適從喔！','canonical_url' => 'http://www.toeicok.com.tw/7%E5%80%8B%E5%96%AE%E5%AD%97%E7%9C%8B%E6%87%82%E8%8B%B1%E6%96%87%E6%B0%A3%E8%B1%A1%E9%A0%90%E5%A0%B1-shower%E5%8F%AF%E4%B8%8D%E6%98%AF%E6%8C%87%E6%B4%97%E6%BE%A1/','feature_image_alt' => '天氣預報學英文','feature_image_title' => '天氣預報學英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a17ec94c53d0-nOdRx6zDuLVYL5GDupI4kC6mKCZUDe-640x476.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2017-11-24 09:55:57"'],
  ['id' => '80','title' => '給上班小資族，陪你學習英文的5個免費好工具','slug' => '小資族免費學習英文工具','excerpt' => '自我學習英文的方法有很多，但對於忙碌工作又常態加班的台灣小資族群來說時間與荷包都一樣寶貴，有甚麼好工具能免費學習英文呢，且每天不會花太多時間呢？最好玩的英文免費學習工具一起來看看！','content' => '"<p><span style=""font-weight: 400;"">自我學習英文的方法有很多，聽英文歌曲、看外國影集，或者透過玩線上遊戲互動學習。但對於忙碌又常態加班的台灣小資族群來說，每天這麼忙，下班回家又疲累，周末如果能不加班當然要出去玩！如果想學英文要怎麼偷出時間，又有甚麼好工具能省荷包免費學習英文呢？</span></p>
<h2><strong>特搜好看又好玩的英文免費學習工具，一起來看看！</strong></h2>
<p><span style=""font-weight: 400;"">上班小資族每天除了辛勞的工作之餘，社群工具也漸漸成為生活中很重要的一環，既然生活離不開社群，那麼好好利用社群工具來免費學習英文就對了。</span></p>
<p>&nbsp;</p>
<h3><strong>學習英文｜免費好工具1號｜FB｜<span style=""color: #f28f00;"">那些電影教我的事- lessons from movies</span></strong></h3>
<p><span style=""font-weight: 400;"">比起制式的英文教材，透過每部電影的精采佳句來學英文真的有趣多了！</span></p>
<p><span style=""font-weight: 400;"">▶哪裡找：喜愛電影的小資族只要在Facebook搜尋「那些電影教我的事 - Lessons from Movies」，然後按讚表示追蹤此粉絲專業，之後粉專只要發佈新貼文，馬上就能在自己的動態版上看到訊息。</span></p>
<p><span style=""font-weight: 400;"">每篇貼文除了引用電影中的經典佳句之外，還會帶上電影介紹連結與電視播出時間，非常實用的資訊。</span></p>
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b0115b7f36-T9E2D1vjkzfXjXjqemmwEdo483ziOb-600x352.jpeg"" alt="""" width=""600"" height=""352"" /></p>
<p><img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b017db957f-gt9EL6e5IAWwKCVEoMcNaoGNsmj4tU-500x427.jpeg"" alt=""小資族免費學習英文工具"" width=""500"" height=""427"" /></p>
<h3>&nbsp;</h3>
<h3><br /><strong>學習英文｜免費好工具2號｜FB｜</strong><strong><span style=""color: #f28f00;"">BBC Travel </span></strong></h3>
<p><span style=""font-weight: 400;"">「如果不在家，就在旅遊路上的你」這款跟小編一樣，非玩不可的小資族，真的快來按讚追蹤。</span></p>
<p><span style=""font-weight: 400;"">▶哪裡找：在Facebook搜尋「</span><span style=""font-weight: 400;"">BBC Travel</span><span style=""font-weight: 400;"">」&rarr;按讚 Like &rarr;追蹤此粉絲專頁，一樣新貼文會自動出現在動態版上面。</span></p>
<p><span style=""font-weight: 400;"">透過每篇貼文的短句，以及每篇文章對於許多城市、國家的深度介紹都讓人好著迷，且拍攝照片張張都是精心安排，光看照片心情都美好起來。</span></p>
<p><br /><img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b0203a0f6a-VzUQ6Gai1XKQhqz3MdE220ebokInVO-600x469.jpeg"" alt=""小資族免費學習英文工具"" width=""600"" height=""469"" /><br /><br /><br /></p>
<p><img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b0219d7daf-bRM06VxvGXzvQtwGxF8ckdKKlPOGl0-600x291.jpeg"" alt=""小資族免費學習英文工具"" width=""600"" height=""291"" /></p>
<p>&nbsp;</p>
<h3><strong>學習英文｜免費好工具3號 ｜APP｜<span style=""color: #f28f00;"">LINE每日英文</span></strong></h3>
<p><span style=""font-weight: 400;"">時下不可或缺的聯繫好工具-LINE，幾乎成為每天起床睡醒滑開手機第一個接觸的社群工具，小資族必載必加入的好工具。</span></p>
<p><span style=""font-weight: 400;"">▶哪裡找：必須先下載LINE的APP&rarr;使用者只需加入 LINE 好友，每天就可以收到各種實用的英文學習主題。</span></p>
<p><span style=""font-weight: 400;"">「LINE每日英文」的內容是由空中英語教室獨家提供，每天都會自動送上一篇英文教學，簡單的從單字、句型及片語，只要花一點點時間就可以達到馬上學習效果。</span></p>
<p><img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b044042284-176SiQREzIUJEY0Vjx33MzePfeuLK4-300x519.jpeg"" alt=""小資族免費學習英文工具"" width=""300"" height=""519"" />&nbsp; &nbsp; &nbsp; &nbsp;<img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b04a681104-mfivM0MAs3edcQzjn7p53cJb1lfMh9-300x496.png"" alt=""小資族免費學習英文工具"" width=""300"" height=""496"" /><br /><br /></p>
<p>&nbsp;</p>
<h3><strong>學習英文｜免費好工具4號 ｜APP｜<span style=""color: #f28f00;"">VoiceTube</span></strong></h3>
<p><span style=""font-weight: 400;"">上班族的疲累與壓力就靠這個服務平台來紓壓吧！邊學英文邊吸收各地的知識，這裡有豐富的分類、大量的影音收集，最新最有話題的音樂MV，加上對於初學者及中高級程度都有清楚分類，真的是一個非常大心且貼心的服務。</span></p>
<p><span style=""font-weight: 400;"">▶哪裡找：</span></p>
<p><span style=""font-weight: 400;"">Android :</span> <a href=""https://goo.gl/WJTX3o""><span style=""font-weight: 400;"">https://goo.gl/WJTX3o</span></a></p>
<p><span style=""font-weight: 400;"">iOS :</span> <a href=""https://goo.gl/Z700gw""><span style=""font-weight: 400;"">https://goo.gl/Z700gw</span></a></p>
<p><span style=""font-weight: 400;"">此服務是由台灣團隊開發的平台，網站與 App 都能免費註冊使用，以下為APP圖片介紹</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;""><br /><img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b057d62d30-abCtrLAMhajFLeyjUsfQVySQjZPLz1-300x519.png"" alt=""小資族免費學習英文工具"" width=""300"" height=""519"" />&nbsp; &nbsp; &nbsp;<img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b0593858d8-JFO0AGBMD66ja9UIacEPSo1Q4Zm1nM-300x519.png"" alt=""小資族免費學習英文工具"" width=""300"" height=""519"" /><br /></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3><strong>學習英文｜免費好工具5號 ｜APP｜<span style=""color: #f28f00;"">Vocabulazy</span></strong></h3>
<p><span style=""font-weight: 400;"">背單字真的是背了又忘，忘了又背嗎?好用的背單字APP正適合忙碌的上班小資族，免費好用的英文單字學習，不僅主題多樣化，使用介面簡單操作好入門。</span></p>
<p><span style=""font-weight: 400;"">▶哪裡找：</span></p>
<p><span style=""font-weight: 400;"">Android :</span><a href=""https://goo.gl/519CKe""><span style=""font-weight: 400;"">https://goo.gl/519CKe</span></a></p>
<p><span style=""font-weight: 400;"">iOS :</span><a href=""https://goo.gl/6G4FqU""><span style=""font-weight: 400;"">https://goo.gl/6G4FqU</span><span style=""font-weight: 400;""><br /><br /></span></a></p>
<p><img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b062825f9b-5u88ughAjmjuV0kkqeBTozpJO8on0z-300x533.png"" alt=""小資族免費學習英文工具"" width=""300"" height=""533"" />&nbsp; &nbsp;&nbsp;<img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b06366916d-vWDiEReFse4PynlBgerrZySBv79DZ1-300x519.png"" alt=""小資族免費學習英文工具"" width=""300"" height=""519"" />&nbsp; &nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><img title=""小資族免費學習英文工具"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1b063b4aa30-DDlYKaY6tH1m9sSX3isAL1cwV60x0B-300x519.png"" alt=""小資族免費學習英文工具"" width=""300"" height=""519"" /><br /><br /><br /><br /></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">讀完以上的5個好工具，是否挑選到自己喜愛的方式了呢？學習語言的最好捷徑，除了找感興趣、較輕鬆的方式之外，對於上班小資族來說，更重要的學習動力，就是找到適合自己的免費學習英文工具，既不傷荷包又能讓自我英文能力提升。剩下的就是每天都出一點時間，即使通勤的10~15分鐘，還是午餐時間的空檔(例如等餐點來的時間)...等等。</span></p>
<p><span style=""font-weight: 400;"">相信每天的一小步，都是累積自我英文實力的一大步。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '給上班小資族，陪你學習英文的5個免費好工具','og_description' => '自我學習英文的方法有很多，但對於忙碌工作又常態加班的台灣小資族群來說時間與荷包都一樣寶貴，有甚麼好工具能免費學習英文呢，且每天不會花太多時間呢？最好玩的英文免費學習工具一起來看看！','meta_title' => '給上班小資族，陪你學習英文的5個免費好工具','meta_description' => '自我學習英文的方法有很多，但對於忙碌工作又常態加班的台灣小資族群來說時間與荷包都一樣寶貴，有甚麼好工具能免費學習英文呢，且每天不會花太多時間呢？最好玩的英文免費學習工具一起來看看！','canonical_url' => 'https://tw.english.agency/小資族免費學習英文工具','feature_image_alt' => '小資族免費學習英文工具','feature_image_title' => '小資族免費學習英文工具','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1ad8dee5d4f-NAKWt66SW823710UncH3HP9sVJq7ri-1200x630.jpeg','author_id' => '10','category_id' => '19','created_at' => '"2017-11-26 15:08:33"'],
  ['id' => '81','title' => '跟著《正義聯盟》英雄一起學多益單字！','slug' => '跟著正義聯盟英雄一起學多益單字','excerpt' => '"電影「正義聯盟」這次讓英雄們齊聚，並由「蝙蝠俠」領軍，帶著今年夏天爆紅的神力女超人（Wonder Woman），加上另外三位初登場的英雄與邪惡勢力對抗。接著，就讓我們在英雄們與邪惡勢力的對抗中學習多益單字吧！"','content' => '"<p>英雄電影近年正夯，Marvel Comics旗下的「復仇者聯盟」拍了三集，DC Comics也終於推出了旗下的英雄電影《正義聯盟》（Justice League），上周在台灣上映，票房亮眼。</p>
<p>電影「正義聯盟」這次讓英雄們齊聚，並由「蝙蝠俠」領軍，帶著今年夏天爆紅的神力女超人（Wonder Woman），加上另外三位初登場的英雄與邪惡勢力對抗。接著，就讓我們在英雄們與邪惡勢力的對抗中學習多益單字吧！</p>
<p>在電影《正義聯盟》中，蝙蝠俠Bruce Wayne說：I believe that an enemy is coming from far away. I ́m looking for warriors &hellip; I ́m building an alliance to defend us.（敵人已從遠方逼進，我要尋找戰士並建立聯盟來共同抵抗外敵。）</p>
<p><strong><span style=""color: #a42d00;"">alliance（聯盟）</span></strong><br />Alliance是名詞「聯盟」之意，也就是「夥伴關係（partnership）」的意思；而動詞則為ally，有「結合、聯合」之意。另外ally若當作名詞，可做為「盟友」解釋。</p>
<p>因此，例如在第二次世界大戰時，和美國並肩作戰的同盟國即稱為The Allies。e.g. You need to form a close alliance with your colleagues if you want to improve your team work.（如果你想要改善你們的團隊工作效率，你需要和你的同事建立更緊密的合作關係。）</p>
<p>除此之外，coalition也是類似的意思，指的是合作、聯合之意，其他常用的近似字還包括：</p>
<p><strong><span style=""color: #a42d00;"">league（聯盟）</span></strong><br />如我們所談論的Justice League（正義聯盟），或是喜歡看棒球的人都知道的Major League Baseball（美國大聯盟），另外還有學生們熟悉的Ivy League（常春藤盟校）。</p>
<p><strong><span style=""color: #a42d00;"">crew（全體人員）</span></strong><br />常搭飛機旅行的人會聽到機長說：Cabin crew ，please take your seats for takeoff.（機艙組員，請坐好準備起飛。）</p>
<p><strong><span style=""color: #a42d00;"">squad（團隊）</span></strong><br />例如啦啦隊可以叫作cheerleading squad，另外常出現在新聞和電影中，隸屬於警局的拆炸彈團隊則可用the bomb squad。</p>
<p>DC英雄 有何不同？<br />DC Comics的正義聯盟最開始時是由七位英雄組合而成，包括超人、蝙蝠俠、神力女超人、水行俠、閃電俠、綠光戰警以及火星獵人，後來陸續更新部分成員，大家或許會好奇，除了一些無與倫比的超能力之外，DC Comics的超級英雄們還有什麼特別之處？在正義聯盟中，閃電俠Barry Allen問蝙蝠俠Bruce Wayne：</p>
<p>Barry Allen：What are your superpowers again?（你說你的超能力是什麼？）<br />Bruce Wayne：I ́m rich.（我超有錢。）</p>
<p>除此之外，英雄們的能力似乎是與生俱有的，如神力女超人，有近似於神一般的特質。因此我們可以觀察到DC的英雄們個性比較壓抑、冷靜，在情緒上不會有太大的波動。而且他們通常來自一個虛構的地方，故事也發生在虛構的城市如 Gotham City（Batman）、Metropolis（Superman），故事當中的壞人也常常會被抓住，關在虛構的地方（Arkham Asylum），但終究會越獄逃脫，逍遙法外。</p>
<p>電影《正義聯盟》除了蝙蝠俠與神力女超人外，還加入了閃電俠水行俠及鋼骨，經典英雄一字排開讓影迷直呼期待，而英雄們出不完的任務，以及與邪惡的黑暗勢力一場又一場的戰爭，更是讓我們有永遠看不完的英雄電影。</p>
<h1><span style=""color: #000080;"">多益測驗練習題</span></h1>
<p><br />1.Japan has been our closest __________ for several decades.<br />(A)alley<br />(B) alliance<br />(C)ally<br />(D)allies</p>
<p>2. Rachel is also __________with her appearance. She spends 90 minutes in the bathroom every morning before she goes out.<br />(A) oblivious<br />(B) obsessed<br />(C) obliterate<br />(D) omission</p>
<p>解析<br />1. 正解為(C)。選項(A)是巷子；(B)是聯盟、合作；(D)則是(C)盟友的複數，日本是單一盟友，所以應選單數(C)。本題為字義題，所有選項都是名詞，因此無法由詞性判斷出正確答案，而且這幾個字剛好是拼字類似、易混淆字。此題句意為「日本幾十年來一直是我們最親近的盟友。」</p>
<p>2. 正解為(B)著迷的。選項(A)oblivious解釋為「視而不見的」，oblivious後面基本上要接介系詞 to；(C) obliterate (v)「摧毀，破壞」；(D)omission (n)「省略，漏掉」，動詞為omit。本題為字義題，要選適合題意的字。因此符合本題題意的應是(B)。<br />本題句意為「瑞秋非常著迷於她的外表，每天早上出門前，她都會花90分鐘在浴室裡打理自己。」</p>
<p>文／徐碧霞</p>
<p>photo: Justice League FB fanpage</p>
<p>&nbsp;</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '跟著《正義聯盟》英雄一起學多益單字！','og_description' => '"電影「正義聯盟」這次讓英雄們齊聚，並由「蝙蝠俠」領軍，帶著今年夏天爆紅的神力女超人（Wonder Woman），加上另外三位初登場的英雄與邪惡勢力對抗。接著，就讓我們在英雄們與邪惡勢力的對抗中學習多益單字吧！"','meta_title' => '跟著《正義聯盟》英雄一起學多益單字！','meta_description' => '"電影「正義聯盟」這次讓英雄們齊聚，並由「蝙蝠俠」領軍，帶著今年夏天爆紅的神力女超人（Wonder Woman），加上另外三位初登場的英雄與邪惡勢力對抗。接著，就讓我們在英雄們與邪惡勢力的對抗中學習多益單字吧！"','canonical_url' => 'http://www.toeicok.com.tw/%E4%BD%A0%E7%9C%8B%E6%AD%A3%E7%BE%A9%E8%81%AF%E7%9B%9F%E4%BA%86%E5%97%8E-%E8%B7%9F%E8%91%97%E8%8B%B1%E9%9B%84%E5%AD%B8%E5%A4%9A%E7%9B%8A%E5%96%AE%E5%AD%97/','feature_image_alt' => '正義聯盟','feature_image_title' => '正義聯盟','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a1fd2075bcc5-Q98Z0UnixW5BqXogLBknIqAiFKstsY-1117x636.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2017-11-30 09:37:54"'],
  ['id' => '82','title' => '即使我英文不好，我還是能培養孩子的英語競爭力！','slug' => '如何幫助小孩學好英文','excerpt' => '栽培孩子學好英文是每位家長的心聲，有哪些方法與管道能幫助家長找到好工具呢？如果一開始沒有太多的預算是否就無法開啟讓孩子好好學英文呢？今天整理了4大方向，幫助家長為孩子打造英語競爭力！','content' => '"<h2><strong>陪伴，永遠是孩子最好的禮物</strong></h2>
<p><span style=""font-weight: 400;"">全球化經濟興起，觸發許多家長對孩子學好英文的期待，卻常擔心自己的英文能力不夠好，缺乏信心教導孩子學好英文。但實際上，每個</span><span style=""font-weight: 400;"">小小孩天生就喜歡父母的聲音，親子間像是有種魔力，只要父母能一直牽引著孩子往前走，每一跨步都是最好的禮物！</span></p>
<p>&nbsp;</p>
<h2>&nbsp;<img title=""如何幫助小孩學好英文"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a204b0d180df-PraJd35tMuafa2zpOX8pecdcR2OALj-600x315.jpeg"" alt=""如何幫助小孩學好英文"" width=""600"" height=""315"" /></h2>
<p>&nbsp;</p>
<hr />
<h2><strong>掌握4方向，打造孩子英文學習計畫</strong></h2>
<p><span style=""font-weight: 400;"">既然栽培孩子的心意是每位家長的心聲，那到底有哪些方法與管道能幫助家長找到好工具呢？如果一開始沒有太多的預算是否就無法開啟讓孩子好好學英文呢？</span></p>
<p><span style=""font-weight: 400;"">以下幫家長們整理了4大方向，用經濟的方式也可以幫助孩子學好英文！</span></p>
<p>&nbsp;</p>
<h2><img title=""如何幫助孩子學好英文"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a2096d568c9d-ZUq4EckQJfi8ZKPby2HM9TSUrhtucS-600x338.jpeg"" alt=""如何幫助孩子學好英文"" width=""600"" height=""338"" />&nbsp;</h2>
<h3><strong><span style=""color: #f28f00;"">善用免費工具 &nbsp;X 啟發孩子學英文樂趣</span></strong></h3>
<p>▶適合族群有誰呢：超低預算家長</p>
<p>▶超實用管道報報：地方圖書館、學英文APP、網路英文歌謠頻道...等等。</p>
<p>&nbsp;</p>
<p>身在網路學習資源多元的年代，有太多免費的好資源值得探索，其中最好的免費工具，首推的就是「地方圖書館」。許多研究統計，父母陪伴孩子共讀，學習成效都是大加分的，然而透過地方資源(例如：新北市立圖書館)可以免費借閱許多很棒的英文繪本，且分類多元、借閱方便，只要一個月借閱10本，以單月內一起共讀10本為目標輪流閱讀，主要不求一定都完全看懂或是能念出發音，最重要的是透過此共讀習慣，幫助孩子建立學英文的樂趣。</p>
<p><br /><br /></p>
<h3><strong><span style=""color: #f28f00;"">活用趣味資源 X 引導孩子從聽說開始學英文</span></strong></h3>
<p>▶適合族群有誰呢：有少少預算家長</p>
<p>▶超實用管道報報：訂閱親子相關共學社團、找適合的繪本教具團購、二手書書展...等等。</p>
<p>&nbsp;</p>
<p>孩子學英文，建議從聽說開始打造基礎，就像我們小時候學ㄅㄆㄇ一樣，也是先大量聽到很多大人說+自己牙牙模仿學發音開始。所以如果能有一小筆預算的家長，建議找尋一些有趣的繪本並搭配CD、定時定量的陪伴孩子邊聽邊共讀。或是有趣的互動教具， 例如字母或拼音積木...等等，都能激發孩子的興趣，且引導孩子習慣「聽」、習慣「英文腦思考」。</p>
<p>&nbsp;&nbsp;</p>
<h3><strong><span style=""color: #f28f00;"">觀察孩子喜好 X 培養英文學習熱枕</span></strong></h3>
<p>▶適合族群有誰呢：小資家長、每月可固定撥出學英文基金的家長</p>
<p>▶超實用管道報報：參加英文繪畫課、英文體育課程(屬短期，大約一週一次)</p>
<p>&nbsp;</p>
<p>近年來安排孩子到美術教室學畫畫且結合學英文的課程，愈來愈受歡迎，其中真諦雖不是讓孩子學習文法或大量英文會話，但是透過將「興趣」結合的學習力的確不容小覷。因為如果繪畫是孩子的喜好，那麼從玩樂中學那是最開心的事了！讓孩子自然且開心的學習，成效最好。<br /><br /></p>
<p>&nbsp;</p>
<h3><strong><span style=""color: #f28f00;"">創造情境學習 X 打造孩子的國際英語競爭力</span></strong></h3>
<p>▶適合族群有誰呢：有多一點預算的家長</p>
<p>▶超實用管道報報：報名英文短期營隊(例如：寒暑假營)、親子遊學、菲律賓遊學密集英文</p>
<p>&nbsp;</p>
<p>身在非英語系國家學習英文的最主軸問題，常常是因為缺乏英語環境。如果能幫孩子創造出更多的情境，鼓勵孩子自然開口使用英文，且不懼怕開口說英文，想必這是很大的一步。所以透過團體互動的英文營隊方式，大量且密集在團體中說英文，孩子受到同儕相互鼓勵往往變成學習的動力。</p>
<p>近期除了親子遊學，也有更經濟且時間更有效率的菲律賓遊學漸受到大家歡迎，除了菲律賓就在亞洲地區飛行距離縮短之外，短期且密集式的學習方式，打造聽說讀寫各層面的好基礎，且同時能培養孩子的國際觀，相信也是值得考慮的方向之一喔！</p>
<p>&nbsp;</p>
<p>今天分享的以上4個大方向，視家長的時間、經濟狀況由淺至深都能搭配一起並用，如果能從基本的陪伴共讀開始，讓孩子先固定投入對英文學習的時間，將會是一個很好的習慣建立，希望大家都能找到適合自己與孩子良好的互動方式！</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '即使我英文不好，我還是能培養孩子的英語競爭力！','og_description' => '栽培孩子學好英文是每位家長的心聲，有哪些方法與管道能幫助家長找到好工具呢？如果一開始沒有太多的預算是否就無法開啟讓孩子好好學英文呢？今天整理了4大方向，幫助家長為孩子打造英語競爭力！','meta_title' => '即使我英文不好，我還是能培養孩子的英語競爭力！','meta_description' => '栽培孩子學好英文是每位家長的心聲，有哪些方法與管道能幫助家長找到好工具呢？如果一開始沒有太多的預算是否就無法開啟讓孩子好好學英文呢？今天整理了4大方向，幫助家長為孩子打造英語競爭力！','canonical_url' => 'https://tw.english.agency/如何幫助小孩學好英文','feature_image_alt' => '如何幫助小孩學好英文','feature_image_title' => '如何幫助小孩學好英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a202499ad72e-l3uJM6gsja4yd3HkerGRp3aONt7M5j-1200x630.jpeg','author_id' => '10','category_id' => '19','created_at' => '"2017-11-30 15:32:57"'],
  ['id' => '83','title' => '"在 We English 語言學校體驗小小世界村，菲律賓克拉克遊學等你衝"','slug' => 'We-English-語言學校-上班族進修','excerpt' => '"在菲律賓國際旅遊與商務中心的克拉克遊學，We Academy 是一所價格優惠的鄉村式學院，提供最優質的多元化教育，提供各種一對一課程，包含ESL、雅斯、多益、托福，教師超過十年教學經驗。"','content' => '"<p><br />海外遊學早已盛行多年，不僅年輕學子趨之若鶩，也吸引許多社會新鮮人和就業多年的上班族。近年異軍突起的菲律賓，英語教學早已成為一個產業，每年粗估十萬日韓學生，都前往菲律賓進修，不僅平均學費較歐美來得低之外，語言學校更會提供餐食、宿舍及專人負責打掃房間及洗衣服等，不用自行打理生活起居，更能專心投入語言學習，也因此成為亞洲學生遊學的新興國家。</p>
<p>&nbsp;</p>
<h2>菲律賓克拉克溫馨語言學校 與世界當朋友</h2>
<p>上班族進修何處去？想在菲律賓進修，菲律賓向來是年輕人相當喜歡的地區，菲律賓的克拉克 (Clark) 地區，曾是美軍駐菲律賓的空軍基地，於1993年設立菲律賓克拉克經濟特區（Clark Special Economic Zone）。曾經是美國海軍在亞洲最大的基地的她，現已成為一個國際旅遊與商務中心，吸引著來自世界各國的遊客前來。特區內風景優美、航運發達、各種設施齊全，是菲律賓的旅遊勝地之一，也是菲律賓遊學的熱門選項。</p>
<p>想在旅遊與商務活動發達的菲律賓克拉克遊學，We English 是不錯的選擇！We English 是一所價格優惠的鄉村式學院，並提供最優質的多元化教育，提供各種一對一課程，包含ESL、雅斯、多益、托福與各種個人需求課程，教師皆超過十年教學經驗。</p>
<p><img title=""We English 語言學校上班族進修一對一課程"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a20c57e589b0-KtuRpPmq0xb7WCfKTB1SFyxhF1PG9I-850x600.png"" alt=""We Academy 語言學校上班族進修一對一課程"" width=""850"" height=""600"" /></p>
<p>We English 一對一課程</p>
<p>&nbsp;</p>
<p>教師們是通過八個階段考試篩選，並通過Tutor教育系統，學校也是教育認證通過單位通過TESDA（菲律賓教育部）和SSP（留學許可證）。週一到週四有開設免費的特別講座、學生1天最多可安排6小時的一對一教學，特別適合需要長時間一對一授課且有低學費需求的同學。</p>
<p>&nbsp;</p>
<p><img title="" We English 菲律賓克拉克進修宿舍實景"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a20c623d7707-kP11FMZDgfSH3Yx69IcUBUBaPDIrcM-850x600.png"" alt="" We Academy 菲律賓朗怡進修宿舍實景"" width=""850"" height=""600"" /></p>
<p>We English 宿舍實景</p>
<p>&nbsp;</p>
<p>為了不讓來自世界各地的學生有身在異鄉的孤獨與不安，除了建造廣闊校地的鄉村式生活環境，校長更致力於打造居家環境氣氛的學校，也因此讓學生與教師，甚至學校所有教職人員都相當親近，在融洽氣氛的環境中下，同學們更能輕鬆無旁貸的專心學習。保持與其他學校的師資優勢是 We English 的辦學理念，注重在校學員的課程滿意度和獲得學員的延長學習時間，宿舍安排上也可見其用心程度，儘量安排不同國籍的學員作為室友，讓所有學生以全天候日常英語環境下學習與生活體驗。</p>
<p><img title="" We English 上班族進修校園設備"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a20c5d7b2c8b-XLrhFTXN4JZEJ3YP73cXYNYte7MLjO-850x600.png"" alt="" We Academy 上班族進修校園設備"" width=""850"" height=""600"" /></p>
<p>We English 校內涼亭</p>
<p>&nbsp;</p>
<h2>語言能力與文化衝擊一次感受</h2>
<p>曾經被美國殖民過的菲律賓，早已是東南亞英文最好的國家，We English 的所有師資皆受過教育訓練，大部分講的英文俚語特點都是美式英文為主，因而學校亦會定時訓練老師、讓老師本身提升自我能力，如想短期海外語言密集學習，前往菲律賓克拉克遊學肯定是最佳選擇，更是上班族提升自我語言能力的好去處喔！</p>
<p>-</p>
<p>前往官網深入了解 <a href=""https://we-academy.english.agency"" target=""_blank"" rel=""noopener noreferrer"">We English 語言學校</a></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"在 We English 語言學校體驗小小世界村，菲律賓克拉克遊學等你衝"','og_description' => '"在菲律賓國際旅遊與商務中心的克拉克遊學，We English是一所價格優惠的鄉村式學院，提供最優質的多元化教育，提供各種一對一課程，包含ESL、雅斯、多益、托福，教師超過十年教學經驗。"','meta_title' => '"在 We English 語言學校體驗小小世界村，菲律賓克拉克遊學等你衝"','meta_description' => '"在菲律賓國際旅遊與商務中心的克拉克遊學，We English是一所價格優惠的鄉村式學院，提供最優質的多元化教育，提供各種一對一課程，包含ESL、雅斯、多益、托福，教師超過十年教學經驗。"','canonical_url' => 'https://tw.english.agency/We-English-語言學校-上班族進修','feature_image_alt' => '"We English 語言學校，菲律賓克拉克遊學適合上班族進修"','feature_image_title' => '"We English 語言學校，菲律賓克拉克遊學適合上班族進修"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a215de9af725-CWqWo2kFuApxV1zNOFwhQlq9aHUNiF-1200x630.jpeg','author_id' => '7','category_id' => '1','created_at' => '"2017-12-01 03:02:32"'],
  ['id' => '84','title' => '英文應徵信該怎麼寫？範例供你參考！','slug' => '英文應徵信該怎麼寫-範例供你參考','excerpt' => '找工作面試時，避不了需要撰寫應徵信！但英文應徵信該如何撰寫，才能提高機會順利地獲取面試機會呢？提供你英文應徵信的範例寫法，讓你不再不知所措！','content' => '"<p>應徵信函與自傳最忌洋洋灑灑長篇大論，它不是個人生活史（life history）的闡述，而是獲取面試機會的敲門磚。撰寫應徵信函或自傳時，切記謹守簡潔（brief）、有焦點（focused）、活潑的（dynamic）、清晰（clearly layout）與易懂（easy to understand）等原則。<br />此外，也不要以一份制式（a single purpose）的應徵信函或自傳適用於所有不同性質或產業的職務應徵上，應該針對不同工作屬性提出量身裁製（tailor-made）的應徵信函和自傳。建議讀者可以先撰寫一份完整詳盡的自傳，隨後針對不同的公司要求或工作內容調整寫法。</p>
<h2><span style=""color: #000080;"">【應徵信函参考範例】</span></h2>
<p><br />Application Letter<br />I am so glad to have this opportunity to introduce myself to you. I hope the following information will help you to have a better understanding either on my academic background or evaluating my work experiences.<br />I was born on January 25th，1985 in a traditional family in Taipei County where education was deemed as the priority for the next generation. Under this circumstance together with my own efforts，I am proud to say that my performance in school was excellent. In the meantime，I also joined some kinds of school and community activities，such as the Association of the Scouts and swimming group. I like to help people when they need. I enjoy my life and always try my best to complete every event.<br />In 2002，the same year of high school graduation，I passed the Joint Examination and chose Business Management as my major at Taipei College of Business. During the four-year college life，not only had I actively taken part in various societies，but also read all kinds of books and journals. In addition，I was granted the scholarship from college and local government. In the final year at Taipei College of Business，I was busy preparing the team dissertation. The title of our dissertation was &ldquo;An assessment of Management and Corporate Social Responsibility&rdquo;，supervised by Professor Irene Pan. After one-year of devoted working，we won the first prize.<br />After college，I chose Taipei County Bank as my first job，where I was in charge of clerk services. From this position，I had learned some management skills and was able to apply the objection handling knowledge to my daily operation. I had been in this job for one year and received a customer satisfaction award. This gives you an example of my service ability.<br />I am quite sure about myself and always persist in carrying out my future plan. To achieve my goal，I decide to change my job orientation and choose a forward-looking company like yours to work with. Should you require any information，I would be pleased to provide anytime. Please also refer to the attached documents of my awards and certificates. I look forward to hearing from you soon.</p>
<h3><span style=""color: #000080;"">【應徵信函範例中文翻譯】</span></h3>
<p><br />非常榮幸有此機會向 貴公司介紹我自己，期待以下的訊息將幫助您更了解不僅是我的學術背景，而且得以評估我的工作經驗。<br />我生於1985年1月25日，在台北縣的一個傳統家庭，家中非常重視下一代的教育。在這樣的環境中，加上自己本身的努力，我很榮幸的說：我在學校的表現一直很優異。同時，我也加入一些學校與社區社團，諸如：童軍社和游泳社。當別人需要幫助時，我樂意幫助他人。我享受生活，並總是盡全力完成每一件事。<br />2002年，也是我高中畢業那年，我通過了聯招考試，並選擇了台北商業大學的企業管理作為我的主修。在四年的大學生活，個人不僅積極参與各種社團，也廣泛閱讀了各式書籍與期刊。除此之外，我也榮獲來自學校與地方政府的獎學金。大四那年，我忙於準備小組論文。我們的論文題目是：「管理與企業社會責任之評估」，該論文由潘愛琳教授指導。在一整年的努力，我們終於拿到第一名。<br />大學畢業後，台北縣銀行的櫃檯服務成為我的首份工作。在這份工作裡，我學到了一些管理技巧，並得以運用危機管理知識於每日的工作中。這份工作持續了一年，也榮獲一項客戶滿意獎。希望這個事蹟讓您了解我的服務能力。<br />我有自信，也常堅持實踐我未來的計畫。為達目標，我決定改變工作屬性，並選擇一家如 貴公司一樣有前瞻性的公司工作。如果您需任何訊息，我將很樂意隨時提供給您。請参考附件我的獎狀與證照。期待能很快的接到您的訊息。</p>
<h4><strong><span style=""color: #a42d00;"">單字解析</span></strong></h4>
<p><br />evaluate (v)：評價、估價<br />Customer value is no longer evaluated by product price.<br />（客戶價值不再由產品價格來決定。）<br />撰寫履歷時，描述家庭背景的表達：<br />傳統的家庭traditional family ＝ conventional family<br />平凡的家庭 ordinary family ＝ normal family</p>
<p><br />achieve (v)：完成、達成<br />Handerson achieved his sales quota last year.<br />（韓德森去年達到業績。）<br />有前瞻的 forward-looking ＝ future-oriented</p>
<p><br />refer to (v)：参考、談及<br />Please refer to below sales figures.<br />（請参考以下的銷售數字。）</p>
<p>摘自五南文化事業機構出版<a href=""http://www.wunanbooks.com.tw/product/9789571163840"" target=""_blank"" rel=""nofollow noopener noreferrer"">《商用英文：最佳商務往來溝通藝術》</a></p>"','published' => '1','og_title' => '英文應徵信該怎麼寫？範例供你參考！','og_description' => '找工作面試時，避不了需要撰寫應徵信！但英文應徵信該如何撰寫，才能提高機會順利地獲取面試機會呢？提供你英文應徵信的範例寫法，讓你不再不知所措！','meta_title' => '英文應徵信該怎麼寫？範例供你參考！','meta_description' => '找工作面試時，避不了需要撰寫應徵信！但英文應徵信該如何撰寫，才能提高機會順利地獲取面試機會呢？提供你英文應徵信的範例寫法，讓你不再不知所措！','canonical_url' => 'https://tw.english.agency/英文應徵信該怎麼寫-範例供你參考','feature_image_alt' => '英文應徵信','feature_image_title' => '英文應徵信','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a21068dd5e85-aJn7fAcJzA0SLmiF8EOVKnPqSFGydQ-626x359.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-12-01 07:36:59"'],
  ['id' => '85','title' => '害怕用英文講電話？別怕，常用句型告訴你！','slug' => '害怕用英文講電話-別怕-常用句型告訴你','excerpt' => '用英文講電話是你的罩門？國際化讓各國都有業務來往，在工作職場上用電話溝通勢必免不了的！我們整理了常用的電話英文範例，讓你可以輕鬆練習！','content' => '"<p>電話是商業往來中經常使用的溝通方式之ㄧ，乃是透過電話通訊進行交談與聯繫，屬於較不正式的溝通。一般企業對企業有關報價與下單都還是以書面方式進行，以避免未來交易時爭議的產生。<br />通常接受電話下單多屬於零售業或網路行銷業，這種電話交易方式都會錄音為證，以免未來有爭議或麻煩。本篇主要是一些慣用電話溝通時的英文技巧，熟練之後面對交易對象才不會結巴或不知所云，讓自己隨時擁有職場競爭力。<br />在正式的商務場合中，打一通成功的陌生拜訪電話並不是件簡單的事。不過，如果能掌握一些基本用詞，其實在最短的時間內，找到對的人是容易的。以下列出電話溝通中開啟的實用語詞與範例供大家参考。</p>
<h2><strong><span style=""color: #000080;"">【Telephone English：電話英文範例】</span></strong></h2>
<p><br />Operator: Hello，Interactive Consulting，How can I help you?<br />（接線生：Interactive顧問公司您好，我能幫您嗎？）<br />Dick: This is Dick Peppers. Can I have extension 7539?<br />（狄克：我是狄克˙派伯斯，能幫我轉分機7539嗎？）<br />Operator: Certainly，hold on a minute，I\'ll put you through to extension7539.<br />（接線生：沒問題，請等一下！我幫轉分機7539。）<br />Don: Ernan Roman\'s office，Don speaking.<br />（當：爾納˙羅馬辦公室，我是當。）<br />Dick: This is Dick Peppers calling，is Ernan in?<br />（狄克：我是狄克˙派伯斯，請問爾納在嗎？）<br />Don: I\'m afraid he\'s out of the office at this moment. Can I take a message?<br />（當：很抱歉他現不在辦公室，我能幫您留言給他嗎？）<br />Dick: Yes，Could you ask him to call me back at 0915-245242. I need to talk to him about the Food Taipei Show，it\'s urgent.<br />（狄克：是的，能請他打我的電話0915-245242，我必須與他談論台北食品展，這很緊急。）<br />Don: Could you repeat the mobile phone number，please?<br />（當：請您再說一次手機號碼，好嗎？）<br />Dick: Yes，that\'s 0915-245242，and this is Dick Peppers calling.<br />（狄克：電話是0915-245242，我叫是狄克˙派伯斯。）<br />Don: Thank you Mr. Peppers，I\'ll make sure Ernan gets this as soon as possible.<br />（當：謝謝派伯斯先生，我會儘快讓爾納知道。）<br />Dick: Thanks，bye. （狄克：謝謝，再見。）<br />Don: Bye. （當：再見。）</p>
<h3><br /><strong><span style=""color: #a42d00;"">單字解析</span></strong></h3>
<p><br />put through (ph.) 轉接<br />I will put you through to Brown now.<br />（我現在將你的電話轉給布朗。）<br />at this moment (ph.) 此刻<br />What are you doing at this moment?<br />（你現在再做什麼？）<br />as soon as possible ＝ ASAP (ph.) 儘快<br />He was noticed to submit the proposal as soon as possible.（他被通知儘早繳交提案。）</p>
<p>摘自五南文化事業機構出版<a href=""http://www.wunanbooks.com.tw/product/9789571163840"" target=""_blank"" rel=""nofollow noopener noreferrer"">《商用英文：最佳商務往來溝通藝術》</a></p>"','published' => '1','og_title' => '害怕用英文講電話？別怕，常用句型告訴你！','og_description' => '用英文講電話是你的罩門？國際化讓各國都有業務來往，在工作職場上用電話溝通勢必免不了的！我們整理了常用的電話英文範例，讓你可以輕鬆練習！','meta_title' => '害怕用英文講電話？別怕，常用句型告訴你！','meta_description' => '用英文講電話是你的罩門？國際化讓各國都有業務來往，在工作職場上用電話溝通勢必免不了的！我們整理了常用的電話英文範例，讓你可以輕鬆練習！','canonical_url' => 'https://tw.english.agency/害怕用英文講電話-別怕-常用句型告訴你','feature_image_alt' => '電話英文','feature_image_title' => '電話英文','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a210ae52285e-rOmdH4nDyxluhZOb9So19Ngbmk6qqC-626x381.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-12-01 07:55:30"'],
  ['id' => '86','title' => '掌握英文電話「起、承、轉、合」，溝通更有效率！','slug' => '掌握電話英文-起承轉合-溝通更有效率','excerpt' => '講電話的過程，容易把時間拖太長！如何掌握英文電話的「起、承、轉、合」，讓電話溝通更有效率，在職場上可以自如運用！','content' => '"<p>企業內部一通專業有效的電話溝通可以迅速提供來電者服務，亦可鞏固或提昇顧客滿意度和忠誠度。但是電話英文該怎麼說？以下完整的電話溝通技巧「起、承、轉、合」，讓你在職場上可以自如的運用！</p>
<p><br /><span style=""color: #000080;""><strong>1、起 (Open a Call)</strong></span><br />電話開啟時，首先自我介紹、告知名字、公司名稱、服務單位以及去電事由。為了打破首次通話的尷尬與冷場，以寒喧的方式做開場白是個不錯的選擇。親近的溝通容易打破彼此的藩籬，在最短的時間內與對方打成一片。<br />若顧客主動打電話進來，最好在三至四個鈴響內接通，以避免緊急的顧客需求無法被及時地解決或滿足。假如一位顧客緊急需要的產品訊息或報價資料，無法被及時滿足，可能該心急如焚的顧客就會轉向該企業友商尋求解答與支援，影響所及公司可能損失一個潛在的商機。<br />電話開啟十分關鍵，猶如面對面溝通的「第一印象」。溝通開啟無須冗長，簡潔、好的問題可以很快地吸引顧客或對方的興趣。大部分的人是禮尚往來，只要態度謙和些，常常會有意想不到的收穫。</p>
<p><br /><strong><span style=""color: #000080;"">2、承 (Identify Customer&rsquo;s Pains and Requirements)</span></strong><br />在溝通開啟後，就正式進入主題 － 傾聽顧客的心聲。若是你主動撥出電話，除完整傳達並交付去電目的外，就是提供顧客服務。<br />在互動的過程中，顧客可能會適時的表達他公司所面臨的困境、亟待解的問題或未來的需求讓你知悉。若是顧客直接來電，可能只是「正面的」、單純表達採購需求或想知道某些產品訊息；也可能是針對某一次不滿交易的抱怨，這些「負面的」抱怨可能是不良的產品品質、過高售價或是售後服務差等等。<br />面對憤怒的顧客，必須使用正面的語辭，儘量緩和當時的緊張氣氛，避免與顧客直接發生衝突，找出他真正不滿的原因並儘速協助解決。假如事情處理得當，顧客的問題能夠確實被解決，就能將危機(crisis)變成轉機(turning point)，進而成未來的潛在商機(business opportunity)。<br />顧客需求與抱怨是一體的兩面，全賴你用心處理和解決；當面對顧客的抱怨時可以理性思考其原委，然後感性回應。</p>
<p><br /><strong><span style=""color: #000080;"">3、轉 (Propose Solution)</span></strong><br />在了解顧客的想法與需求之後，便是幫顧客找到合適的解決方案。若是顧客的需求簡單，便立即處理；若是顧客的問題必須跨部門才能解決，這時找到企業內部對的資源或決策者益顯重要。此刻可採「三方通話」，邀請相關人員至談話中，及時在線上幫顧客解答疑問，以提高顧客滿意度。<br />若是無法立即解決的問題，承諾顧客何時前給予答案是必要之舉。處理顧客的不滿，必要時找高階主管與顧客對談，讓顧客有被重視的感覺。<br />幫顧客解決問題，是建立個人信用(credit)的好時機。解決問題的過程中，必須以客為尊、以解決顧客的問題與需求為優先考量。在提出解決方案時，亦不忘與顧客確認解決方案是否符合他公司真正的需要。針對顧客提出的每一個問題，要找出問題背後的根本起因，才不會徒勞無功，不至於提出的解決方案無法滿足顧客的根本需求。</p>
<p><strong><span style=""color: #000080;"">4、合 (Close a Call)</span></strong><br />掛斷電話前，必須再確認顧客需求與抱怨是否都已被滿足或解決，所提出的解決方案是否能解決顧客的最終問題。當時未決的也要告知顧客給予答案的期限，讓顧客得以對其老闆或公司交代。<br />大多數人認為服務開始與結束在顧客眼中同樣重要，但有些研究顯示結束時更舉足輕重。在電話溝通結束後，建議詳實紀錄談話內容與重點，這些訊息都將成為企業商業智慧(business intelligence)的信息來源。<br />根據商業智慧分析結果，企業主動提供顧客高附加價值(value-added)的產品、服務或解決方案。這種將顧客的來龍去脈融入企業業務中，以了解「顧客情境」（customer scenario）建立更深遠的關係，讓企業在顧客心目中將日漸重要，最後成為不可或缺的夥伴關係(partnership)。</p>
<h2><strong>【練習題】</strong></h2>
<p><br />請兩人一組，一人扮演打電話的人，另一人為受話者，彼此練習打電話與接電話的禮儀。<br />情境一：找的人接電話（請翻譯成英文）。</p>
<p>摘自五南文化事業機構出版<a href=""http://www.wunanbooks.com.tw/product/9789571163840"" target=""_blank"" rel=""nofollow noopener noreferrer"">《商用英文：最佳商務往來溝通藝術》</a></p>"','published' => '1','og_title' => '掌握電話英文「起、承、轉、合」，溝通更有效率！','og_description' => '講電話的過程，容易把時間拖太長！如何掌握英文電話的「起、承、轉、合」，讓電話溝通更有效率，在職場上可以自如運用！','meta_title' => '掌握電話英文「起、承、轉、合」，溝通更有效率！','meta_description' => '講電話的過程，容易把時間拖太長！如何掌握英文電話的「起、承、轉、合」，讓電話溝通更有效率，在職場上可以自如運用！','canonical_url' => 'https://tw.english.agency/掌握電話英文-起承轉合-溝通更有效率','feature_image_alt' => '電話英文技巧','feature_image_title' => '電話英文技巧','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a2117a4a5469-iFUpd3foPMeFwlEZT6np43d38HMjf2-626x382.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-12-01 08:53:08"'],
  ['id' => '87','title' => '"在日式 Howdy English 語言學校，上班族、親子、樂齡都能學好英文！"','slug' => '上班族、親子、樂齡就讀Howdy-English-語言學校','excerpt' => '"Howdy English 語言學校以「適合亞洲學生的學習法」為核心，上班族、親子遊學、樂齡都能學好英文，鄰接購物中心並且擁有知名美味的高評價日式餐點！"','content' => '"<h2><strong>從畏懼到愛上英文 &nbsp;宿霧遊學功力大增</strong></h2>
<p><span style=""font-weight: 400;"">如何增強自己的競爭力，走出自己的一片天？</span></p>
<p><span style=""font-weight: 400;"">語言學習，是目前台灣學生必要的條件之一，也幫助自己具備更多往外商與海外企業發展的機會。許多人談起學習英文，總是第一時間想到英語補習班或甚至前往歐美遊學，但高昂的學費卻往往讓人望之卻步；也因此價格低廉卻有效的菲律賓遊學逐漸興起，日韓學生每年更湧進萬人前往學習，成為了新型態遊學勝地。</span></p>
<p>&nbsp;</p>
<h2><strong>日籍設計規劃 英文質感學習</strong></h2>
<p><span style=""font-weight: 400;"">位於語言學校一級戰區－宿霧的 Howdy English 語言學校，是由日籍設計師來作主要規劃、設計、施工的菲律賓語言學校，因此硬體設施、宿舍以及餐廳伙食都是最高水準。日本籍校長管理學生學習與教師進修，師資皆為大學畢業生。</span></p>
<p><span style=""font-weight: 400;"">所有老師於上課前，都會先受訓 1~2 個月，並由宿霧的語言學家 Dr. Tampus 指導，課程以一對一為主、企業培訓（商務）和親子遊學等，培養學齡前孩子對英語的學習基礎與興趣，家長與孩子共通參與課程，1 天合計有 10 班的課程，親子之間一起成長與學習，對於國高中生、初出社會的新鮮人和上班一段時間的社會人士，都是相當好的選擇！</span></p>
<p><span style=""font-weight: 400;""><img title=""Howdy English 語言學校親子遊學高評價首選"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a215b2b86931-J5HF16sHINyAFRF0cadc1axWCV4jsD-850x566.png"" alt=""Howdy English 語言學校親子遊學高評價首選"" width=""850"" height=""566"" /></span></p>
<p>HOWDY English 語言學校一對一課程&nbsp;</p>
<p>&nbsp;</p>
<h2><strong>最適合亞洲學生的學習法</strong></h2>
<p><span style=""font-weight: 400;"">Howdy English 語言學校以「適合亞洲學生的學習法」廣受學生高評價，重視師生良好的互動來提升學員主動學習的動機。主要因素是日本與台灣學生有著相近的學習弱點，較害怕英文口說及在組織英文句型，斯巴達式的英文學習方式未必適用於每個人。</span></p>
<p><span style=""font-weight: 400;"">在 Howdy English，一共有教師 72 名對學員 142 名，師生比低且師資皆受過英語教學訓練，團體課程著重於商務技能、談判技巧、發音矯正、發音練習、文法、群眾演說、小組討論與電影欣賞，以活潑方式引導學習不論是大人、銀髮族、親子同行帶孩子一同學習，都能膽開口說英文！最讓人覺得貼心的則是 50 歲以上折扣方案，讓大家都能貼心邀請銀髮長青族一同前往宿霧學習英文。</span></p>
<p><span style=""font-weight: 400;""><img title=""鄰近 HOWDY English 語言學校之 Ayala Mall"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a215bd789227-4LO3ZIeFoNjVFiupfwKsob44DWc99k-850x566.png"" alt=""鄰近 HOWDY English 語言學校之 Ayala Mall"" width=""850"" height=""566"" /></span></p>
<p>鄰近 HOWDY English 語言學校之 Ayala Mall&nbsp;</p>
<p>&nbsp;</p>
<h2><strong>正統日式料理 補充唸書體力</strong></h2>
<p><span style=""font-weight: 400;"">鄰接購物中心的 Howdy English 生活機能佳，前往超市和銀行，餐廳，咖啡店等都只需步行 1 分鐘，學生們在沒有壓力的環境下，可以集中精神來學習英語，學校餐飲絕對是讓人大為讚嘆的一部分，由日本籍主廚親自掌廚，以日本料理為中心，可以每天享受高評價美味的料理，不但能攝取足夠蔬菜，也全面使用日本製造的調味料，比餐廳還好吃喔！</span></p>
<p><span style=""font-weight: 400;""><img title=""Howdy English 語言學校日式餐點高評價首選"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a215b7dddbed-p2c0AW5jWDJrNc5C9WLNpA3luuGXqw-850x637.png"" alt=""Howdy English 語言學校日式餐食高評價首選"" width=""850"" height=""637"" /></span></p>
<p>HOWDY English 廣受好評之日式餐點&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">菲律賓遊學與以往出國遊學最不一樣的地方在於，有機會選擇老師一對一課程，隨時依狀況調整，學生透過各國多元文化，培養出獨立自主的個性，並學會勇於表達內心想法與靈活應用的思考能力，有這樣子的學習環境，不論是上班族、親子遊學、銀髮族，都能在英文學習上有所進步！</span></p>
<p><span style=""font-weight: 400;"">-</span></p>
<p>前往官網深入了解 <a title=""HOWDY English 語言學校"" href=""https://tw.howdyenglish.asia"">HOWDY English 語言學校</a></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"在日式 Howdy English 語言學校，上班族、親子、樂齡都能學好英文！"','og_description' => '"Howdy English 語言學校以「適合亞洲學生的學習法」為核心，上班族、親子遊學、樂齡都能學好英文，鄰接購物中心並且擁有知名美味的高評價日式餐點！"','meta_title' => '"在日式 Howdy English 語言學校，上班族、親子、樂齡都能學好英文！"','meta_description' => '"Howdy English 語言學校以「適合亞洲學生的學習法」為核心，上班族、親子遊學、樂齡都能學好英文，鄰接購物中心並且擁有知名美味的高評價日式餐點！"','canonical_url' => 'https://tw.english.agency/上班族、親子、樂齡就讀Howdy-English-語言學校','feature_image_alt' => '"Howdy English 語言學校親子遊學首選"','feature_image_title' => '"Howdy English 語言學校親子遊學首選"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a215ca80b5c0-eFZgKU2vYyQYgGbCk8qvwUoIaZHL0i-1200x630.png','author_id' => '7','category_id' => '1','created_at' => '"2017-12-01 13:41:40"'],
  ['id' => '88','title' => '"在 Kredo 語言學校英文與程式雙修，成為外商工程師不是夢"','slug' => 'Kredo語言學校英文與程式雙修','excerpt' => '"Kredo 語言學校 (Kredo IT Abroad) 為一所日商學校，最大的特色就是有專門為想成為 IT人員設計許多專業課程，無論英文、 程式和平面設計課程，兼具實用與創新，所有課程教學都以英文為主，讓你提早使用工作用的專業術語！"','content' => '"<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">相信所有在職場上，無論是剛出社會或是上班多年的人，都能強烈感受到，在全球化市場的競爭下，許多企業需要具備外語能力的人才，早已不限外商，需要雙語或能說多國語言人才的本土企業也越來越多，在國際化人才的需求逐漸攀高下，資訊業、科技業與金融業更是最主要的幾大產業，不僅紛紛宣布英文為公司內部的共同語言，甚至在員工未來的升遷上，也將英文能力列為重要參考指標。</span></p>
<p>&nbsp;</p>
<h2><strong>不只學英文更有程式專業培訓課程</strong></h2>
<p><span style=""font-weight: 400;"">不管是大學生、社會新鮮人或在職上班，如果要出國進修，除了時間上的考量外，價格和課程選擇都是最核心的問題。台灣人普遍喜歡海島國家，而近期相當受歡迎的宿霧，也早已吸引各國人士前往英文短期進修，不僅有相當專業語言學校，針對 以成為IT人員為目標的英文與專業課程也有，Kredo 語言學校即是！</span></p>
<p><span style=""font-weight: 400;""><img title=""Kredo 語言學校 Kredo IT Abroad 成為IT人員之路"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a21656454fc2-QRuOKVChlLIIsQ2FViBpsfTwjAdYwe-850x478.jpeg"" alt=""Kredo 語言學校 Kredo IT Abroad 成為IT人員之路"" width=""850"" height=""478"" /></span></p>
<p><span style=""font-weight: 400;"">Kredo IT Abroad 程式課程</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">位於宿霧金融與資訊發展首屈一指的 IT Park 園區，Kredo 語言學校 (Kredo IT Abroad) 為一所日商學校，最大的特色就是有專門為想成為 IT人員設計許多專業課程，無論英文、 程式和平面設計課程，兼具實用與創新，且英文是這裡唯一語言，所有課程教學都以英文為主，讓你置身在全英文環境外，提早使用工作用的專業術語，來養成成為網頁工程師的必要實作能力！</span>校園內也非常重視安全，一進校園就得通過安檢，避免學生攜帶不當且危險的用品，免去必要的擔憂，餐廳更供應各國料理，每餐都有不同變化菜色，住宿部分亦提供舒適的電梯大樓套房，每間房都備有自用小廚房喔！</p>
<p><img title=""Kredo 語言學校 Kredo IT Abroad 學習英文與網頁程式"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a2166498b490-QEhboA0NQhvC1cHO0orfALaz4efY5Z-850x638.jpeg"" alt=""Kredo 語言學校 Kredo IT Abroad 成為IT人員之路"" width=""850"" height=""638"" /></p>
<p><span style=""font-weight: 400;"">Kredo IT Abroad 學生宿舍</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">以現今科技業而言，語文與專業技能的提昇都是必要的，海外遊學無疑是最佳選擇，Kredo 語言學校以高單位且密集英文課程和專業IT學習課程，帶領大家通往高階工程師之路，不僅培養出許多外商高階主管，來這裡進修的人，在專業度與自信心上都有了更大的進步，且在回國後不僅有了高升的機會，更開啟海外工作的一扇門，無疑是最大收穫。</span></p>
<p>&nbsp;</p>
<h2><strong>英文不僅代表升遷 早已是基本能力</strong></h2>
<p><span style=""font-weight: 400;"">科技業走向全球布局，日常溝通需要大量應用英語，外語力會決定工作表現與升遷，上班族也必須加強自己與國際接軌的能力，第一步便是具備良好的外語能力，求職者能夠同時具備雙聲帶或多聲帶的外語能力，在職場的競爭環境中較脫穎而出，而資訊與科技業更為重要，要能夠跟上國外最新科技與資訊發展，就用使用英文不斷自學精進，成為職場硬實力！這也是為何英文還是關鍵核心點，唯有不斷提升才能讓自己在職場上屹立不搖。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">-</span></p>
<p><span style=""font-weight: 400;"">前往官網深入了解 <a href=""https://kredo.english.agency"" target=""_blank"" rel=""noopener noreferrer"">Kredo IT Abroad&nbsp;語言學校</a></span></p>"','published' => '1','og_title' => '"在 Kredo 語言學校英文與程式雙修，成為外商工程師不是夢"','og_description' => '"Kredo 語言學校 (Kredo IT Abroad) 為一所日商學校，最大的特色就是有專門為想成為 IT人員設計許多專業課程，無論英文、 程式和平面設計課程，兼具實用與創新，所有課程教學都以英文為主，讓你提早使用工作用的專業術語！"','meta_title' => '"在 Kredo 語言學校英文與程式雙修，成為外商工程師不是夢"','meta_description' => '"Kredo 語言學校 (Kredo IT Abroad) 為一所日商學校，最大的特色就是有專門為想成為 IT人員設計許多專業課程，無論英文、 程式和平面設計課程，兼具實用與創新，所有課程教學都以英文為主，讓你提早使用工作用的專業術語！"','canonical_url' => 'https://tw.english.agency/Kredo語言學校英文與程式雙修','feature_image_alt' => '"Kredo 語言學校 Kredo IT Abroad 學英文與網頁程式"','feature_image_title' => '"Kredo 語言學校 Kredo IT Abroad 學英文與網頁程式"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a2164bf844c3-mfcbDWY2w89NZAoIJHrhnCBjoqCAYy-1200x630.jpeg','author_id' => '7','category_id' => '1','created_at' => '"2017-12-01 14:26:52"'],
  ['id' => '89','title' => '"專心學習英語就好！CELC 語言學校貼心照料學習與生活"','slug' => 'CELC語言學校鍛鍊英語硬實力','excerpt' => '"CELC 語言學校是得到眾多學生推崇宿霧斯巴達平價課程密度高的好選擇！擁有傲視各校的師生比，大部份老師皆有 3 年以上的教學經驗，讓學生享有各式目標的斯巴達會話課程選擇。"','content' => '"<p>許多人期待透過國外遊學，藉以培養世界觀與外語能力，並與各國同學進行文化交流，也因此多數人都曾花上大把的時間與金錢，語言能力卻七零八落，只為了應付考試中過去。<span style=""font-size: 14px;"">菲律賓遊學不僅打破以往在其他歐美國家需要長時間飛行及昂貴的價格，更能在全英文環境下學習下，在短時間內突飛猛進，不僅吸引世界各地的學生前往進修，一對一的個人教學，學費更僅需歐美語言學校三分之一！</span><span style=""font-size: 14px;"">&nbsp;</span></p>
<p>在眾多語言學校群聚的菲律賓宿霧斯巴達戰區， CELC 語言學校則是得到眾多學生推崇平價課程密度高的好選擇！全校共有 80 名菲律賓籍老師以及 1 名英國籍老師，可容納 130 名學生，擁有傲視各校的師生比，每位學生都能得到滿滿的學習關懷！大部份老師皆有 3 年以上的教學經驗，且為國立宿霧師範大學畢業，讓學生享有各式目標的斯巴達會話課程選擇。</p>
<p>&nbsp;</p>
<h2>紮實鍛鍊英文能力&nbsp;</h2>
<p><span style=""font-weight: 400;"">在宿霧這樣的斯巴達與校戰場，CELC 當然是有兩把刷子！斯巴達「ESL速成」與「商用英文」課程每日上課10小時；「強力口說」與「雅思」課程每天上課11小時，每位教師都會學生建立專屬的教學回饋與考試成果紀錄，讓學生了解自己更需要進步的地方，逐步踏實天天進步！</span>&nbsp;</p>
<p><img title=""CELC 菲律賓宿霧斯巴達語言學校一對一課程"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a23aaf0108f3-wSYEWg8Pl9UQ5nxdYLMz0uTJ1M1KK9-850x565.jpeg"" alt=""CELC 菲律賓宿霧斯巴達語言學校一對一課程"" width=""850"" height=""565"" /></p>
<p>CELC 語言學校一對一課程上課情況</p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">不只有高強度的斯巴達學習，同時也貫徹英文口說使用的必要性，在校內完整的獎懲制度推動「僅說英文 (English Only Policy)」政策，不難發現每隔幾個月就有學生以流利的口說英文出現，甚至在說話時都變得相當有自信。</span></p>
<p>&nbsp;</p>
<h2>展望多語學習與職涯成長</h2>
<p><span style=""font-weight: 400;"">不只學得到英文，CELC 語言學校更提供專業韓文課程，讓對韓語學習以及韓國文化有興趣的台灣、日本學生也能在學英語的閒暇時間接觸韓文，並且與韓國學生進行文化交流，提升自己多國語言軟實力。</span></p>
<p><span style=""font-weight: 400;"">CELC 也積極展望學生的職涯，與韓國國際就業教育中心（International Job Education Center） 、澳洲人力資源諮詢顧問（Australia HR Consulting） 以及新加坡人力資源網絡（Singapore Human Network）等機構合作，協助計劃至澳洲與新加坡打工實習或是就業的學生，也可為即將想前往美加或澳洲出國留學打工的人，加速英語能力的升級。</span></p>
<p>&nbsp;</p>
<h2>體驗美味韓式家鄉料理</h2>
<p><span style=""font-weight: 400;""> 校內提供美味韓式料理，其最著名的就是偏辣的韓式風味，適合喜愛韓式料理的學生們，如果怕辣的也沒關係，學校餐廳亦有符合台灣口味的菜色選擇。同時每月一次 Vitamin Day 各式水果讓學生盡情享用，補充健康身體所需的各式營養素，在享用健康水果的同時，也能藉此多聊天，全英文環境又再度展開。</span></p>
<p><span style=""font-weight: 400;""><img title=""菲律賓宿霧CELC斯巴達語言學校 Vitamin Day"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a23ab3306542-CJOWDdRRQUHnbrPRem3FtDYxf8upXp-850x478.jpeg"" alt=""菲律賓宿霧CELC斯巴達語言學校 Vitamin Day"" width=""850"" height=""478"" /></span></p>
<p>CELC 語言學校 Vitamin Day&nbsp;</p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">學習必有目標，學習英文則是為了更好的來，然而，學英文跟減肥一樣，靠著短期的衝刺可能會看到體重計上的成效，但是長期下來，如果沒有養成健康的生活習慣，還是會回到復胖的循環，因此，讓英文大量融入在日常環境裡，絕對是必要的課題，一定要密集學習，展望職涯未來！</span></p>
<p>&nbsp;</p>
<p>-</p>
<p>前往官網深入了解 <a title=""CELC語言學校"" href=""https://celc.english.agency"" target=""_blank"" rel=""noopener noreferrer"">CELC語言學校</a></p>"','published' => '1','og_title' => '"一起“菲”去遊學，CELC 語言學校鍛鍊英語硬實力！"','og_description' => '"CELC 語言學校是得到眾多學生推崇宿霧斯巴達平價課程密度高的好選擇！擁有傲視各校的師生比，大部份老師皆有 3 年以上的教學經驗，讓學生享有各式目標的斯巴達會話課程選擇。"','meta_title' => '"一起“菲”去遊學，CELC 語言學校鍛鍊英語硬實力！"','meta_description' => '"CELC 語言學校是得到眾多學生推崇宿霧斯巴達平價課程密度高的好選擇！擁有傲視各校的師生比，大部份老師皆有 3 年以上的教學經驗，讓學生享有各式目標的斯巴達會話課程選擇。"','canonical_url' => '"https://tw.english.agency/CELC 語言學校鍛鍊英語硬實力"','feature_image_alt' => '"菲律賓宿霧斯巴達 CELC 語言學校"','feature_image_title' => '"菲律賓宿霧斯巴達 CELC 語言學校"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a23a9ed54219-FQS8gNV7fOHOPpboJlFK4CEPEtTUXh-1200x630.jpeg','author_id' => '7','category_id' => '1','created_at' => '"2017-12-03 07:46:00"'],
  ['id' => '90','title' => '"上課就像私人家教，菲律賓宿霧 First English 語言學校做得到"','slug' => 'First-English-語言學校親子遊學','excerpt' => '"菲律賓宿霧 First English Global College 為學生身量身打造適合自己的課程，課程內容重視英語實用性，發音、會話、情境練習、辯論、演說…等細項科目都可以依學生自己的需求彈性調整，每位學生都有專屬導師。"','content' => '"<p>這幾年讓亞洲學生趨之若鶩的新興遊學點，菲律賓宿霧當之無愧！除了地理優勢便利，飛航時間短外，費用低廉，能奠定語言能力，提高就業機會，並擁有高CP值的學習過程與經驗，甚至藉此踏進國外企業實習的機會，也難怪許多人紛紛前往。</p>
<p>&nbsp;</p>
<h2><strong>日式教育標準</strong> 英文彈性學習</h2>
<p><span style=""font-weight: 400;""> 日本人創立的「First English Global College」，學校成立大約兩年，位於宿霧 (Cebu) 的馬克坦島 (Mactan Island) 上，學校位於鬧中取靜的住宅區，平日可讓學生全心專注在學習上，方圓一公里只有個當地的小市集，適合專心唸書，週末再出去玩，適合想要加強英文口語的學生或親子遊學，另外，因為學生幾乎都是日本人，不熟悉日語的朋友只能強迫自己開口說英文，若對日文學習有興趣的同學也可以藉此練習日文會話。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">依照學生的需求，學校為學生身量身打造適合自己的課程，著重由學生自主性規畫自己的學習，課程內容重視英語實用性，發音、會話、情境練習、辯論、演說&hellip;等細項科目都可以依學生自己的需求彈性調整，每位學生安排一位「Buddy Teacher」，不管生活或學習任何問題，都可以與專屬的導師討論。</span></p>
<p><span style=""font-weight: 400;""><img title=""菲律賓宿霧 First English 語言學校一對一課程上班族進修"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a23b57b1c0b3-vL8Arz1mKZAq3Ri9NGhC4HKENq6OFB-850x498.jpeg"" alt=""菲律賓宿霧 First English 語言學校一對一課程上班族進修"" width=""850"" height=""498"" /></span></p>
<p><span style=""font-weight: 400;"">First English 語言學校上課情形</span></p>
<p>&nbsp;</p>
<p>針對托福及雅思，學校也有專屬課程，針對聽、說、讀、寫各方面專精的老師個別輔導學生，有定期的考試檢定學習成效，幫助學生順利達成多益、托福或雅思的目標分數，對社會新鮮人或早已上班一段時間的職場上班族，都是進修的好選擇。到First English 語言學校可以服務寬廣年齡層的學生，如親子遊學、學生暑期遊學、上班族進修與樂齡族學習，想要進行親子遊學的朋友，也有托嬰服務可以讓爸媽在英語進修之餘，安心讓孩子得到良好照顧。</p>
<p>&nbsp;</p>
<h2><strong>亞洲風味餐點 菜色每日更換</strong></h2>
<p><span style=""font-weight: 400;""> 在First English 校內餐點，午晚餐偏向日式料理，有親子丼、味噌湯等等，口味不會吃不慣，用餐時間開始前廚房人員就會把餐都放好，每天都不用煩惱吃什麼，餐廳更貼心的將每日菜色這片貼在牆上，可以先看看當週的菜色喔！</span></p>
<p><img title=""菲律賓宿霧 First English 語言學校餐廳菜色"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a24c0662736e-wIukcidUhptgqPaSdFImsCMomz40yb-850x637.jpeg"" alt=""菲律賓宿霧 "" width=""850"" height=""637"" />&nbsp;</p>
<p><span style=""font-weight: 400;"">First English 語言學校餐廳菜色</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">菲律賓語言學習的特色就是密集與一對一口說，在學習的過程中，老師與學生以一對一練習英語口說，模擬實際英語會話情境，對於習慣填鴉式教育，長期缺乏練習的台灣學生幫助非常大！老師也可以解答任何英文相關問題，如同個人家教，學習成效肯定是顯著的，如有興趣前往海外遊學，如暑期遊學、親子遊學或上班族進修，菲律賓宿霧是不錯的選擇。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>-</p>
<p>前往網站深入了解 <a title=""First English 語言學校"" href=""http://firstenglish.asia"" target=""_blank"" rel=""noopener noreferrer"">First English 語言學校</a></p>"','published' => '1','og_title' => '"感受如同擁有私人家教，菲律賓宿霧 First English 語言學校做得到"','og_description' => '"菲律賓宿霧 First English Global College 為學生身量身打造適合自己的課程，課程內容重視英語實用性，發音、會話、情境練習、辯論、演說…等細項科目都可以依學生自己的需求彈性調整，每位學生都有專屬導師。"','meta_title' => '"感受如同擁有私人家教，菲律賓宿霧 First English 語言學校做得到"','meta_description' => '"菲律賓宿霧 First English Global College 為學生身量身打造適合自己的課程，課程內容重視英語實用性，發音、會話、情境練習、辯論、演說…等細項科目都可以依學生自己的需求彈性調整，每位學生都有專屬導師。"','canonical_url' => 'https://tw.english.agency/First-English-語言學校親子遊學','feature_image_alt' => '"菲律賓宿霧 First English 語言學校親子遊學與上班進修"','feature_image_title' => '"菲律賓宿霧 First English 語言學校親子遊學與上班進修"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a23b477d4000-fBh3HAE1GHGGsOw7h1QbywGsPRHoOG-1200x630.jpeg','author_id' => '7','category_id' => '1','created_at' => '"2017-12-03 08:29:44"'],
  ['id' => '91','title' => '"菲律賓遊學短期衝刺好選擇，SMEAG 語言學校保證你考到英語證照！"','slug' => 'SMEAG-語言學校保證你考到英語證照','excerpt' => '"SMEAG 語言學校擁有悠久歷史與縝密教學與評量系統，同時也是菲律賓規模最大的語言學校，每個校區都有自己的特色項目和管理系統，並提供多樣的特色學習服務和設施。"','content' => '"<p><span style=""font-weight: 400;"">菲律賓遊學旋風早已默默風行好幾年，</span><span style=""font-weight: 400;"">赴菲律賓學英語早更是日韓學生學習英語的流行趨勢，尤其是到宿霧，隨著來自世界各地的學生不斷增加，若想在短期內迅速提升英語能力，前往菲律賓遊學做密集的英語課程安排，讓英語能力實質提升，絕對是值得一試的機會，</span><span style=""font-weight: 400;"">飛航時間短外，費用低廉，</span><span style=""font-weight: 400;"">都是日韓學生都選擇菲律賓宿霧作為提升英文首要城市。</span></p>
<p>&nbsp;</p>
<h2>英語考照的多樣選擇</h2>
<p><span style=""font-weight: 400;"">菲律賓的語言學校之所以能夠吸引世界各地的學生前往進修，最大動力無非是一對一個人教學以及僅需歐美語言學校三分之一的學費，宿霧更是熱門地點，當中擁有悠久歷史與縝密教學與評量系統的非 SMEAG 語言學校莫屬，SMEAG 同時也是菲律賓規模最大的語言學校，每個校區都有自己的特色項目和管理系統，並提供多樣的特色學習服務和設施。</span></p>
<p><span style=""font-weight: 400;"">SMEAG 在宿霧共有三個校區，距離宿霧機場皆在 30 分鐘的距離內，分別為：擁有少見劍橋課程的 Sparta 斯巴達校區（將遷往市區）、雅思保證班為主的 Classic 雅思校區 、Capital 校區擁有多種專業課程，如多益、托福、商業課程等等，更擁有宿霧唯一雅思官方考場與多益官方指定考場，是菲律賓少數可以提供 IELTS7.0、TOEFL 110 分及 TOEIC 900 分保證班的語言學校。</span></p>
<p><img title=""菲律賓宿霧 SMEAG 語言學校考試教室"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a23c2b5a3f05-16G2XVUb0iNk1vvRifZgNIowwEg2vJ-850x566.jpeg"" alt=""菲律賓宿霧 SMEAG 語言學校考試教室"" width=""850"" height=""566"" /></p>
<p><span style=""font-weight: 400;"">SMEAG 語言學校考試教室</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">SMEAG 語言學校也和許多國家的大學和教育機構的合作，為出國留學的同學准備了大學的招生計劃和培訓項目，且通過多國大學和教育機構的合作，包括美國、澳洲、加拿大、英國、新加坡等國家，提供「海外大學免雅思入學方案」，讓學生在沒有大學的入學資格和雅思成績時，通過 EAP 課程的培訓，讓學生仍有機會輕鬆申請進入美加英澳的大學！</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2><strong>寓教於樂 學習與休閒並重</strong></h2>
<p><span style=""font-weight: 400;"">學校除了提供最優質的英與課程外，更為學生提供校運動空間，學生皆可使用校內健身房、室內高爾夫球練習場和籃球場等，也會定期舉辦校外旅遊活動。</span><span style=""font-weight: 400;"">由於主要是日韓學生較多，也因此學校大多是日式和韓式料理，也可到校外餐廳外食，學校附近有許多特色餐廳喔！</span></p>
<p><span style=""font-weight: 400;"">SMEAG語言學校擁有相當多的外國學生，和來自世界各地的同學互相交流與學習，英語實力也會因此提高，課餘時間還可以讓不同國家的文化與資訊，物超想像的最高學習CP質，正打算到宿霧遊學的人，讓自己在放鬆的環境下，好好學習吧！</span></p>
<p>&nbsp;</p>
<p>-</p>
<p>前往網站深入了解 <a title=""SMEAG 語言學校"" href=""https://smeag.english.agency"" target=""_blank"" rel=""noopener noreferrer"">SMEAG 語言學校</a></p>"','published' => '1','og_title' => '"菲律賓遊學短期衝刺好選擇，SMEAG 語言學校保證你考到英語證照！"','og_description' => '"SMEAG 語言學校擁有悠久歷史與縝密教學與評量系統，同時也是菲律賓規模最大的語言學校，每個校區都有自己的特色項目和管理系統，並提供多樣的特色學習服務和設施。"','meta_title' => '"菲律賓遊學短期衝刺好選擇，SMEAG 語言學校保證你考到英語證照！"','meta_description' => '"SMEAG 語言學校擁有悠久歷史與縝密教學與評量系統，同時也是菲律賓規模最大的語言學校，每個校區都有自己的特色項目和管理系統，並提供多樣的特色學習服務和設施。"','canonical_url' => 'https://tw.english.agency/SMEAG-語言學校保證你考到英語證照','feature_image_alt' => '"菲律賓宿霧 SMEAG語言學校劍橋雅思托福考試課程"','feature_image_title' => '"菲律賓宿霧 SMEAG語言學校劍橋雅思托福考試課程"','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a23c17244a68-IQbT7nwiSA4jpoS0lY8XjYZKtU1vwF-1200x630.jpeg','author_id' => '7','category_id' => '1','created_at' => '"2017-12-03 09:32:10"'],
  ['id' => '92','title' => '上班族職場英文加分題｜第一印象篇','slug' => '職場英文加分題第一印象篇','excerpt' => '每一位員工都是企業對外大使，對於企業聲譽的塑造扮演著舉足輕重的角色。本篇介紹人與人見面溝通與自我介紹的方式以及注意事項，職場上班族必讀的好印象加分秘訣以及英文單字一起幫你整理！','content' => '"<p><span style=""font-weight: 400;"">每一位員工都是企業對外大使，對於企業聲譽的塑造扮演著舉足輕重的角色。員工對顧客或合作夥伴的態度與應對進退，攸關著企業整體形象。本篇首先介紹人與人見面溝通與自我介紹的方式以及注意事項，職場上班族必讀的好印象加分秘訣以及英文單字一起幫你整理！</span></p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #f28f00;"">如何建立良好的第一印象(First Impression)</span></strong></h2>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">根據哈佛大學行為科學研究顯示，面對面溝通包含三大要素：肢體語言，談話內容和語調。</span></p>
<p><span style=""font-weight: 400;"">(1) &nbsp;肢體語言是一個成功溝通的最重要因素，它佔了全部重要性的55%。</span></p>
<p><span style=""font-weight: 400;"">(2) &nbsp;其次是語調，它佔了三分之一強的重要性。</span></p>
<p><span style=""font-weight: 400;"">(3) &nbsp;最後便是談話內容僅剩7%。</span></p>
<p><span style=""font-weight: 400;"">在電話溝通裡，語調的重要性躍升至四分之三強，甚至比面對面溝通的肢體語言比重還重，談話內容也升至25%。上揚的語調易使受話者放下戒心、敞開心胸暢談，對顧客的誠心與尊重更是產生好語調的不二法門。</span><span style=""font-weight: 400;""><br /><br /></span></p>
<p><img title=""職場加分英文第一印象篇"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a28362000933-kNmGD9WRA1Qez6iOva3X8AsnOtKYCx-800x426.jpeg"" alt=""職場加分英文第一印象篇"" width=""800"" height=""426"" /></p>
<h2><strong><span style=""color: #f28f00;"">哪些讓印象加分的英文單字與句型，一起筆記！</span></strong></h2>
<p><span style=""font-weight: 400;"">Smile － 微笑是國際通用的語言。</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Smiling is an international language.</span></p>
<h3><span style=""font-weight: 400;""><br /></span><strong>面對面溝通（face-to-face communication）：<br /></strong><span style=""font-weight: 400;"">(1) 肢體語言 &nbsp;body language &nbsp;55%</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">(2) 談話內容 &nbsp;words &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7%</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">(3) 語調 &nbsp;tone &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;38%</span><span style=""font-weight: 400;""><br /><br /></span></h3>
<p><span style=""font-weight: 400;"">Your attitude is the first thing people see in a face-to-face communication. </span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">面對面溝通中，態度是決定他人對你看法的首要關鍵</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Thanksgiving，appreciation，gratitude </span></p>
<p><span style=""font-weight: 400;"">感謝之情</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Thanksgiving，appreciation and gratitude will make your business easier. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">心存善、感謝與感恩讓你在商場做生意更容易</span></p>
<p><span style=""font-weight: 400;""><br /><br /></span></p>
<h2><strong><span style=""color: #f28f00;"">要留給對方好印象還需具備什麼條件，英文怎麼說？</span></strong></h2>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;""> always have plenty to say &nbsp;&nbsp;&nbsp;不會語拙</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">have many amazing stories to tell &nbsp;&nbsp;話題豐富與多樣</span><span style=""font-weight: 400;""><br /><br /></span></p>
<p><span style=""font-weight: 400;"">maintain eye contact &nbsp;目視對方</span><span style=""font-weight: 400;""><br /><br /></span></p>
<p><span style=""font-weight: 400;"">be honest &nbsp;誠實</span><span style=""font-weight: 400;""><br /></span></p>
<p><span style=""font-weight: 400;"">never interrupt &nbsp;不會插嘴</span></p>
<p><span style=""font-weight: 400;"">listen carefully to what others are saying &nbsp;&nbsp;&nbsp;仔細聆聽對方說話</span><span style=""font-weight: 400;""><br /><br /></span></p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #f28f00;"">職場溝通術再晉級，聆廳技巧怎麼學</span></strong></h2>
<p><span style=""font-weight: 400;"">「聽」在溝通中扮演非常重要的角色。如果只是一股腦兒地表達自己的想法，未曾給對方喘息的機會，對方可能會放棄表達心中真正的想法或需求。所以要避免說話時漠視對話內容，或是佯裝在聽（fake listening）、選擇性聽取訊息，這不僅讓溝通事倍功半，也是不禮貌的。在商務場合上，容易造成顧客對企業產生負面印象。</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">「積極聆聽」（active listening）是對方在講話時，專心聆聽（non-verbal communication），不插嘴或中斷談話（listening without interruption）；針對對方的問題先想再答，想從顧客端知道的訊息先想再問。而針對一個主題的討論也不會顧左右而言他；如顧客質問：「你們的產品很爛！」業務人員卻回答：「目前該產品正在做促銷，有沒有興趣購買？」這樣的對話是很難達成共識，唯徒增顧客的抱怨。</span></p>
<h2><span style=""font-weight: 400;""><img title=""職場加分英文第一印象篇"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a28366c52617-JrH60lUxYoIoaWqetxiisPC1XrR2tO-800x426.jpeg"" alt=""職場加分英文第一印象篇"" width=""800"" height=""426"" /></span><span style=""font-weight: 400;""><br /></span></h2>
<h2><strong>本篇單字解析</strong></h2>
<p><span style=""font-weight: 400;"">plenty (n)：充裕、大量 </span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">There are still plenty of product brochures in the Marketing Department.（在行銷部門還有很多產品手冊。）</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">maintain (v)：維持、保持 </span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Dely company maintains close relations with their loyal customers.（德理公司與它們的忠實客戶保持緊密的關係。）</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">interrupt (v)：中斷、阻斷 </span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Don&rsquo;t interrupt the speaker，ask your questions afterwards.</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">（勿打斷演說者的演講，待演講結束後再提問。）</span><span style=""font-weight: 400;""><br /><br /></span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">面對國際化的商業環境，如果無法以清晰和正確的英文介紹自己或表達意見，商業夥伴可能會對你的印象大打折扣，甚至可能拒絕進一步的交談。因此，如何在職場上擁有的正確態度，進而藉由充分展現個人及建立自信心是學習的重點。希望此篇關於第一印象的英文加分題能具體給予讀者幫助。</span></p>
<p><br /><span style=""font-weight: 400;"">摘自五南文化事業機構出版</span><a href=""http://www.wunanbooks.com.tw/product/9789571163840""><span style=""font-weight: 400;"">《商用英文：最佳商務往來溝通藝術》</span></a></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '上班族職場加分題學英文｜第一印象篇','og_description' => '每一位員工都是企業對外大使，對於企業聲譽的塑造扮演著舉足輕重的角色。本篇介紹人與人見面溝通與自我介紹的方式以及注意事項，職場上班族必讀的好印象加分秘訣以及英文單字一起幫你整理！','meta_title' => '上班族職場加分題學英文｜第一印象篇','meta_description' => '每一位員工都是企業對外大使，對於企業聲譽的塑造扮演著舉足輕重的角色。本篇介紹人與人見面溝通與自我介紹的方式以及注意事項，職場上班族必讀的好印象加分秘訣以及英文單字一起幫你整理！','canonical_url' => 'https://tw.english.agency/職場英文加分題第一印象篇','feature_image_alt' => '職場英文加分題第一印象篇','feature_image_title' => '職場英文加分題第一印象篇','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a283469a60e4-sFy5nrmylJyFzKtdj856JjTTPEKPCx-1200x630.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-12-06 18:18:40"'],
  ['id' => '93','title' => '"超實用！打造專業英文履歷的4個萬用步驟 (上)"','slug' => '打造英文履歷的4個步驟','excerpt' => '履歷是求職時最基本的文件，能讓對方在最短的時間內熟悉你的個人背景。但如何簡潔又能完整呈現個人特色於履歷之中，且撰寫正確又條理的英文履歷呢？超實用的4個步驟，幫助你打造完美的英文履歷！','content' => '"<p><span style=""font-weight: 400;"">履歷是求職時最基本的文件，而英文履歷同樣無固定格式。履歷是讓對方可以在最短的時間內熟悉你個人背景的管道。所以，必須完整呈現個人的基本資料於履歷之中。撰寫之前同樣要收集自己相關資料，並以正確、條理的英文翻譯。通常有些單位要求英文的佐證資料，如：英文畢業證書、成績單等；有些國外單位或公司更要求有法院與外交部的認證，以杜絕佐證資料的造假，求職者可能要有被錄取後提供這些公證文件的心理準備。</span></p>
<p>&nbsp;</p>
<p><br /><img title=""打造英文履歷的4個步驟"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a3fe59311088-pcKFb5vd2SCCSqFiXMFDjiDyUdSYuW-800x420.jpeg"" alt=""打造英文履歷的4個步驟"" width=""800"" height=""420"" /></p>
<h2><strong> <span style=""color: #b7043e;"">STEP 01｜英文履歷必備！簡潔清楚的個人資料</span></strong></h2>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">基本的履歷必須具備個人資訊、學術背景、工作經驗與才能等四個部份。但是，請記得個人簡歷和廣告單一樣必須簡潔、清楚。根據統計，履歷頁數最好維持在A4大小紙兩頁最適當。個人資訊除要有姓名、性別、生日、地址與聯絡方式外，有時身分證件亦須一併提供。</span></p>
<h3><strong><span style=""color: #1d3854;""> 英文履歷格式參考-Curriculum Vitae</span></strong></h3>
<div style=""overflow-x: auto;"">
<table style=""width: 643px; height: 208px;"">
<tbody>
<tr style=""height: 6px;"">
<td style=""border: 1px solid black; width: 172.667px; text-align: center; padding-left: 2px; padding-right: 2px; height: 6px; vertical-align: middle;"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">Name</span></p>
</td>
<td style=""border: 1px solid black; width: 230px; padding-left: 2px; padding-right: 2px; height: 6px; vertical-align: middle; text-align: left;""><span style=""font-weight: 400;"">Male / Female</span></td>
<td style=""border: 1px solid black; width: 212.667px; text-align: center; padding-left: 2px; padding-right: 2px; height: 38.8542px; vertical-align: middle;"" rowspan=""4"">
<p><img title=""專業英文履歷"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a3fd585b544d-w9UQxUC0CuCzlUBzF85nX5rjUFj2xs-149x164.jpeg"" alt=""專業英文履歷"" width=""149"" height=""164"" /></p>
<p>若是應徵公司有特別規定，否則無需在履歷上放上大頭照。</p>
</td>
</tr>
<tr style=""height: 19px;"">
<td style=""border: 1px solid black; width: 410.667px; padding-left: 2px; padding-right: 2px; height: 19px; vertical-align: middle; text-align: left;"" colspan=""2""><span style=""font-weight: 400;"">Date of Birth: Month/Date/Year</span></td>
</tr>
<tr style=""height: 20px;"">
<td style=""border: 1px solid black; width: 410.667px; padding-left: 2px; padding-right: 2px; height: 20px; text-align: left;"" colspan=""2""><span style=""font-weight: 400;"">Address</span></td>
</tr>
<tr style=""height: 14.3333px;"">
<td style=""border: 1px solid black; width: 172.667px; text-align: center; padding-left: 2px; padding-right: 2px; height: 14.3333px;"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">E-mail</span></p>
</td>
<td style=""border: 1px solid black; width: 230px; padding-left: 2px; padding-right: 2px; height: 14.3333px;"">
<p><span style=""font-weight: 400;"">Tel</span></p>
<p><span style=""font-weight: 400;"">Mobile ph</span><span style=""font-weight: 400;"">one</span></p>
</td>
</tr>
</tbody>
</table>
</div>
<h3>&nbsp;</h3>
<h3><span style=""color: #1d3854;""> 單字小幫手</span></h3>
<p><span style=""font-weight: 400;"">(1) Curriculum Vitae&nbsp;&nbsp;</span><span style=""font-weight: 400;"">簡歷</span></p>
<p><span style=""font-weight: 400;"">可以縮寫成 CV，或以美語常用的resume來代表簡歷。</span></p>
<p><span style=""font-weight: 400;"">(2)&nbsp;英文姓名的表達</span></p>
<p><span style=""font-weight: 400;"">名在前、姓在後。如：Susan Powell，Susan是名＝first name、given name，Powell是姓＝surname、family name、last name。也可以姓在前、名在後，但是姓後必須加逗號，如：Powell，Susan。</span></p>
<p><span style=""font-weight: 400;"">(3)&nbsp;Date of Birth</span><span style=""font-weight: 400;"">（出生日期）</span></p>
<p><span style=""font-weight: 400;"">Date/Month/Year</span><span style=""font-weight: 400;"">(英式英文表達方式，</span><em><span style=""font-weight: 400;"">BrE</span></em><span style=""font-weight: 400;"">)</span></p>
<p><span style=""font-weight: 400;"">Month/Date/Year </span><span style=""font-weight: 400;"">(美式英文表達方式，</span><em><span style=""font-weight: 400;"">AmE</span></em><span style=""font-weight: 400;"">)</span></p>
<p><span style=""font-weight: 400;"">(4)&nbsp;地址的表達</span><span style=""font-weight: 400;"">：□□□□□（郵遞區號zip code/postcode/postal code）地址撰寫從最小單位至最大地方。</span></p>
<p><span style=""font-weight: 400;"">如：台北市103大同區承德路二段133巷76號8樓之2</span></p>
<p><span style=""font-weight: 400;"">103&nbsp; &nbsp;8F-2，No.76，Lane 133，Chen-De Road Section 2，Da-Tung District，Taipei，Taiwan</span></p>
<p><span style=""font-weight: 400;"">【其他地址的英文說法】室 </span><span style=""font-weight: 400;"">Room</span><span style=""font-weight: 400;"">，樓 </span><span style=""font-weight: 400;"">Fl. </span><span style=""font-weight: 400;"">或 </span><span style=""font-weight: 400;"">F</span><span style=""font-weight: 400;"">，號 </span><span style=""font-weight: 400;"">No.</span><span style=""font-weight: 400;"">，棟 </span><span style=""font-weight: 400;"">Building</span><span style=""font-weight: 400;"">，弄 </span><span style=""font-weight: 400;"">Alley</span><span style=""font-weight: 400;"">，巷 </span><span style=""font-weight: 400;"">Lane</span><span style=""font-weight: 400;"">，街 </span><span style=""font-weight: 400;"">Street</span><span style=""font-weight: 400;"">，段 </span><span style=""font-weight: 400;"">Section</span><span style=""font-weight: 400;"">，路 </span><span style=""font-weight: 400;"">Road</span><span style=""font-weight: 400;"">，鄰 </span><span style=""font-weight: 400;"">Neighborhood</span><span style=""font-weight: 400;"">，里、村 </span><span style=""font-weight: 400;"">Village</span><span style=""font-weight: 400;"">，區 </span><span style=""font-weight: 400;"">District</span><span style=""font-weight: 400;"">，鄉、鎮 </span><span style=""font-weight: 400;"">Town</span><span style=""font-weight: 400;"">，市 </span><span style=""font-weight: 400;"">City</span><span style=""font-weight: 400;"">，縣 </span><span style=""font-weight: 400;"">County</span><span style=""font-weight: 400;"">，省 </span><span style=""font-weight: 400;"">Province</span></p>
<p><span style=""font-weight: 400;"">(5)&nbsp;手機</span></p>
<p><em><span style=""font-weight: 400;"">BrE</span></em><span style=""font-weight: 400;"">多使用mobile phone，</span><em><span style=""font-weight: 400;"">AmE</span></em><span style=""font-weight: 400;"">較多使用cell phone或cellular phone。</span></p>
<p>&nbsp;</p>
<p><img title=""打造英文履歷的4個步驟"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a3fe6ed481d9-y3yS6JZ1VmKOe5Z9C8SZfQ2d0htdAi-720x384.jpeg"" alt=""打造英文履歷的4個步驟"" width=""720"" height=""384"" /></p>
<h2>&nbsp;</h2>
<h2><strong><span style=""color: #b7043e;"">STEP 02｜學術背景英文怎麼寫，履歷重點單字一次整理</span></strong></h2>
<p><span style=""font-weight: 400;"">學術背景基本上要敘述受教育過程，但需要用到英文履歷的用人單位，大致上只要陳述專科以上學歷，包括學校、國別、求學期間、主修課程、學業成績以及論文等。</span></p>
<h3>&nbsp;</h3>
<h3><strong><span style=""color: #1d3854;""> 英文履歷格式參考-Academic Background</span></strong></h3>
<div style=""overflow-x: auto;"">
<table style=""width: 628px; height: 305px;"">
<tbody>
<tr style=""height: 6px;"">
<td style=""border: 1px solid black; width: 86.3333px; text-align: center; padding-left: 2px; padding-right: 2px; height: 6.7083px; vertical-align: middle;"" rowspan=""2"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">Date</span></p>
</td>
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 6px; vertical-align: middle; text-align: left;"">
<p><span style=""font-weight: 400;"">University graduated and degree received</span></p>
<p><span style=""font-weight: 400;"">(碩士學位、畢業學校)</span></p>
</td>
</tr>
<tr style=""height: 14px;"">
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 14px;"">
<p><span style=""font-weight: 400;"">your major，or dissertation title</span></p>
<p><span style=""font-weight: 400;"">(說明主修學程，或是碩士論文題目，或是獲獎細項)</span></p>
</td>
</tr>
<tr style=""height: 14px;"">
<td style=""border: 1px solid black; width: 86.3333px; text-align: center; padding-left: 2px; padding-right: 2px; height: 28px;"" rowspan=""2"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">Date</span></p>
</td>
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 14px;"">
<p><span style=""font-weight: 400;"">University or College graduated and degree received</span></p>
<p><span style=""font-weight: 400;"">（學士學位、畢業學校）</span></p>
</td>
</tr>
<tr style=""height: 14px;"">
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 14px;"">
<p>&nbsp;<span style=""font-weight: 400;"">your major，or records of awards</span></p>
<p><span style=""font-weight: 400;"">(說明主修學程，或是獲獎細項。)</span></p>
</td>
</tr>
<tr style=""height: 14px;"">
<td style=""border: 1px solid black; width: 86.3333px; text-align: center; padding-left: 2px; padding-right: 2px; height: 28px;"" rowspan=""2"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">Date</span></p>
&nbsp;</td>
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 14px;"">&nbsp;<span style=""font-weight: 400;"">School graduated</span><span style=""font-weight: 400;"">（畢業學校）</span></td>
</tr>
<tr style=""height: 14px;"">
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 14px;"">&nbsp;<span style=""font-weight: 400;"">records of awards (說明獲獎細項)</span></td>
</tr>
</tbody>
</table>
</div>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<h3><strong><span style=""color: #1d3854;""> 單字小幫手</span></strong></h3>
<p><span style=""font-weight: 400;"">(1) Academic Background</span><span style=""font-weight: 400;"">：學術背景</span></p>
<p><span style=""font-weight: 400;"">If you want to apply for a good job，it would be necessary to describe your </span><em><span style=""font-weight: 400;"">academic background</span></em><span style=""font-weight: 400;""> in relation to the job.</span></p>
<p><span style=""font-weight: 400;"">（假如你想找份好工作，描述與工作相關的學術背景是必要的。）</span></p>
<p><span style=""font-weight: 400;"">(2) 學校與學位的表達</span><span style=""font-weight: 400;"">：</span></p>
<p><span style=""font-weight: 400;"">elementary school/primary school</span><span style=""font-weight: 400;""> 小學</span></p>
<p><span style=""font-weight: 400;"">junior high school</span><span style=""font-weight: 400;""> &nbsp;國中</span></p>
<p><span style=""font-weight: 400;"">senior high school</span><span style=""font-weight: 400;""> 高中</span></p>
<p><span style=""font-weight: 400;"">college</span><span style=""font-weight: 400;""> 專科/學院：畢業後所授予的學位叫「學士」。</span></p>
<p><span style=""font-weight: 400;"">B.A. (Bachelor of Arts) 文學士</span></p>
<p><span style=""font-weight: 400;"">B.Sc. (Bachelor of Science) 理學士</span></p>
<p><em><span style=""font-weight: 400;"">專科（五專、三專、二專）文憑叫 College Diploma</span></em></p>
<p><span style=""font-weight: 400;"">university</span><span style=""font-weight: 400;""> 大學：畢業後所授予的學位叫「學士」。</span></p>
<p><span style=""font-weight: 400;"">B.A. (Bachelor of Arts) 文學士</span></p>
<p><span style=""font-weight: 400;"">B.Sc. (Bachelor of Science) 理學士</span></p>
<p><em><span style=""font-weight: 400;"">大學生叫 &rdquo;undergraduate&rdquo;，&ldquo;college student &rdquo;</span></em></p>
<p><span style=""font-weight: 400;"">graduate school</span><span style=""font-weight: 400;""> 研究所：畢業後所授予的學位叫「碩士」。</span></p>
<p><span style=""font-weight: 400;"">M.A. (Master of Arts) 文學碩士</span></p>
<p><span style=""font-weight: 400;"">M.L. (Master of Laws) 法學碩士</span></p>
<p><span style=""font-weight: 400;"">MBA (Mater of Business Administration)</span> <span style=""font-weight: 400;"">企業管理碩士</span></p>
<p><span style=""font-weight: 400;"">MSc (Master of Science) 理學碩士</span></p>
<p><span style=""font-weight: 400;"">Ph.D. (Doctor of Philosophy) 博士</span></p>
<p><span style=""font-weight: 400;"">post doctoral / post doc博士後研究</span></p>
<p><em><span style=""font-weight: 400;"">研究生叫 &rdquo;postgraduate/graduate student&rdquo;</span></em></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">看完以上的兩個步驟，希望提供大家一個方向，當然自己也能參考修改成自己專屬的一份好的英文履歷。</span></p>
<p><span style=""font-weight: 400;"">好的英文履歷</span><span style=""font-weight: 400;"">代表求職者的英文能力，或是有尋找跟運用英文資源的能力。這種能力自然對需要英文能力的公司很有幫助，包括外商、本地公司的涉外部門、外派職位等。希望本篇對大家有幫助。</span></p>
<p>&nbsp;</p>
<p><em><span style=""font-weight: 400;"">摘自五南文化事業機構出版</span><a href=""http://www.wunanbooks.com.tw/product/9789571163840""><span style=""font-weight: 400;"">《商用英文：最佳商務往來溝通藝術》</span></a></em></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"超實用！打造專業英文履歷的4個萬用步驟 (上)"','og_description' => '履歷是求職時最基本的文件，能讓對方在最短的時間內熟悉你的個人背景。但如何簡潔又能完整呈現個人特色於履歷之中，且撰寫正確又條理的英文履歷呢？超實用的4個步驟，幫助你打造完美的英文履歷！','meta_title' => '"超實用！打造專業英文履歷的4個萬用步驟 (上)"','meta_description' => '履歷是求職時最基本的文件，能讓對方在最短的時間內熟悉你的個人背景。但如何簡潔又能完整呈現個人特色於履歷之中，且撰寫正確又條理的英文履歷呢？超實用的4個步驟，幫助你打造完美的英文履歷！','canonical_url' => 'https://tw.english.agency/打造英文履歷的4個步驟','feature_image_alt' => '打造英文履歷的4個步驟','feature_image_title' => '打造英文履歷的4個步驟','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a3fe54bd051e-x9c2vMeSFUZoh7Og0A9EKADDccZ8Rb-1200x630.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-12-22 01:56:43"'],
  ['id' => '94','title' => '"超實用！打造專業英文履歷的4個萬用步驟 (下)"','slug' => '打造英文履歷的4個步驟下','excerpt' => '通常履歷中能力與資格的呈現是不可或缺的，學歷與畢業科系符合，也是徵才的必要條件！如何從寫英文履歷開始脫穎而出，又該如何呈現自己的優勢呢？超實用的4個步驟，幫助你打造完美的英文履歷！','content' => '"<p>前篇與大家分享撰寫英文履歷中重要的4步驟，其中簡潔清楚的「個人資料」與「自我學術背景」描述部分都是比較好掌握的主體。接下來幫大家整理的兩個步驟，在英文履歷中屬於最具靈魂的部分，能透過履歷呈現自己最好的價值，打造起跑點的好分數，那麼，該如何呈現呢？跟著讀完接續的兩個步驟，專屬於你自己的完美英文履歷即將完成！</p>
<p>&nbsp;</p>
<h2><img title=""打造英文履歷的4個步驟"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a3ff52945e03-amk4rFPXGHv5U15WXtoKVG7WdaDKZQ-800x426.jpeg"" alt=""打造英文履歷的4個步驟"" width=""800"" height=""426"" /></h2>
<h2><br /><strong><span style=""color: #b7043e;"">STEP 03｜一份好的英文履歷，工作經驗不可或缺</span></strong></h2>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">工作經驗則需詳列之前曾經工作過的公司或單位，並描述該工作內容，及提供公司聯絡方式以做必要時之檢驗。才能項目則可羅列許多細項，包含語文能力、人格特質、擁有證照等等。當然你也可以依照工作特性詳列個人的傑出表現，例如：特殊榮譽或成就表現、實驗成果、獲得專利、表揚事蹟、展覽演出等等。</span></p>
<p>在工作經驗欄目中，涵蓋所有支薪或是義務的職位，花點時間寫下從事這些工作，你學習到的技能和專才以及從中所獲取的成就與傑出表現。另外，專家並不建議在履歷中寫下你的薪資所得。</p>
<h3><strong><span style=""color: #1d3854;""> 英文履歷格式參考-Work Experiences</span></strong></h3>
<table style=""width: 628px; height: 305px;"">
<tbody>
<tr style=""height: 6px;"">
<td style=""border: 1px solid black; width: 86.3333px; text-align: center; padding-left: 2px; padding-right: 2px; height: 6.7083px; vertical-align: middle;"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">Date</span></p>
</td>
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 6px; vertical-align: middle; text-align: left;"">
<p><span style=""font-weight: 400;"">Company Name，Job Title（公司名稱、職稱）</span></p>
<p><span style=""font-weight: 400;"">Job descriptions</span><span style=""font-weight: 400;"">（工作內容簡述）</span></p>
<p><span style=""font-weight: 400;"">Address</span><span style=""font-weight: 400;"">（地址）</span></p>
</td>
</tr>
<tr style=""height: 14px;"">
<td style=""border: 1px solid black; width: 86.3333px; text-align: center; padding-left: 2px; padding-right: 2px; height: 28px;"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">Date</span></p>
</td>
<td style=""border: 1px solid black; width: 523.667px; padding-left: 2px; padding-right: 2px; height: 14px;"">
<p><span style=""font-weight: 400;"">Company Name，Job Title（公司名稱、職稱）</span></p>
<p><span style=""font-weight: 400;"">Job descriptions</span><span style=""font-weight: 400;"">（工作內容簡述）</span></p>
<p><span style=""font-weight: 400;"">Address</span><span style=""font-weight: 400;"">（地址）</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h3><span style=""color: #1d3854;""> 單字小幫手</span></h3>
<p>work experience：工作經驗</p>
<p><span style=""font-weight: 400;"">Our manager has plenty of </span><em><span style=""font-weight: 400;"">work experiences</span></em><span style=""font-weight: 400;"">.</span></p>
<p><span style=""font-weight: 400;"">（我們的經理擁有豐富的工作經驗。）</span></p>
<p><span style=""font-weight: 400;"">MBA students with limited or no </span><em><span style=""font-weight: 400;"">work experience</span></em><span style=""font-weight: 400;""> need to highlight their potential.</span></p>
<p><span style=""font-weight: 400;"">（僅有有限或是無工作經驗的企管碩士學生，必須強調他們的潛力。）</span></p>
<p>&nbsp;</p>
<h2>&nbsp;<img title=""打造英文履歷的4個步驟"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a3ff6fbee650-2fi3NETz4H3Ke5tzM4SfUU77oaZYzd-680x371.jpeg"" alt=""打造英文履歷的4個步驟"" width=""680"" height=""371"" /></h2>
<p>&nbsp;</p>
<h2><strong><span style=""color: #b7043e;"">STEP 04｜展現自己最好的優勢，英文履歷大加分</span></strong></h2>
<p><span style=""font-weight: 400;"">通常履歷中能力與資格的呈現是不可或缺的，學歷與畢業科系符合徵才需求是必要條件，而能力與資格則是充分條件。在不造假的情況下，列出你所擁有的「優勢」，才能從求職者中脫穎而出。如果你驚覺在這方面仍「不具競爭力」，就要在求學時或想要轉業前，好好規劃如何提升自己的競爭優勢。</span></p>
<p><span style=""font-weight: 400;"">對於能力與資格的描述，可以關鍵字方式條列，應避免長篇大論式的細數家珍。</span></p>
<h3>&nbsp;</h3>
<h3><strong><span style=""color: #1d3854;""> 英文履歷格式參考-Competence and Qualifications</span></strong></h3>
<div style=""overflow-x: auto;"">
<table style=""width: 597px; height: 119px;"">
<tbody>
<tr style=""height: 6px;"">
<td style=""border: 1px solid black; width: 585.333px; text-align: center; padding-left: 2px; padding-right: 2px; height: 6.7083px; vertical-align: middle;"">
<p style=""text-align: left;""><span style=""font-weight: 400;"">Please write down your personality and core competences.</span></p>
<p style=""text-align: left;""><span style=""font-weight: 400;"">（請寫下你的人格特質與核心能力。）</span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<h3><strong><span style=""color: #1d3854;""> 單字小幫手</span></strong></h3>
<p><span style=""font-weight: 400;"">(1)&nbsp;</span>Competence (n)：能力、優勢</p>
<p><em><span style=""font-weight: 400;"">Competence</span></em><span style=""font-weight: 400;""> is a standardized requirement for an individual to properly perform a specific job.</span></p>
<p><span style=""font-weight: 400;"">(能力是指一個人可以稱心地執行一項工作之標準條件。)</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">(2)&nbsp;擁有「聽、說、讀、寫」的語言能力，這些語言可能含有：</span></p>
<p><span style=""font-weight: 400;"">Taiwanese 台語、Mandarin/Chinese 中文、English 英語、French 法語、Deutsche/German 德語、Japanese 日語、Korean 韓語、Spanish 西班牙語、Italian 義大利語、Portuguese 葡萄牙語、Indian 印度語、Thai泰國語、Russian 俄語、Swedish瑞典語、Hebrew 希伯來語。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">(3)&nbsp;擁有個人的「優勢」，這些強項可能指擁有以下的人格特質：</span></p>
<p><span style=""font-weight: 400;"">－interpersonal skills - ability to put people at ease &nbsp;&nbsp;人際關係智慧</span></p>
<p><span style=""font-weight: 400;"">－dependable 可靠的</span></p>
<p><span style=""font-weight: 400;"">－organized 有組織能力的</span></p>
<p><span style=""font-weight: 400;"">－efficient 有效率的</span></p>
<p><span style=""font-weight: 400;"">－team worker 具團隊精神</span></p>
<p><span style=""font-weight: 400;"">－self-starter 自動自發</span></p>
<p><span style=""font-weight: 400;"">－adaptable 適應能力強</span></p>
<p><span style=""font-weight: 400;"">－client focused 以客為尊</span></p>
<p><span style=""font-weight: 400;"">－communication 溝通能力</span></p>
<p><span style=""font-weight: 400;"">－creative problem solver 創意危機處理</span></p>
<p><span style=""font-weight: 400;"">－drive to achieve 達成目標</span></p>
<p><span style=""font-weight: 400;"">－passion for the business 工作熱忱</span></p>
<p><span style=""font-weight: 400;"">－take ownership 負責任、主動参與各項任務</span></p>
<p><span style=""font-weight: 400;"">－trustworthy 可信賴</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">(4)&nbsp;擁有「商業技能」，這些可包含：</span></p>
<p><span style=""font-weight: 400;"">－word processing 電腦文書處理</span></p>
<p><span style=""font-weight: 400;"">－presentation skill 演說能力</span></p>
<p><span style=""font-weight: 400;"">－negotiating skill 協商能力</span></p>
<p><span style=""font-weight: 400;"">－objection handling skill 危機處理能力</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">(5)&nbsp;qualification (n)：資格證書、執照</span></p>
<p><span style=""font-weight: 400;"">What are the </span><em><span style=""font-weight: 400;"">qualifications</span></em><span style=""font-weight: 400;""> for a sales specialist?</span></p>
<p><span style=""font-weight: 400;"">(什麼是當一名業務代表須具備的條件？)</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">(6)&nbsp;擁有國家或國際認可之「證照」將為你的履歷與求職加分，這些可能是：</span></p>
<p><span style=""font-weight: 400;"">外語證照：GEPT全民英檢、TOEIC多益、JLPT日語檢定</span></p>
<p><span style=""font-weight: 400;"">TESL、TESOL、TEFL英文師資認證。</span></p>
<p><span style=""font-weight: 400;"">電腦證照：</span><span style=""font-weight: 400;"">作業系統</span><span style=""font-weight: 400;""> &rarr; CSA、LINUX、LINUX LPI、RHCE</span></p>
<p><span style=""font-weight: 400;"">網路管理</span><span style=""font-weight: 400;""> &rarr; CCDA、CCDP、CCIE、CCNA、CCNP、CCSP、CNA、ITE、NCIP、CNE、Master CNE</span></p>
<p><span style=""font-weight: 400;"">程式設計</span><span style=""font-weight: 400;""> &rarr; Embedded 微軟、ITE、SCAJ、 &nbsp;SCJD、SCJP</span></p>
<p><span style=""font-weight: 400;"">多媒體</span><span style=""font-weight: 400;""> &rarr; 3D Studio Max、AutoCAD、SolidWorks、3D動畫應用、MAYA 動畫設計、電腦輔助製圖技術士、電腦遊戲動畫認證、影像動畫特效</span></p>
<p><span style=""font-weight: 400;"">商管證照：PMP專案管理、WBSA商務策劃　</span></p>
<p><span style=""font-weight: 400;"">技職證照：CAD建築機械製圖、中餐技術士、國家乙丙級技術士認證、電腦技能基金會TQC認證　</span></p>
<p><span style=""font-weight: 400;"">財經證照：</span><span style=""font-weight: 400;"">國家</span><span style=""font-weight: 400;""> &rarr;不動產經紀人、保險證照、證券期貨</span></p>
<p><span style=""font-weight: 400;"">國際 &rarr;CFA美國特許財務分析師、FRM風險管理師</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">此外，有些公司或單位甚至要求應徵者提供推薦信（references），一般推薦人多為學校老師或是之前工作的主管。所以，及早準備推薦信是必要的，好的、具說服力的推薦信絕對可以為求職加分。</span><span style=""font-weight: 400;""><br /></span></p>
<p>&nbsp;</p>
<p><em><span style=""font-weight: 400;"">摘自五南文化事業機構出版</span><a href=""http://www.wunanbooks.com.tw/product/9789571163840""><span style=""font-weight: 400;"">《商用英文：最佳商務往來溝通藝術》</span></a></em></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"超實用！打造專業英文履歷的4個萬用步驟 (下)"','og_description' => '通常履歷中能力與資格的呈現是不可或缺的，學歷與畢業科系符合，也是徵才的必要條件！如何從寫英文履歷開始脫穎而出，又該如何呈現自己的優勢呢？超實用的4個步驟，幫助你打造完美的英文履歷！','meta_title' => '"超實用！打造專業英文履歷的4個萬用步驟 (下)"','meta_description' => '通常履歷中能力與資格的呈現是不可或缺的，學歷與畢業科系符合，也是徵才的必要條件！如何從寫英文履歷開始脫穎而出，又該如何呈現自己的優勢呢？超實用的4個步驟，幫助你打造完美的英文履歷！','canonical_url' => 'https://tw.english.agency/打造英文履歷的4個步驟下','feature_image_alt' => '打造英文履歷的4個步驟','feature_image_title' => '打造英文履歷的4個步驟','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a3fecb288a1f-VmKGDoGdGWb2h88IzxRERQ13BM5SNd-1200x630.jpeg','author_id' => '11','category_id' => '18','created_at' => '"2017-12-24 18:06:50"'],
  ['id' => '95','title' => '托福聽力準備？考試技巧、聽力題型、計分方式、托福聽力筆記方法公開！','slug' => 'tofel-listening','excerpt' => '到底該如何準備托福的聽力呢？對於很多人來說，托福聽力考試是相當難以準備的，而除了硬底子的乖乖準備考試之外，不可忽略的部分當然還包括了最重要的考試技巧-『做筆記』，那惡這筆記到底該怎麼做呢？','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p>托福聽力到底該如何準備呢？對於很多人來說，<a href=""http://www.toefl.com.tw/nl201408_08n.jsp"">托福聽力考試</a>是相當難以準備的，而除了硬底子的乖乖準備考試之外，不可忽略的部分當然還包括了最重要的考試技巧-『做筆記』，那惡這筆記到底該怎麼做呢？</p>
<p>&nbsp;</p>
<p><img title=""托福聽力考試題型"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4627154bba4-NWoV4jk177Fdc47vKD5E8YFi5LleK6-800x600.jpeg"" alt=""托福考試聽力題型"" width=""100%"" height=""auto"" />&nbsp;</p>
<h2>托福聽力考試題型：</h2>
<p><span style=""font-weight: 400;"">托福聽力主要有兩種題型，<strong>3分鐘長度的 conversation</strong> 以及<strong>5分鐘左右的 lecture</strong> 題型，不要覺得3、5分鐘好像很短，其實裡面內容可是相當多，畢竟托福考試是在測試考生的「硬實力」和找尋重要資訊的能力，因此一篇聽力的內容可是相當豐富，而且範圍也很廣泛，所以考生在準備聽力時常遇到的問題如下：</span></p>
<p>&nbsp;</p>
<ul>
<li style=""color: #ff6666;"">加試題是什麼？</li>
</ul>
<blockquote>加試題試托福官方<a href=""http://www.toefl.com.tw/about_ets.jsp"">ETS</a>用來衡量本次考試難度的一個指標，而這樣的加試題又會分別出現在閱讀、聽力，此外加試題還分為經典題、非經典題，兩種類型，而考生通常是無法察覺的，此外加試題通常是不計算分數的！</blockquote>
<ul>
<li style=""color: #ff6666;"">那麼托福聽力計分怎麼算？</li>
</ul>
<blockquote>托福的聽力計分是採取量表的方式計分，而聽力又有分為多選、複選類型的題目，例如說4選3這樣的題目，如果選錯了則會加上一定的分數，而這樣的計分骯是又分為1分值、2分值，每一題的配分方式不同。</blockquote>
<span style=""font-weight: 400;"">所以考生在準備聽力時常遇到的問題如下：</span><hr />
<p>&nbsp;</p>
<h2>托福聽力筆記 ：</h2>
<p style=""color: #ff6666;""><strong>A.考托福聽力一定要做筆記嗎？</strong></p>
<p><span style=""font-weight: 400;"">當然可以不用，畢竟 </span><span style=""font-weight: 400;"">ETS </span><span style=""font-weight: 400;"">並沒有強迫考生做筆記，如果有做筆記考完試也不會被收回去。</span></p>
<p>&nbsp;</p>
<p style=""color: #ff6666;""><strong>B.那到底要不要做筆記？做筆記真的有效嗎？</strong></p>
<p><span style=""font-weight: 400;"">如果一篇聽力只能聽懂 </span><span style=""font-weight: 400;"">60% </span><span style=""font-weight: 400;"">以下，那會建議先了解文章大意，等到聽力能力慢慢進步，聽的懂 </span><span style=""font-weight: 400;"">60~70% </span><span style=""font-weight: 400;"">後再來試著邊聽邊做筆記。做筆記當然有效，前提是能抓到 </span><span style=""font-weight: 400;"">keywords </span><span style=""font-weight: 400;"">並把它寫下來，而不是一字不漏地把聽到的都「抄寫」下來。</span></p>
<p>&nbsp;</p>
<p style=""color: #ff6666;""><strong>C.抄筆記會影響聽力，一定要抄嗎？</strong></p>
<p><span style=""font-weight: 400;"">就像前面所說的，如果一篇</span><span style=""font-weight: 400;"">lecture</span><span style=""font-weight: 400;"">只能聽得懂 </span><span style=""font-weight: 400;"">60% </span><span style=""font-weight: 400;"">那就先專注於理解文章大意，不要分心做筆記，但如果能理解 </span><span style=""font-weight: 400;"">70% </span><span style=""font-weight: 400;"">以上，就建議要動手寫筆記，畢竟 95% 的考生聽托福聽力會恍神，一篇 5 分鐘的 </span><span style=""font-weight: 400;"">lecture</span><span style=""font-weight: 400;"">，如果有信心可以不抄筆記還能正確地回答出主旨題、細節題、</span><span style=""font-weight: 400;"">imply </span><span style=""font-weight: 400;"">題、</span><span style=""font-weight: 400;"">conclusion </span><span style=""font-weight: 400;"">題、推測題、</span><span style=""font-weight: 400;"">tone </span><span style=""font-weight: 400;"">題，那當然可以不抄！</span></p>
<hr />
<h2><img title=""TOFEL聽力準備方法"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a462c8923a28-VuUOjJKRKFol7PsXyY8lfT7LTvHJH0-800x600.jpeg"" alt=""TOFEL聽力準備方法"" width=""100%"" height=""auto"" /></h2>
<h2>該如何做托福聽力準備？</h2>
<p>托福聽力的確是很多人無法突破的一關，但偏偏聽力實際所佔的比例又很高，除了本身的30分外，口說和寫作也都有聽力的介入，而且所佔的比例也不低，需要通過聽力才能作答，因此累積聽力實力是必然的。</p>
<p><strong><em>累積？ 對！累積！英文實力得確是必須透過累積才能在考試時幫助你拿高分。</em></strong></p>
<p>&nbsp;</p>
<p><strong><span style=""color: #ff884d;"">1.準備週期太短，成效不彰，6-12月為最佳準備期間：</span></strong></p>
<p><span style=""font-weight: 400;"">考托福前三個月甚至兩個月前才來衝刺的進步是很有限的，我建議最好把準備考試的時間軸拉到半年或以上並且不要快到考試前才來接觸英文，尤其是平時很少接觸英文的考生們，因為英文實力是必須透過累積的。</span></p>
<p><strong><span style=""color: #ff884d;"">2.全英語的環境很重要，培養英文的邏輯思考好處多多：</span></strong></p>
<p><span style=""font-size: 14px;"">首先最重要的就是讓自己長時間沈浸在英文的環境，你可能會覺得很 clich&eacute; ，但好習慣的養成是很重要的，不只是多看、多聽、多說、多寫，除了平常坐下來認真把書打開準備托福之外，其餘時間也必須好好利用來養成用英文邏輯思考的能力。培養英文的思考邏輯會讓你在準備托福時更得心應手，之後出國讀書才能較輕鬆地應付essay和各種報告考試。</span></p>
<p><img title=""pod-cast-TOFEL聽力準備"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a462302bac10-56pAe40rqIDDj3tLPDnRydx6ee2yi3-785x487.png"" alt=""pod-cast-TOFEL聽力準備"" width=""100%"" height=""auto"" />&nbsp;</p>
<p style=""text-align: center;""><strong>Podcast 播客</strong></p>
<p><span style=""font-weight: 400;"">在準備考試時除了下載TPO聽力在手機裡練習時，Podcast有非常多資源能增加背景知識，尤其是</span><a href=""https://www.scientificamerican.com/podcast/60-second-science/""><span style=""font-weight: 400;"">60-second science</span></a><span style=""font-weight: 400;"">，每篇只有60秒並以科學知識為主題，托福聽力其實很常出現科學或生物相關的內容（雖然我只考過一次托福，但那次的聽力和托福有很大一部分都是生物科學相關，像是爬蟲類、壁虎的習性和繁殖和元素週期表）。</span></p>
<p><span style=""font-weight: 400;"">以下是準備的建議方法：</span></p>
<p>&nbsp;</p>
<p style=""color: #ff6666;""><span style=""font-weight: 400;"">1<strong>.不要仰賴字幕，嘗試用邏輯推斷、專注。</strong></span></p>
<p><span style=""font-weight: 400;"">建議在練習聽力時，不要一邊看著transcript一邊聽，很多考生只聽一遍之後發現太多聽不懂所以直接打開下面的script邊聽邊看，千萬不要這麼做啊！這麼做只會讓你養成依賴翻譯的惰性，所以在聽的時候根本無法專注。</span></p>
<p>&nbsp;</p>
<p style=""color: #ff6666;""><strong>2.聽不懂多聽幾次，習慣發音，練習做筆記跟重點。</strong></p>
<p><span style=""font-weight: 400;"">如果聽一次聽不懂，那就多聽三四次直到了解至少80%，雖然他的語調比考試稍快、起伏較大，但我認為非常適合用來練習短時間練習聽力筆記和做重點，尤其是剛開始接觸托福的同學們一定很難忍受攏長的TPO聽力，很容易聽一聽就恍神無法專注了，60-second science雖然難度稍高，但他只有60秒而且可以調整速度，如果還是容易恍神，就拿紙筆做筆記，練習抓重點，如果身邊沒有工具時，可以聽完後自己 summarize，練習用自己的話來整理剛聽過的內容，同時練習聽力和口說。</span></p>
<p><img title=""TED-托福聽力準備"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a462208cc576-YGOH8pWt4jfxVKeKSYWX6jwuEYDMPP-1205x600.png"" alt=""TED-托福聽力準備"" width=""100%"" height=""auto"" />&nbsp;</p>
<p style=""text-align: center;""><strong>Video</strong></p>
<p><span style=""font-weight: 400;"">準備托福準備的很累時，除了透過英文影集和電影（當然要關掉字幕）一邊放鬆一邊練習聽力之外，以下的線上資源非常適合用來&ldquo;被動的&rdquo;累積實力。</span></p>
<ul>
<li style=""font-weight: 400;""><a href=""https://www.ted.com/talks?language=zh-tw""><span style=""font-weight: 400;"">TED Talk</span></a></li>
<li style=""font-weight: 400;""><a href=""http://edition.cnn.com/cnn10""><span style=""font-weight: 400;"">CNN 10</span></a></li>
<li style=""font-weight: 400;""><a href=""https://tw.voicetube.com/?ref=logo?mtc=Denise_blog_9345""><span style=""font-weight: 400;"">VoiceTube</span></a></li>
</ul>
<p><a href=""http://edition.cnn.com/cnn10""><span style=""font-weight: 400;"">CNN 10</span></a><span style=""font-weight: 400;"">每個影片只有10分鐘，內容主要是時事報導，累積世界觀對於口說和寫作來說非常加分，善用&ldquo;舉例&rdquo;就是托福拿高分的關鍵！TED Talk 也能給你很多寫作方面的靈感啟發。</span></p>
<hr />
<h2>托福聽力練習重點</h2>
<p><span style=""font-weight: 400;"">托福聽力的長度對於剛開始準備托福</span><span style=""font-weight: 400;"">、或英文程度比較沒有那麼好的同學其實是很痛苦的，除了lecture長度無法負荷之外，一但前面重要的觀念沒有接收到，到後面其實是很容易放空恍神、然後就會呈現半放棄的狀態亂猜題目或運用自己的知識來作答，此時影片就是最適合用來培養英文&ldquo;興趣&rdquo;和實力的最佳利器。</span></p>
<p><span style=""font-weight: 400;"">除了有影像比較不會無聊和恍神之外，可以根據當天想準備的主題來挑選內容。比起影集，這些就像是lecture一樣，內容多元而且可以學習到很多重要的英文單字！如果遇到一再出現的英文單字，請把它學起來並且懂的如何&ldquo;運用&rdquo;它！</span></p>
<p><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">最後再補充幾點，如果做完TPO發現在某些&ldquo;主題&rdquo;（例如考古、歷史、宇宙）的背景知識不足所以做題起來很困難，Podcast有很多免費、而且是知名大學所提供的各種lecture，像是</span> <a href=""https://archive.org/details/ucberkeleylectures&amp;tab=about""><span style=""font-weight: 400;"">U.C. Berkeley</span></a><span style=""font-weight: 400;"">，上面就有地理、哲學、星象等托福常見的考試主題。</span></p>
<p><span style=""font-weight: 400;"">好好利用以上的資源來累積英聽實力，你就會慢慢發現，其實托福聽力真的沒有想像中的那麼難征服了！</span></p>
<hr />
<h2><strong><img title=""TOFEL聽力考試技巧"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a462a461853b-GpclnzmfpTbZOJX0hVEpqH6caQU7v0-800x600.jpeg"" alt=""TOFEL聽力考試技巧"" width=""100%"" height=""auto"" /></strong></h2>
<h2><strong>那麼托福聽力技巧有哪些？</strong></h2>
<p><span style=""font-weight: 400;"">前面剛剛提到的筆記也是有技巧，並不是把聽到的全部細節都抄下來，做筆記的重點大致如下：</span></p>
<p>&nbsp;</p>
<p style=""color: #ff751a;""><strong>1.先聽懂再寫，而且是寫重點</strong></p>
<p><span style=""font-weight: 400;"">ETS 會在托福聽力裡面安插不重要的資訊在裡面來使你分心，因此不要每個字都寫下來，不必要的筆記會增加做題時找答案的時間。</span></p>
<p>&nbsp;</p>
<p style=""color: #ff751a;""><strong>2.注意轉折語氣、態度語氣的變化、停頓語氣、強調或問答</strong></p>
<p><span style=""font-weight: 400;"">轉折詞例如 But，However，Nevertheless，Yet，Except 後面通常都是接重要的資訊！也就是考點！因此要特別注意轉折詞後面的資訊，通常 lecturer 前面都會說的落落長，但是一旦 However 出現後，後面接著的通常就是那題的答案！</span></p>
<p><span style=""font-weight: 400;"">如果 lecturer 從負面語氣轉成正面時（或正面轉負面），就需要特別注意使他態度轉變的原因，有時他也會停頓或表示驚訝，像是 hmmm，oh!，wow! 等也都需要特別注意。</span></p>
<p><span style=""font-weight: 400;"">至於聽到 <em>importantly，especially，mainly，the most important of all</em> 等詞出現時，請馬上回神做筆記！</span></p>
<p><span style=""font-weight: 400;""> &nbsp;</span></p>
<p style=""color: #ff751a;""><strong>3.分段做筆記</strong></p>
<p><span style=""font-weight: 400;"">一篇 5 分鐘的聽力擁有相當多的資訊，筆記不用做得很漂亮、很整齊，但是至少要自己看得懂，因此建議在試題開始前，在空白紙上畫線來做區分，或是把紙對折再對折，利用區塊來做出有條理、有邏輯又簡單明瞭的筆記。做筆記是需要練習的，TPO 就是練習的最佳教材！</span></p>
<p>&nbsp;</p>
<p style=""color: #ff751a;""><span style=""font-weight: 400;"">4.&nbsp;</span><strong>多利用符號和簡寫</strong></p>
<p><span style=""font-weight: 400;"">符號和簡寫能有效地縮短做筆記的時間，像是寫下「 </span><span style=""font-weight: 400;"">&agrave;</span><span style=""font-weight: 400;""> / </span><span style=""font-weight: 400;"">&szlig;</span><span style=""font-weight: 400;"">」可以用來表示因果關係，「</span><span style=""font-weight: 400;"">L</span><span style=""font-weight: 400;"">」表示負面或 disagree，「</span><span style=""font-weight: 400;"">J」</span><span style=""font-weight: 400;"">象徵正面的態度，「！」來表示強調，「！！」表示 lecturer 一再強調或換句話說的重點，特別重要就可以把那區塊圈起來。</span></p>
<p><span style=""font-weight: 400;"">縮寫的例子像是 <em>people = ppl，government = gvnt，organization = org，with = w/，culture = cul</em> 等，在練習 TPO 時如果有特別常出現的單字，就可以自己把那些單字寫成縮寫，寫久了就會習慣成自然，等到考試時就不會看不懂自己在寫什麼。</span></p>
<p><span style=""font-weight: 400;"">做筆記的技巧並不是渾然天成，而是需要經驗去累積、磨練，寫完 TPO 對完答案時，建議可以在重新把聽力聽一遍，檢討剛剛題目的考點是否都有在筆記裡面，托福聽力的考題邏輯其實多練習是蠻容易捉摸的，只是切記，千萬不要有「等考試那天再做筆記」的念頭就好。如果發現練習時某些主題的題目錯的特別多，就要先針對不熟悉的科目背單字、增加相關背景知識後再去作題，不然連文章大意都聽不懂，硬抄筆記也是很難作答的。</span></p>
<p><span style=""font-weight: 400;"">總而言之，一定要了解自己的程度落在哪裡，再去對症下藥，才會有效的進步！</span>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '托福聽力怎麼準備？要做筆記嗎？聽力小秘訣大公開！','og_description' => '到底該如何準備托福的聽力呢？對於很多人來說，托福聽力考試是相當難以準備的，而除了硬底子的乖乖準備考試之外，不可忽略的部分當然還包括了最重要的考試技巧-『做筆記』，那惡這筆記到底該怎麼做呢？','meta_title' => '托福聽力準備？考試技巧、聽力題型、計分方式、托福聽力筆記方法公開！','meta_description' => '到底該如何準備托福的聽力呢？對於很多人來說，托福聽力考試是相當難以準備的，而除了硬底子的乖乖準備考試之外，不可忽略的部分當然還包括了最重要的考試技巧-『做筆記』，那惡這筆記到底該怎麼做呢？','canonical_url' => 'https://tw.english.agency/tofel-listening','feature_image_alt' => '托福聽力考試怎麼準備','feature_image_title' => '托福聽力考試怎麼準備','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a464a69d42b6-jTd1hhRMiO01tkIz7cu2AQZanNqdLL-1268x711.jpeg','author_id' => '13','category_id' => '15','created_at' => '"2017-12-29 11:01:42"'],
  ['id' => '96','title' => '"迪士尼收購21世紀福斯 7個相關多益字"','slug' => '迪士尼收購21世紀福斯-7個相關多益字','excerpt' => '"迪士尼（Walt Disney）以524億美元買下美國影視龍頭21世紀福斯（21st Century Fox）。國際職場上時常出現購併案，也是商用英文有可能會使用到的哦！"','content' => '"<p>事業版圖橫跨電影、娛樂產業的迪士尼（Walt Disney），日前以524億美元（約1.57兆台幣）買下美國影視龍頭21世紀福斯（21st Century Fox），可說是拿下全球最大娛樂企業的寶座。</p>
<p>基於公司考量，國際職場上時常出現購併案，且金額之高總能讓外界瞪大眼睛。如果我們能與外國客戶聊一聊這些超大型購併案，即所謂的megadeal，肯定能增加你的職場專業度，以及提升多益答題能力。以下的相關詞彙，你不能不會：</p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #a42d00;"">A.merger and acquisition（M&amp;A）合併與收購</span></strong></h2>
<p><br />職場上，公司間「合併」的動作是merge，讀作[mɝdʒ]，而它的名詞是merger。大公司「收購」小公司是acquisition，讀作[͵&aelig;kwəˋzɪʃən]，這個字來自acquire（獲得）。</p>
<p>合併與收購常相偎相依，所以職場上常以merger and acquisition來討論企業間的合併與收購，簡稱M&amp;A。而大企業之間數十億乃至數百億元美金的合併，又稱為megamerger，「mega-」是「大」的意思。兩家大企業合併時，高階經理人可以拿到天價的離職金，但是部屬可就要擔心合併後人員的異動─遭裁員的可能。</p>
<p>例句：</p>
<p><br /><em>All the new acquisitions will be made with the sanction of board of directors.</em><br /><em>（所有新的收購案都要董事會的批准，才能進行。）</em></p>
<p><em>A merger negotiation between the two companies has reached its final stage.</em><br /><em>（兩家公司的合併談已到了最後階段。）</em></p>
<p><em>The acquisition frenzy has coincided with an era of historically low interest rates.</em><br /><em>（這股收購熱潮，正好遇到史上低利率時代。）</em></p>
<p><em>Corporation&rsquo;s executives could benefit greatly from the megamerger; the employees，however，worried about possible job losses.</em><br /><em>（公司的高階經理人可能從超大合併案中大大地得利，然而員工卻擔心工作會不保。）</em></p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #a42d00;"">B.takeover／hostile takeover收購</span></strong></h2>
<p><br />要看購併的新聞，一定要知道takeover。它是從動詞片語take over而來，字面上是「接管、接收」，但是takeover在購併的議題上指的是「收購」。</p>
<p>除了takeover，不妨學「惡意收購」的英文說法hostile takeover，意指買方公司未經對方公司董事會允許，不管對方公司是否同意，逕行收購股權，成為大股東。</p>
<p>例句：<br />X Inc. and B Company&rsquo;s 100 billion dollar deal ranks among the six largest takeovers.<br />（X和B公司的100億美金收購案是史上第六大的收購交易。）</p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #a42d00;"">C.tender offer買斷合併</span></strong></h2>
<p><br />tender offer是買方在股票市場上公開收購對方公司的股票，比較正式的中文名稱是「買斷合併」。tender在此是取它「投標、（正式）提出」的動詞字義。</p>
<p>例句：<br />Williams said that AW&rsquo;s tender offer was paving the way for a hostile takeover in the future.<br />（威廉斯公司表示，AW的買斷合併是為了未來的惡意購併鋪路。）</p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #a42d00;"">D.tie-up合作、聯合</span></strong></h2>
<p><br />在英文裡，tie是繫、拴、捆，或是打結。所以，當兩家公司「繫在一起」tie-up就是指兩家公司的「合作、聯合」。有趣的是，在美式用法裡，tie-up又指交通阻塞，這也很容易記，因為tie是「打結」，那就是我們所說的「交通大打結」！</p>
<p>例句：<br />The tie-up between X Inc. and B Company hit today&rsquo;s headlines.<br />（X公司公開收購B公司的股票的消息登上今天的新聞頭條。）</p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #a42d00;"">E.synergy綜效</span></strong></h2>
<p><br />公司的股東在面對管理階層進行購併時，多半考慮一件事：兩家公司合併之後，能否產生1加1大於2的結果。若兩個公司結合在一起，所創造出來的整體價值，大於結合前個別價值，這就是「綜效」，英文是synergy。</p>
<p>例句：<br />The shareholders hope their companies&rsquo; merger will create greater synergy.<br />（股東們盼公司的合併案能產生1加1大於2的綜效。）</p>
<p>&nbsp;</p>
<p>看懂了上述「購併」相關字彙，那以下這兩題多益題目就難不倒你了！</p>
<h2><span style=""color: #0000ff;"">多益練習題</span></h2>
<p>1. ______ the advice of the board of directors，Mr. Longman did not vote to support the merger.<br />(A) Amid<br />(B) Against<br />(C) Besides<br />(D) Except</p>
<p>2. No new investments or acquisitions will be made without the ______ of Kolsen Records&rsquo; board of directors.<br />(A) event<br />(B) sanction<br />(C) adoption<br />(D) convenience</p>
<p>解析：<br />1. 正確答案(B)。題意為「Longman先生反對董事會的建議，並沒有投票支持購併案。」，以選項(B)against（反對、違背&hellip;）最符合題意。選項(A)在&hellip;之中、(C) 此外、(D)除了&hellip;之外（不包括），皆與題意不符，故不選。</p>
<p>2. 正確答案(B)。請注意本題有no&hellip;without&hellip;的「雙重否定」句型，題意為「要是沒有Kolsen公司的董事會批准，將不會進行任何新投資或收購案。」選項(B)sanction在新聞英語中的常見字義是「制裁」，例如經濟制裁是economic sanctions，但是sanction還有「批准」的意思，本題即為此義。選項(A)活動、(C) 領養、(D)方便，皆與題意不符，故不選。</p>
<p>文／周強</p>
<p>&nbsp;</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '"迪士尼收購21世紀福斯 7個相關多益字"','og_description' => '"迪士尼（Walt Disney）以524億美元買下美國影視龍頭21世紀福斯（21st Century Fox）。國際職場上時常出現購併案，也是商用英文有可能會使用到的哦！"','meta_title' => '"迪士尼收購21世紀福斯 7個相關多益字"','meta_description' => '"迪士尼（Walt Disney）以524億美元買下美國影視龍頭21世紀福斯（21st Century Fox）。國際職場上時常出現購併案，也是商用英文有可能會使用到的哦！"','canonical_url' => 'http://www.toeicok.com.tw/%E7%9C%8B%E8%BF%AA%E5%A3%AB%E5%B0%BC%E6%94%B6%E8%B3%BC21%E4%B8%96%E7%B4%80%E7%A6%8F%E6%96%AF-%E5%BF%85%E5%AD%B8%E7%9A%847%E5%80%8B%E5%A4%9A%E7%9B%8A%E5%AD%97/','feature_image_alt' => '迪士尼','feature_image_title' => '迪士尼','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4cb5d42f831-6lXnKVh1k2zB9NmkGIhzwbfzKeGEP3-958x638.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2018-01-03 07:46:32"'],
  ['id' => '97','title' => '"2017《Time》年度風雲人物 ！跟著「打破沉默者」學單字"','slug' => '2017Time年度風雲人物-跟著打破沉默者學單字','excerpt' => '《Time》時代雜誌2017年度的風雲人物，這次是特別的一群人，你知道他們的故事嗎？透過他們來看看多益單字吧！','content' => '"<p>《Time》時代雜誌2017年度的風雲人物不是政治領袖或是商業大亨，而是「打破沉默者」（The Silence Breakers），她們揭露了許多的性騷擾與性侵事件，在美國引起一股社會運動。</p>
<p>其實先前在推特上，不少使用者以#MeToo表達曾遭遇性侵或性騷擾，勇敢站出來說出自己不為人知的故事。為了向The Silence Breakers致敬，《Time》決定將這群勇敢發聲的男女做為年度風雲人物，今天就讓我們從這國際大事學習多益單字吧！</p>
<p><strong><span style=""color: #a42d00;"">1. 害怕with the fear of</span></strong><br />They&rsquo;ve had it with the fear of retaliation，of being blackballed，of being fired from a job they can&rsquo;t afford to lose.<br />（他們長期害怕被報復、被反對、或是被裁員，而這個工作卻是他們怎麼也不能失去的。）</p>
<p>這句話道出了受害者的難處，由於害怕，導致很多人無法為自己發聲。說到害怕，通常我們會直覺想到be afraid of，不過，在此例句中Time用了一個介系詞片語with the fear of+Ving/N.（害怕～）。</p>
<p>fear也用作動詞，因此也可以寫成「they fear that~」。但是注意that後面需加上子句，要有完整的句子結構，因此上述句子可以改成they fear that they will be retaliated（句子後段的retaliation改成動詞用法，才能符合英文基礎文法概念）。</p>
<p><strong><span style=""color: #a42d00;"">2. 反對blackball、辭退fire</span></strong><br />請注意，在例句中being blackballed（反對）和being fired（辭退）使用被動態是一大文法重點。在語意上，這群受害者是反對和辭退動作的接受者，因此採用被動語態（be + p.p），只是在介系詞的用法原則下，例句中的be動詞變成了being。</p>
<p><strong><span style=""color: #a42d00;"">3. 承擔afford</span></strong><br />They&rsquo;ve &hellip;. being fired from a job they can&rsquo;t afford to lose.<br />這邊還有個多益常見動詞afford，意思是負擔的起（金錢方面），例句：I can&rsquo;t afford the car.（我無法負擔這輛車。）但afford也可以用在我們無法承擔失去某樣事情的後果，就像上述句子中受害者需要這份工作，而無法承擔失去工作的後果。</p>
<p>例句：They can&rsquo;t afford to spend money on other luxuries.<br />（他們實在無法再把錢花在其他的奢侈品上了。）</p>
<p>※補充單字：afford的形容詞變化為affordable；名詞變化則是affordability。<br />retaliation 報復</p>
<p><strong><span style=""color: #a42d00;"">4.控訴（accused）</span></strong><br />《Time》也提到：<br />But she cannot afford to leave the job and says she must force herself out of bed every day to face the man she&rsquo;s accused.<br />（但是這位女性不能離開她的工作，她每天必須強迫自己下床來面對她所控訴（性騷）的男人。）</p>
<p>Accuse這個動詞指的是控訴。控訴某人做了件不好的事情我們會用accuse sb of something，介系詞of後要接一件事情（名詞）。這邊我們來看看兩個例句：</p>
<p>例句1: They accuse him of inappropriate behavior. （他們控訴他不當的行為。）<br />例句2: The boss accused the employee of stealing. （老闆指控員工偷東西。）</p>
<p>第1句因為behavior本身就是名詞，所以無需任何變化；但第2句裡的steal（偷竊）沒有名詞，因此要用動名詞stealing。通常動名詞更能表達一個動詞的動作，所以臨場感更強。</p>
<p>Accusation則是動詞accuse的抽象名詞變化，例句：Their accusation has been denied by the man. （那個男人否認了他們的指控。）而他的另外一個具體名詞變化則是accuser，「-er」指的是人的名詞，因此就是控訴者之意。</p>
<h2><span style=""color: #0000ff;"">多益模擬試題</span></h2>
<p>1. I would appreciate if you could please refund _______ is left on my payment，if possible.<br />(A)in which<br />(B) that<br />(C) which<br />(D) whatever</p>
<p>2. I would like you to give me a written _______ about my cancellation of your Woman&rsquo;s Secret magazine.<br />(A) confirmation<br />(B) accusation<br />(C) appointment<br />(D) claim</p>
<p>解析：<br />1. 正確答案(D)。本題要選一個可以做為名詞的代名詞，而選項(D)whatever可以用來替代「所有剩下的款項」，作為動詞refund的受詞，故(D)為正解。題意為「如果可以的話，你能退還所有剩下的款項，我會很感謝的。」</p>
<p>2. 正確答案(A)。本題為單字題，題意為 「我希望你能給我一份書寫的證明，來表明我取消Woman&rsquo;s Secret雜誌訂閱。」選項(A)confirmation是「確認」的意思，在此作為「證明、確認（書）」之意，表示顧客已經取消訂閱。選項(B)指控、(C)約（會）、(D)聲明，皆不符合題意，故不選。</p>
<p>文／Buffy Kao</p>
<p>&nbsp;</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '"2017《Time》年度風雲人物 ！跟著「打破沉默者」學單字"','og_description' => '《Time》時代雜誌2017年度的風雲人物，這次是特別的一群人，你知道他們的故事嗎？透過他們來看看多益單字吧！','meta_title' => '"2017《Time》年度風雲人物 ！跟著「打破沉默者」學單字"','meta_description' => '《Time》時代雜誌2017年度的風雲人物，這次是特別的一群人，你知道他們的故事嗎？透過他們來看看多益單字吧！','canonical_url' => 'http://www.toeicok.com.tw/metoo-%E5%9C%A8%E5%B9%B4%E5%BA%A6%E9%A2%A8%E9%9B%B2%E4%BA%BA%E7%89%A9%E8%BA%AB%E4%B8%8A%E5%BF%85%E5%AD%B8%E7%9A%845%E5%80%8B%E8%8B%B1%E6%96%87%E9%97%9C%E9%8D%B5%E5%AD%97/','feature_image_alt' => 'TIME','feature_image_title' => 'TIME','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4ca1d6737ff-AAKNOIslbdboziO7JvHEzdzCscTKv8-640x427.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2018-01-03 09:27:15"'],
  ['id' => '98','title' => '2017菲律賓遊學費用資訊總整理，菲律賓語言學校推薦、代辦最佳挑選辦法告訴你！','slug' => 'philippines-study-aboard','excerpt' => '談到菲律賓遊學，很多不經都會問到到底為什麼要大老遠跑到菲律賓去學英文呢？到底菲律賓語言學校有什麼特別的地方呢？菲律賓遊學代辦該怎麼選，今天小編就帶大家來認識一下菲律賓吧！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p style=""text-align: left;"">談到菲律賓，你可能會想到好玩的<a href=""https://phfuntour.tw"">宿霧自由行行程</a>&nbsp;，但你聽過菲律賓遊學嗎？很多不禁都會問到底為什麼要大老遠跑到菲律賓去學英文呢？到底<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>有什麼特別的地方呢？菲律賓遊學代辦該怎麼選，今天小編就帶大家來認識一下菲律賓吧！</p>
<p style=""text-align: left;"">&nbsp;</p>
<div style=""text-align: left;"">&nbsp;<img title=""菲律賓學英文"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4c8f8007c57-RlLrgSLklMmoQX12NaoXltwyRNZIWW-1020x677.png"" alt=""菲律賓學英文"" width=""100%"" height=""auto"" /></div>
<h2 style=""text-align: left;"">為什麼要去菲律賓學英文？</h2>
<div style=""text-align: left;"">菲律賓遊學近年來受到日韓學生的青睞，除了因為距離近、便宜外還有以下幾點主要的原因：</div>
<p>&nbsp;</p>
<p style=""text-align: left; color: #ff6666;""><strong>全英文嚴格環境：</strong></p>
<p>強調透過全英語的環境訓練學生，讓學生在這樣的環境下培養出以英語的思考邏輯，多數學校都採取這樣的策略，強調English Only，在學校環境只准說英語的策略！</p>
<p>&nbsp;</p>
<p style=""text-align: left; color: #ff6666;""><strong>學校設有官方考場：</strong></p>
<p>為了讓學生不對考試環境感到陌生、同時上學生能夠在快速實踐目標，多半學校都會附設各類考場，如雅思(IELTS)、托福（TOFEL）、多益(TOEIC)，讓學生在完成課程直接在最熟悉的環境下應考！讓學習事半功倍！</p>
<p>&nbsp;</p>
<p style=""text-align: left; color: #ff6666;""><strong>一對一課程，客製化的學習：</strong></p>
<p>菲律賓語言學校最特別的地方就是一對一的教學，透過一對一的教學，適性發展，打造適合的課程，透過這樣的訓練方式針對文法、口說、寫作等等部分進行專業加強，同時學生也可以跟老師做協商，打造適合的課程。</p>
<p>&nbsp;</p>
<p style=""text-align: left; color: #ff6666;""><strong>英語會話訓練至少達20小時/一週:</strong></p>
<p>多數台灣學生學習英文的最大問題，莫過於死記文法、狂背單字，而沒有實際應用上，而多數語言學校透過口語課程，讓學生的以實際應用上這些單字、文法，加強學生的學習效果，而其中密集式英文學習就是一個代表。</p>
<p><span data-sheets-value=""{"" data-sheets-userformat=""{"">推薦閱讀：</span></p>
<p><a href=""https://tw.english.agency/philippines-study-aboard-agent"">如何挑選菲律賓遊學代辦？5件事你要知道&nbsp;</a><img title=""菲律賓遊學對象"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4c8f83c3f71-f978Q82jTmfU5eoBb09yRqzzWcU1a8-1024x669.png"" alt=""菲律賓遊學對象"" width=""100%"" height=""auto"" /></p>
<h2>菲律賓學英文適合誰？</h2>
<p style=""text-align: left; color: #ff6666;""><strong>國中生：</strong></p>
<p>不同於仿間的補習班，菲律賓語言學校提供了許多不一樣的課程、學習環境，強調互動式的學習，例如斯巴達課程、半斯巴達課程、又或是頭過度假結合學習的型態，讓學生可以適性發展，培養好的英語能力。&nbsp;</p>
<p style=""text-align: left; color: #ff6666;""><strong>社會新鮮人：</strong></p>
<p>菲律賓語言學校不只單純培養學生的英文技能，同時也針對托福、雅思、多益等等開設專門課程，如TOEIC保證班、IELTS保證班、TOEFL保證班，以協助學生達標為主要目標！&nbsp;</p>
<p style=""text-align: left; color: #ff6666;""><strong>轉職進修：</strong></p>
<p>針對商用職場、英文證照考試相關的專業訓練課程，這類高強度的英文學習，協助商用人士快速掌握商用英文重點。</p>
<p style=""text-align: left; color: #ff6666;""><strong>親子遊學：</strong></p>
<p>打造適合親子一同學習的環境，如透過互動式的有趣活動、戶外探索等等方式讓家長跟小孩一起快樂學英文，同時也讓各國的家長、小孩彼此交流，而其中長灘島的paradise English就是相當有名的一個。</p>
<p>&nbsp;<img title=""菲律賓遊學地區"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4c8f583f1a0-zTEXTfnTAcTLWXZI74ydOtWLN9A5Oi-1023x622.jpeg"" alt=""菲律賓遊學地區"" width=""100%"" height=""auto"" /></p>
<h2>有哪些菲律賓遊學推薦地區？</h2>
<p>菲律賓遊學通常常見地區大致上分為以下幾點：</p>
<p>&nbsp;&nbsp;</p>
<h4 style=""color: #ff8800;""><strong>宿霧遊學(cebu):</strong></h4>
<p>菲律賓<a href=""https://zh.wikipedia.org/zh-tw/%E5%AE%BF%E9%9C%A7%E5%B8%82"">宿霧</a>不單單只是語言學校的集中地，更是許多人的到菲律賓旅遊的首選，而在宿霧的語言學校有各式各樣不同的特色，如強調軍事化管理的斯巴達體系語言學校，像是EV語言學校、SME語言學校，或是半斯巴達體系的語言學校，讓學生可以自由學習，如SME classic、SME capital等等語言學校，當然還有以度假設備為最大特色的語言學校，像是CPI語言學校、Fella語言學校、Howdy等等。</p>
<p>另外有不少學校都是外資的，如韓國、日本、美國等等，而隨著外資企業的狀況不同，在學校的設備與管理上也會有所不同喔！</p>
<p><a href=""https://tw.english.agency/cebu-esl-school"">延伸閱讀：宿霧遊學推薦、心得、評價說明，你不可不知的宿霧語言學校特色大公開!</a></p>
<p>&nbsp;</p>
<p><strong>克拉克遊學(clark):</strong>&nbsp;</p>
<p>菲律賓的<a href=""https://zh.wikipedia.org/wiki/%E5%85%8B%E6%8B%89%E5%85%8B"">克拉克</a>前身是美軍基地，因此在這裡可以常常看到一些歐美人士，但也因為如此在這裡部分學校會配有一定數量的外籍教師，強調正確、有效的口說教學喔！ 這裡知名的學校有Help語言學校克拉克校區、CIP語言學校等等。</p>
<p>延伸閱讀：<a href=""https://tw.english.agency/study-aboard-philippine-clark"">克拉克遊學值得你去嗎？三個你該去克拉克遊學的理由！</a></p>
<p>&nbsp;</p>
<h4 style=""color: #ff8800;""><strong>碧瑤遊學(Baugio):</strong></h4>
<p><a href=""https://zh.wikipedia.org/zh-tw/%E7%A2%A7%E7%91%B6%E5%B8%82"">碧瑤</a>可以說是與宿霧有很大的不同，這裡可以說是世外桃園，這裡早期受到美軍開發，因此有相當不錯的硬體設備與基礎建設，同時英語已經成為了當地重要的生活語言，此外Baugio同時也是菲律賓重要的大學城。</p>
<p>延伸閱讀：<a href=""https://tw.english.agency/baguio-study-abroad"">碧瑤遊學Baguio：加強英文，海外遊學｜菲律賓語言學校・遊學推薦</a></p>
<p>&nbsp;</p>
<h4 style=""color: #ff8800;""><strong>怡朗遊學(Iloilo):</strong></h4>
<p><a href=""https://zh.wikipedia.org/wiki/%E4%BC%8A%E6%B4%9B%E4%BC%8A%E6%B4%9B%E5%B8%82"">怡朗</a>是個景色優美、風景怡人，在這邊的語言學校林立，CP值高，也是菲律賓遊學的好選擇。</p>
<p>&nbsp;</p>
<h4 style=""color: #ff8800;""><strong>蘇比克遊學（Subic）:</strong></h4>
<p><a href=""http://www.itsmorefuninthephilippines.com.tw/spots/Subic"">蘇比克</a>為前美海軍基地，當地有眾多美籍軍人，因此成為了當地學校招攬的對象 ，因此學校常常有很多的外師，如果想要培養好的口說能力，可以考慮這裡！</p>
<p>(如果想要深入認識菲律賓，你可以到<a href=""http://www.itsmorefuninthephilippines.com.tw/"">菲律賓觀光局</a>的網站看看喔！)</p>
<p><a href=""https://tw.english.agency/philippines-study-aboard-city"">延伸閱讀：如何選擇菲律賓遊學城市？</a></p>
<p><img title=""菲律賓語言學校課程"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4c91e390e5f-IP1TguyArQJ5d16rk0AJvjHcnNA8u7-1024x768.jpeg"" alt=""菲律賓語言學校課程"" width=""100%"" height=""auto"" /></p>
<h2>菲律賓語言學校課程與校園說明：</h2>
<p>菲律賓的語言學校課程就如同前面所說的共分為3種，斯巴達、半斯巴達、以及非斯巴達，而這三種課程分別有以下幾點特性：</p>
<p>&nbsp;</p>
<h4 style=""color: #ff8800;""><strong>斯巴達課程</strong>：</h4>
強調軍事化的管理課程，透過一週40-50小時的課程與自主訓練，快速提高學生的英文能力。
<h4 style=""color: #ff8800;""><strong>半斯巴達課程</strong>：</h4>
一週大概20小時的訓練課程，讓學生有較多的自修時間。
<h4 style=""color: #ff8800;""><strong>非斯巴達課程</strong>：</h4>
彈性化的課程安排，強度較低，但強調學生自主學習，例如日商經營相當出色的<a href=""https://tw.howdyenglish.asia/"">Howdy語言學校</a>。
<p><a href=""https://tw.english.agency/philippines-esl-academy-recommend"">延伸閱讀：如何選擇菲律賓語言學校課程？&nbsp;</a></p>
<p>&nbsp;</p>
<h2>菲律賓遊學費用大概多少？</h2>
<p>去菲律賓遊學，每間語言學校的費用都不大固定，但整體來說可以將支出區分成以下幾點：</p>
<ul>
<li class=""p1""><em><span class=""s1"">學費</span></em></li>
<li class=""p1""><em><span class=""s1"">住宿費</span><span class=""s2"">/</span><span class=""s1"">入學費</span></em></li>
<li class=""p1""><em><span class=""s1"">生活費（餐費）</span></em></li>
<li class=""p1""><em><span class=""s1"">洗衣費</span><span class=""s2"">/</span><span class=""s1"">清潔費</span></em></li>
<li class=""p1""><em><span class=""s1"">菲律賓遊學費用</span></em></li>
<li class=""p1""><em><span class=""s1"">證件費用（</span><span class=""s2"">ssp</span><span class=""s1"">，</span><span class=""s2""> ACR ICard</span><span class=""s1"">，</span><span class=""s2""> ECC，</span><span class=""s1"">觀光證延長簽證費）</span></em></li>
<li class=""p1""><em><span class=""s1"">語言學校生活管理費</span></em></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;而學費部分多半是以週數計費喔！詳細要依照學校的價目來看!</p>
<p>&nbsp;</p>
<h2><strong>特色學校分類：</strong></h2>
<h4 class=""margin-bottom-1em"">熱門語言學校</h4>
<ul>
<li class=""color-goedu margin-bottom-1em""><a href=""https://helpenglish.com.tw/"">HELP 語言學校</a>：人稱斯巴達語言學校的始祖，是許多人學習的好選擇。</li>
<li class=""color-goedu margin-bottom-1em"">C2 UBEC 語言學校：菲律賓宿霧地區唯一的一間日資管理的語言學校。</li>
<li class=""color-goedu margin-bottom-1em"">CPI 語言學校：採取飯店、度假村形式的管理方式，讓學生邊放鬆邊學習。</li>
</ul>
<p>&nbsp;</p>
<h4 class=""margin-bottom-1em""><strong>英語檢定推薦語言學校</strong></h4>
<ul>
<li><a href=""https://smeag.english.agency/"">SME 語言學校</a>：以檢定考試為導向，依照校區分為托福/雅思/多益三類學習考場。</li>
<li>HELP 語言學校：分為三類校區，也依照三種檢定考試區分。</li>
<li>FELLA 語言學校：斯巴達、半斯巴達兼具的學習環境，結合度假村的學習方式。</li>
</ul>
<p>&nbsp;</p>
<h4 class=""margin-bottom-1em"">口碑認證語言學校</h4>
<ul>
<li class=""color-goedu margin-bottom-1em"">CG 語言學校：度假式學習、結合高強度斯巴達，學校有南洋風氣。</li>
<li class=""color-goedu margin-bottom-1em""><a href=""https://tw.evenglish.asia/"">EV 語言學校</a>：2017重新裝修，以TOEIC為經營重點。</li>
<li class=""color-goedu margin-bottom-1em"">Philinter 語言學校：以學生學習為核心經營管理。</li>
</ul>
<h2>&nbsp;</h2>
<h2><strong>學校常見的住宿情況為何？</strong></h2>
<table dir=""ltr"" border=""1"" cellspacing=""0"" cellpadding=""0""><colgroup> <col width=""162"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /> <col width=""100"" /></colgroup>
<tbody>
<tr>
<td>&nbsp;</td>
<td colspan=""4"" rowspan=""1"" data-sheets-value=""{"">宿舍房型</td>
<td colspan=""3"" rowspan=""1"" data-sheets-value=""{"">其他設備</td>
</tr>
<tr>
<td>&nbsp;</td>
<td data-sheets-value=""{"">單人</td>
<td data-sheets-value=""{"">雙人</td>
<td data-sheets-value=""{"">三人</td>
<td data-sheets-value=""{"">四人</td>
<td data-sheets-value=""{"">
<p>個人衛浴</p>
</td>
<td data-sheets-value=""{"">曬衣空間</td>
<td data-sheets-value=""{"">洗衣機</td>
</tr>
<tr>
<td data-sheets-value=""{"">help語言學校longlong校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">help語言學校martin校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">help語言學校clark校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">sme語言學校斯巴達校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">sme語言學校classic校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">sme語言學校captial校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">howdy語言學校</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">cpi語言學校</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">cg語言學校</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">fella語言學校-第一校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
</tr>
<tr>
<td data-sheets-value=""{"">fella語言學校-第二校區</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
</tr>
<tr>
<td data-sheets-value=""{"">C2語言學校</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">FirstEnglish語言學校</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td data-sheets-value=""{"">cip語言學校</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td>&nbsp;</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
<td data-sheets-value=""{"">V</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2><strong>該如何找到適當的菲律賓遊學代辦？</strong></h2>
<p>挑選代辦是一件相當不容易的事情！對於每個想要出去唸書的朋友來說，都是個大問題，那到底該怎麼挑選呢？請先記住以下幾個原則！</p>
<p><strong>1.挑選菲律賓遊學代辦時，續注意該代辦的中心的顧問是不是真實了解該所語言學校！</strong></p>
<p><strong>2.遊學顧問應該要能跟你說清楚，到底哪些該注意！而你也應該清楚知道哪些課題你該先做功課，才不會被牽著鼻子走！大致上如以下幾點：</strong></p>
<ul>
<li>課程時數</li>
<li>學校報價</li>
<li>語言學校校園總攬</li>
<li>住宿說明</li>
<li>簽證費用與相關法條</li>
<li>機票處理與費用</li>
<li>醫療保健</li>
<li>師資說明</li>
<li>生活狀況與安排</li>
<li>當地文化</li>
<li>校規、合約事宜</li>
<li>保險理賠、相關事宜</li>
</ul>
<p><strong>3.該遊學代辦中心是否偶提供售後服務，確保每位學生權益！</strong></p>
<p><strong>4.課程說明是否有先尊重您的需求？</strong></p>
<p><strong>5.顧問所說的菲律賓遊學心得分享、經驗談不一定為真，你是否有先去做查證？</strong></p>
<p>&nbsp;</p>
<h2>所以菲律賓遊學有缺點嗎？</h2>
<p>每種學習方式都有各自的優點與缺點，菲律賓遊學缺點與常見的問題主要會發生如下：</p>
<ul>
<li>課本教材對於學生來說有難度差異，通常在入學後一週內會察覺。</li>
<li>對於現在課程不滿意，想要更換課程。</li>
<li>學校著色設施有問題，宿舍需要整修。</li>
<li>重要物品遺失，但由於人口複雜，通常難以尋獲。</li>
<li>當地水土不服，過於炎熱導致不適。</li>
<li>學校餐點與當地的料理方式並不合胃口。</li>
<li>學校的課程時數過長，學生缺乏自我學習的時間！</li>
</ul>
<p>以上都是常見的學習問題，但其實都是可以透過與校方的經理溝通來解決的喔！所以有問題務必要與駐校的經理討論並溝通，不然時間過去了吃虧的還是自己。</p>
<p><em>延伸閱讀</em></p>
<ul>
<li><em><a href=""https://tw.english.agency/study-aboard-prepare"">菲律賓遊學行李、行前準備指南</a></em></li>
<li><em><a href=""https://tw.english.agency/philippines-visa-application"">菲律賓遊學簽證說明指南</a></em></li>
<li><em><a href=""https://tw.english.agency/study-aboard-philippine-q&amp;a"">菲律賓遊學常見問題總指南</a></em></li>
<li><em><a href=""https://tw.english.agency/seven-question-of-philippines"">菲律賓遊學必知的7個文化</a></em></li>
</ul>
<p>&nbsp;</p>
<p>最後不論要去找哪些代辦，如GoEducation、ugophilippines、crazyaboard等等大品牌，最重要的是自己要先準備好功課～不然顧問也沒辦法好好幫你諮詢喔！</p>
<iframe style=""width: 100%; border: none;"" data-binding=""form_system"" data-form=""87""></iframe></div>"','published' => '1','og_title' => '菲律賓遊學推薦、費用很複雜？詳細解說菲律賓語言學校環境與課程！','og_description' => '談到菲律賓遊學，很多不經都會問到到底為什麼要大老遠跑到菲律賓去學英文呢？到底菲律賓語言學校有什麼特別的地方呢？菲律賓遊學代辦該怎麼選，今天小編就帶大家來認識一下菲律賓吧！','meta_title' => '菲律賓遊學，為何值得你考量？詳細解說菲律賓語言學校費用與代辦挑選！','meta_description' => '談到菲律賓遊學，很多不經都會問到到底為什麼要大老遠跑到菲律賓去學英文呢？到底菲律賓語言學校有什麼特別的地方呢？菲律賓遊學代辦該怎麼選，今天小編就帶大家來認識一下菲律賓吧！','canonical_url' => 'https://tw.english.agency/philippines-study-aboard','feature_image_alt' => '菲律賓遊學','feature_image_title' => '菲律賓遊學','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a4ca768df55f-BzXqqFbY0bxh9paSqCX2lHMhm9tJFu-1200x675.png','author_id' => '1','category_id' => '1','created_at' => '"2018-01-03 09:51:08"'],
  ['id' => '99','title' => '十二星座英文解析，教你怎麼用英文介紹自己星座！-English.agency','slug' => 'constellation-sign','excerpt' => '生活中很常聊到「星座」這個話題，外國人也一樣迷信星座。今天就來教大家該怎麼用英文聊自己的星座、個性，開啟社交新話題吧！帶你深入各個星座的英文怎麼說，','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>&nbsp;</p>
<h2><strong><span style=""color: #0088a8;"">Zodiac Sign 十二星座</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>12星座的英文對於許多人來說是一大學問，今天就來看看這些12星座的英文吧！</strong></span></p>
<ul>
<li><em>Aries 牡羊座</em></li>
<li><em>Taurus 金牛座</em></li>
<li><em>Gemini 雙子座</em></li>
<li><em>Cancer 巨蟹座(Cancer，小寫cancer為癌症的意思，C大寫則是指巨蟹座)</em></li>
<li><em>Leo 獅子座</em></li>
<li><em>Virgo 處女座</em></li>
<li><em>Libra 天枰座</em></li>
<li><em>Scorpio 天蠍座</em></li>
<li><em>Sagittarius射手座</em></li>
<li><em>Capricorn 摩羯座</em></li>
<li><em>Aquarius 水瓶座</em></li>
<li><em>Pisces 雙魚座</em></li>
</ul>
<p>&nbsp;</p>
<p style=""color: ##ee7700;"">上述所提到的星座英文全名是拉丁文，每個星座也有比較口語的對應英文名稱說法，分別是：</p>
<p>&nbsp;</p>
<ul>
<li><em>Ram (公羊) 牡羊座&nbsp;</em></li>
<li><em>Bull (公牛) 金牛座&nbsp;</em></li>
<li><em>Twins (雙胞胎) 雙子座</em></li>
<li><em>Crab (螃蟹) 巨蟹座</em></li>
<li><em>Lion (獅子) 獅子座</em></li>
<li><em>Virgin (處女) 處女座</em></li>
<li><em>Scales/Balance (秤/平衡) 天枰座</em></li>
<li><em>Scorpion (蠍子) 天蠍座</em></li>
<li><em>Archer (射箭手) 射手座</em></li>
<li><em>Goat/Sea Goat (山羊/海山羊) 摩羯座</em></li>
<li><em>Water Carrier (裝水的桶子) 水瓶座</em></li>
<li><em>Fishes (魚) 雙魚座（</em>星座有四個象限，分別為：Water<span style=""color: #009fcc;"">水象、</span>Earth<span style=""color: #009fcc;"">土象、</span>Air<span style=""color: #009fcc;"">風象、</span>Fire<span style=""color: #009fcc;"">火象）</span></li>
</ul>
<p>&nbsp;</p>
<p style=""color: #cc0000;""><strong>【補充】</strong></p>
<ul>
<li>「星座」的英文有很多種說法，<strong>Zodiac</strong>的是天文學名詞「黃道帶」，指的是在黃道上星座組成的環帶。其他說法還有：<strong>Constellation，Horoscopes，Star Signs，Western Zodiac</strong></li>
<li>星座運勢的英文就叫<strong>Horoscopes</strong>，每日的是<strong>Daily Horoscopes</strong>，每週的是 <strong>Weekly Horoscopes</strong>，每月的是 <strong>Monthly Horoscopes</strong>，每年的叫 <strong>Yearly Horoscopes</strong></li>
<li>「生肖」則可以說是：<strong>Chinese Zodiac，Twelve Zodiac Animals</strong></li>
</ul>
<p>&nbsp;</p>
<hr />
<h2><strong><span style=""color: #0088a8;"">Chinese Zodiac 生肖</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>中國文化裡面常說的『生肖』的英文可以簡單整理成以下這些：</strong></span></p>
<ul>
<li><em>Rat 鼠 (不能用mouse)</em></li>
<li><em>Ox 牛 (不能用 cow or bull)</em></li>
<li><em>Tiger 虎</em></li>
<li><em>Rabbit 兔</em></li>
<li><em>Dragon 龍</em></li>
<li><em>Snake 蛇</em></li>
<li><em>Horse 馬</em></li>
<li><em>Goat 羊 (不能用sheep)</em></li>
<li><em>Monkey 猴</em></li>
<li><em>Rooster 雞 (不能用chicken)</em></li>
<li><em>Dog 狗&nbsp;</em></li>
<li><em>Pig 豬</em></li>
</ul>
<p>&nbsp;</p>
<p><img title=""星座的人格特質Personality traits"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a5300305935a-HSP26jl1E4thfqL8aiAV7IquZrr9kQ-1038x584.png"" alt=""星座的人格特質Personality traits"" width=""100%"" height=""auto"" /></p>
<h2><strong><span style=""color: #0088a8;"">Personality traits 人格特徵</span></strong></h2>
<p><strong>每個星座也有對應的人格特徵，而究竟該用什麼英文來描述他們的人格特徵呢？</strong></p>
<div style=""color: #666666;"">
<h4 style=""color: #fa8072;""><strong>Aries(牡羊座):</strong></h4>
<ul>
<li>Adventurous 愛冒險的</li>
<li>Energetic 有活力的</li>
<li>Curious 好奇心旺盛的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Taurus(<em>金牛座</em>):</strong></h4>
<ul>
<li>Reliable 可靠的</li>
<li>Practical 實際的</li>
<li>Stable 穩定的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Gemini（雙子座）:</strong></h4>
<ul>
<li>Broad-minded 心胸寬廣的</li>
<li>Creative 創意的</li>
<li>Quick-witted 機智的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Cancer（巨蟹座）:</strong></h4>
<ul>
<li>Home-loving 愛家的</li>
<li>Loyal 忠誠的</li>
<li>Persuasive 有說服力的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Leo（獅子座）:</strong></h4>
<ul>
<li>Action-oriented 行動派的</li>
<li>Confident 自信的</li>
<li>Ambitious 野心勃勃的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Virgo（處女座）:</strong></h4>
<ul>
<li>Hardworking 勤奮的</li>
<li>Analytical 分析的</li>
<li>Kind 善良的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Libra（天秤座）:</strong></h4>
<ul>
<li>Easygoing 隨和的</li>
<li>Gracious 親切的</li>
<li>Social 善於社交的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Scorpio（天蠍座）:</strong></h4>
<ul>
<li>Mysterious 神秘的</li>
<li>Resourceful 足智多謀的</li>
<li>Brave 勇敢的&nbsp;</li>
</ul>
<h3>&nbsp;</h3>
<h4 style=""color: #fa8072;""><strong>Sagittarius（射手座）:</strong></h4>
<ul>
<li>Freedom-loving 愛好自由的</li>
<li>Optimistic 樂觀的</li>
<li>Jovial 快活的</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Capricorn（摩羯座）:</strong></h4>
<ul>
<li>Persistent 堅持不懈的</li>
<li>Self-disciplined 自律的</li>
<li>Calm 冷靜的</li>
</ul>
<p>&nbsp;</p>
<h3 style=""color: #fa8072;""><strong>Aquarius（水瓶座）:</strong></h3>
<ul>
<li>Friendly 友善的</li>
<li>Independent 獨立的</li>
<li>Humanitarian 人道主義</li>
</ul>
<p>&nbsp;</p>
<h3 style=""color: #fa8072;""><strong>Pisces（雙魚座）:</strong></h3>
<ul>
<li>Romantic 浪漫的</li>
<li>Compassionate 有同情心的</li>
<li>Sympathetic 懂得包容</li>
</ul>
</div>
<p>&nbsp;</p>
<hr />
<h2><strong><span style=""color: #0088a8;"">Dialog 對話</span></strong></h2>
<p><strong><span style=""color: #ee7700;"">最後看完了那們堆的實際案例，現在就來看看實際應用的部分吧！</span></strong></p>
<p>&nbsp;<strong>詢問星座詢問星座</strong></p>
<p><em><strong>1.What is your star sign? 你的星座是什麼</strong></em></p>
<ul>
<li>I am a Taurus. 我是金牛座</li>
<li>I am an Aquarius. 我是水瓶座</li>
</ul>
<p>&nbsp;</p>
<p><strong><em>2.Is your star sign Leo? 你的星座是獅子座嗎？</em></strong></p>
<ul>
<li>No. I am a typical Cancer.&nbsp; 不是 我是典型的巨蟹座</li>
</ul>
<p>&nbsp;</p>
<p><strong><em>3.As a person born under the sign of Pisces，he is very sensitive and imaginative.</em></strong></p>
<p>身為雙魚座，他非常的善解人意也富有想像力</p>
<p>&nbsp;</p>
<p><em><strong>4.A Capricorn guy and a Taurus girl are a perfect match. Both of them are Earth sign.</strong></em>&nbsp;</p>
<p>魔羯男和金牛女非常配。他們都是土象星座</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '【生活英文】十二星座英文解析，教你怎麼用英文介紹自己星座！','og_description' => '生活中很常聊到「星座」這個話題，外國人也一樣迷信星座。今天就來教大家該怎麼用英文聊自己的星座、個性，開啟社交新話題～帶大家深入了解常用的星座英文用語吧！','meta_title' => '【生活英文】十二星座英文解析，教你怎麼用英文介紹自己星座！','meta_description' => '生活中很常聊到「星座」這個話題，外國人也一樣迷信星座。今天就來教大家該怎麼用英文聊自己的星座、個性，開啟社交新話題～','canonical_url' => 'https://tw.english.agency/constellation-sign','feature_image_alt' => '十二星座英文解析，教你怎麼用英文介紹自己星座！','feature_image_title' => '十二星座英文解析，教你怎麼用英文介紹自己星座！','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a530f564c388-1i9oxkPF39nLKM3aSaDAUt8e2OXyWo-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-01-08 05:40:29"'],
  ['id' => '100','title' => '最實用的美妝英文教學！搞懂歐美Youtuber的介紹','slug' => '美妝用品英文教學','excerpt' => '前陣子是Sephora的折扣季，想網購國外美妝用品卻不知道產品英文怎麼說？在國的彩妝店卻不知道怎麼跟店員對話？或是想聽懂歐美Youtuber的介紹嗎？今天就來教大家最實用的美妝相關英文！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p>前陣子是Sephora的折扣季，想網購國外美妝用品卻不知道產品英文怎麼說？在國的彩妝店卻不知道怎麼跟店員對話？或是想聽懂歐美Youtuber的介紹嗎？今天就來教大家最實用的美妝相關英文！</p>
<p>&nbsp;</p>
<h1><span style=""color: #003377;""><strong>Cosmetics</strong> 化妝品</span></h1>
<h3><span style=""color: #a42d00;""><strong><img title=""化妝品英文怎麼說"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a5314568e38a-o07wM5hrXX7c8FOXAqT9ioBr1Dnn3g-1040x554.png"" alt=""化妝品英文怎麼說"" width=""100%"" height=""554"" /></strong></span></h3>
<h3><span style=""color: #a42d00;""><strong>Foundation Cosmetics 底妝化妝品</strong> </span></h3>
<p><strong>Foundation liquid </strong>粉底液</p>
<p><strong>Foundation Compact</strong> 粉餅</p>
<p><strong>Make up base/primer</strong> 妝前乳</p>
<p><strong>Sun screen/Sun cream</strong> 隔離霜/防曬</p>
<p><strong>Blemism balm </strong>BB霜</p>
<p><strong>Color correction </strong>CC霜</p>
<p><strong>Concealer</strong> 遮瑕</p>
<p><strong>Pen concealer</strong> 遮瑕筆</p>
<p><strong>Stick concealer</strong> 遮瑕膏條</p>
<p><strong>Pressed powder</strong> 蜜粉餅</p>
<p><strong>Contour/Shading powder</strong> 修容餅</p>
<p><strong>Blush</strong> 腮紅</p>
<p>&nbsp;</p>
<h3><span style=""color: #a42d00;""><strong>Eye Cosmetics</strong> 眼部彩妝品</span></h3>
<p><strong>Eye shadow</strong> 眼影</p>
<p><strong>Single eye shadow </strong>單色眼影</p>
<p><strong>Eye shadow duo</strong> 雙色眼影</p>
<p><strong>Eye shadow trio </strong>三色眼影</p>
<p><strong>Eye liner</strong> 眼線筆</p>
<p><strong>Liquid eye liner</strong> 眼線液</p>
<p><strong>Gel eyeliner</strong> 眼線膠</p>
<p><strong>Mascara</strong> 睫毛膏</p>
<p><strong>False eyelashes</strong> 假睫毛</p>
<p><strong>Eyebrow pencil</strong> 眉筆</p>
<p><strong>Eyebrow powder</strong> 眉粉</p>
<p>&nbsp;</p>
<h3><span style=""color: #a42d00;""><strong>Lip cosmetics 唇部化妝品</strong></span></h3>
<p><strong>Lip linear</strong> 唇線筆</p>
<p><strong>Lipstick</strong> 口紅</p>
<p><strong>Lip gloss</strong> 唇蜜</p>
<p><strong>Lip balm</strong> 護唇膏</p>
<p><strong>Lip palette</strong> 唇彩盤</p>
<p>&nbsp;</p>
<h3><span style=""color: #a42d00;""><strong>Accessories 配件</strong></span></h3>
<p><strong>Oil-absorbing sheets</strong> 吸油紙</p>
<p><strong>Cotton pads</strong> 化妝棉</p>
<p><strong>Makeup remover</strong> 卸妝用品</p>
<p><strong>Makeup removing lotion</strong> 卸妝乳</p>
<p><strong>Cleansing oil</strong> 卸妝油</p>
<p><strong>Eyelash curler</strong> 睫毛夾</p>
<p><strong>Puff</strong> 粉撲</p>
<p><strong>Foundation sponge</strong> 粉底海綿</p>
<p><strong>Brush</strong> 刷子</p>
<p><strong>Eye shadow brush</strong> 眼影刷</p>
<p><strong>Blush brush </strong>腮紅刷</p>
<p><strong>Powder brush </strong>蜜粉刷</p>
<p><strong>Brow brush</strong> 眉毛刷</p>
<p><strong>Brush cleaner</strong> 刷具清潔液</p>
<p><strong>Tweezers</strong> 鑷子</p>
<p>&nbsp;</p>
<p>【補充】化妝的英文Make up還有很多其他意思別是：</p>
<ol>
<li>編造</li>
</ol>
<p>The whole story Brian told you was not true. He made it up.</p>
<p>Brian告訴你的整個故事都不是真的。他編造了。</p>
<ol start=""2"">
<li>組成</li>
</ol>
<p>Our marketing team was made up of twelve members.&nbsp;</p>
<p>我們的行銷團隊由十二位成員組成。</p>
<ol start=""3"">
<li>補償/補考</li>
</ol>
<p>Professor，I was so sick and could not write the midterm yesterday. Can I make up the test?</p>
<p>教授，我昨天病得很嚴重沒辦法寫期中考卷，我可以補考嗎？</p>
<ol start=""4"">
<li>和好</li>
</ol>
<p>Vivian had a fight with her boyfriend，but she has made up with him now.</p>
<p>Vivian 和她的男朋友吵架，不過他們現在已經和好了</p>
<ul>
<li>腮紅的英文Blush作為動詞是指因為害羞、尷尬而臉紅，那有一種亞洲人常見的飲酒後會臉紅的現象，則叫做Asian Flush，指酒精性臉紅反應</li>
<li>彩妝中，要了解自己的肌膚性質，選擇相對應的底妝產品，那些肌膚性質丁文分別是：</li>
</ul>
<ul>
<ul>
<li><strong>Dry</strong> 乾性肌膚</li>
<li><strong>Oily</strong> 油性肌膚</li>
<li><strong>Normal</strong> 中性/一般性肌膚</li>
<li><strong>Combination</strong> 混合性肌膚</li>
<li><strong>Sensitive</strong> 敏感性肌膚</li>
</ul>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h1><span style=""color: #003377;""><strong>Dialog 常見的美妝店對話</strong></span></h1>
<p>&nbsp;<img title=""Dialog 常見的美妝店對話"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a5314ad0d9b3-HczRgd5E3GVmibDygflBr64F4T1djY-976x646.png"" alt=""Dialog 常見的美妝店對話"" width=""100%"" height=""646"" /></p>
<p>Saleswoman: How can I help you?</p>
<p>店員:請問需要什麼協助嗎?</p>
<p>I am looking for ______ (ex: mascara/ foundation/ lipsticks&hellip;)&nbsp;</p>
<p>我想買_____ (例如 睫毛膏、粉底、口紅&hellip;.)</p>
<p>&nbsp;</p>
<p>Saleswoman: What&rsquo;s your skin type?</p>
<p>店員：你是什麼種類的肌膚呢?</p>
<p>The T zone of my face is oily，but my cheeks are so dry.</p>
<p>我的T字部位很油，但雙頰很乾。</p>
<p>Saleswoman: You have combination skin. This non-oily gel can help to hydrate your skin.</p>
<p>店員：你有混合性肌膚。這種非油性的凝膠可以幫助你滋潤肌膚。</p>
<p>&nbsp;</p>
<p>Is there any promotion offering right now?</p>
<p>請問現在有沒有這扣？</p>
<p>May I have a try?&nbsp;</p>
<p>可以試用一下嗎？</p>
</div>"','published' => '0','og_title' => '【生活英文】想聽懂歐美Youtuber的介紹嗎？最實用的美妝英文教學！','og_description' => '前陣子是Sephora的折扣季，想網購國外美妝用品卻不知道產品英文怎麼說？在國的彩妝店卻不知道怎麼跟店員對話？或是想聽懂歐美Youtuber的介紹嗎？今天就來教大家最實用的美妝相關英文！','meta_title' => '【生活英文】想聽懂歐美Youtuber的介紹嗎？最實用的美妝英文教學！','meta_description' => '前陣子是Sephora的折扣季，想網購國外美妝用品卻不知道產品英文怎麼說？在國的彩妝店卻不知道怎麼跟店員對話？或是想聽懂歐美Youtuber的介紹嗎？今天就來教大家最實用的美妝相關英文！','canonical_url' => 'https://tw.english.agency/美妝用品英文教學','feature_image_alt' => '【生活英文】想聽懂歐美Youtuber的介紹嗎？最實用的美妝英文教學！','feature_image_title' => '【生活英文】想聽懂歐美Youtuber的介紹嗎？最實用的美妝英文教學！','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a531cc01a1a6-89svRv8Zjp1A1LsdYDAaptBW3b5yvh-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-01-08 06:56:16"'],
  ['id' => '101','title' => '各式保養品英文怎麼說？想在國外買保養品必學！','slug' => 'care-products','excerpt' => '不論到哪都需要保養，尤其在國外常常會因為空氣很乾，適合台灣的保養品到了國外可能就不夠用了，這時候就需要在國外買保養品，或是在國外機場想買特定保養品但不知道英文該怎麼說？小編今天整理了各式保養品的英文說法，要來教教大家最實用的保養英文！讓你到哪都能順利買到適合自己的保養品～','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<h2><span style=""color: #0088a8;""><strong>Care Products 保養品</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">各個部位的保養品都有不同的說法，一起來學怎麼說吧！</span></strong></p>
<h4 style=""color: #fa8072;""><strong>Facial Care Products 臉部保養品</strong></h4>
<ul>
<li><em>Facial cleanser/ face wash ➩洗面乳</em></li>
<li><em>Toning lotion ➩化妝水</em></li>
<li><em>Essence ➩精華液</em></li>
<li><em>Moisturizer ➩保濕</em></li>
<li><em>Nutritious ➩滋養</em></li>
<li><em>Hydro ➩水的</em></li>
<li><em>Whitening ➩美白</em></li>
<li><em>Lotion ➩露</em></li>
<li><em>Cream ➩霜</em></li>
<li><em>Day cream ➩日霜</em></li>
<li><em>Night cream ➩晚霜</em></li>
<li><em>Facial mask/ Masque ➩面膜</em></li>
<li><em>Peel off mask ➩撕去性面膜</em></li>
<li><em>Rinse off mask ➩沖洗式面膜</em></li>
<li><em>Scrub ➩去角質、磨砂</em></li>
<li><em>Water spray ➩保濕噴霧</em></li>
<li><em>Eye cream/ Eye gel ➩眼霜</em></li>
<li><em>Eye mas ➩眼膜</em></li>
<li><em>Lip balm ➩護唇膏</em></li>
<li><em>Skin Care ➩護膚</em></li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Body Care Product 身體保養品</strong></h4>
<ul>
<li><em>Body moisturizer ➩身體保濕</em></li>
<li><em>Body lotion ➩身體乳液</em></li>
<li><em>Body butter ➩身體乳霜(油)</em></li>
<li><em>Body cream ➩身體霜</em></li>
<li><em>Body spray ➩身體噴霧</em></li>
<li><em>Body wash/showing gel ➩沐浴乳</em></li>
<li><em>Sunscreen spray ➩防曬噴霧</em></li>
<li><em>Body scrub ➩身體去角質</em></li>
<li><em>Self-tanning oil ➩助曬</em></li>
<li><em>Perfume/fragrance ➩香水</em></li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Hand Care Product 手部保養品</strong></h4>
<ul>
<li><em>Hand cream/ hand lotion ➩護手霜</em></li>
<li><em>Hand cleanser ➩洗手乳</em></li>
<li><em>Nail polish ➩指甲油</em></li>
<li><em>Polish remover ➩去光水</em></li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Foot Care Product 腿部保養品</strong></h4>
<ul>
<li><em>Foot cream ➩腿霜</em></li>
<li><em>Foot scrub ➩腿部去角質</em></li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #fa8072;""><strong>Hair Care Product 頭髮保養品</strong></h4>
<ul>
<li><em>Shampoo ➩洗髮精</em></li>
<li><em>Conditioner ➩潤髮乳</em></li>
<li><em>Mousse ➩慕斯</em></li>
<li><em>Hair treatment/ Hair Essence ➩護髮精華</em></li>
</ul>
<p>&nbsp;</p>
<p style=""color: #cc0000;""><strong>【補充】</strong></p>
<p style=""color: ##ee7700;"">保養品有許多不同的功能，其中<strong>anti-</strong> 指的就是防、抗、反，像是:</p>
<ul>
<li>Anti-wrinkle 就是指抗皺的意思，同理，<strong>anti-</strong>這個字根也可以用在很多其他領域，例如：</li>
<li>Anti-social ➩不愛社交的</li>
<li>Anti-oxidant ➩抗氧化劑</li>
<li>Antipathy ➩反感</li>
<li>Antiwar ➩反戰的</li>
<li>Antidepressant ➩抗抑鬱劑</li>
</ul>
<p>&nbsp;</p>
<p style=""color: ##ee7700;"">保養品其他常見的功能還包括了：</p>
<ul>
<li>Oil control ➩控油</li>
<li>Brightening ➩亮膚</li>
<li>Balance ➩油水平衡</li>
<li>Clean ➩清潔</li>
<li>Waterproof ➩防水</li>
<li>Firming ➩緊實</li>
<li>Gentle ➩溫和的</li>
<li>Long lasting ➩持久性</li>
<li>Treatment ➩修護</li>
<li>Renewal ➩活膚修護</li>
</ul>
<p>&nbsp;</p>
<p style=""color: ##ee7700;"">保養品也含了許多成分，常見的有：</p>
<ul>
<li>Mineral oil ➩礦物油</li>
<li>Plant oil ➩植物油</li>
<li>Aloe vera ➩蘆薈</li>
<li>Alcohol ➩酒精</li>
<li>Fruit acids ➩果酸</li>
<li>Sulfur ➩硫磺</li>
<li>Tea tree ➩茶樹</li>
<li>Green tea extract ➩綠茶精華</li>
<li>Kukui nut oil ➩夏威夷核油</li>
<li>Urea ➩尿素</li>
<li>Vitamin ➩維他命</li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;<img title=""購買保養品時常用例句"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a532e4283a92-Pru4YbhSEtbouL56SU9QC9gYfFOQNj-1032x688.png"" alt=""購買保養品時常用例句"" width=""100%"" height=""688"" /></p>
<h2><span style=""color: #0088a8;""><strong>Examples 保養品店常用例句</strong></span></h2>
<p><strong><span style=""color: #ee7700;"">到國外購買保養品一定要學會以下這幾句：</span></strong></p>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<ol>
<li><em><strong>Do you have any sample I can try?&nbsp;</strong></em><br />有試用品讓我用用看嗎？</li>
<li><em><strong>Can you apply a little on my face?&nbsp;</strong></em><br />可以試一點在我臉上嗎？</li>
<li><em><strong>How much does Jo Malone perfume cost?&nbsp;</strong></em><br />Jo Malone的這款香水要多少錢？</li>
<li><em><strong>What is this lotion for?&nbsp;</strong></em><br />這款乳液的功效是什麼？</li>
<li><em><strong>I am looking for an alcohol-free toner.</strong></em><br />我在找一個不含酒精的化妝水</li>
<li><em><strong>I heard that your eye creams work pretty well. Could you recommend one?</strong></em><br />我聽說你們的眼霜很有用，可以推薦我一下嗎？&nbsp;</li>
<li><em><strong>I have sensitive skin. Could you recommend any suitable skin care product to me?&nbsp;</strong></em><br />我是敏感肌。可以請你推薦任何適合的護膚產品給我嗎？</li>
<li><em><strong>Is there any discount/promotion?&nbsp;</strong></em><br /><em><strong>Are there any discounts/ promotions?&nbsp;</strong></em><br />有任何折扣、促銷活動嗎？&nbsp;</li>
</ol>
<p>(<strong>Is or Are depends on</strong> 指的是單數還是複數(或是整間店)的產品)</p>
</div>
</div>"','published' => '1','og_title' => '【生活英文】各式保養品英文怎麼說？想在國外買保養品必學！','og_description' => '不論到哪都需要保養，尤其在國外常常會因為空氣很乾，適合台灣的保養品到了國外可能就不夠用了，這時候就需要在國外買保養品，或是在國外機場想買特定保養品但不知道英文該怎麼說？小編今天整理了各式保養品的英文說法，要來教教大家最實用的保養英文！讓你到哪都能順利買到適合自己的保養品～','meta_title' => '【生活英文】各式保養品英文怎麼說？想在國外買保養品必學！','meta_description' => '不論到哪都需要保養，尤其在國外常常會因為空氣很乾，適合台灣的保養品到了國外可能就不夠用了，這時候就需要在國外買保養品，或是在國外機場想買特定保養品但不知道英文該怎麼說？小編今天整理了各式保養品的英文說法，要來教教大家最實用的保養英文！讓你到哪都能順利買到適合自己的保養品～','canonical_url' => 'https://tw.english.agency/care-products','feature_image_alt' => '【生活英文】各式保養品英文怎麼說？想在國外買保養品必學！','feature_image_title' => '【生活英文】各式保養品英文怎麼說？想在國外買保養品必學！','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a532b197a9da-gmjZYqErYLQ59bRnG0HDwTzpRWrDer-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-01-08 08:06:23"'],
  ['id' => '102','title' => '健身英文｜六塊肌、深蹲、人魚線的英文怎麼說？','slug' => '常用健身英文','excerpt' => '近年來健身已經變為一股新的潮流！該怎麼和外國朋友交流健身技巧？如何讀懂外國網站、Youtube上的的健身資訊？今天就來教教大家最實用的健身英文！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p>近年來健身已經變為一股新的潮流！該怎麼和外國朋友交流健身技巧？如何讀懂外國網站、Youtube上的的健身資訊？今天就來教教大家最實用的健身英文！</p>
<p>&nbsp;<img title=""健身房器材、健身動作怎麼說"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a533da34ff3a-c2JxPWK7cjGVQgThFEn2G0FGrwV5jg-1034x516.png"" alt=""健身房器材、健身動作怎麼說"" width=""100%"" height=""516"" /></p>
<h1><span style=""color: #a42d00;""><strong>Muscles 肌肉</strong></span></h1>
<p><strong>Core</strong> ➪核心肌群</p>
<p><strong>Biceps </strong>➩二頭肌</p>
<p><strong>Triceps</strong> ➩三頭肌</p>
<p><strong>Abdominal muscle </strong>➩腹肌 (簡稱<strong>abs</strong>)</p>
<p><strong>Six-pack abs</strong> ➩六塊腹肌</p>
<p><strong>Firm abs </strong>➩馬甲線</p>
<p><strong>V-line abs</strong> ➩人魚線</p>
<p><strong>Calf muscle</strong> ➩小腿肌群</p>
<p><strong>Gluteus </strong>➩臀大肌</p>
<p><strong>Trapezius </strong>➩斜方肌</p>
<p>&nbsp;</p>
<h1><span style=""color: #a42d00;""><strong>Fitness equipment 健身器材</strong></span></h1>
<p><strong>Dumbbells</strong> ➩啞鈴</p>
<p><strong>Barbells</strong> ➩槓鈴</p>
<p><strong>Bandgrip</strong> ➩握力器</p>
<p><strong>Chestpull </strong>➩彈簧擴展器</p>
<p><strong>Treadmill </strong>➩跑步機</p>
<p><strong>Aero bike</strong> ➩有氧健身車</p>
<p>&nbsp;&nbsp;</p>
<h1><span style=""color: #a42d00;""><strong>Workout moves 健身動作</strong></span></h1>
<p><strong>Squat </strong>➩深蹲</p>
<p><strong>Push-up </strong>➩俯地挺身</p>
<p><strong>Sit-up </strong>➩仰臥起坐</p>
<p><strong>Crunch </strong>➩卷腹</p>
<p><strong>Reverse crunch </strong>➩反向卷腹</p>
<p><strong>Dead lift</strong> ➩硬舉</p>
<p><strong>Hip extension</strong> ➩臀部伸展</p>
<p><strong>Bench press </strong>➩臥推</p>
<p><strong>Lunge </strong>➩跨步</p>
<p><strong>Plank </strong>➩棒式</p>
<p><strong>Bridge </strong>➩橋式</p>
<p><strong>Lunges</strong> ➩弓箭步</p>
<p><strong>Jumping jacks </strong>➩開合跳</p>
<p><strong>Burpee </strong>➩波比跳</p>
<p><strong>Pull-ups</strong> ➩引體向上</p>
<p><strong>Aerobic exercise</strong> ➩有氧運動</p>
<p><strong>Cardiovascular training </strong>➩心肺適能訓練</p>
<p>&nbsp;</p>
<p><strong>Gym</strong> ➩健身房</p>
<p><strong>Instructor</strong> ➩教練</p>
<p><strong>Workout </strong>➩健身</p>
<p><strong>Warm up </strong>➩暖身</p>
<p><strong>Cool down </strong>➩緩和運動</p>
<p><strong>Stretching</strong> ➩伸展</p>
<p><strong>Circuit training</strong> ➩循環運動</p>
<p>&nbsp;</p>
<p>【補充】</p>
<ul>
<li>健身這個英文單字，我們可以用<strong>work out</strong>、<strong>fitness</strong><strong>，</strong>而<strong>exercise</strong>指的是比較廣泛的運動的統稱</li>
</ul>
<h3>&nbsp;</h3>
<h3><span style=""color: #003377;"">人的身材也有很多不同的英語形容詞：</span></h3>
<p><strong>Well-built </strong>➩結實的</p>
<p><strong>Fit </strong>➩健康、身材健美的</p>
<p><strong>Lean</strong> ➩精實的</p>
<p><strong>Wiry </strong>➩節食、精瘦</p>
<p><strong>Muscular </strong>➩肌肉發達的</p>
<p><strong>Brawny </strong>➩肌肉發達、強壯的</p>
<p><strong>Ripped </strong>➩肌肉線條分明的</p>
<p><strong>Husky</strong> ➩高壯的</p>
<p><strong>Beefy </strong>➩塊頭大的</p>
<p><strong>Curvy</strong> ➩玲瓏有緻、曲線的</p>
<p><strong>Big-boned </strong>➩骨架大的</p>
<p><strong>Chubby </strong>➩胖的</p>
<p><strong>Skinny</strong> ➩瘦的</p>
<p><strong>Slim</strong> ➩苗條的 (女生)</p>
<p><strong>Slender </strong>➩體態優雅 (女生)</p>
<p><strong>Petite</strong> ➩嬌小的 (女生)</p>
<p>&nbsp;</p>
<h3><span style=""color: #003377;"">除此之外，還有一些健身有關的其他英文單詞：</span></h3>
<p><strong>Muscle strain</strong> ➩肌肉拉傷</p>
<p><strong>Muscle failure</strong> ➩肌肉疲乏</p>
<p><strong>Muscle head </strong>➩健身狂人</p>
<p><strong>Supplement</strong> ➩健身營養補給品</p>
<p><strong>Protein drink </strong>➩蛋白質飲料</p>
<p><strong>Protein powder </strong>➩高蛋白粉</p>
<p><strong>Protein bar</strong> ➩蛋白質營養棒</p>
<p><strong>Carbohydrate </strong>➩碳水化合物，簡稱Carbs</p>
<p><strong>Sodium </strong>➩鈉</p>
<p><strong>Fat</strong> ➩脂肪</p>
<p><strong>Repetitions</strong> ➩次數(每個健身動作要做幾次)</p>
<p><strong>Sets</strong> ➩組數(每個健身動作要做幾組)</p>
<p><strong>Fully-equipped gym</strong> ➩設備健全的健身房</p>
<p><strong>Gym membership</strong> ➩健身房會員</p>
<p>&nbsp;</p>
<h1><span style=""color: #a42d00;""><strong>Dialogs 常用的健身相關對話</strong></span></h1>
<p>&nbsp;<img title=""健身相關對話"" src=""https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a533deedf622-Fd3ZeHyX0mp40kJC6QP9KXNTe3lWZd-974x512.png"" alt=""健身相關對話"" width=""100%"" height=""512"" /></p>
<p>A: You look so ripped! How often do you work out?</p>
<p>A:你的肌肉看起來好有線條！你都多久健身一次啊？</p>
<p>B: I have been working out 5 times a week since university.&nbsp;</p>
<p>B: 從大學開始，我每個禮拜健身五次</p>
<p>A: Nice! Can I know how many sets and reps do you do for each machine?</p>
<p>A: 真棒！我可以知道你每個運動器材都做幾組跟幾下嗎？</p>
<p>B: My workout routine consisted of 3 sets and 10 to 12 reps for each set until muscle failure.</p>
<p>B: 我的日常健身內容是由每個器材三組、每組十到十二下直到肌肉疲勞所構成</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '【生活英文】六塊肌、深蹲、人魚線的英文怎麼說？常用的健身英文！','og_description' => '近年來健身已經變為一股新的潮流！該怎麼和外國朋友交流健身技巧？如何讀懂外國網站、Youtube上的的健身資訊？今天就來教教大家最實用的健身英文！','meta_title' => '【生活英文】六塊肌、深蹲、人魚線的英文怎麼說？常用的健身英文！','meta_description' => '近年來健身已經變為一股新的潮流！該怎麼和外國朋友交流健身技巧？如何讀懂外國網站、Youtube上的的健身資訊？今天就來教教大家最實用的健身英文！','canonical_url' => 'https://tw.english.agency/常用健身英文','feature_image_alt' => '【生活英文】六塊肌、深蹲、人魚線的英文怎麼說？常用的健身英文！','feature_image_title' => '【生活英文】六塊肌、深蹲、人魚線的英文怎麼說？常用的健身英文！','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a533d722ac4d-FvnWloWOEtxsNH7VrWRZiDR0rvCbjm-1200x630.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-01-08 08:44:35"'],
  ['id' => '103','title' => '如何挑選菲律賓遊學代辦？菲律賓遊學，5件事你要知道!-English.agency','slug' => 'philippines-study-aboard-agent','excerpt' => '想去菲律賓遊學的人幾乎都會遇到很重要的問題，就是不知道該如何挑選菲律賓語言學校、該選擇哪個地區？而通常大家這時候都會把問題帶到菲律賓遊學代辦中心解決，但究竟要怎麼辨別代辦中心的好壞呢？今天小編帶大家來看看幾個重要的代辦中心原則！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p class=""p1"">想去<a title=""菲律賓遊學"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>的人幾乎都會遇到很重要的問題，就是不知道該如何挑選菲律賓語言學校、該選擇哪個地區？而通常大家這時候都會把問題帶到菲律賓遊學代辦中心解決，但究竟要怎麼辨別代辦中心的好壞呢？今天小編帶大家來看看幾個重要的代辦中心原則！</p>
<h2 class=""p2"">&nbsp;</h2>
<h2 class=""p3""><span class=""s1""><strong>1.</strong></span>找代辦、挑顧問，他真的懂菲律賓學英文？懂語言學校嗎？</h2>
<p class=""p1"">許多人在挑選代辦的時候往往都會貨比三家，但是到底該比較什麼呢？以下幾點是大家可以參考的：</p>
<p class=""p2"">&nbsp;</p>
<ul class=""ul1"">
<li class=""li1"" style=""color: lightcoral;"">顧問在提及語言學校時，有沒有提到費用、學習型態、周邊設施、校內師資比與學生比？<span class=""s3""><br /></span></li>
<li class=""li1"" style=""color: lightcoral;"">代辦中心彼此之間對於該菲律賓學校的解說有沒有出入？<span class=""s3""><br /></span></li>
<li class=""li1"" style=""color: lightcoral;"">菲律賓遊學顧問在諮詢時，有沒有考慮到你的需求？</li>
</ul>
<p>&nbsp;</p>
<p class=""p1"">一個好的顧問會相當重視學生當時的需求以及學生的實際程度與目標，因此在諮詢的過程中，會依據這兩點來為學生挑選適當的課程，接著可能會挑選學校，這種間沒有特定的順序關係，但是必都會以學生的考量為中心喔！</p>
<p class=""p1"">但如果學生本身並沒有先做研究與調查，基本上這樣諮詢的意義就不大了喔！如果想要深入調查學校資訊，可以到各個代辦的網站，或是直接到學校的網站，部分學校網站都有提供中文介版喔，例如：<a href=""tw.howdyenglish.asia"">howdy語言學校</a>、<a href=""https://helpenglish.com.tw/"">Help語言學校</a>、<a href=""http://3dacademy.tw/"">3D語言學校</a>、<a href=""http://smenglish.com/tw/?ckattempt=1"">SME語言學校</a>。&nbsp;</p>
<p class=""p1"">&nbsp;</p>
<h2 class=""p1"">2.菲律賓遊學費用清晰化，代辦應該要告訴你的收費細節：</h2>
<p class=""p1"">大家再作諮詢時，不外乎最在意的就是非用是否有清楚透明，而專業的顧問也會依具你的需要提供詳細的學校收費資訊給你，而這些常見的收費內容大致上包括以下幾點：</p>
<ul>
<li class=""p1"" style=""color: lightcoral;"">學費平均價格</li>
<li class=""p1"" style=""color: lightcoral;"">住宿費</li>
<li class=""p1"" style=""color: lightcoral;"">來回機票</li>
<li class=""p1"" style=""color: lightcoral;""><a href=""https://www.liontravel.com/webvisa/webvsse02.aspx?sForm=CSOR&amp;sVscountry=PH&amp;sVSseq=01"">觀光簽證</a></li>
<li class=""p1"" style=""color: lightcoral;"">ss特別許可</li>
<li class=""p1"" style=""color: lightcoral;"">餐費</li>
</ul>
<p>以下幾點為由學校提供的服務：</p>
<ul>
<li style=""color: lightcoral;"">洗衣、清潔服務由學校提供</li>
<li style=""color: lightcoral;"">三餐由學校提供</li>
</ul>
<p>附註：自2018年4月起宿霧太平洋航空不再提供<a href=""https://www.backpackers.com.tw/forum/showthread.php?t=10031700"">宿霧直飛</a>！&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2>3.菲律賓遊學代辦使命-提供學生10件與學校相關的事</h2>
<p>當然除了談到費用、初步介紹菲律賓遊學之外，更重要的是顧問要能夠相繫的說明學校的細節，不論是學校課程、或是學校設施，都需要加以說明，而大致上有以下幾點：</p>
<p>&nbsp; &nbsp; &nbsp;&nbsp;</p>
<ul style=""list-style-type: circle;"">
<li style=""color: lightcoral;"">
<div><strong>課程時數：</strong></div>
</li>
<li>
<div>學校通常是以課程時數為計算，因此對於學生 來說這可說是最重要的一段資訊。</div>
</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>菲律賓語言學校價目表估算：</strong></li>
<li>價目估算可以幫助學生在有限的預算內做出最佳選擇，透明化的費用絕對是專業顧問必做的事情。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>語言學校校園介紹：</strong></li>
<li>學校設備、住宿問題、教育設備、交通等等對於學生來說都是最重要的一環。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>住宿相關情況：</strong></li>
<li>學校房舍、宿舍基本設施說明。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>簽證規費：</strong></li>
<li>前往菲律賓有遊學有許多的簽證事宜，多數菲律賓遊學代辦會替學生處理，但還是會向學生說明。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>機票費用：</strong></li>
<li>機票代辦中心通常也都會替學生代訂，但詳細的手續流程也會向學生說明。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>醫療保健相關事項：</strong></li>
<li>顧問除了安排事宜之外，在健康方面，必須給予學生相對對應的解決辦法，如在菲律賓的就醫手續等等。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>師資來源：</strong></li>
<li>師資說明、透明化，絕對是學生關注的一個重要因素。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>生活打理與概況：</strong></li>
<li>學校是否有提供洗衣服務、洗衣機、曬衣場、房間清掃等等都是學生關注的點。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>當地風俗及生活注意事項：</strong></li>
<li>為避免學生產生『文化衝擊』，多數顧問也會在出發前詳細的說明在地的文化，並讓學生能夠適應。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>學校法規與合約內容：</strong></li>
<li>講解學校法規，以免學生觸犯校規引起糾紛。</li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""color: lightcoral;""><strong>保險相關：</strong></li>
<li>對於保險相關事宜也是許多人相當重視的一環，是否需要保險？有沒有協助辦理等等顧問都應該加以說明。</li>
</ul>
<h2 class=""p1"">&nbsp;</h2>
<h2 class=""p1"">4.好的菲律賓遊學代辦，售後服務很重要！</h2>
<p>許多代辦在送出學生後，可能就會與學生失聯，但一個好的顧問是不會輕忽售後服務的，可能會透過學校經理、或是用其他方式與學生保持聯絡，以免學生在遇到問題時不知道該如何處理，此外也避免學生有必須臨時返國，需要退費、或是辦理相關手續的問題，適時的提供協助。</p>
<p>&nbsp;</p>
<h2>5.找代辦前，先看看別人的菲律賓遊學心得，但心得不一定為真！</h2>
<p>多數學生在找代辦時，不一定會自己主動找資料，可能都是直接帶著疑問去找代辦，而代辦也往往會直接提供學長姐的心得作為參考，但這樣的參考真的有價值嗎？建議學生要先思考自己的需求、自己的情境，然後自己主動找過一輪資料，之後再詳細比較學長姐的心得文，免得片面解讀了心得文的訊息，挑選了不適合自己的菲律賓遊學課程，因此善用網路資源是相當重要的一件事。</p>
</div>"','published' => '1','og_title' => '如何挑選菲律賓遊學代辦？5件事你要知道','og_description' => '想去菲律賓遊學的人幾乎都會遇到很重要的問題，就是不知道該如何挑選菲律賓語言學校、該選擇哪個地區？而通常大家這時候都會把問題帶到菲律賓遊學代辦中心解決，但究竟要怎麼辨別代辦中心的好壞呢？今天小編帶大家來看看幾個重要的代辦中心原則！','meta_title' => '如何挑選菲律賓遊學代辦？5件事你要知道','meta_description' => '想去菲律賓遊學的人幾乎都會遇到很重要的問題，就是不知道該如何挑選菲律賓語言學校、該選擇哪個地區？而通常大家這時候都會把問題帶到菲律賓遊學代辦中心解決，但究竟要怎麼辨別代辦中心的好壞呢？今天小編帶大家來看看幾個重要的代辦中心原則！','canonical_url' => 'https://tw.english.agency/philippines-study-aboard-agent','feature_image_alt' => '菲律賓遊學代辦','feature_image_title' => '菲律賓遊學代辦','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a5388a9267ba-z89Sgug290aDxh09GFeBI89HiT5CKu-1182x584.jpeg','author_id' => '1','category_id' => '1','created_at' => '"2018-01-08 09:05:34"'],
  ['id' => '104','title' => '菲律賓遊學行前準備、行李懶人包，讓你出國遊學不忘東忘西！','slug' => 'study-aboard-prepare','excerpt' => '準備要去菲律賓遊學的你，對於即將前往的就讀的菲律賓語言學校有所認識了嗎？都知道自己應該要帶什麼東西了嗎？許多人去菲律賓學英文時常只記得要帶電腦、課本、以及各種上課可能會用到的東西，但藥品、或是其他必需品真的都帶齊了嗎？今天小編就帶大家來看看去菲律賓唸書到底應該要帶到哪些東西吧！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>準備要去<a title=""菲律賓遊學，為何值得你考量？詳細解說菲律賓語言學校費用與代辦挑選！"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>的你，對於即將前往的就讀的<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>有所認識了嗎？都知道自己行李應該要準備什麼東西了嗎？許多人去菲律賓學英文時常只記得要帶電腦、課本、以及各種上課可能會用到的東西，但藥品、或是其他必需品真的都帶齊了嗎？今天小編就帶大家來看看去菲律賓唸書到底應該要帶到哪些東西吧！</p>
<hr />
<h2 style=""font-size: 28px;"">菲律賓遊學行李必備項目清單：</h2>
<h4>菲律賓遊學必帶證件相關物品：</h4>
<p>&nbsp;</p>
<ul>
<li><strong>消費方面</strong>：請準備信用卡、現金、或是提款卡，確認是否可以跨國提款。</li>
<li><strong>菲律賓簽證</strong>: 要確認護照是否已經通過檢核，以及該護照是不是在簽證、護照期間。</li>
<li><strong>大頭照</strong>： 主要是給學校辦理相關證件所使用，至少兩組2吋、至少準備7-8張，2張簽證用、4-5張學校用，需採用5X5、如果有延續簽證，請先準備至16張。</li>
<li><strong>電子機票影本</strong>：兩份，提供給學校辦理SSP。</li>
<li><strong>旅遊平安險</strong>： 相關證明間是否已經帶齊。</li>
<li><strong>學校及代辦的聯絡資訊</strong>：請確保聯繫管道暢通，以便提供協助。&nbsp; &nbsp;</li>
</ul>
<p>部分東西你的<a title=""如何挑選菲律賓遊學代辦？菲律賓遊學，5件事你要知道!-English.agency"" href=""https://tw.english.agency/philippines-study-aboard-agent"">菲律賓遊學代辦</a>會替你處理好，建議學員還是要先做詢問喔！</p>
<p>對於菲律賓簽證有問題，可以參考這篇<a href=""https://www.liontravel.com/webvisa/webvsse02.aspx?sForm=CSOR&amp;sVscountry=PH&amp;sVSseq=01"">菲律賓簽證大全！</a>&nbsp;或是這篇<a href=""http://www.ugophilippines.com/?p=1038&amp;variant=zh-cn"">簽證指南</a>！</p>
<hr />
<h2 style=""font-size: 28px;"">菲律賓遊學必帶生活用品:</h2>
<p>&nbsp;</p>
<ul>
<li><strong>個人盥洗用品</strong>：建議如果有慣用的請先帶齊，以免當地的用不習慣。</li>
<li><strong>個人藥品</strong>：當地藥橘販售的要不一定適合，所以務必要攜帶個人藥品，如胃藥、防蚊液、暈車藥等等。</li>
<li><strong>眼鏡</strong>：對於許多常使用隱形眼鏡的朋友，建議要一次攜帶足夠的量，避免在當地購買喔！</li>
<li><strong>保養用品</strong>：對於就讀斯巴達學校的朋友來說，不一定有足夠的時間可以外出購買建議先準備好喔！</li>
<li><strong>零食、點心</strong>：不一定要帶，當地也有很多好吃的東西，在學系校內也有福利社，但如果想帶還是可以喔！</li>
<li><strong>衛生用品</strong>：請至少攜帶一些可以隨身攜帶的面紙，女生的話務必攜帶衛生棉，口罩也是。</li>
</ul>
<hr />
<h2 style=""font-size: 28px;"">&nbsp;菲律賓學英文必帶電子用品：</h2>
<p>&nbsp;</p>
<ul>
<li><strong>變壓器</strong>： 菲律賓的電壓是220v，台灣是110v，務必帶變壓器，免得你的寶貝電腦、手機等等東西燒壞喔！</li>
<li><strong>筆電</strong>：筆電會是你在晚上自習時的好幫手，對於要攜帶筆電的朋友，務必要繫帶變壓器喔！</li>
<li><strong>手機</strong>：攜帶手機務必雖身攜帶，不要讓它離開你的身邊喔！</li>
<li><strong>相機</strong>：可以用在假日去校外活動時使用，相當不錯。</li>
<li><strong>電子辭典</strong>：自習時的好工具，建議晚上充飽電，可以帶去上課喔！</li>
</ul>
<p>如果對於菲律賓一些文化還不是很熟習，建議可以到<a href=""http://www.itsmorefuninthephilippines.com.tw/"">菲律賓觀光局</a>看看喔!&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2 style=""color: #ff0000; font-size: 28px;"">注意事項</h2>
<p>&nbsp;</p>
<p><strong>禁止放入隨身行李相關注意事項：</strong></p>
<ul>
<li>剛剛上面已經敘述了常見、與建議攜帶的物品，那麼不要忘記有些東西是禁止當作隨身物品攜帶上飛機的喔！</li>
<li>自拍棒、腳架等等長度為<strong>25公分以</strong>上的棒狀工具都是靜止的。</li>
<li><strong>牙膏、香水、護手霜</strong>等等這類液態物品，建議都放置在行李箱內，超過100毫升(指容器，而非實際液體)是靜止攜帶上飛機的喔！</li>
<li>刀類，如指甲剪、美工刀、小刀等等這類日常生活常用的工具都是禁止攜帶上飛機的。</li>
</ul>
<p>&nbsp;</p>
<p><strong>禁止放入托運行李的相關事項：</strong></p>
<ul>
<li>打火機不可以放入托運行李，必須放入雖身行李。</li>
<li><strong>鋰電池、行動電源</strong>等等東西必不可以放入托運行李，過去曾發生多次電子產品爆炸導致機艙出事的問題，因此如果要把這類電子產品放入行李箱內，務必確認店員已經關閉。</li>
<li>肉類、水果、農產不可以過海關，所以請不要攜帶喔！醃漬食品類的東西可以攜帶～</li>
</ul>
<p>&nbsp;</p>
<p>那最後看完這麼多東西，是不是也清楚自己應該要帶哪些東西了呢？</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '菲律賓遊學行前準備、行李懶人包，讓你出國遊學不忘東忘西！','og_description' => '準備要去菲律賓遊學的你，對於即將前往的就讀的菲律賓語言學校有所認識了嗎？都知道自己應該要帶什麼東西了嗎？許多人去菲律賓學英文時常只記得要帶電腦、課本、以及各種上課可能會用到的東西，但藥品、或是其他必需品真的都帶齊了嗎？今天小編就帶大家來看看去菲律賓唸書到底應該要帶到哪些東西吧！','meta_title' => '菲律賓遊學行前準備、行李懶人包，讓你出國遊學不忘東忘西！','meta_description' => '準備要去菲律賓遊學的你，對於即將前往的就讀的菲律賓語言學校有所認識了嗎？都知道自己應該要帶什麼東西了嗎？許多人去菲律賓學英文時常只記得要帶電腦、課本、以及各種上課可能會用到的東西，但藥品、或是其他必需品真的都帶齊了嗎？今天小編就帶大家來看看去菲律賓唸書到底應該要帶到哪些東西吧！','canonical_url' => 'https://tw.english.agency/study-aboard-prepare','feature_image_alt' => '菲律賓遊學行前準備','feature_image_title' => '菲律賓遊學行前準備','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a54770bdd0c6-7EkwgI591t2D91bYSNAY2daK2HeOZx-1165x583.png','author_id' => '1','category_id' => '1','created_at' => '"2018-01-09 08:06:57"'],
  ['id' => '105','title' => '要去菲律賓遊學？七個菲律賓文化和語言的小知識你不可不知！','slug' => 'seven-question-of-philippines','excerpt' => '隨著最近海島度假風氣越來越盛行，菲律賓開始成為大家想出國玩的第一選擇，但其實很多人都以為菲律賓還是跟以前一樣落後，對菲律賓抱著許多懷疑和誤解，今天就來幫大家揭開菲律賓的神秘面紗，想去菲律賓打工或旅遊的你別再錯過啦！','content' => '"<div style=""line-height: 30px; letter-spacing: 1px; font-size: 14px; color: #666666;"">
<p>隨著到國外遊學的風氣越來越盛行，離台灣最近的<a href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>也逐漸成為大家的第一選擇，但其實很多人都以為菲律賓還是跟以前一樣落後，對菲律賓抱著許多懷疑和誤解，今天就來幫大家揭開菲律賓的神秘面紗，想去菲律賓打工或旅遊的你別再錯過啦！</p>
<div class=""heading-1"" style=""color: black;"">
<h2 style=""color: #ff6666; font-size: 25px;"">1.菲律賓講什麼語言？</h2>
<p>菲律賓的官方語言是英文以及他加祿語，但此之外，在菲律賓各地還有許多不同種的方言，而最多人講的方言是Ilongo，不過不用擔心，大部份的地方只要用英文都可以簡單溝通，不用擔心人生地不熟又語言不通的問題啦！</p>
</div>
<h2 style=""color: #ff6666; font-size: 25px;"">2.菲律賓的治安如何？</h2>
<p>其實菲律賓的治安並非不好，而是首都<a href=""https://zh.wikipedia.org/wiki/%E9%A9%AC%E5%B0%BC%E6%8B%89"">馬尼拉</a>相對於台灣來說還是比較混亂，而宿霧或長灘島就相對較良好一點，菲趣建議大家去菲律賓旅行的時候儘量不要一個人走在路上，也不要跟不認識的人搭話，深夜也儘量不要外出，這樣就能降低很多被搶的風險了！</p>
<h2 style=""color: #ff6666; font-size: 25px;"">3.菲律賓的天氣如何？</h2>
<p>菲律賓位處於<a href=""https://zh.wikipedia.org/wiki/%E7%86%B1%E5%B8%B6%E6%B0%A3%E5%80%99"">熱帶氣候區</a>，年均溫約在26度上下，沒有明顯的四季，只有雨季和乾季的分別，如果你希望去菲律賓的時候能享受舒適的氣溫，建議可以選擇在11月到2月這段期間到那邊去，這時候是當地的乾季，不但不下雨，氣溫也大概在25度上下，完全就是一個避暑天堂啊！</p>
<h2 style=""color: #ff6666; font-size: 25px;"">4.菲律賓人都吃什麼？口味會很重嗎？</h2>
<p>身為海島殖民地，菲律賓的飲食文化是非常多元的，大至美式炸雞漢堡，小至西班牙美食等等的異國料理都可以在菲律賓找到，而也因為氣候的關係，菲律賓的菜餚都會有點偏酸甜的口味，讓你即使在炎熱的天氣也能吃得津津有味！</p>
<h2 style=""color: #ff6666; font-size: 25px;"">5.菲律賓交通會很不便嗎？</h2>
<p>基本上菲律賓有四種大眾交通運輸工具可以搭，分別是地鐵、火車、<a href=""https://zh.wikipedia.org/wiki/%E5%90%89%E6%99%AE%E5%B0%BC"">吉普尼</a>和巴士，而地鐵總共只有三條線，而且座位擁擠，能到達的地方不多，因此相對來說並不算太方便，而巴士可以說是菲律賓最普遍的交通工具，搭乘方式簡單而且價格便宜，尤其對於從馬尼拉出發的旅客來說更是方便，就連要到遙遠的山區碧瑤都有公車可以搭呢！</p>
<h2 style=""color: #ff6666; font-size: 25px;"">6.菲律賓有百貨公司或購物中心嗎？</h2>
<p>菲律賓的購物中心相當多，光是在馬尼拉就有超過上百間的<a href=""https://imccp.com/cebu-shopping/"">shopping mall</a>，不管想買什麼在那邊都可以買到，而且他們的購物中心功能超級多，除了有各大品牌進駐，還有遊樂場、美容spa等等設備，就連診所跟快遞都有，只要你一踏進購物中心，不花個三五個小時是絕對出不來的！</p>
<h2 style=""color: #ff6666; font-size: 25px;"">7.菲律賓有什麼特別的節日嗎？</h2>
<p>在菲律賓最盛大的節日就是一月的聖嬰節了，每年一月的第三個禮拜天會是大家瘋狂舉辦慶祝儀式的大日子，這時候常常會出現萬人空巷的現象，因為每個人都跑到大遊行去啦！不過也要特別注意，在慶祝聖嬰節時，很多地方都會交通管制，你如果真的想混進去遊行一起慶祝的話只能靠自己的雙腿慢慢走了！</p>
</div>"','published' => '1','og_title' => '要去菲律賓遊學？七個菲律賓文化和語言的小知識你不可不知！','og_description' => '隨著最近海島度假風氣越來越盛行，菲律賓開始成為大家想出國玩的第一選擇，但其實很多人都以為菲律賓還是跟以前一樣落後，對菲律賓抱著許多懷疑和誤解，今天就來幫大家揭開菲律賓的神秘面紗，想去菲律賓打工或旅遊的你別再錯過啦！','meta_title' => '要去菲律賓遊學？七個菲律賓文化和語言的小知識你不可不知！','meta_description' => '隨著最近海島度假風氣越來越盛行，菲律賓開始成為大家想出國玩的第一選擇，但其實很多人都以為菲律賓還是跟以前一樣落後，對菲律賓抱著許多懷疑和誤解，今天就來幫大家揭開菲律賓的神秘面紗，想去菲律賓打工或旅遊的你別再錯過啦！','canonical_url' => 'https://tw.english.agency/seven-question-of-philippines','feature_image_alt' => 'philippines-culture','feature_image_title' => 'philippines-culture','feature_image_url' => 'https://media-english-agency.s3-ap-southeast-1.amazonaws.com/images/5a55dbd193fbf-rUE2O1MutAZQXZ2v9nwg0yj9S1qfXC-1020x595.jpeg','author_id' => '14','category_id' => '1','created_at' => '"2018-01-10 08:41:19"'],
  ['id' => '106','title' => '菲律賓簽證、SSP申請一把抓！輕鬆搞定，菲律賓遊學簽證那些大小事！','slug' => 'philippines-visa-application','excerpt' => '談到要去菲律賓遊學，多數學生遇到的第一個問題是部隻到該如和處理簽證，通常簽證會交由合作的菲律賓遊學代辦，但到了當地如果簽證方面有問題時，除了可以透過學校方面的協助，當然也可以嘗試自己處理，那麼究竟要如何處遊學簽證呢？今天小編就帶大家來看看遊學簽證該怎麼處理吧！','content' => '"<p>談到要去<a href=""https://tw.english.agency/philippines-study-aboard-agent"">菲律賓遊學</a>，多數學生遇到的第一個問題是部隻到該如和處理簽證，通常簽證會交由合作的<a href=""https://tw.english.agency/study-aboard-prepare"">菲律賓遊學代辦</a>，但到了當地如果簽證方面有問題時，除了可以透過<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>方面的協助，當然也可以嘗試自己處理，那麼究竟要如何處遊學簽證呢？今天小編就帶大家來看看遊學簽證該怎麼處理吧！&nbsp;</p>
<hr />
<h2>菲律賓簽證基本須知</h2>
<p>如果想要去菲律賓玩，可以申請旅遊簽證等等，但如果想要在菲律賓語言學校念書，你需要申請「觀光簽證＋ SSP」，成為合法的遊學學生喔！而以下是關於菲律賓遊學簽證的大小事：</p>
<p>&nbsp;</p>
<h2>菲律賓遊學簽證說明：</h2>
<p>菲律賓的簽證系統分為兩個管道，一個是紙本簽證，一個是電子簽證這兩種電子簽證方式，而簽證核發之後都會有一定的限制，以下是常見的限制：</p>
<p>&nbsp;</p>
<h3>Ａ.紙本簽證</h3>
<p><strong>適用對象</strong>：<em>遊學天數超過30天以上者</em></p>
<p><strong>逗留期限</strong>：<em>59天</em></p>
<p><strong>費用</strong>：<em>NT$ 1200</em></p>
<p><strong>申請時間</strong>：<em>至少要兩天工作天</em></p>
<p><strong>申請方式</strong>：<em>申請人必須於台灣境內申請</em></p>
<p><strong>注意事項</strong>：</p>
<ul>
<li><em>14歲以下未有身分證者，則以3個月內申請的戶籍謄本正本代替</em></li>
<li><em>15歲以下得申請者需要由父母之一陪同，若父母無法一同前往，需要事前與菲律賓移民局提出申請。</em></li>
</ul>
<p><strong>所需文件</strong>：</p>
<ul>
<li><em>觀光簽證申請書（簽名需與護照一致）</em></li>
<li><em>清單紙（ Checking List）</em></li>
<li><em>有效期達6個月以上護照正本</em></li>
<li><em>身分證正反面影本</em></li>
<li><em>6個月內，證件照2張(菲律賓簽證照片用)</em></li>
</ul>
<p><strong>申請地點</strong>：</p>
<p>可至可至各地區，馬尼拉經濟文化辦事處辦理</p>
<p>台北</p>
<ul>
<li><em>地址：台北市長春路176號11樓</em></li>
<li><em>電話：02-25081719 分機221</em></li>
</ul>
<p>台中</p>
<ul>
<li><em>地址：台中市民權路239號4樓2A</em></li>
<li><em>電話：04-2302-9080</em></li>
</ul>
<p>高雄</p>
<ul>
<li><em>地址：高雄市三民區民族一路80號9F之2</em></li>
<li><em>電話：07-3985935</em></li>
</ul>
<p>&nbsp;</p>
<hr />
<h3><strong>B.電子簽證</strong></h3>
<p><strong>適用對象</strong>：<em>短期旅遊、遊學者</em></p>
<p><strong>逗留期限</strong>：<em>30天</em></p>
<p><strong>費用</strong>：<em>NT$ 1100</em></p>
<p><strong>申請時間</strong>：<em>由於是電子簽證，最快1-3小時，最慢3天</em></p>
<p><strong>申請方式</strong>：<em>網路申請，最好為出發前7日辦理好手續，且年紀必須大於15歲以上。</em></p>
<p><strong>所需文件：</strong></p>
<ul>
<li><em>6個月以上效期的正本護照</em></li>
<li><em>身分證正反面影本</em></li>
<li><em>語言學校與住宿資訊</em></li>
<li><em>信用卡（繳費使用</em>）</li>
</ul>
<p>申請地點：<em>請直接至官方網站申請-<a href=""https://onlinetravel.meco.org.tw/EVISA/"">馬尼拉辦事處簽證</a></em><em>&nbsp;</em></p>
<p><em>如果對於菲律賓入境相關有問題，可以參考這篇：<a href=""http://www.mrlin.tw/blog/ph/arrivalstutorial/"">[教學] 菲律賓入境教學(含電子簽證、入境卡、健康申報表)&nbsp;</a></em></p>
<div class=""post-thumbnail"">&nbsp;</div>
<hr />
<h2>什麼是SSP（Special Study Permit）</h2>
<p>SSP就是特殊學習申請許可，適用於在菲律賓語言學校、菲律賓遊學的學生們，可以說是在當地遊學、旅外學生的身分證，學生可以透過此簽證享有以下福利：</p>
<ul>
<li>公車（jeepney）優惠</li>
<li>船票優惠</li>
<li>巴士優惠</li>
</ul>
<p>注意事項：</p>
<ul>
<li>通常學校都是以觀光簽證＋ssp座位學生遊學簽證，通常都在學校入學當天處理掉。</li>
<li>通常在申請時還有會有一些附帶條件，如『非取得學位之學習、一年以下語言學校學程、或是外籍人士』等等。</li>
<li>SSP每次都只能在一所學校登記，如果有轉校、則需要辦理全新的SSP(簽證跟學校為綁一起的。)</li>
<li>每次申請的費用約莫是6000 -6500披索。</li>
</ul>
<p>要申SSP的話，還需要有當地銀行戶頭、存款證明，並前往該區辦事處處理，但由於辦事處的點不是每個地方都有，多半都是在大都市才有，多數學生對於當地人生地不熟，因此通常學校會給予協助、或是直接幫學生處理。</p>
<h2>ＳＳＰ不等於延長簽證</h2>
<p>在這邊有件重要的是一定要提醒前往菲律賓就學的學生，延簽與SSP是兩個完全不同的東西，SSP僅僅提供你『合法遊學證明』，但並不代表你有『觀光簽證』的許可，如果說你申請了卻未就讀語言學校，這部分將會被視為違法，菲律賓政府會開罰，此外你可能還會被列入黑名單，並遣返回國。&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2>&nbsp;</h2>
<h2>電子簽證實際申請辦法說明：</h2>
<p>&nbsp;首先請進入簽證處申請網址，並依照給予的指示申請：</p>
<h3>1.請先申請帳號，申請完成後可以直接填入以下頁面：</h3>
<p><img title=""電子簽證"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a5ee0e0cc4be-bFRHmE4h7QH2ICEtdxIJH0lzN073q4-300x152.png"" alt=""電子簽證"" width=""100%"" height=""auto"" />&nbsp;</p>
<h3>2. 填寫申請表格：&nbsp;</h3>
<p><img title=""菲律賓電子簽證申請"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a5edefb65391-rytsCnQ9NB7rqrR60WuW83ldn4ogYr-300x182.png"" alt=""菲律賓電子簽證申請"" width=""100%"" height=""auto"" /></p>
<p>&nbsp;</p>
<h3>3.完成手續後依照表格哪規定，依序將護照、緊急聯絡人等等的資訊填入，送出表單。</h3>
<p><img title=""菲律賓簽證手續"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a5edf53b925a-ZtyP2eS3MSDpCg7OcYnS5fJCwUugpG-1701x2211.jpeg"" alt=""菲律賓簽證手續"" width=""100%"" height=""auto"" />&nbsp;</p>
<h3>4.請務必記下編號，在送出申請後，可以直接用此編號查詢資訊。</h3>
<p><img title=""菲律賓簽證"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a5edd132d705-ARAgY7vrpMEH6H2EyGZJFzg28ilavq-300x181.png"" alt=""菲律賓簽證"" width=""100%"" height=""auto"" /></p>"','published' => '1','og_title' => '菲律賓簽證、電子簽證申請一把抓！要去菲律賓遊學，簽證那些大小事！','og_description' => '談到要去菲律賓遊學，多數學生遇到的第一個問題是部隻到該如和處理簽證，通常簽證會交由合作的菲律賓遊學代辦，那麼究竟要如何處遊學簽證呢？今天小編就帶大家來看看遊學簽證該怎麼處理吧！','meta_title' => '菲律賓簽證、電子簽證申請一把抓！要去菲律賓遊學，簽證那些大小事！','meta_description' => '談到要去菲律賓遊學，多數學生遇到的第一個問題是部隻到該如和處理簽證，通常簽證會交由合作的菲律賓遊學代辦，但到了當地如果簽證方面有問題時，除了可以透過學校方面的協助，當然也可以嘗試自己處理，那麼究竟要如何處遊學簽證呢？今天小編就帶大家來看看遊學簽證該怎麼處理吧！','canonical_url' => 'https://tw.english.agency/philippines-visa-application','feature_image_alt' => '菲律賓簽證','feature_image_title' => '菲律賓簽證','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a5ee82356bc1-7FtMAE2wYED58Df1Z5k5AWGCuXBY7j-1200x600.jpeg','author_id' => '1','category_id' => '1','created_at' => '"2018-01-17 05:51:28"'],
  ['id' => '107','title' => '碧瑤遊學Baguio：加強英文，海外遊學｜菲律賓語言學校・遊學推薦','slug' => 'baguio-study-abroad','excerpt' => '想要加強英文、海外遊學卻又擔心荷包撐不住嗎？菲律賓語言學校訓練扎實、氣候舒適、不需要長時間飛行，更重要的是相較於歐美國家，價格經濟實惠許多。說到菲律賓海外遊學，碧瑤語言學校更是一大推薦。「碧瑤」是座知名大學城位於北呂宋島，自然風光加上書香氣息，是個值得推薦的好地方。當地擁有八所高等教育學院，其中兩所更是菲律賓國內排名前10的名校（top10），有些宿霧的教師更是從這裡挖角過去的，碧瑤可以說是師資培育搖籃。','content' => '"<h2><span style=""font-weight: 400;"">【海外遊學英語加強推薦-菲律賓碧瑤語言學校】</span></h2>
<p>想要加強英文、海外遊學卻又擔心荷包撐不住嗎？<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>訓練扎實、氣候舒適、不需要長時間飛行，更重要的是相較於歐美國家，價格經濟實惠許多。說到<a href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>，碧瑤語言學校更是一大推薦。</p>
<p>「碧瑤」是座知名大學城位於北呂宋島，自然風光加上書香氣息，是個值得推薦的好地方。當地擁有八所高等教育學院，其中兩所更是菲律賓國內排名前10的名校（top10），有些<a title=""cebu-city-介紹"" href=""https://zh.wikipedia.org/zh-tw/%E5%AE%BF%E9%9C%A7%E5%B8%82"">宿霧</a>的教師更是從這裡挖角過去的，碧瑤可以說是師資培育搖籃。</p>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p>&nbsp;<img title=""IELTS"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6abb13dfbe1-IYGK4zww4fi6kKi0vRQ4S8P1lNKWJM-800x543.jpeg"" alt=""IELTS"" width=""100%"" height=""auto"" /></p>
<h2><span style=""font-weight: 400;"">【碧瑤語言學校課程豐富扎實-多益托福雅思保分班】</span></h2>
<p><span style=""font-weight: 400;""><a href=""https://zh.wikipedia.org/zh-tw/%E7%A2%A7%E7%91%B6%E5%B8%82"">碧瑤</a>語言學校課程內容扎實，環境舒適，從早到晚都有精心設計的課程，例如知名的<a href=""https://helpenglish.com.tw/"">Help語言學校</a>、<a href=""http://mymonol.com/"">Monol語言學校</a>。<a title=""斯巴達語言學校說明"" href=""https://www.goeducation.com.tw/%E5%AE%BF%E9%9C%A7%E9%81%8A%E5%AD%B8-%E8%8F%B2%E5%BE%8B%E8%B3%93%E8%AA%9E%E8%A8%80%E5%AD%B8%E6%A0%A1-%E6%96%AF%E5%B7%B4%E9%81%94%E9%AB%94%E7%B3%BB%E6%9C%89%E5%93%AA%E5%B9%BE%E9%96%93.html"">斯巴達式</a>與半斯巴達式的教育系統，不論是<strong>多益、托福、雅思、青少年英文、商業英文、學齡前英語以及親子英語課程</strong>都有完善的規劃。在評估學習需求後，依照能力分班進行一對一聽、書、讀、寫專攻課程；文法、單字加強訓練；團體時間能夠和來自世界各的遊學生做英語練習與文化交流。其中也有多益、托福、雅思保分班，讓你英文程度突飛猛進。</span></p>
<p><span style=""font-weight: 400;"">&nbsp;<img title=""碧瑤遊學學校"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6aba79ec779-v3ftPNXEhMbWYO7cymxpzChIn2WrXD-900x300.png"" alt=""碧瑤遊學學校"" width=""100%"" height=""auto"" /></span></p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">【青山環繞的大學城-碧瑤天氣涼爽舒適】</span></h2>
<p><span style=""font-weight: 400;"">來碧瑤遊學除了語言課程外，當地的自然風光與人文藝術也是不能錯過的！碧瑤是個人文薈萃的大學城，被翠綠的青山環繞，有別於印象中那個只有海灘的菲律賓，氣候涼爽宜人，全年氣溫12-25度，暑假期間平均氣溫更是舒適的18.6度。</span></p>
<p>&nbsp;&nbsp;</p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">【旅遊勝地碧瑤結合人文藝術與自然風光】</span></h2>
<p><span style=""font-weight: 400;"">碧瑤當地有許多特色景點，讓你在學習之餘也能感受當地文化風情：結合自然與原住民藝術的Tam-Awan Vilage天空中的花園-藝術文化村；集古代與當代藝術於一身的<a href=""http://www.bencabmuseum.org/"">Bencab Museum</a>；讓你同時享受陽光、山城、自然的<a href=""https://www.explorebaguio.com/places/sunshine-park/"">Baquio Sunshine Park</a>碧瑤日落公園；鳥瞰碧瑤市風光的礦山景觀公園。在距離碧瑤車程5小時左右的鄰近城市薩加達也有許多不可錯過的景點：蘇馬晶洞與龍眠洞的鐘乳石和洞穴潛水；印在1000比索鈔票上的巴拿威梯田。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">&nbsp;<img title=""Baugio-city"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6aba6acfc08-URoK6pXU5sLqqMjJk6aq8htHh2HiXS-1000x508.png"" alt=""Baugio-city"" width=""100%
"" height=""auto"" /></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">【邊學邊玩兼避暑】</span></h2>
<p>碧瑤的書香氣息加上壯麗的自然風光，讓你不只語言進步，更能深入當地文化，探索不一樣的菲律賓！不論你是想加強英語能力的學生、剛出社會的新鮮人、轉職進修需要或者是想來場親子學習度假，想要邊學習、邊旅遊、邊避暑，來碧瑤遊學、碧瑤語言學校準沒錯！</p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '碧瑤遊學Baguio：加強英文，海外遊學｜菲律賓語言學校・遊學推薦','og_description' => '想要加強英文、海外遊學卻又擔心荷包撐不住嗎？菲律賓語言學校訓練扎實、氣候舒適、不需要長時間飛行，更重要的是相較於歐美國家，價格經濟實惠許多。說到菲律賓海外遊學，碧瑤語言學校更是一大推薦。「碧瑤」是座知名大學城位於北呂宋島，自然風光加上書香氣息，是個值得推薦的好地方。當地擁有八所高等教育學院，其中兩所更是菲律賓國內排名前10的名校（top10），有些宿霧的教師更是從這裡挖角過去的，碧瑤可以說是師資培育搖籃。','meta_title' => '碧瑤遊學Baguio：加強英文，海外遊學｜菲律賓語言學校・遊學推薦','meta_description' => '想要加強英文、海外遊學卻又擔心荷包撐不住嗎？菲律賓語言學校訓練扎實、氣候舒適、不需要長時間飛行，更重要的是相較於歐美國家，價格經濟實惠許多。說到菲律賓海外遊學，碧瑤語言學校更是一大推薦。「碧瑤」是座知名大學城位於北呂宋島，自然風光加上書香氣息，是個值得推薦的好地方。當地擁有八所高等教育學院，其中兩所更是菲律賓國內排名前10的名校（top10），有些宿霧的教師更是從這裡挖角過去的，碧瑤可以說是師資培育搖籃。','canonical_url' => 'https://tw.english.agency/baguio-study-abroad','feature_image_alt' => '碧瑤遊學Baguio-加強英文，海外遊學｜菲律賓語言學校・遊學推薦','feature_image_title' => '碧瑤遊學Baguio-加強英文，海外遊學｜菲律賓語言學校・遊學推薦','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ace741821e-oGpMm6T2NgpCQ60AW94MTjYqHqKIBz-1000x508.jpeg','author_id' => '15','category_id' => '1','created_at' => '"2018-01-22 09:36:44"'],
  ['id' => '108','title' => '"善用托福紙筆測驗，預先掌握題型 – 托福滿分達人分享讀書計畫"','slug' => '善用托福紙筆測驗-預先掌握題型-托福滿分達人分享讀書計畫','excerpt' => '如何有效率地在有限的時間內考到托福高分呢？我的準備時間表要如何安排？其實沒有你想像中的困難，快來看看有哪些3大準備重點！','content' => '"<h1>你不可不知的TOEFL iBT 托福網路測驗3大準備重點</h1>
<p>如何有效率地在有限的時間內考到托福高分呢？我的<strong>準備時間表</strong>要如何安排？其實沒有你想像中的困難，抓緊3大準備重點：精進聽力、訂定目標分數、擬定讀書計畫，與加強<strong>托福背景知識</strong>，善用學習資源與練習題，不僅事半功倍，大幅縮短<strong>托福準備</strong>時間，還能增加拿到心目中理想分數的機會。首先，先來認識一下托福網路測驗TOEFL iBT的考試內容。</p>
<h2>到底托福在考什麼？我的讀書計畫如何開始？</h2>
<p><a href=""http://www.toefl.com.tw/campaign2016/iBT90/"" target=""_blank"" rel=""noopener noreferrer"">托福網路測驗</a>（以下簡稱TOEFL iBT）的<strong>考試內容</strong>依順序共有<strong>閱讀</strong>、<strong>聽力</strong>、<strong>口說</strong>及<strong>寫作</strong>等四項測驗內容，考試時間約為4 小時，測驗架構如下： <img class=""aligncenter wp-image-215 size-full"" src=""http://www.toeflworld.com.tw/wp-content/uploads/2015/02/未命名3.png"" alt=""托福考試時間"" width=""700"" height=""354"" /> &nbsp;</p>
<h2>掌握聽力 + 訂下目標?+ 高效率讀書計畫 = 節省你的備考心力與時間</h2>
<p>TOEFL<strong> iBT滿分</strong>達人 羽角俊之老師分享準備<a href=""http://www.toefl.com.tw/campaign2016/iBT90/"" target=""_blank"" rel=""noopener noreferrer"">TOEFL iBT?</a>的3大重點，如果大家在備考的時候能掌握並有效運用，拿下高分其實並不難，也能更有效率分配<strong>準備口說</strong>、寫作、聽力與閱讀4大考試項目的時間，趕快一起來看看是哪3大重點：</p>
<p><strong><span style=""color: #ff9900;"">重點１　精進聽力技能</span></strong> 許多學生覺得TOEFL iBT的口說或寫作測驗很難，但其實四項技能中最難的是「聽力」。由於聽力測驗是需要先把整段題目聽完再作答、且聽力測驗又在閱讀測驗之後，許多考生會因此搞混。再者，聽力是最需要花時間增強的一項技能，因為除了要在聽到的當下立即抓到題目的重點，還有單字的理解力─能在聽到單字的瞬間知道這個單字的所有涵義。所以，聽力能力越早開始培養越好。</p>
<p><strong><span style=""color: #ff9900;"">重點２　訂好目標分數</span></strong> 若把TOEFL iBT的目標訂在80分來看，在台灣施測的學生（含外籍在台學生）在2013年的平均是聽說讀寫各得20分。但就台灣籍學生而言，分數分別為閱讀20分、聽力18分、口說19分與寫作20分，共77分。總體來說，台灣籍學生的聽力最差，閱讀及寫作最高。 這個情況是由於在台灣的教育體制中，國中英文著重於文法、高中則加強單字，造成考生文法與單字非常好、聽力卻無法高分的結果。但台灣欠缺英語口說的環境，而若長久欠缺口說訓練的情況下，對英語聽力的訓練自然會降低。</p>
<p><span style=""color: #ff9900;""><strong>重點３　擬定備考計畫</strong></span> 在準備TOEFL iBT的過程中共能分成三個步驟：</p>
<p><strong>STEP 1 透過托福紙筆測驗（TOEFL ITP）建立自我認知</strong> 建議學生可從最基本的聽力與閱讀測驗了解自身的程度在哪，進而對症下藥，找出對的方法來補強。至於口說及寫作測驗，學生可以透過基本的文法來設定進步指標，而這些都能透過托福紙筆測驗（以下簡稱TOEFL ITP）了解自我英語能力。 托福紙筆測驗（TOEFL ITP）是TOEFL Family一員，題目設計與TOEFL iBT的「難度」是一樣的，但「長度」只有一半，是學生Pre-iBT最佳檢測，且價格較TOEFL iBT便宜。更重要的是，TOEFL ITP的聽力測驗也會要求考生在聽完整段題目後再作答，閱讀則是分段針對單題作答，作答時間也與TOEFL iBT的作答時間最相近，整個測驗架構是與TOEFL iBT類似的！</p>
<p><strong>STEP 2 達到門檻再考 TOEFL iBT</strong> 針對已施測完TOEFL ITP的學生來說，測驗成績至少需達500分以上再去考TOEFL iBT，比較不會受花了大錢卻得不到理想成績的不利結果。 但是，TOEFL ITP只是一個準備TOEFL iBT 的入門測驗，並不是TOEFL ITP測驗考得好，TOEFL iBT就一定會考高分。就分析來看， TOEFL ITP的500分當中，聽力至少需要50~55分、文法結構需50~55分、閱讀則需要55~58分。</p>
<p><strong>STEP 3 針對單項較弱的技能加強</strong> 閱讀：使用TOEFL ITP 相關書籍、多方閱讀學術相關文章，都是訓練閱讀速度的方式。 聽力：需要長時間不間斷地聽英文，尤其聽學術方面教授的授課內容，從中聽到關鍵字並做筆記，這能同時訓練聽力與做筆記的能力。 文法結構：需要熟悉文法，找出較常犯錯的地方並加以修正，正確使用文法也能讓口說更加流暢。 若以上的流程都完整準備好了，在準備應考之前一定要先去考TPO（托福官方線上模擬測驗）。因為藉由TPO的模擬考題，學生不但能更熟悉考試題型，也能習慣整個考試流程。更可藉由這個機會來自我訓練專注力。建議練習TPO可從3個月為一階段，一天3到4次、一次10到15分鐘的學習成效最佳。考前3週建議天天做一套TPO，測驗當日方能快速進入狀況，避免因不適應狀況而失分。</p>
<p style=""text-align: right;"">(以上節錄自《<a href=""http://www.toeflworld.com.tw/"">托福世界誌</a>》第一期 P.30 〈善用托福紙筆測驗作為考試入門 TOEFL iBT滿分達人分享備考計畫〉)</p>
<h2>大家都在讀的<strong>托福必備書</strong>單</h2>
<p>書店裡跟<a href=""http://shan815322.pixnet.net/blog/post/197609551-%E6%88%91%E7%9A%84%E4%BA%A4%E6%8F%9B%E7%94%9F%E5%A4%A2%E6%83%B3---%E6%89%98%E7%A6%8F%E6%BA%96%E5%82%99%E6%9B%B8%E7%B1%8D%E5%92%8C%E5%AF%A6%E7%94%A8%E7%B6%B2%E7%AB%99%E5%BB%BA""><strong>托福準備相關的書</strong></a>起碼有上百本，再加上網路上可以使用的資源與工具，讓人眼花撩亂，不過很多老師與網友們提供了3個準備托福必買的工具書跟超實用的線上資源：</p>
<p>1.Official TOEFL iBT&reg; Tests 托福網路測驗練習試題 這套有第一冊、第二冊總共有2本，如果你不想補習，選擇用自修的方式來準備托福的話，一定要好好讀個精熟！每冊裡面有5套TOEFL iBT測驗歷年試題的練習試題，包括實際聽、說、讀、寫題型。每套練習試題均包含解答、口說與寫作答題範本，以及完整的聽力文本。每一冊隨書都附有DVD光碟，內含5套互動式電腦試題版本，也有提供聽力部分的音源檔案讓讀者可以用書籍作聽力的準備。</p>
<p>2.TOEFL&reg; Practice Online 托福線上全真模擬測驗 這個就是大家常常提到的TPO，它就像是線上的模擬考，跟托福網路測驗一樣，TPO有「閱讀」、「聽力」、「寫作」及「口說」四個測驗項目的題目，對於<a href=""http://www.toeflworld.com.tw/%E6%89%98%E7%A6%8F%E6%BA%96%E5%82%99/"">準備托福</a>的口說很實用，TPO真實的模擬了參加托福網路測驗會遇到的題目。我們考生藉此了解到時候參加考試會遇到的題型，同時熟悉一下測驗的介面與操作，是參加正式托福網路測驗前，測出最接近真正實力的模擬考。</p>
<p>3. Official Guide to the TOEFL ITP Test 準備從托福紙筆測驗入門，把它當作TOEFL iBT備考計畫之一的考生們，可以用這本來好好做練習題，而且裡面也包含了介紹托福紙筆測驗題型、出題要點及應答策略，並針對所有題型提供大量範例試題，幫助你累積應考技巧並加強聽力、閱讀與文法的學術英語能力。裡面也收錄兩套完整的TOEFL ITP全真模擬試題，考前體驗最真實的模擬測驗。 &nbsp;</p>
<p>&nbsp;</p>
<p><strong>準備托福</strong>的所有功夫要融入在日常生活當中，平時就開始有意識的練習聽力與閱讀，訂下自己想要達成的分數目標，規劃一個適合自己生活作息的讀書計畫，加上幾本托福必備工具書，準備托福網路測驗其實沒有想像中的難以著手；而且，如果真的覺得一開始就準備4個測驗項目很緊張的話，可以先從托福紙筆測驗下手，先把基本盤的聽力與閱讀能力穩固好，再來衝刺口說跟寫作，循序漸進，既經濟又有效率。總之，事先瞭解考試內容，做好合理的時間規劃，集中火力作好托福測驗的準備，再上戰場會更有信心，更有機會考到心目中的理想分數。 &nbsp;</p>"','published' => '1','og_title' => '"善用托福紙筆測驗，預先掌握題型 – 托福滿分達人分享讀書計畫"','og_description' => '如何有效率地在有限的時間內考到托福高分呢？我的準備時間表要如何安排？其實沒有你想像中的困難，快來看看有哪些3大準備重點！','meta_title' => '"善用托福紙筆測驗，預先掌握題型 – 托福滿分達人分享讀書計畫"','meta_description' => '如何有效率地在有限的時間內考到托福高分呢？我的準備時間表要如何安排？其實沒有你想像中的困難，快來看看有哪些3大準備重點！','canonical_url' => 'http://www.toeflworld.com.tw/%E6%89%98%E7%A6%8F%E6%BA%96%E5%82%99/','feature_image_alt' => 'TOEFL','feature_image_title' => 'TOEFL','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ac5d158897-t2JFxxk2YdNEFvLjPDstxEFP3X7Mwy-1280x853.jpeg','author_id' => '12','category_id' => '15','created_at' => '"2018-01-26 06:09:04"'],
  ['id' => '109','title' => '"從勞基法學多益 你知道補休英文怎麽説嗎？"','slug' => '從勞基法學多益-你知道補休英文怎麽説嗎','excerpt' => '想要跟外籍人士討論勞基法，但不知道如何開始嗎？一起從法律修訂、工時計算、相關福利三大方面，介紹一些有關勞基法的英文單字開始吧！','content' => '"<p>近來勞基法修正案已經三讀通過，若你的公司是在臺灣的外商或同事中有外籍人士，在聊天時很有可能會提到這個話題。就讓我們帶大家從法律修訂、工時計算、相關福利三大方面，介紹一些有關勞基法的英文單字，讓你能夠與外籍同事談論這件事，也可以學會一些<a href=""http://www.toeic.com.tw/"" target=""_blank"" rel=""noopener noreferrer"">多益</a>英文單字。</p>
<h2><strong><span style=""color: #a42d00;"">一、法律修訂</span></strong></h2>
<p>從字根legal延伸出跟修法有關的字，包括有立法legislate、立法委員legislator、與立法程序legislation。因為臺灣施行五院分立的制度，所以立法「院」的英文也就俗稱為Legislative Yuan。legal作為字根的意思是法律的，作為單字則變成合法的，不合法則直接改為illegal。另外，修法通過的英文說法為legislature has been cleared。</p>
<p>法律修訂的正式用語為amendment，amend原意為修補，而若要使用動詞，修改法律就是amend/revise the law。此外，正式的修正案通常會在前方加上數字表示以為區分，例如美國憲法中的第二修正案就叫做Second Amendment。</p>
<p>在法律修訂中，最擔心的就是會有人鑽法律漏洞。法律上的漏洞不能使用leak來表示，因為leak指的是實體事物的漏水情況或是資訊上的洩漏，所以法律條文上的漏洞會使用loophole這個字，而loophole也可用於軟體或是硬體的安全漏洞security loophole。</p>
<p><strong>例句：</strong><br />If the company identified the loopholes on chips earlier，the hackers would not be able put a backdoor on the laptop.<br />若這間公司早點發現晶片上的安全漏洞，駭客就無法在筆電上安裝後門程式了。</p>
<p><strong><span style=""color: #000080;"">多益模擬測驗</span></strong></p>
<p>在TOEIC中由於不會有負面場景，所以amendment與loophole都常被用在員工手冊或軟體授權條軟上的修訂。</p>
<p>1. Please kindly check the ______ in the new version of employee rulebook. Some changes will lead to major differences.<br />(A) amendments<br />(B) adjournments<br />(C) advertisements<br />(D) assessments</p>
<h3><strong><span style=""color: #a42d00;"">二、工時計算</span></strong></h3>
<p>這次勞基法的修訂中最多人討論的就是加班彈性，加班英文為overtime。</p>
<p><strong>例句：</strong><br />Please start dinner without me. I need to work overtime tonight.<br />請別等我吃晚餐，我今晚需要加班。</p>
<p>由於一般時薪與薪資的口語說法為pay，故時薪即為hourly pay/wage，與overtime結合在一起，加班費就變成了overtime pay。</p>
<p><strong>例句：</strong><br />The wage for the overtime work will be paid biweekly.<br />加班的費用將會雙週給付一次。</p>
<p>這次的修法還有一個重點，那就是加班費的計算方式。原本加班費是以每四個小時算一次blocks of 4 hours，block的意思為區塊，即是以多大的範圍做為計算標準，而近期很紅的比特幣所使用的運算方式區塊鏈（blockchain）也是使用這個字。</p>
<p>修訂後的加班費改以實際工時actual number of work hours計算，actual的意思是確切的、真實的。而加班從第三個小時開始from the third hour onward就以1.66倍計算，onward的意思為從這個時間點開始就會這樣處理。而加班上限從每月46小時調整到54小時，在這裡上限並不會使用limit，而會使用cap，故overtime cap意思就是加班上限了。</p>
<p>除了政府單位，勞資雙方也就勞基法進行許多的協商會議，這樣的會議稱為employer-staff meetings。而勞資雙方為了達成共識reach a consensus，需要的就是取得員工同意gain the consent of the employees。</p>
<p><strong><span style=""color: #000080;"">多益模擬測驗</span></strong></p>
<p>在TOEIC的考題中，由於人事場景出現的比例極高，所以pay與wage等字都很常見，最後的「達成共識」說法，由於閱讀題中有時會有談判或公司間簽約的場景，所以也常常可以看到。</p>
<p>2. ______ to reach a consensus，the two parties in the negotiation decided to call it off for the day.<br />(A) Failed<br />(B) Failing<br />(C) To fail<br />(D) Fail</p>
<h3><strong><span style=""color: #a42d00;"">三、相關福利</span></strong></h3>
<p>休假的英文名詞是leave，所以請假就是take a ____leave就是請___假，例如sick leave就是病假、maternity leave就是產假。在這次的修法中，補休也是一個討論度很高的話題，而補休的英文為compensatory holidays/leaves，這邊所使用的「補償性」是由「補償」的動詞compensate而來。</p>
<p>法律修改後，勞工沒有用完的補休可以轉換成薪資，因為轉換後的事物與原本的事物不同，故使用convert這個字。</p>
<p><strong>例句：</strong><br />The company must convert the unused compensatory holidays into overtime pay.<br />公司必須將未使用的補休轉為加班費。</p>
<p><strong><span style=""color: #000080;"">多益模擬測驗</span></strong></p>
<p>在TOEIC中，客訴是在閱讀部分中克漏字與閱讀測驗超級常見的場景，所以就會出現賠償客戶損失（compensate for the loss）的用法。而在TOEIC中最常配合leave出現的搭配詞為有薪假paid leaves，相關搭配用法如下題所示。</p>
<p>3. Employees who work for more than 2 years will be _____ for paid leaves.<br />(A) omitted<br />(B) embarked<br />(C) eligible<br />(D) transferred</p>
<p><strong>解析：</strong><br />1. 題意為「煩請記得查閱新版員工守則的『修訂』，許多變革都會導致大量差異。 」<br />由後方的change便知手冊有所修改，故答案為(A)。(B)為散會、(C)為廣告、(D)為評估，皆與語意不合。</p>
<p>2. 題意為「____達成共識，談判雙方決定今天先到此為止。」Fail to表示「沒能達成&hellip;」，兩句間沒有連接詞，前方句又沒有主詞，所以是分詞構句。分詞構句中，主動句要使用現在分詞，被動句使用則用過去分詞。前方的句子原本應為The two parties in the negotiation failed to reach a consensus。以主動句表達，所以要使用現在分詞ing，故答案為(B)。</p>
<p>3. 題意為「年資超過兩年的員工就（有資格）享有有薪假。」這題只要前後文讀懂就能確認「有資格」的意義。所有選項中只有(C)的be eligible具有「擁有某種資格」的意思，故答案為(C)。(A)為刪除、(B)為出發、(D)為移轉，皆與語意不符合。</p>
<p>不論你對這次修法的看法如何，學會這些字可以讓你跟外籍同事或朋友就這件事侃侃而談，其中有不少字在進入職場後也會經常使用，所以把這些單字學起來，對你的職場生活絕對有幫助。</p>
<p>文／李海碩</p>
<p>本文由《<a href=""https://goo.gl/STvFmz"">TOEIC OK News多益情報誌</a>》提供，未經授權請勿任意轉貼節錄。</p>"','published' => '1','og_title' => '"從勞基法學多益 你知道補休英文怎麽説嗎？"','og_description' => '想要跟外籍人士討論勞基法，但不知道如何開始嗎？一起從法律修訂、工時計算、相關福利三大方面，介紹一些有關勞基法的英文單字開始吧！','meta_title' => '"從勞基法學多益 你知道補休英文怎麽説嗎？"','meta_description' => '想要跟外籍人士討論勞基法，但不知道如何開始嗎？一起從法律修訂、工時計算、相關福利三大方面，介紹一些有關勞基法的英文單字開始吧！','canonical_url' => 'http://www.toeicok.com.tw/%E6%99%82%E4%BA%8B-%E5%BE%9E%E5%8B%9E%E5%9F%BA%E6%B3%95%E5%AD%B8%E5%A4%9A%E7%9B%8A-%E4%BD%A0%E7%9F%A5%E9%81%93%E8%A3%9C%E4%BC%91%E8%8B%B1%E6%96%87%E6%80%8E%E9%BA%BD%E8%AA%AC/','feature_image_alt' => '勞基法學多益','feature_image_title' => '勞基法學多益','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6acedb9ee07-poUGqCjinaKlXXUUIBhectDyJgKp2D-1024x683.jpeg','author_id' => '9','category_id' => '19','created_at' => '"2018-01-26 06:47:31"'],
  ['id' => '110','title' => 'CELC語言學校宿霧特色與評價，多語言課程、斯巴達課程專業學校！','slug' => 'celc-language-school-comment','excerpt' => '菲律賓遊學有許多選擇，除了有克拉克遊學、宿霧遊學之外，碧瑤遊學，也是相當不錯的選擇，而菲律賓的碧瑤是一個相當不錯地方，這裡有很多的美景之外，同還是是人文薈萃大學城，今天小編就帶家來看看這所美麗的學校吧！','content' => '"<p>菲律賓遊學有許多選擇，除了有克拉克遊學、宿霧遊學之外，碧瑤遊學，也是相當不錯的選擇，而菲律賓宿霧是個文化大熔爐，這裡有很多的美景、海島風情，同時也是菲律賓語言學校的集中站，今天就跟小編來看看這所CELC語言學校吧！</p>
<p><a href=""http://www.ugophilippines.com/?p=2011&amp;variant=zh-cn"">（Ccebu English Language Center 語言學校官網）</a></p>
<p><img title=""宿霧市"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6adf501b752-nEYd8fJd4IL4ZBprpboh2xvr6MxVcB-1024x550.jpeg"" alt=""宿霧市"" width=""100%"" height=""auto"" />&nbsp;</p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">CELC語言學校環境介紹-</span></h2>
<h3>可度假、可扎實學習 宿霧市的高英語水平教育</h3>
<p><span style=""font-weight: 400;""><a href=""https://en.wikipedia.org/wiki/Cebu_City"">宿霧市（Cebu City）</a>是菲律賓宿霧省的首府，也是菲律賓第二大城。宿霧市位於宿霧島北岸，是首都馬尼拉也比不上的歷史最悠久之城，早在16世紀初即有許多活動出現，更有葡萄牙航海家麥哲倫的足跡，因而成為菲律賓的文化之都，同時也是菲律賓主要國際航線的往來地。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">宿霧市是宿霧大都會的一部分，以休閒海灘、天然景色聞名於世，海產豐富。當地共計超過120所政府認證學校，是眾多日、韓遊學生目的地。伴隨著美麗的海灣風景，和穩扎穩打的務實訓練，玩樂時輕鬆、學習時嚴謹，也可說是菲律賓遊學的最大優點。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">CELC語言學校特色</span></h2>
<p><span style=""font-weight: 400;"">✓CELC語言學校成立於2007年，由一對韓籍年輕兄弟開辦，在CELC能跟佔校內大多數的日韓學生進行互動。除了語言能力提升，在此見識到的國際交流更是珍貴。</span></p>
<p><span style=""font-weight: 400;"">✓CELC校內可容納約130位學生，校內有講究的米飯類、韓食餐點，除了嚴謹扎實的斯巴達英文課程外，還有打工度假英文、韓文課可選。</span></p>
<p><span style=""font-weight: 400;"">✓適合想找尋「平價密集的優質學習」、「順便學韓文&amp;與日韓學生交流」、「計畫打工度假」、「計劃留學」的學生族群。</span></p>
<p>&nbsp;</p>
<p><img title=""CELC"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ae04776f89-4QXVxJD58roNfetqUUMd7rSWWDk2rw-1666x914.png"" alt=""CELC"" width=""100%"" height=""auto"" />&nbsp;</p>
<h2><span style=""font-weight: 400;"">CELC語校師資</span></h2>
<ol>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">熱情友善的老師：CELC語校嚴謹甄選優良師資，每位老師都能輕鬆帶領教學氣氛，引發學生的學習熱忱，尤其對害羞開口的學生，在CELC老師能訓練你自信地說英文。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">實力堅強的口說英文與商用英文師資：CELC語校的口說英文與商用英文師資，皆擁有國家教師證照。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">95%的老師在CELC待超過3年以上：不似其他學校聘用兼任教師，CELC堅持聘用教學經驗豐富的全職英文教師，也因95%以上師資皆待超過3年，優秀的教學品質得以持續維持。</span></li>
</ol>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">CELC語言學校 課程安排</span></h2>
<table>
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: center;"" colspan=""2"">
<p><span style=""font-weight: 400;"">CELC語校的課程設計（每日10-11小時）</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">斯巴達ESL速成課程</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">4小時ESL一對一 + 4小時ESL團體 + 2小時特別課</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">斯巴達商用英文課程</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">4小時ESL一對一 + 4小時商用英文團體 + 2小時特別課</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">斯巴達雅思課程</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">5小時雅思一對一 + 1小時雅思團體 + 3小時ESL團體 + 2小時特別課</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">斯巴達強力口說課程</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">5小時ESL一對一 + 3小時強力口說團體 + 1小時ESL團體 + 2小時特別課</span></p>
</td>
</tr>
</tbody>
</table>
<p><span style=""font-weight: 400;"">＊4小時ESL一對一=聽說讀寫各一小時</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">結業學生對CELC的評價</span></h2>
<p><span style=""font-weight: 400;"">Tom/男，斯巴達ESL速成課程：</span></p>
<p><span style=""font-weight: 400;"">我是一名會計師，來宿霧學英文之前，是一個完全不敢開口說英文的人。不過在三個月斯巴達CELC語校的洗禮之下，我現在可以自信地講英文，這是三個月來最大的收穫。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">當時我選擇CELC的四大理由：</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">(1)斯巴達式管理，週一至週四皆無法離開校園，能待在學校認真學習</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">(2)每週上課時數長，幾乎是歐美語言學習的兩倍。我休息的ESL課程週一到週五有扎實的8堂課，晚上10-11堂還能選擇額外的英文團體課，讓我能全心投入學英文</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">(3)CELC價格低廉，幾乎是歐美遊學的減半再打折，有預算考量又想在短時間之內進步的人，宿霧CELC語言學校是你的最佳選擇</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">(4)CELC嚴格執行EOP(English Only Policy，只說英文的政策)，目前在校內的台灣人相遇時都直接說英文，這項政策也幫助我從不敢開口到輕鬆講英文。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">Shawn /女，斯巴達強力口說課程：</span></p>
<p><span style=""font-weight: 400;"">升高三那年暑假，媽媽幫我安排了在CELC的八週課程。因為語校的規定，在這裡我變得很敢說英文，而我的韓國室友也讓我必須用英文與她們溝通。</span></p>
<p><span style=""font-weight: 400;"">回到台灣上課時，我的英文老師告訴媽媽：「Shawn的英文進步很多，每天還主動交一篇英文作文給我，上英文課時還當我的小助手即時翻譯。」</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">我的表現讓媽媽和老師都很高興，老師還跟媽媽說：「讓Shawn到宿霧CELC學英文的錢沒有白花！」</span></p>
<p><br /><br /></p>"','published' => '1','og_title' => 'CELC語言學校宿霧特色與評價，多語言課程、斯巴達課程專業學校！','og_description' => 'CELC語言學校宿霧特色與評價，多語言課程、斯巴達課程專業學校！','meta_title' => 'CELC語言學校宿霧特色與評價，多語言課程、斯巴達課程專業學校！','meta_description' => '菲律賓遊學有許多選擇，除了有克拉克遊學、宿霧遊學之外，碧瑤遊學，也是相當不錯的選擇，而菲律賓的碧瑤是一個相當不錯地方，這裡有很多的美景之外，同還是是人文薈萃大學城，今天小編就帶家來看看這所美麗的學校吧！','canonical_url' => 'https://tw.english.agency/celc-language-school-comment','feature_image_alt' => 'CELC語言學校','feature_image_title' => 'CELC語言學校','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ade83dcd6d-wqmsWfJke5ZyMOYy0JcTQX5zPgwLmi-1000x508.jpeg','author_id' => '1','category_id' => '1','created_at' => '"2018-01-26 07:54:12"'],
  ['id' => '111','title' => '"Howdy 語言學校特色與評價：日式優質學校，日商企業合作品牌學校！"','slug' => 'howdy-english-academy','excerpt' => '要去菲律賓遊學，如何挑選適合的菲律賓語言學校是一個大問題，而如果你是喜歡自主式管理的朋友，那麼Howdy語言學校絕對是你可以考慮的好學校，採用日式的管理風格以及高規格的設備，同時還是日本企業的指定英語訓練學校，今天就跟小編已起來看看這所優質的宿霧語言學校吧！','content' => '"<p>要去<a href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>，如何挑選適合的<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>是一個大問題，而如果你是喜歡自主式管理的朋友，那麼<a href=""https://tw.howdyenglish.asia/"">Howdy語言學校</a>絕對是你可以考慮的好學校，採用日式的管理風格以及高規格的設備，同時還是日本企業的指定英語訓練學校，今天就跟小編已起來看看這所優質的宿霧語言學校吧！</p>
<p>&nbsp;</p>
<h1>Howdy語言學校環境-</h1>
<h3>鄰近宿霧國際機場 遠離鬧區 治安良好</h3>
<p><span style=""font-weight: 400;"">Howdy English語言學校距離<a href=""http://mciaa.gov.ph/"">麥克坦-宿霧國際機場</a>15~20分鐘車程，與市區中心也相隔不遠。因遠離鬧區，所以治安相當良好，周邊更設有購物中心，機能及交通都相當便利。</span></p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">Howdy語言學校特色</span></h2>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>設備整潔完善</strong>：Howdy語言學校成立於2014年，從餐廳、教室到宿舍，都相當整潔乾淨。建築內的家具皆為訂做，除Wi-Fi設施完善外，也提供免治馬桶等，提供學生最高水準的設備。</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>校內飲食備受稱讚</strong>：Howdy校內可容納約142位學生，校內有由日本主廚掌廚的日式料理，包含講究米飯類、充足蔬菜，被學生盛讚為比餐廳還好吃的學校食堂。</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>比鄰購物中心</strong>：本校隔壁設有名為Insular Square的購物中心，除生鮮超市以外，也有餐廳、咖啡廳、藥局、美容院、健身房及外幣收兌處等店家，十分便利。</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>課程安排相當多元彈性、有最適合亞洲的英語學習法之稱</strong>：適合多種族群，包括想找尋「平價密集的優質學習」、「輕鬆的課程安排（親子或長青族）」、「外商外派等商務進修」等各種族群前往。</span></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">Howdy語校師資及課程特色</span></h2>
<ol>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>「最適合亞洲的英語學習法」</strong>：Dr.Tampus以教育管理學博士的身分，在宿霧地區研究所執教的同時，也因應菲律賓政府教育省的要求，成為世界上專精於教育學、語言學研究的第一人。為提升學生學習意欲，也設計出許多獨特的課程計畫。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>一對一授課的優勢</strong>：在一對一授課中，您可修習 HOWDY 高水準講師群的課程。從初學者到高階學習者，皆可依照自己的步調集中學習，也可以根據會話、文法、發音等各種個人需求調整課程進度。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>親子共學課程</strong>：因生活環境及治安良好，Howdy語校親子留學方案相當受歡迎。校內備有兒童專屬教室，依年齡及程度提供不同的原創課程，同時間便能進行父母的學習課程，成為近年親子出國度假的另一種選項。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>商業課程</strong>：除了針對學生外，也有提供商務人士專屬課程，針對電話、信件、會議及簡報技巧等不同場景的變化，安排實際的學習演練。</span></li>
</ol>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ae5e974b50-HIywHEUZXl8JmdPj8cSiMsqcUn5MIC-720x400.jpeg"" alt="""" width=""720"" height=""400"" /></span></p>
<h2><span style=""font-weight: 400;"">Howdy語校課程安排</span></h2>
<table>
<tbody>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">標準課程方案</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 4堂＋團體課 2堂</span></p>
<p><span style=""font-weight: 400;"">以最正統的方案，平衡學習各項語言能力。</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一課程方案</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 7堂</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">推薦給對團體授課抱有不安，或是想利用短期留學集中學習的您。</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">Enjoy 方案</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 3堂＋團體課 1堂</span></p>
<p><span style=""font-weight: 400;"">一天僅4堂課程，能一邊學習一邊享受宿霧島的生活。</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">親子留學方案</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">1-15歲，最多可聽講6堂課</span></p>
<p><span style=""font-weight: 400;"">為Howdy語校特有的課程設計，可讓1歲起的兒童一同參加。</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">結業學生對HOWDY English語言學校的評價</span></h2>
<p><strong>29歲 男性，商務課程方案：</strong></p>
<p><span style=""font-weight: 400;"">所屬公司因國外出差機會多，為進修語言而來到宿霧。</span></p>
<p><span style=""font-weight: 400;"">歸國後依據留學成果來評價，終於被調派至憧憬已久的國外分公司。</span></p>
<p>&nbsp;</p>
<p><strong>33歲 女性，親子共學方案：</strong></p>
<p><span style=""font-weight: 400;"">暑假和女兒兩個人一起前往親子留學，女兒似乎變得對英語學習相當樂在其中，就連在家裡開始也使用英語來對話了。</span></p>
<p>&nbsp;</p>
<p><strong>60歲 女性，50歲以上折扣方案：</strong></p>
<p><span style=""font-weight: 400;"">我找到除了觀光旅遊外，另一種享受海外生活的方法，每天都過的非常開心，平日讀書、週末出遠門，沒想到除了走馬看花式的觀光外，還有這種海外生活方法。我的留學生活不但過得非常愉快，還非常充實！</span></p>
<p>&nbsp;</p>
<p><strong>27歲 男性，一對一課程方案：</strong></p>
<p><span style=""font-weight: 400;"">為了準備 1 年後的打工度假，因而留學從基礎開始學習英語。</span></p>
<p><span style=""font-weight: 400;"">雖然1天9節的一對一授課剛開始有些辛苦，但也因此在打工度假期間找到了理想的工作。</span></p>"','published' => '1','og_title' => '"Howdy 語言學校特色與評價：日式優質學校，日商企業合作品牌學校！"','og_description' => '要去菲律賓遊學，如何挑選適合的菲律賓語言學校是一個大問題，而如果你是喜歡自主式管理的朋友，那麼Howdy語言學校絕對是你可以考慮的好學校，採用日式的管理風格以及高規格的設備，同時還是日本企業的指定英語訓練學校，今天就跟小編已起來看看這所優質的宿霧語言學校吧！','meta_title' => '"Howdy 語言學校特色與評價：日式優質學校，日商企業合作品牌學校！"','meta_description' => '要去菲律賓遊學，如何挑選適合的菲律賓語言學校是一個大問題，而如果你是喜歡自主式管理的朋友，那麼Howdy語言學校絕對是你可以考慮的好學校，採用日式的管理風格以及高規格的設備，同時還是日本企業的指定英語訓練學校，今天就跟小編已起來看看這所優質的宿霧語言學校吧！','canonical_url' => 'https://tw.english.agency/howdy-english-academy','feature_image_alt' => 'Howdy語言學校','feature_image_title' => 'Howdy語言學校','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ae4f5ea85c-CQd6yfrubcHBWrknioKckxlpauKHcE-1000x508.jpeg','author_id' => '16','category_id' => '1','created_at' => '"2018-01-26 08:26:46"'],
  ['id' => '112','title' => '"First English語言學校心得評價：日式管理學校、溫馨校園打造最佳學習環境！"','slug' => 'first-english-acadmey','excerpt' => '"談到菲律賓遊學，溫馨的First English語言學校位於度假勝地馬克坦島上鬧中取靜的住宅區，除了相當安全、幽靜外，距離宿霧機場僅15分鐘路程。平日可以讓學生專注在英文學習上，假日也能在鄰近的海灘、度假村泳池中享受南島愜意的休閒時光，非常適合注重隱私、想要彈性安排課程、強化英文口語，或是親子遊學的學生。"','content' => '"<p><a title=""菲律賓遊學，為何值得你考量？詳細解說菲律賓語言學校費用與代辦挑選！"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>的選擇很多，而在宿霧的一家<a title=""菲律賓語言學校"" href=""https://tw.english.agency/schools"">菲律賓語言學校</a>渡假形式著稱，溫馨的<a title=""First-English語言學校設備規格介紹"" href=""https://tw.english.agency/schools/first-english-%E8%AA%9E%E8%A8%80%E5%AD%B8%E6%A0%A1"">First English語言學校</a>位於度假勝地馬克坦島上鬧中取靜的住宅區，除了相當安全、幽靜外，距離宿霧機場僅15分鐘路程。平日可以讓學生專注在英文學習上，假日也能在鄰近的海灘、度假村泳池中享受南島愜意的休閒時光，非常適合注重隱私、想要彈性安排課程、強化英文口語，或是親子遊學的學生。</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">First English 語言學校環境介紹-</span></h2>
<h3><span style=""font-weight: 400;"">宿霧Cebu city 介紹：</span></h3>
<p>First English 語言學校位在菲律賓的宿霧市，菲律賓宿霧是個發展相當高的地方這裡不僅僅是菲律賓的財經、政治中心，這裡的人口組成更是特別，早期因為西班牙殖民文化、美國管理、以及在地文化與伊斯蘭教文化， 這裡就如同是文化大熔爐一般。</p>
<h3><span style=""font-weight: 400;"">郊區家庭式小學校、鄰近海灘度假村、遠離鬧區</span></h3>
<p><span style=""font-weight: 400;"">菲律賓遊學的選擇，溫馨的First English語言學校位於度假勝地宿霧馬克坦島上鬧中取靜的住宅區，除了相當安全、幽靜外，距離<a title=""麥克坦機場"" href=""https://zh.wikipedia.org/zh-tw/%E9%BA%A5%E5%85%8B%E5%9D%A6-%E5%AE%BF%E9%9C%A7%E5%9C%8B%E9%9A%9B%E6%A9%9F%E5%A0%B4"">宿霧機場</a>僅15分鐘路程。平日可以讓學生專注在英文學習上，假日也能在鄰近的海灘、度假村泳池中享受南島愜意的休閒時光，非常適合注重隱私、想要彈性安排課程、強化英文口語，或是親子遊學的學生。</span></p>
<p><span style=""font-weight: 400;"">（觀看<a href=""http://www.firstcebu.com/"">First-English-日本官網</a>）</span></p>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">First English Global College語言學校特色</span></h2>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>日本學生比例高</strong>：First English為小型的日籍學校，學生總人數最多70人，能同時兼顧隱私、社交，同時也能降低在校內使用中文的機會，強化英文口語的練習。</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>設備及日用品完善</strong>：每間房內皆有專屬浴廁，並提供學生免費衛生紙及吹風機，開學時提供入學包，必備生活用品不用自己攜帶，同時校內亦有福利社販售零食、日用品，並提供免費洗衣服務，生活相當方便。</span></li>
</ul>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>WIFI訊號強</strong>：許多學生都無法離開高速網路的生活，First English校內被有高速光纖網路，讓下課後的時間仍能享有順暢的無線網路。</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><strong>Buddy Teacher</strong> 專屬助教：針對學生在學習或生活上，甚至未來人生規劃提出協助，並透過每天的複習課，針對課堂上的錯誤紀錄，幫學生特別加強。</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">日式定食大獲好評：資深主廚為學生提供彈性多樣的美食，深獲學生好評。</span></li>
</ul>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">課程安排相當多元彈性、有1對1的多樣課程提供學生每週進行調整、增減，亦有輕鬆的半日悠活、孩童課程，搭配保母照護及托兒所服務，讓想輕鬆學習、親子遊學的學生也有適合的選擇。</span></li>
</ul>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">First English語校課程安排</span></h2>
<p><span style=""font-weight: 400;"">First English採取的斯巴達式學習、與半斯巴達式課程，大家可以根據自己的需要做整理調配&gt;</span></p>
<p>&nbsp;</p>
<table>
<tbody style=""boder-style: solid; boder-width: 1px;"">
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一般英文：</span></p>
<p><span style=""font-weight: 400;"">大部份學生選擇的課程，適合所有程度學生參加。</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 3堂＋團體課 3堂</span></p>
<p><span style=""font-weight: 400;"">一對一課程著重在英文口說訓練，包含發音、會話，每天包含一堂複習課，針對當日課程中需要改進的地方做加強。</span></p>
<p><span style=""font-weight: 400;"">團體課則著重演講、辯論等溝通訓練。</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">完美一對一課程：</span></p>
<p><span style=""font-weight: 400;"">能針對學生各方面的能力進行改進及加強</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 7堂</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">適合想要快速達成學習成效、重視各方面能力的你</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">多益先修班、雅思先修班、托福先修班、商業先修班</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 4堂＋團體課 3堂</span></p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">「測驗先修班」與「一般英文課程」的差異在於，每天新增一堂「測驗先修課」，讓想要針對專業考試提升能力的學生，能利用1對1課程強化測驗能力。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">「商業先修班」則是每天一堂以商業情境為主題的課程，包含書信、會議、電話、社交等，適合想要提升商業溝通技能的學生。</span></li>
</ul>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">半天悠活課程</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 4堂</span></p>
<p><span style=""font-weight: 400;"">只有半天的課程，下午即能自由安排活動及形成，適合想要輕鬆就讀的學生。</span></p>
</td>
</tr>
<tr>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">孩童課程</span></p>
</td>
<td style=""border: 1px solid gray; text-align: center;"">
<p><span style=""font-weight: 400;"">一對一 6堂</span></p>
<p><span style=""font-weight: 400;"">針對4-12歲孩童設計的課程，會依照學生個別能力而調整課程內容。</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">每堂課 50 分鐘，下課 10 分鐘。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">每天早上單字測驗（早上07:50~08:00），測驗內容含：單字詞性、英文解釋、英文造句。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">每週五 TOEIC 模擬考試（測驗：下午13：30～15：20 / 檢討：15:30~16:30）。</span></li>
</ul>
<p><br /><br /></p>
<hr />
<h2><span style=""font-weight: 400;"">First English語言學校的評價-</span><span style=""font-weight: 400;"">結業學生</span></h2>
<p><span style=""font-weight: 400;"">18歲 女性：</span></p>
<p><span style=""font-weight: 400;"">學校的網路評價很好，想認真學英文不被太多外務打擾的話，這裡就是很清幽的選擇。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">26歲 女性：</span></p>
<p><span style=""font-weight: 400;"">環境確實是很大的因素，明明在台灣一句話都不敢講的我，到學校後卻能一直在課堂中跟老師對話，且一對一的課完全不用怕自己講錯，老師都很有耐心的聽我們講完並適時地糾正學生的錯誤，這真的對不敢開口的同學有很大的幫助。團體課則會針對同學的程度作分班，因此不會覺得彼此的程度差異太大而影響學習。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">16歲 男性：</span></p>
<p><span style=""font-weight: 400;"">在國內比較難練習到口說部分，在菲律賓有1對1課程可以密集訓練口說，First English團體課是小班制的程度分班，所以其實我的同學只有我姐一個人，在團體課還是可以講非常多話。</span></p>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"First English Global College語言學校特色與介紹：日式管理學校、溫馨校園打造最佳學習環境！"','og_description' => '"談到菲律賓遊學，溫馨的First English語言學校位於度假勝地馬克坦島上鬧中取靜的住宅區，除了相當安全、幽靜外，距離宿霧機場僅15分鐘路程。平日可以讓學生專注在英文學習上，假日也能在鄰近的海灘、度假村泳池中享受南島愜意的休閒時光，非常適合注重隱私、想要彈性安排課程、強化英文口語，或是親子遊學的學生。"','meta_title' => '"First English Global College語言學校特色與介紹：日式管理學校、溫馨校園打造最佳學習環境！"','meta_description' => '"談到菲律賓遊學，溫馨的First English語言學校位於度假勝地馬克坦島上鬧中取靜的住宅區，除了相當安全、幽靜外，距離宿霧機場僅15分鐘路程。平日可以讓學生專注在英文學習上，假日也能在鄰近的海灘、度假村泳池中享受南島愜意的休閒時光，非常適合注重隱私、想要彈性安排課程、強化英文口語，或是親子遊學的學生。"','canonical_url' => 'https://tw.english.agency/first-english-global-college','feature_image_alt' => 'first-english-global-college','feature_image_title' => 'first-english-global-college','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6da8c62ac14-i2WGxfNjTt5WEMZyuqUvqen9e4Kfmc-1000x667.jpeg','author_id' => '16','category_id' => '1','created_at' => '"2018-01-28 10:49:47"'],
  ['id' => '113','title' => 'Kredo語言學校心得評價：以數位技能與英語學習為特色的先進語言學校！','slug' => 'Kredo-english-language-school','excerpt' => '如果不想要只是到菲律賓遊學，去菲律賓語言學校學英文的話，那你可曾想過在宿霧邊學英文、邊學習做作網路設計？菲律賓宿霧的Kredo語言學校結合了當紅的數位技能教學與英語學習，協助學生在結束學期後可以多項發展。','content' => '"<p>如果不想要只是到<a href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>，去<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>學英文的話，那你可曾想過在<a title=""cebu city "" href=""https://zh.wikipedia.org/zh-tw/%E5%AE%BF%E9%9C%A7%E5%B8%82"">宿霧</a>邊學英文、邊學習做作網路設計？菲律賓宿霧的<a href=""https://tw.english.agency/schools/kredo-%E8%AA%9E%E8%A8%80%E5%AD%B8%E6%A0%A1"">Kredo語言學校</a>結合了當紅的數位技能教學與英語學習，協助學生在結束學期後可以多項發展。</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">Kredo 語言學校環境、特色介紹-</span></h2>
<p><strong>宿霧Ceub city 介紹</strong></p>
<p>菲律賓的宿霧市是重要的財經、製證中心，這裡有許多的外商企業進駐，同時也是語言學校的集中地，此外這裡的人口組成複雜，除了有歐洲人、在地人以外，還有許多華人、韓國人、日本人、以及其他伊斯蘭的國家的人，種族組成是相當複雜的。</p>
<p>&nbsp;</p>
<p><strong>位於經濟成長高速預期區 享有安全、衛生、最方便的地理位置</strong></p>
<p><span style=""font-weight: 400;"">菲律賓的經濟成長被外媒高度期待，而Kredo Cebu IT Abroad語言學校位於被稱為「小新加坡的經濟特區」的宿霧IT園區新大樓的頂層，是唯一通過政府官方認證的IT學校，包括課程、教師、設施等，都通過菲律賓政府嚴格審查。</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">Kredo</span><span style=""font-weight: 400;""> Cebu IT Abroad語言學校特色</span></h2>
<h3><span style=""font-weight: 400;"">Kredo Cebu IT Abroad&nbsp;</span><span style=""font-weight: 400;"">課程特色</span></h3>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">日籍學校中唯一可以用英語學習IT的學校，讓你同時學好英文、並擁有架站或個人部落格的能力！</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">上午上英文，下午上IT課程，依照學生個人能力，從無到有學習基本IT綜合知識、網頁及平面設計、網站開發、程式語言、Wordpress&hellip;...並能夠用英語來溝通IT專業，打造海外職涯力必備。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">亦可針對已有IT資歷者，諮詢並提供合適的對應課程，有助職場專業及英語溝通力養成。</span></li>
</ul>
<h3><span style=""font-weight: 400;"">Kredo Cebu IT Abroad 校園特色</span></h3>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">校舍治安：宿舍位於宿霧島最安全的ITpark，設有24小時保安，沒有註冊的人員無法隨意進出。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">飲食：周邊具有多樣化日式、菲律賓式、義式美食，即使參與多個月的課程訓練，也不怕口味吃膩。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">適合族群：15歲以上具有IT興趣或求學領域的學生、IT產業的社會人士，有無經歷都OK！</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">學員畢業後，能提供適合的就業/實習機會，讓所學立即找到發揮的舞台！</span></li>
</ul>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">Kredo語言學校的心得評價-</span><span style=""font-weight: 400;"">結業學生分享</span></h2>
<h3><strong>家庭主婦 女性</strong></h3>
<p><span style=""font-weight: 400;"">在出國留學之前，我擔心自己無法跟上課程，但是我的老師會考慮到我的英語程度，即使我的程度較低，也能聽從老師的解釋，一遍又一遍地理解課程，並且從基礎開始學習起Photoshop，Illustrator，HTML，CSS。現在我可以幫助我丈夫做一名專業的網頁設計師了！</span></p>
<p>&nbsp;</p>
<h3><strong>高中生 女性</strong></h3>
<p><span style=""font-weight: 400;"">未來我想在海外工作。所以，我希望透過英語＋IT的訓練來提高自己的技能。起初上課時我非常緊張，因為我擔心無法應付IT知識和英語課。不過，老師很仔細的解釋，所以即使我從0開始，也能正確理解。讓我可以很好地學習基本的東西。</span><span style=""font-weight: 400;""><br /><br /></span></p>
<h3><strong>IT企業工作者 女性</strong></h3>
<p><span style=""font-weight: 400;"">我的夢想是成為一個網頁設計師，前往海外、環遊世界，甚至移民到歐洲。</span></p>
<p><span style=""font-weight: 400;"">我認為從現在起IT和英語將成為必要的技能，我是憑直覺決定來Kredo的，當我知道能同時用英語接受IT課程後，就決定是它了！剛開始我很難過，因為我的英語程度不好，我常常不知道自己在說什麼，但是在課程中獲得老師耐心的協助，我的英語能力逐漸提高，當理解力加深了之後，我的疑惑也都漸漸能夠解決。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">在這裡也能遇到許多背景不同的人、很多相當認真的人，這就是為什麼Kredo是一個適合學習的環境，每天在研究之外還能聽到各種不同的故事，絕對值得期待！</span></p>"','published' => '1','og_title' => 'Kredo語言學校心得評價：以數位技能與英語學習為特色的先進語言學校！','og_description' => '如果不想要只是到菲律賓遊學，去菲律賓語言學校學英文的話，那你可曾想過在宿霧邊學英文、邊學習做作網路設計？菲律賓宿霧的Kredo語言學校結合了當紅的數位技能教學與英語學習，協助學生在結束學期後可以多項發展。','meta_title' => 'Kredo語言學校心得評價：以數位技能與英語學習為特色的先進語言學校！','meta_description' => '如果不想要只是到菲律賓遊學，去菲律賓語言學校學英文的話，那你可曾想過在宿霧邊學英文、邊學習做作網路設計？菲律賓宿霧的Kredo語言學校結合了當紅的數位技能教學與英語學習，協助學生在結束學期後可以多項發展。','canonical_url' => 'https://tw.english.agency/kredo-english-language-school','feature_image_alt' => 'Kredo-english-academy','feature_image_title' => 'Kredo-english-academy','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6db874cd5fc-wh8OZ9hApYdDuMtN6TAQoqRJPwzQpD-1200x630.jpeg','author_id' => '16','category_id' => '1','created_at' => '"2018-01-28 11:31:52"'],
  ['id' => '114','title' => '菲律賓地區介紹，宿霧、碧瑤、馬尼拉、克拉克快速解析！','slug' => 'philippines-study-aboard-city','excerpt' => '不知道該去哪個城市菲律賓遊學?菲律賓四大城市介紹讓你不再選擇障礙!最近到海外遊學的風氣越來越盛，而也有越來越多人紛紛跑到菲律賓學英文，但菲律賓這麼大，有上百個城市，該選擇哪個城市就讀菲律賓語言學校呢?今天就來介紹菲律賓四大主要遊學城，讓你立馬分析出最適合自己的地方，不用再選擇障礙啦!','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p><span style=""font-weight: 400;"">不知道該去哪個城市<a title=""菲律賓遊學，為何值得你考量？詳細解說菲律賓語言學校費用與代辦挑選！"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>?菲律賓四大城市介紹讓你不再選擇障礙!最近到海外遊學的風氣越來越盛，而也有越來越多人紛紛跑到菲律賓學英文，但菲律賓這麼大，有上百個城市，該選擇哪個城市就讀<a title=""認識更多菲律賓語言學校"" href=""https://tw.english.agency/schools"">菲律賓語言學校</a>呢?今天就來介紹菲律賓四大主要遊學城，讓你立馬分析出最適合自己的地方，不用再選擇障礙啦!</span></p>
<hr />
<h2><span style=""font-weight: 400;""><img title=""菲律賓遊學-馬尼拉"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6dd2366d5a8-C0kCxBw3NFKcuZZg4BUIbp84WaPXno-1200x760.jpeg"" alt=""菲律賓遊學-馬尼拉"" width=""100%"" height=""auto"" /></span></h2>
<h2><span style=""font-weight: 400;"">1.馬尼拉遊學、馬尼拉語言學校：</span></h2>
<p>&nbsp;</p>
<h3><strong>馬尼拉介紹：</strong></h3>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;""><a title=""馬尼拉介紹"" href=""https://zh.wikipedia.org/zh-tw/%E9%A9%AC%E5%B0%BC%E6%8B%89"">馬尼拉</a>是菲律賓的首都，也是最多航空直達的菲律賓城市，不管你想搭廉航還是一般航空都有超級多樣的選擇讓你挑，而除了交通方便外，馬尼拉也是整個菲律賓的經濟文化中心，繁榮的街景跟交通設施讓你就像置身在台北市一樣，完全不用擔心會有適應不良的問題，如果你是比較怕生或討厭花⻑時間等車的人，馬尼拉絕對是你最好的選擇!</span></p>
<h3><strong>馬尼拉景點</strong></h3>
<ul>
<li><strong><a href=""http://bluehero.pixnet.net/blog/post/41274574-%5B%E9%81%8A%E8%A8%98%5D-%E9%A6%AC%E5%B0%BC%E6%8B%89-%E8%81%96%E5%9C%B0%E7%89%99%E5%93%A5%E5%A0%A1-fort-santiago"">聖地牙哥城堡</a>：</strong></li>
<li><a href=""http://piliapp-mapping.fangwenkuo.com/blog/post/340833114-%E3%80%90%E9%A6%AC%E5%B0%BC%E6%8B%89-manila-%E2%9C%88%E3%80%91%E8%8F%B2%E5%BE%8B%E8%B3%93%E5%A4%A7%E9%9B%85%E5%8F%B0tagaytay%E5%8D%8A%E6%97%A5"">達雅台：</a></li>
</ul>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;""><img title=""宿霧市-cebu-city"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6dd26a0235b-h2BaXIkFBQCEyTrO5Y9iaI6sMyC30X-1200x773.jpeg"" alt=""宿霧市-cebu-city"" width=""100%"" height=""auto"" /></span></h2>
<h2><span style=""font-weight: 400;"">2.宿霧遊學、宿霧語言學校介紹：</span></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">位於菲律賓南方的<a title=""菲律賓宿霧市介紹"" href=""https://zh.wikipedia.org/wiki/%E5%AE%BF%E9%9C%A7%E5%B8%82"">宿霧市</a>，為菲律賓第二大城市，交通方便且治安良好，宿霧擁有上百座海灘，而浮潛、水肺潛水和衝浪也都是當地超級盛行的活動，如果擔心在市區玩不夠，還可以搭船到薄荷島去跟海⻱游泳和賞海豚，除此之外，還可以到奧斯洛布賞全世界最大的哺乳類鯨鯊，熱愛水上活動的你絕對要選擇來宿霧遊學!</span>&nbsp;</p>
<h3><strong>知名宿霧景點：</strong></h3>
<ul>
<li style=""font-weight: 400;""><a href=""http://3dacademy.tw/blogs/2015/04/08/cebufortsanpedro.html"">聖佩特羅堡</a></li>
<li style=""font-weight: 400;""><a href=""http://edisonisme.pixnet.net/blog/post/201903976-%5B%5B%E8%8F%B2%E5%BE%8B%E8%B3%93--%E9%81%8A%E6%A8%82%5D%5D-%E5%AE%BF%E9%9C%A7---%E8%81%96%E5%AC%B0%E8%81%96%E6%AE%BF%28basilica-del-san"">聖嬰聖殿</a></li>
<li style=""font-weight: 400;""><a href=""http://e09006anny.pixnet.net/blog/post/381653750-%E8%8F%B2%E5%BE%8B%E8%B3%93day2-1%E9%A3%AF%E5%BA%97%E6%97%A9%E9%A4%90~%E5%8E%BB%E5%B8%82%E5%8D%80colon-street%E7%A7%91%E9%9A%86%E8%80%81"">科隆街</a></li>
</ul>
<p>&nbsp;</p>
<h3><strong>宿霧語言學校介紹範例： </strong></h3>
<ul>
<li><span style=""font-weight: 400;""><a href=""https://tw.howdyenglish.asia/"">Howdy語言學校</a>：強調自主學習，由日商經營把關，校內風格採取日式規格辦理。</span></li>
<li><span style=""font-weight: 400;""><a title=""Fella語言學校官方網站"" href=""https://fellatw.com/"">Fella語言學校</a>：度假式的學習中心，強調斯巴達與半斯巴達並行。</span></li>
<li><span style=""font-weight: 400;""><a href=""http://smenglish.com/tw/?ckattempt=1"">SME語言學校</a>：以斯巴達式的管理模式著稱，是知名的斯巴達語言學校。</span></li>
<li><span style=""font-weight: 400;""><a href=""http://evacademy.org/"">EV語言學校</a>： 以斯巴達學習為核心，為宿霧數一數二大的學校。</span></li>
</ul>
<hr />
<h2><span style=""font-weight: 400;""><img title=""碧瑤市"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6aba6acfc08-URoK6pXU5sLqqMjJk6aq8htHh2HiXS-1000x508.png"" alt=""碧瑤市"" width=""100%"" height=""auto"" /></span></h2>
<h2><span style=""font-weight: 400;"">3.碧瑤遊學、碧瑤語言學校：</span></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">位於山區的碧瑤，年均溫 17-20 度，比菲律賓其他城市要低上五到十度，可以說是完全沒有夏天的一座城市，除了氣候涼爽宜人以外，受到⻄班牙殖⺠影響，碧瑤處處富含濃厚的藝術氣息，從街景到林立的美術館都能看出這座城市的人文氣息，喜歡藝術又想避暑的你，碧瑤絕對是你最好的選擇!</span></p>
<p>&nbsp;</p>
<h3><strong>知名碧瑤(baugio)景點：</strong></h3>
<ul>
<li style=""font-weight: 400;"">
<p><span style=""font-weight: 400;"">Tam-Awan Village：碧瑤地區的文化村，可以說是天空文化村。</span></p>
</li>
<li style=""font-weight: 400;"">
<p><span style=""font-weight: 400;"">BenCab Museum： 碧瑤市當地的美術館，以古典結合當代為最主要的特色。</span></p>
</li>
<li style=""font-weight: 400;"">
<p><span style=""font-weight: 400;"">Baguio Sunshine Park：碧瑤日落公園，假日絕對適合的好去處。</span></p>
</li>
</ul>
<p>&nbsp;</p>
<h3><strong>碧瑤語言學校： </strong></h3>
<ul>
<li><span style=""font-weight: 400;""><a title=""前往help語言學校官網"" href=""https://tw.helpenglish.aisa"">Help語言學校</a>:help語言學校是斯巴達學習模式的始祖，在碧瑤一共有兩個分校，一個在longlong、一個在Martin，針對TOEIC，IELTS，TOFEL的專攻校區。</span></li>
</ul>
<hr />
<h2><span style=""font-weight: 400;""><img title=""克拉克遊學"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6dd29b59dae-ZXkChoHrvyM1JPoOLyOAAAdoz98Vf0-1614x1080.jpeg"" alt=""克拉克遊學"" width=""100%"" height=""auto"" /></span></h2>
<h2><span style=""font-weight: 400;"">4.克拉克遊學、克拉克語言學校：</span></h2>
<p>&nbsp;<span style=""font-weight: 400;"">菲律賓的Clark（克拉克）曾經是美國海軍最重要的軍事基地的克拉克，是菲律賓航運最發達的一座城市，受到歐美文化的薰陶，克拉克充滿多樣化的各國建築，而它也是全菲律賓度假村最多的一座城市，最知名的克拉克度</span><span style=""font-weight: 400;"">假城更結合了水上樂園、高爾夫球場和娛樂場所，讓你在裡面玩到不想退房，想要在遊學的同時又能好好度假放鬆，去克拉克準沒錯!</span></p>
<p>&nbsp;</p>
<p>&nbsp;<strong>知名克拉克景點：</strong></p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">Hot Air Balloon Festivel</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">Clark Nature Park</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">Nayoung Pilipion 克拉克文化村</span></li>
</ul>
<p><span style=""font-weight: 400;"">(如果想更深入認識克拉克：請參考<a href=""http://www.itsmorefuninthephilippines.com.tw/"">菲律賓觀光局</a>)</span></p>
<p>&nbsp;</p>
<p><strong>克拉克語言學校範例：</strong></p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""前往Eg語言學校官網"" href=""http://egesl.com/"">Eg語言學校</a>：一般密集式英文、半斯巴達式、斯巴達課程為主，菲籍與外師共同授課。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""前往CIP語言學校官網"" href=""https://www.cipenglishschool.com"">CIP語言學校</a>：強調全英語環境，以訓練口說為主要的核心。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;""><a title=""前往Help語言學校官網"" href=""https://helpenglish.com.tw/"">Help語言學校克拉克校區</a>:斯巴達教學的始祖，專業的斯巴達課程教學。</span></li>
</ul>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">看完這四座城市的介紹後，是不是讓你更了解菲律賓了呢?趕快做好各地交通的功課，就能輕輕鬆鬆到你最喜歡的地方遊學，留下美</span><span style=""font-weight: 400;"">好回憶啦!</span></p>
</div>"','published' => '1','og_title' => '菲律賓地區介紹，宿霧、碧瑤、馬尼拉、克拉克快速解析！','og_description' => '不知道該去哪個城市菲律賓遊學?菲律賓四大城市介紹讓你不再選擇障礙!最近到海外遊學的風氣越來越盛，而也有越來越多人紛紛跑到菲律賓學英文，但菲律賓這麼大，有上百個城市，該選擇哪個城市就讀菲律賓語言學校呢?今天就來介紹菲律賓四大主要遊學城，讓你立馬分析出最適合自己的地方，不用再選擇障礙啦!','meta_title' => '菲律賓地區介紹，宿霧、碧瑤、馬尼拉、克拉克快速解析！','meta_description' => '不知道該去哪個城市菲律賓遊學?菲律賓四大城市介紹讓你不再選擇障礙!最近到海外遊學的風氣越來越盛，而也有越來越多人紛紛跑到菲律賓學英文，但菲律賓這麼大，有上百個城市，該選擇哪個城市就讀菲律賓語言學校呢?今天就來介紹菲律賓四大主要遊學城，讓你立馬分析出最適合自己的地方，不用再選擇障礙啦!','canonical_url' => 'https://tw.english.agency/philippines-study-aboard-city','feature_image_alt' => '菲律賓遊學地區','feature_image_title' => '菲律賓遊學地區','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ddb5e4789a-SDtYY6Y7t9gPjttGoltMdfAJbIqC4b-1200x633.jpeg','author_id' => '14','category_id' => '1','created_at' => '"2018-01-28 13:55:06"'],
  ['id' => '115','title' => '菲律賓語言學校推薦，斯巴達語言學校、半斯巴達語言學校、度假式學習總整理！','slug' => 'philippines-esl-academy-recommend','excerpt' => '菲律賓語言學校選擇多，不論是在陽光沙灘的宿霧或是青山環繞的碧瑤。斯巴達教學、半斯巴達教學更是當地的一大特色。在菲律賓語言學校學英文，密集式教學能提高學習效率，透過外籍教師以及當地教師一對一的指導，能更快補足自己的弱點；度假式學習能夠讓自己完全沈浸在語言當中快樂的學習。然而想到菲律賓遊學要如何選擇適合自己的課程呢？','content' => '"<p style=""padding-left: 30px; text-align: left;""><span style=""font-weight: 400;""><a href=""https://tw.english.agency/schools"">菲律賓語言學校選擇</a>多，不論是在陽光沙灘的宿霧或是青山環繞的碧瑤。斯巴達教學、半斯巴達教學更是當地的一大特色。在菲律賓學英文，密集式教學能提高學習效率，透過外籍教師以及當地教師一對一的指導，能更快補足自己的弱點；度假式學習能夠讓自己完全沈浸在語言當中快樂的學習。然而想到<a title=""菲律賓遊學好嗎？菲律賓語言學校費用、代辦最佳挑選辦法告訴你！"" href=""philippines-study-aboard"">菲律賓遊學</a>要如何選擇適合自己的課程呢？</span></p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">Step 1：了解自己的英語程度，挑選適合的菲律賓遊學課程</span></h2>
<p><span style=""font-weight: 400;"">首先必須先了解自己的程度，衡量自己的能力才能夠正確地選擇適合的課程。你可以透過分數了解自己的程度區間，只要你曾經考過多益、托福、雅思、全民英檢等英語測驗，都能夠作為衡量程度的基準。</span></p>
<p><span style=""font-weight: 400;""><img title=""ECFR分級"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6ddef398814-4EJrVWO4eIYfQwpFxiy3ClasNTnM9e-438x707.png"" alt=""ECFR分級"" width=""404px "" height=""707"" /></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">&nbsp;</span><span style=""font-weight: 400;"">Step 2：確立自己的目標，挑選對的菲律賓遊學課程！</span></h2>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">知道自己的程度之後就可以開始制定學習目標，例如：進步多少分、通過哪一個級別考試、加強口說聽力等，也可以是為了出國留學做準備、升職進修所需，安排適合自己目標的課程。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">菲律賓的語言學校開設的課程分類仔細，不論是一般英文、檢定考試英文、商用英文或是打工遊學英文都有專門的安排與專精的課程，讓你可以在短時間之內有方向地學習，提升學習效果，這樣的課程教學也獲得<a title=""CELC語言學校宿霧特色與評價，多語言課程、斯巴達課程專業學校！"" href=""https://tw.english.agency/celc-language-school-comment"">學生的好評</a>。</span></p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;""><img title=""Toeic-test-grade-pyamid"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de0d489ddf-aVHszwDwX1IZ0NRUxD1vc2SKR0qi1Q-1125x1081.png"" alt=""Toeic-test-grade-pyamid"" width=""100%"" height=""auto"" /></span></h2>
<h2>&nbsp;</h2>
<hr />
<h2><span style=""font-weight: 400;"">Step 3：認識菲律賓教學型態「斯巴達、半斯巴達、開放式」</span></h2>
<p><span style=""font-weight: 400;"">菲律賓語言學校的學習型態可分成三種：「斯巴達」、「半斯巴達」、「開放式」，三者最大差異在於門禁管制與每日學習時間安排。</span><span style=""font-weight: 400;"">斯巴達的語言學校以&nbsp;<a title=""https://tw.helpenglish.asia"" href=""https://tw.helpenglish.asia"">HELP 語言學校&nbsp;</a>在碧瑤最為有名，宿霧則是 <a title=""SME 語言學校"" href=""http://smenglish.com/tw/?ckattempt=1"">SME 語言學校</a>。</span></p>
<p>&nbsp;</p>
<h3><strong>菲律賓斯巴達語言學校</strong></h3>
<p><span style=""font-weight: 400;"">週間禁止外出，從早上開始安排課程，起床後的閱讀時間、日間一對一課程、有些學校下午有團體討論時間到晚間課後單字測驗和自習。這樣的方式適合想要留學、升遷要快速獲得成效，達到測驗目標分數的學生。</span></p>
<p><em><span style=""font-weight: 400;"">eg.推薦斯巴達語言學校：Help語言學校、SME語言學校、<a href=""https://fellatw.com/"">Fella語言學校</a>、<a href=""http://evacademy.org/"">EV語言學校</a>、<a href=""https://tw.english.agency/CELC%20%E8%AA%9E%E8%A8%80%E5%AD%B8%E6%A0%A1%E9%8D%9B%E9%8D%8A%E8%8B%B1%E8%AA%9E%E7%A1%AC%E5%AF%A6%E5%8A%9B"">CELC語言學校</a></span></em></p>
<p>&nbsp;</p>
<h3><span style=""font-weight: 400;""><strong>半斯巴達式教學</strong></span></h3>
<p><span style=""font-weight: 400;"">沒有早晚的自習晨讀時間，週間亦可外出，讓學生能有自我運用的時間，適合專注於一般英語聽說讀寫提升的學生。不喜歡高壓學習的學生多半會選擇半斯巴達式，這種教學方式則推薦自律性較高的學生就讀。</span>&nbsp;</p>
<p><em>eg.推薦半斯巴達語言學校：Fella語言學校、<a href=""https://www.facebook.com/Cebu-Pelis-Institute-CPI-1462893634003470"">CPI語言學校</a></em></p>
<p>&nbsp;</p>
<h3><span style=""font-weight: 400;""><strong>開放式</strong></span></h3>
<p><span style=""font-weight: 400;"">較偏向歐美型態管理，能夠自行外出，類似大學上課，所以會有出席率的問題。而這樣類型的學校偏向度假式學習，適合親子共學或是自律性高的學生，當然也有其他名詞，像是度假式學習、自律形都囊括在這開放式的範疇下。</span></p>
<p><em>eg.開放式語言學校：cella語言學校、<a title=""前往CG語言學校官網"" href=""http://www.cebucg.com/Ta/"">CG語言學校</a>、<a title=""前往Howdy語言學校官網"" href=""https://tw.howdyenglish.asia"">Howdy語言學校</a></em></p>
<p>&nbsp;<img title=""philipines-language-school"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de13f887d3-Ufg9AxZVba5VEpT1zeddgtrkTwwry4-1024x1213.png"" alt=""philipines-language-school"" width=""100%"" height=""auto"" /></p>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p>&nbsp;</p>
<hr />
<h2><span style=""font-weight: 400;"">Step 3.再了解自己的程度、立定學習目標且認識菲律賓教學型態之後，還有哪些需要考量的呢?</span></h2>
<p>&nbsp;</p>
<p><strong>1. 學校環境設備與伙食</strong></p>
<p><span style=""font-weight: 400;"">了解自己喜歡怎麼樣的生活型態、<a title=""菲律賓地區介紹，宿霧、碧瑤、馬尼拉、克拉克快速解析！"" href=""https://tw.english.agency/philippines-study-aboard-city"">到哪一個城市遊學</a>，是都市的便利、海島的悠閒還是自然景觀的學習環境？除了課程之外，在一整天的學習之下，學校的設備也是一個很重要的因素。許多學校設備都非常新穎，也有不少設施可以使用。像是電腦自習室、健身房、游泳池、cafe等，讓你在學習之餘能夠有放鬆的空間。</span></p>
<p><span style=""font-weight: 400;""><img title=""cpi-language-school"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de1d30b861-aO8umqzeCGMSu5OndUNtBi5PsMiVqD-720x400.jpeg"" alt=""cpi-language-school"" width=""100%"" height=""auto"" /></span></p>
<p><strong><img title=""cpi-pool"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de24e6db1b-Kwxko5Ew9JquwKBZDsMFpmi1AQqLfz-1200x675.jpeg"" alt=""cpi-pool"" width=""100%"" height=""auto"" /></strong></p>
<p><strong><img title=""cpi-gym"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de2897576a-7ZkKOg5RQtNMDOhzbH1AmylwIv1aAs-1200x675.jpeg"" alt=""cpi-gym"" width=""100%"" height=""auto"" /></strong></p>
<p><strong><img title=""cpi-dinningroom"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de2cbb7de1-z9pQsQEvqCPQTukIs6yBPCOP3T2BjQ-1066x507.png"" alt=""cpi-dinningroom"" width=""100%"" height=""auto"" /></strong></p>
<p><strong><img title=""cpi-2people-room"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de354e6163-jWycz1cEb8YX99hrADUg7PfffCf6Ca-4506x2535.jpeg"" alt=""cpi-2people-room"" width=""100%"" height=""auto"" /></strong></p>
<p><strong>CPI語言學校環境以豪華為特色，擁有戶外泳池、健身房、餐廳等設施</strong></p>
<p>&nbsp;</p>
<p><strong>2. 華人多寡</strong></p>
<p><span style=""font-weight: 400;"">想必華人的多寡也是一個值得考量的因素。菲律賓語言學校大多是由華人、日籍、韓籍的學生所組成。若希望快速加強語言能力，並與他國學生文化交流可以選擇以日、韓籍學生為主的學校；若擔心自己有程度上或適應上的問題則可以選擇華人較多的學校。</span></p>
<p>&nbsp;</p>
<p><strong>3. 價錢考量</strong></p>
<p><span style=""font-weight: 400;"">價格方面應該是大家最重要的考量重點之一，不管是斯巴達、半斯巴達或是開放式的學校，價格上面主要是依據學校的定價，其中多少會有優惠折扣，主要分為兩種：</span></p>
<p>&nbsp;</p>
<ul>
<li><strong>有規定折扣限制:</strong></li>
</ul>
<p><span style=""font-weight: 400;"">優惠折扣方面，有些學校為了維護市場價格穩定，規定合作的經銷代辦用制定的價格給學生，所以比較有名、有制度的學校基本上是採取這樣的價格策略，而這些學校學生來源已經非常穩定了，沒有特別的淡旺季，折扣較少。但是這些學校不管是師資、管理制度、學生進步成績上面都相對有保障。</span></p>
<p>&nbsp;</p>
<ul>
<li><strong> 沒有規定的學校：</strong></li>
</ul>
<p><span style=""font-weight: 400;"">有些學校會有淡季優惠，能夠以較低的價格就讀。此定價策略學校較常出現在巴克羅/怡朗地區，因為地點需要轉機，學生數比較少，或者是宿霧/碧瑤地區較不知名與招生數不足的學校。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">看完了以上的分析，是不是更了解該如何<a title=""精選菲律賓語言學校，從 300 多所語言學校中，精選出 23 所熱門菲律賓語言學校"" href=""https://www.goeducation.com.tw/%E8%8F%B2%E5%BE%8B%E8%B3%93%E8%AA%9E%E8%A8%80%E5%AD%B8%E6%A0%A1%E6%8E%A8%E8%96%A6"">選擇學校</a>了呢？其實不管程度如何、喜歡怎麼樣的學習型態、華人多不多，最重要的是「適合自己」。不論選擇哪一個學校或是課程，它都將是人生中的一個學習體驗和文化交流，只要好好善用資源，努力學習都可以達到「加強英文，為自己的能力與職涯加分」的效果！</span></p>"','published' => '1','og_title' => '菲律賓語言學校選擇-斯巴達語言學校，密集式學英文！考試英文度假式學習','og_description' => '菲律賓語言學校選擇多，不論是在陽光沙灘的宿霧或是青山環繞的碧瑤。斯巴達教學、半斯巴達教學更是當地的一大特色。在菲律賓語言學校學英文，密集式教學能提高學習效率，透過外籍教師以及當地教師一對一的指導，能更快補足自己的弱點；度假式學習能夠讓自己完全沈浸在語言當中快樂的學習。然而想到菲律賓遊學要如何選擇適合自己的課程呢？','meta_title' => '菲律賓語言學校選擇-斯巴達語言學校，密集式學英文！考試英文度假式學習','meta_description' => '菲律賓語言學校選擇多，不論是在陽光沙灘的宿霧或是青山環繞的碧瑤。斯巴達教學、半斯巴達教學更是當地的一大特色。在菲律賓語言學校學英文，密集式教學能提高學習效率，透過外籍教師以及當地教師一對一的指導，能更快補足自己的弱點；度假式學習能夠讓自己完全沈浸在語言當中快樂的學習。然而想到菲律賓遊學要如何選擇適合自己的課程呢？','canonical_url' => 'https://tw.english.agency/philippines-study-aboard-school','feature_image_alt' => '菲律賓語言學校課程','feature_image_title' => '菲律賓語言學校課程','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de4b8e93be-a4wC1yPikUFY5A4X49zXATSVKfRs5K-2500x1250.png','author_id' => '15','category_id' => '1','created_at' => '"2018-01-28 14:31:20"'],
  ['id' => '116','title' => '宿霧遊學推薦、心得、評價說明，你不可不知的宿霧語言學校特色大公開!','slug' => 'cebu-esl-school','excerpt' => '談到宿霧除了想到數不完的觀光景點，你可曾想過到菲律賓遊學，去菲律賓語言學校唸英文呢？菲律賓的宿霧cebu是個除了是個旅遊勝地之外，同時也是一個菲律賓語言學校的集中站，而在宿霧語言學校中又各自有屬於自己的特色！','content' => '"<p>&nbsp;<span style=""font-weight: 400;"">談到<a title=""宿霧-cebu-city介紹"" href=""https://zh.wikipedia.org/zh-tw/%E5%AE%BF%E9%9C%A7%E5%B8%82"">宿霧</a>除了想到數不完的觀光景點，你可曾想過到<a title=""菲律賓遊學好嗎？菲律賓語言學校費用、代辦最佳挑選辦法告訴你！"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>，去<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>唸英文呢？菲律賓的宿霧cebu是個除了是個旅遊勝地之外，同時也是一個菲律賓語言學校的集中站，而在宿霧語言學校中又各自有屬於自己的特色！</span></p>
<p>&nbsp;</p>
<hr />
<h2><img title=""cebu-city"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6dd26a0235b-h2BaXIkFBQCEyTrO5Y9iaI6sMyC30X-1200x773.jpeg"" alt=""cebu-city"" width=""1200"" height=""773"" /></h2>
<h2>宿霧市介紹</h2>
<p>菲律賓宿霧是菲律賓的經濟、政治中心，同時也是許多外商的集中地，這裡同時又是個複雜的文化大熔爐，因此歷史古蹟更是不在話下，例如說：聖佩羅特堡、聖嬰聖殿等等，都是相當由名的文化遺產，此外這裡的自然資源豐富，同時也是個知名的度假勝地喔！</p>
<p>&nbsp;如果想更認識宿霧旅遊相關的景點，可以到<a href=""http://www.itsmorefuninthephilippines.com.tw/spots/Cebu"">菲律賓觀光局宿霧介紹</a>看看喔！</p>
<p>&nbsp;</p>
<hr />
<p><img title=""宿霧遊學-cpi語言學校"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6de1d30b861-aO8umqzeCGMSu5OndUNtBi5PsMiVqD-720x400.jpeg"" alt=""宿霧遊學-cpi語言學校"" width=""720"" height=""400"" /></p>
<h2>推薦宿霧遊學的理由</h2>
<h3><strong>菲律賓語言學校選擇多，宿霧語言學校集中地</strong></h3>
<p><span style=""font-weight: 400;"">菲律賓的語言學校分為三種不同模式，有較嚴格的斯巴達式，半鬆半嚴的半斯巴達式，還有制度寬鬆的歐美式教育，而宿霧的語言學校佔了整個菲律賓約70%的比例，有各式各樣不同教育模式的學習可以任你選擇，而且不管是日本師資還是韓國師資都應有盡有，多</span><span style=""font-weight: 400;"">樣化的選擇超適合愛貨比三家的你!</span><span style=""font-weight: 400;""><br /></span></p>
<p>&nbsp;</p>
<h3><strong>菲律賓旅遊勝地-度假式的學習</strong></h3>
<p><span style=""font-weight: 400;"">位於菲律賓南邊的宿霧四面環海，比起其他城市，它擁有更加豐富的水上自然資源，宿霧四周散落著許多小島，而每座島上也都有不同的水上活動可以從事，到這裡遊學不但能學英文，在閒暇時刻也可以跑到海邊去玩水，甚至可以安排跳島行程，讓你可以邊學英文邊挑戰刺激的水上活動!</span></p>
<p>&nbsp;</p>
<hr />
<h2>宿霧語言學校心得</h2>
<p>&nbsp;</p>
<p><strong>1.English-Only-Policy&nbsp;</strong></p>
<p><span style=""font-weight: 400;"">到菲律賓遊學應該是大家最近朋友圈瘋傳的話題吧，除了到首都馬尼拉遊學外，其實還有很多人選擇到宿霧去讀語言學校，到底為什麼這些人都要選擇去宿霧呢?今天就來把你不可不知的宿霧遊學特色大公開，趕快訂張往宿霧的機票找代辦吧!</span><span style=""font-weight: 400;""><br /><br /></span></p>
<p><span style=""font-weight: 400;"">宿霧是菲律賓第二大城市，不但交通方便，從台灣有直飛的航班以外，同時也是菲律賓運用英文最廣泛的一個城市，在路上看得到的文字幾乎都是用全英文書寫，而且不論是攤販、餐廳服務員還是計程車司機都會講英文，不但可以訓練閱讀還能順便訓練口說，讓你</span><span style=""font-weight: 400;"">整個人都生活在全英文的世界！</span><span style=""font-weight: 400;""><br /></span></p>
<p>&nbsp;</p>
<p><strong>2.菲律賓語言學校價錢低，宿霧遊學費用低CP值高！</strong></p>
<p><span style=""font-weight: 400;"">相對於要快速進修英文的朋友來說，其費用遠比單純在仿間的英文補習班划算，且能共提供學生較為全面且長時間的訓練，是相當有幫助的，對於社會人士、或是短期進修、考試衝刺的朋友來說，在這樣一個幫你打理好部分生活起居的環境下來說，這樣的選擇可以說是物超所值，只能說菲律賓語言學校一個月就的幫助真的讓你超乎想像！</span></p>
<p>&nbsp;</p>
<p><strong>3.學習型態多樣，斯巴達、半斯巴達盛行：</strong></p>
<p>宿霧語言學校眾多，因此這裡的學習型態也隨之多樣，不論是從學校的風格，如度假式、或是一般大學式、都市學校等等形式，到學習型態、斯巴達課程、半斯巴達課程，如<a title=""前往SME語言學校官網"" href=""http://smenglish.com/tw/?ckattempt=1"">SME語言學校</a>、<a title=""前往CPI語言學校粉絲頁"" href=""https://www.facebook.com/Cebu-Pelis-Institute-CPI-1462893634003470"">CPI語言學校</a>、<a title=""fella語言學校官網"" href=""https://fellatw.com/"">fella語言學校</a>，因此學生在選擇學校時有相當多的選擇，可以依照自己的喜好，相較於菲律賓語言學校碧瑤地區來講，選擇也更多元，但是在碧瑤地區也有相當不錯的高級學校。</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<p><span style=""font-weight: 400;"">&nbsp;<img title=""宿霧語言學校推薦"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a6dd2366d5a8-C0kCxBw3NFKcuZZg4BUIbp84WaPXno-1200x760.jpeg"" alt=""宿霧語言學校推薦"" width=""1200"" height=""760"" /></span></p>
<h2><span style=""font-weight: 400;"">宿霧遊學語言學校簡介</span></h2>
<p><strong><br />什麼樣的人適合去宿霧遊學？</strong></p>
<p><span style=""font-weight: 400;"">如果你是想要訓練口說英文的人，或是你想要在遊學之餘還能從事一些有趣的水上活動，那就非常推薦你選擇到宿霧遊學，畢竟日常生活都會用到英文，英文不進步也難，而除了一般課程之外，還有所謂的宿霧親子遊學、宿霧商用英文等等課程，設計給特定對象的喔！</span></p>
<p>&nbsp;</p>
<p><strong>宿霧遊學代辦費用 </strong></p>
<p><span style=""font-weight: 400;"">基本上到菲律賓遊學的價格幾乎都落在一個月四五萬左右，不但含吃和住宿，連洗衣費都含在裡面，相較動輒就要十幾萬的歐美國家，真的是經濟又實惠!</span></p>
<p>&nbsp;</p>
<p><strong>宿霧遊學語言學校簡介 </strong></p>
<p>&nbsp;</p>
<ul>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">Howdy語言學校:強調自主學習，由日商經營把關，校內風格採取日式規格辦理。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">Fella語言學校:度假式的學習中心，強調斯巴達與半斯巴達並行。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">SME語言學校:以斯巴達式的管理模式著稱，是知名的斯巴達語言學校。</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">EV語言學校:以斯巴達學習為核心，為宿霧數一數二大的學校</span></li>
</ul>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">看完以上的介紹，是不是讓你對宿霧遊學的特色更加了解了呢?歡大自然又想練英文的你不要再猶豫了，趕快在遊學申請書上填上宿霧吧!</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">延伸閱讀：</span></p>
<p><a title=""Howdy 語言學校特色與評價：日式優質學校，日商企業合作品牌學校！"" href=""https://tw.english.agency/howdy-english-academy"">Howdy 語言學校特色與評價：日式優質學校，日商企業合作品牌學校！</a></p>
<p><a title=""First English語言學校心得評價：日式管理學校、溫馨校園打造最佳學習環境！"" href=""https://tw.english.agency/first-english-acadmey"">First English語言學校心得評價：日式管理學校、溫馨校園打造最佳學習環境！</a></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '宿霧遊學推薦、心得、評價說明，你不可不知的宿霧語言學校特色大公開!','og_description' => '談到宿霧除了想到數不完的觀光景點，你可曾想過到菲律賓遊學，去菲律賓語言學校唸英文呢？菲律賓的宿霧cebu是個除了是個旅遊勝地之外，同時也是一個菲律賓語言學校的集中站，而在宿霧語言學校中又各自有屬於自己的特色！','meta_title' => '宿霧遊學推薦、心得、評價說明，你不可不知的宿霧語言學校特色大公開!','meta_description' => '宿霧遊學推薦、心得、評價說明，你不可不知的宿霧語言學校特色大公開!','canonical_url' => 'https://tw.english.agency/cebu-esl-school','feature_image_alt' => '宿霧遊學','feature_image_title' => '宿霧遊學','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7087e392129-ghenVvjv8w3hmqLW1odHk5GoQVGjid-1200x630.jpeg','author_id' => '14','category_id' => '1','created_at' => '"2018-01-30 14:44:29"'],
  ['id' => '117','title' => '菲律賓遊學心得-菲律賓遊學生活小知識','slug' => 'philippines-language-school-review','excerpt' => '菲律賓語言學校以斯巴達式教學著名，密集式英文學習，全方位加強不論聽、說、讀、寫或是多益、托福、雅思測驗皆能讓你在18周的課程內提升能力。很多人想到菲律賓學英文，但是就算代辦和你講解了許多，還是覺得少了一點什麼嗎？我們搜集了大家的心得和經驗整理出包含課程、課餘、旅遊、飲食、天氣等五大項去菲律賓遊學生活小知識！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p>菲律賓語言學校以<a title=""菲律賓語言學校推薦，斯巴達語言學校、半斯巴達語言學校、度假式學習總整理！"" href=""https://tw.english.agency/philippines-esl-academy-recommend"">斯巴達式教學</a>著名，密集式英文學習，全方位加強不論聽、說、讀、寫或是多益、托福、雅思測驗皆能讓你在18周的課程內提升能力。很多人想到菲律賓學英文，但是就算代辦和你講解了許多，還是覺得少了一點什麼嗎？</p>
<p>我們搜集了大家的心得和經驗整理出包含<a title=""菲律賓語言學校推薦，斯巴達語言學校、半斯巴達語言學校、度假式學習總整理！"" href=""https://tw.english.agency/philippines-esl-academy-recommend"">課程</a>、課餘、旅遊、飲食、天氣等五大項去<a title=""菲律賓遊學好嗎？菲律賓語言學校費用、代辦最佳挑選辦法告訴你！"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>生活小知識！</p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">1.課程方面</span></h2>
<p><span style=""font-weight: 400;"">不論是不是斯巴達是教學，<a title=""菲律賓語言學校列表"" href=""https://tw.english.agency/schools"">菲律賓語言學校</a>的課程安排都不馬虎。以在宿霧的<a title=""SME-capital"" href=""https://www.smecapital.com/"">SME capital校區</a>，半斯巴達式教學為範例，每天都有一對一、聽說讀寫的課程，也有團體課程時間。因為是半斯巴達式，不像斯巴達式較密集的考試以及平日禁外出，學生能夠自主運用課餘時間，不論是自行複習、預習課程、與他國學生英語對談練習、使用學校內設施（健身房、游泳池等）或是傍晚下課後出去走走都能自己規劃。</span></p>
<p><img title=""SME-class-table"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a79732e47e8c-1I7zHA4axR1utULUlYgN1yIIEBRu54-916x422.png"" alt=""SME-class-table"" width=""100%"" height=""auto"" /></p>
<p><span style=""font-weight: 400;""><strong>學習小撇步：</strong>把握課餘等零碎時間，利用單字app、Quizlet單字卡等小工具加強自己不足的單字量、片語等，也能多與不同國籍的學生交流練習口說，加強生活用語及表達能力。</span></p>
<h1>&nbsp;</h1>
<h2><span style=""font-weight: 400;"">2.課餘生活</span></h2>
<p><span style=""font-weight: 400;"">不論是斯巴達、半斯巴達還是開放式教學週末皆可以外出，18周的上課時間就有18個週末可以自由運用！以宿霧為例，當地有許多的Mall可以逛，也有很多特別的餐廳。宿霧是度假大城，海灘水上活動當然是不可缺少的，安排一個假日，幾個異國好友揪團出發，一起留下最特別的回憶！</span></p>
<p>&nbsp;<img title=""cebu-beach"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa79b7e154-0yJm3fHevuO3aB3HddiuLcAMMEVSYx-1401x748.jpeg"" alt=""cebu-beach"" width=""100%"" height=""auto"" /></p>
<h2>&nbsp;</h2>
<h3><span style=""font-weight: 400;"">【Mall必去!】</span></h3>
<p><span style=""font-weight: 400;"">平時在宿霧市區可以搭乘菲律賓的特色交通工具「<a title=""吉普尼"" href=""https://zh.wikipedia.org/wiki/%E5%90%89%E6%99%AE%E5%B0%BC"">吉普尼</a>」，隨叫隨停，一趟只要7披索，非常划算，但是在車內要小心自己的貴重物品，而且要好好把握和當地人交談和練習英文的機會，大部分的當地人都是很友善的，也很樂意幫忙告知你要下車的地點。</span></p>
<p><span style=""font-weight: 400;"">宿霧當地有三個主要大Mall：<a title=""Gaisano Country Mall"" href=""http://gaisanocountrymall.com/?nr=0"">Country Mall</a>、<a title=""Ayala Mall"" href=""http://www.ayalamalls.com.ph/"">Ayala Mall</a>、<a title=""SM mall"" href=""https://www.smsupermalls.com/mall-locator/sm-mall-of-asia/information"">SM Mall</a></span></p>
<p>Mall裡有許多國際品牌如：H&amp;M、屈臣氏，也有電影院，讓你在當地也不錯過首輪電影，因為沒有中文字幕，還可以趁機練習英文聽力呢！</p>
<p><span style=""font-weight: 400;"">這邊也教大家簡單的交通方式，從Country Mall 往Ayala Mall 可以搭乘13C吉普尼；從Ayala Mall 往SM Mall 搭乘40l吉普尼就可以到達。</span></p>
<p><img title=""country-mall"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa47b8c969-rIcAJRv0DYMIDR7RETt7XgZaRpmYCe-1280x585.jpeg"" alt=""country-mall"" width=""100%"" height=""auto"" /></p>
<h2><span style=""font-weight: 400;""><img title=""Ayala-mall"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa4b580716-ETvIPgLWBed5ohBkYY6qQxhdP7QCHv-768x480.jpeg"" alt=""Ayala-mall"" width=""100%"" height=""auto"" /></span></h2>
<h2><span style=""font-weight: 400;""><img title=""SM-mall"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa50b62a5e-EuFfRPzETcRFrksR5hgo07GfkZElo1-839x472.jpeg"" alt=""SM-mall"" width=""100%"" height=""auto"" /></span></h2>
<h2>&nbsp;</h2>
<h3><span style=""font-weight: 400;"">【小零食必買！】</span></h3>
<p><span style=""font-weight: 400;"">菲律賓當地有許多好吃的小零食，平常到超級市場逛逛可以發現不少特別又便宜的小點心，很適合放在房間中邊讀書邊品嚐當地美味。（菲律賓換算台幣約直接乘以0.6）</span></p>
<p><strong>巧克力芒果乾 一包87.5比索（約50多塊台幣）</strong></p>
<p><img title=""Mango-chocolate"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa5b69a3a6-6rBV2z1FhqdDlJB5yKRBsaxiKY2DvD-1000x576.jpeg"" alt=""Mango-chocolate"" width=""100%"" height=""auto"" /></p>
<p>&nbsp;</p>
<p><strong>7D芒果汁 一杯11比索（約10塊台幣）</strong></p>
<p><img title=""Mango-juice"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa5eec9af2-OzvYNc4xP28ksPkNcaRdjzIwnxWXb6-800x800.png"" alt=""Mango-juice"" width=""100%"" height=""auto"" />&nbsp;</p>
<h3><span style=""font-weight: 400;"">【高CP值餐廳必吃！】</span></h3>
<p><span style=""font-weight: 400;"">菲律賓曾受西班牙殖民，美食也深受西班牙風味影響。菲律賓的菜式猶如它的文化一樣，在食物種類和烹調方法上都反映出多元種族融合的特色。</span></p>
<p><strong>當地餐廳飲食習慣：</strong></p>
<p><strong>宿霧的大多餐廳都會提供兩種飯 一般plain和大蒜garlic</strong></p>
<p><strong>當地人用餐習慣一人點一杯飲料，但不會強迫</strong></p>
<p><strong>餐廳結帳會收5-10%服務費，不必另給服務生小費</strong></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">這邊我們和大家分享兩家便宜好吃適合學生三五好友一起共享的餐廳！</span></p>
<h3><a title=""Zubu Chon"" href=""https://www.tripadvisor.com.tw/Restaurant_Review-g298460-d2338110-Reviews-Zubuchon-Cebu_City_Cebu_Island_Visayas.html"">Zubu Chon</a>&nbsp;推薦第一名 烤豬腹肉</h3>
<p><span style=""font-weight: 400;"">3-4人份330比索（約198台幣） </span><span style=""font-weight: 400;"">6-7人份440比索（約246台幣）</span></p>
<p><span style=""font-weight: 400;"">菲律賓有名的烤乳豬店，超高CP值，一個人分下來吃飽飽不用台幣200！</span></p>
<p><span style=""font-weight: 400;""><img title=""zubu-store"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa694dfd26-jhg2ut3CPD16GLIcZmn0d6G7eDO8Mg-600x450.jpeg"" alt=""zubu-store"" width=""100%"" height=""auto"" /></span></p>
<p><img title=""zubu"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa66953d63-C6B1dSdijFbdnPQ9LjpXHZGZvKLKN5-640x360.jpeg"" alt=""zubu"" width=""100%"" height=""auto"" />&nbsp;</p>
<h3><span style=""font-weight: 400;""><a title=""Jollibee"" href=""http://www.jollibee.com.ph/"">Jollibee</a> 在地速食店 菲律賓的麥當勞</span></h3>
<p><span style=""font-weight: 400;"">平均一份餐才100比索（約60台幣）超高CP值，不只有常見的漢堡薯條，也有飯類可以選擇！適合坐下來與朋友聊天一起分享。</span></p>
<h2><img title=""jollibee-store"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa4ab731a3-m65Oz4xKuaR6o20karNodfKbFw9U4d-640x360.jpeg"" alt=""jollibee-store"" width=""100%"" height=""auto"" /><br /><img title=""jollibee-meal"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa6e22297f-fkLYdeIH9lLyQ6vJj3keONWHpPw61T-1200x630.png"" alt=""jollibee-meal"" width=""100%"" height=""auto"" /></h2>
<h2>&nbsp;</h2>
<h3><span style=""font-weight: 400;"">【水上活動必玩！】</span></h3>
<p><span style=""font-weight: 400;"">宿霧是度假天堂，海灘超美，炎熱的夏日最適合從事水上活動，體驗刺激有趣的拖曳傘、水上摩托車、香蕉船。有名的鯨鯊共游也是不能錯過的經典行程！</span></p>
<h1><img title=""beach-activity"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa7109ee92-zxh7TAsmGZEtsSloSU7QONhsczTfWL-1080x810.jpeg"" alt=""beach-activity"" width=""100%"" height=""auto"" /><br /><img title=""whale"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa73a4f608-ppAvbjLHDNAbQKWn3seebFlemNNw5Q-3502x2335.jpeg"" alt=""whale"" width=""100%"" height=""auto"" /></h1>
<h1>&nbsp;</h1>
<h2><span style=""font-weight: 400;"">3.菲律賓天氣</span></h2>
<p><span style=""font-weight: 400;"">不同的地區、季節，天氣也有所差異，以下我們整理了大家常選擇的宿霧、碧瑤的氣候，幫助大家了解菲律賓的天氣。</span></p>
<h3><span style=""font-weight: 400;"">宿霧天氣：</span></h3>
<p><span style=""font-weight: 400;"">屬於海洋性熱帶氣候，高溫、潮濕、多雨；全年四季如夏，只有乾季、雨季之分。</span></p>
<p><span style=""font-weight: 400;"">每年的12月至次年5月氣候比較乾燥，溫度可高達37℃。6月至11月是雨季，也是颱風季節，要小心午後的短暫雷陣雨。溫度比較涼快，<strong>全年多數氣溫在25-35℃之間</strong>。</span></p>
<h3>&nbsp;</h3>
<h3><span style=""font-weight: 400;"">碧瑤天氣：</span></h3>
<p><span style=""font-weight: 400;"">碧瑤海拔1450米，氣候涼爽，<strong>全年平均溫度為17.7℃</strong>，因而被稱為""菲律賓的夏都""。</span></p>
<p><span style=""font-weight: 400;"">馬尼拉與碧瑤的溫差很大，當馬尼拉人在攝氏30度的炎熱高溫下煎熬時，碧瑤的天氣卻可以穿夾克。怕熱的學生可選擇這個氣候涼爽宜人的城市喔！</span></p>
<p><br /><br /><br /></p>
<h2><span style=""font-weight: 400;"">4.<a title=""去菲律賓遊學？菲律賓治安大解析"" href=""https://tw.english.agency/%E8%8F%B2%E5%BE%8B%E8%B3%93%E9%81%8A%E5%AD%B8-%E6%B2%BB%E5%AE%89-%E8%A7%A3%E6%9E%90"">菲律賓治安</a></span></h2>
<p><span style=""font-weight: 400;"">以宿霧而言，治安指數超越舊金山和紐約差不多。</span></p>
<p><span style=""font-weight: 400;"">台灣可說是治安很好的國家，已經習慣治安良好的我們，無論如何在國外都要更注意自己的安全，只要不要過度招搖財富、不走暗巷、結伴同行，菲律賓的治安是比許多大城市還好的！</span></p>
<p><br /><br /></p>
<h2><span style=""font-weight: 400;"">5.菲律賓生活其他注意事項</span></h2>
<ol>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">到Mall換錢比在機場、台灣的銀行更划算。跟台幣相比的話，帶美金來菲律賓換成菲律賓披索會更划算。換匯時建議一次用50或100面額的美金來換，如果你用25美金以下的鈔票來換的話，匯率會比較差一點喔！</span></li>
<li style=""font-weight: 400;""><span style=""font-weight: 400;"">坐計程車要小心，一定要跟司機溝通好是跳表（pay by meter）最後才付錢，免得最後起爭議。</span></li>
</ol>
<p>&nbsp; &nbsp; &nbsp; &nbsp; 更多菲律賓小知識：<a href=""http://www.norraytvl.com/common/travel_information/ph/ph.htm"">http://www.norraytvl.com/common/travel_information/ph/ph.htm&nbsp;</a></p>
<p><br /><br />看了這麼多<a title=""菲律賓遊學好嗎？菲律賓語言學校費用、代辦最佳挑選辦法告訴你！"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>生活的介紹，有沒有對菲律賓遊學更加認識了呢？</p>
<p>不同的學校、地區都有不同的特色，不管你是在<a title=""宿霧遊學推薦、心得、評價說明，你不可不知的宿霧語言學校特色大公開!"" href=""https://tw.english.agency/cebu-esl-school"">宿霧遊學</a>、<a title=""碧瑤遊學Baguio：加強英文，海外遊學｜菲律賓語言學校・遊學推薦"" href=""https://tw.english.agency/baguio-study-abroad"">碧瑤語言學校</a>還是其他城市，好好把握18週的課程，努力學習、練習，不但英文會進步，也能養成自己靜下來讀書做事的能力，與他國學生文化交流更能讓你結交到異國好友，這段時間將是一生中很棒的體驗和回憶！</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '菲律賓語言學校心得-菲律賓遊學生活小知識','og_description' => '菲律賓語言學校以斯巴達式教學著名，密集式英文學習，全方位加強不論聽、說、讀、寫或是多益、托福、雅思測驗皆能讓你在18周的課程內提升能力。很多人想到菲律賓學英文，但是就算代辦和你講解了許多，還是覺得少了一點什麼嗎？我們搜集了大家的心得和經驗整理出包含課程、課餘、旅遊、飲食、天氣等五大項去菲律賓遊學生活小知識！','meta_title' => '菲律賓語言學校心得-菲律賓遊學生活小知識','meta_description' => '菲律賓語言學校以斯巴達式教學著名，密集式英文學習，全方位加強不論聽、說、讀、寫或是多益、托福、雅思測驗皆能讓你在18周的課程內提升能力。很多人想到菲律賓學英文，但是就算代辦和你講解了許多，還是覺得少了一點什麼嗎？我們搜集了大家的心得和經驗整理出包含課程、課餘、旅遊、飲食、天氣等五大項去菲律賓遊學生活小知識！','canonical_url' => 'https://tw.english.agency/philippines-language-school-review','feature_image_alt' => 'philippines-study-aboard-school-review','feature_image_title' => 'philippines-study-aboard-school-review','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7aa3a594f0e-83SQ1eIN4LZUXKSMx6z3xFeQDV3hCZ-1200x600.png','author_id' => '15','category_id' => '1','created_at' => '"2018-02-07 06:55:47"'],
  ['id' => '118','title' => '對菲律賓遊學還是一知半解？你一定想知道的菲律賓遊學問題Q&A大公開！','slug' => 'study-aboard-philippine-q&a','excerpt' => '相信到菲律賓遊學對大家來說已經不是一件陌生的事情，不過畢竟要到國外待一兩個月，心裡還是會有些怕怕的不知道到那邊能不能適應，也一定有很多疑問，別擔心！今天就一次幫你們分成三階段解答菲律賓遊學最常見的問題，想出國讀書的你還不趕快看下去！','content' => '"<p>相信到<a title=""菲律賓遊學好嗎？菲律賓語言學校費用、代辦最佳挑選辦法告訴你！"" href=""https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>對大家來說已經不是一件陌生的事情，不過畢竟要到國外待一兩個月去念<a title=""前往菲律賓語言學校列表"" href=""https://tw.english.agency/schools"">菲律賓語言學校</a>，心裡還是會有些怕怕的不知道到那邊能不能適應，也一定有很多疑問，別擔心！今天就一次幫你們分成三階段解答菲律賓遊學常見問題，想出國讀書的你還不趕快看下去！</p>
<p><strong><span style=""text-decoration: underline; color: #c63300; font-size: 20px;"">觀望菲律賓遊學階段</span></strong></p>
<p>&nbsp;</p>
<p style=""font-size: 18px;""><strong>菲律賓遊學大概要多少錢？</strong></p>
<p><br /> 一般來說菲律賓遊學的價格約落在四萬五到五萬不等，其中包含吃住和洗衣費，而<a href=""https://www.skyscanner.com.tw/?ksh_id=8dfed10e-bead-41c5-94d3-b6847261011d&amp;associateID=SEM_GGT_00065_00032&amp;utm_source=google&amp;utm_medium=cpc&amp;utm_campaign=TW-Travel-Search-Brand-Skyscanner%20Only-Exact&amp;utm_term=skyscanner&amp;gclid=Cj0KCQiAh_DTBRCTARIsABlT9MYl6ZA9xa8nVInNCLA5WjBZO1bWq1MWKJxGM0WqzbVenKYHXBnxF_saAhzMEALw_wcB&amp;associateid_proc=true"">廉航機票</a>則差不多五千到八千左右，再加上簽證費一千二，總共差不多需準備七萬是最保險的喔！</p>
<p style=""font-size: 18px;""><br /> <strong>菲律賓安全嗎？</strong></p>
<p><br /> 其實這會因地區而異，首都<a href=""https://zh.wikipedia.org/zh-tw/%E9%A9%AC%E5%B0%BC%E6%8B%89"">馬尼拉</a>相對來說不算治安很好，晚上也比較多閒雜人等出沒，而<a href=""https://zh.wikipedia.org/wiki/%E5%AE%BF%E9%9C%A7%E7%9C%81"">宿霧</a>、<a href=""https://zh.wikipedia.org/wiki/%E7%A2%A7%E7%91%B6%E5%B8%82"">碧瑤</a>等城市治安就會比較好，不過其實再安全的地方也會有危險發生的可能，而且只要在公共場所要進出一定都會有警衛守門，所以只要你保護好自己，晚上盡量不要自己在外走動，在菲律賓就算是很安全的了！</p>
<p style=""font-size: 18px;""><strong>菲律賓的老師會有口音嗎？</strong></p>
<p><br />很多人都會擔心去遊學時，菲律賓的老師會有很重的口音讓人聽不懂，但其實當地的老師都受過很多年的訓練，要說完全沒有口音是不可能的，但至少是能夠清楚表達而不會讓人聽不懂的發音，擔心會學到菲式英文的人可以放一百二十個心啦！<br /> <strong><br /> <span style=""text-decoration: underline; color: #c63300; font-size: 20px;"">考慮菲律賓遊學階段</span></strong></p>
<p style=""font-size: 18px;""><strong>簽證需要事先申請嗎？</strong></p>
<p><br /> 從台灣到菲律賓一定需要申請簽證，一般的簽證可以在當地停留三十天，而且現在用<a href=""https://onlinetravel.meco.org.tw/EVISA/"">線上申請簽證</a>也很方便快速，最快五分鐘就可以拿到簽證，只需要在登機前列印出來即可，而如果預計會在菲律賓待上一個月以上的時間，則需到<a href=""https://www.meco.org.tw/"">馬尼拉經濟文化辦事處</a>申請59天效期的紙本簽證。</p>
<p style=""font-size: 18px;""><strong>什麼是SSP？什麼是ACR-I卡？</strong></p>
<p><br /> SSP(Special Study permit)為學生就讀許可證，是一種針對以觀光為目的入境的外國人在菲律賓國内合法入學時必備的許可證，而SSP發行單位為菲律賓法務省管轄的移民局，不管你在菲律賓遊學或打工等都要申請這張證件，一般來說，語言學校會幫忙辦理SSP，費用約在6000～6800披索之間。<br /> <br /> 只要你是以觀光為目的長期留在菲律賓的外國人，或因遊學而在菲律賓停留兩個月以上的外國籍人士，都必須辦理外國人登錄證ACR-I卡。在移民局登錄過的外國人即可領到ACR-I卡，是合法可停留在菲律賓的法律正式文件，也是簡易的身分證證明。各語言學校亦會代為辦理ACR-I卡，費用約在3000～3500披索之間。</p>
<p>延伸閱讀：<a href=""https://tw.english.agency/philippines-visa-application"">菲律賓簽證、SSP申請一把抓！輕鬆搞定，菲律賓遊學簽證那些大小事！</a></p>
<p style=""font-size: 18px;""><strong>遊學該選哪個城市好？</strong></p>
<p><br /> 目前菲律賓最熱門的遊學城市有四個，分別是馬尼拉、克拉克、碧瑤和宿霧，每個城市的學校教學模式不盡相同，有些是很嚴格的斯巴達教育，有些則是很自由的歐美教育，而不同城市的特色也不太一樣，碧瑤位於山區，氣溫陰涼舒適，也很適合閉關學習，位於海邊的宿霧適合愛玩水的人，馬尼拉適合喜歡夜生活和方便交通的人，而克拉克則適合想邊遊學邊度假的人，建議你可以根據自己的需求來選擇最適合的城市喔！</p>
<p>延伸閱讀：<a href=""https://tw.english.agency/philippines-study-aboard-city"">菲律賓地區介紹，宿霧、碧瑤、馬尼拉、克拉克快速解析！</a></p>
<p><br /> <span style=""text-decoration: underline; color: #c63300; font-size: 20px;""><strong>決定去菲律賓遊學階段</strong></span></p>
<p style=""font-size: 18px;""><strong>學校接機有時間限制嗎？要怎麼跟他們聯繫？</strong></p>
<p><br /> 一般來說，每個學校都會訂定固定的接機日，宿霧地區的學校大多是固定會在禮拜天接機，而碧瑤地區則因爲路途遙遠，會有特定的接機時間和地點，建議大家出發前先和顧問確認接機時間，下飛機後購買當地電話卡聯絡接機人員，就不怕在機場乾等啦！</p>
<p style=""font-size: 18px;""><strong>在菲律賓可以用ATM領錢嗎？</strong></p>
<p><br /> 如果你擔心帶去的現金不夠，又怕路邊的小攤販沒辦法刷卡的話，當然是可以在菲律賓用ATM領錢，只要你在出發前記得先去銀行申請國際金融卡，這樣就可以輕鬆在當地領出披索啦！不過值得注意的是，在國外領錢會收兩次的手續費，所以建議能不在國外領錢就盡量不要領喔！</p>
<p style=""font-size: 18px;""><strong>要辦菲律賓當地的上網卡或電話卡嗎？</strong></p>
<p><br /> 大部分的學校都有提供固定的wifi可以使用，機場或是比較大型的建築物也都有提供網路，不過如果你喜歡無時無刻都用手機上網的話，也可以在機場辦理smart或globe兩家電信公司的上網卡，由於菲律賓的網路其實並不算太發達，所以電話卡也是很重要的，記得要辦一張預付卡才方便聯絡當地協助遊學的人員喔！</p>"','published' => '1','og_title' => '對菲律賓遊學還是一知半解？你一定想知道的菲律賓遊學問題Q&A大公開！','og_description' => '相信到菲律賓遊學對大家來說已經不是一件陌生的事情，不過畢竟要到國外待一兩個月，心裡還是會有些怕怕的不知道到那邊能不能適應，也一定有很多疑問，別擔心！今天就一次幫你們分成三階段解答菲律賓遊學最常見的問題，想出國讀書的你還不趕快看下去！','meta_title' => '對菲律賓遊學還是一知半解？你一定想知道的菲律賓遊學問題Q&A大公開！','meta_description' => '相信到菲律賓遊學對大家來說已經不是一件陌生的事情，不過畢竟要到國外待一兩個月，心裡還是會有些怕怕的不知道到那邊能不能適應，也一定有很多疑問，別擔心！今天就一次幫你們分成三階段解答菲律賓遊學最常見的問題，想出國讀書的你還不趕快看下去！','canonical_url' => 'https://tw.english.agency/question-and-answer-about-phillipines-study-tour','feature_image_alt' => '菲律賓遊學問答','feature_image_title' => '菲律賓遊學問答','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7d31b7d5082-oeF9YcIdxOLZ2pL9LPSCah6ESPJ2OF-1022x616.jpeg','author_id' => '14','category_id' => '1','created_at' => '"2018-02-09 05:41:01"'],
  ['id' => '119','title' => '克拉克遊學值得你去嗎？三個你該去克拉克遊學的理由！','slug' => 'study-aboard-philippine-clark','excerpt' => '一講到菲律賓遊學，大家心裡應該都會冒出馬尼拉或是宿霧這兩個有班機直飛的大城市，不過其實最近也有越來越多人選擇的充滿異國風情的克拉克遊學，到底這些人為什麼會想選擇到交通相對不便的克拉克遊學呢？今天就來介紹克拉克這座美麗的城市，順便告訴你去克拉克遊學的三大好處，還沒想好要去哪個城市遊學的你一定要看的啊！','content' => '"<div style=""line-height: auto; font-size: 18px;"">一講到<a title=""菲律賓遊學好嗎？菲律賓語言學校費用、代辦最佳挑選辦法告訴你！"" href=""https://tw.english.agency/https://tw.english.agency/philippines-study-aboard"">菲律賓遊學</a>，大家心裡應該都會冒出馬尼拉或是<a href=""https://tw.english.agency/cebu-esl-school"">宿霧</a>這兩個有班機直飛的大城市，不過其實最近也有越來越多人選擇的充滿異國風情的克拉克遊學，到底這些人為什麼會想選擇到交通相對不便的克拉克遊學呢？今天就來介紹克拉克這座美麗的城市跟他們的<a href=""https://tw.english.agency/schools"">菲律賓語言學校</a>，順便告訴你去克拉克遊學的三大好處，還沒想好要去哪個城市遊學的你一定要看的啊！
<h2 style=""color: #c63300; font-size: 20px;"">&nbsp;</h2>
<h2 style=""color: #c63300; font-size: 20px;""><strong>菲律賓遊學地區介紹-</strong><strong>克拉克遊學簡介</strong></h2>
<p>克拉克是過去美軍在菲律賓駐點的空軍基地，也因此吸收了不少歐美文化精華，同時，曾經是歐美在東南亞最大貿易口的克拉克也逐漸成為國際最大的旅遊中心，吸引來自世界各地的旅客爭相前往，克拉克風景優美、航運發達、度假村林立且設備發達，可以說是菲律賓最適合度假放鬆的城市之一！<br /> <strong><br /> </strong></p>
<p style=""color: #c63300; font-size: 20px;""><strong>克拉克遊學的三大好處</strong></p>
<p style=""color: #c63300; font-size: 20px;"">&nbsp;</p>
<p style=""color: #c63300;""><strong>1.外籍師資超級多</strong></p>
<p>曾經為美軍統治時最大殖民地的克拉克，受了不少歐美文化的薰陶，同時也是菲律賓最多歐美人士居住的地方，如果你很擔心跟當地師資學習英文口說會有當地口音的話，來克拉克遊學就絕對不用擔心這個困擾！</p>
<p style=""color: #c63300; font-size: 18px;""><strong> 2.教育模式有助學習<br /> </strong></p>
<p>克拉克的學校多半採取半斯巴達教育模式，不用擔心到那邊遊學會有像去碧瑤一樣整天都必須待在學校裡學習的情形，半斯巴達教育的好處是老師該逼你的時候會逼很緊，該放鬆的時候則會讓你超級放鬆，如果你自認是不太能自律又需要適當休息的人，選擇克拉克的學校就對啦！</p>
<p style=""color: #c63300; font-size: 18px;""><strong>3.度假氣息濃厚適合放鬆</strong></p>
<p>有小加州之稱的克拉克擁有上百家度假村，每家度假村都有各自的特色，有的裡面還有水上樂園和高爾夫球場，在讀書讀到煩心不已的時候有這些好地方可以去真是太讚了！除此之外，克拉克市區購物中心林立，除了可以shopping外，有的購物中心還設有健身房、理髮廳和按摩店等等設施，讓你可以完全在這邊好好放鬆！</p>
<p style=""color: #c63300; font-size: 20px;"">&nbsp;</p>
<p style=""color: #c63300; font-size: 20px;""><strong>克拉克學校介紹</strong></p>
<ul>
<li><a href=""http://www.egesl.com/jp/"">Eg語言學校</a>：一般密集式英文、半斯巴達式、斯巴達課程為主，菲籍與外師共同授課。</li>
<li><a href=""https://www.cipenglishschool.com/zh/"">CIP語言學校</a>：強調全英語環境，以訓練口說為主要的核心。</li>
<li><a href=""https://helpenglish.com.tw/"">Help語言學校克拉克校</a>區：斯巴達教學的始祖，專業的斯巴達課程教學</li>
</ul>
<p>看完上述介紹後是不是讓大家更加了解克拉克啦？喜歡克拉克這種風格的你別再猶豫，趕快在申請書填上克拉克吧！</p>
<p>&nbsp;</p>
</div>"','published' => '1','og_title' => '要不要去克拉克遊學呢？三個你該去克拉克遊學的理由！','og_description' => '一講到菲律賓遊學，大家心裡應該都會冒出馬尼拉或是宿霧這兩個有班機直飛的大城市，不過其實最近也有越來越多人選擇的充滿異國風情的克拉克遊學，到底這些人為什麼會想選擇到交通相對不便的克拉克遊學呢？今天就來介紹克拉克這座美麗的城市，順便告訴你去克拉克遊學的三大好處，還沒想好要去哪個城市遊學的你一定要看的啊！','meta_title' => '要不要去克拉克遊學呢？三個你該去克拉克遊學的理由！','meta_description' => '一講到菲律賓遊學，大家心裡應該都會冒出馬尼拉或是宿霧這兩個有班機直飛的大城市，不過其實最近也有越來越多人選擇的充滿異國風情的克拉克遊學，到底這些人為什麼會想選擇到交通相對不便的克拉克遊學呢？今天就來介紹克拉克這座美麗的城市，順便告訴你去克拉克遊學的三大好處，還沒想好要去哪個城市遊學的你一定要看的啊！','canonical_url' => 'https://tw.english.agency/three-reason-for-going-to-Clark-study-tour','feature_image_alt' => '克拉克遊學理由','feature_image_title' => '克拉克遊學理由','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a7d3dc954168-dsmPLZyx6N5ps7qHTnFI0AicAViJnv-1200x630.jpeg','author_id' => '14','category_id' => '1','created_at' => '"2018-02-09 06:51:16"'],
  ['id' => '120','title' => '「你今天吃雞了嗎？」從Freefire學英文','slug' => 'mobile-game-freefire-english','excerpt' => '吃雞遊戲－絕地求生Freefire是最近的熱門手遊，那你知道這個為什麼要「吃雞」嗎？今天就帶大家從Freefire中學習實用的英文知識！','content' => '"<p>吃雞遊戲－絕地求生Freefire是最近的熱門手遊，那你知道這個為什麼要「吃雞」嗎？今天就帶大家從Freefire中學習實用的英文知識！</p>
<p>&nbsp;</p>
<p>在遊戲中，當你擊殺所有玩家獲得最後冠軍時，就會有一句話出現「大吉大利，晚上吃雞」，這也是為什麼大家叫他吃雞遊戲。那為什麼要吃雞呢？其實來源是美國電影21中的台詞<em><span style=""color: #8e2800;"">Winner winner chicken dinner.</span></em></p>
<p>在賭城Las Vegas 有一種飯，飯裡有三塊雞肉價值1.79 dollars，而贏一次賭局的標準回饋是2 dollars，所以Winners賭贏的錢就剛好可以買一份Chicken dinner。</p>
<p>&nbsp;</p>
<p><strong><span style=""color: #eb7f00;"">Chicken</span></strong> ➪雞</p>
<ul>
<li>Chicken 也有膽小鬼、懦夫(coward)的意思</li>
<li>Chicken out 指臨陣退縮</li>
</ul>
<p><strong><em><span style=""color: #225378;"">You don&rsquo;t want to get into this haunted house? Come on! Let&rsquo;s go! Don&rsquo;t be such a chicken!</span></em></strong></p>
<p>你不想進去這間鬧鬼的房子嗎？快點！我們走嗎！別當膽小鬼了！</p>
<p><strong><span style=""color: #eb7f00;"">Rooster </span></strong>➪公雞</p>
<ul>
<li>The year of Rooster 雞年</li>
</ul>
<p><strong><span style=""color: #eb7f00;"">Hen</span></strong> ➪母雞</p>
<p><strong><span style=""color: #eb7f00;"">Chick</span></strong> ➪小雞</p>
<ul>
<li>Chick在口語中也有指女生、小妞的意思 ，在饒舌歌曲中常聽到</li>
</ul>
<p>Ex. <em><strong><span style=""color: #225378;"">Do you see that chick on the dancefloor? She is so sexy!</span> </strong></em></p>
<p>你看到那個在舞池跳舞的女生了嗎？她好性感！</p>
<p><strong><span style=""color: #eb7f00;"">Turkey</span></strong> ➪火雞<br /><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a82a369d5b8b-xJvNN3Ob7AdkoekIsgFAzUVwkweNTa-1080x608.jpeg"" alt="""" width=""690"" height=""400"" /></p>
<p>絕地求生是一款射擊大逃殺遊戲，遊戲中不可或缺的元素包括跳傘、找尋物資、槍枝武器等等，英文說法分別為：</p>
<p><strong><span style=""color: #eb7f00;"">Shoot</span></strong> ➪射擊</p>
<ul>
<li>Shoot也有拍攝照片、影片的意思，例如 Shoot films 拍攝影片、Shoot photo拍照</li>
<li>Shoot range ➪射擊範圍</li>
<li>Shoot the target ➪射擊指定目標</li>
</ul>
<p><strong><span style=""color: #eb7f00;"">Skydiving </span></strong>➪高空跳傘</p>
<p><strong><span style=""color: #eb7f00;"">Parachute</span></strong> ➪降落傘</p>
<p>常見的極限運動(Extreme Sports)還包含了：</p>
<p><strong><span style=""color: #eb7f00;"">Diving</span></strong> ➪潛水</p>
<p><strong><span style=""color: #eb7f00;"">Skuba diving </span></strong>➪水肺潛水</p>
<p><strong><span style=""color: #eb7f00;"">Snorkel diving</span></strong> ➪浮潛</p>
<p><strong><span style=""color: #eb7f00;"">Bungee jumping</span></strong> ➪高空彈跳</p>
<p><strong><span style=""color: #eb7f00;"">Surfing </span></strong>➪衝浪</p>
<p><strong><span style=""color: #eb7f00;"">Parkour</span> </strong>➪跑酷</p>
<p><strong><span style=""color: #eb7f00;"">War game </span></strong>➪生存遊戲</p>
<ul>
<li>生存遊戲的英文說法還有 Survival game 和Airsoft gun 則是指軟氣槍</li>
</ul>
<p>Freefire 中的槍枝種類包含了：</p>
<p>Handgun ➪手槍</p>
<p>Pistol ➪單膛式手槍</p>
<p>Revolver ➪左輪</p>
<p><strong><span style=""color: #eb7f00;"">Shotgun (SG) </span></strong>➪散彈槍</p>
<p><strong><span style=""color: #eb7f00;"">Submachine gun (SMG) </span></strong>➪衝鋒槍</p>
<p><strong><span style=""color: #eb7f00;"">Machine gun (MG) </span></strong>➪機關槍</p>
<p><strong><span style=""color: #eb7f00;"">Sniper rifle</span></strong> ➪狙擊步槍</p>
<p><strong><span style=""color: #eb7f00;"">Assault rifle </span></strong>➪突擊步槍</p>
<h3>一些關於槍枝的用語：</h3>
<p><strong><span style=""color: #eb7f00;"">Power </span></strong>➪火力</p>
<p><strong><span style=""color: #eb7f00;"">Range </span></strong>➪範圍</p>
<p><strong><span style=""color: #eb7f00;"">Accuracy</span> </strong>➪準確性</p>
<p><strong><span style=""color: #eb7f00;"">Rate of fire </span></strong>➪射擊速度</p>
<h3>投擲類的武器則有：</h3>
<p><strong><span style=""color: #eb7f00;"">Grenade</span></strong> ➪手榴彈</p>
<p><strong><span style=""color: #eb7f00;"">Stun grenade</span></strong> ➪震撼彈</p>
<p><strong><span style=""color: #eb7f00;"">Smoke grenade</span></strong> ➪煙霧彈</p>
<h3>遊戲中的裝備、配件包含：</h3>
<p><strong><span style=""color: #eb7f00;"">Ammo</span></strong> ➪彈藥</p>
<p><strong><span style=""color: #eb7f00;"">Bullet</span></strong> ➪子彈</p>
<p><strong><span style=""color: #eb7f00;"">Bullet loops</span></strong> ➪子彈袋</p>
<p><strong><span style=""color: #eb7f00;"">QuickDraw mag</span></strong> ➪快速彈夾</p>
<p><strong><span style=""color: #eb7f00;"">Extended mag</span></strong> ➪擴容彈夾</p>
<p><strong><span style=""color: #eb7f00;"">Dual mag</span></strong> ➪雙彈夾</p>
<p><strong><span style=""color: #eb7f00;"">Flash hider muzzle</span></strong> ➪消焰槍口</p>
<p><strong><span style=""color: #eb7f00;"">Suppressor muzzle</span></strong> ➪消音槍口</p>
<p><strong><span style=""color: #eb7f00;"">Iron sight </span></strong>➪機械瞄準具</p>
<p><strong><span style=""color: #eb7f00;"">Telescopic sight</span></strong> ➪瞄準鏡</p>
<p><strong><span style=""color: #eb7f00;"">Night vision</span></strong> ➪夜視鏡</p>
<p><strong><span style=""color: #eb7f00;"">Pan </span></strong>➪平底鍋</p>
<h3>補給品則包含了：</h3>
<p><strong><span style=""color: #eb7f00;"">Helmet</span></strong> ➪頭盔 平常騎機車戴的安全帽也是這個字</p>
<p><strong><span style=""color: #eb7f00;"">Vest</span></strong> ➪背心</p>
<p><strong><span style=""color: #eb7f00;"">Backpack</span></strong> ➪背包</p>
<p><strong><span style=""color: #eb7f00;"">Airdrop</span></strong> ➪空投</p>
<p><strong><span style=""color: #eb7f00;"">Vehicle </span></strong>➪交通</p>
<p><strong><span style=""color: #eb7f00;"">Jeep</span></strong> ➪吉普車</p>
<h3>在Freefire中，地形是非常重要的一件事，每個地形的物資匱乏、隱蔽程度成了能不能吃到雞的關鍵：</h3>
<p><strong><span style=""color: #eb7f00;"">Shipyard</span></strong> ➪船廠</p>
<p><strong><span style=""color: #eb7f00;"">Peak</span></strong> ➪高峰</p>
<p><strong><span style=""color: #eb7f00;"">Hill</span></strong> ➪山丘</p>
<p><strong><span style=""color: #eb7f00;"">Town </span></strong>➪城鎮</p>
<p><strong><span style=""color: #eb7f00;"">Riverside</span></strong> ➪河邊</p>
<p><strong><span style=""color: #eb7f00;"">Wreckage</span></strong> ➪船廠</p>
<p><strong><span style=""color: #eb7f00;"">Ranch</span></strong> ➪牧場</p>
<p><strong><span style=""color: #eb7f00;"">Hangar</span></strong> ➪機庫</p>
<p><strong><span style=""color: #eb7f00;"">Plantation</span></strong> ➪種植園</p>
<p><strong><span style=""color: #eb7f00;"">Factory</span></strong> ➪工廠</p>
<p><strong><span style=""color: #eb7f00;"">Office</span></strong> ➪辦公室</p>
<p><strong><span style=""color: #eb7f00;"">Cabin</span></strong> ➪木屋</p>
<p><strong><span style=""color: #eb7f00;"">Garage</span></strong> ➪車庫</p>
<p><strong><span style=""color: #eb7f00;"">Warehouse</span></strong> ➪倉庫</p>
<p><strong><span style=""color: #eb7f00;"">Secure zone</span></strong> ➪安全區域</p>
<p><strong><span style=""color: #eb7f00;"">Red zone</span></strong> ➪轟炸區</p>
<h3>另外，遊戲的類別很常是用縮寫來表示，他們分別代表的意思是：</h3>
<p><strong><span style=""color: #eb7f00;"">RPG = Role-Playing Game</span></strong> ➪角色扮演遊戲</p>
<p><strong><span style=""color: #eb7f00;"">FPS = First-Person Shooting Game</span></strong> ➪第一人稱視角射擊遊戲</p>
<p><strong><span style=""color: #eb7f00;"">FTG = Fighting Game</span></strong> ➪格鬥類遊戲</p>
<p><strong><span style=""color: #eb7f00;"">ACT= Action Game</span></strong> ➪動作類遊戲</p>
<p><strong><span style=""color: #eb7f00;"">AVG = Adventure Game</span></strong> ➪冒險類遊戲</p>
<p><strong><span style=""color: #eb7f00;"">SLG = Strategy Game</span></strong> ➪策略類遊戲</p>
<p><strong><span style=""color: #eb7f00;"">SPT = Sports Game</span></strong> ➪運動類遊戲</p>
<p><strong><span style=""color: #eb7f00;"">PZL = Puzzle Game</span></strong> ➪益智類遊戲</p>
<p><strong><span style=""color: #eb7f00;"">VR = Virtual Reality </span></strong>➪虛擬實境</p>
<p><strong><span style=""color: #eb7f00;"">AR = Augmented Reality</span></strong> ➪擴增實境</p>
<p><strong><span style=""color: #eb7f00;"">MR = Mixed Reality</span></strong> ➪混合實境</p>
<p><strong>&nbsp;</strong></p>
<hr />
<h3>&nbsp;</h3>
<h3><strong>Dialogs </strong><strong>對話</strong></h3>
<p><em><strong><span style=""color: #225378;"">Hello，wanna team up?</span></strong></em></p>
<p>想組隊嗎？</p>
<p><em><strong><span style=""color: #225378;"">I can share you guys some medicine/guns/attachments.</span></strong></em></p>
<p>我可以分給你們一些補給品/槍枝/配備。</p>
<p><em><strong><span style=""color: #225378;"">I need more time looting.</span></strong></em></p>
<p>我需要更多時間搜刮資源。</p>
<p><em><strong><span style=""color: #225378;"">I have an extra SCAR here. Do you guys need a rifle? </span></strong></em></p>
<p>我這裡有把多餘的SCAR。你們要不要撿這把步槍?</p>
<p><em><strong><span style=""color: #225378;"">I don&rsquo;t have enough ammo. Can you give me some ammo? </span></strong></em></p>
<p>我沒有足夠的子彈了。你可以給我一些子彈嗎？</p>
<p><em><strong><span style=""color: #225378;"">There are two squads around me.</span></strong></em></p>
<p>在我附近有兩隊人</p>
<p><em><strong><span style=""color: #225378;"">I hear a car coming</span></strong></em></p>
<p>我聽見車子要過來了</p>
<p><em><strong><span style=""color: #225378;"">Oh! They found me! Cover me!</span></strong></em></p>
<p>噢！他們發現我了！掩護我</p>
<p><em><strong><span style=""color: #225378;"">Watch your back!</span></strong></em></p>
<p>注意你的身後！</p>
<p><em><strong><span style=""color: #225378;"">I&rsquo;ve got you back!</span></strong></em></p>
<p>我來罩你的後方！</p>
<p><em><strong><span style=""color: #225378;"">Take cover in buildings!</span></strong></em></p>
<p>進建築物找掩護!</p>
<p><em><strong><span style=""color: #225378;"">Beat a retreat into buildings!</span></strong></em></p>
<p>撤退至建築物!</p>
<p><em><strong><span style=""color: #225378;"">I need backup!</span></strong></em></p>
<p>我需要支援!</p>
<p><em><strong><span style=""color: #225378;"">Follow me. Let&rsquo;s kill them all.</span></strong></em></p>
<p>跟著我，我們殺光他們!</p>
<p><em><strong><span style=""color: #225378;"">Nice shot! </span></strong></em></p>
<p>幹得漂亮！</p>
<p><em><strong><span style=""color: #225378;"">Great game. Thank you all.</span></strong></em></p>
<p>漂亮的一場(遊戲)，謝謝你們</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '「你今天吃雞了嗎？」從Freefire學英文','og_description' => '「你今天吃雞了嗎？」從Freefire學英文','meta_title' => '「你今天吃雞了嗎？」從Freefire學英文','meta_description' => '吃雞遊戲－絕地求生Freefire是最近的熱門手遊，那你知道這個為什麼要「吃雞」嗎？今天就帶大家從Freefire中學習實用的英文知識！','canonical_url' => 'https://tw.english.agency/mobile-game-freefire-english','feature_image_alt' => '從Freefire學英文','feature_image_title' => '從Freefire學英文','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a83d42709a8b-rRbF0QP3kBBJuDXTAdVT7C2frJ8NSr-14173x7441.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-13 06:39:07"'],
  ['id' => '121','title' => '烹飪英文單字整理！','slug' => 'cooking-english-words-Consolidation','excerpt' => '想做出道地異國料理，卻看不懂英文食譜嗎？在國外餐廳打工，害怕聽不懂廚師間的專業烹飪術語嗎？現在就來學學這些常見又實用的烹飪英文吧！','content' => '"<p>想做出道地異國料理，卻看不懂英文食譜嗎？在國外餐廳打工，害怕聽不懂廚師間的專業烹飪術語嗎？現在就來學學這些常見又實用的烹飪英文吧！</p>
<h2><strong><span style=""color: #225378;"">烹調方式Cookery</span></strong></h2>
<ul>
<li><strong><span style=""color: #eb7f00;"">烹飪➩Cook</span></strong></li>
</ul>
<p>【易混淆字】 通常英文中，er結尾的字都是&hellip;人，例如Teacher老師、Farmer農夫、Driver司機、Dancer舞者。</p>
<p>但是，Cooker的意思是爐具！廚師是Cook or Chef (主廚)，像是上圖的帥哥主廚Curtis Stone就是Chef的代表啦</p>
<ul>
<li><strong><span style=""color: #eb7f00;"">水煮➩Boil</span></strong></li>
</ul>
<p><strong>Poach</strong> 也有水煮的意思，水煮蛋的英文是<strong>Poach eggs</strong></p>
<ul>
<li><strong><span style=""color: #eb7f00;"">炒➩Fry</span></strong></li>
</ul>
<p>炒飯的英文是<strong>Fried rice</strong>，依此類推：炒麵<strong> Fried noodle</strong></p>
<ul>
<li><strong><span style=""color: #eb7f00;"">煎炒➩Pan-fry</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">快炒➩Stir-fry</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">油炸➩Deep-fry</span></strong></li>
</ul>
<p>薯條 <strong>French fries</strong> 炸雞 <strong>Fried chicken</strong> 薯餅則叫 <strong>Hash browns</strong> 喔</p>
<ul>
<li><strong><span style=""color: #eb7f00;"">燒烤➩Barbecue</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">火烤➩Grill</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">煎烤➩Roast</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">烘焙➩Bake</span></strong></li>
</ul>
<p>簡單來說， <strong>Grill，Roast，Bake</strong> 就是「烤」的方式不同啦！</p>
<p><strong>Grill</strong>是將食物放在鐵網上用大火烤</p>
<p><strong>Roast</strong>是將食物放進烤箱或是炭火上烤</p>
<p><strong>Bake</strong>則是用烤箱烘焙蛋糕、麵包</p>
<ul>
<li><strong><span style=""color: #eb7f00;"">煙燻➩Smoke</span></strong></li>
</ul>
<p><strong>煙燻鮭魚</strong>的英文就是<strong>Smoked salmon</strong></p>
<ul>
<li><strong><span style=""color: #eb7f00;"">燉➩Stew</span></strong></li>
</ul>
<p><strong>燉飯</strong>的英文是<strong>Risotto</strong>，例如<strong>海鮮燉飯</strong>:<strong>Seafood risotto </strong></p>
<p>而<strong>Stew with rice</strong>是將燉好的肉與醬汁淋在飯上，比較像是中文所說的<strong>燴飯</strong></p>
<p>正統的肉跟飯都燉到爛的燉飯還是要用<strong>Risotto</strong>比較好喔</p>
<ul>
<li><strong><span style=""color: #eb7f00;"">蒸➩Steam</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">微波➩Nuke</span></strong></li>
</ul>
<p><strong>微波爐</strong>的英文是<strong>Microwave</strong></p>
<p><strong>&nbsp;</strong></p>
<h2><strong><span style=""color: #225378;"">食材處理Process Ingredients</span></strong></h2>
<ul>
<li><strong><span style=""color: #eb7f00;"">冷凍➩Freeze</span></strong></li>
</ul>
<p><strong>冷凍庫</strong>的英文是<strong>Freezer</strong>，<strong>冷藏</strong>用<strong>Fridge</strong>(<strong>冰箱</strong>)就可以啦</p>
<ul>
<li><strong><span style=""color: #eb7f00;"">解凍➩Defrost</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">削皮➩Peel</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">去鱗➩Scale</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">去核➩Core</span></strong></li>
</ul>
<p><strong>Core</strong>這個字也有<strong>核心</strong>的意思，像是健身的<strong>核心肌群</strong>英文就是<strong>Core muscle</strong></p>
<ul>
<li><strong><span style=""color: #eb7f00;"">切除不要的部分➩Trim</span></strong></li>
</ul>
<p><strong>剪輯影片</strong>的英文也可以用<strong>Trim</strong><strong>，</strong>就是在時間軸中處理、裁減的意思</p>
<ul>
<li><strong><span style=""color: #eb7f00;"">切成小塊➩Cube</span></strong></li>
</ul>
<p><strong>Cube</strong>也有<strong>立方體</strong>的意思，像是<strong>冰塊</strong>就是<strong>Ice cube</strong></p>
<ul>
<li><strong><span style=""color: #eb7f00;"">切成丁狀➩Dice</span></strong></li>
</ul>
<p><strong>Dice</strong>也是<strong>骰子</strong></p>
<ul>
<li><strong><span style=""color: #eb7f00;"">切薄片➩Slice</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">切絲➩Julienne</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">醃漬➩Pickle</span></strong></li>
<li><strong><span style=""color: #eb7f00;"">翻面➩Turnover/Flip</span></strong></li>
</ul>
<p><strong>&nbsp;</strong></p>
<hr />
<p>&nbsp;</p>
<p><strong>在學完了基本的烹調英文單字後，我們來試試看英文食譜吧！要來教大家做的是「芹菜炒魷魚」</strong></p>
<h2><strong><span style=""color: #225378;"">Celery Stir Fry Squid 芹菜炒魷魚</span></strong></h2>
<p><strong><span style=""color: #225378;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a83e7558035d-tGCjZLOskXjfAC2KmUyz1X7XxETRYA-300x300.jpeg"" alt="""" width=""300"" height=""300"" /></span></strong></p>
<h4><em><strong><span style=""color: #8e2800;"">Step1. Clean the squid，and remove all skin and intestine</span></strong></em></h4>
<p>第一步 清洗魷魚，並拔掉魷魚的皮與內臟</p>
<ul>
<li><strong>大腸</strong>的英文是<strong>Large intestine</strong><strong>，</strong>同理<strong>小腸</strong>就是<strong>Small intestine</strong></li>
</ul>
<h4><em><strong><span style=""color: #8e2800;"">Step2. Wash celery</span></strong></em></h4>
<p>第二步 洗芹菜</p>
<h3><strong><span style=""color: #225378;"">【補充】常見的蔬菜英文：</span></strong></h3>
<p><strong><span style=""color: #eb7f00;"">菠菜 Spinach</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">綠花椰菜 Broccoli</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">白花椰菜 Cauliflower</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">萵苣 Lettuce</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">高麗菜 Cabbage</span> </strong></p>
<p><strong><span style=""color: #eb7f00;"">韭菜 Garlic Chives</span></strong></p>
<h4><strong><em><span style=""color: #8e2800;"">Step3. Cut the squid into your desired shape and celery into bite sizes</span></em></strong></h4>
<p>第三步 將魷魚及芹菜切成您喜愛的、好入口的大小</p>
<h4><strong><em><span style=""color: #8e2800;"">Step4. Stir-fry garlic and oil to bring out the flavor</span></em></strong></h4>
<p>第四步 快炒油、蒜，爆香</p>
<h4><strong><em><span style=""color: #8e2800;"">Step5. Add in celery</span></em></strong></h4>
<p>第五步 將芹菜加入鍋</p>
<h4><strong><em><span style=""color: #8e2800;"">Step6. Flavor the dish into your liking (Recommend: Shacha sauce)</span></em></strong></h4>
<p>第六步 調味，調成自己喜歡的味道啦！ (推薦：沙茶醬)</p>
<p>&nbsp;</p>
<h3><strong><span style=""color: #225378;"">【補充】常見的調味料英文</span></strong></h3>
<p><strong><span style=""color: #eb7f00;"">番茄醬 Ketchup</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">黃芥末醬 Mustard</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">美乃滋Mayonnaise可以簡稱Mayo</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">辣椒醬Chili sauce</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">醬油 Soy sauce</span></strong></p>
<p><strong><span style=""color: #eb7f00;"">糖醋醬 Sweet and sour sauce</span></strong></p>
<p><strong>&nbsp;</strong></p>
<p>(食譜來自小編本人的廚神閨蜜 想深入學習請看她的教學影片https://www.youtube.com/watch?v=Nk6NuVS3nZQ)</p>"','published' => '1','og_title' => '烹飪英文單字整理！','og_description' => '烹飪英文單字整理！','meta_title' => '烹飪英文單字整理！','meta_description' => '想做出道地異國料理，卻看不懂英文食譜嗎？在國外餐廳打工，害怕聽不懂廚師間的專業烹飪術語嗎？現在就來學學這些常見又實用的烹飪英文吧！','canonical_url' => 'https://tw.english.agency/cooking-english-words-Consolidation','feature_image_alt' => '烹飪英文單字整理！','feature_image_title' => '烹飪英文單字整理！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a844488ea23f-IRDBhQaP5lO7Nq4QMb4I7YwhMJ4vPv-14173x7441.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-14 07:22:55"'],
  ['id' => '122','title' => '【生活英文】「可以請你幫我拍張照嗎？」常用的攝影英文！','slug' => 'photography-take-a-photo','excerpt' => '照相已經是現代人生活不可或缺的一部份！出國旅遊時該怎麼用英文請外國人幫忙拍照？看不懂專業的英文攝影名詞？今天就來教教大家最實用的攝影相關英文！','content' => '"<p>照相已經是現代人生活不可或缺的一部份！出國旅遊時該怎麼用英文請外國人幫忙拍照？看不懂專業的英文攝影名詞？今天就來教教大家最實用的攝影相關英文！</p>
<p>&nbsp;</p>
<p><strong><span style=""color: #8e2800;"">Photo/Picture➮相片</span></strong></p>
<ul>
<li><strong>Take a photo/picture of sb./sth.</strong> 拍(某人/某事物的)照片</li>
<li><strong>Take a photo/picture for sb./sth.</strong> 幫(某人/某事物)拍照</li>
</ul>
<p>Ex. <strong><em><span style=""color: #225378;"">Could you take a photo for me?</span></em></strong></p>
<p>&nbsp;&nbsp;<em><strong> <span style=""color: #225378;"">Could you take a photo of me?</span></strong></em></p>
<p>&nbsp;&nbsp; 可以請你幫我拍一張照片嗎？</p>
<p>&nbsp;&nbsp; <em><strong><span style=""color: #225378;"">Could you take a picture of me with the Taipei 101?</span></strong></em></p>
<p>&nbsp;&nbsp; 可以請你幫我跟台北101拍張照嗎？</p>
<p>&nbsp; &nbsp;<em><strong><span style=""color: #225378;"">May I take a photo with you?</span></strong></em></p>
<p><em><strong>&nbsp;&nbsp; <span style=""color: #225378;"">Could we take a photo together?</span></strong></em></p>
<p>&nbsp;&nbsp; 可以請你跟我一起拍一張照片嗎？</p>
<p><em><strong>&nbsp;&nbsp; <span style=""color: #225378;"">May I take a photo of you?</span></strong></em></p>
<p>&nbsp;&nbsp; 可以讓我拍一張你的照片嗎？</p>
<p>&nbsp;</p>
<ul>
<li><strong>Save the photo/picture</strong>儲存照片</li>
<li><strong>Delete the photo/picture </strong>刪除照片</li>
<li><strong>Download the photo/picture</strong> 下載照片</li>
<li><strong>Upload the photo/picture</strong> 上傳照片</li>
</ul>
<p><strong><span style=""color: #8e2800;"">Camera ➮相機</span></strong></p>
<ul>
<li><strong>Turn on the camera</strong> 打開相機</li>
<li><strong>Turn off the camera</strong> 關掉相機</li>
</ul>
<p><strong><span style=""color: #8e2800;"">Selfie ➮自拍</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Selfie-stick ➮自拍棒</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Self-timer ➮定時自拍</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Flash ➮閃光燈</span></strong></p>
<ul>
<li><strong>Turn off the flash</strong>關閉閃光燈</li>
</ul>
<p><strong><span style=""color: #8e2800;"">Flashlight ➮手電筒</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Shoot ➮按下快門</span></strong></p>
<ul>
<li><strong>Set the shutter speed</strong> 設定快門速度</li>
<li><strong>Set the timer</strong> 設定定時器</li>
<li><strong>Adjust the aperture </strong>調整光圈</li>
</ul>
<p><strong><span style=""color: #8e2800;"">Portrait ➮拍照(直)</span></strong></p>
<ul>
<li>Portrait另一個名詞的意思是肖像、描繪</li>
</ul>
<p><em><strong><span style=""color: #225378;"">Ann is a famous portrait painter.</span></strong></em></p>
<p>Ann是一個知名的肖像畫家</p>
<p><em><strong><span style=""color: #225378;"">J.K. Rowling&rsquo;s novels paint a vivid portrait of the magic world.</span></strong></em></p>
<p>J.K. Rowling的小說描繪了一個生動的魔法世界</p>
<p><strong><span style=""color: #8e2800;"">Landscape ➮拍照(橫)</span></strong></p>
<ul>
<li>Landscape作為名詞有另一個常見的意思是風景、景色</li>
</ul>
<p><em><strong><span style=""color: #225378;"">In winter，the landscape of Niagara Fall is magnificent.</span></strong></em></p>
<p>冬天時，尼加拉瓜瀑布的景色很壯觀</p>
<p><strong><span style=""color: #8e2800;"">Burst➮連拍</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Edit ➮修圖</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Filter ➮濾鏡</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Contrast➮對比</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Brightness ➮亮度</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Adjust ➮調整</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Saturation➮飽和度</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Definition➮清晰度</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Temperature ➮色調的溫度</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Tint➮色調</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Warmth ➮溫暖</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Shadow ➮陰影</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Exposure ➮曝光量</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Matt ➮無光澤的</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Camera lens ➮鏡頭</span></strong></p>
<ul>
<li>Focus the lens 對焦</li>
</ul>
<p><strong><span style=""color: #8e2800;"">Pixel ➮畫素</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Resolution ➮解析度</span></strong></p>
<ul>
<li>Resolution還有決心、決定的意思，</li>
</ul>
<p>新年新希望的英文就是New Year Resolution</p>
<p><strong><span style=""color: #8e2800;"">Memory card ➮記憶卡</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Wide-angle ➮廣角</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Pano➮全景模式</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Vibration reduction ➮防手震</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Red-eye reduction ➮防紅眼</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Shutter ➮快門</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Charger ➮充電器</span></strong></p>
<p><strong><span style=""color: #8e2800;"">Tripod ➮三腳架</span></strong></p>
<p>&nbsp;</p>
<hr />
<p><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a83e92b9861b-8wicTQccvdUVL45GZCBAlGJgbPmRJH-300x199.jpeg"" alt="""" width=""490"" height=""300"" /></strong></p>
<h2><strong><span style=""color: #eb7f00;"">Dialogs 常用對話</span></strong></h2>
<p><em><strong><span style=""color: #225378;"">Could you please take a photo of us?</span></strong></em></p>
<p>可以請你幫我們照張相嗎？</p>
<p><em><strong><span style=""color: #225378;"">Mannn，you cut off my head! Can you take one more?</span></strong></em></p>
<p>嘿～你切到我的頭了！可以請你再拍一張嗎？</p>
<p><em><strong><span style=""color: #225378;"">Could you take the camera vertically?</span></strong></em></p>
<p>可以請你把相機拿成直的嗎？</p>
<p><em><strong><span style=""color: #225378;"">Oh no! storage space running out!</span></strong></em></p>
<p>不！容量用完了！</p>
<p><em><strong><span style=""color: #225378;"">Step forward a little，please.</span></strong></em></p>
<p>請往前站一點</p>
<p><em><strong><span style=""color: #225378;"">Please move a little to your left/right.</span></strong></em></p>
<p>請往你的左邊/右邊移動一點</p>
<p><em><strong><span style=""color: #225378;"">Please come closer.</span></strong></em></p>
<p>請靠近一點</p>
<p><em><strong><span style=""color: #225378;"">Do you want me to take one for you as well?</span></strong></em></p>
<p>要不要我也幫你拍一張？</p>
<p><em><strong><span style=""color: #225378;"">Nice photo! Thank you!</span></strong></em></p>
<p>拍得好！謝謝你！</p>
<p><em><strong><span style=""color: #225378;"">This photo is amazing! You are such an awesome photographer!</span></strong></em></p>
<p>拍得太好了！你真是一個厲害的攝影師！</p>"','published' => '1','og_title' => '【生活英文】「可以請你幫我拍張照嗎？」常用的攝影英文！','og_description' => '【生活英文】「可以請你幫我拍張照嗎？」常用的攝影英文！','meta_title' => '【生活英文】「可以請你幫我拍張照嗎？」常用的攝影英文！','meta_description' => '照相已經是現代人生活不可或缺的一部份！出國旅遊時該怎麼用英文請外國人幫忙拍照？看不懂專業的英文攝影名詞？今天就來教教大家最實用的攝影相關英文！','canonical_url' => 'https://tw.english.agency/photography-take-a-photo','feature_image_alt' => '「可以請你幫我拍張照嗎？」常用的攝影英文！','feature_image_title' => '「可以請你幫我拍張照嗎？」常用的攝影英文！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a843cc2e17c9-kNH5VQTMNJ5vaz09oXYwZykyx9dTun-14173x7441.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-14 07:53:39"'],
  ['id' => '123','title' => '【節慶英文】農曆新年到！過年吉祥話、新年用語的英文','slug' => 'lunar-New-Year-auspicious','excerpt' => '狗年迎新春！農曆新年就快到了，在國外要怎麼跟外國人介紹農曆新年習俗呢？發紅包、放鞭炮、各式新年吉祥話的英文又該怎麼說？今天就來學習關於農曆新年的英文用語','content' => '"<p>狗年迎新春！農曆新年就快到了，在國外要怎麼跟外國人介紹農曆新年習俗呢？發紅包、放鞭炮、各式新年吉祥話的英文又該怎麼說？今天就來學習關於農曆新年的英文用語</p>
<p>&nbsp;</p>
<p><span style=""color: #8e2800;"">Chinese New Year / Lunar New Year ➩農曆新年</span></p>
<ul>
<li>Lunar 指的是太陰的意思，像是農曆的英文就是 Lunar Calendar，</li>
<li>正月（農曆一月）的英文就是 Lunar January</li>
<li>今年是狗年，狗年的英文就是 Year of the Dog</li>
</ul>
<p>&nbsp;</p>
<p><span style=""color: #8e2800;"">Spring festival➩ 春節</span></p>
<p>&nbsp;<span style=""color: #8e2800;"">Lunar New Year&rsquo;s Eve ➩除夕</span></p>
<ul>
<li>Eve指的是前夕，像是平安夜（聖誕節前夕）英文是Christmas Eve</li>
</ul>
<p>&nbsp;</p>
<p><span style=""color: #8e2800;"">New Year&rsquo;s Eve Dinner ➩年夜飯</span></p>
<p><span style=""color: #8e2800;"">Family Reunion Dinner ➩團圓宴</span></p>
<p><span style=""color: #8e2800;"">Lion dance➩舞獅</span></p>
<p><span style=""color: #8e2800;"">Dragon dance ➩舞龍</span></p>
<p><span style=""color: #8e2800;"">The monster Nien ➩年獸</span></p>
<p>&nbsp;</p>
<p><span style=""color: #8e2800;"">Firecrackers➩鞭炮</span></p>
<ul>
<li>而放鞭炮的放，這個動詞英文是Lighting，Light作為動詞指的是點燃、點亮。</li>
<li>放鞭炮 ➩Lighting firecrackers</li>
</ul>
<p>&nbsp;</p>
<p><span style=""color: #8e2800;"">Firework ➩煙火</span></p>
<p><span style=""color: #8e2800;"">Mahjong➩麻將</span></p>
<p><span style=""color: #8e2800;"">Red envelope (Lucky red envelope)/ Red packet (Lucky red packet)➩紅包</span></p>
<p><span style=""color: #8e2800;"">Spring couplets/ Red couplets ➩春聯（</span>Couplet指的是對句）</p>
<p><span style=""color: #8e2800;"">Lucky money➩壓歲錢</span></p>
<p><span style=""color: #8e2800;"">Year-end bonus ➩年終獎金</span></p>
<p>&nbsp;</p>
<p><span style=""color: #8e2800;"">Lantern ➩燈籠</span></p>
<ul>
<li>燈會的英文就是Exhibits of lantern ，而元宵節則是叫做The Lantern Festival</li>
</ul>
<p><span style=""color: #8e2800;"">New clothes➩新衣</span></p>
<p>&nbsp;<span style=""color: #8e2800;"">Staying up➩守歲</span></p>
<p><span style=""color: #8e2800;"">Temple ➩寺廟</span></p>
<p><span style=""color: #8e2800;"">Return to parents&rsquo; home➩回娘家</span></p>
<p><span style=""color: #8e2800;"">Worship ancestor ➩祭祖</span></p>
<p><span style=""color: #8e2800;"">Ask the gods for a prophecy➩求神問卜</span></p>
<p><span style=""color: #8e2800;"">Incense ➩香</span></p>
<p><span style=""color: #8e2800;"">Taboo ➩禁忌</span></p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #eb7f00;"">【補充】過年一定要吃的東西包含：</span></strong></h2>
<p>Fish ➩魚，代表年年有餘</p>
<p>Orange ➩橘子，代表大吉大利</p>
<p>Leeks ➩韭菜，代表長久、永遠</p>
<p>Turnips ➩白蘿蔔，代表好彩頭</p>
<p>Rice cake/ New year cake ➩年糕，代表年年高升、逐步興旺</p>
<p>Dumplings ➩水餃，代表元寶，累積好運</p>
<p>Eight treasures rice/ Babao rice ➩八寶飯，代表恭喜發財</p>
<p>Spring roll ➩春捲，代表黃金萬兩</p>
<p>Perennial vegetables ➩長年菜，代表長壽</p>
<p>(Sweet) rice ball ➩湯圓，代表團團圓圓</p>
<p>Pineapple ➩鳳梨，代表旺來&nbsp;</p>
<p>&nbsp;</p>
<h2><span style=""color: #eb7f00;"">【補充】過年常說的吉祥話：</span></h2>
<p>Lucky words ➩吉祥話</p>
<p>Happy Chinese New Years ➩新年快樂</p>
<p>Wish you prosperity and wealth ➩恭喜發財</p>
<p>May wealth come generously to you ➩財源廣進</p>
<p>Great fortune and great favour ➩大吉大利</p>
<p>May your happiness be as broad as the sea ➩喜氣洋洋</p>
<p>New year，new beginning ➩新的一年，新的開始</p>
<p>May you welcome happiness with the spring ➩迎春納福</p>
<p>May all your wishes come true ➩心想事成</p>
<p>May fortune come to your door ➩五福臨門</p>
<p>May there be bounty every year ➩年年有餘</p>
<p>May everything go as you hope ➩萬事如意</p>
<p>May you be blessed with peace and safety in all four seasons ➩四季平安</p>
<p>Everlasting peace year after year ➩歲歲平安</p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a840567eeb57-AEmSPblNYp61wIvLwsZcb3AG1ySkjp-800x600.jpeg"" alt="""" width=""590"" height=""450"" /></p>
<h2><strong><span style=""color: #eb7f00;"">常用Dialogs對話</span></strong></h2>
<p><em><strong><span style=""color: #225378;"">How do you celebrate the Chinese New Year?</span></strong></em></p>
<p>你怎麼慶祝農曆新年呢？</p>
<p><em><strong><span style=""color: #225378;"">I had a family reunion dinner at Lunar New Year&rsquo;s Eve.</span></strong></em></p>
<p>我在除夕夜那天和家人吃了團圓飯</p>
<p><em><strong><span style=""color: #225378;"">After the reunion dinner，I played Mahjong with relatives.</span></strong></em></p>
<p>在吃了團圓飯之後，我和親戚們打了麻將</p>
<p><em><strong><span style=""color: #225378;"">Besides，I got a lot of red envelopes! My grandmother gave me 10 thousand. OMG!</span></strong></em></p>
<p>此外，我還得到了好多紅包！我的外婆給了我一萬塊 我的天哪！</p>
<p><em><strong><span style=""color: #225378;"">My family and I stayed up all night to welcome the new year.</span></strong></em></p>
<p>我跟我的家人整晚守歲不睡覺，歡迎新年的到來</p>
<p><em><strong><span style=""color: #225378;"">My New Year&rsquo;s resolution is to graduate!</span></strong></em></p>
<p>我的新年新希望就是希望可以畢業！</p>"','published' => '1','og_title' => '【節慶英文】農曆新年到！過年吉祥話、新年用語的英文','og_description' => '【節慶英文】農曆新年到！過年吉祥話、新年用語的英文','meta_title' => '【節慶英文】農曆新年到！過年吉祥話、新年用語的英文','meta_description' => '狗年迎新春！農曆新年就快到了，在國外要怎麼跟外國人介紹農曆新年習俗呢？發紅包、放鞭炮、各式新年吉祥話的英文又該怎麼說？今天就來學習關於農曆新年的英文用語','canonical_url' => 'https://tw.english.agency/lunar-New-Year-auspicious','feature_image_alt' => '農曆新年到！過年吉祥話、新年用語的英文','feature_image_title' => '農曆新年到！過年吉祥話、新年用語的英文','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8440f8c0e89-JGMKZqmCSpJueLRHei0CnAWZaRYLVe-14173x7441.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-14 09:50:43"'],
  ['id' => '124','title' => '出國想買化妝品卻看不懂英文單字？你一定要知道的美妝用品單字大公開！','slug' => 'the-vocabularies-of-cosmetics-product','excerpt' => '前陣子是Sephora的折扣季，許多人都趁著這個機會大肆下單觀望已久的外國彩妝，但是這些彩妝品都是英文，要怎麼分辨什麼是什麼呢？妳是不是一直很想搞懂這些英文單字？每次想網購國外美妝用品卻不知道產品英文怎麼說，在國外的彩妝店也不知道怎麼跟店員對話，連要聽懂歐美美妝Youtuber的介紹都是一大難事，不過不用擔心，今天就來教大家最實用的美妝相關英文單字，學會這些單字再也不怕到國外買不到化妝品啦！','content' => '"<p style=""font-size: 16px;"">前陣子是Sephora的折扣季，許多人都趁著這個機會大肆下單觀望已久的外國彩妝，但是這些彩妝品都是英文，要怎麼分辨什麼是什麼呢？妳是不是一直很想搞懂這些英文單字？每次想網購國外美妝用品卻不知道產品英文怎麼說，在國外的彩妝店也不知道怎麼跟店員對話，連要聽懂歐美美妝Youtuber的介紹都是一大難事，不過不用擔心，今天就來教大家最實用的美妝相關英文單字，另外加碼最常見的彩妝店日常對話，學會這些單字再也不怕到國外買不到化妝品啦！</p>
<p style=""font-size: 16px;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a841ccb91d50-lKZQdaAZFYsFwWQzlrQrUizssDLEDc-788x443.jpeg"" alt="""" width=""788"" height=""443"" /></p>
<p style=""color: #c63300; font-size: 20px;""><strong>Cosmetics 臉部化妝品</strong></p>
<p><span style=""text-decoration: underline;"">Foundation Cosmetics<span style=""font-weight: 400;""> 底妝化妝品</span></span></p>
<p><span style=""text-decoration: underline;"">Foundation liquid <span style=""font-weight: 400;"">粉底液</span></span></p>
<p><span style=""text-decoration: underline;"">Foundation Compact<span style=""font-weight: 400;""> 粉餅</span></span></p>
<p><span style=""text-decoration: underline;"">Make up base/primer<span style=""font-weight: 400;""> 妝前乳</span></span></p>
<p><span style=""text-decoration: underline;"">Sun screen/Sun cream<span style=""font-weight: 400;""> 隔離霜/防曬</span></span></p>
<p><span style=""text-decoration: underline;"">Blemism balm <span style=""font-weight: 400;"">BB霜</span></span></p>
<p><span style=""text-decoration: underline;"">Color correction <span style=""font-weight: 400;"">CC霜</span></span></p>
<p><span style=""text-decoration: underline;"">Concealer<span style=""font-weight: 400;""> 遮瑕</span></span></p>
<p><span style=""text-decoration: underline;"">Pen concealer<span style=""font-weight: 400;""> 遮瑕筆</span></span></p>
<p><span style=""text-decoration: underline;"">Stick concealer<span style=""font-weight: 400;""> 遮瑕膏條</span></span></p>
<p><span style=""text-decoration: underline;"">Pressed powder <span style=""font-weight: 400;"">蜜粉餅</span></span></p>
<p><span style=""text-decoration: underline;"">Contour/Shading powder<span style=""font-weight: 400;""> 修容餅</span></span></p>
<p><span style=""text-decoration: underline;"">Blush<span style=""font-weight: 400;""> 腮紅</span></span></p>
<p><span style=""text-decoration: underline;""><span style=""font-weight: 400;""><img title=""眼影盤"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a841d83955ea-AwHDp6QvAfaaJarMT2zvHhzGGE6Z1u-724x483.jpeg"" alt=""眼影盤"" width=""724"" height=""483"" /></span></span></p>
<p style=""color: #c63300; font-size: 20px;""><strong>Eye Cosmetics 眼部彩妝品</strong></p>
<p><span style=""text-decoration: underline;"">Eye shadow<span style=""font-weight: 400;""> 眼影</span></span></p>
<p><span style=""text-decoration: underline;"">Single eye shadow <span style=""font-weight: 400;"">單色眼影</span></span></p>
<p><span style=""text-decoration: underline;"">Eye shadow duo<span style=""font-weight: 400;""> 雙色眼影</span></span></p>
<p><span style=""text-decoration: underline;"">Eye shadow trio <span style=""font-weight: 400;"">三色眼影</span></span></p>
<p><span style=""text-decoration: underline;"">Eye liner<span style=""font-weight: 400;""> 眼線筆</span></span></p>
<p><span style=""text-decoration: underline;"">Liquid eye liner<span style=""font-weight: 400;""> 眼線液</span></span></p>
<p><span style=""text-decoration: underline;"">Gel eyeliner<span style=""font-weight: 400;""> 眼線膠</span></span></p>
<p><span style=""text-decoration: underline;"">Mascara<span style=""font-weight: 400;""> 睫毛膏</span></span></p>
<p><span style=""text-decoration: underline;"">False eyelashes<span style=""font-weight: 400;""> 假睫毛</span></span></p>
<p><span style=""text-decoration: underline;"">Eyebrow pencil<span style=""font-weight: 400;""> 眉筆</span></span></p>
<p><span style=""text-decoration: underline;"">Eyebrow powder<span style=""font-weight: 400;""> 眉粉</span></span></p>
<p><span style=""text-decoration: underline;""><span style=""font-weight: 400;""><img title=""唇膏"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a841ddcb2ea8-ltq0WDzCqC6DgPJBATwQNyUKTlKaHt-740x472.jpeg"" alt=""唇膏"" width=""740"" height=""472"" /></span></span></p>
<p style=""color: #c63300; font-size: 20px;""><strong>Lip cosmetics 唇部化妝品</strong></p>
<p><span style=""text-decoration: underline;"">Lip linear<span style=""font-weight: 400;""> 唇線筆</span></span></p>
<p><span style=""text-decoration: underline;"">Lipstick<span style=""font-weight: 400;""> 口紅</span></span></p>
<p><span style=""text-decoration: underline;"">Lip gloss<span style=""font-weight: 400;""> 唇蜜</span></span></p>
<p><span style=""text-decoration: underline;"">Lip balm<span style=""font-weight: 400;""> 護唇膏</span></span></p>
<p><span style=""text-decoration: underline;"">Lip palette<span style=""font-weight: 400;""> 唇彩盤</span></span></p>
<p><span style=""text-decoration: underline;""><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a841e5f8bb48-fRaMyUixgGfyaBaZoXHbGh7ggqDkPU-503x340.jpeg"" alt="""" width=""503"" height=""340"" /></span></span></p>
<p style=""color: #c63300; font-size: 20px;""><strong>Makeup Tools 輔助工具</strong></p>
<p><span style=""text-decoration: underline;"">Oil-absorbing sheets<span style=""font-weight: 400;""> 吸油紙</span></span></p>
<p><span style=""text-decoration: underline;"">Cotton pads<span style=""font-weight: 400;""> 化妝棉</span></span></p>
<p><span style=""text-decoration: underline;"">Makeup remover<span style=""font-weight: 400;""> 卸妝用品</span></span></p>
<p><span style=""text-decoration: underline;"">Makeup removing lotion<span style=""font-weight: 400;""> 卸妝乳</span></span></p>
<p><span style=""text-decoration: underline;"">Cleansing oil<span style=""font-weight: 400;""> 卸妝油</span></span></p>
<p><span style=""text-decoration: underline;"">Eyelash curler<span style=""font-weight: 400;""> 睫毛夾</span></span></p>
<p><span style=""text-decoration: underline;"">Puff<span style=""font-weight: 400;""> 粉撲</span></span></p>
<p><span style=""text-decoration: underline;"">Foundation sponge<span style=""font-weight: 400;""> 粉底海綿</span></span></p>
<p><span style=""text-decoration: underline;"">Brush<span style=""font-weight: 400;""> 刷子</span></span></p>
<p><span style=""text-decoration: underline;"">Eye shadow brush<span style=""font-weight: 400;""> 眼影刷</span></span></p>
<p><span style=""text-decoration: underline;"">Blush brush <span style=""font-weight: 400;"">腮紅刷</span></span></p>
<p><span style=""text-decoration: underline;"">Powder brush <span style=""font-weight: 400;"">蜜粉刷</span></span></p>
<p><span style=""text-decoration: underline;"">Brow brush<span style=""font-weight: 400;""> 眉毛刷</span></span></p>
<p><span style=""text-decoration: underline;"">Brush cleaner<span style=""font-weight: 400;""> 刷具清潔液</span></span></p>
<p><span style=""text-decoration: underline;"">Tweezers<span style=""font-weight: 400;""> 鑷子</span></span></p>
<h3 style=""font-size: 20px; color: #c63300;"">【補充】</h3>
<ul>
<li><span style=""font-weight: 400;"">化妝的英文</span><span style=""font-weight: 400;"">Make up</span><span style=""font-weight: 400;"">還有很多其他意思分別是：</span></li>
</ul>
<p><span style=""font-weight: 400;"">1.</span><span style=""font-weight: 400;"">編造</span></p>
<p><span style=""font-weight: 400;"">The whole story Brian told you was not true. He made it up.</span></p>
<p><span style=""font-weight: 400;"">Brian告訴你的整個故事都不是真的。他編造了。</span></p>
<p><span style=""font-weight: 400;"">2.組成</span></p>
<p><span style=""font-weight: 400;"">Our marketing team was made up of twelve members. </span></p>
<p><span style=""font-weight: 400;"">我們的行銷團隊由十二位成員組成。</span></p>
<p><span style=""font-weight: 400;"">3.</span><span style=""font-weight: 400;"">補償/補考</span></p>
<p><span style=""font-weight: 400;"">Professor，I was so sick and could not write the midterm yesterday. Can I make up the test?</span></p>
<p><span style=""font-weight: 400;"">教授，我昨天病得很嚴重沒辦法寫期中考卷，我可以補考嗎？</span></p>
<p><span style=""font-weight: 400;"">4.和好</span></p>
<p><span style=""font-weight: 400;"">Vivian had a fight with her boyfriend，but she has made up with him now.</span></p>
<p><span style=""font-weight: 400;"">Vivian 和她的男朋友吵架，不過他們現在已經和好了</span></p>
<ul>
<li><span style=""font-weight: 400;"">腮紅的英文</span><span style=""font-weight: 400;"">Blush</span><span style=""font-weight: 400;"">作為動詞是指因為害羞、尷尬而臉紅，那有一種亞洲人常見的飲酒後會臉紅的現象，則叫做</span><span style=""font-weight: 400;"">Asian Flush</span><span style=""font-weight: 400;"">，指的是酒精性臉紅反應。</span></li>
</ul>
<h4><strong>彩妝中，要了解自己的肌膚性質，選擇相對應的底妝產品，這些肌膚性質英文分別是：</strong></h4>
<p><span style=""text-decoration: underline;"">Dry<span style=""font-weight: 400;""> 乾性肌膚</span></span></p>
<p><span style=""text-decoration: underline;"">Oily<span style=""font-weight: 400;""> 油性肌膚</span></span></p>
<p><span style=""text-decoration: underline;"">Normal<span style=""font-weight: 400;""> 中性/一般性肌膚</span></span></p>
<p><span style=""text-decoration: underline;"">Combination<span style=""font-weight: 400;""> 混合性肌膚</span></span></p>
<p><span style=""text-decoration: underline;"">Sensitive<span style=""font-weight: 400;""> 敏感性肌膚</span></span></p>
<p><span style=""text-decoration: underline;""><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a841f545d19b-l5sJ7u2GYwMksthrymg8o2vATWhqS9-724x483.jpeg"" alt="""" width=""724"" height=""483"" /></span></span></p>
<p style=""color: #003c9d; font-size: 16px;""><strong>Dialog 常見的美妝店對話</strong></p>
<p><span style=""color: #003c9d; font-weight: 400;"">Saleswoman: How can I help you?</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">店員:請問需要什麼協助嗎?</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">I am looking for ______ (ex: mascara/ foundation/ lipsticks&hellip;) </span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">我想買_____ (例如 睫毛膏、粉底、口紅&hellip;.)</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">Saleswoman: What&rsquo;s your skin type?</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">店員：你是什麼種類的肌膚呢?</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">The T zone of my face is oily，but my cheeks are so dry.</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">我的T字部位很油，但雙頰很乾。</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">Saleswoman: You have combination skin. This non-oily gel can help to hydrate your skin.</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">店員：你有混合性肌膚。這種非油性的凝膠可以幫助你滋潤肌膚。</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">Is there any promotion offering right now?</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">請問現在有沒有折扣？</span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">May I have a try? </span></p>
<p><span style=""color: #003c9d; font-weight: 400;"">可以試用一下嗎？</span></p>
<p style=""font-size: 16px;"">看完以上的介紹，大家是不是都對美妝產品的英文單字更加熟悉了呢？下次到國外藥妝店不用再擔心看不懂英文或沒辦法對話啦，大方地跟店員詢問妳要的商品吧！</p>"','published' => '1','og_title' => '出國想買化妝品卻看不懂英文單字？你一定要知道的美妝用品單字大解密！','og_description' => '前陣子是Sephora的折扣季，許多人都趁著這個機會大肆下單觀望已久的外國彩妝，但是這些彩妝品都是英文，要怎麼分辨什麼是什麼呢？妳是不是一直很想搞懂這些英文單字？每次想網購國外美妝用品卻不知道產品英文怎麼說，在國外的彩妝店也不知道怎麼跟店員對話，連要聽懂歐美美妝Youtuber的介紹都是一大難事，不過不用擔心，今天就來教大家最實用的美妝相關英文單字，學會這些單字再也不怕到國外買不到化妝品啦！','meta_title' => '出國想買化妝品卻看不懂英文單字？你一定要知道的美妝用品單字大解密！','meta_description' => '前陣子是Sephora的折扣季，許多人都趁著這個機會大肆下單觀望已久的外國彩妝，但是這些彩妝品都是英文，要怎麼分辨什麼是什麼呢？妳是不是一直很想搞懂這些英文單字？每次想網購國外美妝用品卻不知道產品英文怎麼說，在國外的彩妝店也不知道怎麼跟店員對話，連要聽懂歐美美妝Youtuber的介紹都是一大難事，不過不用擔心，今天就來教大家最實用的美妝相關英文單字，學會這些單字再也不怕到國外買不到化妝品啦！','canonical_url' => 'https://tw.english.agency/the-vocabularies-of-cosmetics-product','feature_image_alt' => '出國想買化妝品卻看不懂英文單字？你一定要知道的美妝用品單字大公開！','feature_image_title' => '出國想買化妝品卻看不懂英文單字？你一定要知道的美妝用品單字大公開！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8453fe01be3-FUNGSqmXboA4dVI863fQd7BMQoCoFl-724x483.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-14 11:19:07"'],
  ['id' => '125','title' => '【寵物英文】愛狗人士是你？狗狗品種跟相關狗英文！','slug' => 'puppy-pet-dog-variety','excerpt' => '說到養寵物，最常見的寵物冠軍就是狗了！今年又剛好是農曆新年的狗年，就來學學跟狗有關的英文吧','content' => '"<p>說到養寵物，最常見的寵物冠軍就是狗了！今年又剛好是農曆新年的狗年，就來學學跟狗有關的英文吧</p>
<h2>&nbsp;</h2>
<h2><strong><span style=""color: #eb7f00;"">Dog breeds 犬種</span></strong></h2>
<p><strong>Labrador Retriever</strong> ➩拉布拉多犬</p>
<p><strong>Golden Retriever</strong> ➩黃金獵犬</p>
<p><strong>Dalmatian</strong> ➩大麥町犬</p>
<p><strong>Beagle</strong> ➩米格魯</p>
<p><strong>Yorkshire Terrier</strong> ➩約克夏</p>
<p><strong>Schnauzer</strong> ➩雪納瑞</p>
<p><strong>Siberian Husky </strong>➩哈士奇</p>
<p><strong>Maltese</strong> ➩馬爾濟斯</p>
<p><strong>Dachshund</strong> ➩臘腸犬</p>
<p><strong>German Shepherd</strong> ➩德國狼犬</p>
<p><strong>Shibu Inu</strong> ➩柴犬</p>
<p><strong>Akita</strong> ➩秋田犬</p>
<p><strong>Old English Sheepdog</strong> ➩古代牧羊犬</p>
<p><strong>Chinese Shar-Pei </strong>➩沙皮狗</p>
<p><strong>Welsh-Corgi </strong>➩柯基</p>
<p><strong>Bichon Frise </strong>➩比熊犬</p>
<p><strong>Tibetan Mastiff</strong> ➩藏獒</p>
<p><strong>Bulldog</strong> ➩鬥牛犬</p>
<p><strong>Bull terrier</strong> ➩牛頭梗</p>
<p><strong>Pug </strong>➩巴哥狗</p>
<p><strong>Boxer </strong>➩拳師狗</p>
<p><strong>Poodle </strong>➩貴賓狗</p>
<p><strong>Pomerarian</strong> ➩博美犬</p>
<p><strong>Mix</strong> ➩米克斯(混種犬)</p>
<p>&nbsp;</p>
<h2><strong><span style=""color: #eb7f00;"">【補充】</span></strong></h2>
<ul>
<li>寵物的英文是Pet，養寵物則是Keep a pet</li>
<li>幼犬是Puppy，除此之外，狗還有很多種專業技能犬，例如<strong>Guide dog</strong> 導盲犬、<strong>Drug-sniffing dog</strong> 緝毒犬、<strong>Police dog</strong> 警犬、<strong>Stray dog/Street dog</strong> 流浪犬</li>
</ul>
<ul>
<li>動物的特殊用語有： Fur 動物毛髮、 Paws 動物腳爪</li>
<li>描述狗叫「汪汪」的狀聲詞，英文則是用： Bark 狗吠、ruff、woof</li>
<li>現代人常形容自己是單身狗，英文則是<strong>Damn single</strong>，Damn指的就是該死的、不好的意思</li>
</ul>
<p>&nbsp;</p>
<hr />
<p><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8424291cab6-eHkHceHG3PXgBaceKhhlPiUMIRvil5-600x379.jpeg"" alt="""" width=""600"" height=""379"" /></strong></p>
<h2><strong><span style=""color: #eb7f00;"">Dog idioms and slangs 狗相關的俚語俗諺</span></strong></h2>
<p>It&rsquo;s a dog&rsquo;s life➩跟狗一樣慘的生活</p>
<p><em><strong><span style=""color: #225378;"">I am leading a dog&rsquo;s life!</span></strong></em></p>
<p>我正過著跟狗一樣慘的生活</p>
<p>&nbsp;</p>
<p><strong>Raining cats and dogs</strong> ➩傾盆大雨</p>
<p><em><strong><span style=""color: #225378;"">Look at the weather outside! It&rsquo;s raining cats and dogs</span></strong></em></p>
<p>看看外面的天氣！正在下著傾盆大雨呢</p>
<p>&nbsp;</p>
<p><strong>Dog&rsquo;s breakfast </strong>➩一團混亂</p>
<p><em><strong><span style=""color: #225378;"">Jim&rsquo;s essay is a dog&rsquo;s breakfast!</span></strong></em></p>
<p>Jim的小論文一團混亂！</p>
<p>&nbsp;</p>
<p><strong>Love me，love my dog</strong> ➩愛屋及烏</p>
<p><em><strong><span style=""color: #225378;"">If you want to be my girlfriend，you should also take care of my disabled brother. Just as the idiom says，&ldquo;Love me，love my dog.&rdquo;</span></strong></em></p>
<p>如果你想當我的女朋友，那你也要照顧我殘疾的兄弟。就像慣用語說的一樣，要愛屋及烏。</p>
<p>&nbsp;</p>
<p><strong>Sick as a dog</strong> ➩病得不輕</p>
<p><em><strong><span style=""color: #225378;"">After getting wet from the pouring rain，Joanne is sick as a dog right now.</span></strong></em></p>
<p>再淋了一場大雨之後，Joanne正病得不輕</p>
<p>&nbsp;</p>
<p><strong>A dog-eat-dog world</strong> ➩一個狗咬狗(自相殘殺、弱肉強食)的世界</p>
<p><em><strong><span style=""color: #225378;"">With competitive market，it is a dog-eat-dog world for every company in this field.</span></strong></em></p>
<p>在這個競爭的市場內，每間公司都處於在一個弱肉強食的世界。</p>
<p>&nbsp;</p>
<p><strong>Not have a dog&rsquo;s chance of Ving.</strong> ➩沒有機會做某事</p>
<p><em><strong><span style=""color: #225378;"">Since Andrew only studied for 10 minutes，he does not have a dog&rsquo;s chance of passing the exam.</span></strong></em></p>
<p>因為Andrew只複習了十分鐘，他沒有機會在這次的考試中及格。</p>
<p>&nbsp;</p>
<p><strong>Every dog has its day </strong>➩風水輪流轉</p>
<p><strong>Dog does not eat dog</strong> ➩同類不相殘</p>
<p><strong>Barking dogs never bite</strong> ➩虛張聲勢</p>
<p>&nbsp;</p>
<hr />
<p><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a84256405879-ebgDngvxvqFnA73YWLWdAMxMftPyqI-640x444.jpeg"" alt="""" width=""640"" height=""444"" /></strong></p>
<h2><strong><span style=""color: #eb7f00;"">Dialogs 對話</span></strong></h2>
<p><em><strong><span style=""color: #225378;"">Are you a dog person? </span></strong></em>你是愛狗人士嗎？</p>
<p><em><strong><span style=""color: #225378;"">No，I am a cat person. </span></strong></em>不是，我是愛貓人士</p>
<p>&nbsp;</p>
<p><em><strong><span style=""color: #225378;"">What is his/her name? </span></strong></em>他叫什麼名子?</p>
<p><em><strong><span style=""color: #225378;"">He&rsquo;s name is Joseph.</span></strong></em> 他叫做Joesph</p>
<p>&nbsp;</p>
<p><em><strong><span style=""color: #225378;"">How old is your dog? </span></strong></em>你的狗幾歲了?</p>
<p><em><strong><span style=""color: #225378;"">She&rsquo;s 7.</span></strong></em>他七歲了</p>
<p><em><strong><span style=""color: #225378;"">What breed is your dog?</span></strong></em> 你的狗是什麼品種的?</p>
<p><em><strong><span style=""color: #225378;"">He is a Bichon Frise. </span></strong></em>他是比熊犬</p>
<p>&nbsp;</p>
<p>雖然對於動物的主詞應該要用It，但對於自己的寵物還是很常用He/She來稱呼他們喔～</p>"','published' => '1','og_title' => '【寵物英文】愛狗人士是你？狗狗品種跟相關狗英文！','og_description' => '【寵物英文】愛狗人士是你？狗狗品種跟相關狗英文！','meta_title' => '【寵物英文】愛狗人士是你？狗狗品種跟相關狗英文！','meta_description' => '說到養寵物，最常見的寵物冠軍就是狗了！今年又剛好是農曆新年的狗年，就來學學跟狗有關的英文吧','canonical_url' => 'https://tw.english.agency/puppy-pet-dog-variety','feature_image_alt' => '愛狗人士是你？狗狗品種跟相關狗英文！','feature_image_title' => '愛狗人士是你？狗狗品種跟相關狗英文！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a84396b7cfa3-rFQwzKI6sxQG8Ootkbrb1REhMeeGJI-14173x7441.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-14 11:55:41"'],
  ['id' => '126','title' => '去國外旅行必備的英文單字是什麼？愛自助旅行的你絕對不能錯過！','slug' => 'vocabularies-of-traveling','excerpt' => '出國自助旅行搭火車、地鐵是多數人的選擇，但要怎麼看懂地鐵或是公車的英文單字呢？今天就來告訴大家到國外旅行必備的旅遊相關英文單字，順便介紹美國紐約地鐵的搭車技巧，出國別怕搭錯車，問路就用這幾個單字，保證你不用吃迷路的虧！','content' => '"<p style=""font-size: 16px;"">出國自助旅行搭火車、地鐵是多數人的選擇，但要怎麼看懂地鐵或是公車的英文單字呢？今天就來告訴大家到國外旅行必備的旅遊相關英文單字，順便介紹美國紐約地鐵的搭車技巧，出國別怕搭錯車，問路就用這幾個單字，保證你不用吃迷路的虧！&nbsp;</p>
<p style=""color: #0000cc; font-size: 22px;""><span style=""text-decoration: underline;""><strong> 出國人生地不熟，這些單字一定要熟</strong></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Subway地鐵</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Tube地鐵(英國常用)</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Metro地鐵</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Timetable時刻表</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Ticket office售票處</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Platform月台</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Local train 區間車</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Express快車</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Carriage車廂</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Attendant乘務員</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Stop-over中途下車</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Dining car餐車</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Berth 臥鋪</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Rack架子</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Luggage行李</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Water dispenser飲水機</span></span></p>
<p style=""color: #0000cc; font-size: 22px;""><span style=""text-decoration: underline;""><strong>學會車票單字，讓你挖到更好價格</strong></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">One-way ticket單程票</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Single ticket單程票</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Round-trip ticket來回票</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Return ticket來回票</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Off-Peak Return離峰來回票(只能搭票上顯示的日期)</span></span></p>
<p style=""color: #0000cc; font-size: 22px;""><span style=""text-decoration: underline;""><strong>車廂和搭乘時間相關單字</strong></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Coach普通車廂</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Sleeping car臥鋪車廂</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Lounge car休息車廂</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Checked baggage託運行李</span></span></p>
<p style=""font-size: 18px;""><span style=""text-decoration: underline;""><span style=""font-weight: 400;"">Pacific time太平洋地區時間(PT，美加墨地區使用的時區)</span></span></p>
<p>&nbsp;</p>
<p style=""color: #aa0000; font-size: 22px;""><strong>如何用英文問路&nbsp;</strong></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">How can I get to Time Square?</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">我要如何抵達時代廣場？</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">Which platform does the train to Brooklyn leave from?</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">前往布魯克林火車是在幾號月台出發？</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">What time is the train back to New York?</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">請問回紐約的火車是幾點</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">Could you tell me the times of trains to Liverpool，please?</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">你能告訴我前往利物浦的班次時間嗎，謝謝</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">I&rsquo;d like a ticket to Chicago Union Station，please.</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">我要一張到芝加哥聯合車站車票，謝謝</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">That&rsquo;s $5.50. (</span><em><span style=""font-weight: 400; color: #aa0000;"">5 dollars 50 cents</span></em><span style=""font-weight: 400;"">)</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">一共五.五元</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">The train leaves from platform 2.</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">火車從2號月台駛離</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">Could I have a ticket for the next train to London，please?</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">我要一張下一班車往倫敦的車票，謝謝</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">Can I see your ticket，please?</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">能讓我看您的票嗎？</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">What places do you recommend I visit?</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">你可以推薦我一些景點嗎？</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">Recommend (that) someone do something：建議某人做某事</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">The doctor recommended he drink more milk.</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">醫生建議他多喝牛奶</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">The house has much to recommend it.</span></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400; color: #aa0000;"">這間房子可圈可點的地方很多</span></p>
<p>&nbsp;</p>
<p style=""color: #003c9d; font-size: 18px;""><span style=""text-decoration: underline;""><strong>紐約地鐵Subway怎麼搭</strong></span></p>
<p style=""font-size: 16px;"">說到美國旅遊，紐約必為朝聖之地。想去紐約最道地又省錢的方法就是搭地鐵！搭乘紐約地鐵可以用<strong>Metro card</strong>，性質就像悠遊卡、一卡通。不過進站是用「刷」的，不是用「逼」的；出站時只要推開閘門就好！而搭乘來回方式，他們有分<strong>Uptown(北上)</strong>和<strong>Downtown(南下)</strong>，不像台灣捷運(只要看搭乘方向的終點站，就知道要搭哪班車)。美國地鐵也有分大站、小站，所以快車(Express)是不會站站停靠，這時就要記得搭區間車(Local)。</p>
<p style=""font-size: 16px;"">在美國除了搭地鐵，你也可以考慮其他的搭乘方式。例如美鐵Amtrak、灰狗客運Greyhound、超級巴士Megabus等等。但不管搭哪種陸上交通，最安全的方法就是事先閱讀旅遊指南、安排好搭乘路線並提早前往搭乘車站！</p>
<p><span style=""font-weight: 400;"">&nbsp;</span></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '去國外旅行必備的英文單字是什麼？愛自助旅行的你絕對不能錯過！','og_description' => '出國自助旅行搭火車、地鐵是多數人的選擇，但要怎麼看懂地鐵或是公車的英文單字呢？今天就來告訴大家到國外旅行必備的旅遊相關英文單字，順便介紹美國紐約地鐵的搭車技巧，出國別怕搭錯車，問路就用這幾個單字，保證你不用吃迷路的虧！','meta_title' => '去國外旅行必備的英文單字是什麼？愛自助旅行的你絕對不能錯過！','meta_description' => '出國自助旅行搭火車、地鐵是多數人的選擇，但要怎麼看懂地鐵或是公車的英文單字呢？今天就來告訴大家到國外旅行必備的旅遊相關英文單字，順便介紹美國紐約地鐵的搭車技巧，出國別怕搭錯車，問路就用這幾個單字，保證你不用吃迷路的虧！','canonical_url' => 'https://tw.english.agency/vocabularies-of-traveling','feature_image_alt' => '去國外旅行必備的英文單字是什麼？愛自助旅行的你絕對不能錯過！','feature_image_title' => '去國外旅行必備的英文單字是什麼？愛自助旅行的你絕對不能錯過！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a84519e55332-R9IzX0QThk1zFBeCCXbAaPtbduPp3E-724x483.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-14 12:00:34"'],
  ['id' => '127','title' => '生活英文│2018最夯的美劇你看了沒？你一定想知道的冰與火之歌小八卦大公開！','slug' => 'beyond-game-of-throne','excerpt' => '《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！','content' => '"<p style=""font-size: 16px;"">《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！</p>
<p><em><span style=""font-weight: 400;"">A song of Ice and Fire</span></em><span style=""font-weight: 400;""> is a series of an epic fantasy novel written by George R.R. Martin，who conceived of it as a trilogy. Today five of the seven volumes have been published.</span></p>
<p><em><span style=""font-weight: 400;"">《冰與火之歌》小說是喬治馬丁筆下的奇幻史詩鉅作，原先以為只會出三部曲，現今已出版五冊，一系列共七冊。</span></em></p>
<p>&nbsp;</p>
<p style=""font-size: 22px; color: #880000;""><strong>冰與火之歌常見單字和句子：</strong></p>
<p style=""font-size: 18px;""><strong>單字</strong></p>
<p><span style=""text-decoration: underline; font-size: 16px;""><span style=""font-weight: 400;"">Iron Throne 鐵王座</span></span></p>
<p><span style=""text-decoration: underline; font-size: 16px;""><span style=""font-weight: 400;"">Motto 家訓</span></span></p>
<p><span style=""text-decoration: underline; font-size: 16px;""><span style=""font-weight: 400;"">Declare war on/against 宣戰</span></span></p>
<p><span style=""text-decoration: underline; font-size: 16px;""><span style=""font-weight: 400;"">Come to power 掌權</span></span></p>
<p><span style=""text-decoration: underline; font-size: 16px;""><span style=""font-weight: 400;"">Battle for the crown 爭奪王位</span>&nbsp;</span></p>
<p><span style=""text-decoration: underline; font-size: 16px;""><span style=""font-weight: 400;"">Winter is coming. 凜冬將至 (史塔克家族的家訓)</span>&nbsp;</span></p>
<p><span style=""text-decoration: underline; font-size: 16px;""><span style=""font-weight: 400;"">Hear me roar 聽我怒吼 (蘭尼斯特家族的家訓)</span></span></p>
<p style=""font-size: 18px;""><strong>句子</strong></p>
<p><span style=""font-weight: 400; font-size: 16px;"">You know nothing，Jon Snow. 你極其無知、你什麼都不懂</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">(這是用來罵瓊恩什麼都不懂，奇妙的是在書中這句是翻成：</span><span style=""font-weight: 400;"">你懂個屁呀！</span><span style=""font-weight: 400;"">)</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">The conflict started between the Starks and the Lannisters.</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">史塔克家族和蘭尼斯特家族間產生衝突。</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">They conspired together to betray the hand of the king.</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">他們共同密謀，背叛國王首相。</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">They kill the king to take power.</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">他們為掌權殺掉國王。</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">Littlefinger tried to turn Sansa against her sister.</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">小指頭試圖慫恿珊莎與她妹妹反目成仇。</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">So who is the rightful heir to the Iron Throne?</span></p>
<p><span style=""font-weight: 400; font-size: 16px;"">所以誰才是鐵王座的合法繼承人？</span></p>
<p style=""font-size: 22px; color: #880000;""><strong>劇情太精彩，你可以這樣形容：</strong></p>
<p style=""font-size: 16px;"">I&rsquo;m so hyped for tonight&rsquo;s episode.</p>
<p style=""font-size: 16px;"">好興奮今晚要播的影集！</p>
<p style=""font-size: 16px;"">After this finale，I have to wait one year for the new season.</p>
<p style=""font-size: 16px;"">看完最後一集，我得等上一年才有新的一季。&nbsp;</p>
<p style=""font-size: 16px;"">Boom! 看吧! &nbsp;(當你發現好的一方總是獲勝時，就可以撂下這句話！)</p>
<p style=""font-size: 16px;"">I didn&rsquo;t see that coming. 我完全沒想過會這樣、這是我始料未及的&nbsp;</p>
<p style=""font-size: 16px;"">Did that just happen? 剛剛的事都是真的嗎？</p>
<p style=""font-size: 16px;"">It took me a while to register. 我剛剛才意識到</p>
<p style=""font-size: 16px;"">I rewatched all the series! 我每個系列都重看了</p>
<p style=""font-size: 22px; color: #880000;""><strong>冰與火之歌演員八卦大公開</strong></p>
<p style=""font-size: 16px;""><strong>什麼？瑟曦曾因試鏡「潛規則」丟失角色 &nbsp;What? Cersei lost roles for not flirting. </strong></p>
<p style=""font-size: 16px;"">飾演瑟曦的琳娜‧海蒂 (Lena Headey) 在與梅西‧威廉斯 (Maisie Williams) 對談中，透露她曾經在試鏡中因不和對方調情而喪失演出。</p>
<p style=""font-size: 16px;"">She said: &ldquo;When I was in my twenties，and doing a lot of audition tapes in the States，a casting director told me: \'The men take these tapes home and watch them and say，\'Who would you f*ck?\'</p>
<p style=""font-size: 16px;"">她說：「當時我二十幾歲，在美國參加許多錄影帶試鏡。一位選角導演告訴我：這些人會把錄影帶帶回家看。然後問我『你會跟誰上床？』」</p>
<p style=""font-size: 16px;"">琳娜‧海蒂公開這種莫名的潛規則 (Unspoken rules)，也凸顯了女性在職場上的地位不平等。更別說最近出現已久的「Mansplaining」(直男癌說教、男性說教)。就連女星珍妮佛‧勞倫斯、臉書營運長雪柔‧桑德伯格都曾表示同工不同酬 (Gender pay gap)、女性領導就是強勢 (Strong、Aggressive、Pushy)等刻板印象。</p>
<p style=""font-size: 16px;"">A woman has to say the same thing ten times，but a man says it once and everybody listens.</p>
<p style=""font-size: 16px;"">同一件事女性得說上十次，大家才會聽；但男性只需要說一次。</p>
<p style=""font-size: 16px;""><strong>再來聽聽這個小故事：一位男上司誤用女同事的信箱，發現性別歧視的嚴重性</strong></p>
<p style=""font-size: 16px;"">Nicole and I worked for a small employment service firm and one complaint always came from our boss: She took too long to work with clients. As her supervisor，I considered this a minor nuisance at best. I figured the reason I got things done faster was from having more experience. So one day I\'m emailing a client back-and-forth about his resume and he is just being IMPOSSIBLE. Rude，dismissive，ignoring my questions. Telling me his methods were the industry standards (they weren\'t) and I couldn\'t understand the terms he used (I could).</p>
<p style=""font-size: 16px;"">妮可和我在一家小型人力仲介公司工作，但我們的老闆總是抱怨她花太多時間處理客戶。身為她的上司，我頂多當作是小問題處理。我原以為自己做事效率較好是因為經驗豐富的關係。有一天我為了處理一位客戶的履歷，不斷跟他互相來信，他的反應實在不可理喻，他的態度無禮、輕視，還無視我的提問。只告訴我他的方法是業界標準 (明明不是)，還用那些我聽不懂的術語 (我懂)。</p>
<p style=""font-size: 16px;"">Anyway I was getting sick of his shit when I noticed something. Thanks to our shared inbox，I\'d been signing all communications as ""Nicole""</p>
<p style=""font-size: 16px;"">正當我被他搞得身心俱疲時，我發現了一件事。感謝我用到了她的信箱，郵件上都是署名Nicole。</p>
<p style=""font-size: 16px;"">By the time she could get clients to accept that she knew what she was doing，I could get halfway through another client.</p>
<p style=""font-size: 16px;"">等到她還在讓客戶相信她知道自己在做什麼時，我可能已經在處理下一位客戶的事了。</p>
<p style=""font-size: 16px;"">《冰與火之歌》資料遭洩！ &nbsp;Game of Thrones data leak</p>
<p style=""font-size: 16px;"">Hackers claimed to have obtained 1.5 terabytes of data from the company. So far，an upcoming episode of Ballers and Room 104 have apparently been put online. There is also written material that&rsquo;s allegedly from next week&rsquo;s fourth episode of Game of Thrones. More is promised to be &ldquo;coming soon.&rdquo;</p>
<p style=""font-size: 16px;"">駭客表示已從公司內部竊取1.5TB的資料。目前即將播出的影集《好球天團》以及《104號房》也已遭公布網路，還包括《冰與火之歌》下周即映的第四集內容手稿。駭客聲稱即將曝光更多內容。</p>
<p style=""font-size: 16px;""><strong>遭駭事件讓大家議論紛紛，來聽聽這名網友的評論：</strong></p>
<p style=""font-size: 16px;"">I\'d be more worried about an HBO hack that leaked user data，such as credit card info for payments through HBO Go.</p>
<p><span style=""font-weight: 400;"">我比較擔心HBO遭駭會洩漏使用者的資料，例如我們使用付費程式HBO Go時填入的信用卡資訊。</span></p>
<p>&nbsp;</p>
<h3><strong>同場加映：</strong></h3>
<p>&nbsp;</p>
<p style=""font-size: 16px;"">《冰與火之歌》 艾蜜莉亞‧克拉克談論第一次！</p>
<p><a href=""https://www.youtube.com/watch?v=YnxgpgyX4HE&amp;t=35s""><span style=""font-weight: 400;"">https://www.youtube.com/watch?v=YnxgpgyX4HE&amp;t=35s</span></a></p>
<p>&nbsp;</p>
<p style=""font-size: 16px;"">《冰與火之歌》冏恩來晚餐！</p>
<p><a href=""https://www.youtube.com/watch?v=AXdfTUb1N-A&amp;t=209s""><span style=""font-weight: 400;"">https://www.youtube.com/watch?v=AXdfTUb1N-A&amp;t=209s</span></a></p>
<p><br /><br /></p>"','published' => '1','og_title' => '生活英文│2018最夯的美劇你看了沒？你一定想知道的冰與火之歌小八卦大公開！','og_description' => '《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！','meta_title' => '生活英文│2018最夯的美劇你看了沒？你一定想知道的冰與火之歌小八卦大公開！','meta_description' => '《冰與火之歌》第七季播出風靡全世界，成為熱度居高不下的熱門影集。討論影集也瞬間變成了生活中不可或缺的話題！不過你知道如何形容影集太精彩、出乎意料，以及爭奪王位、密謀反叛嗎？快來學學實用的生活句子，讓你拒當「冷場王」！','canonical_url' => 'https://tw.english.agency/beyond-game-of-throne','feature_image_alt' => '生活英文│最夯的美劇你看了沒？你不可不知的冰與火之歌小八卦大公開！','feature_image_title' => '生活英文│最夯的美劇你看了沒？你不可不知的冰與火之歌小八卦大公開！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a844ea2d18dd-DzOHnQWlyBhN9uBFbGKolkiPWwd1Nn-1024x639.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-14 12:43:54"'],
  ['id' => '128','title' => '"你還只會說The movie was great?學會這些電影單字讓你更能跟朋友暢聊電影！"','slug' => 'movie-vocabularies','excerpt' => '每次看完新上映的電影，都忍不住內心的激動想跟朋友分享心得，但是如果要跟外國朋友聊電影的話，「特效」、「票房」、「電影好好看！」這些單字跟句子該怎麼說呢？今天就來教你電影相關的單字詞彙，讓你不再只會用Great形容電影啦！把英文學好、學道地、學流利，從生活交談中開始！','content' => '"<p style=""font-size: 18px;"">每次看完新上映的電影，都忍不住內心的激動想跟朋友分享心得，但是如果要跟外國朋友聊電影的話，「特效」、「票房」、「電影好好看！」這些單字跟句子該怎麼說呢？今天就來教你電影相關的單字詞彙，讓你不再只會用Great形容電影啦！把英文學好、學道地、學流利，從生活交談中開始！<br /><br /></p>
<p style=""color: #4400b3; font-size: 22px;"">電影類型 Genre</p>
<p style=""font-size: 18px;"">Chick flick movie女性喜愛看的片（例如愛情喜劇片）<br />Bromance movie男性愛情片（例如《斷背山》）<br />Superhero movie 超級英雄片<br />Sci-fi movie科幻片<br />Horror movie 恐怖片<br />War movie 戰爭片<br />History movie 歷史片<br />Comedy movie 喜劇片<br />Suspense movie 懸疑片<br />Thriller movie 驚悚片<br />Mystery movie 推理片<br />Action movie 動作片<br />Romance movie 愛情片<br />Romantic comedy movie 浪漫喜劇片<br />Disaster movie 災難片</p>
<p style=""font-size: 18px;"">Animation movie 動畫片<br />Drama movie 劇情片<br />Musical film 音樂劇電影<br />Documentary 紀錄片<br />Blockbuster 賣座鉅片（這種電影成本極高，例如《復仇者聯盟》）</p>
<p style=""font-size: 22px; color: #4400b3;"">其他單字 Others</p>
<p style=""font-size: 18px;"">Box office 售票亭（買票地方）、票房（電影收入）<br />Cinematography 電影拍攝手法<br />Plot 劇情<br />Scene 電影場景<br />Budget 成本<br />Cast 演員<br />A-lister 一線明星（也就是A咖明星）<br />Performance （演員）表現、詮釋<br />Cameo 客串角色<br />Premiere 首映、首播<br />Debut （演員的）首演、第一次演出<br />Director 導演<br />Producer 製作人<br />Cinematographer 電影攝影師、攝影指導（也可說Director of Photography）<br />Special effects 特效<br />Sequel 續集<br />Prequel 前傳<br />Epic 史詩的（電影或書籍）、壯麗的</p>
<p style=""font-size: 18px; color: #4400b3;"">Epic除了有「史詩」意思，也可以替代Great來形容「很棒」的意思。</p>
<p style=""font-size: 18px;"">＊ The hamburger is epic.<br />這個漢堡太好吃了<br />＊ Epic fail大失敗<br />當你想灌籃時卻跌了一跤，這時別人就會說你Epic fail（大失敗）</p>
<p style=""font-size: 22px; color: #4400b3;"">問別人電影的技巧：</p>
<p style=""font-size: 18px;"">What&rsquo;s your favorite film?<br />你最愛的電影是哪部？<br />Why do you like the film?<br />為什麼喜歡那部片？</p>
<p style=""font-size: 18px;"">What genre of movie do you like?<br />你喜歡什麼類型的電影？<br />So you&rsquo;re a moviegoer?<br />所以你是愛看電影的人？</p>
<p style=""font-size: 18px;"">如何介紹一部電影：<br />It&rsquo;s a history movie.<br />它是歷史片<br />It&rsquo;s directed by Steven Spielberg.<br />這部電影由史蒂芬‧史匹柏執導<br />Tom Cruise starred in this film.<br />湯姆克魯斯主演這部電影</p>
<p style=""font-size: 18px;"">It&rsquo;s set in France during WWII.<br />電影故事設在世界二次大戰時的法國<br />＊ It&rsquo;s set in France：電影故事場景設在法國<br />＊ It&rsquo;s filmed in France：電影在法國當地拍攝<br />＊ It&rsquo;s filmed on location in Central Park. 電影實地在中央公園拍攝、取景（而非<br />後製）</p>
<p style=""font-size: 18px;"">It is adapted from a novel.<br />這部電影是小說改編<br />＊ Adapt：(1) 改編(書籍、事件)，(2) 適應<br />Academy Award for Best Adapted screenplay:奧斯卡最佳改編劇本<br />The movie is adapted from the best-selling novel of the same name.<br />這部電影改編自同名最佳暢銷小說<br />＊ 你也可以用based on(根據)代替Adapt<br />This movie is based on a true story.<br />這部電影根據真實事件改編</p>
<p style=""font-size: 22px; color: #4400b3;"">如何形容你對電影的感覺：</p>
<p style=""font-size: 18px;"">I love that movie.<br />我愛那部電影<br />It&rsquo;s a must-see.<br />這部電影非看不可<br />I love independent films.<br />我喜歡獨立電影<br />I prefer non-mainstream movies.<br />我偏好非主流電影<br />The cinematography was amazing.<br />電影手法很棒<br />It really brings art to a new level.<br />這部電影將藝術提升到新的層次<br />It captured / caught my attention from the beginning.<br />電影從一開始就引起了我的注意<br />＊ Something + capture / catch one&rsquo;s attention：某事物吸引人的注意<br />That reminds me another trilogy，The Lord of The Rings.<br />這讓我想起另一部三部曲，《魔戒》<br />Well&hellip;I&rsquo;m not a fan of romance movies.<br />這個嗎&hellip;我不是很喜歡愛情片<br />＊ I&rsquo;m not a fan of (doing something/this movie/food&hellip;)：我不是很喜歡&hellip;<br />This is not my type of movie.<br />我沒有很喜歡這種電影類型<br />＊ Not one&rsquo;s type：不是某人會喜歡的種類<br />He is not my type. 他不是我喜歡的類型<br />This is not my type of thing 我不會做這種事</p>"','published' => '1','og_title' => '"你還只會說The movie was great?學會這些電影單字讓你更能跟朋友暢聊電影！"','og_description' => '每次看完新上映的電影，都忍不住內心的激動想跟朋友分享心得，但是如果要跟外國朋友聊電影的話，「特效」、「票房」、「電影好好看！」這些單字跟句子該怎麼說呢？今天就來教你電影相關的單字詞彙，讓你不再只會用Great形容電影啦！把英文學好、學道地、學流利，從生活交談中開始！','meta_title' => '"你還只會說The movie was great?學會這些電影單字讓你更能跟朋友暢聊電影！"','meta_description' => '每次看完新上映的電影，都忍不住內心的激動想跟朋友分享心得，但是如果要跟外國朋友聊電影的話，「特效」、「票房」、「電影好好看！」這些單字跟句子該怎麼說呢？今天就來教你電影相關的單字詞彙，讓你不再只會用Great形容電影啦！把英文學好、學道地、學流利，從生活交談中開始！','canonical_url' => 'https://tw.english.agency/movie-vocabularies','feature_image_alt' => '"你還只會說The movie was great?學會這些單字讓你更能跟朋友暢聊電影！"','feature_image_title' => '"你還只會說The movie was great?學會這些單字讓你更能跟朋友暢聊電影！"','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a845a86f0b87-6AjjbE3AXLtRytAvFgkHrLgIQb1Zwl-724x483.jpeg','author_id' => '1','category_id' => '','created_at' => '"2018-02-14 15:53:51"'],
  ['id' => '129','title' => '護照、出境英文怎麼說？出國必備的機場英文單字報你知！','slug' => 'airport-vocabularies','excerpt' => '想出國旅行，卻害怕聽不懂國外地勤在說什麼嗎？每次拿到的入境表上面都是英文，不知道該怎麼寫才不會出錯？害怕過海關聽不懂海關講的話？今天就來學學最實用的搭飛機必備英文，讓你出國無往不利！','content' => '"<p style=""font-size: 16px;"">想出國旅行，卻害怕聽不懂國外地勤在說什麼嗎？每次拿到的入境表上面都是英文，不知道該怎麼寫才不會出錯？害怕過海關聽不懂海關講的話？今天就來學學最實用的搭飛機必備英文，讓你出國無往不利！</p>
<p style=""font-size: 22px; color: #a42d00;""><strong>機場出境</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Passport </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">護照</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Boarding Pass</span> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">登機證</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Boarding </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">登機</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Boarding gate</span> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">登機門</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Boarding time</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">登機時間</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Terminal</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">航厦</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Check-in</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">報到</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Check(v.)也有檢查、核對的意思</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>After finishing my exam，I made a check of every answer.</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>寫完考卷後，我檢查了所有答案</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Check-in counter</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">報到櫃檯</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Flight number </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">飛機編號</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Delayed</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">延誤</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Luggage </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">行李</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Hand luggage/ Carry on </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">手提行李</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Overweigh </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">超重</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Excess baggage charge </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">超重費</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Security check </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">安檢</span></p>
<p style=""font-size: 22px; color: #a42d00;""><strong>飛機上常用單字</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Cabin crew</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">機組員</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Ground staff </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">地勤人員</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Captain </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">機長</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Seat belt </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">安全帶</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Emergency exits </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">緊急出口</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Overhead luggage compartment </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">頭上放行李的置物空間</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">First-class cabin </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">頭等艙</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Business-class cabin </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">商務艙</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Economy-class cabin </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">經濟艙</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">In-flight entertainment </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">機上娛樂</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Estimated Time of Arrival (ETA)</span><span style=""font-weight: 400;""> ➪</span><span style=""font-weight: 400;"">機上娛樂系統</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Turbulence </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">亂流</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Blanket </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">毯子</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Reading light </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">閱讀燈</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Departure time</span> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">出發時間</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Arrival time</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">抵達時間</span>&nbsp;</p>
<p style=""font-size: 22px; color: #a42d00;""><strong>海關用語</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Customs </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">海關</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Customs declaration form </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">海關申報表</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Application form </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">申請表</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Transfer</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">轉機</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Transit </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">過境</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Transfer</strong><span style=""font-weight: 400;"">就是單純的轉機，例如從台北搭長榮航空到溫哥華，再轉機加拿大航空到蒙特婁。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Transit</strong><span style=""font-weight: 400;"">則是過境，例如從香港出發的長榮航空中停台北桃園站後以同一架飛機繼續延遠至東京成田</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Validity </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">效期</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Immigration counter </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">移民署櫃檯</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Foreign </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">外國的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Foreigner </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">外國人</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Goods to declare </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">申報物品</span></p>
<p style=""font-size: 16px;"">Destination ➪目的地</p>
<p style=""font-size: 16px;"">Visa ➪簽證</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Luggage claim </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">行李提領區</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Claim </strong><span style=""font-weight: 400;"">(v.)</span><span style=""font-weight: 400;"">聲稱、主張 </span><span style=""font-weight: 400;"">(n.)</span><span style=""font-weight: 400;"">要求、權利</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">John claimed that his phone was stolen，not lost. </span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">John 聲稱他的手機是被偷，不是遺失</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">Jason made a claim to the property.</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">Jason 認為他應該得到這筆財產</span>&nbsp;</p>
<p style=""font-size: 22px; color: #66009d;""><strong>機場常見對話</strong></p>
<p style=""font-size: 16px; color: #66009d;"">A: Where is your destination today? &nbsp;</p>
<p style=""font-size: 16px; color: #66009d;"">你今天的目的地是哪裡?</p>
<p style=""font-size: 16px; color: #66009d;"">B: I&rsquo;m flying to Paris today. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p style=""font-size: 16px; color: #66009d;"">我今天要飛往巴黎</p>
<p style=""font-size: 16px; color: #66009d;"">A: How many bags are you checking in today? 你今天有幾個行李要托運</p>
<p style=""font-size: 16px; color: #66009d;"">B: One. Please mark this bag as &ldquo;fragile&rdquo; 一個 請標示為易碎品</p>
<p style=""font-size: 16px; color: #66009d;"">A: Do you have a seat preference? 你有偏好的座位嗎</p>
<p style=""font-size: 16px; color: #66009d;"">B: Window seat/ Aisle seat，please. 靠窗/靠走道，麻煩了</p>
<p style=""font-size: 22px; color: #66009d;""><strong>機上常見對話</strong></p>
<p style=""font-size: 16px; color: #66009d;"">A: Would you like beef，chicken or fish? &nbsp;請問想吃牛肉、雞肉還是魚？</p>
<p style=""font-size: 16px; color: #66009d;"">B: I have ordered a special meal. &nbsp;我有點特別餐</p>
<p style=""font-size: 16px; color: #66009d;"">We are going to take off，would you please switch your electronic device?</p>
<p style=""font-size: 16px; color: #66009d;"">我們即將起飛，可以請你關閉電子傳輸用品嗎？</p>
<p style=""font-size: 16px; color: #66009d;"">We are now passing through an area of rough air (turbulence)，for your own safety，please return to your seat and fasten your seatbelt.</p>
<p style=""font-size: 16px; color: #66009d;"">我們正在經歷一段不穩定氣流(亂流)，為了您的安全，請回到座位並繫緊安全帶。</p>
<p style=""font-size: 22px; color: #66009d;""><strong>海關常見對話&nbsp;</strong></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">A: What&rsquo;s your purpose of visiting Canada? </span><span style=""font-weight: 400;"">你來加拿大的目的是什麼？</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">B: Business. I have a conference in Toronto. 商業 我在多倫多有場會議</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">A: How long will you stay? </span><span style=""font-weight: 400;"">你會待多久？</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">B: 5 days. 五天</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">A: Where will you stay? </span><span style=""font-weight: 400;"">你會住在哪裡？</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">B: Chelsea Hotel. The address is &hellip;. Chelsea 飯店，地址是&hellip;</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">A: Have you confirmed your return ticket? </span><span style=""font-weight: 400;"">你的回程機票訂好了嗎？</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">B: Yes. My flight number is BR35. 有的，我的飛機編號是BR35</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">A: Do you have anything to declare? </span><span style=""font-weight: 400;"">你有任何東西要申報嗎？</span></p>
<p style=""font-size: 16px; color: #66009d;""><span style=""font-weight: 400;"">B: No. I don&rsquo;t have any meat，vegetables or fruit with me. </span><span style=""font-weight: 400;"">沒有。我沒有帶任何肉、蔬菜、水果</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">看完以上介紹，你是不是對出國入海關會用到的英文單字更瞭解了呢？下次出國別再擔心無法跟外國海關溝通，把這些單字都背下來就一無萬失啦！</span></p>"','published' => '1','og_title' => '護照、出境英文怎麼說？出國必備的機場英文單字報你知！','og_description' => '想出國旅行，卻害怕聽不懂國外地勤在說什麼嗎？每次拿到的入境表上面都是英文，不知道該怎麼寫才不會出錯？害怕過海關聽不懂海關講的話？今天就來學學最實用的搭飛機必備英文，讓你出國無往不利！','meta_title' => '護照、出境英文怎麼說？出國必備的機場英文單字報你知！','meta_description' => '想出國旅行，卻害怕聽不懂國外地勤在說什麼嗎？每次拿到的入境表上面都是英文，不知道該怎麼寫才不會出錯？害怕過海關聽不懂海關講的話？今天就來學學最實用的搭飛機必備英文，讓你出國無往不利！','canonical_url' => 'https://tw.english.agency/airport-vocabularies','feature_image_alt' => '護照、出境英文怎麼說？出國必備的機場英文單字報你知！','feature_image_title' => '護照、出境英文怎麼說？出國必備的機場英文單字報你知！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a84e4accc058-M8Rnl0b3z0R9ETojlR8sateGo7BUZh-724x483.jpeg','author_id' => '1','category_id' => '','created_at' => '"2018-02-15 02:09:39"'],
  ['id' => '130','title' => '想要通過托福考試出國留學？這些托福必考單字你一定要學會！','slug' => 'test-vocabularies-of-TOEFL','excerpt' => '"要完成留學的夢想，從準備托福開始！托福主要測驗在國外學校環境中所用的英語能力，多數國外大學以托福成績准許申請入學，就算沒達到門檻，有些學校還會設定條件式入學(Conditional admission)，增加入學的難度，不過其實只要背好常見的托福單字，離出國留學之路就沒這麼遙遠了，現在就來看看小編精選的托福單字，讓你輕鬆拿到合格分數！"','content' => '"<p style=""font-size: 16px;"">要完成留學的夢想，從準備托福開始！托福主要測驗在國外學校環境中所用的英語能力，多數國外大學以托福成績准許申請入學，就算沒達到門檻，有些學校還會設定條件式入學(Conditional admission)，增加入學的難度，不過其實只要背好常見的托福單字，離出國留學之路就沒這麼遙遠了，現在就來看看小編精選的托福單字，讓你輕鬆拿到合格分數！</p>
<p style=""font-size: 18px; color: #880000;""><strong>1.Comprehensive [kɑːm.prəˈhen.sɪv] 綜合的</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Secondary school中學</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊State school ／ Public school 公立學校</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">I was educated at a comprehensive school.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">我在綜合中學受教育的</span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">*綜合中學（Comprehensive school）是一種公立中學，這種學制常見於英國，幾乎等同美國的公立學校。</span></p>
<p style=""font-size: 16px; color: #880000;""><strong>2.Feasible [ˈfiː.zə.bəl] 可行的</strong></p>
<p style=""font-size: 18px;""><span style=""font-weight: 400;"">＊Feasibility 可行性(名詞)</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Your plan seems feasible.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">你的計劃聽起來很可行</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>3.Fatigue [fəˈtiːɡ] 疲累的</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">After working as a reporter for 15 years，Lisa suffered from career fatigue.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">當記者15年以後，麗莎有了工作疲勞</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Suffer from 罹患、患有(疾病)</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Laure had been suffering from liver cancer for a year.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">蘿拉罹患肝癌已經有一年了</span></p>
<p style=""font-size: 16px;"">&nbsp;<span style=""font-weight: 400;"">＊Job burnout 工作倦怠</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>4.Umpire [ˈʌm.paɪr] 裁判</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">The player was not satisfied with the umpire&rsquo;s judgment.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">那位選手不滿意裁判的表決</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>5.Abide by [əˈbaɪd] 遵守、遵循</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">We must abide by the rules regulated by the school.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">我們必須遵守學校制定的規範</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Regulate 控制、管理；調整(動詞)</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Regulation 規定、規程(名詞)</span></p>
<p style=""font-size: 18px; color: #880000;""><br /><strong>6.Abrupt [əˈbrʌpt] 突然的</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Come to an abrupt end (某事物)突然停止</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Come to an end (某事物)結束、終止</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊All good things (must) come to an end</span> <span style=""font-weight: 400;"">天下無不散的筵席</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Their conversation came to an abrupt end when Tom came in.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">當湯姆進來時，他們的對話戛然而止</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">It seems that their relationship has come to an end.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">看來他們彼此的感情已經結束了</span></p>
<p style=""font-size: 18px; color: #880000;""><br /><strong>7.Excursion [ɪkˈskɝː.ʃən] 遠足</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Excursion into sth 涉足、涉獵某事物</span>&nbsp;</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">We will go on an excursion to Volendam next weekend</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">我們預計下周末去沃倫丹遠足</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">After an excursion into writing，Kelly decided to focus on her main interest，filming.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">在涉足寫作之後，凱利決定專心於主要的興趣，就是拍攝</span></p>
<p style=""font-size: 18px; color: #880000;""><br /><strong>8.Heir [er] 繼承人、接班人</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Make a fortune 賺大錢</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Harry is the rightful heir to the throne.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">哈利是王位的合法繼承人</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Jamie is the heir to his father&rsquo;s fortune.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">傑米是他父親的財產繼承人</span></p>
<p style=""font-size: 18px; color: #880000;""><br /><strong>9.Hemisphere [ˈhem.ə.sfɪr] 半球</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">In the southern hemisphere，spring is in September.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">在南半球，春天是在九月</span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">＊Spring has sprung 是一個常見的詞，代表「春天如雨後春筍般湧現」，也可說「春天正式來臨」</span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">＊Spring本身動詞意思就是「湧現」，所以Spring up就是「如雨後春筍般出現」</span></p>
<p style=""font-size: 18px; color: #880000;""><br /><strong>10.Adjacent [əˈdʒeɪ.sənt] 鄰接的、旁邊的</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Nick&rsquo;s house is adjacent to mine.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">尼克的家與我家相鄰</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>11.Intensify [ɪnˈten.sə.faɪ] 增強、加強</strong></p>
<p style=""font-size: 16px;"">&nbsp;<span style=""font-weight: 400;"">The tropical storm is likely to intensify into a hurricane.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">這個熱帶風暴可能轉強為颶風</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Due to the upcoming election，the government will intensify the security.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">由於選舉將近，政府將加強警備</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>12.Constitute [ˈkɑːn.stə.tuːt] 構成</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊Constitution 憲法</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊The Constitution of the United States美國憲法</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">＊The Constitution of the Republic of China中華民國憲法</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Fifty states constitute the U.S.A.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">美國由五十個州組成</span></p>
<p><br /><br /></p>"','published' => '1','og_title' => '想要通過托福考試出國留學？這些托福必考單字你一定要學會！','og_description' => '"要完成留學的夢想，從準備托福開始！托福主要測驗在國外學校環境中所用的英語能力，多數國外大學以托福成績准許申請入學，就算沒達到門檻，有些學校還會設定條件式入學(Conditional admission)，增加入學的難度，不過其實只要背好常見的托福單字，離出國留學之路就沒這麼遙遠了，現在就來看看小編精選的托福單字，讓你輕鬆拿到合格分數！"','meta_title' => '想要通過托福考試出國留學？這些托福必考單字你一定要學會！','meta_description' => '"要完成留學的夢想，從準備托福開始！托福主要測驗在國外學校環境中所用的英語能力，多數國外大學以托福成績准許申請入學，就算沒達到門檻，有些學校還會設定條件式入學(Conditional admission)，增加入學的難度，不過其實只要背好常見的托福單字，離出國留學之路就沒這麼遙遠了，現在就來看看小編精選的托福單字，讓你輕鬆拿到合格分數！"','canonical_url' => 'https://tw.english.agency/test-vocabularies-of-TOEFL','feature_image_alt' => '想要通過托福考試出國留學？這些托福必考單字你一定要學會！','feature_image_title' => '想要通過托福考試出國留學？這些托福必考單字你一定要學會！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a84eee0610cb-3syYDQZlhfQVL9PFFpFOMB4tGxpI62-724x483.jpeg','author_id' => '1','category_id' => '15','created_at' => '"2018-02-15 02:37:28"'],
  ['id' => '131','title' => '想在國外星巴克點咖啡？你一定要學會的星巴克英文單字！','slug' => 'vocabularies-of-Starbucks','excerpt' => '上班、上課前去Starbucks買一杯咖啡是普遍的大眾習慣，如果今天在國外，突然想喝星巴克的咖啡，又該怎麼用英文點咖啡呢？今天就來學學Starbucks專用英文，讓你在國外也能一解咖啡癮的愁！','content' => '"<p style=""font-size: 16px;"">上班、上課前去Starbucks買一杯咖啡是普遍的大眾習慣，如果今天在國外，突然想喝星巴克的咖啡，又該怎麼用英文點咖啡呢？今天就來學學Starbucks專用英文，讓你在國外也能一解咖啡癮的愁！</p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Size 尺寸</strong></p>
<p style=""font-size: 16px; color: #a42d00;"">#Starbucks咖啡size是用義大利文的說法，而不是我們平常慣用的英文(Small，Medium，Large，Extra Large)喔！</p>
<p style=""font-size: 16px;""><strong>Tall </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">中杯</span></p>
<p style=""font-size: 16px;""><strong>Grande </strong><span style=""font-weight: 400;"">➩</span> <span style=""font-weight: 400;"">大杯</span></p>
<p style=""font-size: 16px;""><strong>Venti </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">特大杯</span></p>
<p style=""font-size: 16px;""><strong>Trenta </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">比特大杯更大杯 </span></p>
<p style=""font-size: 16px; color: #003c9d;"">* Venti和Trenta分別是義大利文中的20 twenty和30 thirty 的意思</p>
<p style=""font-size: 16px;"">另外在Starbucks的杯子後面，會看到六格客製化調配的選項，分別是：</p>
<p style=""font-size: 16px;""><strong>Decaf <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">低咖啡因(</span>Decaffeinated<span style=""font-weight: 400;"">)的縮寫，也就是去除咖啡因的意思</span></strong></p>
<p style=""font-size: 16px;""><strong>Shots </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">濃縮咖啡(</span><strong>expresso</strong><span style=""font-weight: 400;"">)的計量單位，一個shot是一份，double shots</span><span style=""font-weight: 400;"">為兩份，</span><span style=""font-weight: 400;"">用來調整咖啡的濃淡</span></p>
<p style=""font-size: 16px;""><strong>Syrup </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">糖漿</span></p>
<p style=""font-size: 16px;""><strong>Milk </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">奶的類別，例如全脂奶 (</span><strong>whole milk</strong><span style=""font-weight: 400;"">)、低脂奶 (</span><strong>low-fat milk</strong><span style=""font-weight: 400;"">)、</span><span style=""font-weight: 400;"">零脂奶(</span><strong>non-fat milk</strong><span style=""font-weight: 400;"">)、豆漿 (</span><strong>soy milk</strong><span style=""font-weight: 400;"">)</span></p>
<p style=""font-size: 16px;""><strong>Custom </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">客制，全寫為</span><strong>Customized</strong><span style=""font-weight: 400;"">，客人的個人需求，例如少冰(</span><strong>less ice</strong><span style=""font-weight: 400;"">)</span></p>
<p style=""font-size: 16px;""><strong>Drink </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">飲品名稱</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Types 種類</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">接下來就介紹一些比較常見的Strabucks咖啡種類：</span></p>
<p style=""font-size: 16px;""><strong>Cappuccino</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">卡布奇諾</span></p>
<p style=""font-size: 16px;""><strong>Mocha </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">摩卡</span></p>
<p style=""font-size: 16px;""><strong>White chocolate mocha</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">白巧克力摩卡</span></p>
<p style=""font-size: 16px;""><strong>Latte</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">拿鐵</span></p>
<p style=""font-size: 16px;""><strong>Green tea latte </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">抹茶拿鐵</span></p>
<p style=""font-size: 16px;""><strong>Caramel macchiato </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">焦糖瑪奇朵</span></p>
<p style=""font-size: 16px;""><strong>Americano</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">美式咖啡 </span></p>
<p style=""font-size: 16px;""><strong>Frappuccino</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">星冰樂 簡稱</span><strong>Frap</strong></p>
<p style=""font-size: 16px;""><strong>Caramel Frappuccino</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">焦糖星冰樂</span></p>
<p style=""font-size: 16px;""><strong>Vanilla bean Frappuccino</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">香草豆星冰樂</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Others 其他相關單字</strong></p>
<p style=""font-size: 16px;""><strong>Mug </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">馬克杯</span></p>
<p style=""font-size: 16px;""><strong>Tumbler</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">隨行杯</span></p>
<p style=""font-size: 16px;""><strong>Whipped cream</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">奶油</span></p>
<p style=""font-size: 16px;""><strong>Foam </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">奶泡</span></p>
<p style=""font-size: 16px;""><strong>Sleeve</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">防熱套</span></p>
<p style=""font-size: 20px; color: #3a0088;""><strong>Dialogues 實用對話</strong></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">Customer: &ldquo; Hi. Can I get a grande salted caramel mocha Frap? &ldquo;</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">客人: 嗨，可以給我一杯大杯海鹽焦糖摩卡星冰樂嗎？</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">&nbsp;&ldquo;Do you want cream on top?&rdquo; </span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">&nbsp;你想要奶油在上面嗎？</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">&nbsp;&ldquo;Sure，please&rdquo;</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">&nbsp;要的，麻煩你了</span></p>
<p style=""font-size: 16px; color: #3a0088;"">Customer: &ldquo;Hello，I would like a tall latte please.&rdquo;</p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">嗨，我想要一杯中杯拿鐵，麻煩了</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;""> &nbsp;&ldquo;OK. Anything to eat?&rdquo;</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;""> &nbsp;好的。要吃什麼嗎？</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;"">&nbsp;&ldquo;Um&hellip; A double-smoked bacon，cheddar and egg sandwich，thank you&rdquo;</span></p>
<p style=""font-size: 16px; color: #3a0088;""><span style=""font-weight: 400;""> &nbsp;嗯..一個雙燻培根、切達起司蛋沙拉三明治 謝謝</span></p>
<p style=""font-size: 16px;""><strong>Starbucks的成功是眾所皆知的，但是什麼樣的因素讓這原先只賣咖啡豆的小咖啡店，在50年間成為全球最大的連鎖咖啡店呢？Joseph Michelli 花了無數時間和Starbucks高階主管們探討，得出了五個關鍵因素，以下節錄：</strong></p>
<p><span style=""font-weight: 400;"">(原文:</span> <a href=""https://addicted2success.com/startups/why-starbucks-is-so-successful-5-must-have-ingredients/""><span style=""font-weight: 400;"">https://addicted2success.com/startups/why-starbucks-is-so-successful-5-must-have-ingredients/</span></a><span style=""font-weight: 400;"">)</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Create strategic alliances with your employee&nbsp;&nbsp;與員工建立戰略聯盟</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Starbucks consistently spends more on training that it does on advertising. They treat their teams with enough care and concern to inspire passion and creativity. </span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">相較於廣告，Starbucks一直以來都花更多在員工訓練上。他們給予團隊足夠的關懷和關注，激發了熱情與創造力。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Starbucks 5 ways of being&nbsp;&nbsp;Starbucks的五種特質</strong></p>
<p><span style=""font-weight: 400;"">Every partner is coached on embodying the Starbucks 5 ways of being，which creates a consistently fresh and delightful experience for patrons.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Starbucks的每個員工都要體驗五種特質，為顧客創造了始終如一的新鮮和愉快體驗</span></p>
<p style=""font-size: 16px;""><strong>Be welcoming</strong><span style=""font-weight: 400;""> 歡迎的</span></p>
<p style=""font-size: 16px;""><strong>Be genuine</strong><span style=""font-weight: 400;""> 真誠的</span></p>
<p style=""font-size: 16px;""><strong>Be considerate</strong><span style=""font-weight: 400;""> 週到的</span></p>
<p style=""font-size: 16px;""><strong>Be knowledgeable</strong><span style=""font-weight: 400;""> 知識淵博的</span></p>
<p style=""font-size: 16px;""><strong>Be involved </strong><span style=""font-weight: 400;"">參與的</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Everything matters&nbsp;</strong>每一件事情都很重要</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">This is referring to solid processes and procedures in daily operations. Starbucks puts an emphasis on&nbsp;</span><em><span style=""font-weight: 400;"">consistency</span></em><span style=""font-weight: 400;"">，even in the minute details. They take the mentality of nothing is trivial and our customers notice everything.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">這是指日程運作中的實際流程和程序。Starbucks強調一致性，即使只是小細節。他們採取沒有任何事情是不重要、顧客會注意所有細節的心態。</span></p>
<p style=""font-size: 18px; color: #a42d00;""><strong>Embrace resistant&nbsp;</strong>接受批評指教</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">When faced with customer complaints，there is an&nbsp;opportunity&nbsp;to actually turn that perceived&nbsp;</span><a href=""https://addicted2success.com/life/2-ways-to-stay-positive-when-dealing-with-negative-people/""><span style=""font-weight: 400;"">negative into a head over heels positive</span></a><span style=""font-weight: 400;"">. This is an opportunity to learn more about what you can do，how to become better and ultimately become closer to creating a great&nbsp;experience&nbsp;for the customer. Just listening is not enough，you must take action which shows the customers that their voices are heard，thus creating brand loyalty.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">當面對客戶投訴時，這其實是一個將負面事實轉為正面的機會。這是一個能更了解該多做什麼、如何變得更好，並最終讓顧客獲得更好的顧客體驗的機會。只有聆聽是不夠的，需要採取行動，讓顧客知道他們的聲音有被聽到，並從而創造出產品忠誠度。</span></p>"','published' => '1','og_title' => '想在國外星巴克點咖啡？你一定要學會的星巴克英文單字！','og_description' => '上班、上課前去Starbucks買一杯咖啡是普遍的大眾習慣，如果今天在國外，突然想喝星巴克的咖啡，又該怎麼用英文點咖啡呢？今天就來學學Starbucks專用英文，讓你在國外也能一解咖啡癮的愁！','meta_title' => '想在國外星巴克點咖啡？你一定要學會的星巴克英文單字！','meta_description' => '上班、上課前去Starbucks買一杯咖啡是普遍的大眾習慣，如果今天在國外，突然想喝星巴克的咖啡，又該怎麼用英文點咖啡呢？今天就來學學Starbucks專用英文，讓你在國外也能一解咖啡癮的愁！','canonical_url' => 'https://tw.english.agency/vocabularies-of-Starbucks','feature_image_alt' => '想在國外星巴克點咖啡？你一定要學會的星巴克英文單字！','feature_image_title' => '想在國外星巴克點咖啡？你一定要學會的星巴克英文單字！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a85145c1aa9a-hHGKWQnlkzoaGqoSISg7L1BIki3nys-1015x577.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-15 05:46:24"'],
  ['id' => '132','title' => '捷運上人潮洶湧該怎麼說？六種惹人厭捷運上現象英文單字一定要學會！','slug' => 'subway-phonamena','excerpt' => '"每次上下班時間要搭捷運時，門一打開裡面擠滿了各種不同職業背景的人，但也不乏有許多莫名其妙的人是通勤族的惡夢，狐臭、太濃的香水味、愛佔位的人這些討厭的現象英文要怎麼說？今天就來帶大家學習這些令人困擾的subway worst people的英文說法，下次想跟外國朋友抱怨捷運太擁擠就不怕沒詞彙可說啦！"','content' => '"<p style=""font-size: 16px;"">每次上下班時間要搭捷運時，門一打開裡面擠滿了各種不同職業背景的人，但也不乏有許多莫名其妙的人是通勤族的惡夢，狐臭、太濃的香水味、愛佔位的人這些討厭的現象英文要怎麼說？今天就來帶大家學習這些令人困擾的subway worst people的英文說法，下次想跟外國朋友抱怨捷運太擁擠就不怕沒詞彙可說啦！</p>
<p style=""font-size: 18px; color: #880000;""><strong>Subway<span style=""font-weight: 400;""> vs. </span>Underground <span style=""font-weight: 400;"">vs. </span>Metro</strong></p>
<p style=""font-size: 16px;""><strong>Subway </strong><span style=""font-weight: 400;"">是美式英文，像紐約地鐵就叫</span><span style=""font-weight: 400;"">NYC Subway</span></p>
<p style=""font-size: 16px;""><strong>Underground </strong><span style=""font-weight: 400;"">是地底、地下的意思，也指 style=""font-size: 16px;""英式英文中的地鐵</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Ex. </span><span style=""font-weight: 400;"">London underground</span><span style=""font-weight: 400;""> 倫敦地鐵</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;""> 在英國也可以用</span><span style=""font-weight: 400;"">Tube</span><span style=""font-weight: 400;"">來指地鐵</span></p>
<p style=""font-size: 16px;""><strong>Metro</strong> <span style=""font-weight: 400;"">台北捷運就是用Metro這個詞，而MRT則是指：</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Mass</span><span style=""font-weight: 400;"">(大眾) </span><span style=""font-weight: 400;"">Rapid</span><span style=""font-weight: 400;""> (快速的)</span><span style=""font-weight: 400;""> Transit</span><span style=""font-weight: 400;""> (運輸系統)</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Commute </span><span style=""font-weight: 400;"">(v.) 通勤，也有減刑的意思</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Commuter </span><span style=""font-weight: 400;"">(n.) 通勤族</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Although Kevin loved his job，he was becoming tired of being a commuter.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">儘管Kevin很熱愛他的工作，他厭倦了當一名通勤者</span></p>
<p>&nbsp;</p>
<p style=""font-size: 18px; color: #880000;""><strong>Manspreaders</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Manspreaders指的是開腿族，</span><span style=""font-weight: 400;"">spread</span><span style=""font-weight: 400;"">在這裡是擴散、擴張的意思，就是坐捷運時腿張很開、佔據了鄰座的令人困擾行為，像是紐約、西班牙馬德里都有修法禁止manspreading (開腿)</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Spread</span><span style=""font-weight: 400;""> (v.) 展開、途、使分散</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Spread one&rsquo;s arms wide </span><span style=""font-weight: 400;"">張開雙臂</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Spread butter on the bread </span><span style=""font-weight: 400;"">在麵包上塗奶油</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Spread the word </span><span style=""font-weight: 400;"">散佈消息</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">The fragrance of flowers spread widely. 鮮花的香氣傳得很廣</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>Pole leaners</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Pole leaners 指的是喜歡將身體靠在欄杆上的人，</span><span style=""font-weight: 400;"">Pole</span><span style=""font-weight: 400;"">指的是地鐵上的杆子，</span><span style=""font-weight: 400;"">lean</span><span style=""font-weight: 400;"">則是有倚靠的意思</span></p>
<p style=""font-size: 16px;"">Lean on 依賴</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Brian always leans on his parents to help. He&rsquo;s such a mummy&rsquo;s boy.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Brian 總是依賴他的父母來幫助他。他是一個媽寶</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>Squeezers </strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Squeezers 指的是在列車已經飽和，還硬要擠上車的人，</span><span style=""font-weight: 400;"">Squeeze</span><span style=""font-weight: 400;"">本身的動詞意思就有榨、擠、壓、擰，</span><span style=""font-weight: 400;"">squeezer</span><span style=""font-weight: 400;"">也有榨汁機、剝削壓榨者的意思</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tiffany squeezed the lemon hard and the last bit of juice came out. </span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tiffany 使勁擠檸檬，擠出了最後一點果汁</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>Backpack wearers</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">尖峰時間的地鐵很擠，背在身後的後背包很容易擋路，適時地把後背包拿下來放在腳下，給自己也給其他人一點空間吧～</span></p>
<p style=""font-size: 16px;"">* 英文中的wear除了「穿」(wear a jacket) 還有其他用法：</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Wear perfume </span><span style=""font-weight: 400;"">擦香水</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Wear makeup</span><span style=""font-weight: 400;""> 化妝</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Wear glasses</span><span style=""font-weight: 400;""> 戴眼鏡</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Wear a smile</span><span style=""font-weight: 400;""> 面露微笑</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>Doorway blockers</strong></p>
<p><span style=""font-weight: 400;"">指的是在捷運上擋門的人，即使後面有空間還是站在門中間，讓人進出困擾</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">* Way</span><span style=""font-weight: 400;"">在英文中有很多用法：</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">美劇中很常聽到 &ldquo;</span><span style=""font-weight: 400;"">My way or the highway&rdquo;</span><span style=""font-weight: 400;"">，</span><span style=""font-weight: 400;"">way</span><span style=""font-weight: 400;"">是路，</span><span style=""font-weight: 400;"">highway</span><span style=""font-weight: 400;""> 是高速公路，而</span><span style=""font-weight: 400;"">my way</span><span style=""font-weight: 400;"">也是「我的方式」，所以這句話的語氣很強烈：不聽我的就快滾 的意思</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">OMW = On My Way </span><span style=""font-weight: 400;"">我出發了，在路上，也可以用on your way</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">/on his way/on her way</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">I am on my way to the gym. </span><span style=""font-weight: 400;"">我正在去健身房的路上</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">BTW = By The Way </span><span style=""font-weight: 400;"">順帶一提</span></p>
<p style=""font-size: 18px; color: #880000;""><strong>Phone snooper</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">這種就是小編最痛恨的啦～陌生人盯著自己手機看的感覺真的很差</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">snoop是窺探的意思，可以作動詞也可作名詞 </span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Snoop around</span><span style=""font-weight: 400;""> 打探、探聽</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Have a snoop around</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Snoop into sth</span><span style=""font-weight: 400;"">. 探聽某事</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">I wish you could stop snooping into my business </span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">我希望你可以停止探聽我的事情</span></p>"','published' => '1','og_title' => '捷運上人潮洶湧該怎麼說？六種惹人厭捷運上現象英文單字一定要學會！','og_description' => '"每次上下班時間要搭捷運時，門一打開裡面擠滿了各種不同職業背景的人，但也不乏有許多莫名其妙的人是通勤族的惡夢，狐臭、太濃的香水味、愛佔位的人這些討厭的現象英文要怎麼說？今天就來帶大家學習這些令人困擾的subway worst people的英文說法，下次想跟外國朋友抱怨捷運太擁擠就不怕沒詞彙可說啦！"','meta_title' => '捷運上人潮洶湧該怎麼說？六種惹人厭捷運上現象英文單字一定要學會！','meta_description' => '"每次上下班時間要搭捷運時，門一打開裡面擠滿了各種不同職業背景的人，但也不乏有許多莫名其妙的人是通勤族的惡夢，狐臭、太濃的香水味、愛佔位的人這些討厭的現象英文要怎麼說？今天就來帶大家學習這些令人困擾的subway worst people的英文說法，下次想跟外國朋友抱怨捷運太擁擠就不怕沒詞彙可說啦！"','canonical_url' => 'https://tw.english.agency/subway-phonamena','feature_image_alt' => '捷運上人潮洶湧該怎麼說？六種惹人厭捷運上現象英文單字一定要學會！','feature_image_title' => '捷運上人潮洶湧該怎麼說？六種惹人厭捷運上現象英文單字一定要學會！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a85219d33b1e-Uq3Bn4JAqOpuxxEI6Y7qOiX5ahmg95-2214x1354.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-15 06:09:54"'],
  ['id' => '133','title' => '聖誕節交換禮物、聖誕祝福的英文怎麼說？','slug' => 'merry-christmas','excerpt' => '"十二月最讓人期待的就是聖誕節了！聖誕節只會說Merry Christmas嗎？來學一些聖誕祝福語，讓你跟其他人不一樣！"','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<h2><strong><span style=""color: #0088a8;"">聖誕節相關單字</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>十二月最讓人期待的就是聖誕節了！聖誕節是個跟基督教很有關聯的節日，在這裡也介紹一些聖經故事中的常見英文，在國外參觀教堂時也很可能會聽到喔：</strong></span></p>
<ul>
<li><em>Christmas ➩聖誕</em></li>
<li><em>Christmas Eve ➩平安夜</em></li>
<li><em>Jesus ➪耶穌</em></li>
<li><em>Mary ➪聖母瑪利亞</em></li>
<li><em>Joseph ➪瑪利亞的丈夫</em></li>
<li><em>Christian ➪基督教徒</em></li>
<li><em>Catholic ➪天主教徒</em></li>
<li><em>Chapel ➪小教堂</em></li>
<li><em>Church ➪教會</em></li>
<li><em>Cathedral ➪大教堂(主教座堂</em></li>
<li><em>Basilica ➪宗教聖殿</em></li>
<li><em>Abbey ➪修道院</em></li>
<li><em>Christmas tree ➪聖誕樹</em></li>
<li><em>Christmas light ➩聖誕燈</em></li>
<li><em>Christmas stocking ➪聖誕長襪</em></li>
<li><em>Santa Claus ➪聖誕老人</em></li>
<li><em>Reindeer ➪麋鹿</em></li>
<li><em>Snowman ➪雪人</em></li>
<li><em>Snowflake ➪雪花</em></li>
<li><em>Sled ➪雪橇</em></li>
<li><em>Gingerbread man ➪薑餅人</em></li>
<li><em>Gingerbread house ➪薑餅屋</em></li>
<li><em>Candy cane ➪拐杖糖</em></li>
<li><em>Christmas gift ➪聖誕禮物</em></li>
<li><em> Gift exchange ➪交換禮物</em></li>
<li><em>Secret Santa ➪外國人很愛玩，就是每個人輪流抽出自己要送禮物的對象，收到禮物的人要猜出是誰送的遊戲</em></li>
<li><em>Wrap ➪包裝(v.)</em></li>
<li><em>Eggnog ➪蛋酒，國外很流行的聖誕節、新年必喝飲品</em></li>
<li><em>Angel ➪天使</em></li>
<li><em>Elf ➪小精靈</em></li>
<li><em>Star ➪星<br />Star of Bethlehem 伯利恆之星，也稱作聖誕之星或耶穌之星，是耶穌誕生時天上一顆特別亮的指引東方三賢士(Biblical Magi)找到耶穌</em></li>
<li><em>Fireplace ➪壁爐</em></li>
<li><em>Wreath ➪花圈</em></li>
<li><em>Bell ➪鈴鐺</em></li>
<li><em>Basket ➪藍子</em></li>
<li><em>Candle ➪蠟燭</em></li>
<li><em>Chimney ➪煙囪</em></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><img title=""Christmas Wishes 聖誕祝福語"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a852d5184586-Lka2SrBOhoFYuenWTLUH772St1Rm5z-1090x724.png"" alt=""Christmas Wishes 聖誕祝福語"" width=""100%"" height=""724"" />&nbsp;</p>
<h2><strong><span style=""color: #0088a8;"">Christmas Wishes 聖誕祝福語</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>聖誕節不能只會Merry Christmas and happy New Year！想要說一口流利的道地聖誕祝福語，就看這：</strong></span></p>
<p>&nbsp;</p>
<ul>
<li><em>A Christmas greeting to cheer you，my dear friend/family/roommate&hellip;<br />獻上令你開心的祝福，我親愛的朋友/家人/室友...</em></li>
<li><em>May the good times and treasures of the present become the golden<br />memories of tomorrow. Wishing you lots of love，joy and happiness. Merry&nbsp;Christmas!<br />願現在的美好時光和珍愛成為明天的美好回憶。祝福你擁有很多愛、喜悅和幸福。聖誕快樂！</em></li>
<li><em>Christmas is the proof that this world can become a better place. If we have&nbsp;lots of people like you who fills it with happiness and hope.<br />聖誕節是一個世界能變更好的證明。如果我們能擁有很多像你這樣充滿幸福和希望的人。</em></li>
<li><em>Jesus loves you，Santa loves you and I love you. Wow，you are really<br />popular!<br />耶穌愛你、聖誕老人愛你，還有我愛你。哇，你真是受歡迎！</em></li>
<li><em>Jingle bells ding dang，good luck a big box，Santa to send peace，happiness&nbsp;away.<br />鈴聲響叮噹，好運一大箱，聖誕老人送平安、幸福走四方。</em></li>
</ul>
<p>&nbsp;</p>
<h2><strong><span style=""color: #0088a8;"">Christmas Poems 聖誕詩</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>最後，來看一首關於聖誕樹的童詩：</strong></span></p>
<br />A Christmas Tree &ndash; Kevin Portilla 一棵聖誕樹 By Kevin Portilla<br />One little star on the top of the tree，一顆小星星位在樹頂<br />Two little presents underneath for me，兩個小禮物在我之下<br />Three silver ropes twisted around the tree，三根銀繩纏繞在樹上<br />Four colored lights shining prettily，四顆彩色燈閃閃發亮<br />Five shining balls flowing silvery. 五顆流淌銀色發亮的球<br />Oh! What a sigh for use to see 噢！真是嘆為觀止
<ul>
<li><em>Presents (n.) 禮物</em></li>
<li><em>Underneath (prep.) 在&hellip;下面</em></li>
<li><em>Silver (n.) 銀 (adj.) 銀的</em></li>
<li><em>Twist (v.)扭轉、纏繞</em></li>
<li><em>Flow (v.)流動 (n.)流、流暢、連貫</em></li>
<li><em>Silvery (adj.) 有銀色光澤的</em></li>
</ul>
</div>"','published' => '1','og_title' => '聖誕節交換禮物、聖誕祝福的英文怎麼說？','og_description' => '"十二月最讓人期待的就是聖誕節了！聖誕節只會說Merry Christmas嗎？來學一些聖誕祝福語，讓你跟其他人不一樣！"','meta_title' => '聖誕節交換禮物、聖誕祝福的英文怎麼說？','meta_description' => '"十二月最讓人期待的就是聖誕節了！聖誕節只會說Merry Christmas嗎？來學一些聖誕祝福語，讓你跟其他人不一樣！"','canonical_url' => 'https://tw.english.agency/merry-christmas','feature_image_alt' => '聖誕節交換禮物、聖誕祝福的英文怎麼說？','feature_image_title' => '聖誕節交換禮物、聖誕祝福的英文怎麼說？','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8544ae74857-8SqECN442HS56ptWNjB6jKcKyqdjXL-1200x630.png','author_id' => '1','category_id' => '','created_at' => '"2018-02-15 06:33:31"'],
  ['id' => '134','title' => '如何讀懂外國新聞媒體？從美國賭城恐攻報導學英文！','slug' => 'terror-attacks','excerpt' => '美國拉斯維加斯發生史上最大規模的恐攻事件！外國媒體CNN、BBC等在第一時間都有即時報導、後續追蹤。今天就來教大家一些恐怖攻擊相關英文，學會了就能輕鬆讀懂外國新聞，掌握第一手資訊','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>美國拉斯維加斯發生史上最大規模的恐攻事件！外國媒體CNN、BBC等在第一時間都有即時報導、後續追蹤。今天就來教大家一些恐怖攻擊相關英文，學會了就能輕鬆讀懂外國新聞，掌握第一手資訊</p>
<h2><strong><span style=""color: #0088a8;"">Terror</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>Terror本身為名詞「恐怖」、「驚駭」的意思</strong></span></p>
<p>動詞形式為Terrorize<br />to terrorize sb. into doing sth. 是威懾、恐嚇某人做某事的意思<br />Ex. The employer is trying to terrorize the union into ending their strikes.<br />雇主正在試圖恐嚇工會結束罷工</p>
<p style=""color: #cc0000;""><strong>【延伸】</strong></p>
<ul>
<li><em><span style=""color: #fa8072;""><strong>Terrorism 恐怖主義</strong></span><br />Ism結尾的單字通常都帶有「主義」、「學派」的意思：<br />Capitalism 資本主義<br />Impressionism 印象派<br />Humanitarianism 人道主義<br />Anti-terrorism 反恐主義<br />Anti開頭的字大多都是「反對」：<br />Anti-social反社會的、Antiwar反戰的</em></li>
<li><em><span style=""color: #fa8072;""><strong>Terrorist 恐怖份子</strong></span><br />Ist 結尾的字大多是「職業」、或是「從事這件事的人」：<br />Terrorist 旅客<br />Scientist 科學家<br />Zoologist 動物學家</em></li>
<li><em><span style=""color: #fa8072;""><strong>Terror attack 恐怖攻擊</strong></span><br />Attack可作動詞也可作名詞，heart attack 片語：心臟病發作<br />Suicide attack 自殺式攻擊<br />Suicide bomber 自殺炸彈客<br />自殺的動詞可以用：commit suicide。<br />這裡的commit當動詞用，有「做某種事的含義」，類似用法還有commit a crime犯罪</em></li>
</ul>
<h2><strong><span style=""color: #0088a8;"">Radical</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>Radical 是形容詞「激進的」、「根本的」</strong></span></p>
<p>也可以作為名詞「根本」、「激進份子(要大寫)」<br />Ex. Jim is a radical political<br />Jim是一個政治上的激進份子<br />There is no radical cure for AIDS.<br />現在還沒有根治愛滋病的治藥</p>
<p style=""color: #cc0000;""><strong>【延伸】</strong></p>
<ul>
<li><em>Radicalize 動詞，使激進</em></li>
<li><em>Radicalism 激進主義</em></li>
<li><em>Extreme 極端的</em></li>
<li><em>Extremism 極端主義</em></li>
<li><em>Go to extremes 走極端</em></li>
<li><em>Bloody<br />Bloody 是形容詞「血腥的」，名詞是blood 血，動詞bleed 流血</em></li>
</ul>
<p style=""color: #cc0000;""><strong>【延伸】</strong></p>
<ul>
<li><em> It&rsquo;s bleeding 在流血</em></li>
<li><em> A nose bleeding 流鼻血</em></li>
<li><em> Blue blood 貴族血統<br />David is proud of being a blue-blooded member of the royalty.<br />David 很驕傲是血統純正的貴族成員</em></li>
<li><em>Violent 暴力的，</em><em>名詞為violence</em></li>
<li><em>Casualty<br />Casualty是名詞 「傷亡人數」的意思，容易混淆字有：<br />Casual (adj.) 隨便的、非正式的<br />Casual wear 休閒裝<br />Business casual 商務休閒<br />Casually (adv.) 順便、胡<br />Causal (adj.) 因果<br />Causality (n.) 因果關係</em></li>
</ul>
</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<h3 style=""font-size: 20px; color: #a42d00;""><strong><img title=""恐怖份子的兒子選擇反恐"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a85af345e630-k8OAvJaIJdPfN8rc72wopMnGkVClgU-1116x600.png"" alt=""恐怖份子的兒子選擇反恐"" width=""100%"" height=""600"" /></strong></h3>
<h3 style=""font-size: 20px; color: #a42d00;""><strong>恐怖份子的兒子選擇反恐</strong></h3>
</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p><span style=""color: #ee7700;""><strong>近幾年，恐怖攻擊越來越猖獗，世界各地都隱藏著不安。Zak Ebrahim，是一位恐怖份子的兒子，他發表了一篇鼓舞人心的演說，敘述他是怎麼在仇恨、教條的成長過程中，選擇了反恐。</strong></span></p>
(<a href=""https://www.ted.com/talks/zak_ebrahim_i_am_the_son_of_a_terrorist_here_s_ how_i_chose_peace"">以下節錄演講中最激勵的一段</a>)&nbsp;</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">&nbsp;</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">I do it in the hopes that perhaps someone someday&nbsp;who is compelled to use violence&nbsp;may hear my story and realize that there is a better way，&nbsp;that although I had been subjected&nbsp;to this violent，intolerant ideology，&nbsp;that I did not become fanaticized</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">我希望，也許有人某天決定使用武力反抗社會時，他可以聽到我的故事並意識 到有比動武更好的方法。儘管我曾被灌輸這樣的暴力思維、偏狹的意識，但我並沒有為其所動。</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">&nbsp;</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">Instead，I choose to use my experience&nbsp;to fight back against terrorism，&nbsp;against the bigotry.&nbsp;I do it for the victims of terrorism&nbsp;and their loved ones，&nbsp;for the terrible pain and loss&nbsp;that terrorism has forced upon their lives.</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">反而，我選擇透過自己的經驗來抵抗恐怖主義、反對偏見。我是為了那些遭受恐 怖襲擊的受害者以及他們的親人而做，我是為了那些被恐怖主義活動所導致的 生命的苦痛與損失而做</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">&nbsp;</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">For the victims of terrorism，I will speak out&nbsp;against these senseless acts&nbsp;and condemn my father\'s actions.&nbsp;I stand here as proof&nbsp;that violence isn\'t inherent in one\'s religion or race，&nbsp;and the son does not have to follow&nbsp;the ways of his father.&nbsp; &nbsp;</div>
<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">為了那些遭受恐怖襲擊的受害者，我站出來&nbsp;反對這些毫無意義的襲擊行動，&nbsp;譴 責我父親的行為。&nbsp;我站在這裡證明&nbsp;暴力不是宗教或種族與生俱來的，兒子也不需要走上和父親相同道路。&nbsp;
<ul>
<li><em>Compelled (v.) 被迫</em></li>
<li><em> Intolerant (adj.) 不可忍耐的</em></li>
<li><em> Ideology (n.) 思想意識</em></li>
<li><em> Fanaticized (adj.) 成為狂熱的</em></li>
<li><em> Bigotry (n.) 偏執</em></li>
<li><em> Condemn (v.) 譴責</em></li>
<li><em> Inherent (adj.) 固有的</em></li>
</ul>
</div>"','published' => '1','og_title' => '如何讀懂外國新聞媒體？從美國賭城恐攻報導學英文！','og_description' => '美國拉斯維加斯發生史上最大規模的恐攻事件！外國媒體CNN、BBC等在第一時間都有即時報導、後續追蹤。今天就來教大家一些恐怖攻擊相關英文，學會了就能輕鬆讀懂外國新聞，掌握第一手資訊','meta_title' => '如何讀懂外國新聞媒體？從美國賭城恐攻報導學英文！','meta_description' => '美國拉斯維加斯發生史上最大規模的恐攻事件！外國媒體CNN、BBC等在第一時間都有即時報導、後續追蹤。今天就來教大家一些恐怖攻擊相關英文，學會了就能輕鬆讀懂外國新聞，掌握第一手資訊','canonical_url' => 'https://tw.english.agency/terror-attacks','feature_image_alt' => '如何讀懂外國新聞媒體？從美國賭城恐攻報導學英文！','feature_image_title' => '如何讀懂外國新聞媒體？從美國賭城恐攻報導學英文！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a85af0c41ca5-CJXm6I7qamwbppXg8Tr0YVxACOUWAI-1200x630.png','author_id' => '1','category_id' => '10','created_at' => '"2018-02-15 09:28:21"'],
  ['id' => '135','title' => '學政治英文看懂中國「十九大」三大重點','slug' => 'xi-jinping','excerpt' => '中國在十月18日舉行的十九大（中國共產黨第十九次全國代表大會），習近平發布長達三小時演講，強調中國「新時代」來臨、台灣問題以及新的政治局常委誕生。快和我們一起認識十九大、習近平的演說，跟上時事潮流！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>&nbsp;</p>
<h2><strong><span style=""color: #0088a8;"">十九大相關字彙</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>中國在十月18日舉行的十九大（中國共產黨第十九次全國代表大會），快和我們一起認識十九大、習近平的演說，跟上時事潮流！</strong></span></p>
<ul>
<li><em>The 19 th National Congress of the Communist Party of China 中國共產黨第十九次全國代表大會</em></li>
<li><em>Communist Party of China（CPC）中國共產黨</em></li>
<li><em>Politburo Standing Committee政治局常務委員會<br />這是中國共產黨最高機關，一共由七位委員組成，最高領導為總書記，也就是習近平。<br />這次的十九大意在選出新的常委，這七位人選將「入常」。</em></li>
</ul>
<h2><strong><span style=""color: #0088a8;""><img title=""19大習近平演說摘錄"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a85beee99760-BBhThnnifzWQtOCcLrVGLJvAa6dtsl-1010x758.jpeg"" alt=""19大習近平演說摘錄"" width=""100%"" height=""758"" /></span></strong></h2>
<h2><strong><span style=""color: #0088a8;"">習近平演說摘錄</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>習近平發布長達三小時演講，強調中國「新時代」來臨、台灣問題以及新的政治局常委誕生。</strong></span></p>
<p>&ldquo;At present，circumstances both inside the country and abroad are going through a&nbsp;deep and complicated change.&rdquo;<br />「當前，國內外形勢正在發生深刻、複雜變化。」</p>
<p>&ldquo;Dare to change，dare to innovate. Never get rigid，never get stagnate. Unite and&nbsp;lead people of all nationalities of China，to overall construct a well-off society and to&nbsp;fight to win a great victory of socialism with Chinese characteristics in a new era.&rdquo;<br />「勇於變革、勇於創新，永不僵化、永不停滯，團結帶領全國各族人民決勝全面建成小康社會，奮力奪取新時代中國特色社會主義偉大勝利。」</p>
<p>&ldquo;It is time for us to take centre stage in the world and to make a greater contribution&nbsp;to humankind，&rdquo; he declared.<br />「是我國日益走近世界舞台中央、不斷為人類作出更大貢獻的時代」習表示。</p>
<p>&ldquo;We have the resolve，the confidence and the ability to defeat separatist attempts&nbsp;for Taiwan independence in any form. We will never allow anyone，any organization，or any political party at any time or any form to separate any part of Chinese&nbsp;territory from China.&rdquo;<br />「我們有堅定的意志、充分的信心、足夠的能力挫敗任何形式的台獨分裂圖謀。我們絕不允許任何人、任何組織、任何政黨、在任何時候、以任何形式、把任何一塊中國領土從中國分裂出去！」</p>
<h2><strong><span style=""color: #0088a8;"">文法解析</span></strong></h2>
<ul>
<li><em>At present 目前、此刻<br />Charlotte is not in the office at present. She went out with a client.<br />夏洛特目前不在辦公室，她和客戶外出了</em></li>
<li><em>at the moment 目前、此刻</em></li>
<li><em>Rigid 堅固、僵硬的；硬性的、頑固的<br />This parade is against the rigid rules of the game.<br />這項遊行意在反對比賽中的硬性規定</em></li>
<li><em>Get (人或物)變得<br />- Why weren&rsquo;t you at the party?<br /> - You know how I get when I&rsquo;m tired.<br />為什麼你沒去派對？你知道我累的時候都會這樣</em></li>
<li><em>Stagnate 停滯不前(動詞)；Stagnation 停滯(名詞)<br />Economy stagnation 經濟停滯</em></li>
<li><em>Nationality 國籍；民族</em></li>
<li><em>Dual citizenship 雙重國籍</em></li>
<li><em>Well off 富裕的<br />Well-off富裕的人、有錢人<br />My friends Andy is so well off that he spends 10 thousand dollars a day.<br />我朋友安迪很有錢，他甚至一天花一萬塊</em></li>
<li><em>Separatist 分裂主義者<br />Separate A from B 把A跟B分開</em></li>
</ul>
<h4 style=""color: #fa8072;""><strong>關於十九大，我們可以得知3大重點：</strong></h4>
<ol>
<li><em>19th National Congress of Communist Party of China is the country&rsquo;s most important&nbsp;political event where the party&rsquo;s leadership is chosen and plans are made for the&nbsp;next five years.<br />中國共產黨第十九次全國代表大會，為該國最重要的政治活動，黨內將選出將執政未來五年的領導人，同時訂定重大決策。</em></li>
<li><em>Mr. Xi has been consolidating power and is expected to remain as party chief.<br />習近平正在鞏固勢力，並有望連任黨內領導</em></li>
<li><em>As president of China，the world&rsquo;s second-largest economy，Xi Jinping rules the most&nbsp;populous country with 1.4 billion people. He roots out corruption and replaces his&nbsp;potential rivals with allies.<br />身為中國（世界第二大經濟體）的總統，習近平領導全國十四億人民，根除貪腐行為、結盟反敵。</em></li>
</ol>
</div>"','published' => '1','og_title' => '學政治英文看懂中國「十九大」三大重點','og_description' => '中國在十月18日舉行的十九大（中國共產黨第十九次全國代表大會），習近平發布長達三小時演講，強調中國「新時代」來臨、台灣問題以及新的政治局常委誕生。快和我們一起認識十九大、習近平的演說，跟上時事潮流！','meta_title' => '學政治英文看懂中國「十九大」三大重點','meta_description' => '中國在十月18日舉行的十九大（中國共產黨第十九次全國代表大會），習近平發布長達三小時演講，強調中國「新時代」來臨、台灣問題以及新的政治局常委誕生。快和我們一起認識十九大、習近平的演說，跟上時事潮流！','canonical_url' => 'https://tw.english.agency/xi-jinping','feature_image_alt' => '學政治英文看懂中國「十九大」三大重點','feature_image_title' => '學政治英文看懂中國「十九大」三大重點','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a85be630e7ff-AaHnQ2baToGv3saO6YAnyz59KTscSe-1200x630.png','author_id' => '1','category_id' => '10','created_at' => '"2018-02-15 17:01:19"'],
  ['id' => '136','title' => '還不知道該去哪裡旅遊拍美照？你絕不能錯過的觀光英文單字mix世界級秘境在這裡！','slug' => 'tourism-vocabularies','excerpt' => '"身為一個旅遊咖，出去玩第一件事就是拍照 (Take a picture)打卡 (Check in)加標籤小記號# (Hashtag)，今天就來為大家介紹世界級的打卡美照秘境，順便教你怎麼用英文說自拍棒、閃光燈等拍照必備用具，再加碼超實用的觀光英文用語，讓你在國外拍美照的同時也能暢遊旅遊勝地！"','content' => '"<p style=""font-size: 16px;"">身為一個旅遊咖，出去玩第一件事就是拍照 (Take a picture)打卡 (Check in)加標籤小記號# (Hashtag)，今天就來為大家介紹世界級的打卡美照秘境，順便教你怎麼用英文說自拍棒、閃光燈等拍照必備用具，再加碼超實用的觀光英文用語，讓你在國外拍美照的同時也能暢遊旅遊勝地！</p>
<p style=""font-size: 20px; color: #cc0000;""><strong>景點、秘境怎麼說：</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tourist attraction觀光勝地</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tourist spot旅遊景點</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Well-known知名的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Best-loved最愛的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Secret place秘境</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">best kept secret秘境</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Abandoned遺世獨立的、被遺忘(遺棄)的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Off the beaten path人跡罕至的</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>This place was off the beaten path/seldom visited by tourists. 這地方以前人跡罕至。</strong></p>
<p style=""font-size: 20px; color: #cc0000;""><strong>拍照必備：</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Selfie stick自拍棒</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Spotlight閃光燈</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Check in打卡</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Hashtag標記</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Profile photo大頭貼</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Cover photo封面照片</span><br /><br /></p>
<p style=""font-size: 20px; color: #cc0000;""><strong> V&amp;A CAFE，England 維多利亞與艾伯特咖啡館，英國</strong></p>
<p style=""font-size: 20px; color: #cc0000;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a868b995ac44-17VMKebH3GFlYE1c87YIeVIXQDUAoD-509x339.jpeg"" alt="""" width=""509"" height=""339"" /></strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">藏身於維多利亞與艾伯特博物館內的咖啡館，來頭可是不小！因為它是世界上第一座博物館咖啡館。博物館在維多利亞女王執政時期建成，遂以女王與其丈夫艾伯特親王命名。在這裡還能享用維多利亞女王愛吃的下午茶餐點！</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">V&amp;A Museum is the world\'s leading museum of art and design，boasting its permanent collection of 4.5 million objects and the world\'s first museum restaurant.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">V&amp;A博物館是世界級的藝術與設計博物館，擁有多達450萬件永久收藏品和全球第一家博物館餐廳。</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>* 博物館、美術館等等會有永久收藏品(Permanent collection)和所謂的常設展、特展(Temporary exhibition)。</strong>&nbsp;</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">The plush decorations of V&amp;A museum caf&eacute; just caught my eye right away.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">英國V&amp;A咖啡廳這種奢華裝飾，讓我一眼迷上。</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>* Catch one&rsquo;s eye吸引某人注意</strong></p>
<p style=""font-size: 20px; color: #cc0000;""><strong>Palawan，The Philippines巴拉望，菲律賓</strong></p>
<p style=""font-size: 20px; color: #cc0000;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a868e36018c1-Ci5RDkWhNJ1KdVU1tLdO3WPlQOPuuV-507x337.jpeg"" alt="""" width=""507"" height=""337"" /></strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">巴拉望是菲律賓的一個省，由許多群島組成，以群山環繞的白沙海灘和清澈海水聞名。2018年更入選了《Travel+Leisure》雜誌，成為全球13個最乾淨沙灘之一。其中像是El Nido愛妮島、Linapacan利納帕坎島都很受遊客喜愛。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Palawan is an archipelago made up of jungle-filled islands，with the bluest waters among the world and an array of coral reefs，lagoons and secret beaches.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">巴拉望省是由許多森林群島組成的，有著世界最藍的海水、多得數不完的珊瑚礁、潟湖和秘境海灘。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">The island is accessible by either boat or airplane. </span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">可以搭船或飛機前往這座小島。</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>*&nbsp;地方 + Accessible by + 交通工具：(某地)可以抵達、進入</strong>&nbsp;</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">El Nido is perfect for snorkeling，diving，kayaking and sunbathing.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">愛妮島很適合浮潛、跳水、划船和日光浴。</span></p>
<p style=""font-size: 20px; color: #cc0000;""><br /><strong>Blagaj Tekke，Bosnia and Herzegovina布拉加伊修道院，波士尼亞與赫塞哥維納</strong></p>
<p style=""font-size: 20px; color: #cc0000;"">&nbsp;<img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a868e9e883b1-FJSbYml2tHpPQT3L0YMfw2VSguc1tB-509x339.jpeg"" alt="""" width=""509"" height=""339"" /></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">如果你沒聽過Blagaj Tekke布拉加伊修道院，這就是你人生必去的景點之一。因為許多遊客和新聞都稱讚這裡「簡直」比照片更美麗。這座修道院緊鄰山崖峭壁，底下是布納河源頭，河水清澈透亮。擁有六百多年的歷史，堪稱遺世獨立的神聖景點。</span>&nbsp;</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Blagaj Tekke is a dervish house built in the 1500s.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">布拉加伊修道院建於十六世紀。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">#修道院也有人說Monastery，而Dervish是伊斯蘭教的托缽僧(藉由旋舞Sufi whirling與真主阿拉精神溝通、交流)</span>&nbsp;</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">If you visit Blagaj during the summer months，you can take a boat ride into the cave to see the source of the Buna River.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">如果你在夏季造訪布拉加伊，不妨搭船進入洞穴欣賞布納河的源流。</span></p>
<p><span style=""font-weight: 400;"">Several restaurants line the banks of the river，offering trout，Ćevapi and baklava.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">沿著河岸可見好幾家餐廳佇立著，販賣鱒魚、切巴契契以及果仁蜜餅。</span></p>"','published' => '1','og_title' => '還不知道該去哪裡旅遊拍美照？你絕不能錯過的觀光英文單字mix世界級秘境在這裡！','og_description' => '還不知道該去哪裡旅遊拍美照？你絕不能錯過的觀光英文單字mix世界級秘境在這裡！','meta_title' => '還不知道該去哪裡旅遊拍美照？你絕不能錯過的觀光英文單字mix世界級秘境在這裡！','meta_description' => '還不知道該去哪裡旅遊拍美照？你絕不能錯過的觀光英文單字mix世界級秘境在這裡！','canonical_url' => 'https://tw.english.agency/tourism-vocabularies','feature_image_alt' => '還不知道該去哪裡旅遊拍美照？你絕不能錯過的觀光英文單字mix世界級秘境在這裡！','feature_image_title' => '還不知道該去哪裡旅遊拍美照？你絕不能錯過的觀光英文單字mix世界級秘境在這裡！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a86988e4ae04-4Givx1OMf3gcirzZMN7spV6rH10k0r-718x487.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-16 08:03:27"'],
  ['id' => '137','title' => '一字領、寬褲的英文怎麼說？想去國外買衣服必學的相關單字在這裡！','slug' => 'garment-vocabularies','excerpt' => '每次出國絕對少不了的就是Shopping行程，但要如何用英文說款式、顏色和試穿呢？出國購物靠這幾句，讓你輕鬆買到喜歡的衣服！','content' => '"<p style=""font-size: 16px;"">每次出國絕對少不了的就是Shopping行程，但要如何用英文說款式、顏色和試穿呢？出國購物靠這幾句，讓你輕鬆買到喜歡的衣服！</p>
<p style=""font-size: 20px; color: #a42d00;""><strong>衣服風格 </strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Trendy 時髦、流行</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Casual 休閒</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Elegant 優雅</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Exotic 異國風</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Punk 龐克風</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Vibrant (色彩)鮮明的、有活力</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Bohemian 波西米亞風</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Artsy 藝術風</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sporty 運動風</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>花色</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Plain 素色</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Spotted 斑點的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Striped 條紋的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Checked 格子的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Cropped 短版的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Ribbed 有稜紋的、坑條</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">U shaped 圓領的</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>衣服款式</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Dress 洋裝</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Skirt裙子</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tank top 背心</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Blouse 女裝襯衫</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sweater 毛衣</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Knit sweater 編織毛衣</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Jumper / Pullover 運動毛衣</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Cardigan 開襟羊毛衫、外套</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Off-the-shoulder top 一字領上衣</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Trousers 長褲</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Shorts 短褲</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Bermuda 百慕達短褲 (也就是五分褲)</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Crop trousers 七分褲</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Leggings 緊身褲</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Wide leg pants 寬褲</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tracksuit 運動套裝</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Hoodie 連帽衫、帽T</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sweatshirt 毛衣、運動毛衣</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Slippers (室內)拖鞋</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Flip flops 人字拖</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sandals 涼鞋</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Trainers / Sneakers 運動鞋</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Cagoule 防風外套</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Anorak 連帽外套、防寒大衣</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>試穿相關</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Fitting room 試衣間</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Dressing room 試衣間、更衣室(指家中或演員可以更衣的地方)</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Changing room (Locker room) 試衣間、更衣室(指運動後可以更衣的地方)</span></p>
<p style=""font-size: 20px; color: #003377;""><strong>購物對話</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Shop assistant: Are you looking for anything in particular?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">店員：請問你在找什麼樣的衣服？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Customer: Yes，I&rsquo;m looking for a shirt. </span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">顧客：對，我想要找襯衫</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Shop assistant: What size are you?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">店員：你的尺寸多少？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Customer: I&rsquo;m a (size) small / a medium / a large / an extra large</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">顧客：我是S號 / M號 / L號 / XL號</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Shop assistant: Let&rsquo;s see&hellip;how about this one?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">店員：我來找找&hellip;這件如何？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Customer: I like it. Can I try it on?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">顧客：我很喜歡，請問能試穿嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Shop assistant: Yes，the fitting room is over there.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">店員：可以，試衣間在那邊</span></p>
<p style=""font-size: 20px; color: #003377;""><span style=""font-weight: 400;""><strong>常用對話</strong></span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">How can I help you?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">需要幫忙嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Is someone looking after you?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">請問需要幫忙嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Excuse me，could you help me?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">抱歉，請問能幫一下我嗎？</span></p>
<p style=""font-size: 16px; color: #003377;"">Medium should be fine.</p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">M號應該就可以</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Small，I think.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我想是S號</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">In medium we got white，black and blue.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">M號大小有白色、黑色和藍色的款式</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Do you have a plain shirt?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">請問你們有素色襯衫嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Do you have this T-shirt in black?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">請問這件T恤有黑色嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">No，but we got it in blue，white，purple and green. </span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">沒有，但我們有藍色、白色、紫色、綠色的</span></p>
<p style=""font-size: 16px; color: #003377;"">How do you like this?</p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">你覺得這件如何？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">I prefer the sporty style.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我比較喜歡運動風格</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">It&rsquo;s beautiful. </span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">很美</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">I like the color.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我喜歡這顏色</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">I like the T-shirt，but it doesn&rsquo;t fit me.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我喜歡這件T恤，但不合身</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">It fits me perfectly.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">非常合身</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">The dress suits me well.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">這件洋裝很適合我</span></p>
<p style=""font-size: 16px; color: #a42d00;""><em><span style=""font-weight: 400;"">＊Fit是指衣服「合身」，而Suit是指衣服「適不適合」人的風格。</span></em></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Can I try it on?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我能試穿嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Yes，you can try it on in a fitting room.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">可以，你可以去試衣間穿穿看。</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Do you have these in a smaller size?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">請問有沒有尺寸小點的？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Is it OK?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">還可以嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">How does it fit?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">請問合身嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">I&rsquo;d prefer a blue one.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我覺得藍色的會比較好</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Yes，I&rsquo;ll take it.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">很好，我決定買了</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">How much is it?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">請問多少錢？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">It&rsquo;s &pound;20. </span><em><span style=""font-weight: 400;"">(20 pounds)</span></em></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">二十元英鎊</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">It&rsquo;s $30. </span><em><span style=""font-weight: 400;"">(30 dollars)</span></em></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">三十美元</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Would you like to pay in cash or by credit card?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">請問要付現還是刷卡？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Do you take credit cards?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">你們可以刷卡嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Cash，please. </span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">付現</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">I&rsquo;ll pay by credit card.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我刷信用卡</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Thank you. Have a nice day!</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">謝謝你，祝你愉快</span></p>"','published' => '1','og_title' => '一字領、寬褲的英文怎麼說？想去國外買衣服必學的相關單字在這裡！','og_description' => '每次出國絕對少不了的就是Shopping行程，但要如何用英文說款式、顏色和試穿呢？出國購物靠這幾句，讓你輕鬆買到喜歡的衣服！','meta_title' => '一字領、寬褲的英文怎麼說？想去國外買衣服必學的相關單字在這裡！','meta_description' => '每次出國絕對少不了的就是Shopping行程，但要如何用英文說款式、顏色和試穿呢？出國購物靠這幾句，讓你輕鬆買到喜歡的衣服！','canonical_url' => 'https://tw.english.agency/garment-vocabularies','feature_image_alt' => '一字領、寬褲的英文怎麼說？想去國外買衣服必學的相關單字在這裡！','feature_image_title' => '一字領、寬褲的英文怎麼說？想去國外買衣服必學的相關單字在這裡！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8695ec69cea-fHBJiiMw8Cbikkyhv7ahe7NJefD3F9-724x482.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-16 08:28:29"'],
  ['id' => '138','title' => '"你今天嘻哈了嗎？饒舌控的你必學的hip hop英文單字在這裡！"','slug' => 'hiphop-vocabularies','excerpt' => '"中國有嘻哈、韓國嘻哈節目Show me the money討論度日漸高漲，你跟上這股嘻哈潮了嗎？你是不是也跟小編一樣忍不住就會哼出饒舌的旋律，久久不能自己呢？今天就要來教大家最潮、最常見的英文嘻哈用語，讓你輕鬆聽懂饒舌歌詞！"','content' => '"<p style=""font-size: 16px;"">中國有嘻哈、韓國嘻哈節目Show me the money討論度日漸高漲，你跟上這股嘻哈潮了嗎？你是不是也跟小編一樣忍不住就會哼出饒舌的旋律，久久不能自己呢？今天就要來教大家最潮、最常見的英文嘻哈用語，讓你輕鬆聽懂饒舌歌詞！</p>
<p style=""font-size: 20px; color: #a42d00;""><strong>DOPE</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Dope這個字原先的意思是毒品，流行文化中為</span><strong>Awesome</strong><span style=""font-weight: 400;"">的意思，也就是屌爆了、酷斃了 等等，就像是毒品會令人著迷，Dope也可以形容人事物酷炫的令人著迷</span></p>
<p style=""font-size: 16px;""><strong>Drake&rsquo;s songs are all dope AF! 德瑞克的歌全都屌爆了！</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">【延伸】</span></p>
<p style=""font-size: 16px;""><strong>AF</strong><span style=""font-weight: 400;"">這個縮寫在北美很常見，意思是As Fuck 非常，語氣比very還強烈</span> <span style=""font-weight: 400;"">Ex.</span> <strong>The weather is cold AF! 天氣冷死了！</strong></p>
<p style=""font-size: 16px; color: #003377;""><strong><span style=""font-weight: 400;"">＊Dope的相似詞有</span>Lit<span style=""font-weight: 400;"">、</span>Awesome、Cool</strong>&nbsp;</p>
<p style=""font-size: 20px; color: #a42d00;""><strong>VIBE</strong></p>
<p style=""font-size: 16px;"">Vibe意思是一種獨特的氛圍，像是Instagram常用的Hashtag<strong> #PositiveVibes </strong> <span style=""font-weight: 400;"">=正向的氛圍，在嘻哈中代表的就是一種對的感覺、對的節奏</span> <span style=""font-weight: 400;"">【延伸】</span></p>
<p style=""font-size: 16px;""><strong>Kill my vibe</strong><span style=""font-weight: 400;""> 可以說是掃興、破壞心情</span></p>
<p style=""font-size: 16px;""><strong>The rain is killing my vibe 這陣雨正在破壞我的心情</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>SWAG</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Swag形容一個人</span><strong>很有個人風格</strong><span style=""font-weight: 400;"">、</span><strong>很有型</strong><span style=""font-weight: 400;"">，延伸為</span><strong>很潮</strong><span style=""font-weight: 400;"">、</span><strong>很屌</strong><span style=""font-weight: 400;"">的意思，</span><span style=""font-weight: 400;"">這個字近年來有許多人濫用，導致現在有些人會對Swag反彈，認為只有自以為的人才會用這個字。</span></p>
<p style=""font-size: 16px;""><strong>Kevin thinks he&rsquo;s got so much swag，but actually he&rsquo;s just a dweeb.</strong></p>
<p style=""font-size: 16px;""><strong>凱文認為他是一個很有型的人，但其實他只是一個87罷了</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>CHILL</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Chill這個字有很多意思，可以形容</span><strong>寒冷</strong><span style=""font-weight: 400;"">、</span><strong>出去玩(Chill with someone)</strong><span style=""font-weight: 400;"">、</span></p>
<p style=""font-size: 16px;""><strong>放鬆冷靜 (Chill Out)</strong><span style=""font-weight: 400;"">。也可以形容一個人</span><strong>很有自己的態度</strong><span style=""font-weight: 400;"">、不在乎別人的眼光</span></p>
<p style=""font-size: 16px;""><strong>Jessica: Does your mom smoke weeds with you? </strong></p>
<p style=""font-size: 16px;""><strong>妳媽媽會跟妳一起吸大麻嗎</strong></p>
<p style=""font-size: 16px;""><strong>Alice: Yes，she&rsquo;s really chill with that kind of stuff&nbsp;</strong></p>
<p style=""font-size: 16px;""><strong>對 她對那種東西很chill(很有自己的態度)</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">個人認為，Chill這個字是美式文化的經典。那種愛吃速食、看球賽、吸大麻的生活型態都包含在Chill這個字裡面，每次帶朋友去跟白人朋友認識時，對方一定都會問Is he/she chill? 這種Super chill/ I don&rsquo;t give a shit 的生活態度是北美最推崇的之一</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Flow</strong>&nbsp;</p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">饒舌最重要的就是要維持一個Flow。Flow字典中的解釋是：</span><strong>The action of moving along in a steady，continuous stream 沿著穩定、連續的流動行動</strong><span style=""font-weight: 400;"">，嘻哈界中，個人理解為饒舌時要跟上音樂節奏、變化押韻(</span><strong>rhyme</strong><span style=""font-weight: 400;"">)、速度、聲音的能力。</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Diss</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">全寫為</span><strong>Disrespect (不尊重</strong><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><strong>Disparage(蔑視)</strong><span style=""font-weight: 400;"">，就是在饒舌中嗆人罵人。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">而</span><strong>Diss song</strong><span style=""font-weight: 400;"">是指一種貶低某人、某團體的歌曲。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">另外，Diss也有另外一種縮寫為</span><strong>Dis</strong><span style=""font-weight: 400;"">，要小心不要跟</span><strong>This</strong><span style=""font-weight: 400;"">的縮寫</span><strong><em>dis</em></strong><span style=""font-weight: 400;"">語意弄錯喔</span></p>
<p style=""font-size: 16px;""><strong>Dis movie is so amazing! Have you guys watched it? </strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>這部電影真的太棒了！你們看過了嗎？</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">這個的Dis就是This的不正式講法，並不是Disrespect的意思</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">【延伸】常用的不正式縮寫</span></p>
<p style=""font-size: 16px;""><strong>Dat = That</strong></p>
<p style=""font-size: 16px;""><strong>Dose = Those</strong></p>
<p style=""font-size: 16px;""><strong>Dem = Them</strong></p>
<p style=""font-size: 16px;""><strong>Dere= There</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Freestyle</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">中國有嘻哈裡，吳亦凡一句「你有Freestyle嗎？」是最近的網路流行金句。</span></p>
<p style=""font-size: 16px;""><strong>Freestyle就是臨場創作、即興rap一段</strong><span style=""font-weight: 400;"">。不只是嘻哈界，舞蹈界也有freestyle(一種舞風)，講究的是將各種類型的舞蹈混合，沒有舞風的限定，隨心所欲表現。籃球界中的Freestyle，就是即興表演花式動作，如換手運球(Cross-over)</span></p>
<p style=""font-size: 16px;"">【延伸】</p>
<p style=""font-size: 16px;""><strong>Battle 戰鬥</strong><span style=""font-weight: 400;"">，在嘻哈中指的是</span><strong>兩個人要較勁</strong><span style=""font-weight: 400;"">、看誰比較厲害。同理，舞蹈界中的Battle也是同樣尬舞的意思。</span></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '"你今天嘻哈了嗎？饒舌控的你必學的hip hop英文單字在這裡！"','og_description' => '"中國有嘻哈、韓國嘻哈節目Show me the money討論度日漸高漲，你跟上這股嘻哈潮了嗎？你是不是也跟小編一樣忍不住就會哼出饒舌的旋律，久久不能自己呢？今天就要來教大家最潮、最常見的英文嘻哈用語，讓你輕鬆聽懂饒舌歌詞！"','meta_title' => '"你今天嘻哈了嗎？饒舌控的你必學的hip hop英文單字在這裡！"','meta_description' => '"中國有嘻哈、韓國嘻哈節目Show me the money討論度日漸高漲，你跟上這股嘻哈潮了嗎？你是不是也跟小編一樣忍不住就會哼出饒舌的旋律，久久不能自己呢？今天就要來教大家最潮、最常見的英文嘻哈用語，讓你輕鬆聽懂饒舌歌詞！"','canonical_url' => 'https://tw.english.agency/hiphop-vocabularies','feature_image_alt' => '"你今天嘻哈了嗎？饒舌控的你必學的hip hop英文單字在這裡！"','feature_image_title' => '"你今天嘻哈了嗎？饒舌控的你必學的hip hop英文單字在這裡！"','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a869e834fd9f-jUpT1EkKOMozzCzS0uasXapdw2MQnx-594x396.jpeg','author_id' => '1','category_id' => '','created_at' => '"2018-02-16 09:01:36"'],
  ['id' => '139','title' => '寒流、發熱衣的英文怎麼說？冷冷冬天常用的英文單字在這裡！','slug' => 'winter-vocabularies','excerpt' => '最近天氣越來越冷，又濕又冷的真是讓人受不了，尤其是從小住南部的小編完全承受不住這種溫度，每天都想躲在被窩吹著暖氣不出門，不過暖氣、發熱衣或是暖暖包的英文該怎麼說呢？今天就來教教大家冬天常用的英文單字！','content' => '"<p style=""font-size: 16px;"">最近天氣越來越冷，又濕又冷的真是讓人受不了，尤其是從小住南部的小編完全承受不住這種溫度，每天都想躲在被窩吹著暖氣不出門，不過暖氣、發熱衣或是暖暖包的英文該怎麼說呢？今天就來教教大家冬天常用的英文單字！</p>
<p style=""font-size: 20px; color: #003377;""><strong>Daily supplies日常用品</strong></p>
<p style=""font-size: 16px;""><strong>Hand warmer </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">暖暖包</span></p>
<p style=""font-size: 16px;""><strong>Heater </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">暖氣</span></p>
<p style=""font-size: 16px;""><strong>Fireplace </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">壁爐</span></p>
<p style=""font-size: 16px;""><strong>Sled </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">雪橇</span></p>
<p style=""font-size: 16px;""><strong>Candle </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">蠟燭</span></p>
<p style=""font-size: 16px;""><strong>Electric blanket </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">電熱毯</span></p>
<p style=""font-size: 16px;""><strong>Thermos bottle </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">保溫瓶</span></p>
<p style=""font-size: 20px; color: #003377;""><strong>Winter Clothing 冬天衣物</strong></p>
<p style=""font-size: 16px;""><strong>Thermal clothing </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">發熱衣</span></p>
<p style=""font-size: 16px;""><strong>Coat </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">外套</span></p>
<p style=""font-size: 16px;""><strong>Hoodies </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">帽Ｔ</span></p>
<p style=""font-size: 16px;""><strong>Crewneck </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">大學Ｔ</span></p>
<p style=""font-size: 16px;""><strong>Scarf </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">圍巾</span></p>
<p style=""font-size: 16px;""><strong>Shawl </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">披肩</span></p>
<p style=""font-size: 16px;""><strong>Sweater </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">毛衣</span></p>
<p style=""font-size: 16px;""><strong>Boots </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">靴子</span></p>
<p style=""font-size: 16px;""><strong>Down coat </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">羽絨衣</span></p>
<p style=""font-size: 16px;""><strong>Gloves </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">手套</span></p>
<p style=""font-size: 16px;""><strong>Earmuffs </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">耳罩</span></p>
<p style=""font-size: 20px; color: #003377;""><strong>Winter weather 冬天氣候</strong></p>
<p style=""font-size: 16px;""><strong>Chilly </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">微寒的</span></p>
<p style=""font-size: 16px;""><strong>Frosty </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">嚴寒的</span></p>
<p style=""font-size: 16px;""><strong>Frostbite </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">凍傷</span></p>
<p style=""font-size: 16px;""><strong>Blizzard </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">暴風雪</span></p>
<p style=""font-size: 16px;""><strong>Snowsorm</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">暴風雪</span></p>
<p style=""font-size: 16px;""><strong>Cold current </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">寒流</span></p>
<p style=""font-size: 16px;""><strong>Cold wave </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">寒流</span></p>
<p style=""font-size: 20px; color: #003377;""><strong>Seasonal Fun 冬天活動</strong></p>
<p style=""font-size: 16px;""><strong>Snowball fight </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">雪球戰</span></p>
<p style=""font-size: 16px;""><strong>Snowman </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">雪人</span></p>
<p style=""font-size: 16px;""><strong>Hot spring </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">溫泉</span></p>
<p style=""font-size: 16px;""><strong>Hot pot </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">火鍋</span></p>
<p style=""font-size: 16px;""><strong>Hockey </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">冰上曲棍球</span></p>
<p>&lt;pstyle=""font-size: 16px;""&gt;<strong>Skiing </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">滑雪</span></p>
<p style=""font-size: 16px;""><strong>Snowboarding </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">單板滑雪</span></p>
<p style=""font-size: 16px;""><strong>Ice skating </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">溜冰</span><br /><br /></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Winter Idioms 冬天俚語</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>＊</strong><strong>Put something on ice</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">意思是推遲某事情，先冷凍它、不動作。比如說 </span><strong>Put a project on ice</strong><strong>，</strong><span style=""font-weight: 400;"">先暫時不繼續做這份研究報告的意思。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Let&rsquo;s put this project on ice until we have more resources to work on it.</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">在我們有更多資源之前，先把這個project放在一邊吧！</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>＊Cold feet</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">形容一個人原本很有信心，但突然臨時感到膽怯而臨陣退縮。很常被用在結婚或婚禮的時候，如果新郎或新娘在婚禮之前有cold feet，也就表示他們不想結婚了</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>The groom got cold feet just before the wedding and he ran off.</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">新郎在婚禮開始之前退縮逃走了。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>＊Be on thin ice</strong>&nbsp;</p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">On thin ice 的意思是如履薄冰，形容某人身處險境，要特別謹慎小心，不一定是指肉體或涉及生命危險，也可以用來形容其他心理層面的狀況，比如說快被教授當掉。也可以用</span><strong>Skate on thin ice</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Jackson did not turn in his assignments and missed the midterm. He is on thin ice of this class and very close to failing. </strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">Jackson 沒有交作業也沒有參加期中考。他在這堂課的學業情況非常危險，幾乎要被當了！</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>＊Break the ice</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">打破僵局、緩和氣氛的意思。剛認識的朋友常常會玩一些</span><strong>Ice breaker games</strong><span style=""font-weight: 400;"">(破冰遊戲)，就是指團康小遊戲這種東西。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>A friendly smile does a lot to break the ice.</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">一個友善的微笑對打破不熟識的僵局有很大的作用</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>＊Have a snowball&rsquo;s chance in hell</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">希望渺茫、毫無可能的意思，通常用在否定句。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Mary doesn&rsquo;t have a snowball&rsquo;s chance in hell of persuading her boyfriend to take her to Cuba for a vacation.</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">Mary毫無可能說服她男朋友帶她去古巴度假。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>＊Give somebody the cold shoulder</strong>&nbsp;</p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">故意冷落某人，或是冷淡、不友善的對待某人的意思，也就是指</span><strong>Cold treatment</strong><span style=""font-weight: 400;""> 冷處理。</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>After Alice divorced her rich husband，all their mutual friends gave her the cold shoulder.</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">在Alice跟她的有錢老公離婚後，他們所有的共同朋友都開始冷落她</span></p>
<p>&nbsp;</p>"','published' => '1','og_title' => '寒流、發熱衣的英文怎麼說？冷冷冬天常用的英文單字在這裡！','og_description' => '最近天氣越來越冷，又濕又冷的真是讓人受不了，尤其是從小住南部的小編完全承受不住這種溫度，每天都想躲在被窩吹著暖氣不出門，不過暖氣、發熱衣或是暖暖包的英文該怎麼說呢？今天就來教教大家冬天常用的英文單字！','meta_title' => '寒流、發熱衣的英文怎麼說？冷冷冬天常用的英文單字在這裡！','meta_description' => '最近天氣越來越冷，又濕又冷的真是讓人受不了，尤其是從小住南部的小編完全承受不住這種溫度，每天都想躲在被窩吹著暖氣不出門，不過暖氣、發熱衣或是暖暖包的英文該怎麼說呢？今天就來教教大家冬天常用的英文單字！','canonical_url' => 'https://tw.english.agency/winter-vocabularies','feature_image_alt' => '寒流、發熱衣的英文怎麼說？冷冷冬天常用的英文單字在這裡！','feature_image_title' => '寒流、發熱衣的英文怎麼說？冷冷冬天常用的英文單字在這裡！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a86a39a6eb77-J3cp98a0H4AEg36eU5Ryd2LAaUOoAD-724x483.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-16 09:20:17"'],
  ['id' => '140','title' => '天氣好冷英文怎麼說？教你如何聊天氣交朋友！','slug' => 'weather','excerpt' => '"出國打工留學不會用英文聊政治、文學沒關係，但你一定要知道如何說天氣！千萬別再用I’m hot或I’m cold開口讓外國人傻眼啦！學好這幾句，簡單和外國朋友打開話匣子！"','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>出國打工留學不會用英文聊政治、文學沒關係，但你一定要知道如何說天氣！<br />千萬別再用I&rsquo;m hot或I&rsquo;m cold開口讓外國人傻眼啦！學好這幾句，簡單和外國朋友打開話匣子！</p>
<h2><strong><span style=""color: #0088a8;"">天氣修飾詞</span></strong></h2>
<p><span style=""color: #ee7700;""><strong> Remain修飾天氣，Temperature修飾溫度，Intensify修飾風雨</strong></span></p>
<ul>
<li><em>Taiwan&rsquo;s north coast will remain cloudy and rainy in the next three days.<br />台灣北海岸天氣將連三天多雲有雨</em></li>
<li><em>Temperatures are expected to drop to below 20 degrees Celsius over the weekend.<br />周末溫度預計下降到攝氏20度以下</em></li>
<li><em>Warm ocean water intensifies the typhoon.<br />溫暖海水會增強颱風威力</em></li>
<li><em>The wind and rain are expected to intensify through midnight.<br />預計午夜時風雨將增強 </em></li>
</ul>
<h2><strong><span style=""color: #0088a8;"">如何用英文聊天氣？</span></strong></h2>
<ul>
<li><em>What\'s the weather going to be like?<br />天氣將會如何呢？</em></li>
<li><em>The weather is going to be colder this year.<br />今年天氣將會更冷</em></li>
<li><em>The forecast said it would be cold this weekend.<br />天氣預報說這周末會很冷</em></li>
<li><em>It\'s hot outside.<br />外面天氣很熱</em></li>
<li><em>It&rsquo;s baking hot.<br />外面熱得跟烤箱一樣</em></li>
<li><em>I can\'t bear this stifling heat.<br />我簡直無法忍受這種悶熱</em></li>
<li><em>It&rsquo;s hazy outside.<br />外面熱濛濛的</em></li>
<li><em>It&rsquo;s freezing today.<br />今天好冷呀</em></li>
<li><em>It&rsquo;s a bit chilly.<br />今天有點涼</em></li>
<li><em>I can see my breath.<br />冷到可以看到自己呼吸了</em></li>
<li><em>I\'m freezing to death.<br />我快冷死了</em></li>
<li><em>If it&rsquo;s whiteout，stay home. You can&rsquo;t see anything.<br />如果是暴風雪的天空，待在家。因為根本什麼都看不見</em></li>
<li><em>Be / Feel under the weather (身體)感到不舒服</em></li>
<li><em>Feeling under the weather today，Hans called in sick today.<br />由於身體不適，漢斯今天請病假</em></li>
</ul>
<h2><strong><span style=""color: #0088a8;""><img title=""下雨潮濕英文怎麼說"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a86c1aa6ec0b-QFaQXEyCIWQxzJwfptGx7kqqsGHvWd-1920x1137.jpeg"" alt=""下雨潮濕英文怎麼說"" width=""100%"" height=""500px"" /></span></strong></h2>
<h2><strong><span style=""color: #0088a8;"">看懂氣象新聞必知：</span></strong></h2>
<ul>
<li><em>Central Weather Bureau中央氣象局</em></li>
<li><em>Forecast 預報</em></li>
<li><em>Temperature溫度</em></li>
<li><em>Thermometer 溫度計</em></li>
<li><em>Fahrenheit 費氏</em></li>
<li><em>Celsius 攝氏</em></li>
<li><em>Precipitation 雨量</em></li>
<li><em>Wind 風力</em></li>
<li><em>UV index 紫外線</em></li>
<li><em>Humidity 濕度</em></li>
<li><em>Wind radii 暴風半徑</em></li>
<li><em>Moisture 水氣、水分</em></li>
<li><em>Intensify 增強</em></li>
<li><em>Drought 乾旱</em></li>
<li><em>Trade wind 信風</em></li>
<li><em>Northeasterly winds東北季風</em></li>
<li><em>Southwesterly wind 西南季風</em></li>
<li><em>Mountainous areas 山區</em></li>
<li><em>Daytime日間</em></li>
<li><em>Overnight 夜間</em></li>
<li><em>La Ni&ntilde;o聖嬰現象</em></li>
<li><em><em>La Ni&ntilde;a 反聖嬰現象</em></em></li>
</ul>
<h2><strong><span style=""color: #0088a8;"">各種類型降雨：</span></strong></h2>
<ul>
<li><em>Drizzle 毛毛雨</em></li>
<li><em>Shower 降雨</em></li>
<li><em>Heavy rain 大雨</em></li>
<li><em>Downpour 暴雨、驟雨</em></li>
<li><em>Outbreaks of rain 暴雨</em></li>
<li><em>Persistent rain持續降雨</em></li>
<li><em>Hail 冰雹</em></li>
<li><em>Blizzard 暴風雪</em></li>
<li><em>Whiteout 乳白天空（也就是發生嚴重暴風雪時，會影響路上能見度） </em></li>
</ul>
<h2><strong><span style=""color: #0088a8;"">天氣形容詞：</span></strong></h2>
<ul>
<li><em>Brisk 天氣好、朝氣蓬勃的</em></li>
<li><em>Humid 潮濕的</em></li>
<li><em>Damp 潮濕的</em></li>
<li><em>Overcast 烏雲密布的</em></li>
<li><em>Muggy 悶熱潮濕的</em></li>
<li><em>Hazy 霧濛濛的（尤其指夏天）</em></li>
<li><em>Foggy 霧濛濛的、多霧的</em></li>
<li><em>Misty 霧氣濛濛的 </em></li>
</ul>
<h4 style=""color: #fa8072;""><strong>同樣都是濕，到底差在哪？</strong></h4>
<ul>
<ul>
<li><em>Wet濕透（超越潮濕的程度）<br />A downpour got me all wet.<br />大雨讓我全身濕透了</em></li>
<li><em>Moist濕潤（正向意思，可用在烹飪、環境<br />In order to have a delicious cake，you need to keep it moist.<br />蛋糕好吃的秘訣在於保持濕潤度</em></li>
<li><em>Damp潮濕（負面意思，指環境、衣服很濕；用在天氣是指潮濕的冷）<br />A damp house 充滿濕氣房子</em></li>
<li><em>Humid潮濕（指空氣中的潮濕，用在天氣濕熱時）<br />I hate rainy days because the humidity makes my hair curl.<br />我討厭雨天，因為頭髮會捲起來 </em></li>
</ul>
</ul>
</div>"','published' => '1','og_title' => '天氣好冷英文怎麼說？教你如何聊天氣交朋友！','og_description' => '"出國打工留學不會用英文聊政治、文學沒關係，但你一定要知道如何說天氣！千萬別再用I’m hot或I’m cold開口讓外國人傻眼啦！學好這幾句，簡單和外國朋友打開話匣子！"','meta_title' => '天氣好冷英文怎麼說？教你如何聊天氣交朋友！','meta_description' => '"出國打工留學不會用英文聊政治、文學沒關係，但你一定要知道如何說天氣！ 千萬別再用I’m hot或I’m cold開口讓外國人傻眼啦！學好這幾句，簡單和外國朋友打開話匣子！"','canonical_url' => 'https://tw.english.agency/weather','feature_image_alt' => '天氣好冷英文怎麼說？教你用聊天氣交朋友！','feature_image_title' => '天氣好冷英文怎麼說？教你用聊天氣交朋友！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a86c25265e04-gUbaXwYLRkwSCS8FiQP3vCT4b5ZOw4-1200x630.png','author_id' => '1','category_id' => '10','created_at' => '"2018-02-16 11:37:03"'],
  ['id' => '141','title' => '農曆新年來臨，如何跟外國人介紹農曆新年？','slug' => 'chinese-new-year','excerpt' => '"每年最重要的時刻來臨了！農曆新年來臨，又是大家最期待的春節假期（Spring Festival），但你有沒有發現身邊越來越多外國人開始向你問起新年呢？要如何介紹他們農曆新年、來迪化街辦年貨，就靠這幾句！"','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; color: #666666;"">
<p>&nbsp;</p>
<h2><strong><span style=""color: #0088a8;"">農曆新年相關字彙</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>每年最重要的時刻來臨了！你知道怎麼使用英文介紹我們的文化嗎？</strong></span></p>
<ul>
<li><em>Red envelope紅包</em></li>
<li><em>Red couplets春聯</em></li>
<li><em>Firecracker鞭炮</em></li>
<li><em>New Year&lsquo;s Eve除夕</em></li>
<li><em>Reunion dinner年夜飯、團圓飯</em></li>
<li><em>Reunite團聚</em></li>
<li><em>Stay up late on New Year&rsquo;s Eve守歲</em></li>
<li><em>The Chinese zodiac生肖</em></li>
<li><em>Dragon and Lion dances舞龍舞獅</em></li>
<li><em>New Year\'s Market年貨大街</em></li>
<li><em>Going shopping for the Lunar New Year辦年貨</em></li>
<li><em>Nian年獸 </em></li>
</ul>
<h2><strong><span style=""color: #0088a8;"">用英文介紹農曆新年</span></strong></h2>
<p><span style=""color: #ee7700;""><strong>你有沒有發現身邊越來越多外國人開始向你問起新年呢？要如何介紹他們農曆新年、來迪化街辦年貨，就靠這幾句！</strong></span></p>
<ul>
<li><em>Chinese New Year is the most important annual event for Taiwanese&nbsp;people.<br />對台灣人來說，農曆新年是每年最重要的節日</em></li>
<li><em>2018 is the year of the Dog，one of the twelve Chinese zodiac signs.<br />2018年是狗年，狗是十二生肖中的其中一個生肖</em></li>
<li><em>Chinese New Year is a time for people to reunite with their&nbsp;relatives.<br />農曆新年也是人們與親戚團聚的時刻 </em></li>
</ul>
<h4 style=""color: #fa8072;""><strong><img title=""如何用英文介紹農曆新年會做什麼？"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a86cc2cb563b-W4J3fisWEoHcp6mbHP2igAkkwPGewm-1116x626.png"" alt=""如何用英文介紹農曆新年會做什麼？"" width=""100%"" height=""400"" /></strong></h4>
<h4 style=""color: #fa8072;""><strong>農曆新年會做什麼？</strong></h4>
<ul>
<li><em><em>Many people clean their houses to welcome the Chinese New Year.<br />許多人會清掃家園以迎接新年到來。</em></em></li>
<li><em>People paste Spring Festival couplets vertically on both sides of the<br />front door.<br />人們會將對聯以垂直式分別貼在門的兩側</em></li>
<li><em>Reunion dinner，watching Chinese New Year TV gala and staying up all<br />night are also traditional customs on New Year&rsquo;s Eve.<br />年夜飯、看新年節目演出、守歲等等都是新年除夕的傳統習俗</em></li>
<li><em><em>Taiwan&rsquo;s Dihua Street is known for its stores selling Lunar New Year<br />foods. You can buy snacks，dried meats and street food there. It&rsquo;s worth a visit.<br />台灣迪化街以販賣農曆新年食物聞名。你可以在那裏買到點心、肉乾和街頭小吃，值得一去</em></em>
<ul>
<li><em> Be known for：以&hellip;(特色)聞名 (後面加某事物的特色)<br />Apple is known for its high-end smartphones and laptops.<br />蘋果以販賣高檔智慧型手機和筆電聞名<br />The company is known for selling Frisbees.<br />那家公司以販賣飛盤聞名</em></li>
<li><em> Be known as：以&hellip;(身分)聞名<br />Thom Yorke is known as the lead singer of a British band<br />Radiohead.<br />湯姆約克以擔任英國樂團電台司令主唱而聞名</em></li>
<li><em>Be known to be/do something：(以做某事)為人所知</em></li>
<li><em> Be known to +地方：為&hellip;.所知<br />The suspect in the terror attack known to Belgian authorities may have been killed.<br />比利時一起恐攻嫌疑犯可能已遭殺害<br />People set off firecrackers to scare away Nian.<br />人們放鞭炮以嚇跑年獸</em></li>
<li><em>Set off：引爆(炸彈)、觸發(警報)；引發；啟程、出發<br />The protesters set off the flares，resulting in traffic disruptions.<br />抗議人士引爆信號彈，導致道路交通中斷<br />We will set off for Jenny&rsquo;s house tomorrow.<br />明天我們將出發去珍妮家<br />Parents give their kids and elderly people red envelopes.<br />家長會給小孩和年長者紅包</em></li>
</ul>
</li>
</ul>
<h4 style=""color: #fa8072;""><strong>新年快樂怎麼說？</strong></h4>
<ul>
<li><em>Happy Chinese New Year<br />新年快樂</em></li>
<li><em>Wishing you happiness and prosperity<br />恭喜發財、快樂如意</em></li>
<li><em>May all your dreams come true<br />祝您心想事成</em></li>
<li><em>Wishing you good health<br />身體健康</em></li>
<li><em>Wishing you peace all year around<br />祝您年年平安</em></li>
<li><em>All year around：全年、整年 </em></li>
</ul>
</div>"','published' => '1','og_title' => '農曆新年來臨，如何跟外國人介紹農曆新年？','og_description' => '"每年最重要的時刻來臨了！農曆新年來臨，又是大家最期待的春節假期（Spring Festival），但你有沒有發現身邊越來越多外國人開始向你問起新年呢？要如何介紹他們農曆新年、來迪化街辦年貨，就靠這幾句！"','meta_title' => '農曆新年來臨，如何跟外國人介紹農曆新年？','meta_description' => '"每年最重要的時刻來臨了！農曆新年來臨，又是大家最期待的春節假期（Spring Festival），但你有沒有發現身邊越來越多外國人開始向你問起新年呢？要如何介紹他們農曆新年、來迪化街辦年貨，就靠這幾句！"','canonical_url' => 'https://tw.english.agency/chinese-new-year','feature_image_alt' => '農曆新年來臨，如何跟外國人介紹農曆新年？','feature_image_title' => '農曆新年來臨，如何跟外國人介紹農曆新年？','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a86cc1375e40-WcueG7CfdjvwD4Xrpxo9vPVlbwypRQ-1200x630.png','author_id' => '1','category_id' => '10','created_at' => '"2018-02-16 12:12:25"'],
  ['id' => '142','title' => '2017最新！托福口說內容和題型大解析！','slug' => '2017-ets-topics','excerpt' => '托福口說該如何準備？平常練習時該注意些什麼？了解官方指南3大評分標準，就是拿分的最快準備方法！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">&nbsp;
<h2><strong>口說有六題，主要有如下內容:</strong></h2>
<h3 style=""color: #ff6666;""><strong>Independent Topics (獨立</strong><strong>題型)</strong></h3>
<ul>
<li>
<h4 style=""color: #ff751a;"">Independent task 1:</h4>
要求考生就某一常規話題用英語做45秒鐘的陳述。考生對於這道題的準備應主要集中在people、place、object、event （人、地、物、事）這幾個大方面。<br />在官方模擬考中可充分練習幾道互相涵蓋內容的題目。<br />比如: &ldquo;Describe the place you live in&rdquo; (描述你居住的地方)與&ldquo;Which city do you travel to most&rdquo; (你最常旅行的城市)、就可以相互借用大部分內容。</li>
</ul>
<img title=""ETS托福口說獨立題型"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8709d66b07a-l3xWkPa2Oak7Ao9pcX0XtAbbnIDe5c-5001x2626.png"" alt=""ETS托福口說獨立題型"" width=""100%"" height=""400"" /><br />
<ul>
<li>
<h4 style=""color: #ff751a;"">Independent task 2:</h4>
要求考生在提供的兩個選擇中選擇自己喜歡的一個，並用details (客觀資訊)和examples(範例)支持自己的觀點。<br />如&ldquo;If you could choose to live in the city or live in the country areas，which lifestyle would you prefer and why&rdquo; ( 選擇你最想居住的地方，描述你喜好的生活方式和理由"" 因此有兩部分要描述，答出地方，道出該地的生活特色，以及你為何喜歡。<br />此種題型，考生需要做的是迅速確定自己的立場。例如: 選擇紐約，因為帶來大量資訊和便利性，且我從事時尚產業，走在尖端是必須的...&rdquo;這樣的方向去答。有時，題幹會要你對照出不同的觀點，例如:紐約的繁華背後是步調快，壓力大...因此我選擇...&rdquo;。</li>
</ul>
<img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a870a407af99-pwAwH2nN2VgYZPwq0OokV0Mb2PAae8-762x400.png"" alt="""" width=""100%"" height=""400"" />
<ul>
<li>
<h4 style=""color: #ff751a;"">第3～6題:</h4>
主要考查考生對於題目的認知程度，並依據reading和listening部分做好的notes進行有條理的、清晰的複述，或者加上自己的觀點。對於考生來說，一定要充分理解題目，熟悉考題要求，充分理解它們分別要求自己做什麼事情。<br />首先，ETS 明確3、4題都不允許有任何的personal view，因此不允許出現&ldquo;I think，I believe， I presume，I consider.&rdquo;或&ldquo;As far as I&rsquo;m concerned.&rdquo;、&ldquo;In my opinion.&rdquo;可使用 It is said&hellip; People say...。&rdquo;之類的表達方式。<br />而第5、6題則可根據具體要求加入考生的個人觀點。</li>
</ul>
<p>&nbsp;</p>
<h4 style=""color: #ff751a;""><strong>""答題時間的掌握是平常就要練習的""</strong></h4>
<p>&nbsp;</p>
<h4 style=""color: #ff6666;""><img title=""ETS Official Guide官方指南3大重點"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a870e81c9871-zBmCo8cm9F43tcaN439CGjYv1xjbJ3-762x400.png"" alt=""ETS Official Guide官方指南3大重點"" width=""100%"" height=""400"" /></h4>
<h4 style=""color: #ff6666;"">評分標準是您的練習指標，Official Guide官方指南上列出3點作為評分準則：</h4>
<ol>
<li>口說傳達度的高低: 清晰的發音，恰當的速度和音量。</li>
<li>使用正確字彙和語法，用正規句子回答。</li>
<li>關鍵句是否切中題幹，具備因果關係的程度，給予精準的答案，不要扯遠了。</li>
</ol>
<p><span style=""font-weight: 400;""> &nbsp;&nbsp;一二題準備15秒回答45秒; 三 四題準備為每題30秒，回答時間各自是60秒。最後兩道題準備20秒，回答60秒。</span><span style=""font-weight: 400;"">第1、2中的題目屬於日常話題，準備時間15秒。這兩道題本身難度比較小，所以特別需要注意的就是時間的分配問題，在這45秒鐘裡，一般用5～6秒完成topic sentence (關鍵句，通常講在回答內容的前頭)， 接下來的40秒應以每一個detail (資訊)或者example (舉例)不超過3～4句話的規律完成該話題，也就是說大致20秒完成一個detail或example的表述。如果考生能在考前進行充分的準備，鍛煉自己的口語思維，積累常用的details或examples，考試時就會從容許多。</span></p>
<p><span style=""font-weight: 400;"">　</span></p>
<hr />
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">上述資訊供做為平常練習的指引。又2013年後口說介紹的部分會出現英國腔、澳洲腔，因此用功準備之餘，平常娛樂可找一些喜歡的電影 、電視劇當背景聽力 (播放不要看字幕，試著寫出關鍵句子)，讓自己習慣不同腔調也練習關鍵句的表達。</span></p>
</div>"','published' => '1','og_title' => '2017最新！托福口說內容和題型大解析！','og_description' => '托福口說該如何準備？平常練習時該注意些什麼？了解官方指南3大評分標準，就是拿分的最快準備方法！','meta_title' => '2017最新！托福口說內容和題型大解析！','meta_description' => '托福口說該如何準備？平常練習時該注意些什麼？了解官方指南3大評分標準，就是拿分的最快準備方法！','canonical_url' => 'https://tw.english.agency/2017-ets-topics','feature_image_alt' => '2017最新！托福口說內容和題型大解析！','feature_image_title' => '2017最新！托福口說內容和題型大解析！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8711f1811e0-ywcjPtZstphEHl4dU315fJtixP79Y9-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-16 13:06:00"'],
  ['id' => '143','title' => '要怎麼走出失戀傷痛？五大療癒系電影經典對白幫你療傷！','slug' => 'five-movies-conversation','excerpt' => '失戀的時候除了大哭大鬧之外，看電影也是有效的療傷方式，優美的電影台詞不僅能撫慰人心，也能從中學到實用英文，以下獨家整理5部電影的療傷系對白，讓你邊學英文邊走出失戀的傷痛！','content' => '"<p style=""font-size: 16px;"">失戀的時候除了大哭大鬧之外，看電影也是有效的療傷方式，優美的電影台詞不僅能撫慰人心，也能從中學到實用英文，以下獨家整理5部電影的療傷系對白，讓你邊學英文邊走出失戀的傷痛！</p>
<p style=""font-size: 20px; color: #a42d00;""><strong>The Notebook (2004)</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a877a4bb2fa7-EiBQedFpu658PnhkIPcZFLnrR94w67-780x512.jpeg"" alt="""" width=""780"" height=""512"" /></strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>I knew you feel lost right now，but don&rsquo;t worry.</strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>&ldquo;Nothing is ever lost，nor can be lost; the body sluggish，aged，cold，</strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>the embers left from earlier fires shall duly flame again.&rdquo; － Walt Whitman</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我了解你此刻感到很迷失，但不要擔心，</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">「沒有什麼是曾經失去或能被錯過的。身體遲緩呆滯、因年老而畏寒，如餘燼般地飛離了餘火，必將再次伺機燃燒」</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sluggish</span><span style=""font-weight: 400;""> (adj.) 遲鈍的、呆滯的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Embers</span><span style=""font-weight: 400;""> (n.) 餘火</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Duly </span><span style=""font-weight: 400;"">(adv.)適當地、按時地</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">*星星之火，可以燎原 的英文可以說: A single spark can start a prairie fire.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Spark </span><span style=""font-weight: 400;"">(n.) 火花</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Prairie</span><span style=""font-weight: 400;""> (n.) 大草原</span></p>
<p style=""font-size: 16px;""><strong>Neither&hellip; nor&hellip; 既不&hellip;也不&hellip;</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Jack neither smokes nor drinks.</span><span style=""font-weight: 400;""> &nbsp;Jack既不抽煙也不喝酒</span></p>
<p style=""font-size: 16px;""><strong>Either&hellip;or&hellip; 不是..就是..</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Leo will come to your place either tonight or tomorrow. </span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">里歐今晚或明天會去你家</span></p>
<p style=""font-size: 16px;""><strong>Both&hellip;and&hellip; &nbsp;是...也是&hellip;</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Both my mom and my dad are teachers. </span><span style=""font-weight: 400;"">我的爸爸和媽媽都是老師</span>&nbsp;</p>
<p style=""font-size: 20px; color: #a42d00;""><strong>A room with a View (1985)</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a877b324bed3-ixbOiDrnxZy48RaLWp2btQYRC08dEE-674x616.jpeg"" alt="""" width=""674"" height=""616"" /></strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>When you feel hurt and your tears are gonna drop，</strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>Please look up and have a look at the sky once belongs to us.</strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>If the sky is still vast，clouds are still clear，you shall not cry.</strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>Because my leave doesn&rsquo;t take away the world that belongs to you.</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">當你的心很痛、眼淚快要流下來時，請抬頭看看這片曾經屬於我們的天空。</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">如果天空依舊是那樣的廣闊、雲依舊是那樣的清晰，那你就不應該哭。</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">因為我的離去，並沒有帶走你的世界。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Vast </span><span style=""font-weight: 400;"">(adj.) 廣闊的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Take away</span><span style=""font-weight: 400;""> 帶走、拿走</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">-away 片語整理</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">be away</span><span style=""font-weight: 400;""> 不在</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">run away</span><span style=""font-weight: 400;""> 逃跑</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">put away </span><span style=""font-weight: 400;"">放好、歸位</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">keep away from</span><span style=""font-weight: 400;"">&hellip; 避免靠近</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">pass away</span><span style=""font-weight: 400;""> 過世</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">throw away</span><span style=""font-weight: 400;""> 丟掉</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">break away</span><span style=""font-weight: 400;""> 逃脫</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">get away</span><span style=""font-weight: 400;""> 走開、讓開</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">come away </span><span style=""font-weight: 400;"">離開</span></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>How to be single (2016)</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a877adbb36ad-PWppND6IxIA8qqc6jAkQBrRCWzuRZK-620x510.jpeg"" alt="""" width=""620"" height=""510"" /></strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>&ldquo;The thing about being single is，you should cherish it. Because，in a week，or a lifetime，of being alone，you may only get one moment. One moment，when you\'re not tied up in a relationship with anyone. A parent，a pet，a sibling，a friend. One moment，when you stand on your own. Really，truly single. And then... It\'s gone.&rdquo;</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">你該珍惜單身，因為單身一週或一輩子， 你都會獲得無與倫比的時刻。你不會被誰牽著鼻子走，你可以獨立、真正的一個人生活。接著一轉身，愛情就來了。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Cherish </span><span style=""font-weight: 400;"">(v.) 珍愛、愛護</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Lifetime</span><span style=""font-weight: 400;""> (n.) 一生、終生</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Tied up </span><span style=""font-weight: 400;"">脫不了身的</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Sibling </span><span style=""font-weight: 400;"">(n.) 兄弟姊妹</span></p>
<p style=""font-size: 16px;""><strong>Up片語整理</strong></p>
<p style=""font-size: 16px;""><strong>Take up </strong>開始從事...</p>
<p style=""font-size: 16px;""><strong>Ex. </strong><strong>Malinda took up journalism after she left school.</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Malinda畢業後開始從事記者工作</span></p>
<p style=""font-size: 16px;""><strong>Shut up </strong><span style=""font-weight: 400;"">住口</span><span style=""font-weight: 400;""><br /></span><strong>Pick up </strong><span style=""font-weight: 400;"">撿起、收拾、接送</span></p>
<p style=""font-size: 16px;""><strong>Ex. </strong><strong>I&rsquo;ll pick you up at the airport around 7.</strong><strong><br /></strong><span style=""font-weight: 400;"">我大概七點時去機場接你。</span></p>
<p style=""font-size: 16px;""><strong><br />Show up </strong><span style=""font-weight: 400;"">出現、到場</span><span style=""font-weight: 400;""><br /></span><strong>Make up </strong><span style=""font-weight: 400;"">組成、編造、補償、和解、化妝</span><span style=""font-weight: 400;""><br /></span><strong>Come up with </strong><span style=""font-weight: 400;"">想出來</span></p>
<p style=""font-size: 16px;""><strong>Ex. </strong><strong>Sean came up with a good idea for the product promotion.</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sean 想出了一個推廣產品的好主意。</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;""><strong>Up to</strong> </span><span style=""font-weight: 400;"">取決於</span></p>
<p style=""font-size: 16px;""><strong>Ex.</strong><strong> Cathy: Where should we go tonight? Kelly: It&rsquo;s up to you!</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Cathy:我們今晚要去哪裡? Kelly:看你啊！</span></p>
<p style=""font-size: 16px;""><strong>What are you up to? </strong><span style=""font-weight: 400;"">= 你正在做什麼/你最近在忙什麼啊?</span>&nbsp;</p>
<p style=""font-size: 20px; color: #a42d00;""><strong>Her (2013)</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a877af6cf1b6-uy1cs2xYxPjYWtmjTptiZMKbi914HP-680x452.jpeg"" alt="""" width=""680"" height=""452"" /></strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>&ldquo;The past is just a story we tell ourselves.&rdquo;</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">過去，只是我們告訴自己的故事</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>&ldquo;The heart&rsquo;s not like a box that gets filled up. It expands in size the more you love.&rdquo;</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">人的心不像箱子一樣能夠被填滿。你愛得越深，它就會變得越大才能夠承受越多愛的重量</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Filled up </span><span style=""font-weight: 400;"">&nbsp;裝滿、填滿</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Expand </span><span style=""font-weight: 400;"">(v.) 展開、擴大</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong><span style=""font-weight: 400;"">*Fill in vs. Fill out?</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Fill in</span><span style=""font-weight: 400;""> 和 </span><span style=""font-weight: 400;"">Fill out</span><span style=""font-weight: 400;""> 都是</span>填寫<span style=""font-weight: 400;"">的意思。硬要區分的話，</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Fill in 是把小的空格填滿，像是</span>填空<span style=""font-weight: 400;"">; Fill out則是將大的範圍填滿，像是</span>填表格<span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">但作其他意思時，Fill in 可以表示</span>代替<span style=""font-weight: 400;"">、Fill out 則是</span>發胖</strong></p>
<p style=""font-size: 20px; color: #a42d00;""><strong>About time (2013) (私心大推! 看了三次❤)</strong></p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8779cba91c2-odc2Czk69ctrQjf5phNtcMWGRYo7jG-600x336.png"" alt="""" width=""600"" height=""336"" />&nbsp;</p>
<p style=""font-size: 16px; color: #003377;""><strong>Tim: &ldquo;We&rsquo;re all travelling through time together every day of our lives. </strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>All we can do is do our best to relish this remarkable ride.&rdquo;</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我們一起度過的每一天，其實都是時光中的旅行。</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我們能做的，就是好好地去品味這不凡的旅程。</span></p>
<p style=""font-size: 16px;""><br /><span style=""font-weight: 400;"">Relish </span><span style=""font-weight: 400;"">(</span><span style=""font-weight: 400;"">v.)品味 (n.)滋味</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">Remarkable</span><span style=""font-weight: 400;""> (adj.) 非凡的、卓越的</span></p>"','published' => '1','og_title' => '要怎麼走出失戀傷痛？五大療癒系電影經典對白幫你療傷！','og_description' => '失戀的時候除了大哭大鬧之外，看電影也是有效的療傷方式，優美的電影台詞不僅能撫慰人心，也能從中學到實用英文，以下獨家整理5部電影的療傷系對白，讓你邊學英文邊走出失戀的傷痛！','meta_title' => '要怎麼走出失戀傷痛？五大療癒系電影經典對白幫你療傷！','meta_description' => '失戀的時候除了大哭大鬧之外，看電影也是有效的療傷方式，優美的電影台詞不僅能撫慰人心，也能從中學到實用英文，以下獨家整理5部電影的療傷系對白，讓你邊學英文邊走出失戀的傷痛！','canonical_url' => 'https://tw.english.agency/five-movies-conversation','feature_image_alt' => '要怎麼走出失戀傷痛？五大療癒系電影經典對白幫你療傷！','feature_image_title' => '要怎麼走出失戀傷痛？五大療癒系電影經典對白幫你療傷！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a877da5eb453-bK7EuVXkMoAS9S8ffTeBot2BWixs4F-2121x1414.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-17 00:38:16"'],
  ['id' => '144','title' => '托福口說怎麼拿高分？套模板有效嗎？','slug' => 'toefl-template','excerpt' => '托福(TOEFL)是涵蓋聽、說、讀、寫四大面向的英語能力測驗，其中口說的部分更是不少考生感到棘手的題型。不過你知道嗎？其實托福口說的應答方式就像寫作文一樣，在句子結構上都有基本的模板可以套用，讓考生能夠更冷靜也更有系統的作答。接下來會詳細介紹托福口說的考試題型和相對應的參考模板，讓大家面對托福口說不再感到手足無措！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<h2><strong>標準模板一次報給你，讓你面對托福口說不再傷腦筋！</strong></h2>
<p><span style=""font-weight: 400;"">托福</span><span style=""font-weight: 400;"">(TOEFL)</span><span style=""font-weight: 400;"">是涵蓋聽、說、讀、寫四大面向的英語能力測驗，其中口說的部分更是不少考生感到棘手的題型。不過你知道嗎？其實托福口說的應答方式就像寫作文一樣，在句子結構上都有基本的模板可以套用，讓考生能夠更冷靜也更有系統的作答。接下來會詳細介紹托福口說的考試題型和相對應的參考模板，讓大家面對托福口說不再感到手足無措！</span></p>
<h2><strong>帶你認識托福口說！詳細的題型介紹與模板範例</strong></h2>
<p><span style=""font-weight: 400;"">六題的口說測驗當中，第一、二題是</span><span style=""font-weight: 400;"">Independent</span><span style=""font-weight: 400;"">題型，作答的內容可由考生自行決定；第三～六題是</span><span style=""font-weight: 400;"">Integrated</span><span style=""font-weight: 400;"">題型，考生必須從文章和錄音檔中統整資訊之後發表自己的看法。</span></p>
<ul>
<li style=""color: #ff6666;"">第一題：會問考生熟知的事物，例如喜歡讀的書、喜歡看的電影、喜歡的國家。</li>
</ul>
<ul>
<li style=""color: #ff6666;"">第二題：會問考生的價值觀，對於某議題是抱持贊成還是反對意見，或者是傾向哪一邊，例如喜歡住在城市還是鄉下、認為學生該不該有打工經驗。</li>
</ul>
<p><span style=""font-weight: 400;"">範例模板：</span></p>
<ul>
<ul>
<li><em>In my view，&hellip;.........for two reasons.</em></li>
<li><em>First，&hellip;.........For example (when I) &hellip;.........</em></li>
<li><em>Second，&hellip;.........For instance (when I) &hellip;.........</em></li>
<li><em>That&rsquo;s why&hellip;......... </em></li>
</ul>
</ul>
<ul>
<li style=""color: #ff6666;"">第三題：會先請考生閱讀一篇公告，接著會播放一段男女對話，最後要求考生陳述說話者的意見。</li>
</ul>
<blockquote>
<p>例如:學校公告學生辦活動的申請注意事項，女方在申請的過程中遇到問題詢問男方，男方提供解決方法後結束對話，題目就有可能要考生描述男方所提供的意見。</p>
</blockquote>
<p><span style=""font-weight: 400;"">範例模板：</span></p>
<ul>
<ul>
<li><em>The reading states that&hellip;.........</em></li>
<li><em>The man&rsquo;s opinion is that this is a good/bad idea.</em></li>
<li><em>He provides two reasons.</em></li>
<li><em>First，he feels&hellip;.........because&hellip;.........</em></li>
<li><em>Second，he agrees/disagrees with the idea/statement/issue，because&hellip;.........</em></li>
<li><em>That&rsquo;s why the man thinks it&rsquo;s (not) a good idea. </em></li>
</ul>
</ul>
<ul>
<li style=""color: #ff6666;"">第四題：和第三題相似，會先請考生閱讀一則短篇文章，接著會播放一段教授的講課內容，最後題目會要求考生描述教授所講解的內容。</li>
</ul>
範例模板：
<ul>
<ul>
<li><em>Note:</em></li>
<li><em>Title:（必寫）</em></li>
<li><em>Definition:（對標題的描述）</em></li>
<li><em>Example:（寫關鍵字提醒自己）</em></li>
<li><em>The article is about (title) which the passage defines as (the definition of title).</em></li>
<li><em>The professor provides an example of&hellip;.........to illustrate this.</em></li>
<li><em>He/She states that&hellip;.........</em></li>
<li><em>And so，this example clearly illustrates (title). </em></li>
</ul>
</ul>
<ul>
<li style=""color: #ff6666;"">第五題：會先聽到一段對話，通常是A遇到困難，B幫忙想出兩個解決方式，而考生在聽完之後要描述兩個選項，並且說明自己喜歡哪一個。</li>
</ul>
<p>範例模板：</p>
<ul>
<ul>
<li><em>The woman&rsquo;s/man&rsquo;s problem is that&hellip;.........</em></li>
<li><em>The students discuss two solutions.</em></li>
<li><em>First，&hellip;.........</em></li>
<li><em>Second，&hellip;.........</em></li>
<li><em>If I were the man/woman，I would choose the first/second solution.</em></li>
<li><em>The first/second isn&rsquo;t very good，because&hellip;.........</em></li>
<li><em>The first/second solution is much better because&hellip;.........</em></li>
<li><em>That&rsquo;s why I would choose the first/second solution. </em></li>
</ul>
</ul>
<ul>
<li style=""color: #ff6666;"">第六題：會讓考生聽一段教授的講課，並且要考生重新陳述一次，難度比第四題高。</li>
</ul>
<p>範例模板：</p>
<ul>
<ul>
<li><em>In this lecture，the professor talks about Topic. He/She describes two examples/ways that&hellip;.........</em></li>
<li><em>First，the professor gives an example to illustrate this about&hellip;.........</em></li>
<li><em>The second kind about &hellip;......... is &hellip;.........</em></li>
<li><em>The professor gives an example to describe this about&hellip;......... </em></li>
</ul>
</ul>
<h2><strong>套模板就能得高分？有特色的個人模板才是高分關鍵！</strong></h2>
<p><span style=""font-weight: 400;"">有了模板之後，有助於學生在準備口說測驗時更有方向，也能在正式考試前多加練習，不過對於套模板能否考高分一事，目前分成兩派說法。一派認為模板的句子結構是最保守的作答模式，只要作答時的表現別失常即能考取高分；另一派認為模板的內容雖然結構完整，但是缺乏考生的獨特性，中規中矩的表現難以獲得評審青睞。</span></p>
<p><span style=""font-weight: 400;"">其實兩派所提到的也是模板的優點和缺點，因此建議考生不清楚如何作答時可以參考模板的句子結構，熟悉模板結構之後再加入自己的風格，衍生出架構完整又不失自己特色的個人模板。另外使用模板時也要注意是否有贅字、內容是否死板，不自然的答題內容反而會帶給評審們不好的印象，造成反效果喔！</span></p>
</div>"','published' => '1','og_title' => '托福口說怎麼拿高分？套模板有效嗎？','og_description' => '托福(TOEFL)是涵蓋聽、說、讀、寫四大面向的英語能力測驗，其中口說的部分更是不少考生感到棘手的題型。不過你知道嗎？其實托福口說的應答方式就像寫作文一樣，在句子結構上都有基本的模板可以套用，讓考生能夠更冷靜也更有系統的作答。接下來會詳細介紹托福口說的考試題型和相對應的參考模板，讓大家面對托福口說不再感到手足無措！','meta_title' => '托福口說怎麼拿高分？套模板有效嗎？','meta_description' => '托福(TOEFL)是涵蓋聽、說、讀、寫四大面向的英語能力測驗，其中口說的部分更是不少考生感到棘手的題型。不過你知道嗎？其實托福口說的應答方式就像寫作文一樣，在句子結構上都有基本的模板可以套用，讓考生能夠更冷靜也更有系統的作答。接下來會詳細介紹托福口說的考試題型和相對應的參考模板，讓大家面對托福口說不再感到手足無措！','canonical_url' => 'https://tw.english.agency/toefl-template','feature_image_alt' => '托福口說怎麼拿高分？套模板有效嗎？','feature_image_title' => '托福口說怎麼拿高分？套模板有效嗎？','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a87c29764d96-KjvEn2voC7mS21taagvfKI0eh8XkAq-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-17 05:48:12"'],
  ['id' => '145','title' => 'NBA球賽又開打啦！看懂美國職籃必學的英文單字在這裡！','slug' => 'basketball-vocabularies','excerpt' => '"波士頓塞爾提克隊 (Boston Celtics) 的狂連勝為今年NBA (National Basketball Association) 球季帶來一波高潮，今天就來教教大家常用的籃球術語、NBA專用英文怎麼說，以後看球賽就不怕看不懂啦～"','content' => '"<p style=""gont-size: 16px;"">波士頓塞爾提克隊 (Boston Celtics) 的狂連勝為今年NBA (National Basketball Association) 球季帶來一波高潮，今天就來教教大家常用的籃球術語、NBA專用英文怎麼說，以後看球賽就不怕看不懂啦～</p>
<p style=""font-size: 20px; color: #003377;""><strong>Basketball team球隊</strong></p>
<p style=""font-size: 16px;""><strong>Point Guard</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">控球後衛</span></p>
<p style=""font-size: 16px;""><strong>Shooting Guard </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">得分後衛</span></p>
<p style=""font-size: 16px;""><strong>Small Forward </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">小前鋒</span></p>
<p style=""font-size: 16px;""><strong>Power Forward </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">大前鋒</span></p>
<p style=""font-size: 16px;""><strong>Center </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">中鋒</span></p>
<p style=""font-size: 16px;""><strong>Coach </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">教練</span></p>
<p style=""font-size: 16px;""><strong>Assistant coach</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">助理教練</span></p>
<p style=""font-size: 16px;""><strong>Mascot</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">吉祥物</span></p>
<p style=""font-size: 16px;""><strong>Starting lineup</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">先發陣容</span></p>
<p style=""font-size: 16px;""><strong>Starter</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">先發球員</span></p>
<p style=""font-size: 16px;""><strong>Sixth man </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">第六人 (最先替補球員)</span></p>
<p style=""font-size: 16px;""><strong>Bench </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">板凳球員 (全體)</span></p>
<p style=""font-size: 16px;""><strong>Bench player </strong><span style=""font-weight: 400;"">➪板</span><span style=""font-weight: 400;"">凳球員 (個人)</span></p>
<p style=""font-size: 16px;""><strong>Basketball Terms籃球術語</strong></p>
<p style=""font-size: 16px;""><strong>Shoot</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">投籃</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">*Shoot也有射擊、射殺、開槍、拍攝的意思</span></p>
<p style=""font-size: 16px;""><strong>Shoot a gun</strong><span style=""font-weight: 400;""> 打槍</span></p>
<p style=""font-size: 16px;""><strong>Shoot an arrow</strong><span style=""font-weight: 400;""> 射箭</span></p>
<p style=""font-size: 16px;""><strong>Shoot sb. doing sth.</strong><span style=""font-weight: 400;""> 拍攝某人做某事的畫面</span></p>
<p style=""font-size: 16px;""><strong>Layup </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">帶球上籃</span></p>
<p style=""font-size: 16px;""><strong>(Slam) Dunk</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">(強力)灌籃 </span></p>
<p style=""font-size: 16px;""><strong>Dribble</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">運球</span></p>
<p style=""font-size: 16px;""><strong>Behind-the-back dribble </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">背後運球</span></p>
<p style=""font-size: 16px;""><strong>Air ball </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">空心球</span></p>
<p style=""font-size: 16px;""><strong>3-pointer shot</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">三分球</span></p>
<p style=""font-size: 16px;""><strong>Fast break </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">快攻</span></p>
<p style=""font-size: 16px;""><strong>Assist </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">助攻</span></p>
<p style=""font-size: 16px;""><strong>Jump ball </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">跳球</span></p>
<p style=""font-size: 16px;""><strong>Pass</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">傳球</span></p>
<p style=""font-size: 16px;""><strong>Steal </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">抄球</span></p>
<p style=""font-size: 16px;""><strong>Block/Reject </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">蓋火鍋</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">*Reject也有駁回、拒絕、次品的意思</span></p>
<p style=""font-size: 16px;""><strong>Reject goods </strong><span style=""font-weight: 400;"">次品</span></p>
<p style=""font-size: 16px;""><strong>Buzzer Beater</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">壓哨球</span></p>
<p style=""font-size: 16px;""><strong>Rebound </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">籃板球</span></p>
<p style=""font-size: 16px;""><strong>Free throw</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">罰球</span></p>
<p style=""font-size: 16px;""><strong>Scoring </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">得分</span></p>
<p style=""font-size: 16px;""><strong>Turnover</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">失誤</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">*</span><span style=""font-weight: 400;"">Turnover也可以用來指公司人員更換率</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">The company has a high turnover of staff because the working conditions are very poor.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">這家公司的人員更換率很高，因為工作環境實在太差了</span></p>
<p style=""font-size: 16px;""><strong>Halftime </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">中場休息</span></p>
<p style=""font-size: 16px;""><strong>First half </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">上半場</span></p>
<p style=""font-size: 16px;""><strong>Second half </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">下半場</span></p>
<p style=""font-size: 16px;""><strong>First/Second/Third/Fourth period </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">第一/二/三/四 節</span></p>
<p style=""font-size: 16px;""><strong>Overtime</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">延長賽</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">*Overtime也可以指加班、超時</span></p>
<p style=""font-size: 16px;""><strong>Overtime pay</strong><span style=""font-weight: 400;""> 加班費</span></p>
<p style=""font-size: 16px;""><strong>Overtime parking </strong><span style=""font-weight: 400;"">超時停車</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Employees in this Japanese company are expected to work overtime</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">這間日本公司的員工都被要求加班</span></p>
<p style=""font-size: 16px;""><strong>Timeout</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">暫停</span></p>
<p style=""font-size: 16px;""><strong>Debut </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">首次上場</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">*Debut也可以指明星藝人的出道、首次登台演出</span></p>
<p><span style=""font-weight: 400;"">Natty is so talented! She should debut and become an outstanding idol!</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Natty真是太有天賦了！她應該出道並成為一名傑出的偶像</span></p>
<p>&nbsp;</p>
<p style=""font-size: 20px; color: #003377;""><strong>Basketball Court 籃球場</strong></p>
<p style=""font-size: 16px;""><strong>Backboard </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">籃板</span></p>
<p style=""font-size: 16px;""><strong>Back court </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">後場</span></p>
<p style=""font-size: 16px;""><strong>Front court </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">前場</span></p>
<p style=""font-size: 16px;""><strong>Free throw lane</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">罰球圈</span></p>
<p style=""font-size: 16px;""><strong>Free throw line </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">罰球線</span></p>
<p style=""font-size: 16px;""><strong>Baseline </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">底線</span></p>
<p style=""font-size: 16px;""><strong>Net </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">籃網</span></p>
<p style=""font-size: 16px;""><strong>Arena </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">比賽場館</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">台北小巨蛋的英文就是Taipei Arena，不是什麼 &ldquo;Taipei Small Giant Egg&rdquo;喔！</span></p>
<p style=""font-size: 20px; color: #003377;""><strong>Rules 規則</strong></p>
<p style=""font-size: 16px;""><strong>Foul </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">犯規</span></p>
<p style=""font-size: 16px;""><strong>Personal foul </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">侵人犯規</span></p>
<p style=""font-size: 16px;""><strong>Flagrant foul </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">惡意犯規</span></p>
<p style=""font-size: 16px;""><strong>Technical foul</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">技術犯規</span></p>
<p style=""font-size: 16px;""><strong>Disqualification</strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">犯滿離場 (畢業)</span></p>
<p style=""font-size: 16px;""><strong>Ejection</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">驅逐出場</span></p>
<p style=""font-size: 16px;""><strong>Double Dribble</strong> <span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">二次運球</span></p>
<p style=""font-size: 16px;""><strong>Walking/Traveling </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">走步</span></p>
<p style=""font-size: 16px;""><strong>Charging </strong><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">撞人</span></p>
<p>&nbsp;</p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">#2016年球季結束後，傳奇球星 老大Kobe Bryant退休，在他20年的職業生涯中留下了許多經典、激勵人心的名言：</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>If you are afraid to fail，then you&rsquo;re probably going to fail.</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">如果你害怕失敗，那你很有可能會失敗</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Everything negative －pressure，challenges－ is all an opportunity for me to rise.</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">每一個負面的事情，壓力、挑戰，都是讓我提升的機會</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Love me or hate me. It&rsquo;s one or the other. Always has been. Hate my game，my swagger. Hate my fadeaway，my hunger. Hate that I&rsquo;m a veteran，champion. Hate that. Hate it with all your heart. And hate that I&rsquo;m loved for the exact same reasons.</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">愛我或是恨我，兩者必有其一，一直都是這樣。恨我上場、恨我昂首闊步。恨我的後仰投籃、我的蓬勃野心。恨我是一名老手、萬夫莫敵。恨吧，用你全部的心思去恨吧。我也因為同樣的原因被深愛著。</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>An reporter asked Bryant: &ldquo;Kobe，why are you so successful?&rdquo;</strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>Kobe asked the reporter: &ldquo;Have you seen the Los Angeles at 4a.m.?&rdquo;</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">一名記者問Bryant : &ldquo;Kobe，為何妳這麼成功?&rdquo;</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Kobe反問記者 : &ldquo;你看過凌晨4時洛杉磯的樣子嗎?&rdquo;</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Heros come and go，but legends are forever.</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">英雄一時，但傳奇永世</span></p>"','published' => '1','og_title' => 'NBA球賽又開打啦！看懂美國職籃必學的英文單字在這裡！','og_description' => '"波士頓塞爾提克隊 (Boston Celtics) 的狂連勝為今年NBA (National Basketball Association) 球季帶來一波高潮，今天就來教教大家常用的籃球術語、NBA專用英文怎麼說，以後看球賽就不怕看不懂啦～"','meta_title' => 'NBA球賽又開打啦！看懂美國職籃必學的英文單字在這裡！','meta_description' => '"波士頓塞爾提克隊 (Boston Celtics) 的狂連勝為今年NBA (National Basketball Association) 球季帶來一波高潮，今天就來教教大家常用的籃球術語、NBA專用英文怎麼說，以後看球賽就不怕看不懂啦～"','canonical_url' => 'https://tw.english.agency/basketball-vocabularies','feature_image_alt' => 'NBA球賽又開打啦！看懂美國職籃必學會的英文單字在這裡！','feature_image_title' => 'NBA球賽又開打啦！看懂美國職籃必學會的英文單字在這裡！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a87d7a74fc86-i8hA00FiTmAQKiWuH9lw2nA9I8Ok2G-1024x706.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-17 07:20:57"'],
  ['id' => '146','title' => '國外用餐禁忌有什麼?千萬別這麼做以免鬧出笑話！','slug' => 'table-manners','excerpt' => '"Table manners matter！對所有國家的人來說，餐桌禮儀都是評斷一個人的一個主要條件，台灣人用筷子湯匙就能吃遍大街小巷，但在國外光是用餐就得用上三倍以上的餐具，一不小心拿錯餐具吃飯可是會被瞪的！還有哪些台灣的用餐習慣，在國外是被視為禁忌呢？今天就向大家介紹國外的餐桌禮儀和禁忌，千萬別這樣做以免鬧出笑話喔！"','content' => '"<p style=""font-size: 16px;"">Table manners matter！對所有國家的人來說，餐桌禮儀都是評斷一個人的一個主要條件，台灣人用筷子湯匙就能吃遍大街小巷，但在國外光是用餐就得用上三倍以上的餐具，一不小心拿錯餐具吃飯可是會被瞪的！還有哪些台灣的用餐習慣，在國外是被視為禁忌呢？今天就向大家介紹國外的餐桌禮儀和禁忌，千萬別這樣做以免鬧出笑話喔！</p>
<p style=""font-size: 20px; color: #880000;""><strong>餐桌禮儀須知：</strong></p>
<p style=""font-size: 16px;"">table manners 餐桌禮儀<br />Etiquette 禮儀、規範<br />Utensil廚具<br />Tableware餐具(泛指從擺桌、端餐、用餐都需要用到的餐具，例如高腳杯)<br />Flatware/Cutlery餐具(指用餐時的器具，例如刀叉匙)<br />Silverware 銀器<br />Service set餐具組<br />Glassware 玻璃杯</p>
<p style=""font-size: 20px; color: #880000;""><br /><strong>Hostess gift 作客時送的禮物：</strong></p>
<p style=""font-size: 16px;"">常常在電影裡看到作客的人帶酒、花束、巧克力甚至手作甜點給主人，現實生活中有人喜歡，也有人不喜歡啦！那要如何挑選不怕踩地雷的禮物？</p>
<p style=""font-size: 16px;"">避免有時效性、體積太大的禮物。像是甜點跟花束會讓主人花很多時間處理、展示。也有人推薦橄欖油(Olive oil)、高檔毛巾(Guest towel)、餐具組(Serving set)等等都很適合送禮。</p>
<p style=""font-size: 20px; color: #880000;""><br />Dress Code 著裝需求</p>
<p style=""font-size: 16px;"">通常邀請函上面會寫要求(Request)、建議(Suggest)的穿著。</p>
<p style=""font-size: 20px; color: #880000;"">Arrival 什麼時候抵達呢：</p>
<p style=""font-size: 16px;"">千萬別以為這跟跑趴一樣，晚兩小時到達才是標準時間！最好在聚會時間的前十分鐘來，遲到會給人不好印象。</p>
<p style=""font-size: 20px; color: #880000;"">Seating座位：</p>
<p style=""font-size: 16px;"">別找到位子就一屁股坐下，最好等主人邀請你坐哪個位子才坐。</p>
<p style=""font-size: 20px; color: #880000;"">End of dinner何時結束：</p>
<p style=""font-size: 16px;"">當你看到咖啡或茶類端上桌時，代表整個用餐流程結束。這時候可以告知對方要離開了。</p>
<p style=""font-size: 16px; color: #003377;"">#中途離開請說Excuse me：<br />若中途想上廁所，別脫口說I&rsquo;m going to the restroom。你可以說Excuse me或者I&rsquo;ll be right back就可以了</p>
<p style=""font-size: 20px; color: #880000;"">用餐禮儀，你該知道的事：</p>
<p style=""font-size: 16px;"">Once you are seated，take the napkin from the plate; unfold it and lay it across your lap. But don&rsquo;t use it to wipe your nose.<br />坐下之後，從盤上拿起餐巾，攤開並放在大腿上。但別用它來擦拭鼻子</p>
<p style=""font-size: 16px;"">The flatware is laid on the table in the order of use. Use the flatware from the outside in.<br />餐具擺放是依照用餐先後順序。所以要由外而內使用餐具</p>
<p style=""font-size: 16px;"">Wait until everyone is served before beginning to eat.<br />等到所有人桌上都有餐點才能開動</p>
<p style=""font-size: 16px;"">Taste your food before seasoning it.<br />進食後，才能開始加調味料</p>
<p style=""font-size: 16px;"">The universal resting position is placing your knife and fork in a cross on the plate，fork tines pointed down.<br />還在休息未用餐完畢的話，普遍方法是將刀叉以「X」符號放在盤上，叉齒朝下。</p>
<p style=""font-size: 16px;"">When you&rsquo;ve finished eating，the knife and fork are placed side by side on the right side of the plate in the 4 o&rsquo;clock position，with the fork on the inside，tines up，and the knife on the outside，blade in.<br />當你用餐完畢，將刀叉放置在盤子的右側四點鐘方向。叉子在內側，叉齒朝上；刀子則放在外側，刀鋒朝內。</p>
<p style=""font-size: 20px; color: #880000;"">用餐禁忌：</p>
<p style=""font-size: 16px;"">Don&rsquo;t place your elbows on the table.<br />不要把手肘放在桌子上</p>
<p style=""font-size: 16px;"">Do not talk with your mouth full.<br />嘴巴有食物時別開口說話</p>
<p style=""font-size: 16px;"">Don&rsquo;t reach across the table for anything.<br />別越過桌子拿東西</p>
<p style=""font-size: 16px;"">Don&rsquo;t use your phone during a meal.<br />別在用餐期間使用手機</p>
<p style=""font-size: 16px;""><br />Fork and Knife 刀叉使用方法(美式vs歐式)：<br />The American way美式用法<br />用餐時左手握叉子，右手握刀子來切食物。切完後將刀子橫放盤上，刀鋒面向自己。</p>
<p style=""font-size: 16px;"">Cut the food with the knife and place it on the upper edge of the plate with blading facing in. Transfer the fork over to your right hand.</p>
<p style=""font-size: 16px;"">右手持刀切食物，然後以刀鋒向內將刀子放在盤子前方處，再將叉子轉換到右手來進食</p>
<p style=""font-size: 16px;"">The Continental way 歐式用法<br />和美式用法差別在於進食時候，不需要將叉子換到右手，保持原樣就可以了。</p>
<p style=""font-size: 16px;"">You can eat your food with the fork in the left hand.<br />你可以用左手持叉進食</p>
<p>&nbsp;</p>"','published' => '1','og_title' => '國外用餐禁忌有什麼?千萬別這麼做以免鬧出笑話！','og_description' => '"Table manners matter！對所有國家的人來說，餐桌禮儀都是評斷一個人的一個主要條件，台灣人用筷子湯匙就能吃遍大街小巷，但在國外光是用餐就得用上三倍以上的餐具，一不小心拿錯餐具吃飯可是會被瞪的！還有哪些台灣的用餐習慣，在國外是被視為禁忌呢？今天就向大家介紹國外的餐桌禮儀和禁忌，千萬別這樣做以免鬧出笑話喔！"','meta_title' => '國外用餐禁忌有什麼?千萬別這麼做以免鬧出笑話！','meta_description' => '"Table manners matter！對所有國家的人來說，餐桌禮儀都是評斷一個人的一個主要條件，台灣人用筷子湯匙就能吃遍大街小巷，但在國外光是用餐就得用上三倍以上的餐具，一不小心拿錯餐具吃飯可是會被瞪的！還有哪些台灣的用餐習慣，在國外是被視為禁忌呢？今天就向大家介紹國外的餐桌禮儀和禁忌，千萬別這樣做以免鬧出笑話喔！"','canonical_url' => 'https://tw.english.agency/table-manners','feature_image_alt' => '國外用餐禁忌有什麼?千萬別這麼做以免鬧出笑話！','feature_image_title' => '國外用餐禁忌有什麼?千萬別這麼做以免鬧出笑話！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a87dd4d1e789-iqpPrDYXD8jMtAHMY0EC4fT5J4g4yS-2121x1414.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-17 07:44:26"'],
  ['id' => '147','title' => '眼霜、保濕面膜的英文怎麼說？學會這些保養品單字讓妳出國再也不怕買錯東西！','slug' => 'skin-care-vocabularies','excerpt' => '出國常常會因為空氣很乾需要買保養品急救肌膚，但是國外的藥妝店產品全部都寫英文，也不知道該怎麼跟店員詢問需要的產品，常常會有買錯的情況發生！不過不用擔心，今天就來教大家各式保養品的英文說法，讓妳就算在國外也不怕買不到護膚乳啦！','content' => '"<p style=""font-size: 16px;"">出國常常會因為空氣很乾需要買保養品急救肌膚，但是國外的藥妝店產品全部都寫英文，也不知道該怎麼跟店員詢問需要的產品，常常會有買錯的情況發生！不過不用擔心，今天就來教大家各式保養品的英文說法，讓妳就算在國外也不怕買不到護膚乳啦！</p>
<p style=""font-size: 20px; color: #880000;""><strong>Facial Care Products 臉部保養品</strong></p>
<p style=""font-size: 16px;""><strong>Facial cleanser/ face wash </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">洗面乳</span></p>
<p style=""font-size: 16px;""><strong>Toning lotion </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">化妝水</span></p>
<p style=""font-size: 16px;""><strong>Essence </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">精華液</span></p>
<p style=""font-size: 16px;""><strong>Moisturizer </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">保濕</span></p>
<p style=""font-size: 16px;""><strong>Nutritious </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">滋養</span></p>
<p style=""font-size: 16px;""><strong>Hydro </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">水的</span></p>
<p style=""font-size: 16px;""><strong>Whitening </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">美白</span></p>
<p style=""font-size: 16px;""><strong>Lotion</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">露</span></p>
<p style=""font-size: 16px;""><strong>Cream</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">霜</span></p>
<p style=""font-size: 16px;""><strong>Day cream</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">日霜</span></p>
<p style=""font-size: 16px;""><strong>Night cream</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">晚霜</span></p>
<p style=""font-size: 16px;""><strong>Facial mask/ Masque</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">面膜</span></p>
<p style=""font-size: 16px;""><strong>Peel off mask</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">撕去性面膜</span></p>
<p style=""font-size: 16px;""><strong>Rinse off mask</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">沖洗式面膜</span></p>
<p style=""font-size: 16px;""><strong>Scrub</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">去角質、磨砂</span></p>
<p style=""font-size: 16px;""><strong>Water spray </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">保濕噴霧</span></p>
<p style=""font-size: 16px;""><strong>Eye cream/ Eye gel</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">眼霜</span></p>
<p style=""font-size: 16px;""><strong>Eye mas </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">眼膜</span></p>
<p style=""font-size: 16px;""><strong>Lip balm </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">護唇膏</span></p>
<p style=""font-size: 16px;""><strong>Skin Care </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">護膚</span></p>
<p style=""font-size: 20px; color: #880000;""><strong>Body Care Product 身體保養品</strong></p>
<p style=""font-size: 16px;""><strong>Body moisturizer </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">身體保濕</span></p>
<p style=""font-size: 16px;""><strong>Body lotion </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">身體乳液</span></p>
<p style=""font-size: 16px;""><strong>Body butter </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">身體乳霜(油)</span></p>
<p style=""font-size: 16px;""><strong>Body cream </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">身體霜</span></p>
<p style=""font-size: 16px;""><strong>Body spray </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">身體噴霧</span></p>
<p style=""font-size: 16px;""><strong>Body wash/showing gel </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">沐浴乳</span></p>
<p style=""font-size: 16px;""><strong>Sunscreen spray </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">防曬噴霧</span></p>
<p style=""font-size: 16px;""><strong>Body scrub </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">身體去角質</span></p>
<p style=""font-size: 16px;""><strong>Self-tanning oil </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">助曬</span></p>
<p style=""font-size: 16px;""><strong>Perfume/fragrance </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">香水</span></p>
<p style=""font-size: 20px; color: #880000;""><strong>Hand Care Product 手部保養品</strong></p>
<p style=""font-size: 16px;""><strong>Hand cream/ hand lotion </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">護手霜</span></p>
<p style=""font-size: 16px;""><strong>Hand cleanser </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">洗手乳</span></p>
<p style=""font-size: 16px;""><strong>Nail polish </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">指甲油</span></p>
<p style=""font-size: 16px;""><strong>Polish remover </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">去光水</span></p>
<p style=""font-size: 16px;""><strong>Foot Care Product 腿部保養品</strong></p>
<p style=""font-size: 16px;""><strong>Foot cream </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">腿霜</span></p>
<p style=""font-size: 16px;""><strong>Foot scrub </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">腿部去角質</span></p>
<p style=""font-size: 20px; color: #880000;""><strong>Hair Care Product 頭髮保養品</strong></p>
<p style=""font-size: 16px;""><strong>Shampoo </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">洗髮精</span></p>
<p style=""font-size: 16px;""><strong>Conditioner </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">潤髮乳</span></p>
<p style=""font-size: 16px;""><strong>Mousse</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">慕斯</span></p>
<p style=""font-size: 16px;""><strong>Hair treatment/ Hair Essence</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">護髮精華</span></p>
<p>&nbsp;</p>
<p style=""font-size: 18px; color: #003377;""><strong>【補充】</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">保養品有許多不同的功能，其中</span><strong>anti-</strong><span style=""font-weight: 400;""> 指的就是防、抗、反，像是</span></p>
<p style=""font-size: 16px;""><strong>Anti-wrinkle</strong><span style=""font-weight: 400;""> 就是指抗皺的意思，同理，</span><strong>anti-</strong><span style=""font-weight: 400;"">這個字根也可以用在很多其他領域，例如：</span></p>
<p style=""font-size: 16px;""><strong>Anti-social </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">不愛社交的</span></p>
<p style=""font-size: 16px;""><strong>Anti-oxidant </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">抗氧化劑</span></p>
<p style=""font-size: 16px;""><strong>Antipathy </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">反感</span></p>
<p style=""font-size: 16px;""><strong>Antiwar </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">反戰的</span></p>
<p style=""font-size: 16px;""><strong>Antidepressant </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">抗抑鬱劑</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">保養品其他常見的功能還包括了：</span></p>
<p style=""font-size: 16px;""><strong>Oil control </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">控油</span></p>
<p style=""font-size: 16px;""><strong>Brightening </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">亮膚</span></p>
<p style=""font-size: 16px;""><strong>Balance </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">油水平衡</span></p>
<p style=""font-size: 16px;""><strong>Clean </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">清潔</span></p>
<p style=""font-size: 16px;""><strong>Waterproof </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">防水</span></p>
<p style=""font-size: 16px;""><strong>Firming </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">緊實</span></p>
<p style=""font-size: 16px;""><strong>Gentle </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">溫和的</span></p>
<p style=""font-size: 16px;""><strong>Long lasting </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">持久性</span></p>
<p style=""font-size: 16px;""><strong>Treatment </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">修護</span></p>
<p style=""font-size: 16px;""><strong>Renewal </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">活膚修護</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">保養品也含了許多成分，常見的有：</span></p>
<p style=""font-size: 16px;""><strong>Mineral oil</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">礦物油</span></p>
<p style=""font-size: 16px;""><strong>Plant oil </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">植物油</span></p>
<p style=""font-size: 16px;""><strong>Aloe vera </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">蘆薈</span></p>
<p style=""font-size: 16px;""><strong>Alcohol </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">酒精</span></p>
<p style=""font-size: 16px;""><strong>Fruit acids</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">果酸</span></p>
<p style=""font-size: 16px;""><strong>Sulfur </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">硫磺</span></p>
<p style=""font-size: 16px;""><strong>Tea tree</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">茶樹</span></p>
<p style=""font-size: 16px;""><strong>Green tea extract</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">綠茶精華</span></p>
<p style=""font-size: 16px;""><strong>Kukui nut oil</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">夏威夷核油</span></p>
<p style=""font-size: 16px;""><strong>Urea</strong> <span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">尿素</span></p>
<p style=""font-size: 16px;""><strong>Vitamin </strong><span style=""font-weight: 400;"">➩</span><span style=""font-weight: 400;"">維他命</span></p>
<p style=""font-size: 20px; color: #003377;""><strong>Examples 保養品店常用例句 </strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>Do you have any sample I can try? </strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">有試用品讓我用用看嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Can you apply a little on my face? </strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">可以試一點在我臉上嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>How much does Jo Malone perfume cost?</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Jo Malone的這款香水要多少錢？</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>What is this lotion for? </strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">這款乳液的功效是什麼？</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>I am looking for an alcohol-free toner</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我在找一個不含酒精的化妝水</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>I heard that your eye creams work pretty well. Could you recommend one?</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我聽說你們的眼霜很有用，可以推薦我一下嗎？ </span></p>
<p style=""font-size: 16px; color: #003377;""><strong>I have sensitive skin. Could you recommend any suitable skin care product to me? </strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我是敏感肌。可以請你推薦任何適合的護膚產品給我嗎？</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Is there any discount/promotion? </strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>Are there any discounts/ promotions? </strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">有任何折扣、促銷活動嗎？ </span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">(Is or Are depends on 指的是單數還是複數(或是整間店)的產品)</span></p>"','published' => '1','og_title' => '眼霜、保濕面膜的英文怎麼說？學會這些保養品單字讓妳出國再也不怕買錯東西！','og_description' => '出國常常會因為空氣很乾需要買保養品急救肌膚，但是國外的藥妝店產品全部都寫英文，也不知道該怎麼跟店員詢問需要的產品，常常會有買錯的情況發生！不過不用擔心，今天就來教大家各式保養品的英文說法，讓妳就算在國外也不怕買不到護膚乳啦！','meta_title' => '眼霜、保濕面膜的英文怎麼說？學會這些保養品單字讓妳出國再也不怕買錯東西！','meta_description' => '出國常常會因為空氣很乾需要買保養品急救肌膚，但是國外的藥妝店產品全部都寫英文，也不知道該怎麼跟店員詢問需要的產品，常常會有買錯的情況發生！不過不用擔心，今天就來教大家各式保養品的英文說法，讓妳就算在國外也不怕買不到護膚乳啦！','canonical_url' => 'https://tw.english.agency/skin-care-vocabularies','feature_image_alt' => '眼霜、保濕面膜的英文怎麼說？學會這些保養品單字讓妳出國再也不怕買錯東西！','feature_image_title' => '眼霜、保濕面膜的英文怎麼說？學會這些保養品單字讓妳出國再也不怕買錯東西！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a87e1fd86e23-nwGve21BhSBW4bUcSsV1hJMogYu3cy-724x483.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-17 08:04:21"'],
  ['id' => '148','title' => '到國外也想耍耍文青？這些藝術相關的英文單字你一定要會！','slug' => 'art-vocabularies','excerpt' => '身為一個稱職的文藝青年，出國的時候一定要安排一些去美術館的行程拍拍照看看展覽，對插畫、雕塑、刺繡等等藝術很有興趣的你們，是不是很想知道這些藝術的英文單字是什麼呢？今天就來跟大家介紹這些藝術相關英文單字，讓你離成為一個國際化的文青更近一步！','content' => '"<p style=""font-size: 16px;"">身為一個稱職的文藝青年，出國的時候一定要安排一些去美術館的行程拍拍照看看展覽，對插畫、雕塑、刺繡等等藝術很有興趣的你們，是不是很想知道這些藝術的英文單字是什麼呢？今天就來跟大家介紹這些藝術相關英文單字，讓你離成為一個國際化的文青更近一步！</p>
<p style=""font-size: 16px;""><strong>Art ➪藝術</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">Art這個字加一個s，</span><strong>Arts</strong><span style=""font-weight: 400;"">就有文科的意思，像是文學學士的英文就是</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Bachelor of Arts (B.A.)</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Bachelor of Science (B.S.)</strong><span style=""font-weight: 400;""> 理學學士 </span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Master of Business (MBus)</strong><span style=""font-weight: 400;""> 商學碩士</span></p>
<p style=""font-size: 16px;""><strong><span style=""font-weight: 400;"">藝術品的英文可以用：</span>artwork，work of art，art piece，piece of art，art object</strong></p>
<p style=""font-size: 16px;""><strong>Performing arts ➪表演藝術</strong></p>
<p style=""font-size: 16px;""><strong>Theater ➪劇場</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Theater vs. Theatre</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">美式英文習慣寫成 Theater，英式英文則是將結尾寫成re </span><span style=""font-weight: 400;"">&rarr;</span> <strong>Theatre</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">一些常見的英式、美式英文差別如下：</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>褲子：Trousers (英)/ Pants (美) </strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>一樓：Ground floor(英) / First floor (美) </strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>薯條：Chips (英) / Fries (美) </strong></p>
<p style=""font-size: 16px; color: #003377;""><strong>薯片：Crisps(英)/ Chips (美)</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">That&rsquo;s why 英國菜fish and chips (炸魚薯條)上桌時，看到的是薯條而不是薯片啦</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Music </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">音樂</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Dance </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">舞蹈</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Applause </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">鼓掌</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">外國人有時候也喜歡</span><span style=""font-weight: 400;"">cue &ldquo;</span><strong>drum roll</strong><span style=""font-weight: 400;"">&rdquo; (擊鼓聲) 在重頭戲、高潮部分之前</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">We&rsquo;re now announcing the winner! Drum roll，please!</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">我們現在要揭曉贏家！請來點鼓聲</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Auditorium </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">禮堂</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Opera </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">歌劇</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Symphony </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">交響樂</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Orchesta</span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">管弦樂團</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Concert </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">演唱會</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Composer </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">作曲家</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Lyric </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">歌詞</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tune </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">曲調</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Melody </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">旋律</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Harmony </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">合音</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Scene </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">景、場</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Visual arts </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">視覺藝術</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Ceramics </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">陶瓷</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Drawing </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">繪圖</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Painting </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">畫</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sculpture </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">雕塑</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Statue </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">雕像</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Statue</span><span style=""font-weight: 400;"">的易混淆字: </span><strong>Status</strong><strong>，</strong><span style=""font-weight: 400;""> 意思是</span><strong>狀態、地位、身份</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Women&rsquo;s social status has changed much over the years.</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">女人的社會地位在這些年來有很大的改變</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Printmaking </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">版畫</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Crafts </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">工藝</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Photography </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">攝影</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">動詞就是</span><strong>take</strong><span style=""font-weight: 400;""> a photo of sth./sb. </span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Photograph的重音在PHO，</span><strong>攝影師 Photographer</strong><span style=""font-weight: 400;"">重音則在</span><span style=""font-weight: 400;"">TO</span>&nbsp;</p>
<p style=""font-size: 16px;""><strong>Photobomb <span style=""font-weight: 400;"">則是指破壞照片的人、動物或任何東西</span></strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Video </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">影片</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Filmmaking </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">電影</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Design </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">設計</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Architecture </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">建築</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Calligraphy </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">書法</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Sketch </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">素描</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Embroidery </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">刺繡</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Illustration </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">插圖</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Illustration</strong><span style=""font-weight: 400;"">也有說明、實例的意思</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">作動詞</span><strong>Illustrate</strong><span style=""font-weight: 400;"">: (用圖、例子)說明</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">The professor illustrates his lectures with ppt slides，videos and guest speakers.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">教授用ppt幻燈片、影片和特邀嘉賓 來講解他的課</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Illustrator</strong> <span style=""font-weight: 400;"">插畫圖家、說明者、起實例作用的事物</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Knit </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">編織</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Public art </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">公共藝術</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Fine art </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">美術</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Portfolio </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">作品集</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Portrait </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">肖像</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Venue </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">會場</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Gallery </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">畫廊</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Museum </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">博物館</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Abstract </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">抽象的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Delicate </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">精巧的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Grotesque </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">奇怪的</span></p>
<p style=""font-size: 16px; color: #a42d00;""><span style=""font-weight: 400;"">形容很怪、反常還可以用：</span><strong>Weird，Strange，Odd，Bizarre，Off the wall</strong></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>Creepy</strong><span style=""font-weight: 400;"">是怪誕、毛骨悚然的意思，也可以用來形容遇到怪人</span></p>
<p style=""font-size: 16px; color: #a42d00;""><strong>怪人: Weirdo</strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Nude </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">裸的</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Forgery </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">偽造品</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Landscape </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">景色</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Realism </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">寫實主義</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Texture </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">質地、紋理</span></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Figure </span><span style=""font-weight: 400;"">➪</span><span style=""font-weight: 400;"">數字、圖像</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Figure out <span style=""font-weight: 400;"">是個很常見的片語，意思是:</span>想出(辦法)、搞懂、弄清楚</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Jamil and Rita haven&rsquo;t figured out how to solve this math problem.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Jamil和Rita還想不出這題數學的解題方法</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">＃Figure out vs. Find out</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Figure out </strong><span style=""font-weight: 400;"">是要用</span><strong>腦筋想、理解</strong><span style=""font-weight: 400;"">;</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Find out</strong><span style=""font-weight: 400;""> 則是</span><strong>發現、查明</strong><span style=""font-weight: 400;"">的意思</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">A: Do you know who took Charlie&rsquo;s cellphone?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">你知道是誰拿了Charlie的手機嗎?</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">B: Well，I will find out soon.</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">我很快就會查清楚了</span></p>"','published' => '1','og_title' => '到國外也想耍耍文青？這些藝術相關的英文單字你一定要會！','og_description' => '身為一個稱職的文藝青年，出國的時候一定要安排一些去美術館的行程拍拍照看看展覽，對插畫、雕塑、刺繡等等藝術很有興趣的你們，是不是很想知道這些藝術的英文單字是什麼呢？今天就來跟大家介紹這些藝術相關英文單字，讓你離成為一個國際化的文青更近一步！','meta_title' => '到國外也想耍耍文青？這些藝術相關的英文單字你一定要會！','meta_description' => '身為一個稱職的文藝青年，出國的時候一定要安排一些去美術館的行程拍拍照看看展覽，對插畫、雕塑、刺繡等等藝術很有興趣的你們，是不是很想知道這些藝術的英文單字是什麼呢？今天就來跟大家介紹這些藝術相關英文單字，讓你離成為一個國際化的文青更近一步！','canonical_url' => 'https://tw.english.agency/art-vocabularies','feature_image_alt' => '到國外也想耍耍文青？這些藝術相關的英文單字你一定要會！','feature_image_title' => '到國外也想耍耍文青？這些藝術相關的英文單字你一定要會！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a87f34879119-5rKQ6GqROZmzxY5AlEBpBhzhW6de3H-725x482.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-17 09:13:35"'],
  ['id' => '149','title' => '考口說總是很緊張所以常失分？托福口說準備和攻略總整理！','slug' => 'toefl-speak','excerpt' => '托福考試已經不容易，其中口說更是亞洲學生的痛點，主要是因非母語的生活環境，少用英語思考，短期內要有英語環境不容易，但是短期內準備好托福口說卻不難，先從自己有興趣能讀進去，願意做筆記的資訊開始。','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p><span style=""font-weight: 400;"">托福考試已經不容易，其中口說更是亞洲學生的痛點，主要是因非母語的生活環境，少用英語思考，短期內要有英語環境不容易，但是短期內準備好托福口說卻不難，先從自己有興趣能讀進去，願意做筆記的資訊開始。</span></p>
<h2><strong>托福口說準備法 分析評分標準</strong></h2>
<p><span style=""font-weight: 400;"">從托福口說的評分有三大標準，只要達到標準，就可以掌握基本分數，包括：</span></p>
<p><span style=""font-weight: 400;""><strong>一、邏輯性（pause structure）：</strong>指的是語句的先後邏輯，在講述完內容的時候，必須先做一個小小的停頓，告訴聽者，下一個內容／觀點要開始了。這樣就可以彌補回答中邏輯詞的發音不準或者是誤用、漏用帶來的失分。例如: Firstly，...Secondaly.. ；By the way ...However...</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;""><strong>二、準確性：</strong>針對問題命中紅心，回答並沒有離題。</span></p>
<p><span style=""font-weight: 400;""><strong>三、流利性：</strong> 說出句子的流暢程度，不要重複類似的語法與詞彙，發音清晰。</span></p>
<blockquote>綜合上述3個重點，會發現若平常不準備，其實是無法應答流暢，那麼該如何才能在口說考試時臨危不亂呢？「作筆記」是重要法門，做好筆記就能夠事半功倍，「好的筆記法」有幾個重點：</blockquote>
<h4>&nbsp;</h4>
<h4><span style=""font-weight: 400;"">一 、 &nbsp;</span><strong>平時勤做筆記，利用零碎時間反覆翻閱</strong></h4>
<p><span style=""font-weight: 400;""> （一）、建立萬用主題和屬於該主題的諺語：</span></p>
<ol>
<li><em> <span style=""color: #ff6666;"">依據全真模擬試題（TPO）歸類出數種萬用主題</span></em>
<p><span style=""font-weight: 400;""> &nbsp;&nbsp;例如: （1） learning a lot 學習知識</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;（2） making friends 人際關係</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;（3） being healthy &nbsp;保健</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;（4） making money 創造財富</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;（5） saving time省時或時間管理</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;（6） being environmentally-friendly 環保</span><span style=""font-weight: 400;""><br /></span><span style=""font-weight: 400;"">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;（7） being happy &nbsp;讓自己開心</span></p>
</li>
<li><em> <span style=""color: #ff6666;"">利用COCA資料庫記下相關語句</span></em>
<p><a href=""http://corpus.byu.edu/coca/""><strong>美國當代英語與料庫（Corpus of Contemporary American </strong><span style=""font-weight: 400;"">&nbsp;</span><strong>English，COCA</strong></a><span style=""font-weight: 400;"">。<br />例如:輸入healthy」會出現多種語句 emotional healthy、</span><span style=""font-weight: 400;"">in building a </span><strong>healthy</strong><span style=""font-weight: 400;""> business climate &ldquo;...</span><span style=""font-weight: 400;"">，捕捉符合「醫療保健」這個主題的片語，寫在筆記上，時常翻閱，就不怕考試時沒梗可用。</span><span style=""font-weight: 400;"">筆記可以是用一本或活頁紙，也有人在洗手間貼滿防水文件套，用上廁所時間複誦的筆記法，或利用等公車，搭捷運時讀筆記，都是熟練的方法。</span></p>
</li>
<li><em><span style=""color: #ff6666;"">表達順序或轉折的慣用語</span><br /><a href=""http://www.idiomconnection.com/mostfrequent.html""><span style=""font-weight: 400;"">http://www.idiomconnection.com/mostfrequent.html</span></a><span style=""font-weight: 400;""><br /> 例如：輸入a開頭，會有according to，</span><span style=""font-weight: 400;"">account for</span><strong>&hellip;</strong><span style=""font-weight: 400;"">這些慣用語句請寫入筆記多重複背誦。</span> </em></li>
</ol>
<h4>&nbsp;</h4>
<h4><span style=""font-weight: 400;"">二、 &nbsp;多看多聽英語文化圈的影片</span></h4>
<p><span style=""font-weight: 400;"">網路學習時代要得到英語資訊不難，讓英文學習者有興趣的Youtuber 或英美電視劇都可學到口語上正式又好用的句子，不妨從此著手，並抄錄關鍵句子在筆記，將這句子歸納在各個主題，考試時不怕詞窮！例如:訂閱量超過10萬人，針對學習英語拍攝的Youtuber「mmmEnglish」就針對英語非母語人士常發錯的口音，可糾正自己口說時的錯誤或是對政治權謀型的當紅美劇「紙牌屋」有興趣者，可以在劇中找到許多名言美句，按暫停句抄下來就對了！</span></p>
<h4><a href=""https://www.youtube.com/channel/UCrRiVfHqBIIvSgKmgnSY66g""><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a881c1d9cedf-on0dqG1TG4a3a0SAoqdNUOcIhV8XIY-1982x548.png"" alt="""" width=""100%"" height=""200"" /></span></a></h4>
<h4>&nbsp;</h4>
<h4><span style=""font-weight: 400;"">三 、考前3個月勤做模擬考題</span></h4>
<p><span style=""font-weight: 400;"">TPO 模擬試題跟現場考題接近度很高，考試不外乎準備好切中命題的素材 + 熟練，所以切勿考前幾周才練習模擬試題，平常排好讀書計畫，多練習，練得熟+筆記背熟在考場才能不慌亂。</span></p>
<p><span style=""font-weight: 400;"">看完以上建議，做筆記不外乎切中考題，平時備戰，切莫臨時抱佛腳。做好這些培養英語力的語感和素材磨練，口說考高分就不難！</span></p>
</div>"','published' => '1','og_title' => '考口說總是很緊張所以常失分？托福口說準備和攻略總整理！','og_description' => '托福考試已經不容易，其中口說更是亞洲學生的痛點，主要是因非母語的生活環境，少用英語思考，短期內要有英語環境不容易，但是短期內準備好托福口說卻不難，先從自己有興趣能讀進去，願意做筆記的資訊開始。','meta_title' => '考口說總是很緊張所以常失分？托福口說準備和攻略總整理！','meta_description' => '托福考試已經不容易，其中口說更是亞洲學生的痛點，主要是因非母語的生活環境，少用英語思考，短期內要有英語環境不容易，但是短期內準備好托福口說卻不難，先從自己有興趣能讀進去，願意做筆記的資訊開始。','canonical_url' => 'https://tw.english.agency/toefl-speak','feature_image_alt' => '考口說總是很緊張所以常失分？托福口說準備和攻略總整理！','feature_image_title' => '考口說總是很緊張所以常失分？托福口說準備和攻略總整理！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a881e99d0eaa-s0Yjj5EQcXWsFVmvcIEb5xwlZEWSJt-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-17 12:15:37"'],
  ['id' => '150','title' => '抓不到托福口說準備重點？10題最常見的口說問題公開！','slug' => 'toefl-10question','excerpt' => '對於美語非母語的台灣學生來說，應考托福最難準備的則是口說，因為無論被多少單字文法或是應考模板，在考試當下可能會被考題、現場環境及個人狀態等影響考試表現。而與寫作、聽力及閱讀等考試相比，最大挑戰則是以正確的英語會話說出答案，主要是因英語會話的思考邏輯與脈絡與中文相比有所不同，對於自出生以來習慣以中文的文法及語氣說話在準備上會是一大挑戰。','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p><span style=""font-weight: 400;"">對於美語非母語的台灣學生來說，應考托福最難準備的則是口說，因為無論被多少單字文法或是應考模板，在考試當下可能會被考題、現場環境及個人狀態等影響考試表現。而與寫作、聽力及閱讀等考試相比，最大挑戰則是以正確的英語會話說出答案，主要是因英語會話的思考邏輯與脈絡與中文相比有所不同，對於自出生以來習慣以中文的文法及語氣說話在準備上會是一大挑戰。</span></p>
<p>&nbsp;</p>
<h2><span style=""font-weight: 400;"">TOEFL口說兩大考題類型：獨立型及整合型</span></h2>
<p><span style=""font-weight: 400;"">TOEFL口說主要是測驗應考者在全英語環境下的口語表達能力，以此作為評量應考者英文會話水準是否可在英語／美語系國家就讀，了解課堂上教授的課程內容以及是否有能立在這樣的環境下生活。因此，考題內容會以校園內遇到的真實情境出題，包括學術、個人經驗及生活情境等主題。</span></p>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a882da8cb8ef-Rov3pDdyuh3lMobVJZxFnJFxVcMYWm-2918x1668.png"" alt="""" width=""100%"" height=""400"" /></span></p>
<p><span style=""font-weight: 400;"">口說測驗提供總共6題，第一及二題為「獨立型（Independent）考題」，第三至第四題為「整合型（Integrated）考題」，簡單來說考題方向為：</span></p>
<ul>
<li><span style=""color: #ff751a;"">第1-2題：</span>獨立型考題，以個人日常生活經驗為主，敘述自己的喜好或觀點。</li>
<ul>
<li>第1題：熟悉的事物，如你喜歡的休閒活動、最喜歡的書。</li>
<li>第2題：意見二選一，如贊成或反對學生實習。</li>
<li>第1-2題：電腦朗讀完題目後，有15秒的準備回答時間。準備結束後，有45秒可以回答。</li>
</ul>
<li><span style=""color: #ff751a;"">第3-4題：</span>整合型考題，有45秒時間閱讀一篇文章，時間到後文章自動消失，後續則會接著聽力內容。聽力結束後，有30秒時間可以準備答題內容，最後則有60秒時間回答問題。</li>
<li><span style=""color: #ff751a;"">第5-6題：</span>整合型考題，題目內容以聽力為主，先會聽到1分鐘的對話及2分鐘的講課內容。對話結束後，有20秒時間可以準備答題方向，並有60秒時間可以作答。</li>
</ul>
<p><span style=""font-weight: 400;"">口說測驗的答題時間都有限制的準備及答題時間，建議應考生可邊瀏覽／聽考題時，記下問題的重點以利答題。同時也可以在準備答題時，寫下回答的方向及關鍵字，答題時並注意時間的掌控。</span></p>
<p>&nbsp;</p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a882e59accb9-ALlEqY5CmL62SiXScPE3V09QBbBJFi-800x450.jpeg"" alt="""" width=""100%"" height=""300"" /><br /><br /></p>
<h2><span style=""font-weight: 400;"">TOEFL口說考題大彙整</span></h2>
<p style=""color: #ff6666;""><strong>第一題問題範例：</strong></p>
<ol>
<li><span style=""font-weight: 400;""> What is the most important decision that you have made in your life? Why is it important to you? Use specific examples and details to support your explanation.</span></li>
<li>Talk about a social or political celebration event in your culture that you think is special. What is special about it? Specific examples and details are necessary in your answer.</li>
<li><span style=""font-weight: 400;""> What place do you like best in your city? Give specific examples and details to explain your answer. </span></li>
<li><span style=""font-weight: 400;""> Talk about one of the most important inventions that appeared in the past 100 years. Explain why it is so important. Your explanation should include specific examples and details. </span></li>
<li><span style=""font-weight: 400;""> What do you think are the characteristics of a good friend? Use details and examples to explain your answer. </span></li>
</ol>
<p><br /><br /></p>
<p style=""color: #ff6666;""><strong>第二題問題範例：</strong></p>
<ol>
<li><span style=""font-weight: 400;""> Some students prefer to live alone. Others want to share their rooms with their roommates. Which way of living do you think is better for students and why? </span></li>
<li><span style=""font-weight: 400;""> When you are together with your friends，which place do you prefer to eat，the restaurant，caf&eacute; or at home? Use specific examples to support your statement. </span></li>
<li><span style=""font-weight: 400;""> Some people believe that the TV，newspaper and radio have more influence on individuals than his or her relatives and friends. Others believe that relatives and friends have more influence on people than do the media. Which statement do you prefer? Please give your opinion with specific examples and details. </span></li>
<li><span style=""font-weight: 400;""> Do you agree or disagree with the following statement? Use specific details and examples to explain your opinion. The most important lesson cannot be learned in class. </span></li>
<li><span style=""font-weight: 400;""> Do you agree or disagree with the following statement? Use specific details and examples to explain your opinion. University education should be free. </span></li>
</ol>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">托福口說準備就如其他考試相同，考前絕對沒有100%正確的猜題，或是考得滿分的準備方法。應考者需要的是耐心及持續練習，像是利用錄音和朋友互相問答，掌握英文表達及申論方式，了解口說的弱點進而改善，提升語感。此外，也可以尋求專業指導及練習，如線上英語課程、短期口說課程，又或者是與英語家就、專業師資進行口說練習，透過模擬方式訓練及加強臨場反應，不要讓自己在考試現場措手不及。</span></p>
</div>"','published' => '1','og_title' => '抓不到托福口說準備重點？10題最常見的口說問題公開！','og_description' => '對於美語非母語的台灣學生來說，應考托福最難準備的則是口說，因為無論被多少單字文法或是應考模板，在考試當下可能會被考題、現場環境及個人狀態等影響考試表現。而與寫作、聽力及閱讀等考試相比，最大挑戰則是以正確的英語會話說出答案，主要是因英語會話的思考邏輯與脈絡與中文相比有所不同，對於自出生以來習慣以中文的文法及語氣說話在準備上會是一大挑戰。','meta_title' => '抓不到托福口說準備重點？10題最常見的口說問題公開！','meta_description' => '對於美語非母語的台灣學生來說，應考托福最難準備的則是口說，因為無論被多少單字文法或是應考模板，在考試當下可能會被考題、現場環境及個人狀態等影響考試表現。而與寫作、聽力及閱讀等考試相比，最大挑戰則是以正確的英語會話說出答案，主要是因英語會話的思考邏輯與脈絡與中文相比有所不同，對於自出生以來習慣以中文的文法及語氣說話在準備上會是一大挑戰。','canonical_url' => 'https://tw.english.agency/toefl-10question','feature_image_alt' => '抓不到托福口說準備重點？10題最常見的口說問題公開！','feature_image_title' => '抓不到托福口說準備重點？10題最常見的口說問題公開！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8832104fa11-2wsPOKIF78pj3xfmVKOwNuaE9W94kP-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-17 12:50:21"'],
  ['id' => '151','title' => '3分鐘了解寫作題型和架構！稱霸托福考場！','slug' => 'toefl-3min','excerpt' => '經過漫長的托福閱讀、聽力和口說測驗，最後要面對的就是寫作的關卡了！被放在最後的托福寫作不但能清楚測出考生的表達能力，同樣也考驗考生在考場上的耐心和毅力，因此若是想要沉著應戰，就必須先了解托福寫作的大小事，讓自己的心情穩定下來。','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<h2><strong>摸透托福寫作資訊，增強你的應試平常心</strong></h2>
<p><span style=""font-weight: 400;"">經過漫長的托福閱讀、聽力和口說測驗，最後要面對的就是寫作的關卡了！被放在最後的托福寫作不但能清楚測出考生的表達能力，同樣也考驗考生在考場上的耐心和毅力，因此若是想要沉著應戰，就必須先了解托福寫作的大小事，讓自己的心情穩定下來。對寫作測驗還抱有一絲疑惑的你，接下來的托福寫作資訊即將為你一一解惑，讓你不再感到茫然，面對漫長的考試時間也能冷靜應對！</span></p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8844e479714-RhQCpVt6TPu4qb73zRpC5Z6iUHMJKH-700x400.png"" alt="""" width=""700"" height=""400"" /></p>
<ul>
<li>題數：第一題是<span style=""font-weight: 400;"">Intergrated</span><span style=""font-weight: 400;"">（整合寫作題），第二題是</span><span style=""font-weight: 400;"">Independent</span><span style=""font-weight: 400;"">（獨立寫作題），總計2題。</span></li>
<li>作答時間：整合寫作題有20分鐘，獨立寫作題有30分鐘，總計50分鐘。</li>
<li>文章長度：整合寫作題的建議字數為150~225字，獨立寫作題的建議字數為300字以上。</li>
<li>題型：<br /><span style=""color: #ff6666;"">Intergrated（整合寫作題）</span>會先給考生一篇學術性文章，閱讀時間為<span style=""font-weight: 400;"">3</span><span style=""font-weight: 400;"">分鐘，之後再聽一段約</span><span style=""font-weight: 400;"">2</span><span style=""font-weight: 400;"">分鐘的教授演講，演講內容大多會反駁文章的論點，考生必須整合資訊並寫出文章和教授的見解。<br /><span style=""color: #ff6666;"">Independent（獨立寫作題）</span>必須針對螢幕上出現的題目去撰寫文章，考生們可以根據個人的經驗和看法進行寫作。</span></li>
</ul>
<h2><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8846788341c-ykb7v9LKuHKwWrAOViWYQuN8ooLsbW-700x400.png"" alt="""" width=""700"" height=""400"" /></strong></h2>
<h2><strong>深入了解得分重點，讓你握有寫作的致勝關鍵</strong></h2>
<p><span style=""font-weight: 400;"">前述的測驗資訊想必讓你對托福寫作有基本的概念，不過光是如此仍無法考取高分，考生必須進一步了解寫作測驗的得分重點才能在答題時直達核心，獲得評審的賞識。因此統整出托福寫作的準備方法和評分標準，讓考生知道可以從哪方面著手準備考試，不再白忙一場。</span></p>
<h4><span style=""color: #ff751a;"">如何準備：</span></h4>
<p><span style=""font-weight: 400;"">托福寫作不需要使用艱深的單字和文法，只要可以明確表達自己的意思就能確保得分。若是不清楚常用的句型有哪些，可以參考市面上的寫作練習書，多背一些寫作時可以運用的固定句型，並且多做</span><span style=""font-weight: 400;"">TPO</span><span style=""font-weight: 400;"">的模擬考題來累積實際的寫作經驗。</span></p>
<h4><span style=""color: #ff751a;"">評分標準：</span></h4>
<p style=""color: #ff6666;""><strong>一、</strong><strong>Intergrated</strong><strong>（整合寫作題）</strong><strong>:</strong></p>
<p><span style=""font-weight: 400;"">著重在考生獲得的資訊量和思考邏輯，只要在閱讀文章和聽教授演講的時候有做足夠的筆記，並且整合資訊後能清楚表達他們的論點即可。</span></p>
<p style=""color: #ff6666;""><strong>二、</strong><strong>Independent</strong><strong>（獨立寫作題）</strong><strong>:</strong></p>
<p><span style=""font-weight: 400;"">A.</span><span style=""font-weight: 400;"">文章組織能力佳，能使文章順暢閱讀。</span></p>
<p><span style=""font-weight: 400;"">B.</span><span style=""font-weight: 400;"">可以列出具體的例子和細節來支持自己的論點。</span></p>
<p><span style=""font-weight: 400;"">C.</span><span style=""font-weight: 400;"">可以有效地延伸主題，不離題。</span></p>
<p><span style=""font-weight: 400;"">D.</span><span style=""font-weight: 400;"">能透過用詞遣字來表達對語言的熟悉程度。</span></p>
</div>"','published' => '1','og_title' => '3分鐘了解寫作題型和架構！稱霸托福考場！','og_description' => '經過漫長的托福閱讀、聽力和口說測驗，最後要面對的就是寫作的關卡了！被放在最後的托福寫作不但能清楚測出考生的表達能力，同樣也考驗考生在考場上的耐心和毅力，因此若是想要沉著應戰，就必須先了解托福寫作的大小事，讓自己的心情穩定下來。','meta_title' => '3分鐘了解寫作題型和架構！稱霸托福考場！','meta_description' => '經過漫長的托福閱讀、聽力和口說測驗，最後要面對的就是寫作的關卡了！被放在最後的托福寫作不但能清楚測出考生的表達能力，同樣也考驗考生在考場上的耐心和毅力，因此若是想要沉著應戰，就必須先了解托福寫作的大小事，讓自己的心情穩定下來。','canonical_url' => 'https://tw.english.agency/toefl-3min','feature_image_alt' => '3分鐘了解寫作題型和架構！稱霸托福考場！','feature_image_title' => '3分鐘了解寫作題型和架構！稱霸托福考場！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8847843a9bb-DrVSzCtfUhpebOc6lyX9EfeZ0yohDx-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-17 14:02:03"'],
  ['id' => '152','title' => '你聽過比利時音樂節嗎？盤點那些最炫最夯的世界各國特殊節慶！','slug' => 'world-special-festival','excerpt' => '除了感恩節和復活節這些對歐美人士來說很重要的節慶以外，世界各地也都有很多台灣沒有的節日、慶典，今天就來帶大家看看幾個世界各地的特殊節慶，並學習相關的節慶英文單字。','content' => '"<p style=""font-size: 16px;"">除了感恩節和復活節這些對歐美人士來說很重要的節慶以外，世界各地也都有很多台灣沒有的節日、慶典，今天就來帶大家看看幾個世界各地的特殊節慶，並學習相關的節慶英文單字。</p>
<p style=""font-size: 20px; color: #880000;""><strong>Tomorrowland 比利時電子音樂節</strong></p>
<p style=""font-size: 20px; color: #880000;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a884acae9b17-Qr2ILT2o48P7bm09v4yOHZE09twspQ-1024x682.jpeg"" alt="""" width=""1024"" height=""682"" /></strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">Tomorrow land 是世界最大的電子音樂節之一，從2005 年創辦至今，每年都會有很多電音愛好者朝聖</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">【延伸】</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Electronic Music</strong><span style=""font-weight: 400;""> 電音</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>EDM = Electronic Dance Music</strong><span style=""font-weight: 400;""> 電音舞曲</span>&nbsp;</p>
<p style=""font-size: 16px; color: #003377;""><strong>Electronic <span style=""font-weight: 400;"">vs. </span>Electric<span style=""font-weight: 400;""> vs. </span>Electrical</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">這三個詞在字典裡的解釋幾乎一模一樣，都是「電動的」意思，但他們的分別在於：</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Electronic</strong><span style=""font-weight: 400;""> 比較廣泛，是指用電力發動、稍微複雜、需要經過處理的東西</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">例如 </span><strong>Electronic books</strong><span style=""font-weight: 400;"">電子書</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">Electronic加個s = </span><strong>Electronics</strong><span style=""font-weight: 400;""> 也用來統稱電子產品</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Electric</strong><span style=""font-weight: 400;""> 是「以電力發動的」 或是 「帶電的」、「和電有關的」，但沒那麼複雜的裝置</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">例如 </span><strong>Electric Guitar</strong><span style=""font-weight: 400;""> 電吉他</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Electrica</strong><span style=""font-weight: 400;"">l</span><span style=""font-weight: 400;"">和 Electric 通用，不過Electrical 更常用為「電力學的」、和電路管線相關的</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">像是 </span><strong>Electrical Engineering </strong><span style=""font-weight: 400;"">電機工程</span></p>
<p style=""font-size: 20px; color: #880000;""><strong>Merfest 北卡羅萊納州美人魚節</strong></p>
<p style=""font-size: 20px; color: #880000;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a884b9168b3a-SaogpYRs1wR5VXAvWEtASRQg5dPxSC-720x360.jpeg"" alt="""" width=""720"" height=""360"" /></strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">北卡羅來納州每年會舉辦美人魚節，超過三百隻的&ldquo;男人魚&rdquo;和&ldquo;美人魚&rdquo;聚集一堂，精心打扮成人魚</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">【延伸】</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Mermaid</strong><span style=""font-weight: 400;""> 美人魚</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Jellyfish</strong><span style=""font-weight: 400;""> 水母</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Scallop</strong><span style=""font-weight: 400;""> 干貝</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Coral reef</strong><span style=""font-weight: 400;""> 珊瑚礁</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Atoll</strong><span style=""font-weight: 400;""> 環礁</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Coast</strong><span style=""font-weight: 400;""> 海岸</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Coastline</strong><span style=""font-weight: 400;""> 海岸線</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Underwater coastal city</strong><span style=""font-weight: 400;""> 低於海平面的沿岸城市</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Marine Conservation</strong><span style=""font-weight: 400;""> 海洋保育</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Eco-friendly</strong><span style=""font-weight: 400;""> 對環境友善的</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>【例句】</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">There are many things you can do to save the oceans. For example，you can use less fertilizer，buy ocean-friendly products，use reusable plastic products and eat sustainable seafood</span><strong>.</strong></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">你可以做很多事來拯救海洋。例如 可以使用較少的肥料</span></p>
<p style=""font-size: 20px; color: #880000;""><strong>Holi Festival 印度色彩節</strong></p>
<p style=""font-size: 20px; color: #880000;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a884ca224e9f-6vp9ArHM09Ol5NQ4G1pQxZXH0rlB7r-760x460.jpeg"" alt="""" width=""760"" height=""460"" /></strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">印度人每年到了三月份的色彩節，人們都會將各種顏色的顏料、粉末像彼此身上揮灑。其實這節日象徵的是色彩單調的冬天已經結束，迎接春天這個充滿顏色、希望的季節的到來</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">【延伸】 </span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">一些少見的英文顏色整理：</span></p>
<p style=""font-size: 16px; color: #009fcc;""><strong>azure</strong><span style=""font-weight: 400;""> 天藍色</span></p>
<p style=""font-size: 16px; color: #9900ff;""><strong>blueviolet </strong><span style=""font-weight: 400;"">紫羅蘭色</span></p>
<p style=""font-size: 16px; color: #aa7700;""><strong>linen</strong><span style=""font-weight: 400;""> 亞麻色</span></p>
<p style=""font-size: 16px; color: #668800;""><strong>olive green</strong><span style=""font-weight: 400;""> 橄欖綠</span></p>
<p style=""font-size: 16px; color: #cc0000;""><strong>scarlet</strong><span style=""font-weight: 400;""> 緋紅</span></p>
<p style=""font-size: 16px; color: #888888;""><strong>misty gray</strong><span style=""font-weight: 400;""> 霧灰色</span></p>
<p style=""font-size: 16px; color: #008866;""><strong>aquamarine blue</strong><span style=""font-weight: 400;""> 藍綠色</span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">＃顏色有關的片語整理</span></p>
<p style=""font-size: 16px; color: #880000;""><strong>The grass is always greener than the other side of the fence. </strong></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">指的是我們常常羨慕別人擁有的、覺得別人的比較好，類似於中文中的</span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">「外國的月亮比較圓」</span></p>
<p style=""font-size: 16px; color: #880000;""><strong>Once in the blue moon</strong></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">非常罕見的意思，近於</span><strong>hardly ever，rarely，seldom，almost never </strong></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">Since I live abroad，I get to see my family once in the blue moon.</span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">因為住在國外，我非常少見到我的家人。</span></p>
<p style=""font-size: 16px; color: #880000;""><strong>Green with envy</strong></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">「眼紅」的英文不是red-eyed，而是 </span><strong>green-eyed</strong><span style=""font-weight: 400;"">. 而Green with envy 是指嫉妒得要死的意思。</span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">I was green with envy when Jacky won the lottery. </span></p>
<p style=""font-size: 16px; color: #880000;""><span style=""font-weight: 400;"">當Jacky贏得樂透時，我嫉妒得要命！</span></p>
<p style=""font-size: 20px; color: #880000;""><strong>Oktoberfest 慕尼黑啤酒節</strong></p>
<p style=""font-size: 20px; color: #880000;""><strong><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a884c91b52aa-8YEH5qCRy9z0j6hux0hq9t2ewJK71k-507x338.jpeg"" alt="""" width=""507"" height=""338"" /></strong></p>
<p style=""font-size: 16px;""><span style=""font-weight: 400;"">這個應該比較多人聽說過了！ 慕尼黑啤酒節現在已經是全世界最重要的節慶(World&rsquo;s largest annual fair) 之一，節慶持續兩週，現在已經轉型為嘉年華會了</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">【延伸】</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Carnival</strong><span style=""font-weight: 400;""> 嘉年華會</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">喝酒相關的英文片語</span></p>
<p style=""font-size: 16px; color: #003377;""><span style=""font-weight: 400;"">酒類：</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Beer</strong><span style=""font-weight: 400;""> 啤酒</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Draft beer</strong><span style=""font-weight: 400;""> 生啤酒</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Vodka</strong><span style=""font-weight: 400;""> 伏特加</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Tequila</strong><span style=""font-weight: 400;""> 龍舌蘭</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Cocktails</strong><strong>Cheers</strong><span style=""font-weight: 400;"">/ </span><strong>Bottoms up</strong><span style=""font-weight: 400;"">乾杯</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Tipsy</strong><span style=""font-weight: 400;""> 微醺</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Drunk</strong><span style=""font-weight: 400;"">/ </span><strong>wasted</strong><span style=""font-weight: 400;"">/ </span><strong>pissed</strong><span style=""font-weight: 400;"">/ </span><strong>hammered</strong><span style=""font-weight: 400;"">/ </span><strong>plastered</strong><span style=""font-weight: 400;"">/ </span><strong>smashed</strong><span style=""font-weight: 400;""> 喝醉的</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>On the rocks </strong><span style=""font-weight: 400;"">加冰塊</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Neat</strong><span style=""font-weight: 400;""> 不加冰塊</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Have a hangover</strong><span style=""font-weight: 400;""> 宿醉</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Keep it open</strong><span style=""font-weight: 400;""> 這是一句常跟bartender說的，意思是先不結帳、等等繼續點</span></p>
<p style=""font-size: 16px; color: #003377;""><strong>Asian flush/ Asian glow</strong><span style=""font-weight: 400;""> 喝酒後臉紅的反應，常見於亞洲人中</span></p>"','published' => '1','og_title' => '你聽過比利時音樂節嗎？盤點那些最炫最夯的世界各國特殊節慶！','og_description' => '除了感恩節和復活節這些對歐美人士來說很重要的節慶以外，世界各地也都有很多台灣沒有的節日、慶典，今天就來帶大家看看幾個世界各地的特殊節慶，並學習相關的節慶英文單字。','meta_title' => '你聽過比利時音樂節嗎？盤點那些最炫最夯的世界各國特殊節慶！','meta_description' => '除了感恩節和復活節這些對歐美人士來說很重要的節慶以外，世界各地也都有很多台灣沒有的節日、慶典，今天就來帶大家看看幾個世界各地的特殊節慶，並學習相關的節慶英文單字。','canonical_url' => 'https://tw.english.agency/world-special-festival','feature_image_alt' => '你聽過比利時音樂節嗎？盤點那些最炫最夯的世界各國特殊節慶！','feature_image_title' => '你聽過比利時音樂節嗎？盤點那些最炫最夯的世界各國特殊節慶！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a884d899fd5a-ePk0ZuJ9qmiYdhksfOaV788OApyVTL-724x483.jpeg','author_id' => '1','category_id' => '10','created_at' => '"2018-02-17 15:39:30"'],
  ['id' => '153','title' => '"托福寫作真的很難拿高分嗎？  破除寫作4大迷思，高分不再遙不可及！"','slug' => 'toefl-ibt-4myth','excerpt' => 'TOEFL分為聽力、閱讀、口說及寫作，而對於沒有長期英語寫作經驗的台灣考生來說，更是一大挑戰，無論是在文法使用、單字挑選及文章架構等都需要反覆練習。而在網路上流傳四大寫作高分訣竅（？），包括字數越多分數越高、正規英語五段寫作、套用模板、用艱澀的單字。','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">TOEFL分為聽力、閱讀、口說及寫作，而對於沒有長期英語寫作經驗的台灣考生來說，更是一大挑戰，無論是在文法使用、單字挑選及文章架構等都需要反覆練習。而在網路上流傳四大寫作高分訣竅（？），包括字數越多分數越高、正規英語五段寫作、套用模板、用艱澀的單字。</span></p>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8857d7c0bdc-ee0DIRqItRLEK50QbOcATH6STj6bdn-700x400.png"" alt="""" width=""700"" height=""400"" /></span></p>
<ol>
<li><span style=""color: #ff6666;""> 字數多＝高分？</span>
<p><span style=""font-weight: 400;"">傳說「打越多字越好、字數不足會被扣分」，其實EST評分標準中，字數多寡並不是評分主因，ETS提供整合型題目150字及獨立型300字的建議字數，是要考生在規定的字數下，清楚回答觀點。文章以清楚結構完整的闡述觀點，回答問題核心才是關鍵！</span></p>
</li>
</ol>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8857ef912a3-XYwIvFyNuVVnSgGqGZ3EDUkfFzbUzc-700x400.png"" alt="""" width=""700"" height=""400"" /></span></p>
<ol start=""2"">
<li><span style=""color: #ff6666;""> 標準英語寫作五段分法</span>
<p><span style=""font-weight: 400;"">一般來說，英文寫作中會以五段：一段開頭簡述三個論點，三段說明三個論點段落及一段結論，多數考生會認為以正規寫法分數會比較高。但EST會以文章段落的分段是否合理來評分。所以答題時，應要注意寫作的觀點在恰當的分段中是否合理。</span></p>
</li>
</ol>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8857ffafbdb-Qt4lsEGNQ4fzQloPkkEJ4PbEJZ0h7W-700x400.png"" alt="""" width=""700"" height=""400"" /></span></p>
<ol start=""3"">
<li><span style=""color: #ff6666;""> 套用模板得滿分？</span>
<p><span style=""font-weight: 400;"">準備寫作測驗時，許多考生喜歡背下模板，套入不同題目回答。但TOEFL寫作考題設計是要了解考生對於題目的理解及寫作能力。模板看起來是速成法，但寫作方式則會被侷限住，而非以練習出英文寫作的關鍵。建議可以參考模板的回答方向，但不要被模板給限制住，學會活用！</span></p>
</li>
</ol>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8858112fdad-61FGYlyqWPkDOS9A9bWXsiAggXRwhQ-700x400.png"" alt="""" width=""700"" height=""400"" /></span></p>
<ol start=""4"">
<li><span style=""color: #ff6666;""> 艱深的單字分數越高？</span>
<p><span style=""font-weight: 400;"">翻開TOEFL必備單字的參考書時，從A到Z的單字可能會有非常艱澀的單字，主要是因為TOEFL為學術性測驗，閱讀時可能會碰到這些單字，但沒有保證一定會出現，就會讓考生覺得寫作時也要放上這些艱難的單字，才有高分的機會。但回想TOEFL測驗的本質是想了解考生在全英語環境求學及生活時，是否有能力應對，而非要考生寫出一輩子可能才看過一次的單字。而在背5個這樣的單字時，可能也練習完1篇作文，浪費準備的時間。建議寫作應運用合理且熟悉的單字融會貫通整篇文章，才能寫出完整的文章，同時也可避免拼錯字的窘境。</span></p>
</li>
</ol>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">在練習文章寫作之餘，也要練習英打，因為TOEFL iBT是以電腦進行測驗，若不熟悉英打對於考生來說是件吃虧的要素。同時多閱讀英文新聞及文章訓練觀點闡述及邏輯思考能力，以呈現更完美的答案。</span></p>
</div>"','published' => '1','og_title' => '"托福寫作真的很難拿高分嗎？  破除寫作4大迷思，高分不再遙不可及！"','og_description' => 'TOEFL分為聽力、閱讀、口說及寫作，而對於沒有長期英語寫作經驗的台灣考生來說，更是一大挑戰，無論是在文法使用、單字挑選及文章架構等都需要反覆練習。而在網路上流傳四大寫作高分訣竅（？），包括字數越多分數越高、正規英語五段寫作、套用模板、用艱澀的單字。','meta_title' => '"托福寫作真的很難拿高分嗎？  破除寫作4大迷思，高分不再遙不可及！"','meta_description' => 'TOEFL分為聽力、閱讀、口說及寫作，而對於沒有長期英語寫作經驗的台灣考生來說，更是一大挑戰，無論是在文法使用、單字挑選及文章架構等都需要反覆練習。而在網路上流傳四大寫作高分訣竅（？），包括字數越多分數越高、正規英語五段寫作、套用模板、用艱澀的單字。','canonical_url' => 'https://tw.english.agency/toefl-ibt-4myth','feature_image_alt' => '"托福寫作真的很難拿高分嗎？  破除寫作四大迷思，高分不再遙不可及！"','feature_image_title' => '"托福寫作真的很難拿高分嗎？  破除寫作四大迷思，高分不再遙不可及！"','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8859f638368-dELVrUfHUwkZ3JPTYzzmHJxtIQmDH2-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-17 16:36:15"'],
  ['id' => '154','title' => '只要3分鐘！帶你了解托福閱讀，提高答題正確率！','slug' => 'toefl-reading','excerpt' => '想要在托福(TOEFL)中考取高分，首先要克服的就是閱讀關卡，考題多樣性高、篇幅繁多的閱讀測驗是不少考生的噩夢。然而你對托福閱讀了解得夠透徹嗎？是否已經準備好接受閱讀測驗的考驗了呢？今篇不只是統整了詳盡的托福閱讀資訊，還會分享答題時可使用的小技巧，讓你更能掌握托福閱讀的整體結構，在考場上也能信心滿滿地突破第一道關卡！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p><span style=""font-weight: 400;"">想要在托福</span><span style=""font-weight: 400;"">(TOEFL)</span><span style=""font-weight: 400;"">中考取高分，首先要克服的就是閱讀關卡，考題多樣性高、篇幅繁多的閱讀測驗是不少考生的噩夢。然而你對托福閱讀了解得夠透徹嗎？是否已經準備好接受閱讀測驗的考驗了呢？今篇不只是統整了詳盡的托福閱讀資訊，還會分享答題時可使用的小技巧，讓你更能掌握托福閱讀的整體結構，在考場上也能信心滿滿地突破第一道關卡！</span></p>
<h2>托福閱讀題型介紹</h2>
<ul>
<li>
<p><span style=""font-weight: 400;"">題數：一組閱讀測驗的題數有</span><span style=""font-weight: 400;"">12~14</span><span style=""font-weight: 400;"">題，基本上有三組閱讀測驗，總計約</span><span style=""font-weight: 400;"">36~56</span><span style=""font-weight: 400;"">題，遇到加試題時總共會有五組閱讀測驗。</span></p>
</li>
<li>
<p><span style=""font-weight: 400;"">作答時間：一組約</span><span style=""font-weight: 400;"">20</span><span style=""font-weight: 400;"">分鐘，總計約</span><span style=""font-weight: 400;"">60~100</span><span style=""font-weight: 400;"">分鐘。</span></p>
</li>
<li>
<p><span style=""font-weight: 400;"">文章長度：每一篇閱讀測驗的篇幅大約在</span><span style=""font-weight: 400;"">700~800</span><span style=""font-weight: 400;"">字之間，若是一頁</span><span style=""font-weight: 400;"">A4</span><span style=""font-weight: 400;"">紙寫滿文字大約是</span><span style=""font-weight: 400;"">400</span><span style=""font-weight: 400;"">字，那麼一篇閱讀測驗的文章長度大約是兩面</span><span style=""font-weight: 400;"">A4</span><span style=""font-weight: 400;"">紙的文字量。</span></p>
</li>
<li>
<p><span style=""font-weight: 400;"">十大題型：</span><span style=""font-weight: 400;"">Vocabulary(</span><span style=""font-weight: 400;"">單字題</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Reference(</span><span style=""font-weight: 400;"">猜代名詞對象</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Rhetorical Purpose(</span><span style=""font-weight: 400;"">猜句子功能</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Factual Information(</span><span style=""font-weight: 400;"">問題目的正確選項</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Negative Factual Information</span><span style=""font-weight: 400;"">(</span><span style=""font-weight: 400;"">問題目的錯誤選項</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Inference(</span><span style=""font-weight: 400;"">句子推敲</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Sentence Simplification(</span><span style=""font-weight: 400;"">簡化句子含意</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Insert Text(</span><span style=""font-weight: 400;"">句子填空</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Prose Summary(</span><span style=""font-weight: 400;"">主旨</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">、</span><span style=""font-weight: 400;"">Fill Table(</span><span style=""font-weight: 400;"">配對</span><span style=""font-weight: 400;"">)</span><span style=""font-weight: 400;"">。</span></p>
</li>
<li>
<p><span style=""font-weight: 400;"">考題類型：托福閱讀並不是朝日常生活的方向出題，大多是較學術性的文章內容，例如生物、歷史、人文、科學都有可能是文章主題，題材較多元。</span></p>
</li>
<li>
<p><span style=""font-weight: 400;"">如何準備：雖然是較專業的文章類型，不過測驗的並不是考生的專業知識，而是思考邏輯和理解能力。因此平時可以多閱讀學術性文章、做</span><a href=""http://www.toefl.com.tw/tpo/""><span style=""font-weight: 400;"">TPO</span></a><span style=""font-weight: 400;""><a href=""http://www.toefl.com.tw/tpo/"">的模擬考題</a>多了解各領域的字彙。</span></p>
</li>
</ul>
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a898fa7a4c7a-be9e3ORlP1aGkF9RxLJpHUYDqCyIix-1200x630.png"" alt="""" width=""100%"" height=""360"" /></p>
<h2><strong>五個解題小技巧，提升你的答題精準度</strong></h2>
<p><span style=""font-weight: 400;"">儘管了解前述的托福閱讀資訊，考生也要有自己的答題訣竅才能提升正式測驗時的作答速度和正確率。因此除了平常勤做托福練習題之外，透過以下的解題技巧也有助於考生調整自己的作答速度和節奏：</span></p>
<p style=""color: #ff6666;""><strong>一、要注意每一段的開頭句</strong></p>
<p><span style=""font-weight: 400;"">通常第一句話有八成都會是主旨句，也常常在文章中擔任承上啟下的角色。</span></p>
<p style=""color: #ff6666;""><strong>二、鎖定「定位句」</strong></p>
<p><span style=""font-weight: 400;"">根據定位詞找到定位句之後，從句子中找尋包含選項的關鍵字，再根據上下文來判斷題目和選項的正確與否。</span></p>
<p style=""color: #ff6666;""><strong>三、略讀非定位句，跳過不重要的舉例說明</strong></p>
<p><span style=""font-weight: 400;"">在大概理解舉例說明的對象和訊息的前提之下，可以不用多花時間閱讀沒有所需訊息的句子。</span></p>
<p style=""color: #ff6666;""><strong>四、多加注意介係詞</strong></p>
<p><span style=""font-weight: 400;"">表示原因、結果、語氣轉折等詞彙需要多加注意。</span></p>
<p style=""color: #ff6666;""><strong>五、若題目和舉例內容有關，要注意例子說明的對象和訊息</strong></p>
<p><span style=""font-weight: 400;"">如果題目涉及舉例的內容，要密切注意例子所傳達的訊息，對答題有所幫助。</span></p>
</div>"','published' => '1','og_title' => '只要3分鐘！帶你了解托福閱讀，提高答題正確率！','og_description' => '想要在托福(TOEFL)中考取高分，首先要克服的就是閱讀關卡，考題多樣性高、篇幅繁多的閱讀測驗是不少考生的噩夢。然而你對托福閱讀了解得夠透徹嗎？是否已經準備好接受閱讀測驗的考驗了呢？今篇不只是統整了詳盡的托福閱讀資訊，還會分享答題時可使用的小技巧，讓你更能掌握托福閱讀的整體結構，在考場上也能信心滿滿地突破第一道關卡！','meta_title' => '只要3分鐘！帶你了解托福閱讀，提高答題正確率！','meta_description' => '想要在托福(TOEFL)中考取高分，首先要克服的就是閱讀關卡，考題多樣性高、篇幅繁多的閱讀測驗是不少考生的噩夢。然而你對托福閱讀了解得夠透徹嗎？是否已經準備好接受閱讀測驗的考驗了呢？今篇不只是統整了詳盡的托福閱讀資訊，還會分享答題時可使用的小技巧，讓你更能掌握托福閱讀的整體結構，在考場上也能信心滿滿地突破第一道關卡！','canonical_url' => 'https://tw.english.agency/toefl-reading','feature_image_alt' => '只要三分鐘！帶你了解托福閱讀，提高答題正確率！','feature_image_title' => '只要三分鐘！帶你了解托福閱讀，提高答題正確率！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a898f32d0c21-d7nBM4MUNJ5AsjJoUQK2lC7Ij4T1Av-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-18 14:21:36"'],
  ['id' => '155','title' => '托福閱讀好難拿分？那是因為你沒有把握好技巧！','slug' => 'toefl-reading-skill','excerpt' => '對於無法掌握聽力及口說的考生來說，一定要掌握好閱讀測驗的機會，因為閱讀則是有付出則會有收穫。閱讀測驗以學術主題為測驗內容，如藝術風格評論、生物演化理論及人文歷史等，但別煩惱對於考試的閱讀主題不熟悉，因為該測驗主要目的是想知道考生是否了解文章內容，及架構而非專業能力。在準備時除了天天背單字外，也要掌握以下技巧提升高分機會！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p><span style=""font-weight: 400;"">對於無法掌握聽力及口說的考生來說，一定要掌握好閱讀測驗的機會，因為閱讀則是有付出則會有收穫。閱讀測驗以學術主題為測驗內容，如藝術風格評論、生物演化理論及人文歷史等，但別煩惱對於考試的閱讀主題不熟悉，因為該測驗主要目的是想知道考生是否了解文章內容，及架構而非專業能力。在準備時除了天天背單字外，也要掌握以下技巧提升高分機會！</span></p>
<p>&nbsp;</p>
<h2><strong>提升閱讀速度加快作答腳步</strong></h2>
<p><span style=""font-weight: 400;"">許多考生在準備閱讀測驗時，邊讀文章會習慣性的唸出聲音來，而這將降低閱讀速度。試想一般在讀中文文章時，我們並不會讀出聲音，而是下意識的將文章的結構及內容記下來，了解文章內容。因此，在練習閱讀時，試著在不要唸出聲音或默讀之下，看完了解文章內容及意涵，逐漸提升閱讀速度。</span></p>
<p>&nbsp;</p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8a699d12826-RhLN2z4GfCJkNRn5ooUuo3SQiCvqud-700x400.png"" alt="""" width=""700"" height=""400"" /></p>
<h2><strong>數字與記號輔助答題</strong></h2>
<p><span style=""font-weight: 400;"">托福閱讀測驗提供四個答案選項，有時可能看到第四個已經忘記前三個選項的內容，這時可以邊看選項時，在紙上做出自己習慣的記號，如Ｏ代表正確，？代表不確定，Ｘ代表不正確，作為辨別。舉例來說，題目有5題，選項有4個時，第一列1-4代表選項，左邊第一欄代表題數，在做第一題時，可以直接排除第1及4的選項，而考慮選擇2為答案。</span></p>
<table style=""width: 702px; height: 384px;"">
<tbody>
<tr>
<td style=""width: 125.109375px; text-align: center;"">&nbsp;</td>
<td style=""width: 140.28125px; text-align: center;"">
<p><span style=""font-weight: 400;"">1</span></p>
</td>
<td style=""width: 135.390625px; text-align: center;"">
<p><span style=""font-weight: 400;"">2</span></p>
</td>
<td style=""width: 135.53125px; text-align: center;"">
<p><span style=""font-weight: 400;"">3</span></p>
</td>
<td style=""width: 131.75px; text-align: center;"">
<p><span style=""font-weight: 400;"">4</span></p>
</td>
</tr>
<tr>
<td style=""width: 125.109375px; text-align: center;"">
<p><span style=""font-weight: 400;"">1</span></p>
</td>
<td style=""width: 140.28125px; text-align: center;"">
<p><span style=""font-weight: 400;"">X</span></p>
</td>
<td style=""width: 135.390625px; text-align: center;"">
<p><span style=""font-weight: 400;"">O</span></p>
</td>
<td style=""width: 135.53125px; text-align: center;"">
<p><span style=""font-weight: 400;"">?</span></p>
</td>
<td style=""width: 131.75px; text-align: center;"">
<p><span style=""font-weight: 400;"">X</span></p>
</td>
</tr>
<tr>
<td style=""width: 125.109375px; text-align: center;"">
<p><span style=""font-weight: 400;"">2</span></p>
</td>
<td style=""width: 140.28125px; text-align: center;"">
<p><span style=""font-weight: 400;"">X</span></p>
</td>
<td style=""width: 135.390625px; text-align: center;"">
<p><span style=""font-weight: 400;"">X</span></p>
</td>
<td style=""width: 135.53125px; text-align: center;"">
<p><span style=""font-weight: 400;"">O</span></p>
</td>
<td style=""width: 131.75px; text-align: center;"">
<p><span style=""font-weight: 400;"">X</span></p>
</td>
</tr>
<tr>
<td style=""width: 125.109375px; text-align: center;"">
<p><span style=""font-weight: 400;"">3</span></p>
</td>
<td style=""width: 140.28125px; text-align: center;"">&nbsp;</td>
<td style=""width: 135.390625px; text-align: center;"">&nbsp;</td>
<td style=""width: 135.53125px; text-align: center;"">&nbsp;</td>
<td style=""width: 131.75px; text-align: center;"">&nbsp;</td>
</tr>
<tr>
<td style=""width: 125.109375px; text-align: center;"">
<p><span style=""font-weight: 400;"">4</span></p>
</td>
<td style=""width: 140.28125px; text-align: center;"">&nbsp;</td>
<td style=""width: 135.390625px; text-align: center;"">&nbsp;</td>
<td style=""width: 135.53125px; text-align: center;"">&nbsp;</td>
<td style=""width: 131.75px; text-align: center;"">&nbsp;</td>
</tr>
<tr>
<td style=""width: 125.109375px; text-align: center;"">
<p><span style=""font-weight: 400;"">5</span></p>
</td>
<td style=""width: 140.28125px; text-align: center;"">&nbsp;</td>
<td style=""width: 135.390625px; text-align: center;"">&nbsp;</td>
<td style=""width: 135.53125px; text-align: center;"">&nbsp;</td>
<td style=""width: 131.75px; text-align: center;"">&nbsp;</td>
</tr>
</tbody>
</table>
<p><br /><br /></p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8a69af60bc6-47wJQEfqSDT0tyRdovDKmHrxEZuqgO-700x400.png"" alt="""" width=""700"" height=""400"" /></p>
<h2><strong>18分鐘為單位 一次練習3篇</strong></h2>
<p><span style=""font-weight: 400;"">一開始在練習閱讀測驗時，不能以還沒準備完全，有很多時間可以把文章看完的想法，一開始就要有時間限制的觀念，掌握自己在閱讀及答題上的速度。建議可以18分鐘為一個單位，一次練做3篇閱讀。在回答單字題的時後，不能猶豫直接答題，就算遇到不懂的單字也是如此，因為就算花10分鐘在那邊也想不起來那個單字的意思，不如將時間分配在更有機會拿到分數的題目。而在其他提習中，遇到看不懂的題目要在2分鐘內作答，若可以先選一個可能的答案後繼續作答。</span></p>
<p>&nbsp;</p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8a69c051694-PVOKtRhy3XaSalRfsG6OBUQXixN5oY-700x400.png"" alt="""" width=""700"" height=""400"" /></p>
<h2><strong>掌握關鍵字了解題目意涵 從文章找出答案</strong></h2>
<p><span style=""font-weight: 400;"">遇到Imply題時千萬不要自己想像推論考題及答案，這題目只是要考題目及閱讀內文隱藏的可能解答，或是最符合訊息邏輯的答案。建議遇到這樣的問題，找到專見字，並把該題目看熟後作答。同時在作答時，也可以先辨別題型後，再根據題目類型使不同的解題技巧。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">除了掌握考試技巧之外，基本功如單字量及閱讀量在考試前也需要大量的累積，才能有效的將閱讀技巧的效果提升，單靠考試技巧是無法保證考生可以拿到高分。建議持續閱讀提升英語語感及單字量，後續以18分鐘的閱讀限制下模擬練習。</span></p>
</div>"','published' => '1','og_title' => '托福閱讀好難拿分？那是因為你沒有把握好技巧！','og_description' => '對於無法掌握聽力及口說的考生來說，一定要掌握好閱讀測驗的機會，因為閱讀則是有付出則會有收穫。閱讀測驗以學術主題為測驗內容，如藝術風格評論、生物演化理論及人文歷史等，但別煩惱對於考試的閱讀主題不熟悉，因為該測驗主要目的是想知道考生是否了解文章內容，及架構而非專業能力。在準備時除了天天背單字外，也要掌握以下技巧提升高分機會！','meta_title' => '托福閱讀好難拿分？那是因為你沒有把握好技巧！','meta_description' => '對於無法掌握聽力及口說的考生來說，一定要掌握好閱讀測驗的機會，因為閱讀則是有付出則會有收穫。閱讀測驗以學術主題為測驗內容，如藝術風格評論、生物演化理論及人文歷史等，但別煩惱對於考試的閱讀主題不熟悉，因為該測驗主要目的是想知道考生是否了解文章內容，及架構而非專業能力。在準備時除了天天背單字外，也要掌握以下技巧提升高分機會！','canonical_url' => 'https://tw.english.agency/toefl-reading-skill','feature_image_alt' => '托福閱讀好難拿分？那是因為你沒有把握好技巧！','feature_image_title' => '托福閱讀好難拿分？那是因為你沒有把握好技巧！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8a6aad25664-fJ8npkz63a3pIHDE6IWWizxVPnrNq9-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-19 05:59:31"'],
  ['id' => '156','title' => '除了掌握好考試技巧之外，平常該怎麼提升托福閱讀的實力？','slug' => 'toefl-reading-exercise','excerpt' => '托福閱讀測驗主要是測試考生對於學術文章的理解，因此準備考試時，不僅是要熟悉考試技巧，同時也要提升閱讀速度。因此，平時的英語閱讀習慣養成也是非常重要！','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p><span style=""font-weight: 400;"">托福閱讀測驗主要是測試考生對於學術文章的理解，因此準備考試時，不僅是要熟悉考試技巧，同時也要提升閱讀速度。因此，平時的英語閱讀習慣養成也是非常重要！</span></p>
<ul>
<li style=""color: #ff6666;"">從每天10分鐘閱讀開始</li>
</ul>
<p><span style=""font-weight: 400;""><img title=""CNN"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8ae98d4ec6d-jYmXMI6KjT7v2BLqXpogUNUBjBvupO-2272x1270.png"" alt=""CNN"" width=""100%"" height=""400"" /></span></p>
<p>&nbsp;(<a href=""http://edition.cnn.com/cnn10"">CNN 10</a>：10分鐘英文新聞精選)</p>
<p><span style=""font-weight: 400;"">剛開始讀全英語文章時，考生可能讀了2句就想結束，或是跳著看完。建議可以結合自己的興趣，如藝術時尚、生活風尚、電影影評、歷史文化等，開啟英語閱讀習慣。</span></p>
<p><span style=""font-weight: 400;"">每天從1-2篇文章著手，全盤了解文章內容、架構及單字用法等，提升英語閱讀語感。</span>熟悉英語閱讀後，可以一天一篇托福的長文章開始增加閱讀速度，同時練習抓關鍵字及文章重點，並自己總結文章大綱及重點，反覆練習，習慣托福閱讀文章的內容與架構。</p>
<p>&nbsp;</p>
<ul>
<li style=""color: #ff6666;"">挑選自己有興趣的文章類型</li>
</ul>
<p>&nbsp;</p>
<p><img title=""scientificamerican"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8aea4360e53-pdOnAHH0Q8lMli5byyymJV0M3nHnLt-2532x1282.png"" alt=""scientificamerican"" width=""100%"" height=""400"" />&nbsp;</p>
<p><span style=""font-weight: 400;"">(<a href=""http://www.scientificamerican.com/"">Scientific American</a>：以科普主題為主的網站)</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">閱讀量逐步提升時，可從<a href=""https://tw.english.agency/toefl-reading"">閱讀的9種題型</a>中，每天挑一種練習，以10分鐘為單位練習閱讀文章及答題，從中熟悉每一種題目類型的考題方向及作答模式，同時也可以增加答題時間的掌控！</span></p>
<p><img title=""wired"" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8aeac1a0a35-9dLOA8rzuUM4moUebiRzMUdUbRDwjB-2026x902.png"" alt=""wired"" width=""100%"" height=""300"" /></p>
<p><span style=""font-weight: 400;"">(<a href=""http://www.wired.com/"">Wired</a>：國外知名科技生活網站)</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">而在做TPO練習時，從閱讀測驗文章認識不同的單字，整理出自己的單字筆記。托福閱讀測驗的單字重複率不低，若單字一重複在TPO中，表示這些單字非常重要，記得一定要熟習這些單字的意思及用法，做更好的準備。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">此外，若不曉得從何著手練習閱讀，建議可從上面推薦的3個網站，每天選擇一篇文章著手練習，提升閱讀能力！</span></p>
</div>"','published' => '1','og_title' => '除了掌握好考試技巧之外，平常該怎麼提升托福閱讀的實力？','og_description' => '托福閱讀測驗主要是測試考生對於學術文章的理解，因此準備考試時，不僅是要熟悉考試技巧，同時也要提升閱讀速度。因此，平時的英語閱讀習慣養成也是非常重要！','meta_title' => '除了掌握好考試技巧之外，平常該怎麼提升托福閱讀的實力？','meta_description' => '托福閱讀測驗主要是測試考生對於學術文章的理解，因此準備考試時，不僅是要熟悉考試技巧，同時也要提升閱讀速度。因此，平時的英語閱讀習慣養成也是非常重要！','canonical_url' => 'https://tw.english.agency/toefl-reading-exercise','feature_image_alt' => '除了掌握好考試技巧之外，平常該怎麼提升托福閱讀的實力？','feature_image_title' => '除了掌握好考試技巧之外，平常該怎麼提升托福閱讀的實力？','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8aece6e8564-XLVaDSazfws62DqeSD7Rxytf8PUxuv-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-19 15:24:08"'],
  ['id' => '157','title' => '超好用！準備托福一定要知道的資源網站、app！','slug' => 'toefl-app','excerpt' => '現在的托福考生已經比以往幸福很多，網路上有許多免費但又很實用的網站及手機app讓你隨時隨地打開電腦、手機就能複習托福。不管是自修托福或報名托福補習班的課程，今天介紹的網站能讓你在準備托福時更有效率。','content' => '"<div style=""line-height: 30px; letter-spacing: 3px; font-size: 14px; font-family: PMingLiU;"">
<p><span style=""font-weight: 400;"">現在的托福考生已經比以往幸福很多，網路上有許多免費但又很實用的網站及手機app讓你隨時隨地打開電腦、手機就能複習托福。不管是自修托福或報名托福補習班的課程，今天介紹的網站能讓你在準備托福時更有效率。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">雖然托福備考資源百百種，但我覺得最棒、進步最有感的就是</span><a href=""http://toefl.kmf.com/tpo-exam""><span style=""font-weight: 400;"">考滿分</span></a><span style=""font-weight: 400;"">，考滿分有網頁和<a href=""http://www.kmf.com/static/appcenter?website=toefl"">app版</a>，平時通勤時可以打開手機app馬上進入練習模式或檢討錯誤。</span></p>
<p><span style=""font-weight: 400;"">除了TOEFL，上面也有 <a href=""http://gre.kmf.com/?hmsr=t&amp;hmmd=hf&amp;hmpl=&amp;hmkw=&amp;hmci="">GRE</a>，<a href=""http://sat.kmf.com/?hmsr=t&amp;hmmd=hf&amp;hmpl=&amp;hmkw=&amp;hmci="">SAT</a>，<a href=""http://ielts.kmf.com"">IELTS</a>等，只要註冊登入會員後就可以利用這個網站擬定讀書計畫，像是距離考試還有幾天、目標分數等。</span></p>
<p><span style=""font-weight: 400;"">網站上面有完整的TPO練習題和解答，模擬考的介面和托福考試時幾乎一模一樣，不僅可以習慣考試時介面的操作，也有助於減緩實際測驗時的緊張感。</span></p>
<p><span style=""font-weight: 400;"">如果是平時練習、沒有時間做完整一組TPO時，就可以利用練習的模式（如下圖），根據當天的讀書計畫去選擇教材。</span></p>
<p>&nbsp;</p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8bc3e2d35d2-ATMc59AkwFAZjrDsq6ufTMO8ZKOjp2-946x348.png"" alt="""" width=""100%"" height=""300"" /><br /><br /></p>
<p><span style=""font-weight: 400;"">考滿分最棒的就是聽、說、讀、寫可以依據學科（社會科學、自然、生物、考古）、弱項、題型等去練習，當練習或寫模擬考到一定程度時，即可根據考滿分的分析數據去加強一些容易出錯或不熟悉的地方。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8bc40054173-riLlHFCOcMUEzZXVGvlnoQleg0ri56-726x588.png"" alt="""" width=""726"" height=""588"" /></span></p>
<p><span style=""font-weight: 400;"">當有瑣碎的時間很難做練習時，就能打開詞彙練習。</span></p>
<p><span style=""font-weight: 400;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8bc407cbdf5-2mkCnov8B6QxI9zq2J7FronRM9dJaa-712x630.png"" alt="""" width=""712"" height=""630"" /></span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">例如聽力詞彙是以10個單字為一組，分次進行，學完10個單字後就會進入複習階段，如果忘記該單字就能標記&ldquo;忘了&rdquo;，等到整個測驗完成後再來對標記過的單字做重點複習。</span></p>
<p>&nbsp;</p>
<p>&nbsp;<img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8bc40e1b73b-4f6h7A6LRyQ5Zmj8gF5RkrhqwZ4By1-694x482.png"" alt="""" width=""694"" height=""482"" /></p>
<p><span style=""font-weight: 400;"">考滿分網站其實還有很多能好好利用的資源，像是<a href=""https://toefl.kmf.com/speak/gold/1"">黃金口語80題</a>就很適合拿來練習思考邏輯、解答的速度，考滿分寫作萬能句型有許多不錯的模板，可以讓文法較弱的考生參考、學習，不是說要硬背或死記上面的template，而是透過好的資源去認識更多寫作句型或好利用的詞彙，再去延伸出屬於自己的模板。</span></p>
<p>&nbsp;</p>
<p><span style=""font-weight: 400;"">以上就是今天介紹、大力推薦的備考網站，相信考生們如果好好利用考滿分一定能感受到進步的幅度，根據難易度去選擇適合目前本身的英文實力，搭配著讀書計畫和網站上面的資源，準備托福起來能更加得心應手。</span></p>
</div>"','published' => '1','og_title' => '超好用！準備托福一定要知道的資源網站、app！','og_description' => '現在的托福考生已經比以往幸福很多，網路上有許多免費但又很實用的網站及手機app讓你隨時隨地打開電腦、手機就能複習托福。不管是自修托福或報名托福補習班的課程，今天介紹的網站能讓你在準備托福時更有效率。','meta_title' => '超好用！準備托福一定要知道的資源網站、app！','meta_description' => '現在的托福考生已經比以往幸福很多，網路上有許多免費但又很實用的網站及手機app讓你隨時隨地打開電腦、手機就能複習托福。不管是自修托福或報名托福補習班的課程，今天介紹的網站能讓你在準備托福時更有效率。','canonical_url' => 'https://tw.english.agency/toefl-app','feature_image_alt' => '超好用！準備托福一定要知道的資源網站、app！','feature_image_title' => '超好用！準備托福一定要知道的資源網站、app！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8bc64c8b2a2-uuDlYxrwspCfIlNK7VA2LgPxvydVL7-1200x630.png','author_id' => '13','category_id' => '15','created_at' => '"2018-02-20 06:38:29"'],
  ['id' => '158','title' => '面試應徵英文怎麼說？想要錄取成功看這邊！','slug' => 'interview-application','excerpt' => '如果你應徵的工作是外商公司、大型企業或者是負責對外的職位，那面試時多以英文進行。在無法得知面試者的提問時，只能自己多加練習，平時培養使用英文的習慣，因為口語英文是很難在短時間內完全提升。','content' => '"<p>如果你應徵的工作是外商公司、大型企業或者是負責對外的職位，那面試時多以英文進行。在無法得知面試者的提問時，只能自己多加練習，平時培養使用英文的習慣，因為口語英文是很難在短時間內完全提升。</p>
<p>本節僅羅列一些面試時通用語句，以協助讀者有練習的範本。這些句子大概在一場正式的面試不同階段都派得上用場，但針對專業問答及隨機提問，讀者就需長期培養。</p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8c24dcded12-FzwtXd1AuHEuLZ7qcyUuDiF2EVjt8I-600x400.jpeg"" alt="""" width=""600"" height=""400"" /></p>
<h2><span style=""color: #e74c3c;"">面試應徵時的英文：</span></h2>
<p><em><strong><span style=""color: #225378;"">Good Morning，Sir and/or Madam. I am grateful to be here to present myself.</span></strong></em></p>
<p>（早安！各位女士先生，很榮幸在此介紹我自己。）</p>
<p><em><strong><span style=""color: #225378;"">Good afternoon，Sir and/or Madam. I am very happy to have this opportunity to introduce myself.</span></strong></em></p>
<p>（午安！各位女士先生，很高興有此機會介紹我自己。）</p>
<p><em><strong><span style=""color: #225378;"">Good day，Sir and/or Madam. It is my pleasure as an interviewee to meet you.</span></strong></em></p>
<p>（日安！各位女士先生，很榮幸身為面試者與您見面。）<img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8c1f07f3e55-bvHh78jqkpxr9pkTLDc2LdPwNdFJcu-439x148.png"" alt="""" width=""439"" height=""148"" /></p>
<h2><span style=""color: #e74c3c;"">面試時自我介紹並說明應徵職位：</span></h2>
<p><em><strong><span style=""color: #225378;"">My name is James Chang，and I am applying for the position Sales Specialist in the Marketing Department.</span></strong> </em></p>
<p>（我叫張詹姆士，我要應徵行銷部門的業務專員。）</p>
<p><em><strong><span style=""color: #225378;"">My name is Winland Lee，and I am a candidate for the job － applied engineer.</span></strong></em></p>
<p>（我是李溫嵐德，是應徵應用工程師一職的候選人。）</p>
<h2><span style=""color: #e74c3c;"">剛畢業無工作經驗者：</span></h2>
<p><em><strong><span style=""color: #225378;"">Even if I never have any work experience in this job，I will try my best to learn fast if you give me this opportunity.</span></strong></em></p>
<p>（即使沒有此工作的任何相關經驗，但如有機會獲得此職位，我將盡全力學習。）</p>
<p><em><strong><span style=""color: #225378;"">I just graduated from the university，but I shall <u>utilize</u> (＝apply) my skills and knowledge when I get this position.</span></strong></em></p>
<p>（雖然剛從大學畢業，但是當我被錄取時，將全力運用我的技能與知識。）</p>
<p><em><strong><span style=""color: #225378;"">My work attitude and learning ability will get me into orbit in a short time.</span></strong></em></p>
<p>（我的工作態度與學習能力將於短時間內步入軌道。）</p>
<p><em><strong><span style=""color: #225378;"">I think your company/this job can provide me a great opportunity to learn and grow.</span></strong></em></p>
<p>（我認為貴公司/這份工作可以提供我很好學習與成長的機會。）</p>
<p><em><strong><span style=""color: #225378;"">This position will be a great chance of learning experience and a challenge to my first job in my career. I will definitely benefit from it under your supervision.</span></strong></em></p>
<p>（這份工作將是學習經驗的好機會，也是我職場生涯中第一份工作的挑戰。在您的指導下，我將從中獲得許多。）</p>
<p><em><strong><span style=""color: #225378;"">If you could grant me this opportunity，I should devote myself to work in return.</span></strong></em></p>
<p>（倘若您賜予我這個機會，我會全力投入工作以做回報。）</p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8c1f34e2fb5-CkhtXjOBUykXIq0hTT0MloxmthWMGQ-435x462.png"" alt="""" width=""435"" height=""462"" /></p>
<h2><span style=""color: #e74c3c;""><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8c24fdd5bf6-xKpwk2NOk9LGucw45IsTVnruNbNvPo-339x275.jpeg"" alt="""" width=""339"" height=""275"" /></span></h2>
<h2><span style=""color: #e74c3c;"">面試時要強調自己是合適人選（有經驗者）：</span></h2>
<p><em><strong><span style=""color: #225378;"">My educational background and three years hands-on experience have perfectly matched to this position.</span></strong></em></p>
<p>（我的教育背景以及三年的實務經驗，讓我成為該職位的最佳人選。）</p>
<p><em><strong><span style=""color: #225378;"">I had several case experiences of project management in the past few years by which I am tailor-made to become the team leader of your new project.</span></strong></em></p>
<p>（在過去幾年，我擁有許多專案管理的實戰經驗，所以我是貴公司新專案團隊主管職位量身訂做之人選。）</p>
<p><em><strong><span style=""color: #225378;"">My major in accounting for both undergraduate and postgraduate degrees is just what you need for this open position.</span></strong> </em></p>
<p>（我的學士與碩士主修都是會計，正好符合貴公司徵才的條件。）</p>
<p><em><strong><span style=""color: #225378;"">I had a successful experience of introducing the products of Hi-Capacity Corporation into the European markets，and it is good enough to meet the requirements for this position.</span></strong></em></p>
<p>（我在Hi-Capacity公司時，成功將公司產品銷售至歐洲市場，正好符合該職位的徵才條件。）</p>
<p><img title="""" src=""https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8c1f57aa76a-sPm5eVL0BACLzLzk5bKltgnQaGEegw-434x163.png"" alt="""" width=""434"" height=""163"" /></p>
<h2><span style=""color: #e74c3c;"">獲得錄取：</span></h2>
<p><em><strong><span style=""color: #225378;"">The pleasure is mine to become one of you in Mactan Machinery.</span></strong></em></p>
<p>（能在馬克坦機器公司上班是我的榮幸。）</p>
<p><em><strong><span style=""color: #225378;"">I can&rsquo;t wait to devote myself to Codova Trading Co.</span></strong></em></p>
<p>（我迫不及待想為科多發貿易公司貢獻我的專才。）</p>
<p><em><strong><span style=""color: #225378;"">I welcome this opportunity to work with Lapu Metals.</span></strong></em></p>
<p>（我很高興接受在拉普金屬公司工作。）</p>"','published' => '1','og_title' => '面試應徵英文怎麼說？想要錄取成功看這邊！','og_description' => '如果你應徵的工作是外商公司、大型企業或者是負責對外的職位，那面試時多以英文進行。在無法得知面試者的提問時，只能自己多加練習，平時培養使用英文的習慣，因為口語英文是很難在短時間內完全提升。','meta_title' => '面試應徵英文怎麼說？想要錄取成功看這邊！','meta_description' => '如果你應徵的工作是外商公司、大型企業或者是負責對外的職位，那面試時多以英文進行。在無法得知面試者的提問時，只能自己多加練習，平時培養使用英文的習慣，因為口語英文是很難在短時間內完全提升。','canonical_url' => 'https://tw.english.agency/interview-application','feature_image_alt' => '面試應徵英文怎麼說？想要錄取成功看這邊！','feature_image_title' => '面試應徵英文怎麼說？想要錄取成功看這邊！','feature_image_url' => 'https://media-english-agency.s3.ap-southeast-1.amazonaws.com/images/5a8c287a57ddb-QbIjL4xYMj1y40Nzkj8504SxJ6UiQZ-14173x7441.jpeg','author_id' => '1','category_id' => '18','created_at' => '"2018-02-20 13:25:26"']
];

return $fields;
