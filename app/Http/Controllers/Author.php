<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\AuthorServ;
use App\Services\PostServ;
use App\Services\CategoryServ;

class Author extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ  = app(SessionServ::class);
        $this->authorServ = app(AuthorServ::class);
        $this->postServ = app(PostServ::class);
        $this->categoryServ = app(CategoryServ::class);
    } // END function



    /**
     * find
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     *
     * @throws
     * @return
     */
    public function find(Request $request, $id)
    {
/*
        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if
*/
        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if

/*
        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if
*/

        $authorDatum = $this->authorServ->findById($id);

        if ($authorDatum->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if

        $authorDatum->first()->s3_url = env('AWS_BUCKET_URL');


        // $resultData = ['session' => $sessionCode, 'author' => $authorDatum->first()];
        $resultData = ['author' => $authorDatum->first()];

        Jsonponse::success('find success', $resultData);
    } // END function


    /**
     * findAll
     *
     * @method GET
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $order_way
     *
     * @throws
     * @return
     */
    public function findAll(Request $request, $order_way = 'ASC', $page = -1)
    {
/*
        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if
*/

        $orderWay = strtoupper($order_way);
        $orderWayValidator = Validator::make(['order_way' => $orderWay],
            ['order_way' => ['in:ASC,DESC']]
        );

        if ($orderWayValidator->fails()) {
            $code = 422;
            $comment = 'order_way error';

            Jsonponse::fail($comment, $code);
        } // END if


        $orderby = ['id' => $orderWay];

        $authorData = $this->authorServ->findAll($orderby, $page);

        if ($authorData->isEmpty()) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        // $resultData = ['session' => $sessionCode, 'authors' => $authorData->all()];
        $resultData = ['authors' => $authorData->all()];

        Jsonponse::success('find success', $resultData);
    } // END function


} // END class
