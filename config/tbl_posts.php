<?php
$fields = [
    'POSTS_STATUS_DRAFT'  => 'draft',
    'POSTS_STATUS_PUBLISH'  => 'publish',
    'POSTS_STATUS_DISABLE' => 'disable',
    'POSTS_STATUS_DELETE'  => 'delete',

    'POSTS_HTML_MADE_TRUE'  => 'true',
    'POSTS_HTML_MADE_FALSE' => 'false',

    'POSTS_SYNCED_TRUE'  => 'true',
    'POSTS_SYNCED_FALSE' => 'false',
];

$fields['DEFAULT_POSTS_STATUS'] = $fields['POSTS_STATUS_DRAFT'];
$fields['POSTS_STATUS'] = [
    $fields['POSTS_STATUS_DRAFT'],
    $fields['POSTS_STATUS_PUBLISH'],
    $fields['POSTS_STATUS_DISABLE'],
    $fields['POSTS_STATUS_DELETE']
];

$fields['DEFAULT_POSTS_HTML_MADE'] = $fields['POSTS_HTML_MADE_FALSE'];
$fields['POSTS_HTML_MADES'] = [
    $fields['POSTS_HTML_MADE_TRUE'],
    $fields['POSTS_HTML_MADE_FALSE']
];

$fields['DEFAULT_POSTS_SYNCED'] = $fields['POSTS_SYNCED_FALSE'];
$fields['POSTS_SYNCEDS'] = [
    $fields['POSTS_SYNCED_TRUE'],
    $fields['POSTS_SYNCED_FALSE']
];

return $fields;