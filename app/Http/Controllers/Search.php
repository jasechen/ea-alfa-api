<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\PostServ;
use App\Services\CategoryServ;
use App\Services\AuthorServ;
use App\Services\FileServ;


class Search extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->postServ   = app(PostServ::class);
        $this->categoryServ = app(CategoryServ::class);
        $this->authorServ = app(AuthorServ::class);
        $this->fileServ = app(FileServ::class);
    } // END function


    /**
     * findByTerm
     *
     * @method GET
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function findByTerm(Request $request, $term, $page = -1)
    {

        // $sessionCode = $request->header('session');

        // if (empty($sessionCode)) {
        //     $code = 400;
        //     $comment = 'session empty';

        //     Jsonponse::fail($comment, $code);
        // } // END if

        if (empty($term)) {
            $code = 400;
            $comment = 'term empty';

            Jsonponse::fail($comment, $code);
        } // END if


        // $isAlive = $this->sessionServ->isAlive($sessionCode);

        // if (empty($isAlive)) {
        //     $code = 410;
        //     $comment = 'session is NOT alive';

        //     Jsonponse::fail($comment, $code);
        // } // END if


        $term = urldecode($term);
        $search = ['&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&ndash;', '&lt;', '&gt;', '&middot;', '&hellip;'];

        $postEmpty = $authorEmpty = false;
        $finalPostData = $finalAuthorData = [];
        $status = config('tbl_posts.POSTS_STATUS_PUBLISH');

        $postData = $this->postServ->findTitleByTerm($term, $status, ['p.id' => 'DESC'], $page);

        if ($postData->isEmpty()) {
            $postEmpty = true;
        } else {

            foreach ($postData->all() as $postDatum) {
                $postDatum->content = strip_tags(trim($postDatum->content));
                $postDatum->content = str_replace($search,'', $postDatum->content);

                $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
                $postDatum->category = $categoryDatum->first();

                $authorDatum = $this->authorServ->findById($postDatum->author_id);
                $authorDatum->first()->avatar_links = [];
                if (!empty($authorDatum->first()->avatar)) {
                    $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                    $authorDatum->first()->avatar_links = $avayarLinkData;
                } // END if else
                $postDatum->author = $authorDatum->first();

                $postDatum->cover_links = [];
                if (!empty($postDatum->cover)) {
                    $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                    $postDatum->cover_links = $coverLinkData;
                } // END if else

                $postDatum->s3_url = env('AWS_BUCKET_URL');
                $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

                array_push($finalPostData, $postDatum);
            } // END foreach
        } // END if


        $authorData = $this->authorServ->findFullnameByTerm($term, ['a.id' => 'DESC'], 1, 1);

        if ($authorData->isEmpty()) {
            $authorEmpty = true;
        } else {

            foreach ($authorData->all() as $authorDatum) {

                $postData = $this->postServ->findByAuthorId($authorDatum->id, $status, ['p.id' => 'DESC'], $page);

                if ($postData->isNotEmpty()) {

                    foreach ($postData->all() as $postDatum) {
                        $postDatum->content = strip_tags($postDatum->content);

                        $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
                        $postDatum->category = $categoryDatum->first();

                        $authorDatum->avatar_links = [];
                        if (!empty($authorDatum->avatar)) {
                            $avayarLinkData = $this->fileServ->findLinks($authorDatum->avatar);
                            $authorDatum->avatar_links = $avayarLinkData;
                        } // END if else
                        $postDatum->author = $authorDatum;

                        $postDatum->cover_links = [];
                        if (!empty($postDatum->cover)) {
                            $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                            $postDatum->cover_links = $coverLinkData;
                        } // END if else

                        $postDatum->s3_url = env('AWS_BUCKET_URL');
                        $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

                        array_push($finalAuthorData, $postDatum);
                    } // END foreach

                } // END if

            } // END foreach

        } // END if


        if (!empty($postEmpty) AND !empty($authorEmpty)) {
            Jsonponse::success('data empty', [], 204);
        } // END if


        // $resultData = ['session' => $sessionCode, 'posts' => $finalPostData, 'authors' => $finalAuthorData];
        $resultData = ['posts' => $finalPostData, 'authors' => $finalAuthorData];

        Jsonponse::success('find success', $resultData);
    } // END function

} // END class
