<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnToPostsAndCategoriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('posts', function (Blueprint $table) {
            //
            $table->boolean('recommended')->default(false)->after('status');

            $table->index(['recommended', 'category_id']);
        });
        Schema::connection('mysql')->table('categories', function (Blueprint $table) {
            //
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('posts', function (Blueprint $table) {
            //
            $table->dropColumn('recommended');
        });
        Schema::connection('mysql')->table('categories', function (Blueprint $table) {
            //
            $table->text('description')->nullable()->after('name');
        });
    }
}
