<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use App\Entities\CategorySeo;

use Package\Litedbquery\Litedbquery;
use Kra8\Snowflake\Snowflake;


/**
 * Class CategorySeoRepo
 *
 * @package namespace App\Repositories;
 */
class CategorySeoRepo extends CategorySeo
{


    public function __construct()
    {

    } // END function


    /*
     *
     */
    public function fetch($query, $bindings = [])
    {
        $db = new Litedbquery($this->readConnection);

        return $db->fetch($query, $bindings);
    } // END function


    /**
     *
     */
    public function fetchCount($where = [])
    {
        $db = new Litedbquery($this->readConnection);

        return $db->fetchData($this->table, $where)->count();
    } // END function


    /**
     *
     */
    public function fetchDatum($where = [])
    {
        $db = new Litedbquery($this->readConnection);

        return $db->fetchDatum($this->table, $where);
    } // END function


    /**
     *
     */
    public function fetchData($where = [], $orderby = [], $page = -1, $numItems = 20)
    {
        $db = new Litedbquery($this->readConnection);

        return $db->fetchData($this->table, $where, $orderby, $page, $numItems);
    } // END function


    /**
     *
     */
    public function createData($data)
    {
        $db = new Litedbquery($this->connection);

        $data['id'] = app(Snowflake::class)->next();

        $result = $db->insert($this->table, $data);

        return empty($result) ? new Collection() : $this->fetchDatum(['id' => $data['id']]);
    } // END function


    /**
     *
     */
    public function updateData($data, $where = [])
    {
        $db = new Litedbquery($this->connection);

        $result = $db->update($this->table, $data, $where);

        return empty($result) ? new Collection() : $this->fetchDatum($where);
    } // END function


    /**
     *
     */
    public function deleteData($where = [])
    {
        $db = new Litedbquery($this->connection);

        return $db->delete($this->table, $where);
    } // END function


}
