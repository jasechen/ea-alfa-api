<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />

        <title>{{ $name }} - {{ $site_name }}</title>
        <meta name="title" content="{{ $meta_title }}" />
        <meta name="description" content="{{ $meta_description }}">
        <meta name="robots" content="noodp" />
        <link rel="canonical" href="{{ $url_path }}" />

        <meta property="fb:admins" content="100000010603810">
        <meta property="fb:page_id" content="985568048148380">
        <meta property="fb:app_id" content="1808465192748376">
        <meta property="og:stie_name" content="{{ $site_name }}">
        <meta property="og:title" content="{{ $og_title }}" />
        <meta property="og:description" content="{{ $og_description }}" />

        <meta property="og:url" content="{{ env('WEB_URL') }}{{ $url_path }}" />
        <meta property="og:image" content="@isset($cover_links['o']){{ env('WEB_URL') }}/{{ $cover_links['o'] }}@endisset" />
        <meta property="og:type" content="website" />
        <meta property="twitter:site" content="{{ $site_name }}">

        <link rel="stylesheet" href="app?nm=style" />

    </head>

    <body>

        <div class="app">
            <view-header></view-header>
            <div class="container" :class="{richview:isRichview,essayview:isBoardShow}">
                <div class="lumber" :class="{'is-mobile-visiable':isMobileVisiable}" v-if="isLumberShow">
                    <button class="shortcut" @click="onShortcutClick()"></button>
                    <view-nav-categories></view-nav-categories>
                    <article v-if="!isRichview">

                        <a href="/">Home</a>
                        <a href="{{ $url_path }}">{{ $slug }}</a>
                        <img src="@isset($cover_links['o']){{ env('WEB_URL') }}/{{ $cover_links['o'] }}@endisset" />
                        <h1>{{ $name }}</h1>
                        <section>{{ $excerpt }}</section>
                        <aside>
@isset($recommend_posts[0]['cover_links']['o'])
                            <h3>推薦</h3>
@foreach ($recommend_posts as $recommend_post)
    <a>
        <img src="@isset($recommend_post['cover_links']['o']){{ env('WEB_URL') }}/{{ $recommend_post['cover_links']['o'] }}@endisset" />
        <h4>{{$recommend_post['title']}}</h4>
        <summary>{{$recommend_post['excerpt']}}</summary>
    </a>
@endforeach
@endisset
                        </aside>

                    </article>
                    <footer class="guides" v-if="!isRichview">
                        <a href="/about-us">EA 英語職涯網介紹</a>
                        <a href="/about-us/service-policy">服務條款</a>
                        <a href="/about-us/privacy-policy">隱私權政策</a>
                        <a href="https://www.english.agency/">Educational Institution</a>
                        <a href="/join-us/家教合作申請">我是家教/補習班</a>
                        <a href="/join-us/教育顧問合作申請">我是教育顧問/代辦</a>
                        <a href="/join-us/媒體合作申請">媒體合作</a>
                        <div>
                            <span>服務信箱：info@english.agency</span>
                            <span>服務時間：週一至週五 10:00-18:00</span>
                        </div>
                        <div class="footer-copyright">
                            Copyright © 2017 - 2018 tw.English.Agency
                        </div>
                    </footer>
                </div>
                <view-grid v-if="isGridShow"></view-grid>
            </div>
            <view-crm></view-crm>
        </div>

    </body>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
    <script src="app?nm=script"></script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "{{ env('WEB_URL') }}",
      "name": "首頁",
      "image": ""
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "{{ env('WEB_URL') }}{{ $url_path }}",
      "name": "{{ $slug }}",
      "image": "@isset($cover_links['o']){{ env('WEB_URL') }}/{{ $cover_links['o'] }}@endisset"
    }
  }]
}
</script>
</html>