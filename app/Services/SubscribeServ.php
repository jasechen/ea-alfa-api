<?php

namespace App\Services;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

use App\Repositories\SubscriberRepo;


/**
 * Class SubscribeServ
 *
 * @package namespace App\Services;
 */
class SubscribeServ
{


    public function __construct()
    {

        $this->subscriberRepo = new SubscriberRepo();
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->subscriberRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->subscriberRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $email
     *
     * @return
     */
    public function create($name, $email)
    {
        $data = ['name'   => $name,
                 'email'  => $email,
                 'status' => config('tbl_subscribers.DEFAULT_SUBSCRIBERS_STATUS'),
        ];

        return $this->subscriberRepo->createData($data);
    } // END function


    /*
     * findAll
     *
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findAll($orderby = [], $page = -1, $numItems = 10)
    {
        return $this->subscriberRepo->fetchData([], $orderby, $page, $numItems);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     *
     * @return
     */
    public function findByStatus($status, $orderby = [], $page = -1, $numItems = 10)
    {
        $where = ['status' => $status];

        return $this->subscriberRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByEmail
     *
     * @param $email
     *
     * @return
     */
    public function findByEmail($email)
    {
        $where = ['email' => $email];

        return $this->subscriberRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->subscriberRepo->fetchDatum($where);
    } // END function

}
