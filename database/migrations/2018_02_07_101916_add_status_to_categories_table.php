<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('categories', function (Blueprint $table) {
            //
            $table->enum('status', ['enable', 'delete', 'disable'])->default('enable')->after('cover');
            $table->boolean('html_made')->default(false)->after('status');
            $table->boolean('synced')->default(false)->after('html_made');

            $table->index(['status','html_made','synced']);
            $table->index(['html_made','synced']);
            $table->index('synced');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('categories', function (Blueprint $table) {
            //
            $table->dropColumn('status');
            $table->dropColumn('html_made');
            $table->dropColumn('synced');
        });
    }
}
